-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/14/2018 18:21:27"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	HexClock IS
    PORT (
	CLK : IN std_logic;
	RST : IN std_logic;
	Error : OUT std_logic;
	D0 : OUT std_logic_vector(6 DOWNTO 0);
	D1 : OUT std_logic_vector(6 DOWNTO 0);
	D2 : OUT std_logic_vector(6 DOWNTO 0);
	D3 : OUT std_logic_vector(6 DOWNTO 0)
	);
END HexClock;

-- Design Ports Information
-- Error	=>  Location: PIN_N10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[0]	=>  Location: PIN_H10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[1]	=>  Location: PIN_K13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[2]	=>  Location: PIN_J13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[3]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[4]	=>  Location: PIN_L12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[5]	=>  Location: PIN_L13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[6]	=>  Location: PIN_N13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[0]	=>  Location: PIN_K10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[1]	=>  Location: PIN_L11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[2]	=>  Location: PIN_N12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[3]	=>  Location: PIN_M11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[4]	=>  Location: PIN_N11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[5]	=>  Location: PIN_K12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[6]	=>  Location: PIN_K11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[0]	=>  Location: PIN_C11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[1]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[2]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[3]	=>  Location: PIN_D13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[4]	=>  Location: PIN_D10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[5]	=>  Location: PIN_E10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[6]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[0]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[1]	=>  Location: PIN_G10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[2]	=>  Location: PIN_G9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[3]	=>  Location: PIN_F10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[4]	=>  Location: PIN_E13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[5]	=>  Location: PIN_D11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[6]	=>  Location: PIN_F9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RST	=>  Location: PIN_M13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_J7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF HexClock IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_RST : std_logic;
SIGNAL ww_Error : std_logic;
SIGNAL ww_D0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D3 : std_logic_vector(6 DOWNTO 0);
SIGNAL \CLK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \MCLK|Buff|s_q~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \Error~output_o\ : std_logic;
SIGNAL \D0[0]~output_o\ : std_logic;
SIGNAL \D0[1]~output_o\ : std_logic;
SIGNAL \D0[2]~output_o\ : std_logic;
SIGNAL \D0[3]~output_o\ : std_logic;
SIGNAL \D0[4]~output_o\ : std_logic;
SIGNAL \D0[5]~output_o\ : std_logic;
SIGNAL \D0[6]~output_o\ : std_logic;
SIGNAL \D1[0]~output_o\ : std_logic;
SIGNAL \D1[1]~output_o\ : std_logic;
SIGNAL \D1[2]~output_o\ : std_logic;
SIGNAL \D1[3]~output_o\ : std_logic;
SIGNAL \D1[4]~output_o\ : std_logic;
SIGNAL \D1[5]~output_o\ : std_logic;
SIGNAL \D1[6]~output_o\ : std_logic;
SIGNAL \D2[0]~output_o\ : std_logic;
SIGNAL \D2[1]~output_o\ : std_logic;
SIGNAL \D2[2]~output_o\ : std_logic;
SIGNAL \D2[3]~output_o\ : std_logic;
SIGNAL \D2[4]~output_o\ : std_logic;
SIGNAL \D2[5]~output_o\ : std_logic;
SIGNAL \D2[6]~output_o\ : std_logic;
SIGNAL \D3[0]~output_o\ : std_logic;
SIGNAL \D3[1]~output_o\ : std_logic;
SIGNAL \D3[2]~output_o\ : std_logic;
SIGNAL \D3[3]~output_o\ : std_logic;
SIGNAL \D3[4]~output_o\ : std_logic;
SIGNAL \D3[5]~output_o\ : std_logic;
SIGNAL \D3[6]~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \CLK~inputclkctrl_outclk\ : std_logic;
SIGNAL \MCLK|Count|Add0~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[1]~q\ : std_logic;
SIGNAL \MCLK|Equal0~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Equal0~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[0]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[0]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~1\ : std_logic;
SIGNAL \MCLK|Count|Add0~2_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[2]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~3\ : std_logic;
SIGNAL \MCLK|Count|Add0~4_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[3]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~5\ : std_logic;
SIGNAL \MCLK|Count|Add0~6_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[4]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~7\ : std_logic;
SIGNAL \MCLK|Count|Add0~8_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[5]~q\ : std_logic;
SIGNAL \MCLK|Equal0~1_combout\ : std_logic;
SIGNAL \MCLK|s_qe~q\ : std_logic;
SIGNAL \MCLK|Buff|qn~0_combout\ : std_logic;
SIGNAL \MCLK|Buff|qn~feeder_combout\ : std_logic;
SIGNAL \MCLK|Buff|qn~q\ : std_logic;
SIGNAL \MCLK|Buff|s_q~combout\ : std_logic;
SIGNAL \MCLK|Buff|s_q~clkctrl_outclk\ : std_logic;
SIGNAL \Tick:usv[0]~0_combout\ : std_logic;
SIGNAL \Tick:usv[0]~q\ : std_logic;
SIGNAL \Tick:usv[1]~1_combout\ : std_logic;
SIGNAL \Tick:usv[1]~q\ : std_logic;
SIGNAL \Tick:usv[1]~2\ : std_logic;
SIGNAL \Tick:usv[2]~1_combout\ : std_logic;
SIGNAL \Tick:usv[2]~q\ : std_logic;
SIGNAL \Tick:usv[2]~2\ : std_logic;
SIGNAL \Tick:usv[3]~1_combout\ : std_logic;
SIGNAL \Tick:usv[3]~q\ : std_logic;
SIGNAL \DAT0|Mux6~0_combout\ : std_logic;
SIGNAL \RST~input_o\ : std_logic;
SIGNAL \DAT0|Mux6~1_combout\ : std_logic;
SIGNAL \DAT0|Mux5~0_combout\ : std_logic;
SIGNAL \DAT0|Mux5~1_combout\ : std_logic;
SIGNAL \DAT0|Mux4~0_combout\ : std_logic;
SIGNAL \DAT0|Mux4~1_combout\ : std_logic;
SIGNAL \DAT0|Mux3~0_combout\ : std_logic;
SIGNAL \DAT0|Mux3~1_combout\ : std_logic;
SIGNAL \DAT0|Mux2~0_combout\ : std_logic;
SIGNAL \DAT0|Mux2~1_combout\ : std_logic;
SIGNAL \DAT0|Mux1~0_combout\ : std_logic;
SIGNAL \DAT0|Mux1~1_combout\ : std_logic;
SIGNAL \DAT0|Mux0~0_combout\ : std_logic;
SIGNAL \DAT0|Mux0~1_combout\ : std_logic;
SIGNAL \Tick:usv[3]~2\ : std_logic;
SIGNAL \Tick:usv[4]~1_combout\ : std_logic;
SIGNAL \Tick:usv[4]~q\ : std_logic;
SIGNAL \Tick:usv[4]~2\ : std_logic;
SIGNAL \Tick:usv[5]~1_combout\ : std_logic;
SIGNAL \Tick:usv[5]~q\ : std_logic;
SIGNAL \Tick:usv[5]~2\ : std_logic;
SIGNAL \Tick:usv[6]~1_combout\ : std_logic;
SIGNAL \Tick:usv[6]~q\ : std_logic;
SIGNAL \Tick:usv[6]~2\ : std_logic;
SIGNAL \Tick:usv[7]~1_combout\ : std_logic;
SIGNAL \Tick:usv[7]~q\ : std_logic;
SIGNAL \DAT1|Mux6~0_combout\ : std_logic;
SIGNAL \DAT1|Mux6~1_combout\ : std_logic;
SIGNAL \DAT1|Mux5~0_combout\ : std_logic;
SIGNAL \DAT1|Mux5~1_combout\ : std_logic;
SIGNAL \DAT1|Mux4~0_combout\ : std_logic;
SIGNAL \DAT1|Mux4~1_combout\ : std_logic;
SIGNAL \DAT1|Mux3~0_combout\ : std_logic;
SIGNAL \DAT1|Mux3~1_combout\ : std_logic;
SIGNAL \DAT1|Mux2~0_combout\ : std_logic;
SIGNAL \DAT1|Mux2~1_combout\ : std_logic;
SIGNAL \DAT1|Mux1~0_combout\ : std_logic;
SIGNAL \DAT1|Mux1~1_combout\ : std_logic;
SIGNAL \DAT1|Mux0~0_combout\ : std_logic;
SIGNAL \DAT1|Mux0~1_combout\ : std_logic;
SIGNAL \Tick:usv[7]~2\ : std_logic;
SIGNAL \Tick:usv[8]~1_combout\ : std_logic;
SIGNAL \Tick:usv[8]~q\ : std_logic;
SIGNAL \Tick:usv[8]~2\ : std_logic;
SIGNAL \Tick:usv[9]~1_combout\ : std_logic;
SIGNAL \Tick:usv[9]~q\ : std_logic;
SIGNAL \Tick:usv[9]~2\ : std_logic;
SIGNAL \Tick:usv[10]~1_combout\ : std_logic;
SIGNAL \Tick:usv[10]~q\ : std_logic;
SIGNAL \Tick:usv[10]~2\ : std_logic;
SIGNAL \Tick:usv[11]~1_combout\ : std_logic;
SIGNAL \Tick:usv[11]~q\ : std_logic;
SIGNAL \DAT2|Mux6~0_combout\ : std_logic;
SIGNAL \DAT2|Mux6~1_combout\ : std_logic;
SIGNAL \DAT2|Mux5~0_combout\ : std_logic;
SIGNAL \DAT2|Mux5~1_combout\ : std_logic;
SIGNAL \DAT2|Mux4~0_combout\ : std_logic;
SIGNAL \DAT2|Mux4~1_combout\ : std_logic;
SIGNAL \DAT2|Mux3~0_combout\ : std_logic;
SIGNAL \DAT2|Mux3~1_combout\ : std_logic;
SIGNAL \DAT2|Mux2~0_combout\ : std_logic;
SIGNAL \DAT2|Mux2~1_combout\ : std_logic;
SIGNAL \DAT2|Mux1~0_combout\ : std_logic;
SIGNAL \DAT2|Mux1~1_combout\ : std_logic;
SIGNAL \DAT2|Mux0~0_combout\ : std_logic;
SIGNAL \DAT2|Mux0~1_combout\ : std_logic;
SIGNAL \Tick:usv[11]~2\ : std_logic;
SIGNAL \Tick:usv[12]~1_combout\ : std_logic;
SIGNAL \Tick:usv[12]~q\ : std_logic;
SIGNAL \Tick:usv[12]~2\ : std_logic;
SIGNAL \Tick:usv[13]~1_combout\ : std_logic;
SIGNAL \Tick:usv[13]~q\ : std_logic;
SIGNAL \Tick:usv[13]~2\ : std_logic;
SIGNAL \Tick:usv[14]~1_combout\ : std_logic;
SIGNAL \Tick:usv[14]~q\ : std_logic;
SIGNAL \Tick:usv[14]~2\ : std_logic;
SIGNAL \Tick:usv[15]~1_combout\ : std_logic;
SIGNAL \Tick:usv[15]~q\ : std_logic;
SIGNAL \DAT3|Mux6~0_combout\ : std_logic;
SIGNAL \DAT3|Mux6~1_combout\ : std_logic;
SIGNAL \DAT3|Mux5~0_combout\ : std_logic;
SIGNAL \DAT3|Mux5~1_combout\ : std_logic;
SIGNAL \DAT3|Mux4~0_combout\ : std_logic;
SIGNAL \DAT3|Mux4~1_combout\ : std_logic;
SIGNAL \DAT3|Mux3~0_combout\ : std_logic;
SIGNAL \DAT3|Mux3~1_combout\ : std_logic;
SIGNAL \DAT3|Mux2~0_combout\ : std_logic;
SIGNAL \DAT3|Mux2~1_combout\ : std_logic;
SIGNAL \DAT3|Mux1~0_combout\ : std_logic;
SIGNAL \DAT3|Mux1~1_combout\ : std_logic;
SIGNAL \DAT3|Mux0~0_combout\ : std_logic;
SIGNAL \DAT3|Mux0~1_combout\ : std_logic;
SIGNAL \MCLK|ALT_INV_s_qe~q\ : std_logic;
SIGNAL \DAT3|ALT_INV_Mux0~1_combout\ : std_logic;
SIGNAL \DAT2|ALT_INV_Mux0~1_combout\ : std_logic;
SIGNAL \DAT1|ALT_INV_Mux0~1_combout\ : std_logic;
SIGNAL \DAT0|ALT_INV_Mux0~1_combout\ : std_logic;

BEGIN

ww_CLK <= CLK;
ww_RST <= RST;
Error <= ww_Error;
D0 <= ww_D0;
D1 <= ww_D1;
D2 <= ww_D2;
D3 <= ww_D3;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLK~input_o\);

\MCLK|Buff|s_q~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \MCLK|Buff|s_q~combout\);
\MCLK|ALT_INV_s_qe~q\ <= NOT \MCLK|s_qe~q\;
\DAT3|ALT_INV_Mux0~1_combout\ <= NOT \DAT3|Mux0~1_combout\;
\DAT2|ALT_INV_Mux0~1_combout\ <= NOT \DAT2|Mux0~1_combout\;
\DAT1|ALT_INV_Mux0~1_combout\ <= NOT \DAT1|Mux0~1_combout\;
\DAT0|ALT_INV_Mux0~1_combout\ <= NOT \DAT0|Mux0~1_combout\;

-- Location: IOOBUF_X26_Y0_N9
\Error~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \Error~output_o\);

-- Location: IOOBUF_X33_Y14_N2
\D0[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|Mux6~1_combout\,
	devoe => ww_devoe,
	o => \D0[0]~output_o\);

-- Location: IOOBUF_X33_Y15_N2
\D0[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|Mux5~1_combout\,
	devoe => ww_devoe,
	o => \D0[1]~output_o\);

-- Location: IOOBUF_X33_Y15_N9
\D0[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|Mux4~1_combout\,
	devoe => ww_devoe,
	o => \D0[2]~output_o\);

-- Location: IOOBUF_X33_Y14_N9
\D0[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|Mux3~1_combout\,
	devoe => ww_devoe,
	o => \D0[3]~output_o\);

-- Location: IOOBUF_X33_Y12_N2
\D0[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|Mux2~1_combout\,
	devoe => ww_devoe,
	o => \D0[4]~output_o\);

-- Location: IOOBUF_X33_Y12_N9
\D0[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|Mux1~1_combout\,
	devoe => ww_devoe,
	o => \D0[5]~output_o\);

-- Location: IOOBUF_X33_Y10_N9
\D0[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|ALT_INV_Mux0~1_combout\,
	devoe => ww_devoe,
	o => \D0[6]~output_o\);

-- Location: IOOBUF_X31_Y0_N9
\D1[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|Mux6~1_combout\,
	devoe => ww_devoe,
	o => \D1[0]~output_o\);

-- Location: IOOBUF_X31_Y0_N2
\D1[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|Mux5~1_combout\,
	devoe => ww_devoe,
	o => \D1[1]~output_o\);

-- Location: IOOBUF_X29_Y0_N2
\D1[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|Mux4~1_combout\,
	devoe => ww_devoe,
	o => \D1[2]~output_o\);

-- Location: IOOBUF_X29_Y0_N9
\D1[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|Mux3~1_combout\,
	devoe => ww_devoe,
	o => \D1[3]~output_o\);

-- Location: IOOBUF_X26_Y0_N2
\D1[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|Mux2~1_combout\,
	devoe => ww_devoe,
	o => \D1[4]~output_o\);

-- Location: IOOBUF_X33_Y11_N9
\D1[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|Mux1~1_combout\,
	devoe => ww_devoe,
	o => \D1[5]~output_o\);

-- Location: IOOBUF_X33_Y11_N2
\D1[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|ALT_INV_Mux0~1_combout\,
	devoe => ww_devoe,
	o => \D1[6]~output_o\);

-- Location: IOOBUF_X31_Y31_N2
\D2[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|Mux6~1_combout\,
	devoe => ww_devoe,
	o => \D2[0]~output_o\);

-- Location: IOOBUF_X29_Y31_N2
\D2[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|Mux5~1_combout\,
	devoe => ww_devoe,
	o => \D2[1]~output_o\);

-- Location: IOOBUF_X31_Y31_N9
\D2[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|Mux4~1_combout\,
	devoe => ww_devoe,
	o => \D2[2]~output_o\);

-- Location: IOOBUF_X29_Y31_N9
\D2[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|Mux3~1_combout\,
	devoe => ww_devoe,
	o => \D2[3]~output_o\);

-- Location: IOOBUF_X33_Y27_N9
\D2[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|Mux2~1_combout\,
	devoe => ww_devoe,
	o => \D2[4]~output_o\);

-- Location: IOOBUF_X33_Y27_N2
\D2[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|Mux1~1_combout\,
	devoe => ww_devoe,
	o => \D2[5]~output_o\);

-- Location: IOOBUF_X33_Y28_N9
\D2[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|ALT_INV_Mux0~1_combout\,
	devoe => ww_devoe,
	o => \D2[6]~output_o\);

-- Location: IOOBUF_X33_Y24_N9
\D3[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|Mux6~1_combout\,
	devoe => ww_devoe,
	o => \D3[0]~output_o\);

-- Location: IOOBUF_X33_Y22_N9
\D3[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|Mux5~1_combout\,
	devoe => ww_devoe,
	o => \D3[1]~output_o\);

-- Location: IOOBUF_X33_Y22_N2
\D3[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|Mux4~1_combout\,
	devoe => ww_devoe,
	o => \D3[2]~output_o\);

-- Location: IOOBUF_X33_Y24_N2
\D3[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|Mux3~1_combout\,
	devoe => ww_devoe,
	o => \D3[3]~output_o\);

-- Location: IOOBUF_X33_Y25_N9
\D3[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|Mux2~1_combout\,
	devoe => ww_devoe,
	o => \D3[4]~output_o\);

-- Location: IOOBUF_X33_Y28_N2
\D3[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|Mux1~1_combout\,
	devoe => ww_devoe,
	o => \D3[5]~output_o\);

-- Location: IOOBUF_X33_Y25_N2
\D3[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|ALT_INV_Mux0~1_combout\,
	devoe => ww_devoe,
	o => \D3[6]~output_o\);

-- Location: IOIBUF_X16_Y0_N15
\CLK~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: CLKCTRL_G17
\CLK~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLK~inputclkctrl_outclk\);

-- Location: LCCOMB_X16_Y1_N14
\MCLK|Count|Add0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~0_combout\ = (\MCLK|Count|Count:c[0]~q\ & (\MCLK|Count|Count:c[1]~q\ $ (VCC))) # (!\MCLK|Count|Count:c[0]~q\ & (\MCLK|Count|Count:c[1]~q\ & VCC))
-- \MCLK|Count|Add0~1\ = CARRY((\MCLK|Count|Count:c[0]~q\ & \MCLK|Count|Count:c[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[0]~q\,
	datab => \MCLK|Count|Count:c[1]~q\,
	datad => VCC,
	combout => \MCLK|Count|Add0~0_combout\,
	cout => \MCLK|Count|Add0~1\);

-- Location: FF_X16_Y1_N15
\MCLK|Count|Count:c[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Add0~0_combout\,
	clrn => \MCLK|ALT_INV_s_qe~q\,
	ena => \MCLK|Count|Equal0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[1]~q\);

-- Location: LCCOMB_X16_Y1_N10
\MCLK|Equal0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \MCLK|Equal0~0_combout\ = (!\MCLK|Count|Count:c[2]~q\ & (!\MCLK|Count|Count:c[0]~q\ & !\MCLK|Count|Count:c[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[2]~q\,
	datac => \MCLK|Count|Count:c[0]~q\,
	datad => \MCLK|Count|Count:c[3]~q\,
	combout => \MCLK|Equal0~0_combout\);

-- Location: LCCOMB_X16_Y1_N8
\MCLK|Count|Equal0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \MCLK|Count|Equal0~0_combout\ = ((\MCLK|Count|Count:c[4]~q\) # ((\MCLK|Count|Count:c[1]~q\) # (!\MCLK|Equal0~0_combout\))) # (!\MCLK|Count|Count:c[5]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[5]~q\,
	datab => \MCLK|Count|Count:c[4]~q\,
	datac => \MCLK|Count|Count:c[1]~q\,
	datad => \MCLK|Equal0~0_combout\,
	combout => \MCLK|Count|Equal0~0_combout\);

-- Location: LCCOMB_X16_Y1_N4
\MCLK|Count|Count:c[0]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[0]~0_combout\ = (!\MCLK|Count|Count:c[0]~q\ & \MCLK|Count|Equal0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCLK|Count|Count:c[0]~q\,
	datad => \MCLK|Count|Equal0~0_combout\,
	combout => \MCLK|Count|Count:c[0]~0_combout\);

-- Location: FF_X16_Y1_N5
\MCLK|Count|Count:c[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[0]~0_combout\,
	clrn => \MCLK|ALT_INV_s_qe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[0]~q\);

-- Location: LCCOMB_X16_Y1_N16
\MCLK|Count|Add0~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~2_combout\ = (\MCLK|Count|Count:c[2]~q\ & (!\MCLK|Count|Add0~1\)) # (!\MCLK|Count|Count:c[2]~q\ & ((\MCLK|Count|Add0~1\) # (GND)))
-- \MCLK|Count|Add0~3\ = CARRY((!\MCLK|Count|Add0~1\) # (!\MCLK|Count|Count:c[2]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[2]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~1\,
	combout => \MCLK|Count|Add0~2_combout\,
	cout => \MCLK|Count|Add0~3\);

-- Location: FF_X16_Y1_N17
\MCLK|Count|Count:c[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Add0~2_combout\,
	clrn => \MCLK|ALT_INV_s_qe~q\,
	ena => \MCLK|Count|Equal0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[2]~q\);

-- Location: LCCOMB_X16_Y1_N18
\MCLK|Count|Add0~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~4_combout\ = (\MCLK|Count|Count:c[3]~q\ & (\MCLK|Count|Add0~3\ $ (GND))) # (!\MCLK|Count|Count:c[3]~q\ & (!\MCLK|Count|Add0~3\ & VCC))
-- \MCLK|Count|Add0~5\ = CARRY((\MCLK|Count|Count:c[3]~q\ & !\MCLK|Count|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[3]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~3\,
	combout => \MCLK|Count|Add0~4_combout\,
	cout => \MCLK|Count|Add0~5\);

-- Location: FF_X16_Y1_N19
\MCLK|Count|Count:c[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Add0~4_combout\,
	clrn => \MCLK|ALT_INV_s_qe~q\,
	ena => \MCLK|Count|Equal0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[3]~q\);

-- Location: LCCOMB_X16_Y1_N20
\MCLK|Count|Add0~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~6_combout\ = (\MCLK|Count|Count:c[4]~q\ & (!\MCLK|Count|Add0~5\)) # (!\MCLK|Count|Count:c[4]~q\ & ((\MCLK|Count|Add0~5\) # (GND)))
-- \MCLK|Count|Add0~7\ = CARRY((!\MCLK|Count|Add0~5\) # (!\MCLK|Count|Count:c[4]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[4]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~5\,
	combout => \MCLK|Count|Add0~6_combout\,
	cout => \MCLK|Count|Add0~7\);

-- Location: FF_X16_Y1_N21
\MCLK|Count|Count:c[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Add0~6_combout\,
	clrn => \MCLK|ALT_INV_s_qe~q\,
	ena => \MCLK|Count|Equal0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[4]~q\);

-- Location: LCCOMB_X16_Y1_N22
\MCLK|Count|Add0~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~8_combout\ = \MCLK|Count|Count:c[5]~q\ $ (!\MCLK|Count|Add0~7\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[5]~q\,
	cin => \MCLK|Count|Add0~7\,
	combout => \MCLK|Count|Add0~8_combout\);

-- Location: FF_X16_Y1_N23
\MCLK|Count|Count:c[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Add0~8_combout\,
	clrn => \MCLK|ALT_INV_s_qe~q\,
	ena => \MCLK|Count|Equal0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[5]~q\);

-- Location: LCCOMB_X16_Y1_N26
\MCLK|Equal0~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \MCLK|Equal0~1_combout\ = (!\MCLK|Count|Count:c[5]~q\ & (\MCLK|Count|Count:c[4]~q\ & (\MCLK|Count|Count:c[1]~q\ & \MCLK|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[5]~q\,
	datab => \MCLK|Count|Count:c[4]~q\,
	datac => \MCLK|Count|Count:c[1]~q\,
	datad => \MCLK|Equal0~0_combout\,
	combout => \MCLK|Equal0~1_combout\);

-- Location: FF_X16_Y1_N27
\MCLK|s_qe\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Equal0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|s_qe~q\);

-- Location: LCCOMB_X16_Y1_N6
\MCLK|Buff|qn~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \MCLK|Buff|qn~0_combout\ = (\MCLK|s_qe~q\ & (!\MCLK|Buff|s_q~combout\)) # (!\MCLK|s_qe~q\ & ((\MCLK|Buff|qn~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Buff|s_q~combout\,
	datac => \MCLK|s_qe~q\,
	datad => \MCLK|Buff|qn~q\,
	combout => \MCLK|Buff|qn~0_combout\);

-- Location: LCCOMB_X16_Y1_N24
\MCLK|Buff|qn~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \MCLK|Buff|qn~feeder_combout\ = \MCLK|Buff|qn~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Buff|qn~0_combout\,
	combout => \MCLK|Buff|qn~feeder_combout\);

-- Location: FF_X16_Y1_N25
\MCLK|Buff|qn\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Buff|qn~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Buff|qn~q\);

-- Location: LCCOMB_X16_Y1_N30
\MCLK|Buff|s_q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \MCLK|Buff|s_q~combout\ = (\MCLK|s_qe~q\ & ((\MCLK|Buff|qn~q\))) # (!\MCLK|s_qe~q\ & (\MCLK|Buff|s_q~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Buff|s_q~combout\,
	datac => \MCLK|s_qe~q\,
	datad => \MCLK|Buff|qn~q\,
	combout => \MCLK|Buff|s_q~combout\);

-- Location: CLKCTRL_G15
\MCLK|Buff|s_q~clkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \MCLK|Buff|s_q~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \MCLK|Buff|s_q~clkctrl_outclk\);

-- Location: LCCOMB_X32_Y19_N0
\Tick:usv[0]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[0]~0_combout\ = !\Tick:usv[0]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Tick:usv[0]~q\,
	combout => \Tick:usv[0]~0_combout\);

-- Location: FF_X32_Y19_N1
\Tick:usv[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[0]~q\);

-- Location: LCCOMB_X32_Y19_N2
\Tick:usv[1]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[1]~1_combout\ = (\Tick:usv[1]~q\ & (\Tick:usv[0]~q\ $ (VCC))) # (!\Tick:usv[1]~q\ & (\Tick:usv[0]~q\ & VCC))
-- \Tick:usv[1]~2\ = CARRY((\Tick:usv[1]~q\ & \Tick:usv[0]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[1]~q\,
	datab => \Tick:usv[0]~q\,
	datad => VCC,
	combout => \Tick:usv[1]~1_combout\,
	cout => \Tick:usv[1]~2\);

-- Location: FF_X32_Y19_N3
\Tick:usv[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[1]~q\);

-- Location: LCCOMB_X32_Y19_N4
\Tick:usv[2]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[2]~1_combout\ = (\Tick:usv[2]~q\ & (!\Tick:usv[1]~2\)) # (!\Tick:usv[2]~q\ & ((\Tick:usv[1]~2\) # (GND)))
-- \Tick:usv[2]~2\ = CARRY((!\Tick:usv[1]~2\) # (!\Tick:usv[2]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Tick:usv[2]~q\,
	datad => VCC,
	cin => \Tick:usv[1]~2\,
	combout => \Tick:usv[2]~1_combout\,
	cout => \Tick:usv[2]~2\);

-- Location: FF_X32_Y19_N5
\Tick:usv[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[2]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[2]~q\);

-- Location: LCCOMB_X32_Y19_N6
\Tick:usv[3]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[3]~1_combout\ = (\Tick:usv[3]~q\ & (\Tick:usv[2]~2\ $ (GND))) # (!\Tick:usv[3]~q\ & (!\Tick:usv[2]~2\ & VCC))
-- \Tick:usv[3]~2\ = CARRY((\Tick:usv[3]~q\ & !\Tick:usv[2]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[3]~q\,
	datad => VCC,
	cin => \Tick:usv[2]~2\,
	combout => \Tick:usv[3]~1_combout\,
	cout => \Tick:usv[3]~2\);

-- Location: FF_X32_Y19_N7
\Tick:usv[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[3]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[3]~q\);

-- Location: LCCOMB_X32_Y15_N28
\DAT0|Mux6~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux6~0_combout\ = (\Tick:usv[2]~q\ & (!\Tick:usv[1]~q\ & (\Tick:usv[0]~q\ $ (!\Tick:usv[3]~q\)))) # (!\Tick:usv[2]~q\ & (\Tick:usv[0]~q\ & (\Tick:usv[1]~q\ $ (!\Tick:usv[3]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[0]~q\,
	datab => \Tick:usv[2]~q\,
	datac => \Tick:usv[1]~q\,
	datad => \Tick:usv[3]~q\,
	combout => \DAT0|Mux6~0_combout\);

-- Location: IOIBUF_X33_Y10_N1
\RST~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_RST,
	o => \RST~input_o\);

-- Location: LCCOMB_X32_Y15_N30
\DAT0|Mux6~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux6~1_combout\ = (\DAT0|Mux6~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT0|Mux6~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT0|Mux6~1_combout\);

-- Location: LCCOMB_X32_Y15_N4
\DAT0|Mux5~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux5~0_combout\ = (\Tick:usv[1]~q\ & ((\Tick:usv[0]~q\ & ((\Tick:usv[3]~q\))) # (!\Tick:usv[0]~q\ & (\Tick:usv[2]~q\)))) # (!\Tick:usv[1]~q\ & (\Tick:usv[2]~q\ & (\Tick:usv[0]~q\ $ (\Tick:usv[3]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[0]~q\,
	datab => \Tick:usv[2]~q\,
	datac => \Tick:usv[1]~q\,
	datad => \Tick:usv[3]~q\,
	combout => \DAT0|Mux5~0_combout\);

-- Location: LCCOMB_X32_Y15_N26
\DAT0|Mux5~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux5~1_combout\ = (!\RST~input_o\ & \DAT0|Mux5~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \RST~input_o\,
	datac => \DAT0|Mux5~0_combout\,
	combout => \DAT0|Mux5~1_combout\);

-- Location: LCCOMB_X32_Y15_N20
\DAT0|Mux4~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux4~0_combout\ = (\Tick:usv[2]~q\ & (\Tick:usv[3]~q\ & ((\Tick:usv[1]~q\) # (!\Tick:usv[0]~q\)))) # (!\Tick:usv[2]~q\ & (!\Tick:usv[0]~q\ & (\Tick:usv[1]~q\ & !\Tick:usv[3]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[0]~q\,
	datab => \Tick:usv[2]~q\,
	datac => \Tick:usv[1]~q\,
	datad => \Tick:usv[3]~q\,
	combout => \DAT0|Mux4~0_combout\);

-- Location: LCCOMB_X32_Y15_N22
\DAT0|Mux4~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux4~1_combout\ = (\DAT0|Mux4~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT0|Mux4~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT0|Mux4~1_combout\);

-- Location: LCCOMB_X32_Y15_N16
\DAT0|Mux3~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux3~0_combout\ = (\Tick:usv[1]~q\ & ((\Tick:usv[0]~q\ & (\Tick:usv[2]~q\)) # (!\Tick:usv[0]~q\ & (!\Tick:usv[2]~q\ & \Tick:usv[3]~q\)))) # (!\Tick:usv[1]~q\ & (!\Tick:usv[3]~q\ & (\Tick:usv[0]~q\ $ (\Tick:usv[2]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000010000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[0]~q\,
	datab => \Tick:usv[2]~q\,
	datac => \Tick:usv[1]~q\,
	datad => \Tick:usv[3]~q\,
	combout => \DAT0|Mux3~0_combout\);

-- Location: LCCOMB_X32_Y15_N18
\DAT0|Mux3~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux3~1_combout\ = (\DAT0|Mux3~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT0|Mux3~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT0|Mux3~1_combout\);

-- Location: LCCOMB_X32_Y15_N12
\DAT0|Mux2~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux2~0_combout\ = (\Tick:usv[1]~q\ & (\Tick:usv[0]~q\ & ((!\Tick:usv[3]~q\)))) # (!\Tick:usv[1]~q\ & ((\Tick:usv[2]~q\ & ((!\Tick:usv[3]~q\))) # (!\Tick:usv[2]~q\ & (\Tick:usv[0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001010101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[0]~q\,
	datab => \Tick:usv[2]~q\,
	datac => \Tick:usv[1]~q\,
	datad => \Tick:usv[3]~q\,
	combout => \DAT0|Mux2~0_combout\);

-- Location: LCCOMB_X32_Y15_N10
\DAT0|Mux2~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux2~1_combout\ = (\DAT0|Mux2~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DAT0|Mux2~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT0|Mux2~1_combout\);

-- Location: LCCOMB_X32_Y15_N8
\DAT0|Mux1~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux1~0_combout\ = (\Tick:usv[0]~q\ & (\Tick:usv[3]~q\ $ (((\Tick:usv[1]~q\) # (!\Tick:usv[2]~q\))))) # (!\Tick:usv[0]~q\ & (!\Tick:usv[2]~q\ & (\Tick:usv[1]~q\ & !\Tick:usv[3]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100010110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[0]~q\,
	datab => \Tick:usv[2]~q\,
	datac => \Tick:usv[1]~q\,
	datad => \Tick:usv[3]~q\,
	combout => \DAT0|Mux1~0_combout\);

-- Location: LCCOMB_X32_Y15_N14
\DAT0|Mux1~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux1~1_combout\ = (!\RST~input_o\ & \DAT0|Mux1~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \RST~input_o\,
	datac => \DAT0|Mux1~0_combout\,
	combout => \DAT0|Mux1~1_combout\);

-- Location: LCCOMB_X32_Y15_N24
\DAT0|Mux0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux0~0_combout\ = (\Tick:usv[0]~q\ & ((\Tick:usv[3]~q\) # (\Tick:usv[2]~q\ $ (\Tick:usv[1]~q\)))) # (!\Tick:usv[0]~q\ & ((\Tick:usv[1]~q\) # (\Tick:usv[2]~q\ $ (\Tick:usv[3]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101101111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[0]~q\,
	datab => \Tick:usv[2]~q\,
	datac => \Tick:usv[1]~q\,
	datad => \Tick:usv[3]~q\,
	combout => \DAT0|Mux0~0_combout\);

-- Location: LCCOMB_X32_Y15_N6
\DAT0|Mux0~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT0|Mux0~1_combout\ = (\DAT0|Mux0~0_combout\) # (\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT0|Mux0~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT0|Mux0~1_combout\);

-- Location: LCCOMB_X32_Y19_N8
\Tick:usv[4]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[4]~1_combout\ = (\Tick:usv[4]~q\ & (!\Tick:usv[3]~2\)) # (!\Tick:usv[4]~q\ & ((\Tick:usv[3]~2\) # (GND)))
-- \Tick:usv[4]~2\ = CARRY((!\Tick:usv[3]~2\) # (!\Tick:usv[4]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Tick:usv[4]~q\,
	datad => VCC,
	cin => \Tick:usv[3]~2\,
	combout => \Tick:usv[4]~1_combout\,
	cout => \Tick:usv[4]~2\);

-- Location: FF_X32_Y19_N9
\Tick:usv[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[4]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[4]~q\);

-- Location: LCCOMB_X32_Y19_N10
\Tick:usv[5]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[5]~1_combout\ = (\Tick:usv[5]~q\ & (\Tick:usv[4]~2\ $ (GND))) # (!\Tick:usv[5]~q\ & (!\Tick:usv[4]~2\ & VCC))
-- \Tick:usv[5]~2\ = CARRY((\Tick:usv[5]~q\ & !\Tick:usv[4]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[5]~q\,
	datad => VCC,
	cin => \Tick:usv[4]~2\,
	combout => \Tick:usv[5]~1_combout\,
	cout => \Tick:usv[5]~2\);

-- Location: FF_X32_Y19_N11
\Tick:usv[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[5]~q\);

-- Location: LCCOMB_X32_Y19_N12
\Tick:usv[6]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[6]~1_combout\ = (\Tick:usv[6]~q\ & (!\Tick:usv[5]~2\)) # (!\Tick:usv[6]~q\ & ((\Tick:usv[5]~2\) # (GND)))
-- \Tick:usv[6]~2\ = CARRY((!\Tick:usv[5]~2\) # (!\Tick:usv[6]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[6]~q\,
	datad => VCC,
	cin => \Tick:usv[5]~2\,
	combout => \Tick:usv[6]~1_combout\,
	cout => \Tick:usv[6]~2\);

-- Location: FF_X32_Y19_N13
\Tick:usv[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[6]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[6]~q\);

-- Location: LCCOMB_X32_Y19_N14
\Tick:usv[7]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[7]~1_combout\ = (\Tick:usv[7]~q\ & (\Tick:usv[6]~2\ $ (GND))) # (!\Tick:usv[7]~q\ & (!\Tick:usv[6]~2\ & VCC))
-- \Tick:usv[7]~2\ = CARRY((\Tick:usv[7]~q\ & !\Tick:usv[6]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Tick:usv[7]~q\,
	datad => VCC,
	cin => \Tick:usv[6]~2\,
	combout => \Tick:usv[7]~1_combout\,
	cout => \Tick:usv[7]~2\);

-- Location: FF_X32_Y19_N15
\Tick:usv[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[7]~q\);

-- Location: LCCOMB_X32_Y11_N24
\DAT1|Mux6~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux6~0_combout\ = (\Tick:usv[6]~q\ & (!\Tick:usv[5]~q\ & (\Tick:usv[4]~q\ $ (!\Tick:usv[7]~q\)))) # (!\Tick:usv[6]~q\ & (\Tick:usv[4]~q\ & (\Tick:usv[5]~q\ $ (!\Tick:usv[7]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[5]~q\,
	datab => \Tick:usv[6]~q\,
	datac => \Tick:usv[4]~q\,
	datad => \Tick:usv[7]~q\,
	combout => \DAT1|Mux6~0_combout\);

-- Location: LCCOMB_X32_Y11_N6
\DAT1|Mux6~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux6~1_combout\ = (\DAT1|Mux6~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT1|Mux6~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT1|Mux6~1_combout\);

-- Location: LCCOMB_X32_Y11_N4
\DAT1|Mux5~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux5~0_combout\ = (\Tick:usv[5]~q\ & ((\Tick:usv[4]~q\ & ((\Tick:usv[7]~q\))) # (!\Tick:usv[4]~q\ & (\Tick:usv[6]~q\)))) # (!\Tick:usv[5]~q\ & (\Tick:usv[6]~q\ & (\Tick:usv[4]~q\ $ (\Tick:usv[7]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[5]~q\,
	datab => \Tick:usv[6]~q\,
	datac => \Tick:usv[4]~q\,
	datad => \Tick:usv[7]~q\,
	combout => \DAT1|Mux5~0_combout\);

-- Location: LCCOMB_X32_Y11_N10
\DAT1|Mux5~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux5~1_combout\ = (!\RST~input_o\ & \DAT1|Mux5~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \RST~input_o\,
	datac => \DAT1|Mux5~0_combout\,
	combout => \DAT1|Mux5~1_combout\);

-- Location: LCCOMB_X32_Y11_N20
\DAT1|Mux4~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux4~0_combout\ = (\Tick:usv[6]~q\ & (\Tick:usv[7]~q\ & ((\Tick:usv[5]~q\) # (!\Tick:usv[4]~q\)))) # (!\Tick:usv[6]~q\ & (\Tick:usv[5]~q\ & (!\Tick:usv[4]~q\ & !\Tick:usv[7]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[5]~q\,
	datab => \Tick:usv[6]~q\,
	datac => \Tick:usv[4]~q\,
	datad => \Tick:usv[7]~q\,
	combout => \DAT1|Mux4~0_combout\);

-- Location: LCCOMB_X32_Y11_N26
\DAT1|Mux4~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux4~1_combout\ = (\DAT1|Mux4~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT1|Mux4~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT1|Mux4~1_combout\);

-- Location: LCCOMB_X32_Y11_N16
\DAT1|Mux3~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux3~0_combout\ = (\Tick:usv[5]~q\ & ((\Tick:usv[6]~q\ & (\Tick:usv[4]~q\)) # (!\Tick:usv[6]~q\ & (!\Tick:usv[4]~q\ & \Tick:usv[7]~q\)))) # (!\Tick:usv[5]~q\ & (!\Tick:usv[7]~q\ & (\Tick:usv[6]~q\ $ (\Tick:usv[4]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[5]~q\,
	datab => \Tick:usv[6]~q\,
	datac => \Tick:usv[4]~q\,
	datad => \Tick:usv[7]~q\,
	combout => \DAT1|Mux3~0_combout\);

-- Location: LCCOMB_X32_Y11_N22
\DAT1|Mux3~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux3~1_combout\ = (\DAT1|Mux3~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT1|Mux3~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT1|Mux3~1_combout\);

-- Location: LCCOMB_X32_Y11_N28
\DAT1|Mux2~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux2~0_combout\ = (\Tick:usv[5]~q\ & (((\Tick:usv[4]~q\ & !\Tick:usv[7]~q\)))) # (!\Tick:usv[5]~q\ & ((\Tick:usv[6]~q\ & ((!\Tick:usv[7]~q\))) # (!\Tick:usv[6]~q\ & (\Tick:usv[4]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000011110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[5]~q\,
	datab => \Tick:usv[6]~q\,
	datac => \Tick:usv[4]~q\,
	datad => \Tick:usv[7]~q\,
	combout => \DAT1|Mux2~0_combout\);

-- Location: LCCOMB_X32_Y11_N30
\DAT1|Mux2~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux2~1_combout\ = (\DAT1|Mux2~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT1|Mux2~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT1|Mux2~1_combout\);

-- Location: LCCOMB_X32_Y11_N8
\DAT1|Mux1~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux1~0_combout\ = (\Tick:usv[5]~q\ & (!\Tick:usv[7]~q\ & ((\Tick:usv[4]~q\) # (!\Tick:usv[6]~q\)))) # (!\Tick:usv[5]~q\ & (\Tick:usv[4]~q\ & (\Tick:usv[6]~q\ $ (!\Tick:usv[7]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000010110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[5]~q\,
	datab => \Tick:usv[6]~q\,
	datac => \Tick:usv[4]~q\,
	datad => \Tick:usv[7]~q\,
	combout => \DAT1|Mux1~0_combout\);

-- Location: LCCOMB_X32_Y11_N18
\DAT1|Mux1~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux1~1_combout\ = (!\RST~input_o\ & \DAT1|Mux1~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \RST~input_o\,
	datac => \DAT1|Mux1~0_combout\,
	combout => \DAT1|Mux1~1_combout\);

-- Location: LCCOMB_X32_Y11_N12
\DAT1|Mux0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux0~0_combout\ = (\Tick:usv[4]~q\ & ((\Tick:usv[7]~q\) # (\Tick:usv[5]~q\ $ (\Tick:usv[6]~q\)))) # (!\Tick:usv[4]~q\ & ((\Tick:usv[5]~q\) # (\Tick:usv[6]~q\ $ (\Tick:usv[7]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101101101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[5]~q\,
	datab => \Tick:usv[6]~q\,
	datac => \Tick:usv[4]~q\,
	datad => \Tick:usv[7]~q\,
	combout => \DAT1|Mux0~0_combout\);

-- Location: LCCOMB_X32_Y11_N14
\DAT1|Mux0~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT1|Mux0~1_combout\ = (\DAT1|Mux0~0_combout\) # (\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DAT1|Mux0~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT1|Mux0~1_combout\);

-- Location: LCCOMB_X32_Y19_N16
\Tick:usv[8]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[8]~1_combout\ = (\Tick:usv[8]~q\ & (!\Tick:usv[7]~2\)) # (!\Tick:usv[8]~q\ & ((\Tick:usv[7]~2\) # (GND)))
-- \Tick:usv[8]~2\ = CARRY((!\Tick:usv[7]~2\) # (!\Tick:usv[8]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Tick:usv[8]~q\,
	datad => VCC,
	cin => \Tick:usv[7]~2\,
	combout => \Tick:usv[8]~1_combout\,
	cout => \Tick:usv[8]~2\);

-- Location: FF_X32_Y19_N17
\Tick:usv[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[8]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[8]~q\);

-- Location: LCCOMB_X32_Y19_N18
\Tick:usv[9]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[9]~1_combout\ = (\Tick:usv[9]~q\ & (\Tick:usv[8]~2\ $ (GND))) # (!\Tick:usv[9]~q\ & (!\Tick:usv[8]~2\ & VCC))
-- \Tick:usv[9]~2\ = CARRY((\Tick:usv[9]~q\ & !\Tick:usv[8]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Tick:usv[9]~q\,
	datad => VCC,
	cin => \Tick:usv[8]~2\,
	combout => \Tick:usv[9]~1_combout\,
	cout => \Tick:usv[9]~2\);

-- Location: FF_X32_Y19_N19
\Tick:usv[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[9]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[9]~q\);

-- Location: LCCOMB_X32_Y19_N20
\Tick:usv[10]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[10]~1_combout\ = (\Tick:usv[10]~q\ & (!\Tick:usv[9]~2\)) # (!\Tick:usv[10]~q\ & ((\Tick:usv[9]~2\) # (GND)))
-- \Tick:usv[10]~2\ = CARRY((!\Tick:usv[9]~2\) # (!\Tick:usv[10]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Tick:usv[10]~q\,
	datad => VCC,
	cin => \Tick:usv[9]~2\,
	combout => \Tick:usv[10]~1_combout\,
	cout => \Tick:usv[10]~2\);

-- Location: FF_X32_Y19_N21
\Tick:usv[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[10]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[10]~q\);

-- Location: LCCOMB_X32_Y19_N22
\Tick:usv[11]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[11]~1_combout\ = (\Tick:usv[11]~q\ & (\Tick:usv[10]~2\ $ (GND))) # (!\Tick:usv[11]~q\ & (!\Tick:usv[10]~2\ & VCC))
-- \Tick:usv[11]~2\ = CARRY((\Tick:usv[11]~q\ & !\Tick:usv[10]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[11]~q\,
	datad => VCC,
	cin => \Tick:usv[10]~2\,
	combout => \Tick:usv[11]~1_combout\,
	cout => \Tick:usv[11]~2\);

-- Location: FF_X32_Y19_N23
\Tick:usv[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[11]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[11]~q\);

-- Location: LCCOMB_X32_Y27_N12
\DAT2|Mux6~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux6~0_combout\ = (\Tick:usv[11]~q\ & (\Tick:usv[8]~q\ & (\Tick:usv[9]~q\ $ (\Tick:usv[10]~q\)))) # (!\Tick:usv[11]~q\ & (!\Tick:usv[9]~q\ & (\Tick:usv[8]~q\ $ (\Tick:usv[10]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000110000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[8]~q\,
	datab => \Tick:usv[9]~q\,
	datac => \Tick:usv[11]~q\,
	datad => \Tick:usv[10]~q\,
	combout => \DAT2|Mux6~0_combout\);

-- Location: LCCOMB_X32_Y27_N26
\DAT2|Mux6~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux6~1_combout\ = (\DAT2|Mux6~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DAT2|Mux6~0_combout\,
	datad => \RST~input_o\,
	combout => \DAT2|Mux6~1_combout\);

-- Location: LCCOMB_X32_Y27_N4
\DAT2|Mux5~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux5~0_combout\ = (\Tick:usv[9]~q\ & ((\Tick:usv[8]~q\ & (\Tick:usv[11]~q\)) # (!\Tick:usv[8]~q\ & ((\Tick:usv[10]~q\))))) # (!\Tick:usv[9]~q\ & (\Tick:usv[10]~q\ & (\Tick:usv[8]~q\ $ (\Tick:usv[11]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101011010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[8]~q\,
	datab => \Tick:usv[9]~q\,
	datac => \Tick:usv[11]~q\,
	datad => \Tick:usv[10]~q\,
	combout => \DAT2|Mux5~0_combout\);

-- Location: LCCOMB_X32_Y27_N2
\DAT2|Mux5~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux5~1_combout\ = (\DAT2|Mux5~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DAT2|Mux5~0_combout\,
	datad => \RST~input_o\,
	combout => \DAT2|Mux5~1_combout\);

-- Location: LCCOMB_X32_Y27_N8
\DAT2|Mux4~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux4~0_combout\ = (\Tick:usv[11]~q\ & (\Tick:usv[10]~q\ & ((\Tick:usv[9]~q\) # (!\Tick:usv[8]~q\)))) # (!\Tick:usv[11]~q\ & (!\Tick:usv[8]~q\ & (\Tick:usv[9]~q\ & !\Tick:usv[10]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[8]~q\,
	datab => \Tick:usv[9]~q\,
	datac => \Tick:usv[11]~q\,
	datad => \Tick:usv[10]~q\,
	combout => \DAT2|Mux4~0_combout\);

-- Location: LCCOMB_X32_Y27_N10
\DAT2|Mux4~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux4~1_combout\ = (\DAT2|Mux4~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DAT2|Mux4~0_combout\,
	datad => \RST~input_o\,
	combout => \DAT2|Mux4~1_combout\);

-- Location: LCCOMB_X32_Y27_N20
\DAT2|Mux3~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux3~0_combout\ = (\Tick:usv[9]~q\ & ((\Tick:usv[8]~q\ & ((\Tick:usv[10]~q\))) # (!\Tick:usv[8]~q\ & (\Tick:usv[11]~q\ & !\Tick:usv[10]~q\)))) # (!\Tick:usv[9]~q\ & (!\Tick:usv[11]~q\ & (\Tick:usv[8]~q\ $ (\Tick:usv[10]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100101000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[8]~q\,
	datab => \Tick:usv[9]~q\,
	datac => \Tick:usv[11]~q\,
	datad => \Tick:usv[10]~q\,
	combout => \DAT2|Mux3~0_combout\);

-- Location: LCCOMB_X32_Y27_N30
\DAT2|Mux3~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux3~1_combout\ = (\DAT2|Mux3~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT2|Mux3~0_combout\,
	datad => \RST~input_o\,
	combout => \DAT2|Mux3~1_combout\);

-- Location: LCCOMB_X32_Y27_N16
\DAT2|Mux2~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux2~0_combout\ = (\Tick:usv[9]~q\ & (\Tick:usv[8]~q\ & (!\Tick:usv[11]~q\))) # (!\Tick:usv[9]~q\ & ((\Tick:usv[10]~q\ & ((!\Tick:usv[11]~q\))) # (!\Tick:usv[10]~q\ & (\Tick:usv[8]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101100101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[8]~q\,
	datab => \Tick:usv[9]~q\,
	datac => \Tick:usv[11]~q\,
	datad => \Tick:usv[10]~q\,
	combout => \DAT2|Mux2~0_combout\);

-- Location: LCCOMB_X32_Y27_N18
\DAT2|Mux2~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux2~1_combout\ = (\DAT2|Mux2~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT2|Mux2~0_combout\,
	datad => \RST~input_o\,
	combout => \DAT2|Mux2~1_combout\);

-- Location: LCCOMB_X32_Y27_N24
\DAT2|Mux1~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux1~0_combout\ = (\Tick:usv[8]~q\ & (\Tick:usv[11]~q\ $ (((\Tick:usv[9]~q\) # (!\Tick:usv[10]~q\))))) # (!\Tick:usv[8]~q\ & (\Tick:usv[9]~q\ & (!\Tick:usv[11]~q\ & !\Tick:usv[10]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[8]~q\,
	datab => \Tick:usv[9]~q\,
	datac => \Tick:usv[11]~q\,
	datad => \Tick:usv[10]~q\,
	combout => \DAT2|Mux1~0_combout\);

-- Location: LCCOMB_X32_Y27_N14
\DAT2|Mux1~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux1~1_combout\ = (\DAT2|Mux1~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT2|Mux1~0_combout\,
	datad => \RST~input_o\,
	combout => \DAT2|Mux1~1_combout\);

-- Location: LCCOMB_X32_Y27_N28
\DAT2|Mux0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux0~0_combout\ = (\Tick:usv[8]~q\ & ((\Tick:usv[11]~q\) # (\Tick:usv[9]~q\ $ (\Tick:usv[10]~q\)))) # (!\Tick:usv[8]~q\ & ((\Tick:usv[9]~q\) # (\Tick:usv[11]~q\ $ (\Tick:usv[10]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011111111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[8]~q\,
	datab => \Tick:usv[9]~q\,
	datac => \Tick:usv[11]~q\,
	datad => \Tick:usv[10]~q\,
	combout => \DAT2|Mux0~0_combout\);

-- Location: LCCOMB_X32_Y27_N22
\DAT2|Mux0~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT2|Mux0~1_combout\ = (\DAT2|Mux0~0_combout\) # (\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT2|Mux0~0_combout\,
	datad => \RST~input_o\,
	combout => \DAT2|Mux0~1_combout\);

-- Location: LCCOMB_X32_Y19_N24
\Tick:usv[12]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[12]~1_combout\ = (\Tick:usv[12]~q\ & (!\Tick:usv[11]~2\)) # (!\Tick:usv[12]~q\ & ((\Tick:usv[11]~2\) # (GND)))
-- \Tick:usv[12]~2\ = CARRY((!\Tick:usv[11]~2\) # (!\Tick:usv[12]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Tick:usv[12]~q\,
	datad => VCC,
	cin => \Tick:usv[11]~2\,
	combout => \Tick:usv[12]~1_combout\,
	cout => \Tick:usv[12]~2\);

-- Location: FF_X32_Y19_N25
\Tick:usv[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[12]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[12]~q\);

-- Location: LCCOMB_X32_Y19_N26
\Tick:usv[13]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[13]~1_combout\ = (\Tick:usv[13]~q\ & (\Tick:usv[12]~2\ $ (GND))) # (!\Tick:usv[13]~q\ & (!\Tick:usv[12]~2\ & VCC))
-- \Tick:usv[13]~2\ = CARRY((\Tick:usv[13]~q\ & !\Tick:usv[12]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[13]~q\,
	datad => VCC,
	cin => \Tick:usv[12]~2\,
	combout => \Tick:usv[13]~1_combout\,
	cout => \Tick:usv[13]~2\);

-- Location: FF_X32_Y19_N27
\Tick:usv[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[13]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[13]~q\);

-- Location: LCCOMB_X32_Y19_N28
\Tick:usv[14]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[14]~1_combout\ = (\Tick:usv[14]~q\ & (!\Tick:usv[13]~2\)) # (!\Tick:usv[14]~q\ & ((\Tick:usv[13]~2\) # (GND)))
-- \Tick:usv[14]~2\ = CARRY((!\Tick:usv[13]~2\) # (!\Tick:usv[14]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Tick:usv[14]~q\,
	datad => VCC,
	cin => \Tick:usv[13]~2\,
	combout => \Tick:usv[14]~1_combout\,
	cout => \Tick:usv[14]~2\);

-- Location: FF_X32_Y19_N29
\Tick:usv[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[14]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[14]~q\);

-- Location: LCCOMB_X32_Y19_N30
\Tick:usv[15]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Tick:usv[15]~1_combout\ = \Tick:usv[15]~q\ $ (!\Tick:usv[14]~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[15]~q\,
	cin => \Tick:usv[14]~2\,
	combout => \Tick:usv[15]~1_combout\);

-- Location: FF_X32_Y19_N31
\Tick:usv[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \Tick:usv[15]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Tick:usv[15]~q\);

-- Location: LCCOMB_X32_Y22_N24
\DAT3|Mux6~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux6~0_combout\ = (\Tick:usv[15]~q\ & (\Tick:usv[12]~q\ & (\Tick:usv[13]~q\ $ (\Tick:usv[14]~q\)))) # (!\Tick:usv[15]~q\ & (!\Tick:usv[13]~q\ & (\Tick:usv[14]~q\ $ (\Tick:usv[12]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[15]~q\,
	datab => \Tick:usv[13]~q\,
	datac => \Tick:usv[14]~q\,
	datad => \Tick:usv[12]~q\,
	combout => \DAT3|Mux6~0_combout\);

-- Location: LCCOMB_X32_Y22_N18
\DAT3|Mux6~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux6~1_combout\ = (\DAT3|Mux6~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT3|Mux6~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT3|Mux6~1_combout\);

-- Location: LCCOMB_X32_Y22_N20
\DAT3|Mux5~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux5~0_combout\ = (\Tick:usv[15]~q\ & ((\Tick:usv[12]~q\ & (\Tick:usv[13]~q\)) # (!\Tick:usv[12]~q\ & ((\Tick:usv[14]~q\))))) # (!\Tick:usv[15]~q\ & (\Tick:usv[14]~q\ & (\Tick:usv[13]~q\ $ (\Tick:usv[12]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[15]~q\,
	datab => \Tick:usv[13]~q\,
	datac => \Tick:usv[14]~q\,
	datad => \Tick:usv[12]~q\,
	combout => \DAT3|Mux5~0_combout\);

-- Location: LCCOMB_X32_Y22_N6
\DAT3|Mux5~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux5~1_combout\ = (\DAT3|Mux5~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT3|Mux5~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT3|Mux5~1_combout\);

-- Location: LCCOMB_X32_Y22_N16
\DAT3|Mux4~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux4~0_combout\ = (\Tick:usv[15]~q\ & (\Tick:usv[14]~q\ & ((\Tick:usv[13]~q\) # (!\Tick:usv[12]~q\)))) # (!\Tick:usv[15]~q\ & (\Tick:usv[13]~q\ & (!\Tick:usv[14]~q\ & !\Tick:usv[12]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[15]~q\,
	datab => \Tick:usv[13]~q\,
	datac => \Tick:usv[14]~q\,
	datad => \Tick:usv[12]~q\,
	combout => \DAT3|Mux4~0_combout\);

-- Location: LCCOMB_X32_Y22_N26
\DAT3|Mux4~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux4~1_combout\ = (\DAT3|Mux4~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT3|Mux4~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT3|Mux4~1_combout\);

-- Location: LCCOMB_X32_Y22_N12
\DAT3|Mux3~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux3~0_combout\ = (\Tick:usv[13]~q\ & ((\Tick:usv[14]~q\ & ((\Tick:usv[12]~q\))) # (!\Tick:usv[14]~q\ & (\Tick:usv[15]~q\ & !\Tick:usv[12]~q\)))) # (!\Tick:usv[13]~q\ & (!\Tick:usv[15]~q\ & (\Tick:usv[14]~q\ $ (\Tick:usv[12]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000100011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[15]~q\,
	datab => \Tick:usv[13]~q\,
	datac => \Tick:usv[14]~q\,
	datad => \Tick:usv[12]~q\,
	combout => \DAT3|Mux3~0_combout\);

-- Location: LCCOMB_X32_Y22_N10
\DAT3|Mux3~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux3~1_combout\ = (\DAT3|Mux3~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DAT3|Mux3~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT3|Mux3~1_combout\);

-- Location: LCCOMB_X32_Y22_N28
\DAT3|Mux2~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux2~0_combout\ = (\Tick:usv[13]~q\ & (!\Tick:usv[15]~q\ & ((\Tick:usv[12]~q\)))) # (!\Tick:usv[13]~q\ & ((\Tick:usv[14]~q\ & (!\Tick:usv[15]~q\)) # (!\Tick:usv[14]~q\ & ((\Tick:usv[12]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[15]~q\,
	datab => \Tick:usv[13]~q\,
	datac => \Tick:usv[14]~q\,
	datad => \Tick:usv[12]~q\,
	combout => \DAT3|Mux2~0_combout\);

-- Location: LCCOMB_X32_Y22_N22
\DAT3|Mux2~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux2~1_combout\ = (\DAT3|Mux2~0_combout\ & !\RST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT3|Mux2~0_combout\,
	datac => \RST~input_o\,
	combout => \DAT3|Mux2~1_combout\);

-- Location: LCCOMB_X32_Y22_N4
\DAT3|Mux1~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux1~0_combout\ = (\Tick:usv[13]~q\ & (!\Tick:usv[15]~q\ & ((\Tick:usv[12]~q\) # (!\Tick:usv[14]~q\)))) # (!\Tick:usv[13]~q\ & (\Tick:usv[12]~q\ & (\Tick:usv[15]~q\ $ (!\Tick:usv[14]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110010100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[15]~q\,
	datab => \Tick:usv[13]~q\,
	datac => \Tick:usv[14]~q\,
	datad => \Tick:usv[12]~q\,
	combout => \DAT3|Mux1~0_combout\);

-- Location: LCCOMB_X32_Y22_N2
\DAT3|Mux1~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux1~1_combout\ = (!\RST~input_o\ & \DAT3|Mux1~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \RST~input_o\,
	datac => \DAT3|Mux1~0_combout\,
	combout => \DAT3|Mux1~1_combout\);

-- Location: LCCOMB_X32_Y22_N8
\DAT3|Mux0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux0~0_combout\ = (\Tick:usv[12]~q\ & ((\Tick:usv[15]~q\) # (\Tick:usv[13]~q\ $ (\Tick:usv[14]~q\)))) # (!\Tick:usv[12]~q\ & ((\Tick:usv[13]~q\) # (\Tick:usv[15]~q\ $ (\Tick:usv[14]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111011011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Tick:usv[15]~q\,
	datab => \Tick:usv[13]~q\,
	datac => \Tick:usv[14]~q\,
	datad => \Tick:usv[12]~q\,
	combout => \DAT3|Mux0~0_combout\);

-- Location: LCCOMB_X32_Y22_N14
\DAT3|Mux0~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DAT3|Mux0~1_combout\ = (\RST~input_o\) # (\DAT3|Mux0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \RST~input_o\,
	datac => \DAT3|Mux0~0_combout\,
	combout => \DAT3|Mux0~1_combout\);

ww_Error <= \Error~output_o\;

ww_D0(0) <= \D0[0]~output_o\;

ww_D0(1) <= \D0[1]~output_o\;

ww_D0(2) <= \D0[2]~output_o\;

ww_D0(3) <= \D0[3]~output_o\;

ww_D0(4) <= \D0[4]~output_o\;

ww_D0(5) <= \D0[5]~output_o\;

ww_D0(6) <= \D0[6]~output_o\;

ww_D1(0) <= \D1[0]~output_o\;

ww_D1(1) <= \D1[1]~output_o\;

ww_D1(2) <= \D1[2]~output_o\;

ww_D1(3) <= \D1[3]~output_o\;

ww_D1(4) <= \D1[4]~output_o\;

ww_D1(5) <= \D1[5]~output_o\;

ww_D1(6) <= \D1[6]~output_o\;

ww_D2(0) <= \D2[0]~output_o\;

ww_D2(1) <= \D2[1]~output_o\;

ww_D2(2) <= \D2[2]~output_o\;

ww_D2(3) <= \D2[3]~output_o\;

ww_D2(4) <= \D2[4]~output_o\;

ww_D2(5) <= \D2[5]~output_o\;

ww_D2(6) <= \D2[6]~output_o\;

ww_D3(0) <= \D3[0]~output_o\;

ww_D3(1) <= \D3[1]~output_o\;

ww_D3(2) <= \D3[2]~output_o\;

ww_D3(3) <= \D3[3]~output_o\;

ww_D3(4) <= \D3[4]~output_o\;

ww_D3(5) <= \D3[5]~output_o\;

ww_D3(6) <= \D3[6]~output_o\;
END structure;


