transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {G:/workbench/Altera/projects/Uni_EDA/Basiskomponenten/FF/DFlipFlop/DFlipFlop.vhd}
vcom -93 -work work {G:/workbench/Altera/projects/Uni_EDA/Counter/Counter.vhd}
vcom -93 -work work {G:/workbench/Altera/projects/Uni_EDA/Display/Segmentdisplay/Segmentdisplay.vhd}
vcom -93 -work work {G:/workbench/Altera/projects/Uni_EDA/Clock/ClockDivider/ClockDivider.vhd}
vcom -93 -work work {G:/workbench/Altera/projects/Uni_EDA/Clock/HexClock/HexClock.vhd}

