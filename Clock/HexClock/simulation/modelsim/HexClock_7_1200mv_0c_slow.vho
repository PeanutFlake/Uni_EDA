-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/14/2018 20:37:12"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	HexClock IS
    PORT (
	CLK : IN std_logic;
	nRST : IN std_logic;
	Error : OUT std_logic;
	D0 : OUT std_logic_vector(6 DOWNTO 0);
	D1 : OUT std_logic_vector(6 DOWNTO 0);
	D2 : OUT std_logic_vector(6 DOWNTO 0);
	D3 : OUT std_logic_vector(6 DOWNTO 0)
	);
END HexClock;

-- Design Ports Information
-- Error	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[0]	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[1]	=>  Location: PIN_F22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[2]	=>  Location: PIN_E17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[3]	=>  Location: PIN_L26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[4]	=>  Location: PIN_L25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[5]	=>  Location: PIN_J22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[6]	=>  Location: PIN_H22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[0]	=>  Location: PIN_M24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[1]	=>  Location: PIN_Y22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[2]	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[3]	=>  Location: PIN_W22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[4]	=>  Location: PIN_W25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[5]	=>  Location: PIN_U23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[6]	=>  Location: PIN_U24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[0]	=>  Location: PIN_AA25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[1]	=>  Location: PIN_AA26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[2]	=>  Location: PIN_Y25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[3]	=>  Location: PIN_W26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[4]	=>  Location: PIN_Y26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[5]	=>  Location: PIN_W27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[6]	=>  Location: PIN_W28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[0]	=>  Location: PIN_V21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[1]	=>  Location: PIN_U21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[2]	=>  Location: PIN_AB20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[3]	=>  Location: PIN_AA21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[4]	=>  Location: PIN_AD24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[5]	=>  Location: PIN_AF23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[6]	=>  Location: PIN_Y19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- nRST	=>  Location: PIN_M23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF HexClock IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_nRST : std_logic;
SIGNAL ww_Error : std_logic;
SIGNAL ww_D0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D3 : std_logic_vector(6 DOWNTO 0);
SIGNAL \CLK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \MCLK|Buff|s_q~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \Error~output_o\ : std_logic;
SIGNAL \D0[0]~output_o\ : std_logic;
SIGNAL \D0[1]~output_o\ : std_logic;
SIGNAL \D0[2]~output_o\ : std_logic;
SIGNAL \D0[3]~output_o\ : std_logic;
SIGNAL \D0[4]~output_o\ : std_logic;
SIGNAL \D0[5]~output_o\ : std_logic;
SIGNAL \D0[6]~output_o\ : std_logic;
SIGNAL \D1[0]~output_o\ : std_logic;
SIGNAL \D1[1]~output_o\ : std_logic;
SIGNAL \D1[2]~output_o\ : std_logic;
SIGNAL \D1[3]~output_o\ : std_logic;
SIGNAL \D1[4]~output_o\ : std_logic;
SIGNAL \D1[5]~output_o\ : std_logic;
SIGNAL \D1[6]~output_o\ : std_logic;
SIGNAL \D2[0]~output_o\ : std_logic;
SIGNAL \D2[1]~output_o\ : std_logic;
SIGNAL \D2[2]~output_o\ : std_logic;
SIGNAL \D2[3]~output_o\ : std_logic;
SIGNAL \D2[4]~output_o\ : std_logic;
SIGNAL \D2[5]~output_o\ : std_logic;
SIGNAL \D2[6]~output_o\ : std_logic;
SIGNAL \D3[0]~output_o\ : std_logic;
SIGNAL \D3[1]~output_o\ : std_logic;
SIGNAL \D3[2]~output_o\ : std_logic;
SIGNAL \D3[3]~output_o\ : std_logic;
SIGNAL \D3[4]~output_o\ : std_logic;
SIGNAL \D3[5]~output_o\ : std_logic;
SIGNAL \D3[6]~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \CLK~inputclkctrl_outclk\ : std_logic;
SIGNAL \MCLK|Count|Count:c[0]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Add0~45\ : std_logic;
SIGNAL \MCLK|Count|Add0~46_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[24]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[24]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~47\ : std_logic;
SIGNAL \MCLK|Count|Add0~48_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[25]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[25]~q\ : std_logic;
SIGNAL \MCLK|Equal0~4_combout\ : std_logic;
SIGNAL \MCLK|Equal0~0_combout\ : std_logic;
SIGNAL \MCLK|Equal0~2_combout\ : std_logic;
SIGNAL \MCLK|Equal0~1_combout\ : std_logic;
SIGNAL \MCLK|Equal0~3_combout\ : std_logic;
SIGNAL \MCLK|Equal0~5_combout\ : std_logic;
SIGNAL \MCLK|Count|Add0~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[1]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[1]~q\ : std_logic;
SIGNAL \MCLK|Equal0~6_combout\ : std_logic;
SIGNAL \MCLK|Equal0~7_combout\ : std_logic;
SIGNAL \MCLK|Equal0~8_combout\ : std_logic;
SIGNAL \nRST~input_o\ : std_logic;
SIGNAL \MCLK|s_qe~q\ : std_logic;
SIGNAL \MCLK|s_rst~combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[0]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~1\ : std_logic;
SIGNAL \MCLK|Count|Add0~2_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[2]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[2]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~3\ : std_logic;
SIGNAL \MCLK|Count|Add0~4_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[3]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[3]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~5\ : std_logic;
SIGNAL \MCLK|Count|Add0~6_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[4]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[4]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~7\ : std_logic;
SIGNAL \MCLK|Count|Add0~8_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[5]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[5]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~9\ : std_logic;
SIGNAL \MCLK|Count|Add0~10_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[6]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[6]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~11\ : std_logic;
SIGNAL \MCLK|Count|Add0~12_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[7]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[7]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~13\ : std_logic;
SIGNAL \MCLK|Count|Add0~14_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[8]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[8]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~15\ : std_logic;
SIGNAL \MCLK|Count|Add0~16_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[9]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[9]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~17\ : std_logic;
SIGNAL \MCLK|Count|Add0~18_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[10]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[10]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~19\ : std_logic;
SIGNAL \MCLK|Count|Add0~20_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[11]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[11]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~21\ : std_logic;
SIGNAL \MCLK|Count|Add0~22_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[12]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[12]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~23\ : std_logic;
SIGNAL \MCLK|Count|Add0~24_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[13]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[13]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~25\ : std_logic;
SIGNAL \MCLK|Count|Add0~26_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[14]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[14]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~27\ : std_logic;
SIGNAL \MCLK|Count|Add0~28_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[15]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[15]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~29\ : std_logic;
SIGNAL \MCLK|Count|Add0~30_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[16]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[16]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~31\ : std_logic;
SIGNAL \MCLK|Count|Add0~32_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[17]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[17]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~33\ : std_logic;
SIGNAL \MCLK|Count|Add0~34_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[18]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[18]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~35\ : std_logic;
SIGNAL \MCLK|Count|Add0~36_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[19]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[19]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~37\ : std_logic;
SIGNAL \MCLK|Count|Add0~38_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[20]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[20]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~39\ : std_logic;
SIGNAL \MCLK|Count|Add0~40_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[21]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[21]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~41\ : std_logic;
SIGNAL \MCLK|Count|Add0~42_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[22]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[22]~q\ : std_logic;
SIGNAL \MCLK|Count|Add0~43\ : std_logic;
SIGNAL \MCLK|Count|Add0~44_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[23]~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Count:c[23]~q\ : std_logic;
SIGNAL \MCLK|Count|Equal0~0_combout\ : std_logic;
SIGNAL \MCLK|Count|Equal0~1_combout\ : std_logic;
SIGNAL \MCLK|Count|Equal0~2_combout\ : std_logic;
SIGNAL \MCLK|Count|Equal0~3_combout\ : std_logic;
SIGNAL \MCLK|Count|Equal0~4_combout\ : std_logic;
SIGNAL \MCLK|Count|s_ov~0_combout\ : std_logic;
SIGNAL \MCLK|Count|s_ov~q\ : std_logic;
SIGNAL \MCLK|Buff|qn~0_combout\ : std_logic;
SIGNAL \MCLK|Buff|qn~feeder_combout\ : std_logic;
SIGNAL \MCLK|Buff|qn~1_combout\ : std_logic;
SIGNAL \MCLK|Buff|qn~q\ : std_logic;
SIGNAL \MCLK|Buff|s_q~combout\ : std_logic;
SIGNAL \MCLK|Buff|s_q~clkctrl_outclk\ : std_logic;
SIGNAL \MCOUNT|Count:c[0]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[0]~q\ : std_logic;
SIGNAL \MCOUNT|Add0~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[1]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[1]~q\ : std_logic;
SIGNAL \MCOUNT|Add0~1\ : std_logic;
SIGNAL \MCOUNT|Add0~2_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[2]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[2]~q\ : std_logic;
SIGNAL \MCOUNT|Add0~3\ : std_logic;
SIGNAL \MCOUNT|Add0~4_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[3]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[3]~q\ : std_logic;
SIGNAL \MCOUNT|Add0~5\ : std_logic;
SIGNAL \MCOUNT|Add0~6_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[4]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[4]~q\ : std_logic;
SIGNAL \MCOUNT|Add0~7\ : std_logic;
SIGNAL \MCOUNT|Add0~8_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[5]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[5]~q\ : std_logic;
SIGNAL \MCOUNT|Add0~9\ : std_logic;
SIGNAL \MCOUNT|Add0~10_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[6]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[6]~q\ : std_logic;
SIGNAL \MCOUNT|Add0~11\ : std_logic;
SIGNAL \MCOUNT|Add0~12_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[7]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[7]~q\ : std_logic;
SIGNAL \MCOUNT|Equal0~2_combout\ : std_logic;
SIGNAL \MCOUNT|Add0~13\ : std_logic;
SIGNAL \MCOUNT|Add0~14_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[8]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[8]~q\ : std_logic;
SIGNAL \MCOUNT|Add0~15\ : std_logic;
SIGNAL \MCOUNT|Add0~16_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[9]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[9]~q\ : std_logic;
SIGNAL \MCOUNT|Add0~17\ : std_logic;
SIGNAL \MCOUNT|Add0~18_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[10]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[10]~q\ : std_logic;
SIGNAL \MCOUNT|Add0~19\ : std_logic;
SIGNAL \MCOUNT|Add0~20_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[11]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[11]~q\ : std_logic;
SIGNAL \MCOUNT|Equal0~1_combout\ : std_logic;
SIGNAL \MCOUNT|Equal0~3_combout\ : std_logic;
SIGNAL \MCOUNT|Add0~21\ : std_logic;
SIGNAL \MCOUNT|Add0~22_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[12]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[12]~q\ : std_logic;
SIGNAL \MCOUNT|Add0~23\ : std_logic;
SIGNAL \MCOUNT|Add0~24_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[13]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[13]~q\ : std_logic;
SIGNAL \MCOUNT|Add0~25\ : std_logic;
SIGNAL \MCOUNT|Add0~26_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[14]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[14]~q\ : std_logic;
SIGNAL \MCOUNT|Add0~27\ : std_logic;
SIGNAL \MCOUNT|Add0~28_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[15]~0_combout\ : std_logic;
SIGNAL \MCOUNT|Count:c[15]~q\ : std_logic;
SIGNAL \MCOUNT|Equal0~0_combout\ : std_logic;
SIGNAL \MCOUNT|Equal0~4_combout\ : std_logic;
SIGNAL \MCOUNT|s_ov~0_combout\ : std_logic;
SIGNAL \MCOUNT|s_ov~q\ : std_logic;
SIGNAL \Error~0_combout\ : std_logic;
SIGNAL \DAT0|Mux6~0_combout\ : std_logic;
SIGNAL \DAT0|Mux6~1_combout\ : std_logic;
SIGNAL \DAT0|Mux5~0_combout\ : std_logic;
SIGNAL \DAT0|Mux5~1_combout\ : std_logic;
SIGNAL \DAT0|Mux4~0_combout\ : std_logic;
SIGNAL \DAT0|Mux4~1_combout\ : std_logic;
SIGNAL \DAT0|Mux3~0_combout\ : std_logic;
SIGNAL \DAT0|Mux3~1_combout\ : std_logic;
SIGNAL \DAT0|Mux2~0_combout\ : std_logic;
SIGNAL \DAT0|Mux2~1_combout\ : std_logic;
SIGNAL \DAT0|Mux1~0_combout\ : std_logic;
SIGNAL \DAT0|Mux1~1_combout\ : std_logic;
SIGNAL \DAT0|Mux0~0_combout\ : std_logic;
SIGNAL \DAT0|Mux0~1_combout\ : std_logic;
SIGNAL \DAT1|Mux6~0_combout\ : std_logic;
SIGNAL \DAT1|Mux6~1_combout\ : std_logic;
SIGNAL \DAT1|Mux5~0_combout\ : std_logic;
SIGNAL \DAT1|Mux5~1_combout\ : std_logic;
SIGNAL \DAT1|Mux4~0_combout\ : std_logic;
SIGNAL \DAT1|Mux4~1_combout\ : std_logic;
SIGNAL \DAT1|Mux3~0_combout\ : std_logic;
SIGNAL \DAT1|Mux3~1_combout\ : std_logic;
SIGNAL \DAT1|Mux2~0_combout\ : std_logic;
SIGNAL \DAT1|Mux2~1_combout\ : std_logic;
SIGNAL \DAT1|Mux1~0_combout\ : std_logic;
SIGNAL \DAT1|Mux1~1_combout\ : std_logic;
SIGNAL \DAT1|Mux0~0_combout\ : std_logic;
SIGNAL \DAT1|Mux0~1_combout\ : std_logic;
SIGNAL \DAT2|Mux6~0_combout\ : std_logic;
SIGNAL \DAT2|Mux6~1_combout\ : std_logic;
SIGNAL \DAT2|Mux5~0_combout\ : std_logic;
SIGNAL \DAT2|Mux5~1_combout\ : std_logic;
SIGNAL \DAT2|Mux4~0_combout\ : std_logic;
SIGNAL \DAT2|Mux4~1_combout\ : std_logic;
SIGNAL \DAT2|Mux3~0_combout\ : std_logic;
SIGNAL \DAT2|Mux3~1_combout\ : std_logic;
SIGNAL \DAT2|Mux2~0_combout\ : std_logic;
SIGNAL \DAT2|Mux2~1_combout\ : std_logic;
SIGNAL \DAT2|Mux1~0_combout\ : std_logic;
SIGNAL \DAT2|Mux1~1_combout\ : std_logic;
SIGNAL \DAT2|Mux0~0_combout\ : std_logic;
SIGNAL \DAT2|Mux0~1_combout\ : std_logic;
SIGNAL \DAT3|Mux6~0_combout\ : std_logic;
SIGNAL \DAT3|Mux6~1_combout\ : std_logic;
SIGNAL \DAT3|Mux5~0_combout\ : std_logic;
SIGNAL \DAT3|Mux5~1_combout\ : std_logic;
SIGNAL \DAT3|Mux4~0_combout\ : std_logic;
SIGNAL \DAT3|Mux4~1_combout\ : std_logic;
SIGNAL \DAT3|Mux3~0_combout\ : std_logic;
SIGNAL \DAT3|Mux3~1_combout\ : std_logic;
SIGNAL \DAT3|Mux2~0_combout\ : std_logic;
SIGNAL \DAT3|Mux2~1_combout\ : std_logic;
SIGNAL \DAT3|Mux1~0_combout\ : std_logic;
SIGNAL \DAT3|Mux1~1_combout\ : std_logic;
SIGNAL \DAT3|Mux0~0_combout\ : std_logic;
SIGNAL \DAT3|Mux0~1_combout\ : std_logic;
SIGNAL \MCLK|ALT_INV_s_rst~combout\ : std_logic;
SIGNAL \MCLK|Buff|ALT_INV_qn~1_combout\ : std_logic;

BEGIN

ww_CLK <= CLK;
ww_nRST <= nRST;
Error <= ww_Error;
D0 <= ww_D0;
D1 <= ww_D1;
D2 <= ww_D2;
D3 <= ww_D3;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLK~input_o\);

\MCLK|Buff|s_q~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \MCLK|Buff|s_q~combout\);
\MCLK|ALT_INV_s_rst~combout\ <= NOT \MCLK|s_rst~combout\;
\MCLK|Buff|ALT_INV_qn~1_combout\ <= NOT \MCLK|Buff|qn~1_combout\;

-- Location: IOOBUF_X69_Y73_N16
\Error~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Error~0_combout\,
	devoe => ww_devoe,
	o => \Error~output_o\);

-- Location: IOOBUF_X69_Y73_N23
\D0[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|Mux6~1_combout\,
	devoe => ww_devoe,
	o => \D0[0]~output_o\);

-- Location: IOOBUF_X107_Y73_N23
\D0[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|Mux5~1_combout\,
	devoe => ww_devoe,
	o => \D0[1]~output_o\);

-- Location: IOOBUF_X67_Y73_N23
\D0[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|Mux4~1_combout\,
	devoe => ww_devoe,
	o => \D0[2]~output_o\);

-- Location: IOOBUF_X115_Y50_N2
\D0[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|Mux3~1_combout\,
	devoe => ww_devoe,
	o => \D0[3]~output_o\);

-- Location: IOOBUF_X115_Y54_N16
\D0[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|Mux2~1_combout\,
	devoe => ww_devoe,
	o => \D0[4]~output_o\);

-- Location: IOOBUF_X115_Y67_N16
\D0[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|Mux1~1_combout\,
	devoe => ww_devoe,
	o => \D0[5]~output_o\);

-- Location: IOOBUF_X115_Y69_N2
\D0[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT0|Mux0~1_combout\,
	devoe => ww_devoe,
	o => \D0[6]~output_o\);

-- Location: IOOBUF_X115_Y41_N2
\D1[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|Mux6~1_combout\,
	devoe => ww_devoe,
	o => \D1[0]~output_o\);

-- Location: IOOBUF_X115_Y30_N9
\D1[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|Mux5~1_combout\,
	devoe => ww_devoe,
	o => \D1[1]~output_o\);

-- Location: IOOBUF_X115_Y25_N23
\D1[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|Mux4~1_combout\,
	devoe => ww_devoe,
	o => \D1[2]~output_o\);

-- Location: IOOBUF_X115_Y30_N2
\D1[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|Mux3~1_combout\,
	devoe => ww_devoe,
	o => \D1[3]~output_o\);

-- Location: IOOBUF_X115_Y20_N9
\D1[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|Mux2~1_combout\,
	devoe => ww_devoe,
	o => \D1[4]~output_o\);

-- Location: IOOBUF_X115_Y22_N2
\D1[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|Mux1~1_combout\,
	devoe => ww_devoe,
	o => \D1[5]~output_o\);

-- Location: IOOBUF_X115_Y28_N9
\D1[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT1|Mux0~1_combout\,
	devoe => ww_devoe,
	o => \D1[6]~output_o\);

-- Location: IOOBUF_X115_Y17_N9
\D2[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|Mux6~1_combout\,
	devoe => ww_devoe,
	o => \D2[0]~output_o\);

-- Location: IOOBUF_X115_Y16_N2
\D2[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|Mux5~1_combout\,
	devoe => ww_devoe,
	o => \D2[1]~output_o\);

-- Location: IOOBUF_X115_Y19_N9
\D2[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|Mux4~1_combout\,
	devoe => ww_devoe,
	o => \D2[2]~output_o\);

-- Location: IOOBUF_X115_Y19_N2
\D2[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|Mux3~1_combout\,
	devoe => ww_devoe,
	o => \D2[3]~output_o\);

-- Location: IOOBUF_X115_Y18_N2
\D2[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|Mux2~1_combout\,
	devoe => ww_devoe,
	o => \D2[4]~output_o\);

-- Location: IOOBUF_X115_Y20_N2
\D2[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|Mux1~1_combout\,
	devoe => ww_devoe,
	o => \D2[5]~output_o\);

-- Location: IOOBUF_X115_Y21_N16
\D2[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT2|Mux0~1_combout\,
	devoe => ww_devoe,
	o => \D2[6]~output_o\);

-- Location: IOOBUF_X115_Y25_N16
\D3[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|Mux6~1_combout\,
	devoe => ww_devoe,
	o => \D3[0]~output_o\);

-- Location: IOOBUF_X115_Y29_N2
\D3[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|Mux5~1_combout\,
	devoe => ww_devoe,
	o => \D3[1]~output_o\);

-- Location: IOOBUF_X100_Y0_N2
\D3[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|Mux4~1_combout\,
	devoe => ww_devoe,
	o => \D3[2]~output_o\);

-- Location: IOOBUF_X111_Y0_N2
\D3[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|Mux3~1_combout\,
	devoe => ww_devoe,
	o => \D3[3]~output_o\);

-- Location: IOOBUF_X105_Y0_N23
\D3[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|Mux2~1_combout\,
	devoe => ww_devoe,
	o => \D3[4]~output_o\);

-- Location: IOOBUF_X105_Y0_N9
\D3[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|Mux1~1_combout\,
	devoe => ww_devoe,
	o => \D3[5]~output_o\);

-- Location: IOOBUF_X105_Y0_N2
\D3[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DAT3|Mux0~1_combout\,
	devoe => ww_devoe,
	o => \D3[6]~output_o\);

-- Location: IOIBUF_X0_Y36_N15
\CLK~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: CLKCTRL_G4
\CLK~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLK~inputclkctrl_outclk\);

-- Location: LCCOMB_X98_Y14_N16
\MCLK|Count|Count:c[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[0]~0_combout\ = (\MCLK|Count|Equal0~4_combout\) # (!\MCLK|Count|Count:c[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCLK|Count|Count:c[0]~q\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[0]~0_combout\);

-- Location: LCCOMB_X99_Y13_N20
\MCLK|Count|Add0~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~44_combout\ = (\MCLK|Count|Count:c[23]~q\ & (\MCLK|Count|Add0~43\ $ (GND))) # (!\MCLK|Count|Count:c[23]~q\ & (!\MCLK|Count|Add0~43\ & VCC))
-- \MCLK|Count|Add0~45\ = CARRY((\MCLK|Count|Count:c[23]~q\ & !\MCLK|Count|Add0~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[23]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~43\,
	combout => \MCLK|Count|Add0~44_combout\,
	cout => \MCLK|Count|Add0~45\);

-- Location: LCCOMB_X99_Y13_N22
\MCLK|Count|Add0~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~46_combout\ = (\MCLK|Count|Count:c[24]~q\ & (!\MCLK|Count|Add0~45\)) # (!\MCLK|Count|Count:c[24]~q\ & ((\MCLK|Count|Add0~45\) # (GND)))
-- \MCLK|Count|Add0~47\ = CARRY((!\MCLK|Count|Add0~45\) # (!\MCLK|Count|Count:c[24]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[24]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~45\,
	combout => \MCLK|Count|Add0~46_combout\,
	cout => \MCLK|Count|Add0~47\);

-- Location: LCCOMB_X98_Y13_N28
\MCLK|Count|Count:c[24]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[24]~0_combout\ = (!\MCLK|Count|Equal0~4_combout\ & \MCLK|Count|Add0~46_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCLK|Count|Equal0~4_combout\,
	datad => \MCLK|Count|Add0~46_combout\,
	combout => \MCLK|Count|Count:c[24]~0_combout\);

-- Location: FF_X98_Y13_N29
\MCLK|Count|Count:c[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[24]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[24]~q\);

-- Location: LCCOMB_X99_Y13_N24
\MCLK|Count|Add0~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~48_combout\ = \MCLK|Count|Add0~47\ $ (!\MCLK|Count|Count:c[25]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \MCLK|Count|Count:c[25]~q\,
	cin => \MCLK|Count|Add0~47\,
	combout => \MCLK|Count|Add0~48_combout\);

-- Location: LCCOMB_X98_Y13_N22
\MCLK|Count|Count:c[25]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[25]~0_combout\ = (\MCLK|Count|Equal0~4_combout\ & (\MCLK|Count|Count:c[25]~q\)) # (!\MCLK|Count|Equal0~4_combout\ & ((\MCLK|Count|Add0~48_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Equal0~4_combout\,
	datac => \MCLK|Count|Count:c[25]~q\,
	datad => \MCLK|Count|Add0~48_combout\,
	combout => \MCLK|Count|Count:c[25]~0_combout\);

-- Location: FF_X98_Y13_N23
\MCLK|Count|Count:c[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[25]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[25]~q\);

-- Location: LCCOMB_X97_Y13_N2
\MCLK|Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Equal0~4_combout\ = (!\MCLK|Count|Count:c[23]~q\ & (\MCLK|Count|Count:c[24]~q\ & (\MCLK|Count|Count:c[18]~q\ & !\MCLK|Count|Count:c[25]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[23]~q\,
	datab => \MCLK|Count|Count:c[24]~q\,
	datac => \MCLK|Count|Count:c[18]~q\,
	datad => \MCLK|Count|Count:c[25]~q\,
	combout => \MCLK|Equal0~4_combout\);

-- Location: LCCOMB_X98_Y13_N26
\MCLK|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Equal0~0_combout\ = (\MCLK|Count|Count:c[22]~q\ & (\MCLK|Count|Count:c[21]~q\ & (\MCLK|Count|Count:c[19]~q\ & \MCLK|Count|Count:c[20]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[22]~q\,
	datab => \MCLK|Count|Count:c[21]~q\,
	datac => \MCLK|Count|Count:c[19]~q\,
	datad => \MCLK|Count|Count:c[20]~q\,
	combout => \MCLK|Equal0~0_combout\);

-- Location: LCCOMB_X98_Y14_N8
\MCLK|Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Equal0~2_combout\ = (!\MCLK|Count|Count:c[8]~q\ & (!\MCLK|Count|Count:c[9]~q\ & !\MCLK|Count|Count:c[6]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[8]~q\,
	datac => \MCLK|Count|Count:c[9]~q\,
	datad => \MCLK|Count|Count:c[6]~q\,
	combout => \MCLK|Equal0~2_combout\);

-- Location: LCCOMB_X98_Y13_N2
\MCLK|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Equal0~1_combout\ = (\MCLK|Count|Count:c[14]~q\ & (\MCLK|Count|Count:c[13]~q\ & (!\MCLK|Count|Count:c[10]~q\ & \MCLK|Count|Count:c[12]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[14]~q\,
	datab => \MCLK|Count|Count:c[13]~q\,
	datac => \MCLK|Count|Count:c[10]~q\,
	datad => \MCLK|Count|Count:c[12]~q\,
	combout => \MCLK|Equal0~1_combout\);

-- Location: LCCOMB_X98_Y13_N8
\MCLK|Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Equal0~3_combout\ = (\MCLK|Equal0~0_combout\ & (\MCLK|Equal0~2_combout\ & \MCLK|Equal0~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Equal0~0_combout\,
	datac => \MCLK|Equal0~2_combout\,
	datad => \MCLK|Equal0~1_combout\,
	combout => \MCLK|Equal0~3_combout\);

-- Location: LCCOMB_X98_Y14_N22
\MCLK|Equal0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Equal0~5_combout\ = (\MCLK|Count|Count:c[16]~q\ & (!\MCLK|Count|Count:c[15]~q\ & (!\MCLK|Count|Count:c[17]~q\ & \MCLK|Count|Count:c[11]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[16]~q\,
	datab => \MCLK|Count|Count:c[15]~q\,
	datac => \MCLK|Count|Count:c[17]~q\,
	datad => \MCLK|Count|Count:c[11]~q\,
	combout => \MCLK|Equal0~5_combout\);

-- Location: LCCOMB_X99_Y14_N8
\MCLK|Count|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~0_combout\ = (\MCLK|Count|Count:c[0]~q\ & (\MCLK|Count|Count:c[1]~q\ $ (VCC))) # (!\MCLK|Count|Count:c[0]~q\ & (\MCLK|Count|Count:c[1]~q\ & VCC))
-- \MCLK|Count|Add0~1\ = CARRY((\MCLK|Count|Count:c[0]~q\ & \MCLK|Count|Count:c[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[0]~q\,
	datab => \MCLK|Count|Count:c[1]~q\,
	datad => VCC,
	combout => \MCLK|Count|Add0~0_combout\,
	cout => \MCLK|Count|Add0~1\);

-- Location: LCCOMB_X98_Y14_N6
\MCLK|Count|Count:c[1]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[1]~0_combout\ = (\MCLK|Count|Add0~0_combout\ & !\MCLK|Count|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCLK|Count|Add0~0_combout\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[1]~0_combout\);

-- Location: FF_X98_Y14_N7
\MCLK|Count|Count:c[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[1]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[1]~q\);

-- Location: LCCOMB_X98_Y14_N10
\MCLK|Equal0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Equal0~6_combout\ = (\MCLK|Count|Count:c[3]~q\ & (\MCLK|Count|Count:c[4]~q\ & (\MCLK|Count|Count:c[5]~q\ & !\MCLK|Count|Count:c[7]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[3]~q\,
	datab => \MCLK|Count|Count:c[4]~q\,
	datac => \MCLK|Count|Count:c[5]~q\,
	datad => \MCLK|Count|Count:c[7]~q\,
	combout => \MCLK|Equal0~6_combout\);

-- Location: LCCOMB_X98_Y14_N12
\MCLK|Equal0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Equal0~7_combout\ = (\MCLK|Count|Count:c[2]~q\ & (!\MCLK|Count|Count:c[0]~q\ & (\MCLK|Count|Count:c[1]~q\ & \MCLK|Equal0~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[2]~q\,
	datab => \MCLK|Count|Count:c[0]~q\,
	datac => \MCLK|Count|Count:c[1]~q\,
	datad => \MCLK|Equal0~6_combout\,
	combout => \MCLK|Equal0~7_combout\);

-- Location: LCCOMB_X98_Y14_N20
\MCLK|Equal0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Equal0~8_combout\ = (\MCLK|Equal0~4_combout\ & (\MCLK|Equal0~3_combout\ & (\MCLK|Equal0~5_combout\ & \MCLK|Equal0~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Equal0~4_combout\,
	datab => \MCLK|Equal0~3_combout\,
	datac => \MCLK|Equal0~5_combout\,
	datad => \MCLK|Equal0~7_combout\,
	combout => \MCLK|Equal0~8_combout\);

-- Location: IOIBUF_X115_Y40_N8
\nRST~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_nRST,
	o => \nRST~input_o\);

-- Location: FF_X98_Y14_N21
\MCLK|s_qe\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Equal0~8_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|s_qe~q\);

-- Location: LCCOMB_X98_Y14_N2
\MCLK|s_rst\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|s_rst~combout\ = (\MCLK|s_qe~q\) # (!\nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCLK|s_qe~q\,
	datad => \nRST~input_o\,
	combout => \MCLK|s_rst~combout\);

-- Location: FF_X98_Y14_N17
\MCLK|Count|Count:c[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[0]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[0]~q\);

-- Location: LCCOMB_X99_Y14_N10
\MCLK|Count|Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~2_combout\ = (\MCLK|Count|Count:c[2]~q\ & (!\MCLK|Count|Add0~1\)) # (!\MCLK|Count|Count:c[2]~q\ & ((\MCLK|Count|Add0~1\) # (GND)))
-- \MCLK|Count|Add0~3\ = CARRY((!\MCLK|Count|Add0~1\) # (!\MCLK|Count|Count:c[2]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[2]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~1\,
	combout => \MCLK|Count|Add0~2_combout\,
	cout => \MCLK|Count|Add0~3\);

-- Location: LCCOMB_X98_Y13_N10
\MCLK|Count|Count:c[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[2]~0_combout\ = (\MCLK|Count|Add0~2_combout\ & !\MCLK|Count|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCLK|Count|Add0~2_combout\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[2]~0_combout\);

-- Location: FF_X99_Y14_N9
\MCLK|Count|Count:c[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \MCLK|Count|Count:c[2]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[2]~q\);

-- Location: LCCOMB_X99_Y14_N12
\MCLK|Count|Add0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~4_combout\ = (\MCLK|Count|Count:c[3]~q\ & (\MCLK|Count|Add0~3\ $ (GND))) # (!\MCLK|Count|Count:c[3]~q\ & (!\MCLK|Count|Add0~3\ & VCC))
-- \MCLK|Count|Add0~5\ = CARRY((\MCLK|Count|Count:c[3]~q\ & !\MCLK|Count|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[3]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~3\,
	combout => \MCLK|Count|Add0~4_combout\,
	cout => \MCLK|Count|Add0~5\);

-- Location: LCCOMB_X99_Y14_N6
\MCLK|Count|Count:c[3]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[3]~0_combout\ = (\MCLK|Count|Add0~4_combout\ & !\MCLK|Count|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Add0~4_combout\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[3]~0_combout\);

-- Location: FF_X99_Y14_N7
\MCLK|Count|Count:c[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[3]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[3]~q\);

-- Location: LCCOMB_X99_Y14_N14
\MCLK|Count|Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~6_combout\ = (\MCLK|Count|Count:c[4]~q\ & (!\MCLK|Count|Add0~5\)) # (!\MCLK|Count|Count:c[4]~q\ & ((\MCLK|Count|Add0~5\) # (GND)))
-- \MCLK|Count|Add0~7\ = CARRY((!\MCLK|Count|Add0~5\) # (!\MCLK|Count|Count:c[4]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[4]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~5\,
	combout => \MCLK|Count|Add0~6_combout\,
	cout => \MCLK|Count|Add0~7\);

-- Location: LCCOMB_X98_Y14_N14
\MCLK|Count|Count:c[4]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[4]~0_combout\ = (\MCLK|Count|Add0~6_combout\ & !\MCLK|Count|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCLK|Count|Add0~6_combout\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[4]~0_combout\);

-- Location: FF_X98_Y14_N15
\MCLK|Count|Count:c[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[4]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[4]~q\);

-- Location: LCCOMB_X99_Y14_N16
\MCLK|Count|Add0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~8_combout\ = (\MCLK|Count|Count:c[5]~q\ & (\MCLK|Count|Add0~7\ $ (GND))) # (!\MCLK|Count|Count:c[5]~q\ & (!\MCLK|Count|Add0~7\ & VCC))
-- \MCLK|Count|Add0~9\ = CARRY((\MCLK|Count|Count:c[5]~q\ & !\MCLK|Count|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[5]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~7\,
	combout => \MCLK|Count|Add0~8_combout\,
	cout => \MCLK|Count|Add0~9\);

-- Location: LCCOMB_X99_Y14_N0
\MCLK|Count|Count:c[5]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[5]~0_combout\ = (\MCLK|Count|Add0~8_combout\ & !\MCLK|Count|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Add0~8_combout\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[5]~0_combout\);

-- Location: FF_X99_Y14_N1
\MCLK|Count|Count:c[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[5]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[5]~q\);

-- Location: LCCOMB_X99_Y14_N18
\MCLK|Count|Add0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~10_combout\ = (\MCLK|Count|Count:c[6]~q\ & (!\MCLK|Count|Add0~9\)) # (!\MCLK|Count|Count:c[6]~q\ & ((\MCLK|Count|Add0~9\) # (GND)))
-- \MCLK|Count|Add0~11\ = CARRY((!\MCLK|Count|Add0~9\) # (!\MCLK|Count|Count:c[6]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[6]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~9\,
	combout => \MCLK|Count|Add0~10_combout\,
	cout => \MCLK|Count|Add0~11\);

-- Location: LCCOMB_X99_Y14_N4
\MCLK|Count|Count:c[6]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[6]~0_combout\ = (\MCLK|Count|Add0~10_combout\ & !\MCLK|Count|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Add0~10_combout\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[6]~0_combout\);

-- Location: FF_X99_Y14_N5
\MCLK|Count|Count:c[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[6]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[6]~q\);

-- Location: LCCOMB_X99_Y14_N20
\MCLK|Count|Add0~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~12_combout\ = (\MCLK|Count|Count:c[7]~q\ & (\MCLK|Count|Add0~11\ $ (GND))) # (!\MCLK|Count|Count:c[7]~q\ & (!\MCLK|Count|Add0~11\ & VCC))
-- \MCLK|Count|Add0~13\ = CARRY((\MCLK|Count|Count:c[7]~q\ & !\MCLK|Count|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[7]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~11\,
	combout => \MCLK|Count|Add0~12_combout\,
	cout => \MCLK|Count|Add0~13\);

-- Location: LCCOMB_X99_Y14_N2
\MCLK|Count|Count:c[7]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[7]~0_combout\ = (\MCLK|Count|Equal0~4_combout\ & ((\MCLK|Count|Count:c[7]~q\))) # (!\MCLK|Count|Equal0~4_combout\ & (\MCLK|Count|Add0~12_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Add0~12_combout\,
	datac => \MCLK|Count|Count:c[7]~q\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[7]~0_combout\);

-- Location: FF_X99_Y14_N3
\MCLK|Count|Count:c[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[7]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[7]~q\);

-- Location: LCCOMB_X99_Y14_N22
\MCLK|Count|Add0~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~14_combout\ = (\MCLK|Count|Count:c[8]~q\ & (!\MCLK|Count|Add0~13\)) # (!\MCLK|Count|Count:c[8]~q\ & ((\MCLK|Count|Add0~13\) # (GND)))
-- \MCLK|Count|Add0~15\ = CARRY((!\MCLK|Count|Add0~13\) # (!\MCLK|Count|Count:c[8]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[8]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~13\,
	combout => \MCLK|Count|Add0~14_combout\,
	cout => \MCLK|Count|Add0~15\);

-- Location: LCCOMB_X98_Y14_N24
\MCLK|Count|Count:c[8]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[8]~0_combout\ = (\MCLK|Count|Add0~14_combout\ & !\MCLK|Count|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Add0~14_combout\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[8]~0_combout\);

-- Location: FF_X98_Y14_N25
\MCLK|Count|Count:c[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[8]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[8]~q\);

-- Location: LCCOMB_X99_Y14_N24
\MCLK|Count|Add0~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~16_combout\ = (\MCLK|Count|Count:c[9]~q\ & (\MCLK|Count|Add0~15\ $ (GND))) # (!\MCLK|Count|Count:c[9]~q\ & (!\MCLK|Count|Add0~15\ & VCC))
-- \MCLK|Count|Add0~17\ = CARRY((\MCLK|Count|Count:c[9]~q\ & !\MCLK|Count|Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[9]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~15\,
	combout => \MCLK|Count|Add0~16_combout\,
	cout => \MCLK|Count|Add0~17\);

-- Location: LCCOMB_X98_Y14_N4
\MCLK|Count|Count:c[9]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[9]~0_combout\ = (\MCLK|Count|Add0~16_combout\ & !\MCLK|Count|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCLK|Count|Add0~16_combout\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[9]~0_combout\);

-- Location: FF_X98_Y14_N5
\MCLK|Count|Count:c[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[9]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[9]~q\);

-- Location: LCCOMB_X99_Y14_N26
\MCLK|Count|Add0~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~18_combout\ = (\MCLK|Count|Count:c[10]~q\ & (!\MCLK|Count|Add0~17\)) # (!\MCLK|Count|Count:c[10]~q\ & ((\MCLK|Count|Add0~17\) # (GND)))
-- \MCLK|Count|Add0~19\ = CARRY((!\MCLK|Count|Add0~17\) # (!\MCLK|Count|Count:c[10]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[10]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~17\,
	combout => \MCLK|Count|Add0~18_combout\,
	cout => \MCLK|Count|Add0~19\);

-- Location: LCCOMB_X98_Y14_N18
\MCLK|Count|Count:c[10]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[10]~0_combout\ = (\MCLK|Count|Add0~18_combout\ & !\MCLK|Count|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Add0~18_combout\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[10]~0_combout\);

-- Location: FF_X98_Y14_N19
\MCLK|Count|Count:c[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[10]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[10]~q\);

-- Location: LCCOMB_X99_Y14_N28
\MCLK|Count|Add0~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~20_combout\ = (\MCLK|Count|Count:c[11]~q\ & (\MCLK|Count|Add0~19\ $ (GND))) # (!\MCLK|Count|Count:c[11]~q\ & (!\MCLK|Count|Add0~19\ & VCC))
-- \MCLK|Count|Add0~21\ = CARRY((\MCLK|Count|Count:c[11]~q\ & !\MCLK|Count|Add0~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[11]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~19\,
	combout => \MCLK|Count|Add0~20_combout\,
	cout => \MCLK|Count|Add0~21\);

-- Location: LCCOMB_X98_Y14_N28
\MCLK|Count|Count:c[11]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[11]~0_combout\ = (\MCLK|Count|Add0~20_combout\ & !\MCLK|Count|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCLK|Count|Add0~20_combout\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[11]~0_combout\);

-- Location: FF_X98_Y14_N29
\MCLK|Count|Count:c[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[11]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[11]~q\);

-- Location: LCCOMB_X99_Y14_N30
\MCLK|Count|Add0~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~22_combout\ = (\MCLK|Count|Count:c[12]~q\ & (!\MCLK|Count|Add0~21\)) # (!\MCLK|Count|Count:c[12]~q\ & ((\MCLK|Count|Add0~21\) # (GND)))
-- \MCLK|Count|Add0~23\ = CARRY((!\MCLK|Count|Add0~21\) # (!\MCLK|Count|Count:c[12]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[12]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~21\,
	combout => \MCLK|Count|Add0~22_combout\,
	cout => \MCLK|Count|Add0~23\);

-- Location: LCCOMB_X98_Y14_N0
\MCLK|Count|Count:c[12]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[12]~0_combout\ = (\MCLK|Count|Equal0~4_combout\ & ((\MCLK|Count|Count:c[12]~q\))) # (!\MCLK|Count|Equal0~4_combout\ & (\MCLK|Count|Add0~22_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Add0~22_combout\,
	datac => \MCLK|Count|Count:c[12]~q\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[12]~0_combout\);

-- Location: FF_X98_Y14_N1
\MCLK|Count|Count:c[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[12]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[12]~q\);

-- Location: LCCOMB_X99_Y13_N0
\MCLK|Count|Add0~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~24_combout\ = (\MCLK|Count|Count:c[13]~q\ & (\MCLK|Count|Add0~23\ $ (GND))) # (!\MCLK|Count|Count:c[13]~q\ & (!\MCLK|Count|Add0~23\ & VCC))
-- \MCLK|Count|Add0~25\ = CARRY((\MCLK|Count|Count:c[13]~q\ & !\MCLK|Count|Add0~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[13]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~23\,
	combout => \MCLK|Count|Add0~24_combout\,
	cout => \MCLK|Count|Add0~25\);

-- Location: LCCOMB_X98_Y13_N20
\MCLK|Count|Count:c[13]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[13]~0_combout\ = (\MCLK|Count|Equal0~4_combout\ & ((\MCLK|Count|Count:c[13]~q\))) # (!\MCLK|Count|Equal0~4_combout\ & (\MCLK|Count|Add0~24_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Add0~24_combout\,
	datac => \MCLK|Count|Count:c[13]~q\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[13]~0_combout\);

-- Location: FF_X98_Y13_N21
\MCLK|Count|Count:c[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[13]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[13]~q\);

-- Location: LCCOMB_X99_Y13_N2
\MCLK|Count|Add0~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~26_combout\ = (\MCLK|Count|Count:c[14]~q\ & (!\MCLK|Count|Add0~25\)) # (!\MCLK|Count|Count:c[14]~q\ & ((\MCLK|Count|Add0~25\) # (GND)))
-- \MCLK|Count|Add0~27\ = CARRY((!\MCLK|Count|Add0~25\) # (!\MCLK|Count|Count:c[14]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[14]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~25\,
	combout => \MCLK|Count|Add0~26_combout\,
	cout => \MCLK|Count|Add0~27\);

-- Location: LCCOMB_X99_Y13_N30
\MCLK|Count|Count:c[14]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[14]~0_combout\ = (\MCLK|Count|Equal0~4_combout\ & ((\MCLK|Count|Count:c[14]~q\))) # (!\MCLK|Count|Equal0~4_combout\ & (\MCLK|Count|Add0~26_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Add0~26_combout\,
	datac => \MCLK|Count|Count:c[14]~q\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[14]~0_combout\);

-- Location: FF_X99_Y13_N31
\MCLK|Count|Count:c[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[14]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[14]~q\);

-- Location: LCCOMB_X99_Y13_N4
\MCLK|Count|Add0~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~28_combout\ = (\MCLK|Count|Count:c[15]~q\ & (\MCLK|Count|Add0~27\ $ (GND))) # (!\MCLK|Count|Count:c[15]~q\ & (!\MCLK|Count|Add0~27\ & VCC))
-- \MCLK|Count|Add0~29\ = CARRY((\MCLK|Count|Count:c[15]~q\ & !\MCLK|Count|Add0~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[15]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~27\,
	combout => \MCLK|Count|Add0~28_combout\,
	cout => \MCLK|Count|Add0~29\);

-- Location: LCCOMB_X98_Y13_N14
\MCLK|Count|Count:c[15]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[15]~0_combout\ = (\MCLK|Count|Equal0~4_combout\ & (\MCLK|Count|Count:c[15]~q\)) # (!\MCLK|Count|Equal0~4_combout\ & ((\MCLK|Count|Add0~28_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Equal0~4_combout\,
	datac => \MCLK|Count|Count:c[15]~q\,
	datad => \MCLK|Count|Add0~28_combout\,
	combout => \MCLK|Count|Count:c[15]~0_combout\);

-- Location: FF_X98_Y13_N15
\MCLK|Count|Count:c[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[15]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[15]~q\);

-- Location: LCCOMB_X99_Y13_N6
\MCLK|Count|Add0~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~30_combout\ = (\MCLK|Count|Count:c[16]~q\ & (!\MCLK|Count|Add0~29\)) # (!\MCLK|Count|Count:c[16]~q\ & ((\MCLK|Count|Add0~29\) # (GND)))
-- \MCLK|Count|Add0~31\ = CARRY((!\MCLK|Count|Add0~29\) # (!\MCLK|Count|Count:c[16]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[16]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~29\,
	combout => \MCLK|Count|Add0~30_combout\,
	cout => \MCLK|Count|Add0~31\);

-- Location: LCCOMB_X99_Y13_N26
\MCLK|Count|Count:c[16]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[16]~0_combout\ = (\MCLK|Count|Add0~30_combout\ & !\MCLK|Count|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Add0~30_combout\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[16]~0_combout\);

-- Location: FF_X99_Y13_N27
\MCLK|Count|Count:c[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[16]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[16]~q\);

-- Location: LCCOMB_X99_Y13_N8
\MCLK|Count|Add0~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~32_combout\ = (\MCLK|Count|Count:c[17]~q\ & (\MCLK|Count|Add0~31\ $ (GND))) # (!\MCLK|Count|Count:c[17]~q\ & (!\MCLK|Count|Add0~31\ & VCC))
-- \MCLK|Count|Add0~33\ = CARRY((\MCLK|Count|Count:c[17]~q\ & !\MCLK|Count|Add0~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[17]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~31\,
	combout => \MCLK|Count|Add0~32_combout\,
	cout => \MCLK|Count|Add0~33\);

-- Location: LCCOMB_X99_Y13_N28
\MCLK|Count|Count:c[17]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[17]~0_combout\ = (\MCLK|Count|Equal0~4_combout\ & ((\MCLK|Count|Count:c[17]~q\))) # (!\MCLK|Count|Equal0~4_combout\ & (\MCLK|Count|Add0~32_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Add0~32_combout\,
	datac => \MCLK|Count|Count:c[17]~q\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|Count:c[17]~0_combout\);

-- Location: FF_X99_Y13_N29
\MCLK|Count|Count:c[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[17]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[17]~q\);

-- Location: LCCOMB_X99_Y13_N10
\MCLK|Count|Add0~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~34_combout\ = (\MCLK|Count|Count:c[18]~q\ & (!\MCLK|Count|Add0~33\)) # (!\MCLK|Count|Count:c[18]~q\ & ((\MCLK|Count|Add0~33\) # (GND)))
-- \MCLK|Count|Add0~35\ = CARRY((!\MCLK|Count|Add0~33\) # (!\MCLK|Count|Count:c[18]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[18]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~33\,
	combout => \MCLK|Count|Add0~34_combout\,
	cout => \MCLK|Count|Add0~35\);

-- Location: LCCOMB_X100_Y13_N26
\MCLK|Count|Count:c[18]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[18]~0_combout\ = (!\MCLK|Count|Equal0~4_combout\ & \MCLK|Count|Add0~34_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCLK|Count|Equal0~4_combout\,
	datad => \MCLK|Count|Add0~34_combout\,
	combout => \MCLK|Count|Count:c[18]~0_combout\);

-- Location: FF_X100_Y13_N27
\MCLK|Count|Count:c[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[18]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[18]~q\);

-- Location: LCCOMB_X99_Y13_N12
\MCLK|Count|Add0~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~36_combout\ = (\MCLK|Count|Count:c[19]~q\ & (\MCLK|Count|Add0~35\ $ (GND))) # (!\MCLK|Count|Count:c[19]~q\ & (!\MCLK|Count|Add0~35\ & VCC))
-- \MCLK|Count|Add0~37\ = CARRY((\MCLK|Count|Count:c[19]~q\ & !\MCLK|Count|Add0~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[19]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~35\,
	combout => \MCLK|Count|Add0~36_combout\,
	cout => \MCLK|Count|Add0~37\);

-- Location: LCCOMB_X98_Y13_N4
\MCLK|Count|Count:c[19]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[19]~0_combout\ = (\MCLK|Count|Equal0~4_combout\ & (\MCLK|Count|Count:c[19]~q\)) # (!\MCLK|Count|Equal0~4_combout\ & ((\MCLK|Count|Add0~36_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Equal0~4_combout\,
	datac => \MCLK|Count|Count:c[19]~q\,
	datad => \MCLK|Count|Add0~36_combout\,
	combout => \MCLK|Count|Count:c[19]~0_combout\);

-- Location: FF_X98_Y13_N5
\MCLK|Count|Count:c[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[19]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[19]~q\);

-- Location: LCCOMB_X99_Y13_N14
\MCLK|Count|Add0~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~38_combout\ = (\MCLK|Count|Count:c[20]~q\ & (!\MCLK|Count|Add0~37\)) # (!\MCLK|Count|Count:c[20]~q\ & ((\MCLK|Count|Add0~37\) # (GND)))
-- \MCLK|Count|Add0~39\ = CARRY((!\MCLK|Count|Add0~37\) # (!\MCLK|Count|Count:c[20]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[20]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~37\,
	combout => \MCLK|Count|Add0~38_combout\,
	cout => \MCLK|Count|Add0~39\);

-- Location: LCCOMB_X98_Y13_N18
\MCLK|Count|Count:c[20]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[20]~0_combout\ = (\MCLK|Count|Equal0~4_combout\ & (\MCLK|Count|Count:c[20]~q\)) # (!\MCLK|Count|Equal0~4_combout\ & ((\MCLK|Count|Add0~38_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Equal0~4_combout\,
	datac => \MCLK|Count|Count:c[20]~q\,
	datad => \MCLK|Count|Add0~38_combout\,
	combout => \MCLK|Count|Count:c[20]~0_combout\);

-- Location: FF_X98_Y13_N19
\MCLK|Count|Count:c[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[20]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[20]~q\);

-- Location: LCCOMB_X99_Y13_N16
\MCLK|Count|Add0~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~40_combout\ = (\MCLK|Count|Count:c[21]~q\ & (\MCLK|Count|Add0~39\ $ (GND))) # (!\MCLK|Count|Count:c[21]~q\ & (!\MCLK|Count|Add0~39\ & VCC))
-- \MCLK|Count|Add0~41\ = CARRY((\MCLK|Count|Count:c[21]~q\ & !\MCLK|Count|Add0~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[21]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~39\,
	combout => \MCLK|Count|Add0~40_combout\,
	cout => \MCLK|Count|Add0~41\);

-- Location: LCCOMB_X98_Y13_N16
\MCLK|Count|Count:c[21]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[21]~0_combout\ = (\MCLK|Count|Equal0~4_combout\ & (\MCLK|Count|Count:c[21]~q\)) # (!\MCLK|Count|Equal0~4_combout\ & ((\MCLK|Count|Add0~40_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Equal0~4_combout\,
	datac => \MCLK|Count|Count:c[21]~q\,
	datad => \MCLK|Count|Add0~40_combout\,
	combout => \MCLK|Count|Count:c[21]~0_combout\);

-- Location: FF_X98_Y13_N17
\MCLK|Count|Count:c[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[21]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[21]~q\);

-- Location: LCCOMB_X99_Y13_N18
\MCLK|Count|Add0~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Add0~42_combout\ = (\MCLK|Count|Count:c[22]~q\ & (!\MCLK|Count|Add0~41\)) # (!\MCLK|Count|Count:c[22]~q\ & ((\MCLK|Count|Add0~41\) # (GND)))
-- \MCLK|Count|Add0~43\ = CARRY((!\MCLK|Count|Add0~41\) # (!\MCLK|Count|Count:c[22]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Count:c[22]~q\,
	datad => VCC,
	cin => \MCLK|Count|Add0~41\,
	combout => \MCLK|Count|Add0~42_combout\,
	cout => \MCLK|Count|Add0~43\);

-- Location: LCCOMB_X98_Y13_N30
\MCLK|Count|Count:c[22]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[22]~0_combout\ = (\MCLK|Count|Equal0~4_combout\ & (\MCLK|Count|Count:c[22]~q\)) # (!\MCLK|Count|Equal0~4_combout\ & ((\MCLK|Count|Add0~42_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Equal0~4_combout\,
	datac => \MCLK|Count|Count:c[22]~q\,
	datad => \MCLK|Count|Add0~42_combout\,
	combout => \MCLK|Count|Count:c[22]~0_combout\);

-- Location: FF_X98_Y13_N31
\MCLK|Count|Count:c[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[22]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[22]~q\);

-- Location: LCCOMB_X100_Y13_N4
\MCLK|Count|Count:c[23]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Count:c[23]~0_combout\ = (\MCLK|Count|Equal0~4_combout\ & (\MCLK|Count|Count:c[23]~q\)) # (!\MCLK|Count|Equal0~4_combout\ & ((\MCLK|Count|Add0~44_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Count|Equal0~4_combout\,
	datac => \MCLK|Count|Count:c[23]~q\,
	datad => \MCLK|Count|Add0~44_combout\,
	combout => \MCLK|Count|Count:c[23]~0_combout\);

-- Location: FF_X100_Y13_N5
\MCLK|Count|Count:c[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|Count:c[23]~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|Count:c[23]~q\);

-- Location: LCCOMB_X97_Y13_N16
\MCLK|Count|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Equal0~0_combout\ = (\MCLK|Count|Count:c[23]~q\ & (!\MCLK|Count|Count:c[24]~q\ & (!\MCLK|Count|Count:c[18]~q\ & \MCLK|Count|Count:c[25]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[23]~q\,
	datab => \MCLK|Count|Count:c[24]~q\,
	datac => \MCLK|Count|Count:c[18]~q\,
	datad => \MCLK|Count|Count:c[25]~q\,
	combout => \MCLK|Count|Equal0~0_combout\);

-- Location: LCCOMB_X98_Y13_N0
\MCLK|Count|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Equal0~1_combout\ = (\MCLK|Count|Count:c[15]~q\ & (\MCLK|Count|Count:c[17]~q\ & (!\MCLK|Count|Count:c[11]~q\ & !\MCLK|Count|Count:c[16]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[15]~q\,
	datab => \MCLK|Count|Count:c[17]~q\,
	datac => \MCLK|Count|Count:c[11]~q\,
	datad => \MCLK|Count|Count:c[16]~q\,
	combout => \MCLK|Count|Equal0~1_combout\);

-- Location: LCCOMB_X98_Y14_N30
\MCLK|Count|Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Equal0~2_combout\ = (!\MCLK|Count|Count:c[3]~q\ & (!\MCLK|Count|Count:c[4]~q\ & (!\MCLK|Count|Count:c[5]~q\ & \MCLK|Count|Count:c[7]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[3]~q\,
	datab => \MCLK|Count|Count:c[4]~q\,
	datac => \MCLK|Count|Count:c[5]~q\,
	datad => \MCLK|Count|Count:c[7]~q\,
	combout => \MCLK|Count|Equal0~2_combout\);

-- Location: LCCOMB_X98_Y13_N6
\MCLK|Count|Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Equal0~3_combout\ = (\MCLK|Count|Count:c[0]~q\ & (!\MCLK|Count|Count:c[1]~q\ & (\MCLK|Count|Equal0~2_combout\ & !\MCLK|Count|Count:c[2]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Count:c[0]~q\,
	datab => \MCLK|Count|Count:c[1]~q\,
	datac => \MCLK|Count|Equal0~2_combout\,
	datad => \MCLK|Count|Count:c[2]~q\,
	combout => \MCLK|Count|Equal0~3_combout\);

-- Location: LCCOMB_X98_Y13_N12
\MCLK|Count|Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|Equal0~4_combout\ = (\MCLK|Count|Equal0~0_combout\ & (\MCLK|Count|Equal0~1_combout\ & (\MCLK|Equal0~3_combout\ & \MCLK|Count|Equal0~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Count|Equal0~0_combout\,
	datab => \MCLK|Count|Equal0~1_combout\,
	datac => \MCLK|Equal0~3_combout\,
	datad => \MCLK|Count|Equal0~3_combout\,
	combout => \MCLK|Count|Equal0~4_combout\);

-- Location: LCCOMB_X98_Y13_N24
\MCLK|Count|s_ov~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Count|s_ov~0_combout\ = (\MCLK|Count|s_ov~q\) # (\MCLK|Count|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCLK|Count|s_ov~q\,
	datad => \MCLK|Count|Equal0~4_combout\,
	combout => \MCLK|Count|s_ov~0_combout\);

-- Location: FF_X98_Y13_N25
\MCLK|Count|s_ov\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Count|s_ov~0_combout\,
	clrn => \MCLK|ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Count|s_ov~q\);

-- Location: LCCOMB_X97_Y14_N30
\MCLK|Buff|qn~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Buff|qn~0_combout\ = (\MCLK|s_qe~q\ & (!\MCLK|Buff|s_q~combout\)) # (!\MCLK|s_qe~q\ & ((\MCLK|Buff|qn~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Buff|s_q~combout\,
	datac => \MCLK|s_qe~q\,
	datad => \MCLK|Buff|qn~q\,
	combout => \MCLK|Buff|qn~0_combout\);

-- Location: LCCOMB_X97_Y14_N24
\MCLK|Buff|qn~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Buff|qn~feeder_combout\ = \MCLK|Buff|qn~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCLK|Buff|qn~0_combout\,
	combout => \MCLK|Buff|qn~feeder_combout\);

-- Location: LCCOMB_X98_Y14_N26
\MCLK|Buff|qn~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Buff|qn~1_combout\ = (\MCLK|s_qe~q\ & !\nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCLK|s_qe~q\,
	datad => \nRST~input_o\,
	combout => \MCLK|Buff|qn~1_combout\);

-- Location: FF_X97_Y14_N25
\MCLK|Buff|qn\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \MCLK|Buff|qn~feeder_combout\,
	clrn => \MCLK|Buff|ALT_INV_qn~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCLK|Buff|qn~q\);

-- Location: LCCOMB_X97_Y14_N6
\MCLK|Buff|s_q\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCLK|Buff|s_q~combout\ = (\MCLK|s_qe~q\ & ((\MCLK|Buff|qn~q\))) # (!\MCLK|s_qe~q\ & (\MCLK|Buff|s_q~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCLK|Buff|s_q~combout\,
	datac => \MCLK|s_qe~q\,
	datad => \MCLK|Buff|qn~q\,
	combout => \MCLK|Buff|s_q~combout\);

-- Location: CLKCTRL_G7
\MCLK|Buff|s_q~clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \MCLK|Buff|s_q~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \MCLK|Buff|s_q~clkctrl_outclk\);

-- Location: LCCOMB_X110_Y19_N4
\MCOUNT|Count:c[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[0]~0_combout\ = (\MCOUNT|Equal0~4_combout\) # (!\MCOUNT|Count:c[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCOUNT|Count:c[0]~q\,
	datad => \MCOUNT|Equal0~4_combout\,
	combout => \MCOUNT|Count:c[0]~0_combout\);

-- Location: FF_X110_Y19_N5
\MCOUNT|Count:c[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[0]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[0]~q\);

-- Location: LCCOMB_X109_Y19_N2
\MCOUNT|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~0_combout\ = (\MCOUNT|Count:c[0]~q\ & (\MCOUNT|Count:c[1]~q\ $ (VCC))) # (!\MCOUNT|Count:c[0]~q\ & (\MCOUNT|Count:c[1]~q\ & VCC))
-- \MCOUNT|Add0~1\ = CARRY((\MCOUNT|Count:c[0]~q\ & \MCOUNT|Count:c[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[0]~q\,
	datab => \MCOUNT|Count:c[1]~q\,
	datad => VCC,
	combout => \MCOUNT|Add0~0_combout\,
	cout => \MCOUNT|Add0~1\);

-- Location: LCCOMB_X108_Y19_N6
\MCOUNT|Count:c[1]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[1]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Count:c[1]~q\))) # (!\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Add0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Add0~0_combout\,
	datac => \MCOUNT|Count:c[1]~q\,
	datad => \MCOUNT|Equal0~4_combout\,
	combout => \MCOUNT|Count:c[1]~0_combout\);

-- Location: FF_X108_Y19_N7
\MCOUNT|Count:c[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[1]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[1]~q\);

-- Location: LCCOMB_X109_Y19_N4
\MCOUNT|Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~2_combout\ = (\MCOUNT|Count:c[2]~q\ & (!\MCOUNT|Add0~1\)) # (!\MCOUNT|Count:c[2]~q\ & ((\MCOUNT|Add0~1\) # (GND)))
-- \MCOUNT|Add0~3\ = CARRY((!\MCOUNT|Add0~1\) # (!\MCOUNT|Count:c[2]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[2]~q\,
	datad => VCC,
	cin => \MCOUNT|Add0~1\,
	combout => \MCOUNT|Add0~2_combout\,
	cout => \MCOUNT|Add0~3\);

-- Location: LCCOMB_X108_Y19_N16
\MCOUNT|Count:c[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[2]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Count:c[2]~q\))) # (!\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Add0~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Add0~2_combout\,
	datac => \MCOUNT|Count:c[2]~q\,
	datad => \MCOUNT|Equal0~4_combout\,
	combout => \MCOUNT|Count:c[2]~0_combout\);

-- Location: FF_X108_Y19_N17
\MCOUNT|Count:c[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[2]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[2]~q\);

-- Location: LCCOMB_X109_Y19_N6
\MCOUNT|Add0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~4_combout\ = (\MCOUNT|Count:c[3]~q\ & (\MCOUNT|Add0~3\ $ (GND))) # (!\MCOUNT|Count:c[3]~q\ & (!\MCOUNT|Add0~3\ & VCC))
-- \MCOUNT|Add0~5\ = CARRY((\MCOUNT|Count:c[3]~q\ & !\MCOUNT|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[3]~q\,
	datad => VCC,
	cin => \MCOUNT|Add0~3\,
	combout => \MCOUNT|Add0~4_combout\,
	cout => \MCOUNT|Add0~5\);

-- Location: LCCOMB_X108_Y19_N14
\MCOUNT|Count:c[3]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[3]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Count:c[3]~q\))) # (!\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Add0~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Add0~4_combout\,
	datac => \MCOUNT|Count:c[3]~q\,
	datad => \MCOUNT|Equal0~4_combout\,
	combout => \MCOUNT|Count:c[3]~0_combout\);

-- Location: FF_X108_Y19_N15
\MCOUNT|Count:c[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[3]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[3]~q\);

-- Location: LCCOMB_X109_Y19_N8
\MCOUNT|Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~6_combout\ = (\MCOUNT|Count:c[4]~q\ & (!\MCOUNT|Add0~5\)) # (!\MCOUNT|Count:c[4]~q\ & ((\MCOUNT|Add0~5\) # (GND)))
-- \MCOUNT|Add0~7\ = CARRY((!\MCOUNT|Add0~5\) # (!\MCOUNT|Count:c[4]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[4]~q\,
	datad => VCC,
	cin => \MCOUNT|Add0~5\,
	combout => \MCOUNT|Add0~6_combout\,
	cout => \MCOUNT|Add0~7\);

-- Location: LCCOMB_X110_Y19_N30
\MCOUNT|Count:c[4]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[4]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Count:c[4]~q\)) # (!\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Add0~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Equal0~4_combout\,
	datac => \MCOUNT|Count:c[4]~q\,
	datad => \MCOUNT|Add0~6_combout\,
	combout => \MCOUNT|Count:c[4]~0_combout\);

-- Location: FF_X110_Y19_N31
\MCOUNT|Count:c[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[4]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[4]~q\);

-- Location: LCCOMB_X109_Y19_N10
\MCOUNT|Add0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~8_combout\ = (\MCOUNT|Count:c[5]~q\ & (\MCOUNT|Add0~7\ $ (GND))) # (!\MCOUNT|Count:c[5]~q\ & (!\MCOUNT|Add0~7\ & VCC))
-- \MCOUNT|Add0~9\ = CARRY((\MCOUNT|Count:c[5]~q\ & !\MCOUNT|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCOUNT|Count:c[5]~q\,
	datad => VCC,
	cin => \MCOUNT|Add0~7\,
	combout => \MCOUNT|Add0~8_combout\,
	cout => \MCOUNT|Add0~9\);

-- Location: LCCOMB_X110_Y19_N0
\MCOUNT|Count:c[5]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[5]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Count:c[5]~q\)) # (!\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Add0~8_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Equal0~4_combout\,
	datac => \MCOUNT|Count:c[5]~q\,
	datad => \MCOUNT|Add0~8_combout\,
	combout => \MCOUNT|Count:c[5]~0_combout\);

-- Location: FF_X110_Y19_N1
\MCOUNT|Count:c[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[5]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[5]~q\);

-- Location: LCCOMB_X109_Y19_N12
\MCOUNT|Add0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~10_combout\ = (\MCOUNT|Count:c[6]~q\ & (!\MCOUNT|Add0~9\)) # (!\MCOUNT|Count:c[6]~q\ & ((\MCOUNT|Add0~9\) # (GND)))
-- \MCOUNT|Add0~11\ = CARRY((!\MCOUNT|Add0~9\) # (!\MCOUNT|Count:c[6]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCOUNT|Count:c[6]~q\,
	datad => VCC,
	cin => \MCOUNT|Add0~9\,
	combout => \MCOUNT|Add0~10_combout\,
	cout => \MCOUNT|Add0~11\);

-- Location: LCCOMB_X110_Y19_N22
\MCOUNT|Count:c[6]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[6]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Count:c[6]~q\)) # (!\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Add0~10_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Equal0~4_combout\,
	datac => \MCOUNT|Count:c[6]~q\,
	datad => \MCOUNT|Add0~10_combout\,
	combout => \MCOUNT|Count:c[6]~0_combout\);

-- Location: FF_X110_Y19_N23
\MCOUNT|Count:c[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[6]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[6]~q\);

-- Location: LCCOMB_X109_Y19_N14
\MCOUNT|Add0~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~12_combout\ = (\MCOUNT|Count:c[7]~q\ & (\MCOUNT|Add0~11\ $ (GND))) # (!\MCOUNT|Count:c[7]~q\ & (!\MCOUNT|Add0~11\ & VCC))
-- \MCOUNT|Add0~13\ = CARRY((\MCOUNT|Count:c[7]~q\ & !\MCOUNT|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[7]~q\,
	datad => VCC,
	cin => \MCOUNT|Add0~11\,
	combout => \MCOUNT|Add0~12_combout\,
	cout => \MCOUNT|Add0~13\);

-- Location: LCCOMB_X110_Y19_N24
\MCOUNT|Count:c[7]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[7]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Count:c[7]~q\)) # (!\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Add0~12_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Equal0~4_combout\,
	datac => \MCOUNT|Count:c[7]~q\,
	datad => \MCOUNT|Add0~12_combout\,
	combout => \MCOUNT|Count:c[7]~0_combout\);

-- Location: FF_X110_Y19_N25
\MCOUNT|Count:c[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[7]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[7]~q\);

-- Location: LCCOMB_X110_Y19_N12
\MCOUNT|Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Equal0~2_combout\ = (\MCOUNT|Count:c[4]~q\ & (\MCOUNT|Count:c[5]~q\ & (\MCOUNT|Count:c[6]~q\ & \MCOUNT|Count:c[7]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[4]~q\,
	datab => \MCOUNT|Count:c[5]~q\,
	datac => \MCOUNT|Count:c[6]~q\,
	datad => \MCOUNT|Count:c[7]~q\,
	combout => \MCOUNT|Equal0~2_combout\);

-- Location: LCCOMB_X109_Y19_N16
\MCOUNT|Add0~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~14_combout\ = (\MCOUNT|Count:c[8]~q\ & (!\MCOUNT|Add0~13\)) # (!\MCOUNT|Count:c[8]~q\ & ((\MCOUNT|Add0~13\) # (GND)))
-- \MCOUNT|Add0~15\ = CARRY((!\MCOUNT|Add0~13\) # (!\MCOUNT|Count:c[8]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCOUNT|Count:c[8]~q\,
	datad => VCC,
	cin => \MCOUNT|Add0~13\,
	combout => \MCOUNT|Add0~14_combout\,
	cout => \MCOUNT|Add0~15\);

-- Location: LCCOMB_X110_Y19_N14
\MCOUNT|Count:c[8]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[8]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Count:c[8]~q\))) # (!\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Add0~14_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Add0~14_combout\,
	datac => \MCOUNT|Count:c[8]~q\,
	datad => \MCOUNT|Equal0~4_combout\,
	combout => \MCOUNT|Count:c[8]~0_combout\);

-- Location: FF_X110_Y19_N15
\MCOUNT|Count:c[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[8]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[8]~q\);

-- Location: LCCOMB_X109_Y19_N18
\MCOUNT|Add0~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~16_combout\ = (\MCOUNT|Count:c[9]~q\ & (\MCOUNT|Add0~15\ $ (GND))) # (!\MCOUNT|Count:c[9]~q\ & (!\MCOUNT|Add0~15\ & VCC))
-- \MCOUNT|Add0~17\ = CARRY((\MCOUNT|Count:c[9]~q\ & !\MCOUNT|Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCOUNT|Count:c[9]~q\,
	datad => VCC,
	cin => \MCOUNT|Add0~15\,
	combout => \MCOUNT|Add0~16_combout\,
	cout => \MCOUNT|Add0~17\);

-- Location: LCCOMB_X110_Y19_N8
\MCOUNT|Count:c[9]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[9]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Count:c[9]~q\)) # (!\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Add0~16_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Equal0~4_combout\,
	datac => \MCOUNT|Count:c[9]~q\,
	datad => \MCOUNT|Add0~16_combout\,
	combout => \MCOUNT|Count:c[9]~0_combout\);

-- Location: FF_X110_Y19_N9
\MCOUNT|Count:c[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[9]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[9]~q\);

-- Location: LCCOMB_X109_Y19_N20
\MCOUNT|Add0~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~18_combout\ = (\MCOUNT|Count:c[10]~q\ & (!\MCOUNT|Add0~17\)) # (!\MCOUNT|Count:c[10]~q\ & ((\MCOUNT|Add0~17\) # (GND)))
-- \MCOUNT|Add0~19\ = CARRY((!\MCOUNT|Add0~17\) # (!\MCOUNT|Count:c[10]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCOUNT|Count:c[10]~q\,
	datad => VCC,
	cin => \MCOUNT|Add0~17\,
	combout => \MCOUNT|Add0~18_combout\,
	cout => \MCOUNT|Add0~19\);

-- Location: LCCOMB_X110_Y19_N10
\MCOUNT|Count:c[10]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[10]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Count:c[10]~q\)) # (!\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Add0~18_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Equal0~4_combout\,
	datac => \MCOUNT|Count:c[10]~q\,
	datad => \MCOUNT|Add0~18_combout\,
	combout => \MCOUNT|Count:c[10]~0_combout\);

-- Location: FF_X110_Y19_N11
\MCOUNT|Count:c[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[10]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[10]~q\);

-- Location: LCCOMB_X109_Y19_N22
\MCOUNT|Add0~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~20_combout\ = (\MCOUNT|Count:c[11]~q\ & (\MCOUNT|Add0~19\ $ (GND))) # (!\MCOUNT|Count:c[11]~q\ & (!\MCOUNT|Add0~19\ & VCC))
-- \MCOUNT|Add0~21\ = CARRY((\MCOUNT|Count:c[11]~q\ & !\MCOUNT|Add0~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[11]~q\,
	datad => VCC,
	cin => \MCOUNT|Add0~19\,
	combout => \MCOUNT|Add0~20_combout\,
	cout => \MCOUNT|Add0~21\);

-- Location: LCCOMB_X110_Y19_N20
\MCOUNT|Count:c[11]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[11]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Count:c[11]~q\)) # (!\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Add0~20_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Equal0~4_combout\,
	datac => \MCOUNT|Count:c[11]~q\,
	datad => \MCOUNT|Add0~20_combout\,
	combout => \MCOUNT|Count:c[11]~0_combout\);

-- Location: FF_X110_Y19_N21
\MCOUNT|Count:c[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[11]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[11]~q\);

-- Location: LCCOMB_X110_Y19_N2
\MCOUNT|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Equal0~1_combout\ = (\MCOUNT|Count:c[10]~q\ & (\MCOUNT|Count:c[8]~q\ & (\MCOUNT|Count:c[9]~q\ & \MCOUNT|Count:c[11]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[10]~q\,
	datab => \MCOUNT|Count:c[8]~q\,
	datac => \MCOUNT|Count:c[9]~q\,
	datad => \MCOUNT|Count:c[11]~q\,
	combout => \MCOUNT|Equal0~1_combout\);

-- Location: LCCOMB_X108_Y19_N30
\MCOUNT|Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Equal0~3_combout\ = (\MCOUNT|Count:c[1]~q\ & (\MCOUNT|Count:c[3]~q\ & (\MCOUNT|Count:c[0]~q\ & \MCOUNT|Count:c[2]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[1]~q\,
	datab => \MCOUNT|Count:c[3]~q\,
	datac => \MCOUNT|Count:c[0]~q\,
	datad => \MCOUNT|Count:c[2]~q\,
	combout => \MCOUNT|Equal0~3_combout\);

-- Location: LCCOMB_X109_Y19_N24
\MCOUNT|Add0~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~22_combout\ = (\MCOUNT|Count:c[12]~q\ & (!\MCOUNT|Add0~21\)) # (!\MCOUNT|Count:c[12]~q\ & ((\MCOUNT|Add0~21\) # (GND)))
-- \MCOUNT|Add0~23\ = CARRY((!\MCOUNT|Add0~21\) # (!\MCOUNT|Count:c[12]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCOUNT|Count:c[12]~q\,
	datad => VCC,
	cin => \MCOUNT|Add0~21\,
	combout => \MCOUNT|Add0~22_combout\,
	cout => \MCOUNT|Add0~23\);

-- Location: LCCOMB_X110_Y19_N18
\MCOUNT|Count:c[12]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[12]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Count:c[12]~q\)) # (!\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Add0~22_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Equal0~4_combout\,
	datac => \MCOUNT|Count:c[12]~q\,
	datad => \MCOUNT|Add0~22_combout\,
	combout => \MCOUNT|Count:c[12]~0_combout\);

-- Location: FF_X110_Y19_N19
\MCOUNT|Count:c[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[12]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[12]~q\);

-- Location: LCCOMB_X109_Y19_N26
\MCOUNT|Add0~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~24_combout\ = (\MCOUNT|Count:c[13]~q\ & (\MCOUNT|Add0~23\ $ (GND))) # (!\MCOUNT|Count:c[13]~q\ & (!\MCOUNT|Add0~23\ & VCC))
-- \MCOUNT|Add0~25\ = CARRY((\MCOUNT|Count:c[13]~q\ & !\MCOUNT|Add0~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCOUNT|Count:c[13]~q\,
	datad => VCC,
	cin => \MCOUNT|Add0~23\,
	combout => \MCOUNT|Add0~24_combout\,
	cout => \MCOUNT|Add0~25\);

-- Location: LCCOMB_X110_Y19_N28
\MCOUNT|Count:c[13]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[13]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Count:c[13]~q\))) # (!\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Add0~24_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Add0~24_combout\,
	datac => \MCOUNT|Count:c[13]~q\,
	datad => \MCOUNT|Equal0~4_combout\,
	combout => \MCOUNT|Count:c[13]~0_combout\);

-- Location: FF_X110_Y19_N29
\MCOUNT|Count:c[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[13]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[13]~q\);

-- Location: LCCOMB_X109_Y19_N28
\MCOUNT|Add0~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~26_combout\ = (\MCOUNT|Count:c[14]~q\ & (!\MCOUNT|Add0~25\)) # (!\MCOUNT|Count:c[14]~q\ & ((\MCOUNT|Add0~25\) # (GND)))
-- \MCOUNT|Add0~27\ = CARRY((!\MCOUNT|Add0~25\) # (!\MCOUNT|Count:c[14]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \MCOUNT|Count:c[14]~q\,
	datad => VCC,
	cin => \MCOUNT|Add0~25\,
	combout => \MCOUNT|Add0~26_combout\,
	cout => \MCOUNT|Add0~27\);

-- Location: LCCOMB_X109_Y19_N0
\MCOUNT|Count:c[14]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[14]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Count:c[14]~q\))) # (!\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Add0~26_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \MCOUNT|Add0~26_combout\,
	datac => \MCOUNT|Count:c[14]~q\,
	datad => \MCOUNT|Equal0~4_combout\,
	combout => \MCOUNT|Count:c[14]~0_combout\);

-- Location: FF_X109_Y19_N1
\MCOUNT|Count:c[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[14]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[14]~q\);

-- Location: LCCOMB_X109_Y19_N30
\MCOUNT|Add0~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Add0~28_combout\ = \MCOUNT|Add0~27\ $ (!\MCOUNT|Count:c[15]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \MCOUNT|Count:c[15]~q\,
	cin => \MCOUNT|Add0~27\,
	combout => \MCOUNT|Add0~28_combout\);

-- Location: LCCOMB_X110_Y19_N6
\MCOUNT|Count:c[15]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Count:c[15]~0_combout\ = (\MCOUNT|Equal0~4_combout\ & (\MCOUNT|Count:c[15]~q\)) # (!\MCOUNT|Equal0~4_combout\ & ((\MCOUNT|Add0~28_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Equal0~4_combout\,
	datac => \MCOUNT|Count:c[15]~q\,
	datad => \MCOUNT|Add0~28_combout\,
	combout => \MCOUNT|Count:c[15]~0_combout\);

-- Location: FF_X110_Y19_N7
\MCOUNT|Count:c[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|Count:c[15]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|Count:c[15]~q\);

-- Location: LCCOMB_X110_Y19_N16
\MCOUNT|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Equal0~0_combout\ = (\MCOUNT|Count:c[15]~q\ & (\MCOUNT|Count:c[12]~q\ & (\MCOUNT|Count:c[14]~q\ & \MCOUNT|Count:c[13]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[15]~q\,
	datab => \MCOUNT|Count:c[12]~q\,
	datac => \MCOUNT|Count:c[14]~q\,
	datad => \MCOUNT|Count:c[13]~q\,
	combout => \MCOUNT|Equal0~0_combout\);

-- Location: LCCOMB_X110_Y19_N26
\MCOUNT|Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|Equal0~4_combout\ = (\MCOUNT|Equal0~2_combout\ & (\MCOUNT|Equal0~1_combout\ & (\MCOUNT|Equal0~3_combout\ & \MCOUNT|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Equal0~2_combout\,
	datab => \MCOUNT|Equal0~1_combout\,
	datac => \MCOUNT|Equal0~3_combout\,
	datad => \MCOUNT|Equal0~0_combout\,
	combout => \MCOUNT|Equal0~4_combout\);

-- Location: LCCOMB_X108_Y19_N8
\MCOUNT|s_ov~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \MCOUNT|s_ov~0_combout\ = (\MCOUNT|s_ov~q\) # (\MCOUNT|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCOUNT|s_ov~q\,
	datad => \MCOUNT|Equal0~4_combout\,
	combout => \MCOUNT|s_ov~0_combout\);

-- Location: FF_X108_Y19_N9
\MCOUNT|s_ov\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MCLK|Buff|s_q~clkctrl_outclk\,
	d => \MCOUNT|s_ov~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \MCOUNT|s_ov~q\);

-- Location: LCCOMB_X98_Y19_N16
\Error~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Error~0_combout\ = (\MCLK|Count|s_ov~q\) # (\MCOUNT|s_ov~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \MCLK|Count|s_ov~q\,
	datad => \MCOUNT|s_ov~q\,
	combout => \Error~0_combout\);

-- Location: LCCOMB_X107_Y19_N8
\DAT0|Mux6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux6~0_combout\ = (\MCOUNT|Count:c[2]~q\ & (!\MCOUNT|Count:c[1]~q\ & (\MCOUNT|Count:c[0]~q\ $ (!\MCOUNT|Count:c[3]~q\)))) # (!\MCOUNT|Count:c[2]~q\ & (\MCOUNT|Count:c[0]~q\ & (\MCOUNT|Count:c[3]~q\ $ (!\MCOUNT|Count:c[1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[0]~q\,
	datab => \MCOUNT|Count:c[2]~q\,
	datac => \MCOUNT|Count:c[3]~q\,
	datad => \MCOUNT|Count:c[1]~q\,
	combout => \DAT0|Mux6~0_combout\);

-- Location: LCCOMB_X107_Y19_N18
\DAT0|Mux6~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux6~1_combout\ = (\DAT0|Mux6~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DAT0|Mux6~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT0|Mux6~1_combout\);

-- Location: LCCOMB_X107_Y19_N24
\DAT0|Mux5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux5~0_combout\ = (\MCOUNT|Count:c[3]~q\ & ((\MCOUNT|Count:c[0]~q\ & ((\MCOUNT|Count:c[1]~q\))) # (!\MCOUNT|Count:c[0]~q\ & (\MCOUNT|Count:c[2]~q\)))) # (!\MCOUNT|Count:c[3]~q\ & (\MCOUNT|Count:c[2]~q\ & (\MCOUNT|Count:c[0]~q\ $ 
-- (\MCOUNT|Count:c[1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[0]~q\,
	datab => \MCOUNT|Count:c[2]~q\,
	datac => \MCOUNT|Count:c[3]~q\,
	datad => \MCOUNT|Count:c[1]~q\,
	combout => \DAT0|Mux5~0_combout\);

-- Location: LCCOMB_X107_Y39_N16
\DAT0|Mux5~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux5~1_combout\ = (\DAT0|Mux5~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT0|Mux5~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT0|Mux5~1_combout\);

-- Location: LCCOMB_X107_Y19_N30
\DAT0|Mux4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux4~0_combout\ = (\MCOUNT|Count:c[2]~q\ & (\MCOUNT|Count:c[3]~q\ & ((\MCOUNT|Count:c[1]~q\) # (!\MCOUNT|Count:c[0]~q\)))) # (!\MCOUNT|Count:c[2]~q\ & (!\MCOUNT|Count:c[0]~q\ & (!\MCOUNT|Count:c[3]~q\ & \MCOUNT|Count:c[1]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[0]~q\,
	datab => \MCOUNT|Count:c[2]~q\,
	datac => \MCOUNT|Count:c[3]~q\,
	datad => \MCOUNT|Count:c[1]~q\,
	combout => \DAT0|Mux4~0_combout\);

-- Location: LCCOMB_X107_Y19_N12
\DAT0|Mux4~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux4~1_combout\ = (\DAT0|Mux4~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DAT0|Mux4~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT0|Mux4~1_combout\);

-- Location: LCCOMB_X108_Y19_N28
\DAT0|Mux3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux3~0_combout\ = (\MCOUNT|Count:c[1]~q\ & ((\MCOUNT|Count:c[0]~q\ & ((\MCOUNT|Count:c[2]~q\))) # (!\MCOUNT|Count:c[0]~q\ & (\MCOUNT|Count:c[3]~q\ & !\MCOUNT|Count:c[2]~q\)))) # (!\MCOUNT|Count:c[1]~q\ & (!\MCOUNT|Count:c[3]~q\ & 
-- (\MCOUNT|Count:c[0]~q\ $ (\MCOUNT|Count:c[2]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100100100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[1]~q\,
	datab => \MCOUNT|Count:c[0]~q\,
	datac => \MCOUNT|Count:c[3]~q\,
	datad => \MCOUNT|Count:c[2]~q\,
	combout => \DAT0|Mux3~0_combout\);

-- Location: LCCOMB_X112_Y19_N16
\DAT0|Mux3~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux3~1_combout\ = (\DAT0|Mux3~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DAT0|Mux3~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT0|Mux3~1_combout\);

-- Location: LCCOMB_X108_Y19_N10
\DAT0|Mux2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux2~0_combout\ = (\MCOUNT|Count:c[1]~q\ & (\MCOUNT|Count:c[0]~q\ & (!\MCOUNT|Count:c[3]~q\))) # (!\MCOUNT|Count:c[1]~q\ & ((\MCOUNT|Count:c[2]~q\ & ((!\MCOUNT|Count:c[3]~q\))) # (!\MCOUNT|Count:c[2]~q\ & (\MCOUNT|Count:c[0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110101001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[1]~q\,
	datab => \MCOUNT|Count:c[0]~q\,
	datac => \MCOUNT|Count:c[3]~q\,
	datad => \MCOUNT|Count:c[2]~q\,
	combout => \DAT0|Mux2~0_combout\);

-- Location: LCCOMB_X108_Y19_N0
\DAT0|Mux2~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux2~1_combout\ = (\DAT0|Mux2~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DAT0|Mux2~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT0|Mux2~1_combout\);

-- Location: LCCOMB_X108_Y19_N26
\DAT0|Mux1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux1~0_combout\ = (\MCOUNT|Count:c[1]~q\ & (!\MCOUNT|Count:c[3]~q\ & ((\MCOUNT|Count:c[0]~q\) # (!\MCOUNT|Count:c[2]~q\)))) # (!\MCOUNT|Count:c[1]~q\ & (\MCOUNT|Count:c[0]~q\ & (\MCOUNT|Count:c[3]~q\ $ (!\MCOUNT|Count:c[2]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[1]~q\,
	datab => \MCOUNT|Count:c[0]~q\,
	datac => \MCOUNT|Count:c[3]~q\,
	datad => \MCOUNT|Count:c[2]~q\,
	combout => \DAT0|Mux1~0_combout\);

-- Location: LCCOMB_X108_Y19_N24
\DAT0|Mux1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux1~1_combout\ = (\DAT0|Mux1~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DAT0|Mux1~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT0|Mux1~1_combout\);

-- Location: LCCOMB_X108_Y19_N22
\DAT0|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux0~0_combout\ = (\MCOUNT|Count:c[0]~q\ & ((\MCOUNT|Count:c[3]~q\) # (\MCOUNT|Count:c[1]~q\ $ (\MCOUNT|Count:c[2]~q\)))) # (!\MCOUNT|Count:c[0]~q\ & ((\MCOUNT|Count:c[1]~q\) # (\MCOUNT|Count:c[3]~q\ $ (\MCOUNT|Count:c[2]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011111111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[1]~q\,
	datab => \MCOUNT|Count:c[0]~q\,
	datac => \MCOUNT|Count:c[3]~q\,
	datad => \MCOUNT|Count:c[2]~q\,
	combout => \DAT0|Mux0~0_combout\);

-- Location: LCCOMB_X108_Y19_N12
\DAT0|Mux0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT0|Mux0~1_combout\ = (!\DAT0|Mux0~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DAT0|Mux0~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT0|Mux0~1_combout\);

-- Location: LCCOMB_X111_Y19_N16
\DAT1|Mux6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux6~0_combout\ = (\MCOUNT|Count:c[7]~q\ & (\MCOUNT|Count:c[4]~q\ & (\MCOUNT|Count:c[6]~q\ $ (\MCOUNT|Count:c[5]~q\)))) # (!\MCOUNT|Count:c[7]~q\ & (!\MCOUNT|Count:c[5]~q\ & (\MCOUNT|Count:c[6]~q\ $ (\MCOUNT|Count:c[4]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[7]~q\,
	datab => \MCOUNT|Count:c[6]~q\,
	datac => \MCOUNT|Count:c[5]~q\,
	datad => \MCOUNT|Count:c[4]~q\,
	combout => \DAT1|Mux6~0_combout\);

-- Location: LCCOMB_X112_Y19_N18
\DAT1|Mux6~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux6~1_combout\ = (\DAT1|Mux6~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DAT1|Mux6~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT1|Mux6~1_combout\);

-- Location: LCCOMB_X111_Y19_N26
\DAT1|Mux5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux5~0_combout\ = (\MCOUNT|Count:c[7]~q\ & ((\MCOUNT|Count:c[4]~q\ & ((\MCOUNT|Count:c[5]~q\))) # (!\MCOUNT|Count:c[4]~q\ & (\MCOUNT|Count:c[6]~q\)))) # (!\MCOUNT|Count:c[7]~q\ & (\MCOUNT|Count:c[6]~q\ & (\MCOUNT|Count:c[5]~q\ $ 
-- (\MCOUNT|Count:c[4]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[7]~q\,
	datab => \MCOUNT|Count:c[6]~q\,
	datac => \MCOUNT|Count:c[5]~q\,
	datad => \MCOUNT|Count:c[4]~q\,
	combout => \DAT1|Mux5~0_combout\);

-- Location: LCCOMB_X111_Y19_N20
\DAT1|Mux5~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux5~1_combout\ = (\DAT1|Mux5~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DAT1|Mux5~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT1|Mux5~1_combout\);

-- Location: LCCOMB_X111_Y19_N14
\DAT1|Mux4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux4~0_combout\ = (\MCOUNT|Count:c[7]~q\ & (\MCOUNT|Count:c[6]~q\ & ((\MCOUNT|Count:c[5]~q\) # (!\MCOUNT|Count:c[4]~q\)))) # (!\MCOUNT|Count:c[7]~q\ & (!\MCOUNT|Count:c[6]~q\ & (\MCOUNT|Count:c[5]~q\ & !\MCOUNT|Count:c[4]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[7]~q\,
	datab => \MCOUNT|Count:c[6]~q\,
	datac => \MCOUNT|Count:c[5]~q\,
	datad => \MCOUNT|Count:c[4]~q\,
	combout => \DAT1|Mux4~0_combout\);

-- Location: LCCOMB_X112_Y19_N4
\DAT1|Mux4~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux4~1_combout\ = (\DAT1|Mux4~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DAT1|Mux4~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT1|Mux4~1_combout\);

-- Location: LCCOMB_X111_Y19_N8
\DAT1|Mux3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux3~0_combout\ = (\MCOUNT|Count:c[5]~q\ & ((\MCOUNT|Count:c[6]~q\ & ((\MCOUNT|Count:c[4]~q\))) # (!\MCOUNT|Count:c[6]~q\ & (\MCOUNT|Count:c[7]~q\ & !\MCOUNT|Count:c[4]~q\)))) # (!\MCOUNT|Count:c[5]~q\ & (!\MCOUNT|Count:c[7]~q\ & 
-- (\MCOUNT|Count:c[6]~q\ $ (\MCOUNT|Count:c[4]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000100100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[7]~q\,
	datab => \MCOUNT|Count:c[6]~q\,
	datac => \MCOUNT|Count:c[5]~q\,
	datad => \MCOUNT|Count:c[4]~q\,
	combout => \DAT1|Mux3~0_combout\);

-- Location: LCCOMB_X111_Y19_N22
\DAT1|Mux3~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux3~1_combout\ = (\nRST~input_o\ & \DAT1|Mux3~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datac => \DAT1|Mux3~0_combout\,
	combout => \DAT1|Mux3~1_combout\);

-- Location: LCCOMB_X111_Y19_N12
\DAT1|Mux2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux2~0_combout\ = (\MCOUNT|Count:c[5]~q\ & (!\MCOUNT|Count:c[7]~q\ & ((\MCOUNT|Count:c[4]~q\)))) # (!\MCOUNT|Count:c[5]~q\ & ((\MCOUNT|Count:c[6]~q\ & (!\MCOUNT|Count:c[7]~q\)) # (!\MCOUNT|Count:c[6]~q\ & ((\MCOUNT|Count:c[4]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[7]~q\,
	datab => \MCOUNT|Count:c[6]~q\,
	datac => \MCOUNT|Count:c[5]~q\,
	datad => \MCOUNT|Count:c[4]~q\,
	combout => \DAT1|Mux2~0_combout\);

-- Location: LCCOMB_X111_Y19_N10
\DAT1|Mux2~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux2~1_combout\ = (\nRST~input_o\ & \DAT1|Mux2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datad => \DAT1|Mux2~0_combout\,
	combout => \DAT1|Mux2~1_combout\);

-- Location: LCCOMB_X111_Y19_N24
\DAT1|Mux1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux1~0_combout\ = (\MCOUNT|Count:c[6]~q\ & (\MCOUNT|Count:c[4]~q\ & (\MCOUNT|Count:c[7]~q\ $ (\MCOUNT|Count:c[5]~q\)))) # (!\MCOUNT|Count:c[6]~q\ & (!\MCOUNT|Count:c[7]~q\ & ((\MCOUNT|Count:c[5]~q\) # (\MCOUNT|Count:c[4]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101100100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[7]~q\,
	datab => \MCOUNT|Count:c[6]~q\,
	datac => \MCOUNT|Count:c[5]~q\,
	datad => \MCOUNT|Count:c[4]~q\,
	combout => \DAT1|Mux1~0_combout\);

-- Location: LCCOMB_X112_Y19_N2
\DAT1|Mux1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux1~1_combout\ = (\DAT1|Mux1~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT1|Mux1~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT1|Mux1~1_combout\);

-- Location: LCCOMB_X111_Y19_N6
\DAT1|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux0~0_combout\ = (\MCOUNT|Count:c[4]~q\ & ((\MCOUNT|Count:c[7]~q\) # (\MCOUNT|Count:c[6]~q\ $ (\MCOUNT|Count:c[5]~q\)))) # (!\MCOUNT|Count:c[4]~q\ & ((\MCOUNT|Count:c[5]~q\) # (\MCOUNT|Count:c[7]~q\ $ (\MCOUNT|Count:c[6]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111011110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[7]~q\,
	datab => \MCOUNT|Count:c[6]~q\,
	datac => \MCOUNT|Count:c[5]~q\,
	datad => \MCOUNT|Count:c[4]~q\,
	combout => \DAT1|Mux0~0_combout\);

-- Location: LCCOMB_X111_Y19_N0
\DAT1|Mux0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT1|Mux0~1_combout\ = (\nRST~input_o\ & !\DAT1|Mux0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datad => \DAT1|Mux0~0_combout\,
	combout => \DAT1|Mux0~1_combout\);

-- Location: LCCOMB_X107_Y19_N14
\DAT2|Mux6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux6~0_combout\ = (\MCOUNT|Count:c[10]~q\ & (!\MCOUNT|Count:c[9]~q\ & (\MCOUNT|Count:c[11]~q\ $ (!\MCOUNT|Count:c[8]~q\)))) # (!\MCOUNT|Count:c[10]~q\ & (\MCOUNT|Count:c[8]~q\ & (\MCOUNT|Count:c[9]~q\ $ (!\MCOUNT|Count:c[11]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[9]~q\,
	datab => \MCOUNT|Count:c[10]~q\,
	datac => \MCOUNT|Count:c[11]~q\,
	datad => \MCOUNT|Count:c[8]~q\,
	combout => \DAT2|Mux6~0_combout\);

-- Location: LCCOMB_X108_Y19_N2
\DAT2|Mux6~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux6~1_combout\ = (\DAT2|Mux6~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DAT2|Mux6~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT2|Mux6~1_combout\);

-- Location: LCCOMB_X107_Y19_N16
\DAT2|Mux5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux5~0_combout\ = (\MCOUNT|Count:c[9]~q\ & ((\MCOUNT|Count:c[8]~q\ & ((\MCOUNT|Count:c[11]~q\))) # (!\MCOUNT|Count:c[8]~q\ & (\MCOUNT|Count:c[10]~q\)))) # (!\MCOUNT|Count:c[9]~q\ & (\MCOUNT|Count:c[10]~q\ & (\MCOUNT|Count:c[11]~q\ $ 
-- (\MCOUNT|Count:c[8]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[9]~q\,
	datab => \MCOUNT|Count:c[10]~q\,
	datac => \MCOUNT|Count:c[11]~q\,
	datad => \MCOUNT|Count:c[8]~q\,
	combout => \DAT2|Mux5~0_combout\);

-- Location: LCCOMB_X108_Y19_N4
\DAT2|Mux5~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux5~1_combout\ = (\DAT2|Mux5~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DAT2|Mux5~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT2|Mux5~1_combout\);

-- Location: LCCOMB_X107_Y19_N2
\DAT2|Mux4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux4~0_combout\ = (\MCOUNT|Count:c[10]~q\ & (\MCOUNT|Count:c[11]~q\ & ((\MCOUNT|Count:c[9]~q\) # (!\MCOUNT|Count:c[8]~q\)))) # (!\MCOUNT|Count:c[10]~q\ & (\MCOUNT|Count:c[9]~q\ & (!\MCOUNT|Count:c[11]~q\ & !\MCOUNT|Count:c[8]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[9]~q\,
	datab => \MCOUNT|Count:c[10]~q\,
	datac => \MCOUNT|Count:c[11]~q\,
	datad => \MCOUNT|Count:c[8]~q\,
	combout => \DAT2|Mux4~0_combout\);

-- Location: LCCOMB_X107_Y19_N4
\DAT2|Mux4~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux4~1_combout\ = (\DAT2|Mux4~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT2|Mux4~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT2|Mux4~1_combout\);

-- Location: LCCOMB_X107_Y19_N22
\DAT2|Mux3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux3~0_combout\ = (\MCOUNT|Count:c[9]~q\ & ((\MCOUNT|Count:c[10]~q\ & ((\MCOUNT|Count:c[8]~q\))) # (!\MCOUNT|Count:c[10]~q\ & (\MCOUNT|Count:c[11]~q\ & !\MCOUNT|Count:c[8]~q\)))) # (!\MCOUNT|Count:c[9]~q\ & (!\MCOUNT|Count:c[11]~q\ & 
-- (\MCOUNT|Count:c[10]~q\ $ (\MCOUNT|Count:c[8]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100100100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[9]~q\,
	datab => \MCOUNT|Count:c[10]~q\,
	datac => \MCOUNT|Count:c[11]~q\,
	datad => \MCOUNT|Count:c[8]~q\,
	combout => \DAT2|Mux3~0_combout\);

-- Location: LCCOMB_X107_Y19_N28
\DAT2|Mux3~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux3~1_combout\ = (\DAT2|Mux3~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DAT2|Mux3~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT2|Mux3~1_combout\);

-- Location: LCCOMB_X107_Y19_N10
\DAT2|Mux2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux2~0_combout\ = (\MCOUNT|Count:c[9]~q\ & (((!\MCOUNT|Count:c[11]~q\ & \MCOUNT|Count:c[8]~q\)))) # (!\MCOUNT|Count:c[9]~q\ & ((\MCOUNT|Count:c[10]~q\ & (!\MCOUNT|Count:c[11]~q\)) # (!\MCOUNT|Count:c[10]~q\ & ((\MCOUNT|Count:c[8]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[9]~q\,
	datab => \MCOUNT|Count:c[10]~q\,
	datac => \MCOUNT|Count:c[11]~q\,
	datad => \MCOUNT|Count:c[8]~q\,
	combout => \DAT2|Mux2~0_combout\);

-- Location: LCCOMB_X107_Y19_N20
\DAT2|Mux2~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux2~1_combout\ = (\DAT2|Mux2~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DAT2|Mux2~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT2|Mux2~1_combout\);

-- Location: LCCOMB_X107_Y19_N26
\DAT2|Mux1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux1~0_combout\ = (\MCOUNT|Count:c[9]~q\ & (!\MCOUNT|Count:c[11]~q\ & ((\MCOUNT|Count:c[8]~q\) # (!\MCOUNT|Count:c[10]~q\)))) # (!\MCOUNT|Count:c[9]~q\ & (\MCOUNT|Count:c[8]~q\ & (\MCOUNT|Count:c[10]~q\ $ (!\MCOUNT|Count:c[11]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100101100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[9]~q\,
	datab => \MCOUNT|Count:c[10]~q\,
	datac => \MCOUNT|Count:c[11]~q\,
	datad => \MCOUNT|Count:c[8]~q\,
	combout => \DAT2|Mux1~0_combout\);

-- Location: LCCOMB_X108_Y19_N18
\DAT2|Mux1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux1~1_combout\ = (\DAT2|Mux1~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DAT2|Mux1~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT2|Mux1~1_combout\);

-- Location: LCCOMB_X107_Y19_N0
\DAT2|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux0~0_combout\ = (\MCOUNT|Count:c[8]~q\ & ((\MCOUNT|Count:c[11]~q\) # (\MCOUNT|Count:c[9]~q\ $ (\MCOUNT|Count:c[10]~q\)))) # (!\MCOUNT|Count:c[8]~q\ & ((\MCOUNT|Count:c[9]~q\) # (\MCOUNT|Count:c[10]~q\ $ (\MCOUNT|Count:c[11]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011010111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[9]~q\,
	datab => \MCOUNT|Count:c[10]~q\,
	datac => \MCOUNT|Count:c[11]~q\,
	datad => \MCOUNT|Count:c[8]~q\,
	combout => \DAT2|Mux0~0_combout\);

-- Location: LCCOMB_X111_Y19_N30
\DAT2|Mux0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT2|Mux0~1_combout\ = (!\DAT2|Mux0~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT2|Mux0~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT2|Mux0~1_combout\);

-- Location: LCCOMB_X112_Y19_N24
\DAT3|Mux6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux6~0_combout\ = (\MCOUNT|Count:c[15]~q\ & (\MCOUNT|Count:c[12]~q\ & (\MCOUNT|Count:c[14]~q\ $ (\MCOUNT|Count:c[13]~q\)))) # (!\MCOUNT|Count:c[15]~q\ & (!\MCOUNT|Count:c[13]~q\ & (\MCOUNT|Count:c[14]~q\ $ (\MCOUNT|Count:c[12]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[15]~q\,
	datab => \MCOUNT|Count:c[14]~q\,
	datac => \MCOUNT|Count:c[13]~q\,
	datad => \MCOUNT|Count:c[12]~q\,
	combout => \DAT3|Mux6~0_combout\);

-- Location: LCCOMB_X112_Y19_N26
\DAT3|Mux6~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux6~1_combout\ = (\DAT3|Mux6~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT3|Mux6~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT3|Mux6~1_combout\);

-- Location: LCCOMB_X112_Y19_N28
\DAT3|Mux5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux5~0_combout\ = (\MCOUNT|Count:c[15]~q\ & ((\MCOUNT|Count:c[12]~q\ & ((\MCOUNT|Count:c[13]~q\))) # (!\MCOUNT|Count:c[12]~q\ & (\MCOUNT|Count:c[14]~q\)))) # (!\MCOUNT|Count:c[15]~q\ & (\MCOUNT|Count:c[14]~q\ & (\MCOUNT|Count:c[13]~q\ $ 
-- (\MCOUNT|Count:c[12]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[15]~q\,
	datab => \MCOUNT|Count:c[14]~q\,
	datac => \MCOUNT|Count:c[13]~q\,
	datad => \MCOUNT|Count:c[12]~q\,
	combout => \DAT3|Mux5~0_combout\);

-- Location: LCCOMB_X112_Y19_N14
\DAT3|Mux5~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux5~1_combout\ = (\DAT3|Mux5~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT3|Mux5~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT3|Mux5~1_combout\);

-- Location: LCCOMB_X111_Y19_N28
\DAT3|Mux4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux4~0_combout\ = (\MCOUNT|Count:c[14]~q\ & (\MCOUNT|Count:c[15]~q\ & ((\MCOUNT|Count:c[13]~q\) # (!\MCOUNT|Count:c[12]~q\)))) # (!\MCOUNT|Count:c[14]~q\ & (!\MCOUNT|Count:c[15]~q\ & (\MCOUNT|Count:c[13]~q\ & !\MCOUNT|Count:c[12]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[14]~q\,
	datab => \MCOUNT|Count:c[15]~q\,
	datac => \MCOUNT|Count:c[13]~q\,
	datad => \MCOUNT|Count:c[12]~q\,
	combout => \DAT3|Mux4~0_combout\);

-- Location: LCCOMB_X108_Y19_N20
\DAT3|Mux4~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux4~1_combout\ = (\DAT3|Mux4~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DAT3|Mux4~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT3|Mux4~1_combout\);

-- Location: LCCOMB_X112_Y19_N8
\DAT3|Mux3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux3~0_combout\ = (\MCOUNT|Count:c[13]~q\ & ((\MCOUNT|Count:c[14]~q\ & ((\MCOUNT|Count:c[12]~q\))) # (!\MCOUNT|Count:c[14]~q\ & (\MCOUNT|Count:c[15]~q\ & !\MCOUNT|Count:c[12]~q\)))) # (!\MCOUNT|Count:c[13]~q\ & (!\MCOUNT|Count:c[15]~q\ & 
-- (\MCOUNT|Count:c[14]~q\ $ (\MCOUNT|Count:c[12]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000100100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[15]~q\,
	datab => \MCOUNT|Count:c[14]~q\,
	datac => \MCOUNT|Count:c[13]~q\,
	datad => \MCOUNT|Count:c[12]~q\,
	combout => \DAT3|Mux3~0_combout\);

-- Location: LCCOMB_X112_Y19_N30
\DAT3|Mux3~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux3~1_combout\ = (\DAT3|Mux3~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DAT3|Mux3~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT3|Mux3~1_combout\);

-- Location: LCCOMB_X111_Y19_N2
\DAT3|Mux2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux2~0_combout\ = (\MCOUNT|Count:c[13]~q\ & (((!\MCOUNT|Count:c[15]~q\ & \MCOUNT|Count:c[12]~q\)))) # (!\MCOUNT|Count:c[13]~q\ & ((\MCOUNT|Count:c[14]~q\ & (!\MCOUNT|Count:c[15]~q\)) # (!\MCOUNT|Count:c[14]~q\ & ((\MCOUNT|Count:c[12]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[14]~q\,
	datab => \MCOUNT|Count:c[15]~q\,
	datac => \MCOUNT|Count:c[13]~q\,
	datad => \MCOUNT|Count:c[12]~q\,
	combout => \DAT3|Mux2~0_combout\);

-- Location: LCCOMB_X111_Y19_N4
\DAT3|Mux2~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux2~1_combout\ = (\nRST~input_o\ & \DAT3|Mux2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datad => \DAT3|Mux2~0_combout\,
	combout => \DAT3|Mux2~1_combout\);

-- Location: LCCOMB_X111_Y19_N18
\DAT3|Mux1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux1~0_combout\ = (\MCOUNT|Count:c[14]~q\ & (\MCOUNT|Count:c[12]~q\ & (\MCOUNT|Count:c[15]~q\ $ (\MCOUNT|Count:c[13]~q\)))) # (!\MCOUNT|Count:c[14]~q\ & (!\MCOUNT|Count:c[15]~q\ & ((\MCOUNT|Count:c[13]~q\) # (\MCOUNT|Count:c[12]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011100100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[14]~q\,
	datab => \MCOUNT|Count:c[15]~q\,
	datac => \MCOUNT|Count:c[13]~q\,
	datad => \MCOUNT|Count:c[12]~q\,
	combout => \DAT3|Mux1~0_combout\);

-- Location: LCCOMB_X107_Y19_N6
\DAT3|Mux1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux1~1_combout\ = (\DAT3|Mux1~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DAT3|Mux1~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT3|Mux1~1_combout\);

-- Location: LCCOMB_X112_Y19_N12
\DAT3|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux0~0_combout\ = (\MCOUNT|Count:c[12]~q\ & ((\MCOUNT|Count:c[15]~q\) # (\MCOUNT|Count:c[14]~q\ $ (\MCOUNT|Count:c[13]~q\)))) # (!\MCOUNT|Count:c[12]~q\ & ((\MCOUNT|Count:c[13]~q\) # (\MCOUNT|Count:c[15]~q\ $ (\MCOUNT|Count:c[14]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111011110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \MCOUNT|Count:c[15]~q\,
	datab => \MCOUNT|Count:c[14]~q\,
	datac => \MCOUNT|Count:c[13]~q\,
	datad => \MCOUNT|Count:c[12]~q\,
	combout => \DAT3|Mux0~0_combout\);

-- Location: LCCOMB_X112_Y19_N22
\DAT3|Mux0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DAT3|Mux0~1_combout\ = (!\DAT3|Mux0~0_combout\ & \nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DAT3|Mux0~0_combout\,
	datad => \nRST~input_o\,
	combout => \DAT3|Mux0~1_combout\);

ww_Error <= \Error~output_o\;

ww_D0(0) <= \D0[0]~output_o\;

ww_D0(1) <= \D0[1]~output_o\;

ww_D0(2) <= \D0[2]~output_o\;

ww_D0(3) <= \D0[3]~output_o\;

ww_D0(4) <= \D0[4]~output_o\;

ww_D0(5) <= \D0[5]~output_o\;

ww_D0(6) <= \D0[6]~output_o\;

ww_D1(0) <= \D1[0]~output_o\;

ww_D1(1) <= \D1[1]~output_o\;

ww_D1(2) <= \D1[2]~output_o\;

ww_D1(3) <= \D1[3]~output_o\;

ww_D1(4) <= \D1[4]~output_o\;

ww_D1(5) <= \D1[5]~output_o\;

ww_D1(6) <= \D1[6]~output_o\;

ww_D2(0) <= \D2[0]~output_o\;

ww_D2(1) <= \D2[1]~output_o\;

ww_D2(2) <= \D2[2]~output_o\;

ww_D2(3) <= \D2[3]~output_o\;

ww_D2(4) <= \D2[4]~output_o\;

ww_D2(5) <= \D2[5]~output_o\;

ww_D2(6) <= \D2[6]~output_o\;

ww_D3(0) <= \D3[0]~output_o\;

ww_D3(1) <= \D3[1]~output_o\;

ww_D3(2) <= \D3[2]~output_o\;

ww_D3(3) <= \D3[3]~output_o\;

ww_D3(4) <= \D3[4]~output_o\;

ww_D3(5) <= \D3[5]~output_o\;

ww_D3(6) <= \D3[6]~output_o\;
END structure;


