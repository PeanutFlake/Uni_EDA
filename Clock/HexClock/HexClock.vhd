LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY HexClock IS
	GENERIC (
		Clock_Freq		:	Integer := 50000000	-- 50MHz
	);
	PORT (
		CLK, nRST		:	IN		std_logic;
		Error				:	OUT	std_logic;
		D0, D1, D2, D3	:	OUT	std_logic_vector(6 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF HexClock IS

	COMPONENT ClockDivider IS
		GENERIC (
			MemLimit	:	Integer	:= 255;
			Cycles	:	Integer	:= 4
		);
		PORT (
			CLK, RST		:	IN		std_logic;
			DIV, Err		:	OUT	std_logic
		);
	END COMPONENT;
	
	COMPONENT Segmentdisplay IS
		PORT (
			LTN, BLN, HB	:	IN		std_logic;
			D					:	IN		std_logic_vector(3 downto 0);
			S					:	OUT	std_logic_vector(6 downto 0)
		);
	END COMPONENT;
	
	COMPONENT Counter IS
		GENERIC (
			Limit		:	Integer	:= 255
		);
		PORT (
			CLK, RST	:	IN		std_logic;
			Ov			:	OUT	std_logic;
			Q			:	OUT	integer range 0 to Limit
		);
	END COMPONENT;

	SIGNAL s_clk, s_clk_err, s_rst	:	std_logic := '0';
	
	SIGNAL s_d0, s_d1, s_d2, s_d3		:	std_logic_vector(3 downto 0) := (Others => '0');
	SIGNAL s_ltn, s_bln, s_hb 			:	std_logic := '0';
	
	SIGNAL s_d								:	integer range 0 to 65535 := 0;
	SIGNAL s_db								:	std_logic_vector(15 downto 0) := (Others => '0');
	SIGNAL s_ov								:	std_logic := '0';
	
BEGIN

	MCLK : ClockDivider
	GENERIC MAP (Clock_Freq+1, Clock_Freq/2)
	PORT MAP (CLK => CLK, RST => s_rst, DIV => s_clk, Err => s_clk_err);
	
	DAT0 : Segmentdisplay
	PORT MAP (s_ltn, s_bln, s_hb, s_d0, D0);

	DAT1 : Segmentdisplay
	PORT MAP (s_ltn, s_bln, s_hb, s_d1, D1);
	
	DAT2 : Segmentdisplay
	PORT MAP (s_ltn, s_bln, s_hb, s_d2, D2);
	
	DAT3 : Segmentdisplay
	PORT MAP (s_ltn, s_bln, s_hb, s_d3, D3);
	
	MCOUNT : Counter
	GENERIC MAP (65535)
	PORT MAP (s_clk, s_rst, s_ov, s_d);
	
	s_hb <= '0';
	s_ltn <= nRST;
	s_bln <= NOT nRST;
	s_rst <= NOT nRST;
	
	s_db <= std_logic_vector(to_unsigned(s_d, 16));
	
	s_d3 <= s_db(15 downto 12);
	s_d2 <= s_db(11 downto 8);
	s_d1 <= s_db(7 downto 4);
	s_d0 <= s_db(3 downto 0);
	
	Error <= s_clk_err OR s_ov;

END Behaviour;