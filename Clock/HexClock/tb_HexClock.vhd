LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_HexClock IS
END ENTITY;

ARCHITECTURE Testbench OF tb_HexClock IS
	COMPONENT HexClock IS
		GENERIC (
			Clock_Freq		:	Integer := 50000000	--	50MHz
		);
		PORT (
			CLK, nRST		:	IN		std_logic;
			Error				:	OUT	std_logic;
			D0, D1, D2, D3	:	OUT	std_logic_vector(6 downto 0)
		);
	END COMPONENT;
	
	SIGNAL s_clk, s_rst		:	std_logic	:= '1';
	SIGNAL s_err				:	std_logic	:= '0';
	SIGNAL s_d0, s_d1, s_d2, s_d3	:	std_logic_vector(6 downto 0) := (Others => '0');
	
BEGIN

	DUT	:	HexClock
	-- 1kHz Clock
	GENERIC MAP (50000000)
	PORT MAP (
		s_clk, s_rst, s_err, s_d0, s_d1, s_d2, s_d3
	);

	s_clk <= NOT s_clk AFTER 10ns;
	
	--s_rst <= '1' AFTER 100us, '0' AFTER 110us;

END Testbench;