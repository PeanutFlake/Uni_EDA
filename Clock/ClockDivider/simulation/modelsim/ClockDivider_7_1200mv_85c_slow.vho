-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/14/2018 13:00:29"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	ClockDivider IS
    PORT (
	CLK : IN std_logic;
	DIV : BUFFER std_logic;
	Err : BUFFER std_logic
	);
END ClockDivider;

-- Design Ports Information
-- DIV	=>  Location: PIN_E21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Err	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_M23,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF ClockDivider IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_DIV : std_logic;
SIGNAL ww_Err : std_logic;
SIGNAL \DIV~output_o\ : std_logic;
SIGNAL \Err~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \Count|Add0~5\ : std_logic;
SIGNAL \Count|Add0~6_combout\ : std_logic;
SIGNAL \Count|Count:c[4]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[4]~q\ : std_logic;
SIGNAL \Count|Add0~7\ : std_logic;
SIGNAL \Count|Add0~8_combout\ : std_logic;
SIGNAL \Count|Count:c[5]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[5]~q\ : std_logic;
SIGNAL \Count|Add0~9\ : std_logic;
SIGNAL \Count|Add0~10_combout\ : std_logic;
SIGNAL \Count|Count:c[6]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[6]~q\ : std_logic;
SIGNAL \Count|Add0~11\ : std_logic;
SIGNAL \Count|Add0~12_combout\ : std_logic;
SIGNAL \Count|Count:c[7]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[7]~q\ : std_logic;
SIGNAL \Count|Equal0~0_combout\ : std_logic;
SIGNAL \Count|Count:c[0]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[0]~q\ : std_logic;
SIGNAL \Count|Add0~1\ : std_logic;
SIGNAL \Count|Add0~2_combout\ : std_logic;
SIGNAL \Count|Count:c[2]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[2]~q\ : std_logic;
SIGNAL \Count|Add0~3\ : std_logic;
SIGNAL \Count|Add0~4_combout\ : std_logic;
SIGNAL \Count|Count:c[3]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[3]~q\ : std_logic;
SIGNAL \Count|Equal0~1_combout\ : std_logic;
SIGNAL \Count|Add0~0_combout\ : std_logic;
SIGNAL \Count|Count:c[1]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[1]~q\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \Buff|qn~0_combout\ : std_logic;
SIGNAL \Buff|qn~q\ : std_logic;
SIGNAL \Buff|s_q~combout\ : std_logic;
SIGNAL \Count|s_ov~0_combout\ : std_logic;
SIGNAL \Count|s_ov~q\ : std_logic;
SIGNAL \ALT_INV_Equal0~2_combout\ : std_logic;

BEGIN

ww_CLK <= CLK;
DIV <= ww_DIV;
Err <= ww_Err;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_Equal0~2_combout\ <= NOT \Equal0~2_combout\;

-- Location: IOOBUF_X107_Y73_N9
\DIV~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Buff|s_q~combout\,
	devoe => ww_devoe,
	o => \DIV~output_o\);

-- Location: IOOBUF_X69_Y73_N16
\Err~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Count|s_ov~q\,
	devoe => ww_devoe,
	o => \Err~output_o\);

-- Location: IOIBUF_X115_Y40_N8
\CLK~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: LCCOMB_X107_Y46_N20
\Count|Add0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Add0~4_combout\ = (\Count|Count:c[3]~q\ & (\Count|Add0~3\ $ (GND))) # (!\Count|Count:c[3]~q\ & (!\Count|Add0~3\ & VCC))
-- \Count|Add0~5\ = CARRY((\Count|Count:c[3]~q\ & !\Count|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Count|Count:c[3]~q\,
	datad => VCC,
	cin => \Count|Add0~3\,
	combout => \Count|Add0~4_combout\,
	cout => \Count|Add0~5\);

-- Location: LCCOMB_X107_Y46_N22
\Count|Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Add0~6_combout\ = (\Count|Count:c[4]~q\ & (!\Count|Add0~5\)) # (!\Count|Count:c[4]~q\ & ((\Count|Add0~5\) # (GND)))
-- \Count|Add0~7\ = CARRY((!\Count|Add0~5\) # (!\Count|Count:c[4]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[4]~q\,
	datad => VCC,
	cin => \Count|Add0~5\,
	combout => \Count|Add0~6_combout\,
	cout => \Count|Add0~7\);

-- Location: LCCOMB_X108_Y46_N20
\Count|Count:c[4]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Count:c[4]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & ((\Count|Count:c[4]~q\))) # (!\Count|Equal0~0_combout\ & (\Count|Add0~6_combout\)))) # (!\Count|Equal0~1_combout\ & (\Count|Add0~6_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datab => \Count|Add0~6_combout\,
	datac => \Count|Count:c[4]~q\,
	datad => \Count|Equal0~0_combout\,
	combout => \Count|Count:c[4]~0_combout\);

-- Location: FF_X108_Y46_N21
\Count|Count:c[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \Count|Count:c[4]~0_combout\,
	clrn => \ALT_INV_Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[4]~q\);

-- Location: LCCOMB_X107_Y46_N24
\Count|Add0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Add0~8_combout\ = (\Count|Count:c[5]~q\ & (\Count|Add0~7\ $ (GND))) # (!\Count|Count:c[5]~q\ & (!\Count|Add0~7\ & VCC))
-- \Count|Add0~9\ = CARRY((\Count|Count:c[5]~q\ & !\Count|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Count|Count:c[5]~q\,
	datad => VCC,
	cin => \Count|Add0~7\,
	combout => \Count|Add0~8_combout\,
	cout => \Count|Add0~9\);

-- Location: LCCOMB_X108_Y46_N28
\Count|Count:c[5]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Count:c[5]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & ((\Count|Count:c[5]~q\))) # (!\Count|Equal0~0_combout\ & (\Count|Add0~8_combout\)))) # (!\Count|Equal0~1_combout\ & (\Count|Add0~8_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Add0~8_combout\,
	datab => \Count|Equal0~1_combout\,
	datac => \Count|Count:c[5]~q\,
	datad => \Count|Equal0~0_combout\,
	combout => \Count|Count:c[5]~0_combout\);

-- Location: FF_X108_Y46_N29
\Count|Count:c[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \Count|Count:c[5]~0_combout\,
	clrn => \ALT_INV_Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[5]~q\);

-- Location: LCCOMB_X107_Y46_N26
\Count|Add0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Add0~10_combout\ = (\Count|Count:c[6]~q\ & (!\Count|Add0~9\)) # (!\Count|Count:c[6]~q\ & ((\Count|Add0~9\) # (GND)))
-- \Count|Add0~11\ = CARRY((!\Count|Add0~9\) # (!\Count|Count:c[6]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[6]~q\,
	datad => VCC,
	cin => \Count|Add0~9\,
	combout => \Count|Add0~10_combout\,
	cout => \Count|Add0~11\);

-- Location: LCCOMB_X107_Y46_N30
\Count|Count:c[6]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Count:c[6]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & (\Count|Count:c[6]~q\)) # (!\Count|Equal0~0_combout\ & ((\Count|Add0~10_combout\))))) # (!\Count|Equal0~1_combout\ & (((\Count|Add0~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datab => \Count|Equal0~0_combout\,
	datac => \Count|Count:c[6]~q\,
	datad => \Count|Add0~10_combout\,
	combout => \Count|Count:c[6]~0_combout\);

-- Location: FF_X107_Y46_N31
\Count|Count:c[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \Count|Count:c[6]~0_combout\,
	clrn => \ALT_INV_Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[6]~q\);

-- Location: LCCOMB_X107_Y46_N28
\Count|Add0~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Add0~12_combout\ = \Count|Add0~11\ $ (!\Count|Count:c[7]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \Count|Count:c[7]~q\,
	cin => \Count|Add0~11\,
	combout => \Count|Add0~12_combout\);

-- Location: LCCOMB_X107_Y46_N6
\Count|Count:c[7]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Count:c[7]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & ((\Count|Count:c[7]~q\))) # (!\Count|Equal0~0_combout\ & (\Count|Add0~12_combout\)))) # (!\Count|Equal0~1_combout\ & (\Count|Add0~12_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Add0~12_combout\,
	datab => \Count|Equal0~1_combout\,
	datac => \Count|Count:c[7]~q\,
	datad => \Count|Equal0~0_combout\,
	combout => \Count|Count:c[7]~0_combout\);

-- Location: FF_X107_Y46_N7
\Count|Count:c[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \Count|Count:c[7]~0_combout\,
	clrn => \ALT_INV_Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[7]~q\);

-- Location: LCCOMB_X108_Y46_N24
\Count|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Equal0~0_combout\ = (\Count|Count:c[4]~q\ & (\Count|Count:c[5]~q\ & (\Count|Count:c[7]~q\ & \Count|Count:c[6]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[4]~q\,
	datab => \Count|Count:c[5]~q\,
	datac => \Count|Count:c[7]~q\,
	datad => \Count|Count:c[6]~q\,
	combout => \Count|Equal0~0_combout\);

-- Location: LCCOMB_X108_Y46_N30
\Count|Count:c[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Count:c[0]~0_combout\ = ((\Count|Equal0~1_combout\ & \Count|Equal0~0_combout\)) # (!\Count|Count:c[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datac => \Count|Count:c[0]~q\,
	datad => \Count|Equal0~0_combout\,
	combout => \Count|Count:c[0]~0_combout\);

-- Location: FF_X107_Y46_N9
\Count|Count:c[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	asdata => \Count|Count:c[0]~0_combout\,
	clrn => \ALT_INV_Equal0~2_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[0]~q\);

-- Location: LCCOMB_X107_Y46_N16
\Count|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Add0~0_combout\ = (\Count|Count:c[1]~q\ & (\Count|Count:c[0]~q\ $ (VCC))) # (!\Count|Count:c[1]~q\ & (\Count|Count:c[0]~q\ & VCC))
-- \Count|Add0~1\ = CARRY((\Count|Count:c[1]~q\ & \Count|Count:c[0]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[1]~q\,
	datab => \Count|Count:c[0]~q\,
	datad => VCC,
	combout => \Count|Add0~0_combout\,
	cout => \Count|Add0~1\);

-- Location: LCCOMB_X107_Y46_N18
\Count|Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Add0~2_combout\ = (\Count|Count:c[2]~q\ & (!\Count|Add0~1\)) # (!\Count|Count:c[2]~q\ & ((\Count|Add0~1\) # (GND)))
-- \Count|Add0~3\ = CARRY((!\Count|Add0~1\) # (!\Count|Count:c[2]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Count|Count:c[2]~q\,
	datad => VCC,
	cin => \Count|Add0~1\,
	combout => \Count|Add0~2_combout\,
	cout => \Count|Add0~3\);

-- Location: LCCOMB_X107_Y46_N14
\Count|Count:c[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Count:c[2]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & ((\Count|Count:c[2]~q\))) # (!\Count|Equal0~0_combout\ & (\Count|Add0~2_combout\)))) # (!\Count|Equal0~1_combout\ & (\Count|Add0~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datab => \Count|Add0~2_combout\,
	datac => \Count|Count:c[2]~q\,
	datad => \Count|Equal0~0_combout\,
	combout => \Count|Count:c[2]~0_combout\);

-- Location: FF_X107_Y46_N15
\Count|Count:c[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \Count|Count:c[2]~0_combout\,
	clrn => \ALT_INV_Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[2]~q\);

-- Location: LCCOMB_X107_Y46_N2
\Count|Count:c[3]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Count:c[3]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & ((\Count|Count:c[3]~q\))) # (!\Count|Equal0~0_combout\ & (\Count|Add0~4_combout\)))) # (!\Count|Equal0~1_combout\ & (\Count|Add0~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Add0~4_combout\,
	datab => \Count|Equal0~1_combout\,
	datac => \Count|Count:c[3]~q\,
	datad => \Count|Equal0~0_combout\,
	combout => \Count|Count:c[3]~0_combout\);

-- Location: FF_X107_Y46_N3
\Count|Count:c[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \Count|Count:c[3]~0_combout\,
	clrn => \ALT_INV_Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[3]~q\);

-- Location: LCCOMB_X108_Y46_N26
\Count|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Equal0~1_combout\ = (\Count|Count:c[3]~q\ & (\Count|Count:c[0]~q\ & (\Count|Count:c[2]~q\ & \Count|Count:c[1]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[3]~q\,
	datab => \Count|Count:c[0]~q\,
	datac => \Count|Count:c[2]~q\,
	datad => \Count|Count:c[1]~q\,
	combout => \Count|Equal0~1_combout\);

-- Location: LCCOMB_X107_Y46_N12
\Count|Count:c[1]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Count:c[1]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & (\Count|Count:c[1]~q\)) # (!\Count|Equal0~0_combout\ & ((\Count|Add0~0_combout\))))) # (!\Count|Equal0~1_combout\ & (((\Count|Add0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datab => \Count|Equal0~0_combout\,
	datac => \Count|Count:c[1]~q\,
	datad => \Count|Add0~0_combout\,
	combout => \Count|Count:c[1]~0_combout\);

-- Location: FF_X107_Y46_N13
\Count|Count:c[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \Count|Count:c[1]~0_combout\,
	clrn => \ALT_INV_Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[1]~q\);

-- Location: LCCOMB_X107_Y46_N8
\Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = (!\Count|Count:c[1]~q\ & (\Count|Count:c[2]~q\ & (!\Count|Count:c[0]~q\ & !\Count|Count:c[3]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[1]~q\,
	datab => \Count|Count:c[2]~q\,
	datac => \Count|Count:c[0]~q\,
	datad => \Count|Count:c[3]~q\,
	combout => \Equal0~1_combout\);

-- Location: LCCOMB_X107_Y46_N10
\Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = (!\Count|Count:c[6]~q\ & (!\Count|Count:c[5]~q\ & (!\Count|Count:c[4]~q\ & !\Count|Count:c[7]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[6]~q\,
	datab => \Count|Count:c[5]~q\,
	datac => \Count|Count:c[4]~q\,
	datad => \Count|Count:c[7]~q\,
	combout => \Equal0~0_combout\);

-- Location: LCCOMB_X107_Y46_N4
\Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = (\Equal0~1_combout\ & \Equal0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Equal0~1_combout\,
	datad => \Equal0~0_combout\,
	combout => \Equal0~2_combout\);

-- Location: LCCOMB_X108_Y46_N18
\Buff|qn~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Buff|qn~0_combout\ = (\Equal0~1_combout\ & ((\Equal0~0_combout\ & ((!\Buff|s_q~combout\))) # (!\Equal0~0_combout\ & (\Buff|qn~q\)))) # (!\Equal0~1_combout\ & (((\Buff|qn~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~1_combout\,
	datab => \Equal0~0_combout\,
	datac => \Buff|qn~q\,
	datad => \Buff|s_q~combout\,
	combout => \Buff|qn~0_combout\);

-- Location: FF_X108_Y46_N19
\Buff|qn\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \Buff|qn~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Buff|qn~q\);

-- Location: LCCOMB_X108_Y46_N22
\Buff|s_q\ : cycloneive_lcell_comb
-- Equation(s):
-- \Buff|s_q~combout\ = (\Equal0~2_combout\ & ((\Buff|qn~q\))) # (!\Equal0~2_combout\ & (\Buff|s_q~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Buff|s_q~combout\,
	datac => \Equal0~2_combout\,
	datad => \Buff|qn~q\,
	combout => \Buff|s_q~combout\);

-- Location: LCCOMB_X107_Y46_N0
\Count|s_ov~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|s_ov~0_combout\ = (\Count|s_ov~q\) # ((\Count|Equal0~1_combout\ & \Count|Equal0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Count|Equal0~1_combout\,
	datac => \Count|s_ov~q\,
	datad => \Count|Equal0~0_combout\,
	combout => \Count|s_ov~0_combout\);

-- Location: FF_X107_Y46_N1
\Count|s_ov\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \Count|s_ov~0_combout\,
	clrn => \ALT_INV_Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|s_ov~q\);

ww_DIV <= \DIV~output_o\;

ww_Err <= \Err~output_o\;
END structure;


