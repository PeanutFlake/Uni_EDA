-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/14/2018 17:25:42"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	ClockDivider IS
    PORT (
	CLK : IN std_logic;
	DIV : OUT std_logic;
	Err : OUT std_logic
	);
END ClockDivider;

-- Design Ports Information
-- DIV	=>  Location: PIN_L9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Err	=>  Location: PIN_M9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_J7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF ClockDivider IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_DIV : std_logic;
SIGNAL ww_Err : std_logic;
SIGNAL \CLK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \DIV~output_o\ : std_logic;
SIGNAL \Err~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \CLK~inputclkctrl_outclk\ : std_logic;
SIGNAL \Count|Add0~1\ : std_logic;
SIGNAL \Count|Add0~2_combout\ : std_logic;
SIGNAL \Count|Count:c[2]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[2]~q\ : std_logic;
SIGNAL \Count|Add0~3\ : std_logic;
SIGNAL \Count|Add0~5\ : std_logic;
SIGNAL \Count|Add0~6_combout\ : std_logic;
SIGNAL \Count|Count:c[4]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[4]~q\ : std_logic;
SIGNAL \Count|Add0~7\ : std_logic;
SIGNAL \Count|Add0~8_combout\ : std_logic;
SIGNAL \Count|Count:c[5]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[5]~q\ : std_logic;
SIGNAL \Count|Add0~9\ : std_logic;
SIGNAL \Count|Add0~10_combout\ : std_logic;
SIGNAL \Count|Count:c[6]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[6]~q\ : std_logic;
SIGNAL \Count|Add0~11\ : std_logic;
SIGNAL \Count|Add0~12_combout\ : std_logic;
SIGNAL \Count|Count:c[7]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[7]~q\ : std_logic;
SIGNAL \Count|Equal0~0_combout\ : std_logic;
SIGNAL \Count|Count:c[0]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[0]~q\ : std_logic;
SIGNAL \Count|Add0~0_combout\ : std_logic;
SIGNAL \Count|Count:c[1]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[1]~q\ : std_logic;
SIGNAL \Count|Equal0~1_combout\ : std_logic;
SIGNAL \Count|Add0~4_combout\ : std_logic;
SIGNAL \Count|Count:c[3]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[3]~q\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \s_qe~q\ : std_logic;
SIGNAL \Buff|qn~0_combout\ : std_logic;
SIGNAL \Buff|qn~feeder_combout\ : std_logic;
SIGNAL \Buff|qn~q\ : std_logic;
SIGNAL \Buff|s_q~combout\ : std_logic;
SIGNAL \Count|s_ov~0_combout\ : std_logic;
SIGNAL \Count|s_ov~q\ : std_logic;
SIGNAL \ALT_INV_s_qe~q\ : std_logic;

BEGIN

ww_CLK <= CLK;
DIV <= ww_DIV;
Err <= ww_Err;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLK~input_o\);
\ALT_INV_s_qe~q\ <= NOT \s_qe~q\;

-- Location: IOOBUF_X24_Y0_N9
\DIV~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Buff|s_q~combout\,
	devoe => ww_devoe,
	o => \DIV~output_o\);

-- Location: IOOBUF_X24_Y0_N2
\Err~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Count|s_ov~q\,
	devoe => ww_devoe,
	o => \Err~output_o\);

-- Location: IOIBUF_X16_Y0_N15
\CLK~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: CLKCTRL_G17
\CLK~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLK~inputclkctrl_outclk\);

-- Location: LCCOMB_X26_Y1_N4
\Count|Add0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Add0~0_combout\ = (\Count|Count:c[0]~q\ & (\Count|Count:c[1]~q\ $ (VCC))) # (!\Count|Count:c[0]~q\ & (\Count|Count:c[1]~q\ & VCC))
-- \Count|Add0~1\ = CARRY((\Count|Count:c[0]~q\ & \Count|Count:c[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[0]~q\,
	datab => \Count|Count:c[1]~q\,
	datad => VCC,
	combout => \Count|Add0~0_combout\,
	cout => \Count|Add0~1\);

-- Location: LCCOMB_X26_Y1_N6
\Count|Add0~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Add0~2_combout\ = (\Count|Count:c[2]~q\ & (!\Count|Add0~1\)) # (!\Count|Count:c[2]~q\ & ((\Count|Add0~1\) # (GND)))
-- \Count|Add0~3\ = CARRY((!\Count|Add0~1\) # (!\Count|Count:c[2]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[2]~q\,
	datad => VCC,
	cin => \Count|Add0~1\,
	combout => \Count|Add0~2_combout\,
	cout => \Count|Add0~3\);

-- Location: LCCOMB_X26_Y1_N22
\Count|Count:c[2]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Count:c[2]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & (\Count|Count:c[2]~q\)) # (!\Count|Equal0~0_combout\ & ((\Count|Add0~2_combout\))))) # (!\Count|Equal0~1_combout\ & (((\Count|Add0~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datab => \Count|Equal0~0_combout\,
	datac => \Count|Count:c[2]~q\,
	datad => \Count|Add0~2_combout\,
	combout => \Count|Count:c[2]~0_combout\);

-- Location: FF_X26_Y1_N23
\Count|Count:c[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Count|Count:c[2]~0_combout\,
	clrn => \ALT_INV_s_qe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[2]~q\);

-- Location: LCCOMB_X26_Y1_N8
\Count|Add0~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Add0~4_combout\ = (\Count|Count:c[3]~q\ & (\Count|Add0~3\ $ (GND))) # (!\Count|Count:c[3]~q\ & (!\Count|Add0~3\ & VCC))
-- \Count|Add0~5\ = CARRY((\Count|Count:c[3]~q\ & !\Count|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Count|Count:c[3]~q\,
	datad => VCC,
	cin => \Count|Add0~3\,
	combout => \Count|Add0~4_combout\,
	cout => \Count|Add0~5\);

-- Location: LCCOMB_X26_Y1_N10
\Count|Add0~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Add0~6_combout\ = (\Count|Count:c[4]~q\ & (!\Count|Add0~5\)) # (!\Count|Count:c[4]~q\ & ((\Count|Add0~5\) # (GND)))
-- \Count|Add0~7\ = CARRY((!\Count|Add0~5\) # (!\Count|Count:c[4]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Count|Count:c[4]~q\,
	datad => VCC,
	cin => \Count|Add0~5\,
	combout => \Count|Add0~6_combout\,
	cout => \Count|Add0~7\);

-- Location: LCCOMB_X26_Y1_N28
\Count|Count:c[4]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Count:c[4]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & (\Count|Count:c[4]~q\)) # (!\Count|Equal0~0_combout\ & ((\Count|Add0~6_combout\))))) # (!\Count|Equal0~1_combout\ & (((\Count|Add0~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datab => \Count|Equal0~0_combout\,
	datac => \Count|Count:c[4]~q\,
	datad => \Count|Add0~6_combout\,
	combout => \Count|Count:c[4]~0_combout\);

-- Location: FF_X26_Y1_N29
\Count|Count:c[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Count|Count:c[4]~0_combout\,
	clrn => \ALT_INV_s_qe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[4]~q\);

-- Location: LCCOMB_X26_Y1_N12
\Count|Add0~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Add0~8_combout\ = (\Count|Count:c[5]~q\ & (\Count|Add0~7\ $ (GND))) # (!\Count|Count:c[5]~q\ & (!\Count|Add0~7\ & VCC))
-- \Count|Add0~9\ = CARRY((\Count|Count:c[5]~q\ & !\Count|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Count|Count:c[5]~q\,
	datad => VCC,
	cin => \Count|Add0~7\,
	combout => \Count|Add0~8_combout\,
	cout => \Count|Add0~9\);

-- Location: LCCOMB_X26_Y1_N2
\Count|Count:c[5]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Count:c[5]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & (\Count|Count:c[5]~q\)) # (!\Count|Equal0~0_combout\ & ((\Count|Add0~8_combout\))))) # (!\Count|Equal0~1_combout\ & (((\Count|Add0~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datab => \Count|Equal0~0_combout\,
	datac => \Count|Count:c[5]~q\,
	datad => \Count|Add0~8_combout\,
	combout => \Count|Count:c[5]~0_combout\);

-- Location: FF_X26_Y1_N3
\Count|Count:c[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Count|Count:c[5]~0_combout\,
	clrn => \ALT_INV_s_qe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[5]~q\);

-- Location: LCCOMB_X26_Y1_N14
\Count|Add0~10\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Add0~10_combout\ = (\Count|Count:c[6]~q\ & (!\Count|Add0~9\)) # (!\Count|Count:c[6]~q\ & ((\Count|Add0~9\) # (GND)))
-- \Count|Add0~11\ = CARRY((!\Count|Add0~9\) # (!\Count|Count:c[6]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[6]~q\,
	datad => VCC,
	cin => \Count|Add0~9\,
	combout => \Count|Add0~10_combout\,
	cout => \Count|Add0~11\);

-- Location: LCCOMB_X26_Y1_N26
\Count|Count:c[6]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Count:c[6]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & (\Count|Count:c[6]~q\)) # (!\Count|Equal0~0_combout\ & ((\Count|Add0~10_combout\))))) # (!\Count|Equal0~1_combout\ & (((\Count|Add0~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datab => \Count|Equal0~0_combout\,
	datac => \Count|Count:c[6]~q\,
	datad => \Count|Add0~10_combout\,
	combout => \Count|Count:c[6]~0_combout\);

-- Location: FF_X26_Y1_N27
\Count|Count:c[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Count|Count:c[6]~0_combout\,
	clrn => \ALT_INV_s_qe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[6]~q\);

-- Location: LCCOMB_X26_Y1_N16
\Count|Add0~12\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Add0~12_combout\ = \Count|Add0~11\ $ (!\Count|Count:c[7]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \Count|Count:c[7]~q\,
	cin => \Count|Add0~11\,
	combout => \Count|Add0~12_combout\);

-- Location: LCCOMB_X26_Y1_N24
\Count|Count:c[7]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Count:c[7]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & (\Count|Count:c[7]~q\)) # (!\Count|Equal0~0_combout\ & ((\Count|Add0~12_combout\))))) # (!\Count|Equal0~1_combout\ & (((\Count|Add0~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datab => \Count|Equal0~0_combout\,
	datac => \Count|Count:c[7]~q\,
	datad => \Count|Add0~12_combout\,
	combout => \Count|Count:c[7]~0_combout\);

-- Location: FF_X26_Y1_N25
\Count|Count:c[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Count|Count:c[7]~0_combout\,
	clrn => \ALT_INV_s_qe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[7]~q\);

-- Location: LCCOMB_X26_Y1_N18
\Count|Equal0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Equal0~0_combout\ = (\Count|Count:c[6]~q\ & (\Count|Count:c[5]~q\ & (\Count|Count:c[4]~q\ & \Count|Count:c[7]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[6]~q\,
	datab => \Count|Count:c[5]~q\,
	datac => \Count|Count:c[4]~q\,
	datad => \Count|Count:c[7]~q\,
	combout => \Count|Equal0~0_combout\);

-- Location: LCCOMB_X26_Y1_N30
\Count|Count:c[0]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Count:c[0]~0_combout\ = ((\Count|Equal0~1_combout\ & \Count|Equal0~0_combout\)) # (!\Count|Count:c[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datab => \Count|Equal0~0_combout\,
	datac => \Count|Count:c[0]~q\,
	combout => \Count|Count:c[0]~0_combout\);

-- Location: FF_X26_Y1_N31
\Count|Count:c[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Count|Count:c[0]~0_combout\,
	clrn => \ALT_INV_s_qe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[0]~q\);

-- Location: LCCOMB_X26_Y1_N0
\Count|Count:c[1]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Count:c[1]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & ((\Count|Count:c[1]~q\))) # (!\Count|Equal0~0_combout\ & (\Count|Add0~0_combout\)))) # (!\Count|Equal0~1_combout\ & (\Count|Add0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datab => \Count|Add0~0_combout\,
	datac => \Count|Count:c[1]~q\,
	datad => \Count|Equal0~0_combout\,
	combout => \Count|Count:c[1]~0_combout\);

-- Location: FF_X26_Y1_N1
\Count|Count:c[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Count|Count:c[1]~0_combout\,
	clrn => \ALT_INV_s_qe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[1]~q\);

-- Location: LCCOMB_X25_Y1_N26
\Count|Equal0~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Equal0~1_combout\ = (\Count|Count:c[3]~q\ & (\Count|Count:c[1]~q\ & (\Count|Count:c[0]~q\ & \Count|Count:c[2]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[3]~q\,
	datab => \Count|Count:c[1]~q\,
	datac => \Count|Count:c[0]~q\,
	datad => \Count|Count:c[2]~q\,
	combout => \Count|Equal0~1_combout\);

-- Location: LCCOMB_X26_Y1_N20
\Count|Count:c[3]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|Count:c[3]~0_combout\ = (\Count|Equal0~1_combout\ & ((\Count|Equal0~0_combout\ & (\Count|Count:c[3]~q\)) # (!\Count|Equal0~0_combout\ & ((\Count|Add0~4_combout\))))) # (!\Count|Equal0~1_combout\ & (((\Count|Add0~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datab => \Count|Equal0~0_combout\,
	datac => \Count|Count:c[3]~q\,
	datad => \Count|Add0~4_combout\,
	combout => \Count|Count:c[3]~0_combout\);

-- Location: FF_X26_Y1_N21
\Count|Count:c[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Count|Count:c[3]~0_combout\,
	clrn => \ALT_INV_s_qe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[3]~q\);

-- Location: LCCOMB_X25_Y1_N20
\Equal0~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = (!\Count|Count:c[3]~q\ & (\Count|Count:c[1]~q\ & (!\Count|Count:c[0]~q\ & !\Count|Count:c[2]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[3]~q\,
	datab => \Count|Count:c[1]~q\,
	datac => \Count|Count:c[0]~q\,
	datad => \Count|Count:c[2]~q\,
	combout => \Equal0~1_combout\);

-- Location: LCCOMB_X25_Y1_N18
\Equal0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = (!\Count|Count:c[5]~q\ & (!\Count|Count:c[4]~q\ & (!\Count|Count:c[6]~q\ & !\Count|Count:c[7]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[5]~q\,
	datab => \Count|Count:c[4]~q\,
	datac => \Count|Count:c[6]~q\,
	datad => \Count|Count:c[7]~q\,
	combout => \Equal0~0_combout\);

-- Location: LCCOMB_X25_Y1_N30
\Equal0~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = (\Equal0~1_combout\ & \Equal0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~1_combout\,
	datad => \Equal0~0_combout\,
	combout => \Equal0~2_combout\);

-- Location: FF_X25_Y1_N31
s_qe : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_qe~q\);

-- Location: LCCOMB_X25_Y1_N28
\Buff|qn~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Buff|qn~0_combout\ = (\s_qe~q\ & (!\Buff|s_q~combout\)) # (!\s_qe~q\ & ((\Buff|qn~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_qe~q\,
	datac => \Buff|s_q~combout\,
	datad => \Buff|qn~q\,
	combout => \Buff|qn~0_combout\);

-- Location: LCCOMB_X25_Y1_N12
\Buff|qn~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Buff|qn~feeder_combout\ = \Buff|qn~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Buff|qn~0_combout\,
	combout => \Buff|qn~feeder_combout\);

-- Location: FF_X25_Y1_N13
\Buff|qn\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Buff|qn~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Buff|qn~q\);

-- Location: LCCOMB_X25_Y1_N22
\Buff|s_q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Buff|s_q~combout\ = (\s_qe~q\ & ((\Buff|qn~q\))) # (!\s_qe~q\ & (\Buff|s_q~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Buff|s_q~combout\,
	datac => \s_qe~q\,
	datad => \Buff|qn~q\,
	combout => \Buff|s_q~combout\);

-- Location: LCCOMB_X25_Y1_N24
\Count|s_ov~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Count|s_ov~0_combout\ = (\Count|s_ov~q\) # ((\Count|Equal0~1_combout\ & \Count|Equal0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Equal0~1_combout\,
	datac => \Count|s_ov~q\,
	datad => \Count|Equal0~0_combout\,
	combout => \Count|s_ov~0_combout\);

-- Location: FF_X25_Y1_N25
\Count|s_ov\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Count|s_ov~0_combout\,
	clrn => \ALT_INV_s_qe~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|s_ov~q\);

ww_DIV <= \DIV~output_o\;

ww_Err <= \Err~output_o\;
END structure;


