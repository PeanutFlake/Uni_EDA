LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY ClockDivider IS
	GENERIC (
		MemLimit	:	Integer	:= 255;
		Cycles	:	Integer	:= 4
	);
	PORT (
		CLK, RST		:	IN		std_logic;
		DIV, Err		:	OUT	std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF ClockDivider IS

	COMPONENT Counter IS
		GENERIC (
			Limit		:	Integer	:= 255
		);
		PORT (
			CLK, RST	:	IN		std_logic;
			Ov			:	OUT	std_logic;
			Q			:	OUT	integer range 0 to Limit
		);
	END COMPONENT;
	
	COMPONENT DFlipFlop IS
		PORT (
			D, CLK, CLR, EN	: IN	std_logic;
			Q, nQ					: OUT	std_logic	
		);
	END COMPONENT;

	SIGNAL s_qe, s_de, s_qd 	:	std_logic := '0';
	SIGNAL s_ov, s_clr, s_rst	:	std_logic := '0';
	SIGNAL s_q						:	integer range 0 to MemLimit := 0;
	
	
BEGIN

	Count : Counter
	GENERIC MAP (MemLimit)
	PORT MAP (CLK, s_rst, s_ov, s_q);
	
	Buff	:	DFlipFlop
	PORT MAP (
		D => s_de, 
		CLK => CLK, 
		CLR => s_clr,
		EN => s_qe, 
		Q => s_qd
	);
	
	Prog : PROCESS(CLK, RST, s_q)
	BEGIN
		if ( RST = '1' ) then
			s_qe <= '0';
			s_clr <= '1';
		else
			if ( rising_edge(CLK) ) then
				-- Cycles - 2 for hold time of Reset Signal
				if ( s_q = Cycles-2 ) then
					s_qe <= '1';
				else
					s_qe <= '0';
				end if;
			end if;
			s_clr <= '0';
		end if;
	END PROCESS Prog;
	
	s_rst <= s_qe OR RST;
	s_de <= NOT s_qd;
	DIV <= s_qd;
	Err <= s_ov;

END Behaviour;