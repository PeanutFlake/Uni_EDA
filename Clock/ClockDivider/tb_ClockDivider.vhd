LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_ClockDivider IS
END ENTITY;

ARCHITECTURE Testbench OF tb_ClockDivider IS

	COMPONENT ClockDivider IS
		GENERIC (
			MemLimit	:	Integer	:= 255;
			Cycles	:	Integer	:= 4
		);
		PORT (
			CLK			:	IN		std_logic;
			DIV, Err		:	OUT	std_logic
		);
	END COMPONENT;
	
	SIGNAL s_div, s_err 	: std_logic := '0';
	SIGNAL s_clk			: std_logic := '1';
BEGIN

	DUT	:	ClockDivider
	-- 134217728
	-- Pulsewidth = 1 / (T_clk - T_div) ; 10^6 = 1 / 10^(-9+3) => 1ms mit T_clk = 10ns (Pulsewidth=100000)
	GENERIC MAP (128, 100000)
	PORT MAP (s_clk, s_div, s_err);
	
	s_clk <= NOT s_clk AFTER 5ns;

END Testbench;