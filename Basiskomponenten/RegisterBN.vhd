LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY RegisterBN IS
	GENERIC(
		Bitsize	: integer := 8
	);
	PORT(
		LD, CLK, CLR	:	std_logic;
		D					:	std_logic_vector(Bitsize-1 downto 0);
		Q					:	out std_logic_vector(Bitsize-1 downto 0)
	);
END ENTITY;

ARCHITECTURE PIPO OF RegisterBN IS

	SIGNAL s_q			:	std_logic_vector(Bitsize-1 downto 0) := (Others => '0');

BEGIN

	PROCESS(CLK, CLR)
		VARIABLE dq		:	std_logic_vector(Bitsize-1 downto 0) := (Others => '0');
	BEGIN
		if ( CLR = '1' ) then
			dq := (Others => '0');
		else
			if ( CLK'event AND CLK='1' ) then
				if (LD = '1') then
					dq := D;
				else
					dq := dq;
				end if;
			else
				dq := dq;
			end if;		
		end if;
		s_q <= dq;
	END PROCESS;

	Q <= s_q;
	
END PIPO;

ARCHITECTURE PISO OF RegisterBN IS

	SIGNAL s_q			:	std_logic := '0';

BEGIN

	PROCESS(CLK, CLR)
		VARIABLE dq		:	std_logic_vector(Bitsize-1 downto 0) := (Others => '0');
		VARIABLE ind	:	integer range 0 to Bitsize := 0;
	BEGIN
		if ( CLR = '1' ) then
			dq := (Others => '0');
		else
			if ( CLK'event AND CLK='1' ) then
				if (LD = '1') then
					dq := D;
				else
					dq := dq;
				end if;
				ind := ind+1;
				if ( ind = Bitsize ) then
					ind := 0;
				else
					ind := ind;
				end if;
			else
				dq := dq;
			end if;		
		end if;
		s_q <= dq(ind);	
	END PROCESS;

	Q <= (0 => s_q, others => 'Z');

END PISO;

ARCHITECTURE SISO OF RegisterBN IS

	SIGNAL s_q			:	std_logic := '0';

BEGIN

	PROCESS(CLK, CLR)
		VARIABLE dq		:	std_logic_vector(Bitsize-1 downto 0) := (Others => '0');
		VARIABLE ind	:	integer range 0 to Bitsize := 0;
	BEGIN
		if ( CLR = '1' ) then
			dq := (Others => '0');
		else
			if ( CLK'event AND CLK='1' ) then
				if (LD = '1') then
					dq(ind) := D(ind);
				else
					dq := dq;
				end if;
				ind := ind+1;
				if ( ind = Bitsize ) then
					ind := 0;
				else
					ind := ind;
				end if;
			else
				dq := dq;
			end if;		
		end if;
		s_q <= dq(ind);
	END PROCESS;

	Q <= (0 => s_q, others => 'Z');

END SISO;

ARCHITECTURE SIPO OF RegisterBN IS

	SIGNAL s_q			:	std_logic_vector(Bitsize-1 downto 0) := (Others => '0');

BEGIN

	PROCESS(CLK, CLR)
		VARIABLE dq		:	std_logic_vector(Bitsize-1 downto 0) := (Others => '0');
		VARIABLE ind	:	integer range 0 to Bitsize := 0;
	BEGIN
		if ( CLR = '1' ) then
			dq := (Others => '0');
		else
			if ( CLK'event AND CLK='1' ) then
				if (LD = '1') then
					dq(ind) := D(0);
				else
					dq := dq;
				end if;
				ind := ind+1;
				if ( ind = Bitsize ) then
					ind := 0;
				else
					ind := ind;
				end if;
			else
				dq := dq;
			end if;		
		end if;
		s_q <= dq;
	END PROCESS;

	Q <= s_q;

END SIPO;