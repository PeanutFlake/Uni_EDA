-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/11/2018 15:03:53"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	RegisterB1xN IS
    PORT (
	CLK : IN std_logic;
	CLR : IN std_logic;
	LD : IN std_logic;
	D : IN std_logic;
	Q : BUFFER std_logic_vector(7 DOWNTO 0)
	);
END RegisterB1xN;

-- Design Ports Information
-- Q[0]	=>  Location: PIN_M9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[1]	=>  Location: PIN_N10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[2]	=>  Location: PIN_N8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[3]	=>  Location: PIN_K9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[4]	=>  Location: PIN_M11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[5]	=>  Location: PIN_L9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[6]	=>  Location: PIN_N9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[7]	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D	=>  Location: PIN_N11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LD	=>  Location: PIN_G13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_J7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLR	=>  Location: PIN_J6,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF RegisterB1xN IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_CLR : std_logic;
SIGNAL ww_LD : std_logic;
SIGNAL ww_D : std_logic;
SIGNAL ww_Q : std_logic_vector(7 DOWNTO 0);
SIGNAL \CLR~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \CLK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \Q[0]~output_o\ : std_logic;
SIGNAL \Q[1]~output_o\ : std_logic;
SIGNAL \Q[2]~output_o\ : std_logic;
SIGNAL \Q[3]~output_o\ : std_logic;
SIGNAL \Q[4]~output_o\ : std_logic;
SIGNAL \Q[5]~output_o\ : std_logic;
SIGNAL \Q[6]~output_o\ : std_logic;
SIGNAL \Q[7]~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \CLK~inputclkctrl_outclk\ : std_logic;
SIGNAL \LD~input_o\ : std_logic;
SIGNAL \D~input_o\ : std_logic;
SIGNAL \ind~3_combout\ : std_logic;
SIGNAL \CLR~input_o\ : std_logic;
SIGNAL \ind~1_combout\ : std_logic;
SIGNAL \Add0~0_combout\ : std_logic;
SIGNAL \ind~0_combout\ : std_logic;
SIGNAL \ind~2_combout\ : std_logic;
SIGNAL \load~0_combout\ : std_logic;
SIGNAL \load~q\ : std_logic;
SIGNAL \dq~0_combout\ : std_logic;
SIGNAL \dq~1_combout\ : std_logic;
SIGNAL \dq~2_combout\ : std_logic;
SIGNAL \CLR~inputclkctrl_outclk\ : std_logic;
SIGNAL \dq[7]~3_combout\ : std_logic;
SIGNAL \dq[1]~4_combout\ : std_logic;
SIGNAL \dq[1]~5_combout\ : std_logic;
SIGNAL \dq[3]~6_combout\ : std_logic;
SIGNAL \dq[2]~7_combout\ : std_logic;
SIGNAL \dq[3]~8_combout\ : std_logic;
SIGNAL \dq[4]~9_combout\ : std_logic;
SIGNAL \dq[4]~10_combout\ : std_logic;
SIGNAL \dq[5]~11_combout\ : std_logic;
SIGNAL \dq[5]~12_combout\ : std_logic;
SIGNAL \dq[7]~13_combout\ : std_logic;
SIGNAL \dq[6]~14_combout\ : std_logic;
SIGNAL \dq[7]~15_combout\ : std_logic;
SIGNAL dq : std_logic_vector(7 DOWNTO 0);
SIGNAL ind : std_logic_vector(3 DOWNTO 0);
SIGNAL \ALT_INV_CLR~inputclkctrl_outclk\ : std_logic;
SIGNAL \ALT_INV_CLR~input_o\ : std_logic;

BEGIN

ww_CLK <= CLK;
ww_CLR <= CLR;
ww_LD <= LD;
ww_D <= D;
Q <= ww_Q;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLR~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLR~input_o\);

\CLK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLK~input_o\);
\ALT_INV_CLR~inputclkctrl_outclk\ <= NOT \CLR~inputclkctrl_outclk\;
\ALT_INV_CLR~input_o\ <= NOT \CLR~input_o\;

-- Location: IOOBUF_X24_Y0_N2
\Q[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => dq(0),
	devoe => ww_devoe,
	o => \Q[0]~output_o\);

-- Location: IOOBUF_X26_Y0_N9
\Q[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => dq(1),
	devoe => ww_devoe,
	o => \Q[1]~output_o\);

-- Location: IOOBUF_X20_Y0_N9
\Q[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => dq(2),
	devoe => ww_devoe,
	o => \Q[2]~output_o\);

-- Location: IOOBUF_X22_Y0_N2
\Q[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => dq(3),
	devoe => ww_devoe,
	o => \Q[3]~output_o\);

-- Location: IOOBUF_X29_Y0_N9
\Q[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => dq(4),
	devoe => ww_devoe,
	o => \Q[4]~output_o\);

-- Location: IOOBUF_X24_Y0_N9
\Q[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => dq(5),
	devoe => ww_devoe,
	o => \Q[5]~output_o\);

-- Location: IOOBUF_X20_Y0_N2
\Q[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => dq(6),
	devoe => ww_devoe,
	o => \Q[6]~output_o\);

-- Location: IOOBUF_X22_Y0_N9
\Q[7]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => dq(7),
	devoe => ww_devoe,
	o => \Q[7]~output_o\);

-- Location: IOIBUF_X16_Y0_N15
\CLK~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: CLKCTRL_G17
\CLK~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLK~inputclkctrl_outclk\);

-- Location: IOIBUF_X33_Y16_N22
\LD~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_LD,
	o => \LD~input_o\);

-- Location: IOIBUF_X26_Y0_N1
\D~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D,
	o => \D~input_o\);

-- Location: LCCOMB_X26_Y5_N26
\ind~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind~3_combout\ = (\LD~input_o\) # (ind(3) $ (!ind(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101111101011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LD~input_o\,
	datab => ind(3),
	datac => ind(0),
	combout => \ind~3_combout\);

-- Location: IOIBUF_X16_Y0_N22
\CLR~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLR,
	o => \CLR~input_o\);

-- Location: FF_X26_Y5_N27
\ind[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \ind~3_combout\,
	ena => \ALT_INV_CLR~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => ind(0));

-- Location: LCCOMB_X26_Y5_N20
\ind~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind~1_combout\ = (!\LD~input_o\ & (ind(1) $ (((!ind(3) & ind(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LD~input_o\,
	datab => ind(3),
	datac => ind(1),
	datad => ind(0),
	combout => \ind~1_combout\);

-- Location: FF_X26_Y5_N21
\ind[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \ind~1_combout\,
	ena => \ALT_INV_CLR~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => ind(1));

-- Location: LCCOMB_X25_Y5_N6
\Add0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Add0~0_combout\ = (\LD~input_o\) # ((!ind(0)) # (!ind(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LD~input_o\,
	datac => ind(1),
	datad => ind(0),
	combout => \Add0~0_combout\);

-- Location: LCCOMB_X25_Y5_N0
\ind~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind~0_combout\ = (\LD~input_o\ & (((!\Add0~0_combout\)))) # (!\LD~input_o\ & (ind(2) $ (((!ind(3) & !\Add0~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011101101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ind(3),
	datab => \LD~input_o\,
	datac => ind(2),
	datad => \Add0~0_combout\,
	combout => \ind~0_combout\);

-- Location: FF_X25_Y5_N1
\ind[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \ind~0_combout\,
	ena => \ALT_INV_CLR~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => ind(2));

-- Location: LCCOMB_X25_Y5_N4
\ind~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind~2_combout\ = (!\LD~input_o\ & ((ind(3)) # ((ind(2) & !\Add0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LD~input_o\,
	datab => ind(2),
	datac => ind(3),
	datad => \Add0~0_combout\,
	combout => \ind~2_combout\);

-- Location: FF_X25_Y5_N5
\ind[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \ind~2_combout\,
	ena => \ALT_INV_CLR~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => ind(3));

-- Location: LCCOMB_X25_Y5_N10
\load~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \load~0_combout\ = (\LD~input_o\) # ((!ind(3) & \load~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ind(3),
	datac => \load~q\,
	datad => \LD~input_o\,
	combout => \load~0_combout\);

-- Location: FF_X25_Y5_N11
load : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \load~0_combout\,
	ena => \ALT_INV_CLR~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \load~q\);

-- Location: LCCOMB_X25_Y5_N18
\dq~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq~0_combout\ = (!ind(3) & !ind(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => ind(3),
	datad => ind(0),
	combout => \dq~0_combout\);

-- Location: LCCOMB_X25_Y5_N28
\dq~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq~1_combout\ = (\load~q\ & (!ind(2) & (!ind(1) & \dq~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \load~q\,
	datab => ind(2),
	datac => ind(1),
	datad => \dq~0_combout\,
	combout => \dq~1_combout\);

-- Location: LCCOMB_X25_Y5_N24
\dq~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq~2_combout\ = (\LD~input_o\ & (\D~input_o\)) # (!\LD~input_o\ & ((\dq~1_combout\ & (\D~input_o\)) # (!\dq~1_combout\ & ((dq(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LD~input_o\,
	datab => \D~input_o\,
	datac => dq(0),
	datad => \dq~1_combout\,
	combout => \dq~2_combout\);

-- Location: CLKCTRL_G19
\CLR~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLR~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLR~inputclkctrl_outclk\);

-- Location: FF_X25_Y5_N25
\dq[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \dq~2_combout\,
	clrn => \ALT_INV_CLR~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(0));

-- Location: LCCOMB_X25_Y5_N14
\dq[7]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[7]~3_combout\ = (!\LD~input_o\ & (!ind(3) & \load~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LD~input_o\,
	datac => ind(3),
	datad => \load~q\,
	combout => \dq[7]~3_combout\);

-- Location: LCCOMB_X26_Y5_N28
\dq[1]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[1]~4_combout\ = (ind(0) & (!ind(1) & (!ind(2) & \dq[7]~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ind(0),
	datab => ind(1),
	datac => ind(2),
	datad => \dq[7]~3_combout\,
	combout => \dq[1]~4_combout\);

-- Location: LCCOMB_X26_Y5_N16
\dq[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[1]~5_combout\ = (\dq[1]~4_combout\ & (\D~input_o\)) # (!\dq[1]~4_combout\ & ((dq(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D~input_o\,
	datac => dq(1),
	datad => \dq[1]~4_combout\,
	combout => \dq[1]~5_combout\);

-- Location: FF_X26_Y5_N17
\dq[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \dq[1]~5_combout\,
	clrn => \ALT_INV_CLR~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(1));

-- Location: LCCOMB_X25_Y5_N16
\dq[3]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[3]~6_combout\ = (!ind(2) & (\dq[7]~3_combout\ & ind(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => ind(2),
	datac => \dq[7]~3_combout\,
	datad => ind(1),
	combout => \dq[3]~6_combout\);

-- Location: LCCOMB_X25_Y5_N30
\dq[2]~7\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[2]~7_combout\ = (ind(0) & (((dq(2))))) # (!ind(0) & ((\dq[3]~6_combout\ & (\D~input_o\)) # (!\dq[3]~6_combout\ & ((dq(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D~input_o\,
	datab => ind(0),
	datac => dq(2),
	datad => \dq[3]~6_combout\,
	combout => \dq[2]~7_combout\);

-- Location: FF_X25_Y5_N31
\dq[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \dq[2]~7_combout\,
	clrn => \ALT_INV_CLR~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(2));

-- Location: LCCOMB_X25_Y5_N12
\dq[3]~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[3]~8_combout\ = (ind(0) & ((\dq[3]~6_combout\ & (\D~input_o\)) # (!\dq[3]~6_combout\ & ((dq(3)))))) # (!ind(0) & (((dq(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D~input_o\,
	datab => ind(0),
	datac => dq(3),
	datad => \dq[3]~6_combout\,
	combout => \dq[3]~8_combout\);

-- Location: FF_X25_Y5_N13
\dq[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \dq[3]~8_combout\,
	clrn => \ALT_INV_CLR~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(3));

-- Location: LCCOMB_X26_Y5_N18
\dq[4]~9\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[4]~9_combout\ = (!ind(0) & (!ind(1) & (ind(2) & \dq[7]~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ind(0),
	datab => ind(1),
	datac => ind(2),
	datad => \dq[7]~3_combout\,
	combout => \dq[4]~9_combout\);

-- Location: LCCOMB_X26_Y5_N30
\dq[4]~10\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[4]~10_combout\ = (\dq[4]~9_combout\ & (\D~input_o\)) # (!\dq[4]~9_combout\ & ((dq(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D~input_o\,
	datac => dq(4),
	datad => \dq[4]~9_combout\,
	combout => \dq[4]~10_combout\);

-- Location: FF_X26_Y5_N31
\dq[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \dq[4]~10_combout\,
	clrn => \ALT_INV_CLR~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(4));

-- Location: LCCOMB_X25_Y5_N2
\dq[5]~11\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[5]~11_combout\ = (!ind(1) & (ind(2) & (\dq[7]~3_combout\ & ind(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ind(1),
	datab => ind(2),
	datac => \dq[7]~3_combout\,
	datad => ind(0),
	combout => \dq[5]~11_combout\);

-- Location: LCCOMB_X25_Y5_N26
\dq[5]~12\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[5]~12_combout\ = (\dq[5]~11_combout\ & (\D~input_o\)) # (!\dq[5]~11_combout\ & ((dq(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \D~input_o\,
	datac => dq(5),
	datad => \dq[5]~11_combout\,
	combout => \dq[5]~12_combout\);

-- Location: FF_X25_Y5_N27
\dq[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \dq[5]~12_combout\,
	clrn => \ALT_INV_CLR~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(5));

-- Location: LCCOMB_X25_Y5_N20
\dq[7]~13\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[7]~13_combout\ = (ind(2) & (\dq[7]~3_combout\ & ind(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => ind(2),
	datac => \dq[7]~3_combout\,
	datad => ind(1),
	combout => \dq[7]~13_combout\);

-- Location: LCCOMB_X25_Y5_N8
\dq[6]~14\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[6]~14_combout\ = (ind(0) & (((dq(6))))) # (!ind(0) & ((\dq[7]~13_combout\ & (\D~input_o\)) # (!\dq[7]~13_combout\ & ((dq(6))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D~input_o\,
	datab => ind(0),
	datac => dq(6),
	datad => \dq[7]~13_combout\,
	combout => \dq[6]~14_combout\);

-- Location: FF_X25_Y5_N9
\dq[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \dq[6]~14_combout\,
	clrn => \ALT_INV_CLR~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(6));

-- Location: LCCOMB_X25_Y5_N22
\dq[7]~15\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[7]~15_combout\ = (ind(0) & ((\dq[7]~13_combout\ & (\D~input_o\)) # (!\dq[7]~13_combout\ & ((dq(7)))))) # (!ind(0) & (((dq(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D~input_o\,
	datab => ind(0),
	datac => dq(7),
	datad => \dq[7]~13_combout\,
	combout => \dq[7]~15_combout\);

-- Location: FF_X25_Y5_N23
\dq[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \dq[7]~15_combout\,
	clrn => \ALT_INV_CLR~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(7));

ww_Q(0) <= \Q[0]~output_o\;

ww_Q(1) <= \Q[1]~output_o\;

ww_Q(2) <= \Q[2]~output_o\;

ww_Q(3) <= \Q[3]~output_o\;

ww_Q(4) <= \Q[4]~output_o\;

ww_Q(5) <= \Q[5]~output_o\;

ww_Q(6) <= \Q[6]~output_o\;

ww_Q(7) <= \Q[7]~output_o\;
END structure;


