-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/11/2018 15:31:00"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	RegisterBNx1 IS
    PORT (
	CLK : IN std_logic;
	CLR : IN std_logic;
	LD : IN std_logic;
	RD : IN std_logic;
	D : IN std_logic_vector(7 DOWNTO 0);
	Q : OUT std_logic
	);
END RegisterBNx1;

-- Design Ports Information
-- Q	=>  Location: PIN_N6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[6]	=>  Location: PIN_N4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_J7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLR	=>  Location: PIN_L7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LD	=>  Location: PIN_N8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[5]	=>  Location: PIN_N9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[4]	=>  Location: PIN_K9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[7]	=>  Location: PIN_M6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[2]	=>  Location: PIN_L4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[1]	=>  Location: PIN_L5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[0]	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[3]	=>  Location: PIN_G13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RD	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF RegisterBNx1 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_CLR : std_logic;
SIGNAL ww_LD : std_logic;
SIGNAL ww_RD : std_logic;
SIGNAL ww_D : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_Q : std_logic;
SIGNAL \CLK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \dt~1clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \Q~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \CLK~inputclkctrl_outclk\ : std_logic;
SIGNAL \D[1]~input_o\ : std_logic;
SIGNAL \dq[1]~feeder_combout\ : std_logic;
SIGNAL \CLR~input_o\ : std_logic;
SIGNAL \LD~input_o\ : std_logic;
SIGNAL \dt~1clkctrl_outclk\ : std_logic;
SIGNAL \ind[0]~6_combout\ : std_logic;
SIGNAL \ind[1]~0_combout\ : std_logic;
SIGNAL \RD~input_o\ : std_logic;
SIGNAL \ind[0]~_emulated_q\ : std_logic;
SIGNAL \ind[0]~8_combout\ : std_logic;
SIGNAL \ind[0]~7_combout\ : std_logic;
SIGNAL \Add0~1_combout\ : std_logic;
SIGNAL \ind[2]~11_combout\ : std_logic;
SIGNAL \ind[2]~_emulated_q\ : std_logic;
SIGNAL \ind[2]~13_combout\ : std_logic;
SIGNAL \ind[2]~12_combout\ : std_logic;
SIGNAL \Add0~2_combout\ : std_logic;
SIGNAL \ind[3]~16_combout\ : std_logic;
SIGNAL \ind[3]~_emulated_q\ : std_logic;
SIGNAL \ind[3]~18_combout\ : std_logic;
SIGNAL \ind[3]~17_combout\ : std_logic;
SIGNAL \dt~1_combout\ : std_logic;
SIGNAL \Add0~0_combout\ : std_logic;
SIGNAL \ind[1]~1_combout\ : std_logic;
SIGNAL \ind[1]~_emulated_q\ : std_logic;
SIGNAL \ind[1]~3_combout\ : std_logic;
SIGNAL \ind[1]~2_combout\ : std_logic;
SIGNAL \D[0]~input_o\ : std_logic;
SIGNAL \Mux0~2_combout\ : std_logic;
SIGNAL \D[3]~input_o\ : std_logic;
SIGNAL \D[2]~input_o\ : std_logic;
SIGNAL \dq[2]~feeder_combout\ : std_logic;
SIGNAL \Mux0~3_combout\ : std_logic;
SIGNAL \D[4]~input_o\ : std_logic;
SIGNAL \D[5]~input_o\ : std_logic;
SIGNAL \Mux0~0_combout\ : std_logic;
SIGNAL \D[7]~input_o\ : std_logic;
SIGNAL \D[6]~input_o\ : std_logic;
SIGNAL \dq[6]~feeder_combout\ : std_logic;
SIGNAL \Mux0~1_combout\ : std_logic;
SIGNAL \Mux0~4_combout\ : std_logic;
SIGNAL dq : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_CLR~input_o\ : std_logic;
SIGNAL \ALT_INV_ind[1]~0_combout\ : std_logic;

BEGIN

ww_CLK <= CLK;
ww_CLR <= CLR;
ww_LD <= LD;
ww_RD <= RD;
ww_D <= D;
Q <= ww_Q;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLK~input_o\);

\dt~1clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \dt~1_combout\);
\ALT_INV_CLR~input_o\ <= NOT \CLR~input_o\;
\ALT_INV_ind[1]~0_combout\ <= NOT \ind[1]~0_combout\;

-- Location: IOOBUF_X12_Y0_N2
\Q~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux0~4_combout\,
	oe => \dt~1_combout\,
	devoe => ww_devoe,
	o => \Q~output_o\);

-- Location: IOIBUF_X16_Y0_N15
\CLK~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: CLKCTRL_G17
\CLK~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLK~inputclkctrl_outclk\);

-- Location: IOIBUF_X14_Y0_N8
\D[1]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(1),
	o => \D[1]~input_o\);

-- Location: LCCOMB_X14_Y1_N2
\dq[1]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[1]~feeder_combout\ = \D[1]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \D[1]~input_o\,
	combout => \dq[1]~feeder_combout\);

-- Location: IOIBUF_X14_Y0_N1
\CLR~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLR,
	o => \CLR~input_o\);

-- Location: IOIBUF_X20_Y0_N8
\LD~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_LD,
	o => \LD~input_o\);

-- Location: FF_X14_Y1_N3
\dq[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \dq[1]~feeder_combout\,
	clrn => \ALT_INV_CLR~input_o\,
	ena => \LD~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(1));

-- Location: CLKCTRL_G16
\dt~1clkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \dt~1clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \dt~1clkctrl_outclk\);

-- Location: LCCOMB_X15_Y1_N22
\ind[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind[0]~6_combout\ = (!\CLR~input_o\ & ((GLOBAL(\dt~1clkctrl_outclk\) & (!\ind[0]~7_combout\)) # (!GLOBAL(\dt~1clkctrl_outclk\) & ((\ind[0]~6_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CLR~input_o\,
	datab => \ind[0]~7_combout\,
	datac => \ind[0]~6_combout\,
	datad => \dt~1clkctrl_outclk\,
	combout => \ind[0]~6_combout\);

-- Location: LCCOMB_X15_Y1_N8
\ind[1]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind[1]~0_combout\ = (\CLR~input_o\) # (\dt~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \CLR~input_o\,
	datad => \dt~1_combout\,
	combout => \ind[1]~0_combout\);

-- Location: IOIBUF_X33_Y16_N15
\RD~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_RD,
	o => \RD~input_o\);

-- Location: FF_X15_Y1_N23
\ind[0]~_emulated\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ind[0]~6_combout\,
	clrn => \ALT_INV_ind[1]~0_combout\,
	ena => \RD~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ind[0]~_emulated_q\);

-- Location: LCCOMB_X15_Y1_N0
\ind[0]~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind[0]~8_combout\ = \ind[0]~6_combout\ $ (\ind[0]~_emulated_q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ind[0]~6_combout\,
	datad => \ind[0]~_emulated_q\,
	combout => \ind[0]~8_combout\);

-- Location: LCCOMB_X15_Y1_N28
\ind[0]~7\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind[0]~7_combout\ = (!\CLR~input_o\ & ((\dt~1_combout\ & (!\ind[0]~7_combout\)) # (!\dt~1_combout\ & ((\ind[0]~8_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dt~1_combout\,
	datab => \ind[0]~7_combout\,
	datac => \CLR~input_o\,
	datad => \ind[0]~8_combout\,
	combout => \ind[0]~7_combout\);

-- Location: LCCOMB_X15_Y1_N18
\Add0~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Add0~1_combout\ = \ind[2]~12_combout\ $ (((\ind[1]~2_combout\ & \ind[0]~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \ind[1]~2_combout\,
	datac => \ind[2]~12_combout\,
	datad => \ind[0]~7_combout\,
	combout => \Add0~1_combout\);

-- Location: LCCOMB_X15_Y1_N26
\ind[2]~11\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind[2]~11_combout\ = (!\CLR~input_o\ & ((GLOBAL(\dt~1clkctrl_outclk\) & ((\Add0~1_combout\))) # (!GLOBAL(\dt~1clkctrl_outclk\) & (\ind[2]~11_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CLR~input_o\,
	datab => \ind[2]~11_combout\,
	datac => \Add0~1_combout\,
	datad => \dt~1clkctrl_outclk\,
	combout => \ind[2]~11_combout\);

-- Location: FF_X15_Y1_N27
\ind[2]~_emulated\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ind[2]~11_combout\,
	clrn => \ALT_INV_ind[1]~0_combout\,
	ena => \RD~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ind[2]~_emulated_q\);

-- Location: LCCOMB_X15_Y1_N24
\ind[2]~13\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind[2]~13_combout\ = \ind[2]~_emulated_q\ $ (\ind[2]~11_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ind[2]~_emulated_q\,
	datad => \ind[2]~11_combout\,
	combout => \ind[2]~13_combout\);

-- Location: LCCOMB_X15_Y1_N30
\ind[2]~12\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind[2]~12_combout\ = (!\CLR~input_o\ & ((\dt~1_combout\ & (\Add0~1_combout\)) # (!\dt~1_combout\ & ((\ind[2]~13_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dt~1_combout\,
	datab => \Add0~1_combout\,
	datac => \CLR~input_o\,
	datad => \ind[2]~13_combout\,
	combout => \ind[2]~12_combout\);

-- Location: LCCOMB_X15_Y1_N20
\Add0~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Add0~2_combout\ = \ind[3]~17_combout\ $ (((\ind[2]~12_combout\ & (\ind[1]~2_combout\ & \ind[0]~7_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ind[2]~12_combout\,
	datab => \ind[1]~2_combout\,
	datac => \ind[0]~7_combout\,
	datad => \ind[3]~17_combout\,
	combout => \Add0~2_combout\);

-- Location: LCCOMB_X15_Y1_N12
\ind[3]~16\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind[3]~16_combout\ = (\CLR~input_o\) # ((GLOBAL(\dt~1clkctrl_outclk\) & ((\Add0~2_combout\))) # (!GLOBAL(\dt~1clkctrl_outclk\) & (\ind[3]~16_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ind[3]~16_combout\,
	datab => \CLR~input_o\,
	datac => \Add0~2_combout\,
	datad => \dt~1clkctrl_outclk\,
	combout => \ind[3]~16_combout\);

-- Location: FF_X15_Y1_N13
\ind[3]~_emulated\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ind[3]~16_combout\,
	clrn => \ALT_INV_ind[1]~0_combout\,
	ena => \RD~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ind[3]~_emulated_q\);

-- Location: LCCOMB_X15_Y1_N6
\ind[3]~18\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind[3]~18_combout\ = \ind[3]~_emulated_q\ $ (\ind[3]~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ind[3]~_emulated_q\,
	datad => \ind[3]~16_combout\,
	combout => \ind[3]~18_combout\);

-- Location: LCCOMB_X14_Y1_N14
\ind[3]~17\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind[3]~17_combout\ = (\CLR~input_o\) # ((\dt~1_combout\ & (\Add0~2_combout\)) # (!\dt~1_combout\ & ((\ind[3]~18_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dt~1_combout\,
	datab => \Add0~2_combout\,
	datac => \CLR~input_o\,
	datad => \ind[3]~18_combout\,
	combout => \ind[3]~17_combout\);

-- Location: LCCOMB_X15_Y1_N16
\dt~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dt~1_combout\ = (!\ind[3]~17_combout\ & !\CLR~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ind[3]~17_combout\,
	datad => \CLR~input_o\,
	combout => \dt~1_combout\);

-- Location: LCCOMB_X15_Y1_N10
\Add0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Add0~0_combout\ = \ind[1]~2_combout\ $ (\ind[0]~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ind[1]~2_combout\,
	datad => \ind[0]~7_combout\,
	combout => \Add0~0_combout\);

-- Location: LCCOMB_X15_Y1_N4
\ind[1]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind[1]~1_combout\ = (!\CLR~input_o\ & ((GLOBAL(\dt~1clkctrl_outclk\) & ((\Add0~0_combout\))) # (!GLOBAL(\dt~1clkctrl_outclk\) & (\ind[1]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ind[1]~1_combout\,
	datab => \Add0~0_combout\,
	datac => \CLR~input_o\,
	datad => \dt~1clkctrl_outclk\,
	combout => \ind[1]~1_combout\);

-- Location: FF_X15_Y1_N5
\ind[1]~_emulated\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ind[1]~1_combout\,
	clrn => \ALT_INV_ind[1]~0_combout\,
	ena => \RD~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ind[1]~_emulated_q\);

-- Location: LCCOMB_X15_Y1_N2
\ind[1]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind[1]~3_combout\ = \ind[1]~_emulated_q\ $ (\ind[1]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ind[1]~_emulated_q\,
	datad => \ind[1]~1_combout\,
	combout => \ind[1]~3_combout\);

-- Location: LCCOMB_X15_Y1_N14
\ind[1]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \ind[1]~2_combout\ = (!\CLR~input_o\ & ((\dt~1_combout\ & (\Add0~0_combout\)) # (!\dt~1_combout\ & ((\ind[1]~3_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dt~1_combout\,
	datab => \Add0~0_combout\,
	datac => \CLR~input_o\,
	datad => \ind[1]~3_combout\,
	combout => \ind[1]~2_combout\);

-- Location: IOIBUF_X22_Y0_N8
\D[0]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(0),
	o => \D[0]~input_o\);

-- Location: FF_X14_Y1_N21
\dq[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \D[0]~input_o\,
	clrn => \ALT_INV_CLR~input_o\,
	sload => VCC,
	ena => \LD~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(0));

-- Location: LCCOMB_X14_Y1_N20
\Mux0~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Mux0~2_combout\ = (\ind[1]~2_combout\ & (((\ind[0]~7_combout\)))) # (!\ind[1]~2_combout\ & ((\ind[0]~7_combout\ & (dq(1))) # (!\ind[0]~7_combout\ & ((dq(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => dq(1),
	datab => \ind[1]~2_combout\,
	datac => dq(0),
	datad => \ind[0]~7_combout\,
	combout => \Mux0~2_combout\);

-- Location: IOIBUF_X33_Y16_N22
\D[3]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(3),
	o => \D[3]~input_o\);

-- Location: FF_X14_Y1_N23
\dq[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \D[3]~input_o\,
	clrn => \ALT_INV_CLR~input_o\,
	sload => VCC,
	ena => \LD~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(3));

-- Location: IOIBUF_X8_Y0_N8
\D[2]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(2),
	o => \D[2]~input_o\);

-- Location: LCCOMB_X14_Y1_N12
\dq[2]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[2]~feeder_combout\ = \D[2]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \D[2]~input_o\,
	combout => \dq[2]~feeder_combout\);

-- Location: FF_X14_Y1_N13
\dq[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \dq[2]~feeder_combout\,
	clrn => \ALT_INV_CLR~input_o\,
	ena => \LD~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(2));

-- Location: LCCOMB_X14_Y1_N22
\Mux0~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Mux0~3_combout\ = (\Mux0~2_combout\ & (((dq(3))) # (!\ind[1]~2_combout\))) # (!\Mux0~2_combout\ & (\ind[1]~2_combout\ & ((dq(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux0~2_combout\,
	datab => \ind[1]~2_combout\,
	datac => dq(3),
	datad => dq(2),
	combout => \Mux0~3_combout\);

-- Location: IOIBUF_X22_Y0_N1
\D[4]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(4),
	o => \D[4]~input_o\);

-- Location: FF_X14_Y1_N5
\dq[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \D[4]~input_o\,
	clrn => \ALT_INV_CLR~input_o\,
	sload => VCC,
	ena => \LD~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(4));

-- Location: IOIBUF_X20_Y0_N1
\D[5]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(5),
	o => \D[5]~input_o\);

-- Location: FF_X14_Y1_N11
\dq[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \D[5]~input_o\,
	clrn => \ALT_INV_CLR~input_o\,
	sload => VCC,
	ena => \LD~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(5));

-- Location: LCCOMB_X14_Y1_N4
\Mux0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Mux0~0_combout\ = (\ind[0]~7_combout\ & ((\ind[1]~2_combout\) # ((dq(5))))) # (!\ind[0]~7_combout\ & (!\ind[1]~2_combout\ & (dq(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ind[0]~7_combout\,
	datab => \ind[1]~2_combout\,
	datac => dq(4),
	datad => dq(5),
	combout => \Mux0~0_combout\);

-- Location: IOIBUF_X12_Y0_N8
\D[7]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(7),
	o => \D[7]~input_o\);

-- Location: FF_X14_Y1_N19
\dq[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \D[7]~input_o\,
	clrn => \ALT_INV_CLR~input_o\,
	sload => VCC,
	ena => \LD~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(7));

-- Location: IOIBUF_X10_Y0_N8
\D[6]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(6),
	o => \D[6]~input_o\);

-- Location: LCCOMB_X14_Y1_N28
\dq[6]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dq[6]~feeder_combout\ = \D[6]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \D[6]~input_o\,
	combout => \dq[6]~feeder_combout\);

-- Location: FF_X14_Y1_N29
\dq[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \dq[6]~feeder_combout\,
	clrn => \ALT_INV_CLR~input_o\,
	ena => \LD~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => dq(6));

-- Location: LCCOMB_X14_Y1_N18
\Mux0~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Mux0~1_combout\ = (\Mux0~0_combout\ & (((dq(7))) # (!\ind[1]~2_combout\))) # (!\Mux0~0_combout\ & (\ind[1]~2_combout\ & ((dq(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux0~0_combout\,
	datab => \ind[1]~2_combout\,
	datac => dq(7),
	datad => dq(6),
	combout => \Mux0~1_combout\);

-- Location: LCCOMB_X14_Y1_N24
\Mux0~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Mux0~4_combout\ = (\ind[2]~12_combout\ & ((\Mux0~1_combout\))) # (!\ind[2]~12_combout\ & (\Mux0~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux0~3_combout\,
	datab => \Mux0~1_combout\,
	datad => \ind[2]~12_combout\,
	combout => \Mux0~4_combout\);

ww_Q <= \Q~output_o\;
END structure;


