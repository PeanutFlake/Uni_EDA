LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY DFF IS
	PORT (
		D, CLK, CLR		: IN	std_logic;
		Q, nQ				: OUT	std_logic	
	);
END ENTITY;

ARCHITECTURE Behaviour OF DFF IS

	SIGNAL s_q	:	std_logic := '0';

BEGIN

	PROCESS(CLK) IS
	
	BEGIN
		if ( CLK'event AND CLK='1' ) then
			s_q <= D;
		else
			-- For Not Compile as Latch
		end if;
	END PROCESS;

	s_q <= '0' WHEN CLR='1' else s_q;
	Q <= s_q;
	nQ <= NOT s_q;
	
END Behaviour;