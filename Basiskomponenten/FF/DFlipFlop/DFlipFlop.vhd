LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY DFlipFlop IS
	PORT (
		D, CLK, CLR, EN	: IN	std_logic;
		Q, nQ					: OUT	std_logic	
	);
END ENTITY;

ARCHITECTURE Behaviour OF DFlipFlop IS

	SIGNAL s_q	:	std_logic := '0';

BEGIN

	PROCESS(CLK, CLR, EN) IS
		VARIABLE qn : std_logic := '0';
	BEGIN
		if ( EN = '1' ) then
			if ( CLR = '1' ) then 
				qn := '0';
			else
				if ( CLK'event AND CLK='1' ) then
					qn := D;
				else
					-- For Not Compile as Latch
					qn := qn;
				end if;
			end if;
			s_q <= qn;
		else
			qn := qn;
		end if;
	END PROCESS;

	Q <= s_q;
	nQ <= NOT s_q;
	
END Behaviour;