LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_ClockDivider IS
END ENTITY;

ARCHITECTURE Testbench OF tb_ClockDivider IS

	COMPONENT ClockDivider IS
		GENERIC (
			MemLimit	:	Integer	:= 255;
			Cycles	:	Integer	:= 4
		);
		PORT (
			CLK			:	IN		std_logic;
			DIV, Err		:	OUT	std_logic
		);
	END COMPONENT;

BEGIN

END Testbench;