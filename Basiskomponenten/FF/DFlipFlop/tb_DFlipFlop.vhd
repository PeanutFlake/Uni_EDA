LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_DFlipFlop IS
END ENTITY;

ARCHITECTURE Testbench OF tb_DFlipFlop IS

	COMPONENT DFlipFlop IS
		PORT (
			D, CLK, CLR		: IN	std_logic;
			Q, nQ				: OUT	std_logic	
		);
	END COMPONENT;

	SIGNAL s_d, s_clk, s_clr, s_q	:	std_logic	:=	'0';
	
BEGIN

	FF	:	DFlipFlop
	PORT MAP (s_d, s_clk, s_clr, s_q);
	
	s_clk	<=	NOT s_clk AFTER 10ns;
	
	s_d 	<= '1' AFTER 55ns, '0' AFTER 120ns, '1' AFTER 160ns;
	s_clr <= '1' AFTER 90ns, '0' AFTER 110ns;

END Testbench;