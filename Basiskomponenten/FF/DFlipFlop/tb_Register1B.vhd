LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_Register1B IS
END ENTITY;

ARCHITECTURE Testbench OF tb_Register1B IS

	COMPONENT Register1B IS
		PORT (
			LD, D, CLR, CLK	:	std_logic;
			Q						:	OUT std_logic
		);
	END COMPONENT;

	SIGNAL s_ld, s_d, s_clr, s_clk, s_q	:	std_logic := '0';
	
BEGIN

	DUT	:	Register1B
	PORT MAP (s_ld, s_d, s_clr, s_clk, s_q);

	s_clk	<= NOT s_clk AFTER 10ns;

	s_d <= '1' AFTER 55ns, '0' AFTER 120ns, '1' AFTER 200ns;
	s_ld <= '1' AFTER 75ns, '0' AFTER 90ns, '1' AFTER 145ns, '0' AFTER 160ns;
	s_clr <= '1' AFTER 100ns, '0' AFTER 110ns;
	
END Testbench;