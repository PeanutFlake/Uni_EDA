LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY RegisterB1 IS
	PORT (
		LD, D, CLR, CLK	:	IN std_logic;
		Q						:	OUT std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF RegisterB1 IS

	COMPONENT Multiplexer2x1 IS
		PORT (
			a, b, s	:	IN std_logic;
			y			:	OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT DFlipFlop IS
		PORT (
			D, CLK, CLR		: IN	std_logic;
			Q, nQ				: OUT	std_logic
		);
	END COMPONENT;

	SIGNAL s_d, s_q		:	std_logic := '0';
	
BEGIN

	MULT	:	Multiplexer2x1
	PORT MAP (s_q, D, LD, s_d);
	
	FF		:	DFlipFlop
	PORT MAP (D => s_d, CLK => CLK, CLR => CLR, Q => s_q);

	Q <= s_q;
	
END Behaviour;