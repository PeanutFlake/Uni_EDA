LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_RegisterB1 IS
END ENTITY;

ARCHITECTURE Testbench OF tb_RegisterB1 IS

	COMPONENT RegisterB1 IS
		PORT (
			LD, D, CLR, CLK	:	IN std_logic;
			Q						:	OUT std_logic
		);
	END COMPONENT;

	SIGNAL s_ld, s_d, s_clr, s_clk, s_q	:	std_logic := '0';
	
BEGIN

	DUT	:	RegisterB1
	PORT MAP (LD => s_ld, D => s_d, CLR => s_clr, CLK => s_clk, Q => s_q);

	s_clk	<= NOT s_clk AFTER 10ns;

	s_d <= '1' AFTER 55ns, '0' AFTER 155ns, '1' AFTER 200ns;
	s_ld <= '1' AFTER 75ns, '0' AFTER 95ns, '1' AFTER 125ns, '0' AFTER 170ns, '1' AFTER 220ns, '0' AFTER 250ns;
	s_clr <= '1' AFTER 100ns, '0' AFTER 115ns;
	
END Testbench;