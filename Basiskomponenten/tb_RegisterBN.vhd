LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_RegisterBN IS
END ENTITY;

ARCHITECTURE Testbench OF tb_RegisterBN IS

	COMPONENT RegisterBN IS
		GENERIC(
			Bitsize	: integer := 8
		);
		PORT(
			LD, CLK, CLR	:	std_logic;
			D					:	std_logic_vector(Bitsize-1 downto 0);
			Q					:	out std_logic_vector(Bitsize-1 downto 0)
		);
	END COMPONENT;

	SIGNAL s_ld, s_clk, s_clr	:	std_logic := '0';
	SIGNAL s_d, s_q				:	std_logic_vector(7 downto 0) := (Others => '0');
	
BEGIN

	DUT	:	entity work.RegisterBN(SISO)
	GENERIC MAP(8)
	PORT MAP(s_ld, s_clk, s_clr, s_d, s_q);

	s_clk <= NOT s_clk AFTER 10ns;
	
	s_d <= "11001100" AFTER 50ns, "00000000" AFTER 120ns, "11111111" AFTER 200ns, "00000000" AFTER 280ns;
	s_ld <= '1' AFTER 70ns, '0' AFTER 100ns, '1' AFTER 245ns, '0' AFTER 260ns, '1' AFTER 300ns, '0' AFTER 320ns;
	s_clr <= '1' AFTER 210ns, '0' AFTER 225ns;

END Testbench;