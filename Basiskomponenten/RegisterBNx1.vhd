LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY RegisterBNx1 IS
	GENERIC (
		Bitsize	:	integer	:= 8
	);
	PORT (
		CLK, CLR, LD, RD	:			std_logic;
		D						:			std_logic_vector(Bitsize-1 downto 0);
		Q						:	OUT	std_logic
	);
END ENTITY;

ARCHITECTURE PISO OF RegisterBNx1 IS

BEGIN

	PROCESS(CLK, CLR)
		VARIABLE dt		:	std_logic := '0';
		VARIABLE dq		:	std_logic_vector(Bitsize-1 downto 0) := (Others => '0');
		VARIABLE ind	:	integer range 0 to Bitsize := 0;
	BEGIN
		if ( CLR = '1' ) then
			dq := (Others => '0');
			dt := 'Z';
			ind := Bitsize;
		else
			if ( CLK'event AND CLK='1' ) then
				if (LD = '1') then
					dq := D;
				else
					dq := dq;
				end if;
				if ( RD = '1' ) then
					ind := 0;
				end if;
				if ( ind < Bitsize ) then
					dt := dq(ind);
					ind := ind+1;
				else
					dt := 'Z';
				end if;
			else
				dq := dq;
			end if;
		end if;
		Q <= dt;
	END PROCESS;
END PISO;