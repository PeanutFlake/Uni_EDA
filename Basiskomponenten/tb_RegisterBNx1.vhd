LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_RegisterBNx1 IS
END ENTITY;

ARCHITECTURE Testbench OF tb_RegisterBNx1 IS

	COMPONENT RegisterBNx1 IS
		GENERIC (
			Bitsize	:	integer	:= 8
		);
		PORT (
			CLK, CLR, LD, RD	:			std_logic;
			D						:			std_logic_vector(Bitsize-1 downto 0);
			Q						:	OUT	std_logic
		);
	END COMPONENT;

	SIGNAL s_clk, s_clr, s_ld, s_rd, s_q		:	std_logic := '0';
	SIGNAL s_d											:	std_logic_vector(7 downto 0) := (Others => '0');
	
BEGIN

	DUT	:	RegisterBNx1
	GENERIC MAP (8)
	PORT MAP(s_clk, s_clr, s_ld, s_rd, s_d, s_q);
	
	s_clk <= NOT s_clk AFTER 10ns;
	
	s_d <= "00000000" AFTER 0ns, "10101010" AFTER 20ns, "11111111" AFTER 300ns;
	s_ld <= '1' AFTER 50ns, '0' AFTER 75ns, '1' AFTER 310ns, '0' AFTER 335ns;
	s_rd <= '1' AFTER 100ns, '0' AFTER 125ns, '1' AFTER 400ns, '0' AFTER 425ns, '1' AFTER 530ns, '0' AFTER 545ns;
	s_clr <= '1' AFTER 500ns, '0' AFTER 525ns;
	
END Testbench;