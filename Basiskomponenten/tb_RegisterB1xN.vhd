LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_RegisterB1xN IS
END ENTITY;

ARCHITECTURE Testbench OF tb_RegisterB1xN IS

	COMPONENT RegisterB1xN IS
		GENERIC (
			Bitsize	:	integer	:= 8
		);
		PORT (
			CLK, CLR, LD, D	:			std_logic;
			Q						:	OUT	std_logic_vector(Bitsize-1 downto 0)
		);
	END COMPONENT;

	SIGNAL s_clk, s_clr, s_ld, s_d		:	std_logic := '0';
	SIGNAL s_q									:	std_logic_vector(7 downto 0) := (Others => '0');
	
BEGIN

	DUT	:	RegisterB1xN
	GENERIC MAP (8)
	PORT MAP(s_clk, s_clr, s_ld, s_d, s_q);
	
	s_clk <= NOT s_clk AFTER 10ns;
	
	s_d <= '0' AFTER 0ns, '1' AFTER 20ns, '0' AFTER 250ns, '1' AFTER 340ns;
	s_ld <= '1' AFTER 50ns, '0' AFTER 75ns, '1' AFTER 310ns, '0' AFTER 335ns;
	s_clr <= '1' AFTER 500ns, '0' AFTER 525ns;
	
END Testbench;