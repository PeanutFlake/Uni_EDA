LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY RegisterB1xN IS
	GENERIC (
		Bitsize	:	integer	:= 8
	);
	PORT (
		CLK, CLR, LD, D	:			std_logic;
		Q						:	OUT	std_logic_vector(Bitsize-1 downto 0)
	);
END ENTITY;

ARCHITECTURE SIPO OF RegisterB1xN IS

	SIGNAL s_q			:	std_logic_vector(Bitsize-1 downto 0) := (Others => '0');

BEGIN

	PROCESS(CLK, CLR)
		VARIABLE dq		:	std_logic_vector(Bitsize-1 downto 0) := (Others => '0');
		VARIABLE ind	:	integer range 0 to Bitsize := 0;
		VARIABLE load	:	bit := '0';
	BEGIN
		if ( CLR = '1' ) then
			dq := (Others => '0');
			ind := 0;
		else
			if ( CLK'event AND CLK='1' ) then
				if (LD = '1') then
					ind := 0;
					dq(ind) := D;
					load := '1';
				else
					if (load = '1') then
						if (ind < Bitsize) then
							dq(ind) := D;
						else
							dq := dq;
							load := '0';
						end if;
					else
						dq := dq;
					end if;
				end if;
				if (ind < Bitsize) then
					ind := ind+1;
				end if;
			else
				dq := dq;
			end if;		
		end if;
		Q <= dq;
	END PROCESS;

END SIPO;