LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY AikenEncoder IS
	PORT (
		D		:	IN		std_logic_vector(3 downto 0);
		Q		:	OUT	std_logic_vector(3 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF AikenEncoder IS

BEGIN

	PROCESS (D)
	BEGIN
		case D is
			when "0000" => Q <= "0000";
			when "0001" => Q <= "0001";
			when "0010" => Q <= "0010";
			when "0011" => Q <= "0011";
			when "0100" => Q <= "0100";
			when "0101" => Q <= "1011";
			when "0110" => Q <= "1100";
			when "0111" => Q <= "1101";
			when "1000" => Q <= "1110";
			when "1001" => Q <= "1111";
			when Others => Q <= "0000";
		end case;
	END PROCESS;

END Behaviour;