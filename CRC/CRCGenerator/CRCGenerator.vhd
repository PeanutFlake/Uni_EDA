LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY CRCGenerator IS
	GENERIC (
		Bitsize		:	Integer range 1 to 256				:=	4		-- Size of Inputdata
	);
	PORT (
		DI				:	std_logic_vector(Bitsize-1 downto 0)
		
		DO				:	std_logic_vector()
	);
END ENTITY;

ARCHITECTURE Behaviour OF CRCGenerator IS

BEGIN


END Behaviour;