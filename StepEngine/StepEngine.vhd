LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY StepEngine IS
	PORT (
		in_power_on							:	IN std_logic;
		in_direction						:	IN std_logic;
		in_power_up, in_power_down		:	IN std_logic;
		in_mode								:	IN std_logic;
		out_engine_p, out_engine_n		:	OUT std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF StepEngine IS



	SIGNAL o_engine_p, o_engine_n		:	std_logic := '0';

BEGIN

	if(in_mode = '0') then
		-- Normal Mode
		if(in_direction = '0') then
			o_engine_p <= '1';
			o_engine_n <= '0';
		else
			o_engine_p <= '0';
			o_engine_n <= '1';
		end if;
	else
		-- Auto Mode
	end if;

	out_engine_p <= o_engine_p;
	out_engine_n <= o_engine_n;
	
END Behaviour;