LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Multiplexer5x1 IS
	PORT (
		a, b, c, d, e	:			std_logic;
		s					:			std_logic_vector(2 downto 0);
		y					:	OUT	std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF Multiplexer5x1 IS

	COMPONENT Multiplexer2x1 IS
		PORT (
			a, b, s	: std_logic;
			y			: OUT std_logic
		);
	END COMPONENT;

	SIGNAL s_ab, s_cd, s_o		:	std_logic := '0';
	
BEGIN

	MUX_AB	:	Multiplexer2x1
	PORT MAP (a, b, s(0), s_ab);
	
	MUX_CD	:	Multiplexer2x1
	PORT MAP (c, d, s(0), s_cd);
	
	MUX_ABCD	:	Multiplexer2x1
	PORT MAP (s_ab, s_cd, s(1), s_o);
	
	MUX_ABCDE:	Multiplexer2x1
	PORT MAP (s_o, e, s(2), y);

END Behaviour;