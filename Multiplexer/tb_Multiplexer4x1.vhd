LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_Multiplexer4x1 IS
END ENTITY;

ARCHITECTURE Testbench OF tb_Multiplexer4x1 IS

	COMPONENT Multiplexer4x1 IS
		PORT (
			A, B, C, D		:	IN		std_logic;
			S					:	IN		std_logic_vector(1 downto 0);
			Y					:	OUT	std_logic
		);
	END COMPONENT;
	
	SIGNAL a, b, c, d, y	:	std_logic := '0';
	SIGNAL s					:	std_logic_vector (1 downto 0) := (Others => '0');

BEGIN

	DUT	:	Multiplexer4x1
	PORT MAP(a, b, c, d, s, y);

	

END Testbench;