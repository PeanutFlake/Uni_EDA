LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Multiplexer4xN IS
	GENERIC (
		N		:	Integer	:= 4
	);
	PORT (
		A, B, C, D		:	IN		std_logic_vector(N-1 downto 0);
		S					:	IN		std_logic_vector(1 downto 0);
		Y					:	OUT	std_logic_vector(N-1 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF Multiplexer4xN IS

	COMPONENT Multiplexer2xN IS
		GENERIC (
			N			:	Integer := 4
		);
		PORT(
			a, b		:	IN		std_logic_vector(N-1 downto 0);
			s			:	IN	 	std_logic;
			y			:	OUT 	std_logic_vector(N-1 downto 0)
		);
	END COMPONENT;

	SIGNAL s_a0, s_a1, s_b0, s_b1, s_y0, s_y1	:	std_logic_vector(N-1 downto 0) := (Others => '0');
	
BEGIN

	M0	:	Multiplexer2xN
	GENERIC MAP (N)
	PORT MAP (s_a0, s_b0, S(0), s_y0);
	
	M1	:	Multiplexer2xN
	GENERIC MAP(N)
	PORT MAP (s_a1, s_b1, S(0), s_y1);
	
	s_a0 <= A;
	s_a1 <= C;
	s_b0 <= B;
	s_b1 <= D;
	
	Y <= s_y0 when S(1) = '0' else s_y1;

END Behaviour;