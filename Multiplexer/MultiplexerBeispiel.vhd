LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY MultiplexerBeispiel IS
	PORT (
		x, y, z, v	:	IN	std_logic;
		f, g			:	OUT std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF MultiplexerBeispiel IS

	COMPONENT Multiplexer2x1 IS
		PORT (
			a, b, s	:	std_logic;
			y			:	OUT std_logic
		);
	END COMPONENT;

	SIGNAL s_s0, s_s1, s_s2	:	std_logic := '0';
	
BEGIN

	MULT_F	:	Multiplexer2x1
	PORT MAP (s_s0, s_s1, x, f);

	MULT_G	:	Multiplexer2x1
	PORT MAP (z, s_s2, x, g);
	
	s_s0 <= v OR z;
	s_s1 <= x AND y;
	s_s2 <= y OR z;
	
END Behaviour;