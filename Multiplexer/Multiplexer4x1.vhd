LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Multiplexer4x1 IS
	PORT (
		A, B, C, D		:	IN		std_logic;
		S					:	IN		std_logic_vector(1 downto 0);
		Y					:	OUT	std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF Multiplexer4x1 IS

	COMPONENT Multiplexer2x1 IS
		PORT(
			a, b, s	:	std_logic;
			y			:	OUT std_logic
		);
	END COMPONENT;

	SIGNAL s_a0, s_a1, s_b0, s_b1, s_y0, s_y1	:	std_logic := '0';
	
BEGIN

	M0	:	Multiplexer2x1
	PORT MAP (s_a0, s_b0, S(0), s_y0);
	
	M1	:	Multiplexer2x1
	PORT MAP (s_a1, s_b1, S(0), s_y1);
	
	s_a0 <= A;
	s_a1 <= C;
	s_b0 <= B;
	s_b1 <= D;
	
	Y <= s_y0 when S(1) = '0' else s_y1;

END Behaviour;