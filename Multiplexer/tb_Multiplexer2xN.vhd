LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_Multiplexer2xN IS
END ENTITY;

ARCHITECTURE Testbench OF tb_Multiplexer2xN IS

	COMPONENT Multiplexer2xN IS
		GENERIC (
			N			:	Integer	:=	4
		);
		PORT(
			a, b		:	IN		std_logic_vector(N-1 downto 0);
			s			:	IN	 	std_logic;
			y			:	OUT 	std_logic_vector(N-1 downto 0)
		);
	END COMPONENT;

	SIGNAL s_a, s_b, s_y : std_logic_vector(7 downto 0) := "ZZZZZZZZ";
	SIGNAL s_s 				: std_logic := '0';
	
BEGIN

	DUT	:	Multiplexer2xN
	GENERIC MAP (8)
	PORT MAP (s_a, s_b, s_s, s_y);
	
	s_a <= "00110011";
	s_b <= "11001100";
	
	s_S <= '0' AFTER 0ns, '1' AFTER 100ns;
	
END Testbench;