LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Multiplexer2xN IS
	GENERIC (
		N			:	Integer	:=	4
	);
	PORT(
		a, b		:	IN		std_logic_vector(N-1 downto 0);
		s			:	IN	 	std_logic;
		y			:	OUT 	std_logic_vector(N-1 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF Multiplexer2xN IS

BEGIN

	y <= a when s='0' else b;

END Behaviour;