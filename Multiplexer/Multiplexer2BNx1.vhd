LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Multiplexer2BNx1 IS
	GENERIC (
		N			:	Integer	:=	4
	);
	PORT(
		a, b		:	IN		std_logic_vector(N downto 0);
		s			:	IN	 	std_logic;
		y			:	OUT 	std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF Multiplexer2BNx1 IS

BEGIN

	y <= a when s='0' else b;

END Behaviour;