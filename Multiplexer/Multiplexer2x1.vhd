LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Multiplexer2x1 IS
	PORT(
		a, b, s	:	std_logic;
		y			:	OUT std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF MUltiplexer2x1 IS

BEGIN

	y <= a when s='0' else b;

END Behaviour;