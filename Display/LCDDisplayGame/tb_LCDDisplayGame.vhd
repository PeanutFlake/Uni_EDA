LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_LCDDisplayGame IS
END ENTITY;

ARCHITECTURE Testbench OF tb_LCDDisplayGame IS

	COMPONENT LCDDisplayGame IS
		GENERIC (
		LCD_WAIT_UNIT				:	Integer range -15 to 15			:= 5;				-- 10us
		F_WAIT_LCD					:	Integer range 0 to 9				:=	5;			
		F_CLK							:	Integer range 0 to (2**31)-1	:=	50000000		-- 50Mhz
		);
		PORT (
			CLK, nRST					:	IN		std_logic;
			D								:	IN		std_logic_vector(7 downto 0);
			LCD_EN, LCD_RW, LCD_RS	:	OUT	std_logic;
			LED_D, LL_D					:	OUT	std_logic_vector(7 downto 0);
			LCD_D							:	INOUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;

	SIGNAL s_clk, s_rst							:	std_logic	:=	'1';
	SIGNAL s_lcd_en, s_lcd_rs, s_lcd_rw		:	std_logic	:= '0';
	SIGNAL s_d, s_led_d, s_ll_d, s_lcd_d	:	std_logic_vector(7 downto 0)	:= (Others => '0');
	
BEGIN

	DUT	:	LCDDisplayGame
	GENERIC MAP (5, 5, 50000000)
	PORT MAP (s_clk, s_rst, s_d, s_lcd_en, s_lcd_rw, s_lcd_rs, s_led_d, s_ll_d, s_lcd_d);

	s_clk <= NOT s_clk AFTER 20ns;
	s_rst <= '0' AFTER 600us, '1' AFTER 700us;
	
END Testbench;