 --	LCD Initialization
 --		RS = 0	(System Register)
 --		RW = 0	(Write)
 --	DE2-115 Display HoldTime = 50us
 -- -------------------------------------------------------------------------
 -- |	XXX	|			|			|			|			|			|			|			|
 -- -------------------------------------------------------------------------
 --	LCD Display Initialization Bytes.
 -- -----------------------------------------------------------------
 -- Function Set				2-line mode, display on					00111100
 --								1-line mode, display on					00110100
 --								1-line mode, display off				00110000
 --								2-line mode, display off				00111000
 -- -----------------------------------------------------------------
 -- Display ON/OFF			display on, cursor off, blink off	00001100
 --								display on, cursor off, blink on		00001101
 --								display on, cursor on, blink off		00001110
 --								display on, cursor on, blink on		00001111
 --								display off, cursor off, blink off	00001000
 --								display off, cursor off, blink on	00001001
 --								display off, cursor on, blink off	00001010
 --								display off, cursor on, blink on		00001011
 -- -----------------------------------------------------------------
 -- Entry Mode Set			increment mode, entire shift off		00000110
 --								increment mode, entire shift on		00000111
 --								decrement mode, entire shift off		00000100
 --								decrement mode, entire shift on		00000101

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY LCDDisplayController IS
	GENERIC (
		WaitCycles					:	Integer range 0 to 65535	:=	2500		--	50us * 50Mhz => 2500
	);
	PORT (
		CLK, nRST, RW, EN			:	IN		std_logic;
		DI								:	IN		std_logic_vector(7 downto 0);
		BUSY							:	OUT	std_logic;
		LCD_EN, LCD_RW, LCD_RS	:	OUT	std_logic;
		DO								:	OUT	std_logic_vector(7 downto 0);
		LCD_D							:	INOUT std_logic_vector(7 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF LCDDisplayController IS

	TYPE StateType IS (Func, Disp, Entry, Ready, Reset, SetLine0, SetLine1);

	SIGNAL s_dat					:	std_logic_vector(7 downto 0)	:= (Others => '0');

	SIGNAL s_rs, s_rw, s_en		:	std_logic	:= '0';
	SIGNAL s_busy					:	std_logic	:= '1';
	
BEGIN

	Init:
	PROCESS(CLK, nRST, RW, DI, EN)
		VARIABLE busy			:	std_logic					:= '1';
		VARIABLE init, nline	:	bit							:= '0';
		VARIABLE toWait		:	Integer range 0 to 65535:= 0;
		VARIABLE state			:	StateType					:= Func;
		VARIABLE linesize		:	Integer range 0 to 16	:=	0;
	BEGIN
		
		if ( rising_edge(CLK) ) then
			if ( nRST = '1' ) then
				-- do init
				if ( init = '0' ) then
					busy := '1';
					s_rs <= '0';
					s_rw <= '0';
					if ( toWait = 0 ) then
						s_en <= '0';
						case state is
							when Reset =>
								s_rs <= '0';
								s_dat <= "00000001";
								toWait := WaitCycles;
								state := SetLine0;
							when SetLine0 =>
								s_rs <= '0';
								s_dat <= "10000000";
								toWait := WaitCycles;
								linesize := 0;
								nline := '0';
								state := Ready;
							when SetLine1 =>
								s_rs <= '0';
								s_dat <= "11000000";
								toWait := WaitCycles;
								linesize := 0;
								nline := '1';
								state := Ready;
							when Func =>
								s_dat <= "00111100";
								toWait := WaitCycles;
								state := Disp;
							when Disp => 
								s_dat <= "00001100";
								toWait := WaitCycles;
								state := Entry;
							when Entry =>
								s_dat <= "00000110";
								toWait := WaitCycles;
								state := Ready;
							when Ready =>
								init := '1';
								s_rs <= '1';
								busy := '0';
						end case;
					else
						s_en <= '1';
						toWait := toWait - 1;
					end if;
				else
				-- is init
					if ( RW = '0' ) then
						s_rw <= '0';
						s_dat <= DI;
					else
						s_rw <= '1';
						DO <= LCD_D;
					end if;
					if ( toWait = 0 ) then
						if ( EN = '1' ) then
							s_en <= '1';
							busy := '1';
							toWait := WaitCycles;
							linesize := linesize + 1;
							if ( linesize = 16 ) then
								nline := not nline;
								if ( nline = '1' ) then
									state := SetLine1;
									init := '0';
								else
									state := SetLine0;
									init := '0';
								end if;
								linesize := 0;
							end if;
						else
							busy := '0';
							s_en <= '0';
						end if;
					else
						s_en <= '0';
						busy := '1';
						toWait := toWait - 1;
					end if;
				end if;
			else
				init := '0';
				toWait := 0;
				if ( state = Reset ) then 
					-- hold Reset
				else
					if ( state = Ready ) then
						state := Reset;
					else
						state := Func;
					end if;
				end if;
			end if;
			s_busy <= busy;
		else
		-- out of clock
		end if;
	END PROCESS Init;
	
	LCD_RS <= s_rs;
	LCD_RW <= s_rw;
	LCD_EN <= s_en;
	
	LCD_D <= (Others => 'Z') when (s_rw = '1') else s_dat;

	BUSY <= s_busy;
	
END Behaviour;