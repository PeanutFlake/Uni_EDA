LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.ALL;

ENTITY LCDDisplayGame IS
	GENERIC (
		LCD_WAIT_UNIT				:	Integer range -15 to 15			:= 5;				-- 10us
		F_WAIT_LCD					:	Integer range 0 to 9				:=	5;			
		F_CLK							:	Integer range 0 to 2147483647	:=	50000000		-- 50Mhz
	);
	PORT (
		CLK, nRST					:	IN		std_logic;
		D								:	IN		std_logic_vector(7 downto 0);
		LCD_EN, LCD_RW, LCD_RS	:	OUT	std_logic;
		LED_D, LL_D					:	OUT	std_logic_vector(7 downto 0);
		LCD_D							:	INOUT std_logic_vector(7 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF LCDDisplayGame IS

	COMPONENT LCDDisplayController IS
		GENERIC (
			WaitCycles					:	Integer	:=	2500		--	50us * 50Mhz => 2500
		);
		PORT (
			CLK, nRST, RW, EN			:	IN		std_logic;
			DI								:	IN		std_logic_vector(7 downto 0);
			BUSY							:	OUT	std_logic;
			LCD_EN, LCD_RW, LCD_RS	:	OUT	std_logic;
			DO								:	OUT	std_logic_vector(7 downto 0);
			LCD_D							:	INOUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;
	
	TYPE		LCD_Row IS ARRAY (0 to 15) of std_logic_vector(7 downto 0);
	TYPE		LCD_Text IS ARRAY (0 to 1) of LCD_Row;
	
	CONSTANT WAIT_TIME				:	Integer	:= F_WAIT_LCD*(F_CLK/10**LCD_WAIT_UNIT);
	CONSTANT PAUSE_TIME				:	Integer	:= (F_CLK-(32*24)*WAIT_TIME)/2;
	CONSTANT LCD_INPUT				:	LCD_Text := 
	(
		("01000010", "01000011", "01000100", "00100000", "01000111", "01010010", "01000101", "01011001", "00100000", "01000001", "01001001", "01001011", "01000101", "01001110", "00100000", "00100000"),
		("00110000", "00110000", "00110000", "00110000", "00100000", "00110000", "00110000", "00110000", "00110000", "00100000", "00110000", "00110000", "00110000", "00110000", "00100000", "00100000")
	);
	
	SIGNAL s_nCLK 						:	std_logic							:=	'1';
	SIGNAL s_rw, s_en, s_busy		:	std_logic							:= '0';
	SIGNAL s_d							:	std_logic_vector(7 downto 0)	:= (Others => '0');
	
BEGIN

	LCD : LCDDisplayController
	GENERIC MAP (WAIT_TIME)
	PORT MAP (
		CLK => CLK,
		nRST => nRST,
		RW => s_rw,
		EN => s_en,
		DI => s_d,
		BUSY => s_busy,
		LCD_EN => LCD_EN,
		LCD_RW => LCD_RW,
		LCD_RS => LCD_RS,
		DO => LL_D,
		LCD_D => LCD_D
	);
	
	PROCESS(CLK, nRST)	
	BEGIN
		if ( rising_edge(CLK) ) then
			s_nCLK <= NOT s_nCLK;
		else
			s_nCLK <= s_nCLK;
		end if;
	END PROCESS;
	
	PROCESS(s_nCLK, nRST, s_busy)
		VARIABLE ri : 	Integer range 0 to 1		:= 0;
		VARIABLE ci	:	Integer range 0 to 15	:= 0;
		VARIABLE wa : 	Integer range 0 to 2147483647 := 0;
	BEGIN
	
		if ( nRST = '1' ) then
			if ( rising_edge(s_nCLK) ) then
				if ( wa = 0 ) then 
					if ( s_busy = '0' ) then
						s_d <= LCD_INPUT(ri)(ci);
						if ( ci = 15 ) then
							ci := 0;
							if ( ri = 1 ) then
								ri := 0;
								wa := PAUSE_TIME;
							else
								ri := 1;
							end if;
						else
							ci := ci + 1;
						end if;
						s_rw <= '0';
						s_en <= '1';
					else
						-- wait
						s_en <= '0';
					end if;
				else
					s_en <= '0';
					wa := wa - 1;
				end if;
			else
				-- out of clock
			end if;
		else
			-- reset
			ri := 0;
			ci := 0;
			wa := 0;
			s_en <= '0';
			s_rw <= '0';
		end if;
	
	END PROCESS;
	
	LED_D(7 downto 3) <= (Others => '0');
	LED_D(2) <= s_en;
	LED_D(1) <= s_busy;
	LED_D(0) <= nRST;
	
END Behaviour;