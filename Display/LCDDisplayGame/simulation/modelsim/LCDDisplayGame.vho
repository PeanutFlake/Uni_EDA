-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "06/18/2018 15:02:02"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	LCDDisplayGame IS
    PORT (
	CLK : IN std_logic;
	nRST : IN std_logic;
	D : IN std_logic_vector(7 DOWNTO 0);
	LCD_EN : BUFFER std_logic;
	LCD_RW : BUFFER std_logic;
	LCD_RS : BUFFER std_logic;
	LED_D : BUFFER std_logic_vector(7 DOWNTO 0);
	LL_D : BUFFER std_logic_vector(7 DOWNTO 0);
	LCD_D : BUFFER std_logic_vector(7 DOWNTO 0)
	);
END LCDDisplayGame;

-- Design Ports Information
-- D[0]	=>  Location: PIN_AC24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[1]	=>  Location: PIN_AB24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[2]	=>  Location: PIN_AB23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[3]	=>  Location: PIN_AA24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[4]	=>  Location: PIN_AA23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[5]	=>  Location: PIN_AA22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[6]	=>  Location: PIN_Y24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[7]	=>  Location: PIN_Y23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_EN	=>  Location: PIN_L4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_RW	=>  Location: PIN_M1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_RS	=>  Location: PIN_M2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[0]	=>  Location: PIN_E21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[1]	=>  Location: PIN_E22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[2]	=>  Location: PIN_E25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[3]	=>  Location: PIN_E24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[4]	=>  Location: PIN_H21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[5]	=>  Location: PIN_G20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[6]	=>  Location: PIN_G22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[7]	=>  Location: PIN_G21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[0]	=>  Location: PIN_J15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[1]	=>  Location: PIN_H16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[2]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[3]	=>  Location: PIN_H17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[4]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[5]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[6]	=>  Location: PIN_G16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[7]	=>  Location: PIN_H15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[0]	=>  Location: PIN_L3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[1]	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[2]	=>  Location: PIN_L2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[3]	=>  Location: PIN_K7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[4]	=>  Location: PIN_K1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[5]	=>  Location: PIN_K2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[6]	=>  Location: PIN_M3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[7]	=>  Location: PIN_M5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- nRST	=>  Location: PIN_M23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF LCDDisplayGame IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_nRST : std_logic;
SIGNAL ww_D : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_LCD_EN : std_logic;
SIGNAL ww_LCD_RW : std_logic;
SIGNAL ww_LCD_RS : std_logic;
SIGNAL ww_LED_D : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_LL_D : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_LCD_D : std_logic_vector(7 DOWNTO 0);
SIGNAL \CLK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \s_nCLK~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \D[0]~input_o\ : std_logic;
SIGNAL \D[1]~input_o\ : std_logic;
SIGNAL \D[2]~input_o\ : std_logic;
SIGNAL \D[3]~input_o\ : std_logic;
SIGNAL \D[4]~input_o\ : std_logic;
SIGNAL \D[5]~input_o\ : std_logic;
SIGNAL \D[6]~input_o\ : std_logic;
SIGNAL \D[7]~input_o\ : std_logic;
SIGNAL \LCD_D[0]~input_o\ : std_logic;
SIGNAL \LCD_D[1]~input_o\ : std_logic;
SIGNAL \LCD_D[2]~input_o\ : std_logic;
SIGNAL \LCD_D[3]~input_o\ : std_logic;
SIGNAL \LCD_D[4]~input_o\ : std_logic;
SIGNAL \LCD_D[5]~input_o\ : std_logic;
SIGNAL \LCD_D[6]~input_o\ : std_logic;
SIGNAL \LCD_D[7]~input_o\ : std_logic;
SIGNAL \LCD_D[0]~output_o\ : std_logic;
SIGNAL \LCD_D[1]~output_o\ : std_logic;
SIGNAL \LCD_D[2]~output_o\ : std_logic;
SIGNAL \LCD_D[3]~output_o\ : std_logic;
SIGNAL \LCD_D[4]~output_o\ : std_logic;
SIGNAL \LCD_D[5]~output_o\ : std_logic;
SIGNAL \LCD_D[6]~output_o\ : std_logic;
SIGNAL \LCD_D[7]~output_o\ : std_logic;
SIGNAL \LCD_EN~output_o\ : std_logic;
SIGNAL \LCD_RW~output_o\ : std_logic;
SIGNAL \LCD_RS~output_o\ : std_logic;
SIGNAL \LED_D[0]~output_o\ : std_logic;
SIGNAL \LED_D[1]~output_o\ : std_logic;
SIGNAL \LED_D[2]~output_o\ : std_logic;
SIGNAL \LED_D[3]~output_o\ : std_logic;
SIGNAL \LED_D[4]~output_o\ : std_logic;
SIGNAL \LED_D[5]~output_o\ : std_logic;
SIGNAL \LED_D[6]~output_o\ : std_logic;
SIGNAL \LED_D[7]~output_o\ : std_logic;
SIGNAL \LL_D[0]~output_o\ : std_logic;
SIGNAL \LL_D[1]~output_o\ : std_logic;
SIGNAL \LL_D[2]~output_o\ : std_logic;
SIGNAL \LL_D[3]~output_o\ : std_logic;
SIGNAL \LL_D[4]~output_o\ : std_logic;
SIGNAL \LL_D[5]~output_o\ : std_logic;
SIGNAL \LL_D[6]~output_o\ : std_logic;
SIGNAL \LL_D[7]~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \CLK~inputclkctrl_outclk\ : std_logic;
SIGNAL \nRST~input_o\ : std_logic;
SIGNAL \LCD|Init:toWait[0]~1_combout\ : std_logic;
SIGNAL \~GND~combout\ : std_logic;
SIGNAL \LCD|Init:toWait[0]~q\ : std_logic;
SIGNAL \LCD|Init:toWait[0]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[1]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[1]~q\ : std_logic;
SIGNAL \LCD|Init:toWait[1]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[2]~1_combout\ : std_logic;
SIGNAL \s_nCLK~0_combout\ : std_logic;
SIGNAL \s_nCLK~q\ : std_logic;
SIGNAL \s_nCLK~clkctrl_outclk\ : std_logic;
SIGNAL \wa[0]~31_combout\ : std_logic;
SIGNAL \Equal0~7_combout\ : std_logic;
SIGNAL \Equal0~8_combout\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \Equal0~3_combout\ : std_logic;
SIGNAL \Equal0~5_combout\ : std_logic;
SIGNAL \Equal0~4_combout\ : std_logic;
SIGNAL \Equal0~6_combout\ : std_logic;
SIGNAL \Equal0~12_combout\ : std_logic;
SIGNAL \wa[0]~32\ : std_logic;
SIGNAL \wa[1]~33_combout\ : std_logic;
SIGNAL \wa[1]~34\ : std_logic;
SIGNAL \wa[2]~35_combout\ : std_logic;
SIGNAL \wa[2]~36\ : std_logic;
SIGNAL \wa[3]~37_combout\ : std_logic;
SIGNAL \wa[3]~38\ : std_logic;
SIGNAL \wa[4]~39_combout\ : std_logic;
SIGNAL \wa[4]~40\ : std_logic;
SIGNAL \wa[5]~41_combout\ : std_logic;
SIGNAL \wa[5]~42\ : std_logic;
SIGNAL \wa[6]~43_combout\ : std_logic;
SIGNAL \ci[0]~1_combout\ : std_logic;
SIGNAL \ci[1]~3_combout\ : std_logic;
SIGNAL \ci[2]~0_combout\ : std_logic;
SIGNAL \ri~0_combout\ : std_logic;
SIGNAL \ci[3]~2_combout\ : std_logic;
SIGNAL \wa[18]~45_combout\ : std_logic;
SIGNAL \LCD|busy~0_combout\ : std_logic;
SIGNAL \LCD|Init:busy~q\ : std_logic;
SIGNAL \ri~1_combout\ : std_logic;
SIGNAL \ri~q\ : std_logic;
SIGNAL \wa[18]~46_combout\ : std_logic;
SIGNAL \wa[6]~44\ : std_logic;
SIGNAL \wa[7]~47_combout\ : std_logic;
SIGNAL \wa[7]~48\ : std_logic;
SIGNAL \wa[8]~49_combout\ : std_logic;
SIGNAL \wa[8]~50\ : std_logic;
SIGNAL \wa[9]~51_combout\ : std_logic;
SIGNAL \wa[9]~52\ : std_logic;
SIGNAL \wa[10]~53_combout\ : std_logic;
SIGNAL \wa[10]~54\ : std_logic;
SIGNAL \wa[11]~55_combout\ : std_logic;
SIGNAL \wa[11]~56\ : std_logic;
SIGNAL \wa[12]~57_combout\ : std_logic;
SIGNAL \wa[12]~58\ : std_logic;
SIGNAL \wa[13]~59_combout\ : std_logic;
SIGNAL \wa[13]~60\ : std_logic;
SIGNAL \wa[14]~61_combout\ : std_logic;
SIGNAL \wa[14]~62\ : std_logic;
SIGNAL \wa[15]~63_combout\ : std_logic;
SIGNAL \wa[15]~64\ : std_logic;
SIGNAL \wa[16]~65_combout\ : std_logic;
SIGNAL \wa[16]~66\ : std_logic;
SIGNAL \wa[17]~67_combout\ : std_logic;
SIGNAL \wa[17]~68\ : std_logic;
SIGNAL \wa[18]~69_combout\ : std_logic;
SIGNAL \wa[18]~70\ : std_logic;
SIGNAL \wa[19]~71_combout\ : std_logic;
SIGNAL \wa[19]~72\ : std_logic;
SIGNAL \wa[20]~73_combout\ : std_logic;
SIGNAL \wa[20]~74\ : std_logic;
SIGNAL \wa[21]~75_combout\ : std_logic;
SIGNAL \wa[21]~76\ : std_logic;
SIGNAL \wa[22]~77_combout\ : std_logic;
SIGNAL \wa[22]~78\ : std_logic;
SIGNAL \wa[23]~79_combout\ : std_logic;
SIGNAL \wa[23]~80\ : std_logic;
SIGNAL \wa[24]~81_combout\ : std_logic;
SIGNAL \wa[24]~82\ : std_logic;
SIGNAL \wa[25]~83_combout\ : std_logic;
SIGNAL \wa[25]~84\ : std_logic;
SIGNAL \wa[26]~85_combout\ : std_logic;
SIGNAL \wa[26]~86\ : std_logic;
SIGNAL \wa[27]~87_combout\ : std_logic;
SIGNAL \wa[27]~88\ : std_logic;
SIGNAL \wa[28]~89_combout\ : std_logic;
SIGNAL \wa[28]~90\ : std_logic;
SIGNAL \wa[29]~91_combout\ : std_logic;
SIGNAL \wa[29]~92\ : std_logic;
SIGNAL \wa[30]~93_combout\ : std_logic;
SIGNAL \Equal0~10_combout\ : std_logic;
SIGNAL \Equal0~11_combout\ : std_logic;
SIGNAL \Equal0~9_combout\ : std_logic;
SIGNAL \s_d[0]~2_combout\ : std_logic;
SIGNAL \s_en~feeder_combout\ : std_logic;
SIGNAL \s_en~q\ : std_logic;
SIGNAL \LCD|busy~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[2]~q\ : std_logic;
SIGNAL \LCD|Init:toWait[2]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[3]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[3]~q\ : std_logic;
SIGNAL \LCD|Equal0~0_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[3]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[4]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[4]~q\ : std_logic;
SIGNAL \LCD|Init:toWait[4]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[5]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[5]~q\ : std_logic;
SIGNAL \LCD|Init:toWait[5]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[6]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[6]~q\ : std_logic;
SIGNAL \LCD|Init:toWait[6]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[7]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[7]~q\ : std_logic;
SIGNAL \LCD|Init:toWait[7]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[8]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[8]~q\ : std_logic;
SIGNAL \LCD|Init:toWait[8]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[9]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[9]~q\ : std_logic;
SIGNAL \LCD|Init:toWait[9]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[10]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[10]~q\ : std_logic;
SIGNAL \LCD|Init:toWait[10]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[11]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[11]~q\ : std_logic;
SIGNAL \LCD|Equal0~2_combout\ : std_logic;
SIGNAL \LCD|Equal0~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[11]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[12]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[12]~q\ : std_logic;
SIGNAL \LCD|Init:toWait[12]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[13]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[13]~q\ : std_logic;
SIGNAL \LCD|Init:toWait[13]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[14]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[14]~q\ : std_logic;
SIGNAL \LCD|Init:toWait[14]~2\ : std_logic;
SIGNAL \LCD|Init:toWait[15]~1_combout\ : std_logic;
SIGNAL \LCD|Init:toWait[15]~q\ : std_logic;
SIGNAL \LCD|Equal0~3_combout\ : std_logic;
SIGNAL \LCD|Equal0~4_combout\ : std_logic;
SIGNAL \LCD|linesize~3_combout\ : std_logic;
SIGNAL \LCD|s_rs~1_combout\ : std_logic;
SIGNAL \LCD|state~6_combout\ : std_logic;
SIGNAL \LCD|linesize~4_combout\ : std_logic;
SIGNAL \LCD|Init:linesize[1]~q\ : std_logic;
SIGNAL \LCD|linesize~2_combout\ : std_logic;
SIGNAL \LCD|Init:linesize[2]~q\ : std_logic;
SIGNAL \LCD|Add0~1_combout\ : std_logic;
SIGNAL \LCD|linesize~1_combout\ : std_logic;
SIGNAL \LCD|Init:linesize[3]~q\ : std_logic;
SIGNAL \LCD|Add0~0_combout\ : std_logic;
SIGNAL \LCD|Equal1~1_combout\ : std_logic;
SIGNAL \LCD|state~4_combout\ : std_logic;
SIGNAL \LCD|state~7_combout\ : std_logic;
SIGNAL \LCD|Init:state.SetLine0~q\ : std_logic;
SIGNAL \LCD|Init:nline~0_combout\ : std_logic;
SIGNAL \LCD|Init:nline~1_combout\ : std_logic;
SIGNAL \LCD|Init:nline~2_combout\ : std_logic;
SIGNAL \LCD|Init:nline~3_combout\ : std_logic;
SIGNAL \LCD|Init:nline~q\ : std_logic;
SIGNAL \LCD|state~8_combout\ : std_logic;
SIGNAL \LCD|Init:state.SetLine1~q\ : std_logic;
SIGNAL \LCD|Init:linesize[4]~0_combout\ : std_logic;
SIGNAL \LCD|Init:linesize[4]~1_combout\ : std_logic;
SIGNAL \LCD|Init:linesize[0]~q\ : std_logic;
SIGNAL \LCD|Equal1~0_combout\ : std_logic;
SIGNAL \LCD|linesize~0_combout\ : std_logic;
SIGNAL \LCD|Init:linesize[4]~q\ : std_logic;
SIGNAL \LCD|init~0_combout\ : std_logic;
SIGNAL \LCD|init~1_combout\ : std_logic;
SIGNAL \LCD|Init:init~q\ : std_logic;
SIGNAL \LCD|state~1_combout\ : std_logic;
SIGNAL \LCD|state~11_combout\ : std_logic;
SIGNAL \LCD|Init:state.Func~q\ : std_logic;
SIGNAL \LCD|state~9_combout\ : std_logic;
SIGNAL \LCD|state~5_combout\ : std_logic;
SIGNAL \LCD|Init:state.Disp~q\ : std_logic;
SIGNAL \LCD|state~3_combout\ : std_logic;
SIGNAL \LCD|Init:state.Entry~q\ : std_logic;
SIGNAL \LCD|state~0_combout\ : std_logic;
SIGNAL \LCD|state~2_combout\ : std_logic;
SIGNAL \LCD|Init:state.Ready~q\ : std_logic;
SIGNAL \LCD|state~10_combout\ : std_logic;
SIGNAL \LCD|Init:state.Reset~q\ : std_logic;
SIGNAL \LCD|s_dat~2_combout\ : std_logic;
SIGNAL \Mux6~0_combout\ : std_logic;
SIGNAL \s_d~3_combout\ : std_logic;
SIGNAL \s_d[0]~9_combout\ : std_logic;
SIGNAL \s_d[0]~4_combout\ : std_logic;
SIGNAL \LCD|s_dat~3_combout\ : std_logic;
SIGNAL \Mux5~0_combout\ : std_logic;
SIGNAL \s_d~5_combout\ : std_logic;
SIGNAL \LCD|s_dat~4_combout\ : std_logic;
SIGNAL \LCD|s_dat~5_combout\ : std_logic;
SIGNAL \Mux4~0_combout\ : std_logic;
SIGNAL \s_d~6_combout\ : std_logic;
SIGNAL \LCD|s_dat~6_combout\ : std_logic;
SIGNAL \LCD|s_dat~7_combout\ : std_logic;
SIGNAL \Mux3~0_combout\ : std_logic;
SIGNAL \s_d~7_combout\ : std_logic;
SIGNAL \s_d~10_combout\ : std_logic;
SIGNAL \s_d~11_combout\ : std_logic;
SIGNAL \LCD|s_dat[4]~8_combout\ : std_logic;
SIGNAL \LCD|s_dat[4]~9_combout\ : std_logic;
SIGNAL \Mux1~0_combout\ : std_logic;
SIGNAL \s_d~8_combout\ : std_logic;
SIGNAL \s_d[5]~feeder_combout\ : std_logic;
SIGNAL \LCD|s_dat[5]~10_combout\ : std_logic;
SIGNAL \LCD|s_dat~11_combout\ : std_logic;
SIGNAL \s_d[6]~13_combout\ : std_logic;
SIGNAL \LCD|s_dat~12_combout\ : std_logic;
SIGNAL \LCD|s_dat~13_combout\ : std_logic;
SIGNAL \LCD|s_en~0_combout\ : std_logic;
SIGNAL \LCD|s_en~q\ : std_logic;
SIGNAL \LCD|s_rs~0_combout\ : std_logic;
SIGNAL \LCD|s_rs~2_combout\ : std_logic;
SIGNAL \LCD|s_rs~q\ : std_logic;
SIGNAL wa : std_logic_vector(30 DOWNTO 0);
SIGNAL \LCD|s_dat\ : std_logic_vector(7 DOWNTO 0);
SIGNAL ci : std_logic_vector(3 DOWNTO 0);
SIGNAL s_d : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_nRST~input_o\ : std_logic;
SIGNAL \LCD|ALT_INV_Init:busy~q\ : std_logic;
SIGNAL \ALT_INV_s_nCLK~clkctrl_outclk\ : std_logic;

BEGIN

ww_CLK <= CLK;
ww_nRST <= nRST;
ww_D <= D;
LCD_EN <= ww_LCD_EN;
LCD_RW <= ww_LCD_RW;
LCD_RS <= ww_LCD_RS;
LED_D <= ww_LED_D;
LL_D <= ww_LL_D;
LCD_D <= ww_LCD_D;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLK~input_o\);

\s_nCLK~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \s_nCLK~q\);
\ALT_INV_nRST~input_o\ <= NOT \nRST~input_o\;
\LCD|ALT_INV_Init:busy~q\ <= NOT \LCD|Init:busy~q\;
\ALT_INV_s_nCLK~clkctrl_outclk\ <= NOT \s_nCLK~clkctrl_outclk\;

-- Location: IOOBUF_X0_Y52_N16
\LCD_D[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|s_dat\(0),
	oe => VCC,
	devoe => ww_devoe,
	o => \LCD_D[0]~output_o\);

-- Location: IOOBUF_X0_Y44_N9
\LCD_D[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|s_dat\(1),
	oe => VCC,
	devoe => ww_devoe,
	o => \LCD_D[1]~output_o\);

-- Location: IOOBUF_X0_Y44_N2
\LCD_D[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|s_dat\(2),
	oe => VCC,
	devoe => ww_devoe,
	o => \LCD_D[2]~output_o\);

-- Location: IOOBUF_X0_Y49_N9
\LCD_D[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|s_dat\(3),
	oe => VCC,
	devoe => ww_devoe,
	o => \LCD_D[3]~output_o\);

-- Location: IOOBUF_X0_Y54_N9
\LCD_D[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|s_dat\(4),
	oe => VCC,
	devoe => ww_devoe,
	o => \LCD_D[4]~output_o\);

-- Location: IOOBUF_X0_Y55_N23
\LCD_D[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|s_dat\(5),
	oe => VCC,
	devoe => ww_devoe,
	o => \LCD_D[5]~output_o\);

-- Location: IOOBUF_X0_Y51_N16
\LCD_D[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|s_dat\(6),
	oe => VCC,
	devoe => ww_devoe,
	o => \LCD_D[6]~output_o\);

-- Location: IOOBUF_X0_Y47_N2
\LCD_D[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|s_dat\(7),
	oe => VCC,
	devoe => ww_devoe,
	o => \LCD_D[7]~output_o\);

-- Location: IOOBUF_X0_Y52_N2
\LCD_EN~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|s_en~q\,
	devoe => ww_devoe,
	o => \LCD_EN~output_o\);

-- Location: IOOBUF_X0_Y44_N23
\LCD_RW~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_RW~output_o\);

-- Location: IOOBUF_X0_Y44_N16
\LCD_RS~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|s_rs~q\,
	devoe => ww_devoe,
	o => \LCD_RS~output_o\);

-- Location: IOOBUF_X107_Y73_N9
\LED_D[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \nRST~input_o\,
	devoe => ww_devoe,
	o => \LED_D[0]~output_o\);

-- Location: IOOBUF_X111_Y73_N9
\LED_D[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|ALT_INV_Init:busy~q\,
	devoe => ww_devoe,
	o => \LED_D[1]~output_o\);

-- Location: IOOBUF_X83_Y73_N2
\LED_D[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_en~q\,
	devoe => ww_devoe,
	o => \LED_D[2]~output_o\);

-- Location: IOOBUF_X85_Y73_N23
\LED_D[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LED_D[3]~output_o\);

-- Location: IOOBUF_X72_Y73_N16
\LED_D[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LED_D[4]~output_o\);

-- Location: IOOBUF_X74_Y73_N16
\LED_D[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LED_D[5]~output_o\);

-- Location: IOOBUF_X72_Y73_N23
\LED_D[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LED_D[6]~output_o\);

-- Location: IOOBUF_X74_Y73_N23
\LED_D[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LED_D[7]~output_o\);

-- Location: IOOBUF_X60_Y73_N23
\LL_D[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LL_D[0]~output_o\);

-- Location: IOOBUF_X65_Y73_N23
\LL_D[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LL_D[1]~output_o\);

-- Location: IOOBUF_X65_Y73_N16
\LL_D[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LL_D[2]~output_o\);

-- Location: IOOBUF_X67_Y73_N9
\LL_D[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LL_D[3]~output_o\);

-- Location: IOOBUF_X58_Y73_N2
\LL_D[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LL_D[4]~output_o\);

-- Location: IOOBUF_X65_Y73_N9
\LL_D[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LL_D[5]~output_o\);

-- Location: IOOBUF_X67_Y73_N2
\LL_D[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LL_D[6]~output_o\);

-- Location: IOOBUF_X60_Y73_N16
\LL_D[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LL_D[7]~output_o\);

-- Location: IOIBUF_X0_Y36_N15
\CLK~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: CLKCTRL_G4
\CLK~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLK~inputclkctrl_outclk\);

-- Location: IOIBUF_X115_Y40_N8
\nRST~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_nRST,
	o => \nRST~input_o\);

-- Location: LCCOMB_X27_Y48_N0
\LCD|Init:toWait[0]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[0]~1_combout\ = \LCD|Init:toWait[0]~q\ $ (VCC)
-- \LCD|Init:toWait[0]~2\ = CARRY(\LCD|Init:toWait[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:toWait[0]~q\,
	datad => VCC,
	combout => \LCD|Init:toWait[0]~1_combout\,
	cout => \LCD|Init:toWait[0]~2\);

-- Location: LCCOMB_X24_Y47_N20
\~GND\ : cycloneive_lcell_comb
-- Equation(s):
-- \~GND~combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~GND~combout\);

-- Location: FF_X27_Y48_N1
\LCD|Init:toWait[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[0]~1_combout\,
	asdata => \~GND~combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[0]~q\);

-- Location: LCCOMB_X27_Y48_N2
\LCD|Init:toWait[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[1]~1_combout\ = (\LCD|Init:toWait[1]~q\ & (\LCD|Init:toWait[0]~2\ & VCC)) # (!\LCD|Init:toWait[1]~q\ & (!\LCD|Init:toWait[0]~2\))
-- \LCD|Init:toWait[1]~2\ = CARRY((!\LCD|Init:toWait[1]~q\ & !\LCD|Init:toWait[0]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:toWait[1]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[0]~2\,
	combout => \LCD|Init:toWait[1]~1_combout\,
	cout => \LCD|Init:toWait[1]~2\);

-- Location: FF_X27_Y48_N3
\LCD|Init:toWait[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[1]~1_combout\,
	asdata => \~GND~combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[1]~q\);

-- Location: LCCOMB_X27_Y48_N4
\LCD|Init:toWait[2]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[2]~1_combout\ = (\LCD|Init:toWait[2]~q\ & ((GND) # (!\LCD|Init:toWait[1]~2\))) # (!\LCD|Init:toWait[2]~q\ & (\LCD|Init:toWait[1]~2\ $ (GND)))
-- \LCD|Init:toWait[2]~2\ = CARRY((\LCD|Init:toWait[2]~q\) # (!\LCD|Init:toWait[1]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:toWait[2]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[1]~2\,
	combout => \LCD|Init:toWait[2]~1_combout\,
	cout => \LCD|Init:toWait[2]~2\);

-- Location: LCCOMB_X1_Y36_N30
\s_nCLK~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_nCLK~0_combout\ = !\s_nCLK~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \s_nCLK~q\,
	combout => \s_nCLK~0_combout\);

-- Location: FF_X1_Y36_N13
s_nCLK : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \s_nCLK~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_nCLK~q\);

-- Location: CLKCTRL_G3
\s_nCLK~clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \s_nCLK~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \s_nCLK~clkctrl_outclk\);

-- Location: LCCOMB_X23_Y48_N2
\wa[0]~31\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[0]~31_combout\ = wa(0) $ (VCC)
-- \wa[0]~32\ = CARRY(wa(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => wa(0),
	datad => VCC,
	combout => \wa[0]~31_combout\,
	cout => \wa[0]~32\);

-- Location: LCCOMB_X24_Y47_N6
\Equal0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~7_combout\ = (!wa(19) & (!wa(17) & (!wa(16) & !wa(18))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => wa(19),
	datab => wa(17),
	datac => wa(16),
	datad => wa(18),
	combout => \Equal0~7_combout\);

-- Location: LCCOMB_X24_Y47_N16
\Equal0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~8_combout\ = (!wa(20) & (!wa(23) & (!wa(22) & !wa(21))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => wa(20),
	datab => wa(23),
	datac => wa(22),
	datad => wa(21),
	combout => \Equal0~8_combout\);

-- Location: LCCOMB_X24_Y47_N30
\Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = (!wa(1) & (!wa(3) & (!wa(0) & !wa(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => wa(1),
	datab => wa(3),
	datac => wa(0),
	datad => wa(2),
	combout => \Equal0~2_combout\);

-- Location: LCCOMB_X24_Y47_N24
\Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~3_combout\ = (!wa(6) & (!wa(7) & (!wa(4) & !wa(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => wa(6),
	datab => wa(7),
	datac => wa(4),
	datad => wa(5),
	combout => \Equal0~3_combout\);

-- Location: LCCOMB_X24_Y47_N26
\Equal0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~5_combout\ = (!wa(15) & (!wa(14) & (!wa(13) & !wa(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => wa(15),
	datab => wa(14),
	datac => wa(13),
	datad => wa(12),
	combout => \Equal0~5_combout\);

-- Location: LCCOMB_X23_Y48_N0
\Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~4_combout\ = (!wa(11) & (!wa(9) & (!wa(10) & !wa(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => wa(11),
	datab => wa(9),
	datac => wa(10),
	datad => wa(8),
	combout => \Equal0~4_combout\);

-- Location: LCCOMB_X24_Y47_N12
\Equal0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~6_combout\ = (\Equal0~2_combout\ & (\Equal0~3_combout\ & (\Equal0~5_combout\ & \Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~2_combout\,
	datab => \Equal0~3_combout\,
	datac => \Equal0~5_combout\,
	datad => \Equal0~4_combout\,
	combout => \Equal0~6_combout\);

-- Location: LCCOMB_X24_Y47_N18
\Equal0~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~12_combout\ = (\Equal0~7_combout\ & (\Equal0~8_combout\ & (\Equal0~11_combout\ & \Equal0~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~7_combout\,
	datab => \Equal0~8_combout\,
	datac => \Equal0~11_combout\,
	datad => \Equal0~6_combout\,
	combout => \Equal0~12_combout\);

-- Location: FF_X23_Y48_N3
\wa[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[0]~31_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(0));

-- Location: LCCOMB_X23_Y48_N4
\wa[1]~33\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[1]~33_combout\ = (wa(1) & (\wa[0]~32\ & VCC)) # (!wa(1) & (!\wa[0]~32\))
-- \wa[1]~34\ = CARRY((!wa(1) & !\wa[0]~32\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(1),
	datad => VCC,
	cin => \wa[0]~32\,
	combout => \wa[1]~33_combout\,
	cout => \wa[1]~34\);

-- Location: FF_X23_Y48_N5
\wa[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[1]~33_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(1));

-- Location: LCCOMB_X23_Y48_N6
\wa[2]~35\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[2]~35_combout\ = (wa(2) & ((GND) # (!\wa[1]~34\))) # (!wa(2) & (\wa[1]~34\ $ (GND)))
-- \wa[2]~36\ = CARRY((wa(2)) # (!\wa[1]~34\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => wa(2),
	datad => VCC,
	cin => \wa[1]~34\,
	combout => \wa[2]~35_combout\,
	cout => \wa[2]~36\);

-- Location: FF_X23_Y48_N7
\wa[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[2]~35_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(2));

-- Location: LCCOMB_X23_Y48_N8
\wa[3]~37\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[3]~37_combout\ = (wa(3) & (\wa[2]~36\ & VCC)) # (!wa(3) & (!\wa[2]~36\))
-- \wa[3]~38\ = CARRY((!wa(3) & !\wa[2]~36\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(3),
	datad => VCC,
	cin => \wa[2]~36\,
	combout => \wa[3]~37_combout\,
	cout => \wa[3]~38\);

-- Location: FF_X23_Y48_N9
\wa[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[3]~37_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(3));

-- Location: LCCOMB_X23_Y48_N10
\wa[4]~39\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[4]~39_combout\ = (wa(4) & ((GND) # (!\wa[3]~38\))) # (!wa(4) & (\wa[3]~38\ $ (GND)))
-- \wa[4]~40\ = CARRY((wa(4)) # (!\wa[3]~38\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => wa(4),
	datad => VCC,
	cin => \wa[3]~38\,
	combout => \wa[4]~39_combout\,
	cout => \wa[4]~40\);

-- Location: FF_X23_Y48_N11
\wa[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[4]~39_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(4));

-- Location: LCCOMB_X23_Y48_N12
\wa[5]~41\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[5]~41_combout\ = (wa(5) & (\wa[4]~40\ & VCC)) # (!wa(5) & (!\wa[4]~40\))
-- \wa[5]~42\ = CARRY((!wa(5) & !\wa[4]~40\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => wa(5),
	datad => VCC,
	cin => \wa[4]~40\,
	combout => \wa[5]~41_combout\,
	cout => \wa[5]~42\);

-- Location: FF_X23_Y48_N13
\wa[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[5]~41_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(5));

-- Location: LCCOMB_X23_Y48_N14
\wa[6]~43\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[6]~43_combout\ = (wa(6) & ((GND) # (!\wa[5]~42\))) # (!wa(6) & (\wa[5]~42\ $ (GND)))
-- \wa[6]~44\ = CARRY((wa(6)) # (!\wa[5]~42\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(6),
	datad => VCC,
	cin => \wa[5]~42\,
	combout => \wa[6]~43_combout\,
	cout => \wa[6]~44\);

-- Location: LCCOMB_X26_Y47_N4
\ci[0]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ci[0]~1_combout\ = ci(0) $ (\s_d[0]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => ci(0),
	datad => \s_d[0]~2_combout\,
	combout => \ci[0]~1_combout\);

-- Location: FF_X26_Y47_N5
\ci[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \ci[0]~1_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => ci(0));

-- Location: LCCOMB_X26_Y47_N20
\ci[1]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \ci[1]~3_combout\ = ci(1) $ (((ci(0) & \s_d[0]~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => ci(0),
	datac => ci(1),
	datad => \s_d[0]~2_combout\,
	combout => \ci[1]~3_combout\);

-- Location: FF_X26_Y47_N21
\ci[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \ci[1]~3_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => ci(1));

-- Location: LCCOMB_X26_Y47_N6
\ci[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \ci[2]~0_combout\ = ci(2) $ (((ci(1) & (ci(0) & \s_d[0]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(1),
	datab => ci(0),
	datac => ci(2),
	datad => \s_d[0]~2_combout\,
	combout => \ci[2]~0_combout\);

-- Location: FF_X26_Y47_N7
\ci[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \ci[2]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => ci(2));

-- Location: LCCOMB_X26_Y47_N10
\ri~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \ri~0_combout\ = (ci(2) & (ci(1) & (ci(0) & \s_d[0]~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(2),
	datab => ci(1),
	datac => ci(0),
	datad => \s_d[0]~2_combout\,
	combout => \ri~0_combout\);

-- Location: LCCOMB_X26_Y47_N30
\ci[3]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \ci[3]~2_combout\ = ci(3) $ (\ri~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => ci(3),
	datad => \ri~0_combout\,
	combout => \ci[3]~2_combout\);

-- Location: FF_X26_Y47_N31
\ci[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \ci[3]~2_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => ci(3));

-- Location: LCCOMB_X26_Y47_N18
\wa[18]~45\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[18]~45_combout\ = (((!ci(2)) # (!ci(0))) # (!ci(1))) # (!ci(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(3),
	datab => ci(1),
	datac => ci(0),
	datad => ci(2),
	combout => \wa[18]~45_combout\);

-- Location: LCCOMB_X25_Y48_N10
\LCD|busy~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|busy~0_combout\ = (\LCD|Equal0~4_combout\ & ((\LCD|Init:init~q\ & ((!\s_en~q\))) # (!\LCD|Init:init~q\ & (\LCD|Init:state.Ready~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:state.Ready~q\,
	datab => \LCD|Equal0~4_combout\,
	datac => \s_en~q\,
	datad => \LCD|Init:init~q\,
	combout => \LCD|busy~0_combout\);

-- Location: FF_X24_Y47_N21
\LCD|Init:busy\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \LCD|busy~0_combout\,
	sload => VCC,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:busy~q\);

-- Location: LCCOMB_X26_Y47_N28
\ri~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ri~1_combout\ = \ri~q\ $ (((ci(3) & \ri~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(3),
	datac => \ri~q\,
	datad => \ri~0_combout\,
	combout => \ri~1_combout\);

-- Location: FF_X26_Y47_N29
ri : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \ri~1_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ri~q\);

-- Location: LCCOMB_X24_Y47_N10
\wa[18]~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[18]~46_combout\ = ((!\wa[18]~45_combout\ & (\LCD|Init:busy~q\ & \ri~q\))) # (!\Equal0~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \wa[18]~45_combout\,
	datab => \LCD|Init:busy~q\,
	datac => \ri~q\,
	datad => \Equal0~12_combout\,
	combout => \wa[18]~46_combout\);

-- Location: FF_X23_Y48_N15
\wa[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[6]~43_combout\,
	asdata => \wa[18]~46_combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(6));

-- Location: LCCOMB_X23_Y48_N16
\wa[7]~47\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[7]~47_combout\ = (wa(7) & (\wa[6]~44\ & VCC)) # (!wa(7) & (!\wa[6]~44\))
-- \wa[7]~48\ = CARRY((!wa(7) & !\wa[6]~44\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(7),
	datad => VCC,
	cin => \wa[6]~44\,
	combout => \wa[7]~47_combout\,
	cout => \wa[7]~48\);

-- Location: FF_X23_Y48_N17
\wa[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[7]~47_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(7));

-- Location: LCCOMB_X23_Y48_N18
\wa[8]~49\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[8]~49_combout\ = (wa(8) & ((GND) # (!\wa[7]~48\))) # (!wa(8) & (\wa[7]~48\ $ (GND)))
-- \wa[8]~50\ = CARRY((wa(8)) # (!\wa[7]~48\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => wa(8),
	datad => VCC,
	cin => \wa[7]~48\,
	combout => \wa[8]~49_combout\,
	cout => \wa[8]~50\);

-- Location: FF_X23_Y48_N19
\wa[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[8]~49_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(8));

-- Location: LCCOMB_X23_Y48_N20
\wa[9]~51\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[9]~51_combout\ = (wa(9) & (\wa[8]~50\ & VCC)) # (!wa(9) & (!\wa[8]~50\))
-- \wa[9]~52\ = CARRY((!wa(9) & !\wa[8]~50\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => wa(9),
	datad => VCC,
	cin => \wa[8]~50\,
	combout => \wa[9]~51_combout\,
	cout => \wa[9]~52\);

-- Location: FF_X23_Y48_N21
\wa[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[9]~51_combout\,
	asdata => \wa[18]~46_combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(9));

-- Location: LCCOMB_X23_Y48_N22
\wa[10]~53\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[10]~53_combout\ = (wa(10) & ((GND) # (!\wa[9]~52\))) # (!wa(10) & (\wa[9]~52\ $ (GND)))
-- \wa[10]~54\ = CARRY((wa(10)) # (!\wa[9]~52\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(10),
	datad => VCC,
	cin => \wa[9]~52\,
	combout => \wa[10]~53_combout\,
	cout => \wa[10]~54\);

-- Location: FF_X23_Y48_N23
\wa[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[10]~53_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(10));

-- Location: LCCOMB_X23_Y48_N24
\wa[11]~55\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[11]~55_combout\ = (wa(11) & (\wa[10]~54\ & VCC)) # (!wa(11) & (!\wa[10]~54\))
-- \wa[11]~56\ = CARRY((!wa(11) & !\wa[10]~54\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(11),
	datad => VCC,
	cin => \wa[10]~54\,
	combout => \wa[11]~55_combout\,
	cout => \wa[11]~56\);

-- Location: FF_X23_Y48_N25
\wa[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[11]~55_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(11));

-- Location: LCCOMB_X23_Y48_N26
\wa[12]~57\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[12]~57_combout\ = (wa(12) & ((GND) # (!\wa[11]~56\))) # (!wa(12) & (\wa[11]~56\ $ (GND)))
-- \wa[12]~58\ = CARRY((wa(12)) # (!\wa[11]~56\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => wa(12),
	datad => VCC,
	cin => \wa[11]~56\,
	combout => \wa[12]~57_combout\,
	cout => \wa[12]~58\);

-- Location: FF_X23_Y48_N27
\wa[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[12]~57_combout\,
	asdata => \wa[18]~46_combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(12));

-- Location: LCCOMB_X23_Y48_N28
\wa[13]~59\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[13]~59_combout\ = (wa(13) & (\wa[12]~58\ & VCC)) # (!wa(13) & (!\wa[12]~58\))
-- \wa[13]~60\ = CARRY((!wa(13) & !\wa[12]~58\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(13),
	datad => VCC,
	cin => \wa[12]~58\,
	combout => \wa[13]~59_combout\,
	cout => \wa[13]~60\);

-- Location: FF_X23_Y48_N29
\wa[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[13]~59_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(13));

-- Location: LCCOMB_X23_Y48_N30
\wa[14]~61\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[14]~61_combout\ = (wa(14) & ((GND) # (!\wa[13]~60\))) # (!wa(14) & (\wa[13]~60\ $ (GND)))
-- \wa[14]~62\ = CARRY((wa(14)) # (!\wa[13]~60\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => wa(14),
	datad => VCC,
	cin => \wa[13]~60\,
	combout => \wa[14]~61_combout\,
	cout => \wa[14]~62\);

-- Location: FF_X23_Y48_N31
\wa[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[14]~61_combout\,
	asdata => \wa[18]~46_combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(14));

-- Location: LCCOMB_X23_Y47_N0
\wa[15]~63\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[15]~63_combout\ = (wa(15) & (\wa[14]~62\ & VCC)) # (!wa(15) & (!\wa[14]~62\))
-- \wa[15]~64\ = CARRY((!wa(15) & !\wa[14]~62\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(15),
	datad => VCC,
	cin => \wa[14]~62\,
	combout => \wa[15]~63_combout\,
	cout => \wa[15]~64\);

-- Location: FF_X23_Y47_N1
\wa[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[15]~63_combout\,
	asdata => \wa[18]~46_combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(15));

-- Location: LCCOMB_X23_Y47_N2
\wa[16]~65\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[16]~65_combout\ = (wa(16) & ((GND) # (!\wa[15]~64\))) # (!wa(16) & (\wa[15]~64\ $ (GND)))
-- \wa[16]~66\ = CARRY((wa(16)) # (!\wa[15]~64\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(16),
	datad => VCC,
	cin => \wa[15]~64\,
	combout => \wa[16]~65_combout\,
	cout => \wa[16]~66\);

-- Location: FF_X23_Y47_N3
\wa[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[16]~65_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(16));

-- Location: LCCOMB_X23_Y47_N4
\wa[17]~67\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[17]~67_combout\ = (wa(17) & (\wa[16]~66\ & VCC)) # (!wa(17) & (!\wa[16]~66\))
-- \wa[17]~68\ = CARRY((!wa(17) & !\wa[16]~66\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(17),
	datad => VCC,
	cin => \wa[16]~66\,
	combout => \wa[17]~67_combout\,
	cout => \wa[17]~68\);

-- Location: FF_X23_Y47_N5
\wa[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[17]~67_combout\,
	asdata => \wa[18]~46_combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(17));

-- Location: LCCOMB_X23_Y47_N6
\wa[18]~69\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[18]~69_combout\ = (wa(18) & ((GND) # (!\wa[17]~68\))) # (!wa(18) & (\wa[17]~68\ $ (GND)))
-- \wa[18]~70\ = CARRY((wa(18)) # (!\wa[17]~68\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => wa(18),
	datad => VCC,
	cin => \wa[17]~68\,
	combout => \wa[18]~69_combout\,
	cout => \wa[18]~70\);

-- Location: FF_X23_Y47_N7
\wa[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[18]~69_combout\,
	asdata => \wa[18]~46_combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(18));

-- Location: LCCOMB_X23_Y47_N8
\wa[19]~71\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[19]~71_combout\ = (wa(19) & (\wa[18]~70\ & VCC)) # (!wa(19) & (!\wa[18]~70\))
-- \wa[19]~72\ = CARRY((!wa(19) & !\wa[18]~70\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(19),
	datad => VCC,
	cin => \wa[18]~70\,
	combout => \wa[19]~71_combout\,
	cout => \wa[19]~72\);

-- Location: FF_X23_Y47_N9
\wa[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[19]~71_combout\,
	asdata => \wa[18]~46_combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(19));

-- Location: LCCOMB_X23_Y47_N10
\wa[20]~73\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[20]~73_combout\ = (wa(20) & ((GND) # (!\wa[19]~72\))) # (!wa(20) & (\wa[19]~72\ $ (GND)))
-- \wa[20]~74\ = CARRY((wa(20)) # (!\wa[19]~72\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => wa(20),
	datad => VCC,
	cin => \wa[19]~72\,
	combout => \wa[20]~73_combout\,
	cout => \wa[20]~74\);

-- Location: FF_X23_Y47_N11
\wa[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[20]~73_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(20));

-- Location: LCCOMB_X23_Y47_N12
\wa[21]~75\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[21]~75_combout\ = (wa(21) & (\wa[20]~74\ & VCC)) # (!wa(21) & (!\wa[20]~74\))
-- \wa[21]~76\ = CARRY((!wa(21) & !\wa[20]~74\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => wa(21),
	datad => VCC,
	cin => \wa[20]~74\,
	combout => \wa[21]~75_combout\,
	cout => \wa[21]~76\);

-- Location: FF_X23_Y47_N13
\wa[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[21]~75_combout\,
	asdata => \wa[18]~46_combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(21));

-- Location: LCCOMB_X23_Y47_N14
\wa[22]~77\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[22]~77_combout\ = (wa(22) & ((GND) # (!\wa[21]~76\))) # (!wa(22) & (\wa[21]~76\ $ (GND)))
-- \wa[22]~78\ = CARRY((wa(22)) # (!\wa[21]~76\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(22),
	datad => VCC,
	cin => \wa[21]~76\,
	combout => \wa[22]~77_combout\,
	cout => \wa[22]~78\);

-- Location: FF_X23_Y47_N15
\wa[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[22]~77_combout\,
	asdata => \wa[18]~46_combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(22));

-- Location: LCCOMB_X23_Y47_N16
\wa[23]~79\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[23]~79_combout\ = (wa(23) & (\wa[22]~78\ & VCC)) # (!wa(23) & (!\wa[22]~78\))
-- \wa[23]~80\ = CARRY((!wa(23) & !\wa[22]~78\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(23),
	datad => VCC,
	cin => \wa[22]~78\,
	combout => \wa[23]~79_combout\,
	cout => \wa[23]~80\);

-- Location: FF_X23_Y47_N17
\wa[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[23]~79_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(23));

-- Location: LCCOMB_X23_Y47_N18
\wa[24]~81\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[24]~81_combout\ = (wa(24) & ((GND) # (!\wa[23]~80\))) # (!wa(24) & (\wa[23]~80\ $ (GND)))
-- \wa[24]~82\ = CARRY((wa(24)) # (!\wa[23]~80\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(24),
	datad => VCC,
	cin => \wa[23]~80\,
	combout => \wa[24]~81_combout\,
	cout => \wa[24]~82\);

-- Location: FF_X23_Y47_N19
\wa[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[24]~81_combout\,
	asdata => \wa[18]~46_combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(24));

-- Location: LCCOMB_X23_Y47_N20
\wa[25]~83\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[25]~83_combout\ = (wa(25) & (\wa[24]~82\ & VCC)) # (!wa(25) & (!\wa[24]~82\))
-- \wa[25]~84\ = CARRY((!wa(25) & !\wa[24]~82\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(25),
	datad => VCC,
	cin => \wa[24]~82\,
	combout => \wa[25]~83_combout\,
	cout => \wa[25]~84\);

-- Location: FF_X23_Y47_N21
\wa[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[25]~83_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(25));

-- Location: LCCOMB_X23_Y47_N22
\wa[26]~85\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[26]~85_combout\ = (wa(26) & ((GND) # (!\wa[25]~84\))) # (!wa(26) & (\wa[25]~84\ $ (GND)))
-- \wa[26]~86\ = CARRY((wa(26)) # (!\wa[25]~84\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => wa(26),
	datad => VCC,
	cin => \wa[25]~84\,
	combout => \wa[26]~85_combout\,
	cout => \wa[26]~86\);

-- Location: FF_X23_Y47_N23
\wa[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[26]~85_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(26));

-- Location: LCCOMB_X23_Y47_N24
\wa[27]~87\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[27]~87_combout\ = (wa(27) & (\wa[26]~86\ & VCC)) # (!wa(27) & (!\wa[26]~86\))
-- \wa[27]~88\ = CARRY((!wa(27) & !\wa[26]~86\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(27),
	datad => VCC,
	cin => \wa[26]~86\,
	combout => \wa[27]~87_combout\,
	cout => \wa[27]~88\);

-- Location: FF_X23_Y47_N25
\wa[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[27]~87_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(27));

-- Location: LCCOMB_X23_Y47_N26
\wa[28]~89\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[28]~89_combout\ = (wa(28) & ((GND) # (!\wa[27]~88\))) # (!wa(28) & (\wa[27]~88\ $ (GND)))
-- \wa[28]~90\ = CARRY((wa(28)) # (!\wa[27]~88\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => wa(28),
	datad => VCC,
	cin => \wa[27]~88\,
	combout => \wa[28]~89_combout\,
	cout => \wa[28]~90\);

-- Location: FF_X23_Y47_N27
\wa[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[28]~89_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(28));

-- Location: LCCOMB_X23_Y47_N28
\wa[29]~91\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[29]~91_combout\ = (wa(29) & (\wa[28]~90\ & VCC)) # (!wa(29) & (!\wa[28]~90\))
-- \wa[29]~92\ = CARRY((!wa(29) & !\wa[28]~90\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => wa(29),
	datad => VCC,
	cin => \wa[28]~90\,
	combout => \wa[29]~91_combout\,
	cout => \wa[29]~92\);

-- Location: FF_X23_Y47_N29
\wa[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[29]~91_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(29));

-- Location: LCCOMB_X23_Y47_N30
\wa[30]~93\ : cycloneive_lcell_comb
-- Equation(s):
-- \wa[30]~93_combout\ = wa(30) $ (\wa[29]~92\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => wa(30),
	cin => \wa[29]~92\,
	combout => \wa[30]~93_combout\);

-- Location: FF_X23_Y47_N31
\wa[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \wa[30]~93_combout\,
	asdata => \~GND~combout\,
	clrn => \nRST~input_o\,
	sload => \Equal0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => wa(30));

-- Location: LCCOMB_X24_Y47_N0
\Equal0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~10_combout\ = (!wa(24) & (!wa(26) & (!wa(25) & !wa(27))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => wa(24),
	datab => wa(26),
	datac => wa(25),
	datad => wa(27),
	combout => \Equal0~10_combout\);

-- Location: LCCOMB_X24_Y47_N22
\Equal0~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~11_combout\ = (!wa(28) & (!wa(29) & (!wa(30) & \Equal0~10_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => wa(28),
	datab => wa(29),
	datac => wa(30),
	datad => \Equal0~10_combout\,
	combout => \Equal0~11_combout\);

-- Location: LCCOMB_X24_Y47_N14
\Equal0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~9_combout\ = (\Equal0~7_combout\ & \Equal0~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~7_combout\,
	datad => \Equal0~8_combout\,
	combout => \Equal0~9_combout\);

-- Location: LCCOMB_X24_Y47_N28
\s_d[0]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d[0]~2_combout\ = (\Equal0~11_combout\ & (\Equal0~9_combout\ & (\Equal0~6_combout\ & \LCD|Init:busy~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~11_combout\,
	datab => \Equal0~9_combout\,
	datac => \Equal0~6_combout\,
	datad => \LCD|Init:busy~q\,
	combout => \s_d[0]~2_combout\);

-- Location: LCCOMB_X26_Y48_N8
\s_en~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_en~feeder_combout\ = \s_d[0]~2_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \s_d[0]~2_combout\,
	combout => \s_en~feeder_combout\);

-- Location: FF_X26_Y48_N9
s_en : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \s_en~feeder_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_en~q\);

-- Location: LCCOMB_X26_Y48_N0
\LCD|busy~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|busy~1_combout\ = (\LCD|Init:init~q\ & ((\s_en~q\))) # (!\LCD|Init:init~q\ & (!\LCD|Init:state.Ready~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:state.Ready~q\,
	datac => \LCD|Init:init~q\,
	datad => \s_en~q\,
	combout => \LCD|busy~1_combout\);

-- Location: FF_X27_Y48_N5
\LCD|Init:toWait[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[2]~1_combout\,
	asdata => \LCD|busy~1_combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[2]~q\);

-- Location: LCCOMB_X27_Y48_N6
\LCD|Init:toWait[3]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[3]~1_combout\ = (\LCD|Init:toWait[3]~q\ & (\LCD|Init:toWait[2]~2\ & VCC)) # (!\LCD|Init:toWait[3]~q\ & (!\LCD|Init:toWait[2]~2\))
-- \LCD|Init:toWait[3]~2\ = CARRY((!\LCD|Init:toWait[3]~q\ & !\LCD|Init:toWait[2]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:toWait[3]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[2]~2\,
	combout => \LCD|Init:toWait[3]~1_combout\,
	cout => \LCD|Init:toWait[3]~2\);

-- Location: FF_X27_Y48_N7
\LCD|Init:toWait[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[3]~1_combout\,
	asdata => \~GND~combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[3]~q\);

-- Location: LCCOMB_X26_Y48_N30
\LCD|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~0_combout\ = (\LCD|Init:toWait[1]~q\) # ((\LCD|Init:toWait[0]~q\) # ((\LCD|Init:toWait[3]~q\) # (\LCD|Init:toWait[2]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:toWait[1]~q\,
	datab => \LCD|Init:toWait[0]~q\,
	datac => \LCD|Init:toWait[3]~q\,
	datad => \LCD|Init:toWait[2]~q\,
	combout => \LCD|Equal0~0_combout\);

-- Location: LCCOMB_X27_Y48_N8
\LCD|Init:toWait[4]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[4]~1_combout\ = (\LCD|Init:toWait[4]~q\ & ((GND) # (!\LCD|Init:toWait[3]~2\))) # (!\LCD|Init:toWait[4]~q\ & (\LCD|Init:toWait[3]~2\ $ (GND)))
-- \LCD|Init:toWait[4]~2\ = CARRY((\LCD|Init:toWait[4]~q\) # (!\LCD|Init:toWait[3]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:toWait[4]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[3]~2\,
	combout => \LCD|Init:toWait[4]~1_combout\,
	cout => \LCD|Init:toWait[4]~2\);

-- Location: FF_X27_Y48_N9
\LCD|Init:toWait[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[4]~1_combout\,
	asdata => \~GND~combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[4]~q\);

-- Location: LCCOMB_X27_Y48_N10
\LCD|Init:toWait[5]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[5]~1_combout\ = (\LCD|Init:toWait[5]~q\ & (\LCD|Init:toWait[4]~2\ & VCC)) # (!\LCD|Init:toWait[5]~q\ & (!\LCD|Init:toWait[4]~2\))
-- \LCD|Init:toWait[5]~2\ = CARRY((!\LCD|Init:toWait[5]~q\ & !\LCD|Init:toWait[4]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:toWait[5]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[4]~2\,
	combout => \LCD|Init:toWait[5]~1_combout\,
	cout => \LCD|Init:toWait[5]~2\);

-- Location: FF_X27_Y48_N11
\LCD|Init:toWait[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[5]~1_combout\,
	asdata => \~GND~combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[5]~q\);

-- Location: LCCOMB_X27_Y48_N12
\LCD|Init:toWait[6]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[6]~1_combout\ = (\LCD|Init:toWait[6]~q\ & ((GND) # (!\LCD|Init:toWait[5]~2\))) # (!\LCD|Init:toWait[6]~q\ & (\LCD|Init:toWait[5]~2\ $ (GND)))
-- \LCD|Init:toWait[6]~2\ = CARRY((\LCD|Init:toWait[6]~q\) # (!\LCD|Init:toWait[5]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:toWait[6]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[5]~2\,
	combout => \LCD|Init:toWait[6]~1_combout\,
	cout => \LCD|Init:toWait[6]~2\);

-- Location: FF_X27_Y48_N13
\LCD|Init:toWait[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[6]~1_combout\,
	asdata => \LCD|busy~1_combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[6]~q\);

-- Location: LCCOMB_X27_Y48_N14
\LCD|Init:toWait[7]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[7]~1_combout\ = (\LCD|Init:toWait[7]~q\ & (\LCD|Init:toWait[6]~2\ & VCC)) # (!\LCD|Init:toWait[7]~q\ & (!\LCD|Init:toWait[6]~2\))
-- \LCD|Init:toWait[7]~2\ = CARRY((!\LCD|Init:toWait[7]~q\ & !\LCD|Init:toWait[6]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:toWait[7]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[6]~2\,
	combout => \LCD|Init:toWait[7]~1_combout\,
	cout => \LCD|Init:toWait[7]~2\);

-- Location: FF_X27_Y48_N15
\LCD|Init:toWait[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[7]~1_combout\,
	asdata => \LCD|busy~1_combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[7]~q\);

-- Location: LCCOMB_X27_Y48_N16
\LCD|Init:toWait[8]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[8]~1_combout\ = (\LCD|Init:toWait[8]~q\ & ((GND) # (!\LCD|Init:toWait[7]~2\))) # (!\LCD|Init:toWait[8]~q\ & (\LCD|Init:toWait[7]~2\ $ (GND)))
-- \LCD|Init:toWait[8]~2\ = CARRY((\LCD|Init:toWait[8]~q\) # (!\LCD|Init:toWait[7]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:toWait[8]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[7]~2\,
	combout => \LCD|Init:toWait[8]~1_combout\,
	cout => \LCD|Init:toWait[8]~2\);

-- Location: FF_X27_Y48_N17
\LCD|Init:toWait[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[8]~1_combout\,
	asdata => \LCD|busy~1_combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[8]~q\);

-- Location: LCCOMB_X27_Y48_N18
\LCD|Init:toWait[9]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[9]~1_combout\ = (\LCD|Init:toWait[9]~q\ & (\LCD|Init:toWait[8]~2\ & VCC)) # (!\LCD|Init:toWait[9]~q\ & (!\LCD|Init:toWait[8]~2\))
-- \LCD|Init:toWait[9]~2\ = CARRY((!\LCD|Init:toWait[9]~q\ & !\LCD|Init:toWait[8]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:toWait[9]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[8]~2\,
	combout => \LCD|Init:toWait[9]~1_combout\,
	cout => \LCD|Init:toWait[9]~2\);

-- Location: FF_X27_Y48_N19
\LCD|Init:toWait[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[9]~1_combout\,
	asdata => \~GND~combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[9]~q\);

-- Location: LCCOMB_X27_Y48_N20
\LCD|Init:toWait[10]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[10]~1_combout\ = (\LCD|Init:toWait[10]~q\ & ((GND) # (!\LCD|Init:toWait[9]~2\))) # (!\LCD|Init:toWait[10]~q\ & (\LCD|Init:toWait[9]~2\ $ (GND)))
-- \LCD|Init:toWait[10]~2\ = CARRY((\LCD|Init:toWait[10]~q\) # (!\LCD|Init:toWait[9]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:toWait[10]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[9]~2\,
	combout => \LCD|Init:toWait[10]~1_combout\,
	cout => \LCD|Init:toWait[10]~2\);

-- Location: FF_X27_Y48_N21
\LCD|Init:toWait[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[10]~1_combout\,
	asdata => \~GND~combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[10]~q\);

-- Location: LCCOMB_X27_Y48_N22
\LCD|Init:toWait[11]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[11]~1_combout\ = (\LCD|Init:toWait[11]~q\ & (\LCD|Init:toWait[10]~2\ & VCC)) # (!\LCD|Init:toWait[11]~q\ & (!\LCD|Init:toWait[10]~2\))
-- \LCD|Init:toWait[11]~2\ = CARRY((!\LCD|Init:toWait[11]~q\ & !\LCD|Init:toWait[10]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:toWait[11]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[10]~2\,
	combout => \LCD|Init:toWait[11]~1_combout\,
	cout => \LCD|Init:toWait[11]~2\);

-- Location: FF_X27_Y48_N23
\LCD|Init:toWait[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[11]~1_combout\,
	asdata => \LCD|busy~1_combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[11]~q\);

-- Location: LCCOMB_X26_Y48_N14
\LCD|Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~2_combout\ = (\LCD|Init:toWait[10]~q\) # ((\LCD|Init:toWait[8]~q\) # ((\LCD|Init:toWait[11]~q\) # (\LCD|Init:toWait[9]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:toWait[10]~q\,
	datab => \LCD|Init:toWait[8]~q\,
	datac => \LCD|Init:toWait[11]~q\,
	datad => \LCD|Init:toWait[9]~q\,
	combout => \LCD|Equal0~2_combout\);

-- Location: LCCOMB_X26_Y48_N4
\LCD|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~1_combout\ = (\LCD|Init:toWait[6]~q\) # ((\LCD|Init:toWait[5]~q\) # ((\LCD|Init:toWait[7]~q\) # (\LCD|Init:toWait[4]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:toWait[6]~q\,
	datab => \LCD|Init:toWait[5]~q\,
	datac => \LCD|Init:toWait[7]~q\,
	datad => \LCD|Init:toWait[4]~q\,
	combout => \LCD|Equal0~1_combout\);

-- Location: LCCOMB_X27_Y48_N24
\LCD|Init:toWait[12]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[12]~1_combout\ = (\LCD|Init:toWait[12]~q\ & ((GND) # (!\LCD|Init:toWait[11]~2\))) # (!\LCD|Init:toWait[12]~q\ & (\LCD|Init:toWait[11]~2\ $ (GND)))
-- \LCD|Init:toWait[12]~2\ = CARRY((\LCD|Init:toWait[12]~q\) # (!\LCD|Init:toWait[11]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:toWait[12]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[11]~2\,
	combout => \LCD|Init:toWait[12]~1_combout\,
	cout => \LCD|Init:toWait[12]~2\);

-- Location: FF_X27_Y48_N25
\LCD|Init:toWait[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[12]~1_combout\,
	asdata => \~GND~combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[12]~q\);

-- Location: LCCOMB_X27_Y48_N26
\LCD|Init:toWait[13]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[13]~1_combout\ = (\LCD|Init:toWait[13]~q\ & (\LCD|Init:toWait[12]~2\ & VCC)) # (!\LCD|Init:toWait[13]~q\ & (!\LCD|Init:toWait[12]~2\))
-- \LCD|Init:toWait[13]~2\ = CARRY((!\LCD|Init:toWait[13]~q\ & !\LCD|Init:toWait[12]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:toWait[13]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[12]~2\,
	combout => \LCD|Init:toWait[13]~1_combout\,
	cout => \LCD|Init:toWait[13]~2\);

-- Location: FF_X27_Y48_N27
\LCD|Init:toWait[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[13]~1_combout\,
	asdata => \~GND~combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[13]~q\);

-- Location: LCCOMB_X27_Y48_N28
\LCD|Init:toWait[14]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[14]~1_combout\ = (\LCD|Init:toWait[14]~q\ & ((GND) # (!\LCD|Init:toWait[13]~2\))) # (!\LCD|Init:toWait[14]~q\ & (\LCD|Init:toWait[13]~2\ $ (GND)))
-- \LCD|Init:toWait[14]~2\ = CARRY((\LCD|Init:toWait[14]~q\) # (!\LCD|Init:toWait[13]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:toWait[14]~q\,
	datad => VCC,
	cin => \LCD|Init:toWait[13]~2\,
	combout => \LCD|Init:toWait[14]~1_combout\,
	cout => \LCD|Init:toWait[14]~2\);

-- Location: FF_X27_Y48_N29
\LCD|Init:toWait[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[14]~1_combout\,
	asdata => \~GND~combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[14]~q\);

-- Location: LCCOMB_X27_Y48_N30
\LCD|Init:toWait[15]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:toWait[15]~1_combout\ = \LCD|Init:toWait[15]~q\ $ (!\LCD|Init:toWait[14]~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:toWait[15]~q\,
	cin => \LCD|Init:toWait[14]~2\,
	combout => \LCD|Init:toWait[15]~1_combout\);

-- Location: FF_X27_Y48_N31
\LCD|Init:toWait[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:toWait[15]~1_combout\,
	asdata => \~GND~combout\,
	sclr => \ALT_INV_nRST~input_o\,
	sload => \LCD|Equal0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:toWait[15]~q\);

-- Location: LCCOMB_X26_Y48_N12
\LCD|Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~3_combout\ = (\LCD|Init:toWait[15]~q\) # ((\LCD|Init:toWait[13]~q\) # ((\LCD|Init:toWait[12]~q\) # (\LCD|Init:toWait[14]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:toWait[15]~q\,
	datab => \LCD|Init:toWait[13]~q\,
	datac => \LCD|Init:toWait[12]~q\,
	datad => \LCD|Init:toWait[14]~q\,
	combout => \LCD|Equal0~3_combout\);

-- Location: LCCOMB_X26_Y48_N22
\LCD|Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~4_combout\ = (!\LCD|Equal0~0_combout\ & (!\LCD|Equal0~2_combout\ & (!\LCD|Equal0~1_combout\ & !\LCD|Equal0~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Equal0~0_combout\,
	datab => \LCD|Equal0~2_combout\,
	datac => \LCD|Equal0~1_combout\,
	datad => \LCD|Equal0~3_combout\,
	combout => \LCD|Equal0~4_combout\);

-- Location: LCCOMB_X28_Y48_N12
\LCD|linesize~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|linesize~3_combout\ = (!\LCD|Init:linesize[0]~q\ & \LCD|Init:init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \LCD|Init:linesize[0]~q\,
	datad => \LCD|Init:init~q\,
	combout => \LCD|linesize~3_combout\);

-- Location: LCCOMB_X25_Y46_N6
\LCD|s_rs~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_rs~1_combout\ = (!\LCD|Init:init~q\ & (\nRST~input_o\ & \LCD|Equal0~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:init~q\,
	datab => \nRST~input_o\,
	datad => \LCD|Equal0~4_combout\,
	combout => \LCD|s_rs~1_combout\);

-- Location: LCCOMB_X25_Y48_N12
\LCD|state~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|state~6_combout\ = (\LCD|Init:init~q\ & ((\LCD|Init:nline~q\))) # (!\LCD|Init:init~q\ & (\LCD|Init:state.Reset~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:state.Reset~q\,
	datac => \LCD|Init:nline~q\,
	datad => \LCD|Init:init~q\,
	combout => \LCD|state~6_combout\);

-- Location: LCCOMB_X28_Y48_N26
\LCD|linesize~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|linesize~4_combout\ = (\LCD|Init:init~q\ & (\LCD|Init:linesize[0]~q\ $ (\LCD|Init:linesize[1]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:linesize[0]~q\,
	datac => \LCD|Init:linesize[1]~q\,
	datad => \LCD|Init:init~q\,
	combout => \LCD|linesize~4_combout\);

-- Location: FF_X28_Y48_N27
\LCD|Init:linesize[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|linesize~4_combout\,
	ena => \LCD|Init:linesize[4]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:linesize[1]~q\);

-- Location: LCCOMB_X28_Y48_N14
\LCD|linesize~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|linesize~2_combout\ = (\LCD|Init:init~q\ & (\LCD|Init:linesize[2]~q\ $ (((\LCD|Init:linesize[1]~q\ & \LCD|Init:linesize[0]~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:linesize[1]~q\,
	datab => \LCD|Init:linesize[0]~q\,
	datac => \LCD|Init:linesize[2]~q\,
	datad => \LCD|Init:init~q\,
	combout => \LCD|linesize~2_combout\);

-- Location: FF_X28_Y48_N15
\LCD|Init:linesize[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|linesize~2_combout\,
	ena => \LCD|Init:linesize[4]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:linesize[2]~q\);

-- Location: LCCOMB_X28_Y48_N18
\LCD|Add0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add0~1_combout\ = \LCD|Init:linesize[3]~q\ $ (((\LCD|Init:linesize[2]~q\ & (\LCD|Init:linesize[0]~q\ & \LCD|Init:linesize[1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:linesize[2]~q\,
	datab => \LCD|Init:linesize[0]~q\,
	datac => \LCD|Init:linesize[1]~q\,
	datad => \LCD|Init:linesize[3]~q\,
	combout => \LCD|Add0~1_combout\);

-- Location: LCCOMB_X28_Y48_N4
\LCD|linesize~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|linesize~1_combout\ = (\LCD|Add0~1_combout\ & \LCD|Init:init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Add0~1_combout\,
	datad => \LCD|Init:init~q\,
	combout => \LCD|linesize~1_combout\);

-- Location: FF_X28_Y48_N5
\LCD|Init:linesize[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|linesize~1_combout\,
	ena => \LCD|Init:linesize[4]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:linesize[3]~q\);

-- Location: LCCOMB_X28_Y48_N20
\LCD|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add0~0_combout\ = (\LCD|Init:linesize[3]~q\ & (\LCD|Init:linesize[2]~q\ & (\LCD|Init:linesize[1]~q\ & \LCD|Init:linesize[0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:linesize[3]~q\,
	datab => \LCD|Init:linesize[2]~q\,
	datac => \LCD|Init:linesize[1]~q\,
	datad => \LCD|Init:linesize[0]~q\,
	combout => \LCD|Add0~0_combout\);

-- Location: LCCOMB_X26_Y48_N20
\LCD|Equal1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal1~1_combout\ = (\LCD|Equal1~0_combout\) # (\LCD|Init:linesize[4]~q\ $ (!\LCD|Add0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:linesize[4]~q\,
	datac => \LCD|Add0~0_combout\,
	datad => \LCD|Equal1~0_combout\,
	combout => \LCD|Equal1~1_combout\);

-- Location: LCCOMB_X26_Y48_N28
\LCD|state~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|state~4_combout\ = ((\LCD|Init:init~q\ & ((\LCD|Equal1~1_combout\) # (!\s_en~q\)))) # (!\LCD|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110101011101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Equal0~4_combout\,
	datab => \LCD|Init:init~q\,
	datac => \s_en~q\,
	datad => \LCD|Equal1~1_combout\,
	combout => \LCD|state~4_combout\);

-- Location: LCCOMB_X25_Y48_N16
\LCD|state~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|state~7_combout\ = (!\LCD|state~4_combout\) # (!\nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \nRST~input_o\,
	datad => \LCD|state~4_combout\,
	combout => \LCD|state~7_combout\);

-- Location: FF_X25_Y48_N13
\LCD|Init:state.SetLine0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|state~6_combout\,
	sclr => \ALT_INV_nRST~input_o\,
	ena => \LCD|state~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:state.SetLine0~q\);

-- Location: LCCOMB_X25_Y46_N14
\LCD|Init:nline~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:nline~0_combout\ = (\nRST~input_o\ & ((\LCD|Init:init~q\) # (\LCD|Init:state.SetLine0~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \nRST~input_o\,
	datac => \LCD|Init:init~q\,
	datad => \LCD|Init:state.SetLine0~q\,
	combout => \LCD|Init:nline~0_combout\);

-- Location: LCCOMB_X25_Y46_N24
\LCD|Init:nline~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:nline~1_combout\ = (\LCD|Init:nline~0_combout\ & (((!\LCD|Equal1~1_combout\ & \s_en~q\)) # (!\LCD|Init:init~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100110001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:init~q\,
	datab => \LCD|Init:nline~0_combout\,
	datac => \LCD|Equal1~1_combout\,
	datad => \s_en~q\,
	combout => \LCD|Init:nline~1_combout\);

-- Location: LCCOMB_X25_Y46_N10
\LCD|Init:nline~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:nline~2_combout\ = (\LCD|Init:nline~q\ & (((!\LCD|Init:nline~1_combout\) # (!\LCD|Equal0~4_combout\)))) # (!\LCD|Init:nline~q\ & (\LCD|Init:init~q\ & (\LCD|Equal0~4_combout\ & \LCD|Init:nline~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:init~q\,
	datab => \LCD|Init:nline~q\,
	datac => \LCD|Equal0~4_combout\,
	datad => \LCD|Init:nline~1_combout\,
	combout => \LCD|Init:nline~2_combout\);

-- Location: LCCOMB_X25_Y46_N8
\LCD|Init:nline~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:nline~3_combout\ = (\LCD|Init:nline~2_combout\) # ((\LCD|s_rs~1_combout\ & \LCD|Init:state.SetLine1~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|s_rs~1_combout\,
	datac => \LCD|Init:state.SetLine1~q\,
	datad => \LCD|Init:nline~2_combout\,
	combout => \LCD|Init:nline~3_combout\);

-- Location: FF_X25_Y46_N9
\LCD|Init:nline\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Init:nline~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:nline~q\);

-- Location: LCCOMB_X25_Y48_N2
\LCD|state~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|state~8_combout\ = (!\LCD|Init:nline~q\ & \LCD|Init:init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \LCD|Init:nline~q\,
	datad => \LCD|Init:init~q\,
	combout => \LCD|state~8_combout\);

-- Location: FF_X25_Y48_N3
\LCD|Init:state.SetLine1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|state~8_combout\,
	sclr => \ALT_INV_nRST~input_o\,
	ena => \LCD|state~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:state.SetLine1~q\);

-- Location: LCCOMB_X28_Y48_N30
\LCD|Init:linesize[4]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:linesize[4]~0_combout\ = (\nRST~input_o\ & ((\LCD|Init:init~q\) # ((\LCD|Init:state.SetLine1~q\) # (\LCD|Init:state.SetLine0~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \LCD|Init:init~q\,
	datac => \LCD|Init:state.SetLine1~q\,
	datad => \LCD|Init:state.SetLine0~q\,
	combout => \LCD|Init:linesize[4]~0_combout\);

-- Location: LCCOMB_X28_Y48_N16
\LCD|Init:linesize[4]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Init:linesize[4]~1_combout\ = (\LCD|Init:linesize[4]~0_combout\ & (\LCD|Equal0~4_combout\ & ((\s_en~q\) # (!\LCD|Init:init~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:linesize[4]~0_combout\,
	datab => \LCD|Init:init~q\,
	datac => \LCD|Equal0~4_combout\,
	datad => \s_en~q\,
	combout => \LCD|Init:linesize[4]~1_combout\);

-- Location: FF_X28_Y48_N13
\LCD|Init:linesize[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|linesize~3_combout\,
	ena => \LCD|Init:linesize[4]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:linesize[0]~q\);

-- Location: LCCOMB_X28_Y48_N22
\LCD|Equal1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal1~0_combout\ = (((!\LCD|Init:linesize[1]~q\) # (!\LCD|Init:linesize[3]~q\)) # (!\LCD|Init:linesize[2]~q\)) # (!\LCD|Init:linesize[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:linesize[0]~q\,
	datab => \LCD|Init:linesize[2]~q\,
	datac => \LCD|Init:linesize[3]~q\,
	datad => \LCD|Init:linesize[1]~q\,
	combout => \LCD|Equal1~0_combout\);

-- Location: LCCOMB_X28_Y48_N2
\LCD|linesize~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|linesize~0_combout\ = (\LCD|Equal1~0_combout\ & (\LCD|Init:init~q\ & (\LCD|Init:linesize[4]~q\ $ (\LCD|Add0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Equal1~0_combout\,
	datab => \LCD|Init:init~q\,
	datac => \LCD|Init:linesize[4]~q\,
	datad => \LCD|Add0~0_combout\,
	combout => \LCD|linesize~0_combout\);

-- Location: FF_X28_Y48_N3
\LCD|Init:linesize[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|linesize~0_combout\,
	ena => \LCD|Init:linesize[4]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:linesize[4]~q\);

-- Location: LCCOMB_X28_Y48_N28
\LCD|init~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|init~0_combout\ = (!\LCD|Equal1~0_combout\ & (\s_en~q\ & (\LCD|Init:linesize[4]~q\ $ (\LCD|Add0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:linesize[4]~q\,
	datab => \LCD|Add0~0_combout\,
	datac => \LCD|Equal1~0_combout\,
	datad => \s_en~q\,
	combout => \LCD|init~0_combout\);

-- Location: LCCOMB_X28_Y48_N24
\LCD|init~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|init~1_combout\ = (\LCD|Equal0~4_combout\ & ((\LCD|Init:init~q\ & ((!\LCD|init~0_combout\))) # (!\LCD|Init:init~q\ & (\LCD|Init:state.Ready~q\)))) # (!\LCD|Equal0~4_combout\ & (((\LCD|Init:init~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101100011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Equal0~4_combout\,
	datab => \LCD|Init:state.Ready~q\,
	datac => \LCD|Init:init~q\,
	datad => \LCD|init~0_combout\,
	combout => \LCD|init~1_combout\);

-- Location: FF_X28_Y48_N25
\LCD|Init:init\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|init~1_combout\,
	sclr => \ALT_INV_nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:init~q\);

-- Location: LCCOMB_X25_Y46_N30
\LCD|state~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|state~1_combout\ = ((\LCD|Equal1~1_combout\) # ((!\s_en~q\) # (!\LCD|Equal0~4_combout\))) # (!\LCD|Init:init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:init~q\,
	datab => \LCD|Equal1~1_combout\,
	datac => \LCD|Equal0~4_combout\,
	datad => \s_en~q\,
	combout => \LCD|state~1_combout\);

-- Location: LCCOMB_X25_Y48_N18
\LCD|state~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|state~11_combout\ = (\nRST~input_o\) # ((\LCD|Init:state.Reset~q\) # (\LCD|Init:state.Ready~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \LCD|Init:state.Reset~q\,
	datad => \LCD|Init:state.Ready~q\,
	combout => \LCD|state~11_combout\);

-- Location: FF_X25_Y48_N19
\LCD|Init:state.Func\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|state~11_combout\,
	ena => \LCD|state~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:state.Func~q\);

-- Location: LCCOMB_X26_Y48_N10
\LCD|state~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|state~9_combout\ = (!\LCD|Init:state.Func~q\ & (!\LCD|Init:init~q\ & \nRST~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:state.Func~q\,
	datac => \LCD|Init:init~q\,
	datad => \nRST~input_o\,
	combout => \LCD|state~9_combout\);

-- Location: LCCOMB_X26_Y48_N18
\LCD|state~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|state~5_combout\ = (\nRST~input_o\ & ((!\LCD|state~4_combout\))) # (!\nRST~input_o\ & (!\LCD|Init:state.Reset~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111011101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:state.Reset~q\,
	datab => \nRST~input_o\,
	datad => \LCD|state~4_combout\,
	combout => \LCD|state~5_combout\);

-- Location: FF_X26_Y48_N11
\LCD|Init:state.Disp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|state~9_combout\,
	ena => \LCD|state~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:state.Disp~q\);

-- Location: LCCOMB_X26_Y48_N2
\LCD|state~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|state~3_combout\ = (\LCD|Init:state.Disp~q\ & (!\LCD|Init:init~q\ & \nRST~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:state.Disp~q\,
	datab => \LCD|Init:init~q\,
	datad => \nRST~input_o\,
	combout => \LCD|state~3_combout\);

-- Location: FF_X26_Y48_N3
\LCD|Init:state.Entry\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|state~3_combout\,
	ena => \LCD|state~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:state.Entry~q\);

-- Location: LCCOMB_X25_Y46_N12
\LCD|state~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|state~0_combout\ = (\LCD|s_rs~1_combout\ & ((\LCD|Init:state.Entry~q\) # ((\LCD|Init:state.SetLine0~q\) # (\LCD|Init:state.SetLine1~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:state.Entry~q\,
	datab => \LCD|Init:state.SetLine0~q\,
	datac => \LCD|Init:state.SetLine1~q\,
	datad => \LCD|s_rs~1_combout\,
	combout => \LCD|state~0_combout\);

-- Location: LCCOMB_X25_Y46_N4
\LCD|state~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|state~2_combout\ = (\LCD|state~0_combout\) # ((\LCD|state~1_combout\ & (\nRST~input_o\ & \LCD|Init:state.Ready~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|state~1_combout\,
	datab => \nRST~input_o\,
	datac => \LCD|Init:state.Ready~q\,
	datad => \LCD|state~0_combout\,
	combout => \LCD|state~2_combout\);

-- Location: FF_X25_Y46_N5
\LCD|Init:state.Ready\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|state~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:state.Ready~q\);

-- Location: LCCOMB_X25_Y48_N14
\LCD|state~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|state~10_combout\ = (!\nRST~input_o\ & ((\LCD|Init:state.Reset~q\) # (\LCD|Init:state.Ready~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datac => \LCD|Init:state.Reset~q\,
	datad => \LCD|Init:state.Ready~q\,
	combout => \LCD|state~10_combout\);

-- Location: FF_X25_Y48_N15
\LCD|Init:state.Reset\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|state~10_combout\,
	ena => \LCD|state~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|Init:state.Reset~q\);

-- Location: LCCOMB_X27_Y47_N20
\LCD|s_dat~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_dat~2_combout\ = (\LCD|Equal0~4_combout\ & ((\LCD|Init:state.Reset~q\) # ((\LCD|s_dat\(0) & \LCD|Init:state.Ready~q\)))) # (!\LCD|Equal0~4_combout\ & (((\LCD|s_dat\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:state.Reset~q\,
	datab => \LCD|Equal0~4_combout\,
	datac => \LCD|s_dat\(0),
	datad => \LCD|Init:state.Ready~q\,
	combout => \LCD|s_dat~2_combout\);

-- Location: LCCOMB_X26_Y47_N24
\Mux6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux6~0_combout\ = ci(2) $ (((ci(1) & (ci(3))) # (!ci(1) & ((ci(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100011110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(3),
	datab => ci(1),
	datac => ci(0),
	datad => ci(2),
	combout => \Mux6~0_combout\);

-- Location: LCCOMB_X27_Y47_N8
\s_d~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d~3_combout\ = (!\ri~q\ & \Mux6~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \ri~q\,
	datad => \Mux6~0_combout\,
	combout => \s_d~3_combout\);

-- Location: LCCOMB_X24_Y47_N8
\s_d[0]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d[0]~9_combout\ = (\Equal0~7_combout\ & (\nRST~input_o\ & \Equal0~8_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~7_combout\,
	datac => \nRST~input_o\,
	datad => \Equal0~8_combout\,
	combout => \s_d[0]~9_combout\);

-- Location: LCCOMB_X24_Y47_N4
\s_d[0]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d[0]~4_combout\ = (\Equal0~6_combout\ & (\s_d[0]~9_combout\ & (\Equal0~11_combout\ & \LCD|Init:busy~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~6_combout\,
	datab => \s_d[0]~9_combout\,
	datac => \Equal0~11_combout\,
	datad => \LCD|Init:busy~q\,
	combout => \s_d[0]~4_combout\);

-- Location: FF_X27_Y47_N9
\s_d[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \s_d~3_combout\,
	ena => \s_d[0]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_d(0));

-- Location: FF_X27_Y47_N21
\LCD|s_dat[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|s_dat~2_combout\,
	asdata => s_d(0),
	sload => \LCD|Init:init~q\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_dat\(0));

-- Location: LCCOMB_X26_Y47_N12
\LCD|s_dat~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_dat~3_combout\ = (\LCD|Equal0~4_combout\ & ((\LCD|Init:state.Entry~q\) # ((\LCD|Init:state.Ready~q\ & \LCD|s_dat\(1))))) # (!\LCD|Equal0~4_combout\ & (((\LCD|s_dat\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:state.Ready~q\,
	datab => \LCD|Equal0~4_combout\,
	datac => \LCD|s_dat\(1),
	datad => \LCD|Init:state.Entry~q\,
	combout => \LCD|s_dat~3_combout\);

-- Location: LCCOMB_X25_Y47_N24
\Mux5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux5~0_combout\ = (ci(3) & ((ci(1) $ (!ci(2))) # (!ci(0)))) # (!ci(3) & (((ci(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001001111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(3),
	datab => ci(0),
	datac => ci(1),
	datad => ci(2),
	combout => \Mux5~0_combout\);

-- Location: LCCOMB_X25_Y47_N16
\s_d~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d~5_combout\ = (!\ri~q\ & !\Mux5~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ri~q\,
	datad => \Mux5~0_combout\,
	combout => \s_d~5_combout\);

-- Location: FF_X25_Y47_N17
\s_d[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \s_d~5_combout\,
	ena => \s_d[0]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_d(1));

-- Location: FF_X26_Y47_N13
\LCD|s_dat[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|s_dat~3_combout\,
	asdata => s_d(1),
	sload => \LCD|Init:init~q\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_dat\(1));

-- Location: LCCOMB_X26_Y48_N16
\LCD|s_dat~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_dat~4_combout\ = ((\LCD|Init:state.Entry~q\) # (\LCD|Init:state.Disp~q\)) # (!\LCD|Init:state.Func~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|Init:state.Func~q\,
	datac => \LCD|Init:state.Entry~q\,
	datad => \LCD|Init:state.Disp~q\,
	combout => \LCD|s_dat~4_combout\);

-- Location: LCCOMB_X26_Y47_N2
\LCD|s_dat~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_dat~5_combout\ = (\LCD|Equal0~4_combout\ & ((\LCD|s_dat~4_combout\) # ((\LCD|Init:state.Ready~q\ & \LCD|s_dat\(2))))) # (!\LCD|Equal0~4_combout\ & (((\LCD|s_dat\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|s_dat~4_combout\,
	datab => \LCD|Init:state.Ready~q\,
	datac => \LCD|s_dat\(2),
	datad => \LCD|Equal0~4_combout\,
	combout => \LCD|s_dat~5_combout\);

-- Location: LCCOMB_X25_Y47_N30
\Mux4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux4~0_combout\ = (ci(3) & (((!ci(1) & ci(2))))) # (!ci(3) & (!ci(0) & ((ci(1)) # (ci(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001101100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(3),
	datab => ci(0),
	datac => ci(1),
	datad => ci(2),
	combout => \Mux4~0_combout\);

-- Location: LCCOMB_X25_Y47_N6
\s_d~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d~6_combout\ = (!\ri~q\ & \Mux4~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ri~q\,
	datac => \Mux4~0_combout\,
	combout => \s_d~6_combout\);

-- Location: FF_X25_Y47_N7
\s_d[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \s_d~6_combout\,
	ena => \s_d[0]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_d(2));

-- Location: FF_X26_Y47_N3
\LCD|s_dat[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|s_dat~5_combout\,
	asdata => s_d(2),
	sload => \LCD|Init:init~q\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_dat\(2));

-- Location: LCCOMB_X26_Y48_N26
\LCD|s_dat~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_dat~6_combout\ = (!\LCD|Init:state.Disp~q\ & \LCD|Init:state.Func~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:state.Disp~q\,
	datad => \LCD|Init:state.Func~q\,
	combout => \LCD|s_dat~6_combout\);

-- Location: LCCOMB_X27_Y47_N6
\LCD|s_dat~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_dat~7_combout\ = (\LCD|Equal0~4_combout\ & (((\LCD|s_dat\(3) & \LCD|Init:state.Ready~q\)) # (!\LCD|s_dat~6_combout\))) # (!\LCD|Equal0~4_combout\ & (((\LCD|s_dat\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010001110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|s_dat~6_combout\,
	datab => \LCD|Equal0~4_combout\,
	datac => \LCD|s_dat\(3),
	datad => \LCD|Init:state.Ready~q\,
	combout => \LCD|s_dat~7_combout\);

-- Location: LCCOMB_X26_Y47_N14
\Mux3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux3~0_combout\ = (ci(2) & (ci(0) & (ci(3) $ (ci(1))))) # (!ci(2) & (ci(3) & (ci(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(3),
	datab => ci(1),
	datac => ci(0),
	datad => ci(2),
	combout => \Mux3~0_combout\);

-- Location: LCCOMB_X27_Y47_N22
\s_d~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d~7_combout\ = (!\ri~q\ & \Mux3~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \ri~q\,
	datad => \Mux3~0_combout\,
	combout => \s_d~7_combout\);

-- Location: FF_X27_Y47_N23
\s_d[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \s_d~7_combout\,
	ena => \s_d[0]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_d(3));

-- Location: FF_X27_Y47_N7
\LCD|s_dat[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|s_dat~7_combout\,
	asdata => s_d(3),
	sload => \LCD|Init:init~q\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_dat\(3));

-- Location: LCCOMB_X25_Y47_N20
\s_d~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d~10_combout\ = (ci(0) & (ci(2) $ (((ci(3) & ci(1)))))) # (!ci(0) & ((ci(3) $ (ci(1))) # (!ci(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111010110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(3),
	datab => ci(0),
	datac => ci(1),
	datad => ci(2),
	combout => \s_d~10_combout\);

-- Location: LCCOMB_X25_Y47_N8
\s_d~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d~11_combout\ = (\ri~q\ & ((\s_d~10_combout\) # ((!ci(3) & ci(0))))) # (!\ri~q\ & (!ci(3) & (ci(0) & \s_d~10_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(3),
	datab => ci(0),
	datac => \ri~q\,
	datad => \s_d~10_combout\,
	combout => \s_d~11_combout\);

-- Location: FF_X25_Y47_N9
\s_d[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \s_d~11_combout\,
	ena => \s_d[0]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_d(4));

-- Location: LCCOMB_X25_Y47_N28
\LCD|s_dat[4]~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_dat[4]~8_combout\ = (\LCD|Init:init~q\ & (((s_d(4))))) # (!\LCD|Init:init~q\ & (!\LCD|Init:state.Func~q\ & ((s_d(4)) # (\LCD|Equal0~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000110110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:init~q\,
	datab => \LCD|Init:state.Func~q\,
	datac => s_d(4),
	datad => \LCD|Equal0~4_combout\,
	combout => \LCD|s_dat[4]~8_combout\);

-- Location: LCCOMB_X25_Y47_N22
\LCD|s_dat[4]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_dat[4]~9_combout\ = (\nRST~input_o\ & ((\LCD|Init:init~q\) # ((!\LCD|Init:state.Ready~q\ & \LCD|Equal0~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:init~q\,
	datab => \LCD|Init:state.Ready~q\,
	datac => \nRST~input_o\,
	datad => \LCD|Equal0~4_combout\,
	combout => \LCD|s_dat[4]~9_combout\);

-- Location: FF_X25_Y47_N29
\LCD|s_dat[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|s_dat[4]~8_combout\,
	ena => \LCD|s_dat[4]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_dat\(4));

-- Location: LCCOMB_X25_Y47_N12
\Mux1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux1~0_combout\ = (ci(0) & (ci(1) & (ci(3) $ (!ci(2))))) # (!ci(0) & (ci(3) & (ci(1) $ (!ci(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000001000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(3),
	datab => ci(0),
	datac => ci(1),
	datad => ci(2),
	combout => \Mux1~0_combout\);

-- Location: LCCOMB_X25_Y47_N10
\s_d~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d~8_combout\ = (\ri~q\) # (\Mux1~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \ri~q\,
	datad => \Mux1~0_combout\,
	combout => \s_d~8_combout\);

-- Location: LCCOMB_X25_Y47_N4
\s_d[5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d[5]~feeder_combout\ = \s_d~8_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \s_d~8_combout\,
	combout => \s_d[5]~feeder_combout\);

-- Location: FF_X25_Y47_N5
\s_d[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \s_d[5]~feeder_combout\,
	ena => \s_d[0]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_d(5));

-- Location: LCCOMB_X25_Y47_N18
\LCD|s_dat[5]~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_dat[5]~10_combout\ = (\LCD|Init:init~q\ & (((s_d(5))))) # (!\LCD|Init:init~q\ & (!\LCD|Init:state.Func~q\ & ((s_d(5)) # (\LCD|Equal0~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000110110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:init~q\,
	datab => \LCD|Init:state.Func~q\,
	datac => s_d(5),
	datad => \LCD|Equal0~4_combout\,
	combout => \LCD|s_dat[5]~10_combout\);

-- Location: FF_X25_Y47_N19
\LCD|s_dat[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|s_dat[5]~10_combout\,
	ena => \LCD|s_dat[4]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_dat\(5));

-- Location: LCCOMB_X26_Y47_N8
\LCD|s_dat~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_dat~11_combout\ = (\LCD|Equal0~4_combout\ & ((\LCD|Init:state.SetLine1~q\) # ((\LCD|Init:state.Ready~q\ & \LCD|s_dat\(6))))) # (!\LCD|Equal0~4_combout\ & (((\LCD|s_dat\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:state.Ready~q\,
	datab => \LCD|Equal0~4_combout\,
	datac => \LCD|s_dat\(6),
	datad => \LCD|Init:state.SetLine1~q\,
	combout => \LCD|s_dat~11_combout\);

-- Location: LCCOMB_X25_Y47_N26
\s_d[6]~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d[6]~13_combout\ = !\s_d~8_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \s_d~8_combout\,
	combout => \s_d[6]~13_combout\);

-- Location: FF_X25_Y47_N27
\s_d[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_nCLK~clkctrl_outclk\,
	d => \s_d[6]~13_combout\,
	ena => \s_d[0]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_d(6));

-- Location: FF_X26_Y47_N9
\LCD|s_dat[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|s_dat~11_combout\,
	asdata => s_d(6),
	sload => \LCD|Init:init~q\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_dat\(6));

-- Location: LCCOMB_X25_Y48_N20
\LCD|s_dat~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_dat~12_combout\ = (\LCD|Init:state.SetLine0~q\) # ((\LCD|Init:state.SetLine1~q\) # ((\LCD|s_dat\(7) & \LCD|Init:state.Ready~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:state.SetLine0~q\,
	datab => \LCD|Init:state.SetLine1~q\,
	datac => \LCD|s_dat\(7),
	datad => \LCD|Init:state.Ready~q\,
	combout => \LCD|s_dat~12_combout\);

-- Location: LCCOMB_X25_Y48_N24
\LCD|s_dat~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_dat~13_combout\ = (!\LCD|Init:init~q\ & ((\LCD|Equal0~4_combout\ & (\LCD|s_dat~12_combout\)) # (!\LCD|Equal0~4_combout\ & ((\LCD|s_dat\(7))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Equal0~4_combout\,
	datab => \LCD|s_dat~12_combout\,
	datac => \LCD|s_dat\(7),
	datad => \LCD|Init:init~q\,
	combout => \LCD|s_dat~13_combout\);

-- Location: FF_X25_Y48_N25
\LCD|s_dat[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|s_dat~13_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_dat\(7));

-- Location: LCCOMB_X25_Y48_N28
\LCD|s_en~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_en~0_combout\ = (\LCD|Equal0~4_combout\ & (\s_en~q\ & \LCD|Init:init~q\)) # (!\LCD|Equal0~4_combout\ & ((!\LCD|Init:init~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_en~q\,
	datac => \LCD|Equal0~4_combout\,
	datad => \LCD|Init:init~q\,
	combout => \LCD|s_en~0_combout\);

-- Location: FF_X25_Y48_N29
\LCD|s_en\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|s_en~0_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_en~q\);

-- Location: LCCOMB_X25_Y46_N26
\LCD|s_rs~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_rs~0_combout\ = (\LCD|s_rs~q\ & ((\LCD|Init:init~q\) # (!\nRST~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Init:init~q\,
	datab => \LCD|s_rs~q\,
	datac => \nRST~input_o\,
	combout => \LCD|s_rs~0_combout\);

-- Location: LCCOMB_X25_Y46_N16
\LCD|s_rs~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_rs~2_combout\ = (\LCD|s_rs~0_combout\) # ((\LCD|Init:state.Ready~q\ & \LCD|s_rs~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|s_rs~0_combout\,
	datac => \LCD|Init:state.Ready~q\,
	datad => \LCD|s_rs~1_combout\,
	combout => \LCD|s_rs~2_combout\);

-- Location: FF_X25_Y46_N17
\LCD|s_rs\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|s_rs~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_rs~q\);

-- Location: IOIBUF_X115_Y4_N15
\D[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(0),
	o => \D[0]~input_o\);

-- Location: IOIBUF_X115_Y5_N15
\D[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(1),
	o => \D[1]~input_o\);

-- Location: IOIBUF_X115_Y7_N15
\D[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(2),
	o => \D[2]~input_o\);

-- Location: IOIBUF_X115_Y9_N22
\D[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(3),
	o => \D[3]~input_o\);

-- Location: IOIBUF_X115_Y10_N8
\D[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(4),
	o => \D[4]~input_o\);

-- Location: IOIBUF_X115_Y6_N15
\D[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(5),
	o => \D[5]~input_o\);

-- Location: IOIBUF_X115_Y13_N1
\D[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(6),
	o => \D[6]~input_o\);

-- Location: IOIBUF_X115_Y14_N8
\D[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(7),
	o => \D[7]~input_o\);

-- Location: IOIBUF_X0_Y52_N15
\LCD_D[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(0),
	o => \LCD_D[0]~input_o\);

-- Location: IOIBUF_X0_Y44_N8
\LCD_D[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(1),
	o => \LCD_D[1]~input_o\);

-- Location: IOIBUF_X0_Y44_N1
\LCD_D[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(2),
	o => \LCD_D[2]~input_o\);

-- Location: IOIBUF_X0_Y49_N8
\LCD_D[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(3),
	o => \LCD_D[3]~input_o\);

-- Location: IOIBUF_X0_Y54_N8
\LCD_D[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(4),
	o => \LCD_D[4]~input_o\);

-- Location: IOIBUF_X0_Y55_N22
\LCD_D[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(5),
	o => \LCD_D[5]~input_o\);

-- Location: IOIBUF_X0_Y51_N15
\LCD_D[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(6),
	o => \LCD_D[6]~input_o\);

-- Location: IOIBUF_X0_Y47_N1
\LCD_D[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(7),
	o => \LCD_D[7]~input_o\);

ww_LCD_EN <= \LCD_EN~output_o\;

ww_LCD_RW <= \LCD_RW~output_o\;

ww_LCD_RS <= \LCD_RS~output_o\;

ww_LED_D(0) <= \LED_D[0]~output_o\;

ww_LED_D(1) <= \LED_D[1]~output_o\;

ww_LED_D(2) <= \LED_D[2]~output_o\;

ww_LED_D(3) <= \LED_D[3]~output_o\;

ww_LED_D(4) <= \LED_D[4]~output_o\;

ww_LED_D(5) <= \LED_D[5]~output_o\;

ww_LED_D(6) <= \LED_D[6]~output_o\;

ww_LED_D(7) <= \LED_D[7]~output_o\;

ww_LL_D(0) <= \LL_D[0]~output_o\;

ww_LL_D(1) <= \LL_D[1]~output_o\;

ww_LL_D(2) <= \LL_D[2]~output_o\;

ww_LL_D(3) <= \LL_D[3]~output_o\;

ww_LL_D(4) <= \LL_D[4]~output_o\;

ww_LL_D(5) <= \LL_D[5]~output_o\;

ww_LL_D(6) <= \LL_D[6]~output_o\;

ww_LL_D(7) <= \LL_D[7]~output_o\;

LCD_D(0) <= \LCD_D[0]~output_o\;

LCD_D(1) <= \LCD_D[1]~output_o\;

LCD_D(2) <= \LCD_D[2]~output_o\;

LCD_D(3) <= \LCD_D[3]~output_o\;

LCD_D(4) <= \LCD_D[4]~output_o\;

LCD_D(5) <= \LCD_D[5]~output_o\;

LCD_D(6) <= \LCD_D[6]~output_o\;

LCD_D(7) <= \LCD_D[7]~output_o\;
END structure;


