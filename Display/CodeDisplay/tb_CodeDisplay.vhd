LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_CodeDisplay IS
END ENTITY;

ARCHITECTURE Testbench OF tb_CodeDisplay IS
	COMPONENT CodeDisplay IS
		GENERIC (
			F_CLK					:	Integer	range 0 to 2147483647 	:= 50*(10**6);
			SECONDS_PER_TICK	:	Integer	range 0 to 60				:= 1
		);
		PORT (
			CLK, nRST	:	IN		std_logic;
			-- Init Value Switches
			SW				:	IN		std_logic_vector(3 downto 0);
			-- LCD Display Connections
			LCD_EN, LCD_RS, LCD_RW	:	OUT	std_logic;
			LCD_Data		:	OUT	std_logic_vector(7 downto 0);
			-- LED Display Connections
			LED_GRAY, LED_AIK	:	OUT	std_logic_vector(3 downto 0);
			-- Hex Display Connections
			SEG0					:	OUT	std_logic_vector(6 downto 0)
		);
	END COMPONENT;
	
	SIGNAL s_clk, s_rst	:	std_logic	:= '1';
	SIGNAL s_sw				:	std_logic_vector(3 downto 0)	:= (Others => '0');
	SIGNAL s_seg			:	std_logic_vector(6 downto 0)	:= (Others => '0');
	
BEGIN
	
	DUT	:	CodeDisplay
	GENERIC MAP (50*(10**6), 1)
	PORT MAP (
		CLK => s_clk, 
		nRST => s_rst, 
		SW => s_sw, 
		SEG0 => s_seg
	);
	
	s_clk <= NOT s_clk after 20ns;
	
	
	s_sw <= "0100";
	s_rst <= '0' after 400ns, '1' after 450ns;
	
END Testbench;