LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY CodeDisplay IS
	GENERIC (
		F_CLK					:	Integer	range 0 to 2147483647 	:= 50*(10**6);
		SECONDS_PER_TICK	:	Integer	range 0 to 60				:= 1
	);
	PORT (
		CLK, nRST	:	IN		std_logic;
		-- Init Value Switches
		SW				:	IN		std_logic_vector(3 downto 0);
		-- LCD Display Connections
		LCD_EN, LCD_RS, LCD_RW	:	OUT	std_logic;
		LCD_Data		:	OUT	std_logic_vector(7 downto 0);
		-- LED Display Connections
		LED_GRAY, LED_AIK	:	OUT	std_logic_vector(3 downto 0);
		-- Hex Display Connections
		SEG0					:	OUT	std_logic_vector(6 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF CodeDisplay IS

	COMPONENT LCDCodesDisplay IS
		GENERIC (
			F_CLK					:	Integer range 0 to 2147483647 := 50*(10**6);
			FRAMES_PER_SECOND	:	Integer range 0 to 60			:=	24
		);
		PORT (
			CLK, nRST			:	IN		std_logic;
			BCD, GRAY, AIKEN	:	IN		std_logic_vector(3 downto 0);
			-- LCD Connections
			LCD_Data						:	OUT	std_logic_vector(7 downto 0);
			LCD_EN, LCD_RW, LCD_RS	:	OUT	std_logic
		);
	END COMPONENT;

	COMPONENT Counter IS
		GENERIC (
			Limit		:	Integer	:= 255
		);
		PORT (
			CLK, nRST	:	IN		std_logic;
			Start			:	IN		integer range 0 to Limit-1;
			Ov				:	OUT	std_logic;
			Q				:	OUT	integer range 0 to Limit
		);
	END COMPONENT;
	
	COMPONENT Segmentdisplay IS
		PORT (
			LTN, BLN, HB	:	IN		std_logic;
			D					:	IN		std_logic_vector(3 downto 0);
			S					:	OUT	std_logic_vector(6 downto 0)
		);
	END COMPONENT;
	
	COMPONENT GrayEncoder IS
		PORT (
			D		:	IN		std_logic_vector(3 downto 0);
			Q		:	OUT	std_logic_vector(3 downto 0)
		);
	END COMPONENT;
	
	COMPONENT AikenEncoder IS
		PORT (
			D		:	IN		std_logic_vector(3 downto 0);
			Q		:	OUT	std_logic_vector(3 downto 0)
		);
	END COMPONENT;
	
	CONSTANT HoldTime : Integer range 0 to 2147483647	:= SECONDS_PER_TICK*F_CLK/2;

	SIGNAL s_ltn, s_rst, s_clk				:	std_logic	:= '1';
	SIGNAL s_ov, s_bln, s_hb, s_rdy		:	std_logic	:= '0';
	SIGNAL s_q									:	integer range 0 to 16 := 0;
	
	SIGNAL s_start						:	integer range 0 to 15 := 0;
	SIGNAL s_seg, s_gray, s_aiken	:	std_logic_vector(3 downto 0) := (Others => '0');
	
BEGIN

	s_start <= to_integer(unsigned(SW));
	s_seg <= std_logic_vector(to_unsigned(s_q, 4));
	
	s_rst <= (nRST XOR s_ov);
	
	TIMER : PROCESS (CLK, nRST)
		VARIABLE c	:	Integer range 0 to 2147483647 := 0;
	BEGIN
		if ( nRST = '1' ) then
			if ( rising_edge(CLK) ) then
				if ( c = 0 ) then
					s_clk <= not s_clk;
					c := HoldTime;
				else
					c := c - 1;
				end if;
			else
				-- out of clock
			end if;
		else
			c := 0;
		end if;
	END PROCESS TIMER;
	
	COUNT 	: Counter
	GENERIC MAP(16)
	PORT MAP (s_clk, s_rst, s_start, s_ov, s_q);

	SEGDISP	: Segmentdisplay
	PORT MAP (s_ltn, s_bln, s_hb, s_seg, SEG0);
	
	GRAY		: GrayEncoder
	PORT MAP (s_seg, s_gray);
	
	AIKEN		: AikenEncoder
	PORT MAP (s_seg, s_aiken);
	
	LCD		: LCDCodesDisplay
	GENERIC MAP (F_CLK, 24)
	PORT MAP (
		CLK => CLK,
		nRST => nRST,
		BCD => s_seg,
		GRAY => s_gray,
		AIKEN => s_aiken,
		LCD_Data => LCD_Data,
		LCD_EN => LCD_EN,
		LCD_RS => LCD_RS,
		LCD_RW => LCD_RW
	);
	
	LED_GRAY <= s_gray;
	LED_AIK <= s_aiken;

END Behaviour;