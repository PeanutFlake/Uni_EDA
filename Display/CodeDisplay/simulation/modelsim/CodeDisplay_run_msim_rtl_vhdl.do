transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {G:/workbench/Altera/projects/Uni_EDA/Display/LCDDeviceController/LCDDeviceController.vhd}
vcom -93 -work work {G:/workbench/Altera/projects/Uni_EDA/Display/LCDCodesDisplay/LCDCodesDisplay.vhd}
vcom -93 -work work {G:/workbench/Altera/projects/Uni_EDA/Codes/Grey/GrayEncoder.vhd}
vcom -93 -work work {G:/workbench/Altera/projects/Uni_EDA/Codes/Aiken/AikenEncoder.vhd}
vcom -93 -work work {G:/workbench/Altera/projects/Uni_EDA/Display/Segmentdisplay/Segmentdisplay.vhd}
vcom -93 -work work {G:/workbench/Altera/projects/Uni_EDA/Counter/Counter.vhd}
vcom -93 -work work {G:/workbench/Altera/projects/Uni_EDA/Display/CodeDisplay/CodeDisplay.vhd}

