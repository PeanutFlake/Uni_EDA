-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "06/18/2018 19:42:45"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	CodeDisplay IS
    PORT (
	CLK : IN std_logic;
	nRST : IN std_logic;
	SW : IN std_logic_vector(3 DOWNTO 0);
	LCD_EN : OUT std_logic;
	LCD_RS : OUT std_logic;
	LCD_RW : OUT std_logic;
	LCD_Data : OUT std_logic_vector(7 DOWNTO 0);
	LED_GRAY : OUT std_logic_vector(3 DOWNTO 0);
	LED_AIK : OUT std_logic_vector(3 DOWNTO 0);
	SEG0 : OUT std_logic_vector(6 DOWNTO 0)
	);
END CodeDisplay;

-- Design Ports Information
-- LCD_EN	=>  Location: PIN_L4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_RS	=>  Location: PIN_M2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_RW	=>  Location: PIN_M1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[0]	=>  Location: PIN_L3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[1]	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[2]	=>  Location: PIN_L2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[3]	=>  Location: PIN_K7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[4]	=>  Location: PIN_K1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[5]	=>  Location: PIN_K2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[6]	=>  Location: PIN_M3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[7]	=>  Location: PIN_M5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_GRAY[0]	=>  Location: PIN_E21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_GRAY[1]	=>  Location: PIN_E22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_GRAY[2]	=>  Location: PIN_E25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_GRAY[3]	=>  Location: PIN_E24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_AIK[0]	=>  Location: PIN_H21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_AIK[1]	=>  Location: PIN_G20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_AIK[2]	=>  Location: PIN_G22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_AIK[3]	=>  Location: PIN_G21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SEG0[0]	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SEG0[1]	=>  Location: PIN_F22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SEG0[2]	=>  Location: PIN_E17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SEG0[3]	=>  Location: PIN_L26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SEG0[4]	=>  Location: PIN_L25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SEG0[5]	=>  Location: PIN_J22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SEG0[6]	=>  Location: PIN_H22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- nRST	=>  Location: PIN_M23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_AA23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_AA22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_Y24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_Y23,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF CodeDisplay IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_nRST : std_logic;
SIGNAL ww_SW : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_LCD_EN : std_logic;
SIGNAL ww_LCD_RS : std_logic;
SIGNAL ww_LCD_RW : std_logic;
SIGNAL ww_LCD_Data : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_LED_GRAY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_LED_AIK : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_SEG0 : std_logic_vector(6 DOWNTO 0);
SIGNAL \CLK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \s_clk~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \s_rst~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \LCD_EN~output_o\ : std_logic;
SIGNAL \LCD_RS~output_o\ : std_logic;
SIGNAL \LCD_RW~output_o\ : std_logic;
SIGNAL \LCD_Data[0]~output_o\ : std_logic;
SIGNAL \LCD_Data[1]~output_o\ : std_logic;
SIGNAL \LCD_Data[2]~output_o\ : std_logic;
SIGNAL \LCD_Data[3]~output_o\ : std_logic;
SIGNAL \LCD_Data[4]~output_o\ : std_logic;
SIGNAL \LCD_Data[5]~output_o\ : std_logic;
SIGNAL \LCD_Data[6]~output_o\ : std_logic;
SIGNAL \LCD_Data[7]~output_o\ : std_logic;
SIGNAL \LED_GRAY[0]~output_o\ : std_logic;
SIGNAL \LED_GRAY[1]~output_o\ : std_logic;
SIGNAL \LED_GRAY[2]~output_o\ : std_logic;
SIGNAL \LED_GRAY[3]~output_o\ : std_logic;
SIGNAL \LED_AIK[0]~output_o\ : std_logic;
SIGNAL \LED_AIK[1]~output_o\ : std_logic;
SIGNAL \LED_AIK[2]~output_o\ : std_logic;
SIGNAL \LED_AIK[3]~output_o\ : std_logic;
SIGNAL \SEG0[0]~output_o\ : std_logic;
SIGNAL \SEG0[1]~output_o\ : std_logic;
SIGNAL \SEG0[2]~output_o\ : std_logic;
SIGNAL \SEG0[3]~output_o\ : std_logic;
SIGNAL \SEG0[4]~output_o\ : std_logic;
SIGNAL \SEG0[5]~output_o\ : std_logic;
SIGNAL \SEG0[6]~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \CLK~inputclkctrl_outclk\ : std_logic;
SIGNAL \nRST~input_o\ : std_logic;
SIGNAL \LCD|DISPLAY|Selector1~0_combout\ : std_logic;
SIGNAL \LCD|Add2~0_combout\ : std_logic;
SIGNAL \LCD|t_wait[0]~54_combout\ : std_logic;
SIGNAL \LCD|Add2~1\ : std_logic;
SIGNAL \LCD|Add2~2_combout\ : std_logic;
SIGNAL \LCD|t_wait[1]~53_combout\ : std_logic;
SIGNAL \LCD|Add2~3\ : std_logic;
SIGNAL \LCD|Add2~4_combout\ : std_logic;
SIGNAL \LCD|t_wait[2]~52_combout\ : std_logic;
SIGNAL \LCD|Add2~5\ : std_logic;
SIGNAL \LCD|Add2~6_combout\ : std_logic;
SIGNAL \LCD|ci[0]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_busy~3_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_busy~q\ : std_logic;
SIGNAL \LCD|s_data[0]~0_combout\ : std_logic;
SIGNAL \LCD|Add0~0_combout\ : std_logic;
SIGNAL \LCD|Add0~1_combout\ : std_logic;
SIGNAL \LCD|Add0~2_combout\ : std_logic;
SIGNAL \LCD|t_wait[30]~35_combout\ : std_logic;
SIGNAL \LCD|t_wait[30]~36_combout\ : std_logic;
SIGNAL \LCD|Add2~7\ : std_logic;
SIGNAL \LCD|Add2~8_combout\ : std_logic;
SIGNAL \LCD|Add2~9\ : std_logic;
SIGNAL \LCD|Add2~10_combout\ : std_logic;
SIGNAL \LCD|t_wait[5]~51_combout\ : std_logic;
SIGNAL \LCD|Add2~11\ : std_logic;
SIGNAL \LCD|Add2~12_combout\ : std_logic;
SIGNAL \LCD|Add2~13\ : std_logic;
SIGNAL \LCD|Add2~14_combout\ : std_logic;
SIGNAL \LCD|Add2~15\ : std_logic;
SIGNAL \LCD|Add2~16_combout\ : std_logic;
SIGNAL \LCD|t_wait[8]~50_combout\ : std_logic;
SIGNAL \LCD|Add2~17\ : std_logic;
SIGNAL \LCD|Add2~18_combout\ : std_logic;
SIGNAL \LCD|t_wait[9]~49_combout\ : std_logic;
SIGNAL \LCD|Add2~19\ : std_logic;
SIGNAL \LCD|Add2~20_combout\ : std_logic;
SIGNAL \LCD|t_wait[10]~48_combout\ : std_logic;
SIGNAL \LCD|Equal0~6_combout\ : std_logic;
SIGNAL \LCD|Add2~21\ : std_logic;
SIGNAL \LCD|Add2~22_combout\ : std_logic;
SIGNAL \LCD|t_wait[11]~47_combout\ : std_logic;
SIGNAL \LCD|Add2~23\ : std_logic;
SIGNAL \LCD|Add2~24_combout\ : std_logic;
SIGNAL \LCD|Add2~25\ : std_logic;
SIGNAL \LCD|Add2~26_combout\ : std_logic;
SIGNAL \LCD|Add2~27\ : std_logic;
SIGNAL \LCD|Add2~28_combout\ : std_logic;
SIGNAL \LCD|t_wait[14]~46_combout\ : std_logic;
SIGNAL \LCD|Equal0~5_combout\ : std_logic;
SIGNAL \LCD|Equal0~7_combout\ : std_logic;
SIGNAL \LCD|Equal0~8_combout\ : std_logic;
SIGNAL \LCD|Add2~29\ : std_logic;
SIGNAL \LCD|Add2~30_combout\ : std_logic;
SIGNAL \LCD|Add2~31\ : std_logic;
SIGNAL \LCD|Add2~32_combout\ : std_logic;
SIGNAL \LCD|t_wait[16]~45_combout\ : std_logic;
SIGNAL \LCD|Add2~33\ : std_logic;
SIGNAL \LCD|Add2~34_combout\ : std_logic;
SIGNAL \LCD|t_wait[17]~44_combout\ : std_logic;
SIGNAL \LCD|Add2~35\ : std_logic;
SIGNAL \LCD|Add2~36_combout\ : std_logic;
SIGNAL \LCD|Add2~37\ : std_logic;
SIGNAL \LCD|Add2~38_combout\ : std_logic;
SIGNAL \LCD|Add2~39\ : std_logic;
SIGNAL \LCD|Add2~40_combout\ : std_logic;
SIGNAL \LCD|Add2~41\ : std_logic;
SIGNAL \LCD|Add2~42_combout\ : std_logic;
SIGNAL \LCD|t_wait[21]~43_combout\ : std_logic;
SIGNAL \LCD|Add2~43\ : std_logic;
SIGNAL \LCD|Add2~44_combout\ : std_logic;
SIGNAL \LCD|Add2~45\ : std_logic;
SIGNAL \LCD|Add2~46_combout\ : std_logic;
SIGNAL \LCD|Add2~47\ : std_logic;
SIGNAL \LCD|Add2~48_combout\ : std_logic;
SIGNAL \LCD|t_wait[24]~42_combout\ : std_logic;
SIGNAL \LCD|Add2~49\ : std_logic;
SIGNAL \LCD|Add2~50_combout\ : std_logic;
SIGNAL \LCD|Add2~51\ : std_logic;
SIGNAL \LCD|Add2~52_combout\ : std_logic;
SIGNAL \LCD|t_wait[26]~41_combout\ : std_logic;
SIGNAL \LCD|Equal0~1_combout\ : std_logic;
SIGNAL \LCD|Equal0~2_combout\ : std_logic;
SIGNAL \LCD|Add2~53\ : std_logic;
SIGNAL \LCD|Add2~54_combout\ : std_logic;
SIGNAL \LCD|t_wait[27]~40_combout\ : std_logic;
SIGNAL \LCD|Add2~55\ : std_logic;
SIGNAL \LCD|Add2~56_combout\ : std_logic;
SIGNAL \LCD|t_wait[28]~39_combout\ : std_logic;
SIGNAL \LCD|Add2~57\ : std_logic;
SIGNAL \LCD|Add2~58_combout\ : std_logic;
SIGNAL \LCD|t_wait[29]~38_combout\ : std_logic;
SIGNAL \LCD|t_wait[30]~34_combout\ : std_logic;
SIGNAL \LCD|Add2~59\ : std_logic;
SIGNAL \LCD|Add2~60_combout\ : std_logic;
SIGNAL \LCD|t_wait[30]~37_combout\ : std_logic;
SIGNAL \LCD|Equal0~0_combout\ : std_logic;
SIGNAL \LCD|Equal0~3_combout\ : std_logic;
SIGNAL \LCD|Equal0~4_combout\ : std_logic;
SIGNAL \LCD|Equal0~9_combout\ : std_logic;
SIGNAL \LCD|s_en~0_combout\ : std_logic;
SIGNAL \LCD|s_en~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_next.Ready~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_send_next~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_send_next~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_send_current~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_send_current~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Equal0~10_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|rs~4_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|rs~2_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|rs~3_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:rs~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:rs~q\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_y~1_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_x[2]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_x[2]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~2_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_y~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_next.Send~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_next.Init~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_next.Init~1_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_next.Ready~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_current.Ready~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_current.Ready~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_busy~2_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_next.Send~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_current.Send~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_current.Send~q\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~24_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[0]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[0]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~25\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~26_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[1]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[1]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~27\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~28_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[2]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[2]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~29\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~30_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~91_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[3]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~31\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~32_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[4]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~4_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[4]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~33\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~34_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~90_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[5]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~35\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~36_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[6]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[6]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~37\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~38_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[7]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[7]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~39\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~40_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[8]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[8]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~41\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~42_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[9]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[9]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~43\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~44_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[10]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[10]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Equal0~5_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~51\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~52_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[14]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[14]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~3_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~53\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~54_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[15]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[15]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Equal0~8_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~55\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~56_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[16]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[16]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~57\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~58_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Equal0~7_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Equal0~6_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Equal0~9_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_next.Finish~1_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_next.Func~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_current.Func~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Selector2~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_next.Disp~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_current.Disp~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_current.Disp~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Selector3~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_next.Entry~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_next.Entry~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_current.Entry~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Selector4~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_next.Finish~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_next.Finish~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_current.Finish~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_current.Finish~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_next.Reset~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_next.Reset~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_current.Reset~q\ : std_logic;
SIGNAL \LCD|DISPLAY|t_wait~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~45\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~46_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[11]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[11]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~47\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~48_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[12]~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[12]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~49\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~50_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~89_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[13]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~59\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~60_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~88_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[18]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~61\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~62_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~3_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Equal0~3_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~63\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~64_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~103_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[20]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~65\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~66_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~102_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[21]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~67\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~68_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~101_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[22]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~69\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~70_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~100_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[23]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Equal0~2_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~71\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~72_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~99_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[24]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~73\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~74_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~98_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[25]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~75\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~76_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~97_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[26]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~77\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~78_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~96_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[27]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~79\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~80_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~95_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[28]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~81\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~82_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~94_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[29]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~83\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~84_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~93_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[30]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~85\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~86_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Add1~92_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:t_wait[31]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|Equal0~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Equal0~1_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|Equal0~4_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_init_next.Finish~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_next.Start~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_next.Start~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_current.Start~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_next.Init~2_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_next.Init~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_current.Init~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_state_current.Init~q\ : std_logic;
SIGNAL \LCD|DISPLAY|en~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:en~q\ : std_logic;
SIGNAL \LCD|DISPLAY|en~1_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_en~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_en~q\ : std_logic;
SIGNAL \LCD|DISPLAY|s_rs~q\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:data[0]~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:data[0]~q\ : std_logic;
SIGNAL \LCD|Mux6~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~5_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~4_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~6_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~7_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:data[1]~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:data[1]~q\ : std_logic;
SIGNAL \LCD|Mux5~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~8_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~11_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~30_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~10_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~9_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~12_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~13_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~14_combout\ : std_logic;
SIGNAL \LCD|Mux4~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~15_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:data[2]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|data~16_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_data[2]~feeder_combout\ : std_logic;
SIGNAL \LCD|Mux3~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~17_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:data[3]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|data~18_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_data[3]~feeder_combout\ : std_logic;
SIGNAL \LCD|Mux2~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~19_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:data[4]~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:data[4]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|data~20_combout\ : std_logic;
SIGNAL \LCD|Mux1~0_combout\ : std_logic;
SIGNAL \LCD|s_data[5]~1_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~21_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:data[5]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|data~22_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_data[5]~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~25_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_y~2_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:c_pos_y~q\ : std_logic;
SIGNAL \LCD|DISPLAY|data~26_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:data[6]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|data~23_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~24_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~27_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_data[6]~feeder_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|P_UEBERGANG:data[7]~q\ : std_logic;
SIGNAL \LCD|DISPLAY|data~28_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|data~29_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_data[7]~feeder_combout\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \Add0~0_combout\ : std_logic;
SIGNAL \c~18_combout\ : std_logic;
SIGNAL \TIMER:c[0]~q\ : std_logic;
SIGNAL \Add0~1\ : std_logic;
SIGNAL \Add0~2_combout\ : std_logic;
SIGNAL \c~17_combout\ : std_logic;
SIGNAL \TIMER:c[1]~q\ : std_logic;
SIGNAL \Add0~3\ : std_logic;
SIGNAL \Add0~4_combout\ : std_logic;
SIGNAL \c~16_combout\ : std_logic;
SIGNAL \TIMER:c[2]~q\ : std_logic;
SIGNAL \Add0~5\ : std_logic;
SIGNAL \Add0~6_combout\ : std_logic;
SIGNAL \c~15_combout\ : std_logic;
SIGNAL \TIMER:c[3]~q\ : std_logic;
SIGNAL \Add0~7\ : std_logic;
SIGNAL \Add0~8_combout\ : std_logic;
SIGNAL \TIMER:c[4]~q\ : std_logic;
SIGNAL \Add0~9\ : std_logic;
SIGNAL \Add0~10_combout\ : std_logic;
SIGNAL \c~14_combout\ : std_logic;
SIGNAL \TIMER:c[5]~q\ : std_logic;
SIGNAL \Add0~11\ : std_logic;
SIGNAL \Add0~12_combout\ : std_logic;
SIGNAL \c~13_combout\ : std_logic;
SIGNAL \TIMER:c[6]~q\ : std_logic;
SIGNAL \Add0~13\ : std_logic;
SIGNAL \Add0~14_combout\ : std_logic;
SIGNAL \c~12_combout\ : std_logic;
SIGNAL \TIMER:c[7]~q\ : std_logic;
SIGNAL \Add0~15\ : std_logic;
SIGNAL \Add0~16_combout\ : std_logic;
SIGNAL \c~11_combout\ : std_logic;
SIGNAL \TIMER:c[8]~q\ : std_logic;
SIGNAL \Add0~17\ : std_logic;
SIGNAL \Add0~18_combout\ : std_logic;
SIGNAL \TIMER:c[9]~q\ : std_logic;
SIGNAL \Add0~19\ : std_logic;
SIGNAL \Add0~20_combout\ : std_logic;
SIGNAL \TIMER:c[10]~q\ : std_logic;
SIGNAL \Equal0~6_combout\ : std_logic;
SIGNAL \Add0~21\ : std_logic;
SIGNAL \Add0~22_combout\ : std_logic;
SIGNAL \TIMER:c[11]~q\ : std_logic;
SIGNAL \Add0~23\ : std_logic;
SIGNAL \Add0~24_combout\ : std_logic;
SIGNAL \TIMER:c[12]~q\ : std_logic;
SIGNAL \Add0~25\ : std_logic;
SIGNAL \Add0~26_combout\ : std_logic;
SIGNAL \c~10_combout\ : std_logic;
SIGNAL \TIMER:c[13]~q\ : std_logic;
SIGNAL \Add0~27\ : std_logic;
SIGNAL \Add0~28_combout\ : std_logic;
SIGNAL \TIMER:c[14]~q\ : std_logic;
SIGNAL \Equal0~5_combout\ : std_logic;
SIGNAL \Equal0~7_combout\ : std_logic;
SIGNAL \Equal0~8_combout\ : std_logic;
SIGNAL \Add0~29\ : std_logic;
SIGNAL \Add0~30_combout\ : std_logic;
SIGNAL \c~9_combout\ : std_logic;
SIGNAL \TIMER:c[15]~q\ : std_logic;
SIGNAL \Add0~31\ : std_logic;
SIGNAL \Add0~32_combout\ : std_logic;
SIGNAL \TIMER:c[16]~q\ : std_logic;
SIGNAL \Add0~33\ : std_logic;
SIGNAL \Add0~34_combout\ : std_logic;
SIGNAL \TIMER:c[17]~q\ : std_logic;
SIGNAL \Add0~35\ : std_logic;
SIGNAL \Add0~36_combout\ : std_logic;
SIGNAL \TIMER:c[18]~q\ : std_logic;
SIGNAL \Add0~37\ : std_logic;
SIGNAL \Add0~38_combout\ : std_logic;
SIGNAL \TIMER:c[19]~q\ : std_logic;
SIGNAL \Add0~39\ : std_logic;
SIGNAL \Add0~40_combout\ : std_logic;
SIGNAL \TIMER:c[20]~q\ : std_logic;
SIGNAL \Add0~41\ : std_logic;
SIGNAL \Add0~42_combout\ : std_logic;
SIGNAL \c~8_combout\ : std_logic;
SIGNAL \TIMER:c[21]~q\ : std_logic;
SIGNAL \Add0~43\ : std_logic;
SIGNAL \Add0~44_combout\ : std_logic;
SIGNAL \TIMER:c[22]~q\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \Add0~45\ : std_logic;
SIGNAL \Add0~46_combout\ : std_logic;
SIGNAL \c~7_combout\ : std_logic;
SIGNAL \TIMER:c[23]~q\ : std_logic;
SIGNAL \Add0~47\ : std_logic;
SIGNAL \Add0~48_combout\ : std_logic;
SIGNAL \c~6_combout\ : std_logic;
SIGNAL \TIMER:c[24]~q\ : std_logic;
SIGNAL \Add0~49\ : std_logic;
SIGNAL \Add0~50_combout\ : std_logic;
SIGNAL \c~5_combout\ : std_logic;
SIGNAL \TIMER:c[25]~q\ : std_logic;
SIGNAL \Add0~51\ : std_logic;
SIGNAL \Add0~52_combout\ : std_logic;
SIGNAL \c~4_combout\ : std_logic;
SIGNAL \TIMER:c[26]~q\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Equal0~3_combout\ : std_logic;
SIGNAL \Add0~53\ : std_logic;
SIGNAL \Add0~54_combout\ : std_logic;
SIGNAL \c~3_combout\ : std_logic;
SIGNAL \TIMER:c[27]~q\ : std_logic;
SIGNAL \Add0~55\ : std_logic;
SIGNAL \Add0~56_combout\ : std_logic;
SIGNAL \c~2_combout\ : std_logic;
SIGNAL \TIMER:c[28]~q\ : std_logic;
SIGNAL \Add0~57\ : std_logic;
SIGNAL \Add0~58_combout\ : std_logic;
SIGNAL \c~1_combout\ : std_logic;
SIGNAL \TIMER:c[29]~q\ : std_logic;
SIGNAL \Add0~59\ : std_logic;
SIGNAL \Add0~60_combout\ : std_logic;
SIGNAL \c~0_combout\ : std_logic;
SIGNAL \TIMER:c[30]~q\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \Equal0~4_combout\ : std_logic;
SIGNAL \Equal0~9_combout\ : std_logic;
SIGNAL \s_clk~0_combout\ : std_logic;
SIGNAL \s_clk~feeder_combout\ : std_logic;
SIGNAL \s_clk~q\ : std_logic;
SIGNAL \s_clk~clkctrl_outclk\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \s_rst~clkctrl_outclk\ : std_logic;
SIGNAL \COUNT|Count:c[2]~1_combout\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \COUNT|Count:c[0]~1_combout\ : std_logic;
SIGNAL \COUNT|Count:c[0]~3_combout\ : std_logic;
SIGNAL \COUNT|Count:c[0]~_emulated_q\ : std_logic;
SIGNAL \COUNT|Count:c[0]~2_combout\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \COUNT|Count:c[1]~1_combout\ : std_logic;
SIGNAL \COUNT|Add0~0_combout\ : std_logic;
SIGNAL \COUNT|Count:c[1]~3_combout\ : std_logic;
SIGNAL \COUNT|Count:c[1]~_emulated_q\ : std_logic;
SIGNAL \COUNT|Count:c[1]~2_combout\ : std_logic;
SIGNAL \COUNT|Add0~1\ : std_logic;
SIGNAL \COUNT|Add0~2_combout\ : std_logic;
SIGNAL \COUNT|Count:c[2]~3_combout\ : std_logic;
SIGNAL \COUNT|Count:c[2]~_emulated_q\ : std_logic;
SIGNAL \COUNT|Count:c[2]~2_combout\ : std_logic;
SIGNAL \COUNT|Add0~3\ : std_logic;
SIGNAL \COUNT|Add0~5\ : std_logic;
SIGNAL \COUNT|Add0~6_combout\ : std_logic;
SIGNAL \COUNT|Count:c[4]~0_combout\ : std_logic;
SIGNAL \COUNT|Count:c[4]~q\ : std_logic;
SIGNAL \COUNT|Equal0~0_combout\ : std_logic;
SIGNAL \COUNT|s_ov~0_combout\ : std_logic;
SIGNAL \COUNT|s_ov~q\ : std_logic;
SIGNAL \s_rst~combout\ : std_logic;
SIGNAL \COUNT|Count:c[3]~1_combout\ : std_logic;
SIGNAL \COUNT|Add0~4_combout\ : std_logic;
SIGNAL \COUNT|Count:c[3]~3_combout\ : std_logic;
SIGNAL \COUNT|Count:c[3]~_emulated_q\ : std_logic;
SIGNAL \COUNT|Count:c[3]~2_combout\ : std_logic;
SIGNAL \GRAY|Mux3~0_combout\ : std_logic;
SIGNAL \GRAY|Mux2~0_combout\ : std_logic;
SIGNAL \GRAY|Mux1~0_combout\ : std_logic;
SIGNAL \GRAY|Mux0~0_combout\ : std_logic;
SIGNAL \AIKEN|Mux3~0_combout\ : std_logic;
SIGNAL \AIKEN|Mux2~0_combout\ : std_logic;
SIGNAL \AIKEN|Mux1~0_combout\ : std_logic;
SIGNAL \AIKEN|Mux0~0_combout\ : std_logic;
SIGNAL \SEGDISP|Mux6~0_combout\ : std_logic;
SIGNAL \SEGDISP|Mux5~0_combout\ : std_logic;
SIGNAL \SEGDISP|Mux4~0_combout\ : std_logic;
SIGNAL \SEGDISP|Mux3~0_combout\ : std_logic;
SIGNAL \SEGDISP|Mux2~0_combout\ : std_logic;
SIGNAL \SEGDISP|Mux1~0_combout\ : std_logic;
SIGNAL \SEGDISP|Mux0~0_combout\ : std_logic;
SIGNAL \LCD|DISPLAY|s_data\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \LCD|s_data\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \LCD|ci\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \LCD|t_wait\ : std_logic_vector(30 DOWNTO 0);
SIGNAL \ALT_INV_s_rst~clkctrl_outclk\ : std_logic;
SIGNAL \ALT_INV_s_clk~clkctrl_outclk\ : std_logic;
SIGNAL \ALT_INV_s_clk~q\ : std_logic;
SIGNAL \SEGDISP|ALT_INV_Mux0~0_combout\ : std_logic;

BEGIN

ww_CLK <= CLK;
ww_nRST <= nRST;
ww_SW <= SW;
LCD_EN <= ww_LCD_EN;
LCD_RS <= ww_LCD_RS;
LCD_RW <= ww_LCD_RW;
LCD_Data <= ww_LCD_Data;
LED_GRAY <= ww_LED_GRAY;
LED_AIK <= ww_LED_AIK;
SEG0 <= ww_SEG0;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLK~input_o\);

\s_clk~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \s_clk~q\);

\s_rst~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \s_rst~combout\);
\ALT_INV_s_rst~clkctrl_outclk\ <= NOT \s_rst~clkctrl_outclk\;
\ALT_INV_s_clk~clkctrl_outclk\ <= NOT \s_clk~clkctrl_outclk\;
\ALT_INV_s_clk~q\ <= NOT \s_clk~q\;
\SEGDISP|ALT_INV_Mux0~0_combout\ <= NOT \SEGDISP|Mux0~0_combout\;

-- Location: IOOBUF_X0_Y52_N2
\LCD_EN~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|DISPLAY|s_en~q\,
	devoe => ww_devoe,
	o => \LCD_EN~output_o\);

-- Location: IOOBUF_X0_Y44_N16
\LCD_RS~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|DISPLAY|s_rs~q\,
	devoe => ww_devoe,
	o => \LCD_RS~output_o\);

-- Location: IOOBUF_X0_Y44_N23
\LCD_RW~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_RW~output_o\);

-- Location: IOOBUF_X0_Y52_N16
\LCD_Data[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|DISPLAY|s_data\(0),
	devoe => ww_devoe,
	o => \LCD_Data[0]~output_o\);

-- Location: IOOBUF_X0_Y44_N9
\LCD_Data[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|DISPLAY|s_data\(1),
	devoe => ww_devoe,
	o => \LCD_Data[1]~output_o\);

-- Location: IOOBUF_X0_Y44_N2
\LCD_Data[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|DISPLAY|s_data\(2),
	devoe => ww_devoe,
	o => \LCD_Data[2]~output_o\);

-- Location: IOOBUF_X0_Y49_N9
\LCD_Data[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|DISPLAY|s_data\(3),
	devoe => ww_devoe,
	o => \LCD_Data[3]~output_o\);

-- Location: IOOBUF_X0_Y54_N9
\LCD_Data[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|DISPLAY|s_data\(4),
	devoe => ww_devoe,
	o => \LCD_Data[4]~output_o\);

-- Location: IOOBUF_X0_Y55_N23
\LCD_Data[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|DISPLAY|s_data\(5),
	devoe => ww_devoe,
	o => \LCD_Data[5]~output_o\);

-- Location: IOOBUF_X0_Y51_N16
\LCD_Data[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|DISPLAY|s_data\(6),
	devoe => ww_devoe,
	o => \LCD_Data[6]~output_o\);

-- Location: IOOBUF_X0_Y47_N2
\LCD_Data[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LCD|DISPLAY|s_data\(7),
	devoe => ww_devoe,
	o => \LCD_Data[7]~output_o\);

-- Location: IOOBUF_X107_Y73_N9
\LED_GRAY[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \GRAY|Mux3~0_combout\,
	devoe => ww_devoe,
	o => \LED_GRAY[0]~output_o\);

-- Location: IOOBUF_X111_Y73_N9
\LED_GRAY[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \GRAY|Mux2~0_combout\,
	devoe => ww_devoe,
	o => \LED_GRAY[1]~output_o\);

-- Location: IOOBUF_X83_Y73_N2
\LED_GRAY[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \GRAY|Mux1~0_combout\,
	devoe => ww_devoe,
	o => \LED_GRAY[2]~output_o\);

-- Location: IOOBUF_X85_Y73_N23
\LED_GRAY[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \GRAY|Mux0~0_combout\,
	devoe => ww_devoe,
	o => \LED_GRAY[3]~output_o\);

-- Location: IOOBUF_X72_Y73_N16
\LED_AIK[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \AIKEN|Mux3~0_combout\,
	devoe => ww_devoe,
	o => \LED_AIK[0]~output_o\);

-- Location: IOOBUF_X74_Y73_N16
\LED_AIK[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \AIKEN|Mux2~0_combout\,
	devoe => ww_devoe,
	o => \LED_AIK[1]~output_o\);

-- Location: IOOBUF_X72_Y73_N23
\LED_AIK[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \AIKEN|Mux1~0_combout\,
	devoe => ww_devoe,
	o => \LED_AIK[2]~output_o\);

-- Location: IOOBUF_X74_Y73_N23
\LED_AIK[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \AIKEN|Mux0~0_combout\,
	devoe => ww_devoe,
	o => \LED_AIK[3]~output_o\);

-- Location: IOOBUF_X69_Y73_N23
\SEG0[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SEGDISP|Mux6~0_combout\,
	devoe => ww_devoe,
	o => \SEG0[0]~output_o\);

-- Location: IOOBUF_X107_Y73_N23
\SEG0[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SEGDISP|Mux5~0_combout\,
	devoe => ww_devoe,
	o => \SEG0[1]~output_o\);

-- Location: IOOBUF_X67_Y73_N23
\SEG0[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SEGDISP|Mux4~0_combout\,
	devoe => ww_devoe,
	o => \SEG0[2]~output_o\);

-- Location: IOOBUF_X115_Y50_N2
\SEG0[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SEGDISP|Mux3~0_combout\,
	devoe => ww_devoe,
	o => \SEG0[3]~output_o\);

-- Location: IOOBUF_X115_Y54_N16
\SEG0[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SEGDISP|Mux2~0_combout\,
	devoe => ww_devoe,
	o => \SEG0[4]~output_o\);

-- Location: IOOBUF_X115_Y67_N16
\SEG0[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SEGDISP|Mux1~0_combout\,
	devoe => ww_devoe,
	o => \SEG0[5]~output_o\);

-- Location: IOOBUF_X115_Y69_N2
\SEG0[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SEGDISP|ALT_INV_Mux0~0_combout\,
	devoe => ww_devoe,
	o => \SEG0[6]~output_o\);

-- Location: IOIBUF_X0_Y36_N15
\CLK~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: CLKCTRL_G4
\CLK~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLK~inputclkctrl_outclk\);

-- Location: IOIBUF_X115_Y40_N8
\nRST~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_nRST,
	o => \nRST~input_o\);

-- Location: LCCOMB_X112_Y42_N10
\LCD|DISPLAY|Selector1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Selector1~0_combout\ = ((\LCD|DISPLAY|s_state_init_next.Func~q\ & \LCD|DISPLAY|s_state_init_current.Finish~q\)) # (!\LCD|DISPLAY|s_state_init_current.Reset~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_init_current.Reset~q\,
	datac => \LCD|DISPLAY|s_state_init_next.Func~q\,
	datad => \LCD|DISPLAY|s_state_init_current.Finish~q\,
	combout => \LCD|DISPLAY|Selector1~0_combout\);

-- Location: LCCOMB_X113_Y45_N2
\LCD|Add2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~0_combout\ = \LCD|t_wait\(0) $ (VCC)
-- \LCD|Add2~1\ = CARRY(\LCD|t_wait\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(0),
	datad => VCC,
	combout => \LCD|Add2~0_combout\,
	cout => \LCD|Add2~1\);

-- Location: LCCOMB_X112_Y45_N22
\LCD|t_wait[0]~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[0]~54_combout\ = (\nRST~input_o\ & (\LCD|Add2~0_combout\ & ((!\LCD|Equal0~9_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Add2~0_combout\,
	datab => \LCD|t_wait\(0),
	datac => \LCD|Equal0~9_combout\,
	datad => \nRST~input_o\,
	combout => \LCD|t_wait[0]~54_combout\);

-- Location: FF_X113_Y45_N13
\LCD|t_wait[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \LCD|t_wait[0]~54_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(0));

-- Location: LCCOMB_X113_Y45_N4
\LCD|Add2~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~2_combout\ = (\LCD|t_wait\(1) & (\LCD|Add2~1\ & VCC)) # (!\LCD|t_wait\(1) & (!\LCD|Add2~1\))
-- \LCD|Add2~3\ = CARRY((!\LCD|t_wait\(1) & !\LCD|Add2~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(1),
	datad => VCC,
	cin => \LCD|Add2~1\,
	combout => \LCD|Add2~2_combout\,
	cout => \LCD|Add2~3\);

-- Location: LCCOMB_X113_Y45_N0
\LCD|t_wait[1]~53\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[1]~53_combout\ = (\nRST~input_o\ & (\LCD|Add2~2_combout\ & ((!\LCD|Equal0~9_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \LCD|Add2~2_combout\,
	datac => \LCD|t_wait\(1),
	datad => \LCD|Equal0~9_combout\,
	combout => \LCD|t_wait[1]~53_combout\);

-- Location: FF_X113_Y45_N1
\LCD|t_wait[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[1]~53_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(1));

-- Location: LCCOMB_X113_Y45_N6
\LCD|Add2~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~4_combout\ = (\LCD|t_wait\(2) & ((GND) # (!\LCD|Add2~3\))) # (!\LCD|t_wait\(2) & (\LCD|Add2~3\ $ (GND)))
-- \LCD|Add2~5\ = CARRY((\LCD|t_wait\(2)) # (!\LCD|Add2~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(2),
	datad => VCC,
	cin => \LCD|Add2~3\,
	combout => \LCD|Add2~4_combout\,
	cout => \LCD|Add2~5\);

-- Location: LCCOMB_X112_Y45_N0
\LCD|t_wait[2]~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[2]~52_combout\ = (\nRST~input_o\ & (\LCD|Add2~4_combout\ & ((!\LCD|Equal0~9_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Add2~4_combout\,
	datab => \LCD|t_wait\(2),
	datac => \LCD|Equal0~9_combout\,
	datad => \nRST~input_o\,
	combout => \LCD|t_wait[2]~52_combout\);

-- Location: FF_X113_Y45_N7
\LCD|t_wait[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \LCD|t_wait[2]~52_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(2));

-- Location: LCCOMB_X113_Y45_N8
\LCD|Add2~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~6_combout\ = (\LCD|t_wait\(3) & (\LCD|Add2~5\ & VCC)) # (!\LCD|t_wait\(3) & (!\LCD|Add2~5\))
-- \LCD|Add2~7\ = CARRY((!\LCD|t_wait\(3) & !\LCD|Add2~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(3),
	datad => VCC,
	cin => \LCD|Add2~5\,
	combout => \LCD|Add2~6_combout\,
	cout => \LCD|Add2~7\);

-- Location: LCCOMB_X114_Y42_N18
\LCD|ci[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|ci[0]~0_combout\ = !\LCD|ci\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \LCD|ci\(0),
	combout => \LCD|ci[0]~0_combout\);

-- Location: LCCOMB_X114_Y43_N10
\LCD|DISPLAY|s_busy~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_busy~3_combout\ = (\LCD|DISPLAY|s_state_init_next.Finish~0_combout\ & ((\LCD|DISPLAY|s_state_current.Ready~q\ & ((!\LCD|s_en~q\))) # (!\LCD|DISPLAY|s_state_current.Ready~q\ & (\LCD|DISPLAY|s_busy~q\)))) # 
-- (!\LCD|DISPLAY|s_state_init_next.Finish~0_combout\ & (((\LCD|DISPLAY|s_busy~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_init_next.Finish~0_combout\,
	datab => \LCD|DISPLAY|s_state_current.Ready~q\,
	datac => \LCD|DISPLAY|s_busy~q\,
	datad => \LCD|s_en~q\,
	combout => \LCD|DISPLAY|s_busy~3_combout\);

-- Location: FF_X114_Y43_N11
\LCD|DISPLAY|s_busy\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_busy~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_busy~q\);

-- Location: LCCOMB_X114_Y42_N0
\LCD|s_data[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_data[0]~0_combout\ = (\LCD|DISPLAY|s_busy~q\ & (\nRST~input_o\ & \LCD|Equal0~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_busy~q\,
	datab => \nRST~input_o\,
	datad => \LCD|Equal0~9_combout\,
	combout => \LCD|s_data[0]~0_combout\);

-- Location: FF_X114_Y42_N19
\LCD|ci[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|ci[0]~0_combout\,
	ena => \LCD|s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|ci\(0));

-- Location: LCCOMB_X114_Y42_N4
\LCD|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add0~0_combout\ = \LCD|ci\(1) $ (\LCD|ci\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \LCD|ci\(1),
	datad => \LCD|ci\(0),
	combout => \LCD|Add0~0_combout\);

-- Location: FF_X114_Y42_N5
\LCD|ci[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add0~0_combout\,
	ena => \LCD|s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|ci\(1));

-- Location: LCCOMB_X114_Y42_N2
\LCD|Add0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add0~1_combout\ = \LCD|ci\(2) $ (((\LCD|ci\(1) & \LCD|ci\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|ci\(1),
	datac => \LCD|ci\(2),
	datad => \LCD|ci\(0),
	combout => \LCD|Add0~1_combout\);

-- Location: FF_X114_Y42_N3
\LCD|ci[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add0~1_combout\,
	ena => \LCD|s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|ci\(2));

-- Location: LCCOMB_X114_Y42_N12
\LCD|Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add0~2_combout\ = \LCD|ci\(3) $ (((\LCD|ci\(0) & (\LCD|ci\(1) & \LCD|ci\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|ci\(0),
	datab => \LCD|ci\(1),
	datac => \LCD|ci\(3),
	datad => \LCD|ci\(2),
	combout => \LCD|Add0~2_combout\);

-- Location: FF_X114_Y42_N13
\LCD|ci[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add0~2_combout\,
	ena => \LCD|s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|ci\(3));

-- Location: LCCOMB_X114_Y42_N22
\LCD|t_wait[30]~35\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[30]~35_combout\ = (\LCD|ci\(3) & (\LCD|ci\(1) & (\LCD|DISPLAY|s_busy~q\ & !\LCD|ci\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|ci\(3),
	datab => \LCD|ci\(1),
	datac => \LCD|DISPLAY|s_busy~q\,
	datad => \LCD|ci\(0),
	combout => \LCD|t_wait[30]~35_combout\);

-- Location: LCCOMB_X114_Y44_N2
\LCD|t_wait[30]~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[30]~36_combout\ = (\nRST~input_o\ & (((\LCD|t_wait[30]~35_combout\ & \LCD|ci\(2))) # (!\LCD|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait[30]~35_combout\,
	datab => \LCD|ci\(2),
	datac => \nRST~input_o\,
	datad => \LCD|Equal0~9_combout\,
	combout => \LCD|t_wait[30]~36_combout\);

-- Location: FF_X113_Y45_N9
\LCD|t_wait[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add2~6_combout\,
	ena => \LCD|t_wait[30]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(3));

-- Location: LCCOMB_X113_Y45_N10
\LCD|Add2~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~8_combout\ = (\LCD|t_wait\(4) & ((GND) # (!\LCD|Add2~7\))) # (!\LCD|t_wait\(4) & (\LCD|Add2~7\ $ (GND)))
-- \LCD|Add2~9\ = CARRY((\LCD|t_wait\(4)) # (!\LCD|Add2~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(4),
	datad => VCC,
	cin => \LCD|Add2~7\,
	combout => \LCD|Add2~8_combout\,
	cout => \LCD|Add2~9\);

-- Location: FF_X113_Y45_N11
\LCD|t_wait[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add2~8_combout\,
	ena => \LCD|t_wait[30]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(4));

-- Location: LCCOMB_X113_Y45_N12
\LCD|Add2~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~10_combout\ = (\LCD|t_wait\(5) & (\LCD|Add2~9\ & VCC)) # (!\LCD|t_wait\(5) & (!\LCD|Add2~9\))
-- \LCD|Add2~11\ = CARRY((!\LCD|t_wait\(5) & !\LCD|Add2~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(5),
	datad => VCC,
	cin => \LCD|Add2~9\,
	combout => \LCD|Add2~10_combout\,
	cout => \LCD|Add2~11\);

-- Location: LCCOMB_X112_Y44_N22
\LCD|t_wait[5]~51\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[5]~51_combout\ = (\nRST~input_o\ & (\LCD|Add2~10_combout\ & ((!\LCD|Equal0~9_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Add2~10_combout\,
	datab => \nRST~input_o\,
	datac => \LCD|t_wait\(5),
	datad => \LCD|Equal0~9_combout\,
	combout => \LCD|t_wait[5]~51_combout\);

-- Location: FF_X112_Y44_N23
\LCD|t_wait[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[5]~51_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(5));

-- Location: LCCOMB_X113_Y45_N14
\LCD|Add2~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~12_combout\ = (\LCD|t_wait\(6) & ((GND) # (!\LCD|Add2~11\))) # (!\LCD|t_wait\(6) & (\LCD|Add2~11\ $ (GND)))
-- \LCD|Add2~13\ = CARRY((\LCD|t_wait\(6)) # (!\LCD|Add2~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(6),
	datad => VCC,
	cin => \LCD|Add2~11\,
	combout => \LCD|Add2~12_combout\,
	cout => \LCD|Add2~13\);

-- Location: FF_X113_Y45_N15
\LCD|t_wait[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add2~12_combout\,
	ena => \LCD|t_wait[30]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(6));

-- Location: LCCOMB_X113_Y45_N16
\LCD|Add2~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~14_combout\ = (\LCD|t_wait\(7) & (\LCD|Add2~13\ & VCC)) # (!\LCD|t_wait\(7) & (!\LCD|Add2~13\))
-- \LCD|Add2~15\ = CARRY((!\LCD|t_wait\(7) & !\LCD|Add2~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(7),
	datad => VCC,
	cin => \LCD|Add2~13\,
	combout => \LCD|Add2~14_combout\,
	cout => \LCD|Add2~15\);

-- Location: FF_X113_Y45_N17
\LCD|t_wait[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add2~14_combout\,
	ena => \LCD|t_wait[30]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(7));

-- Location: LCCOMB_X113_Y45_N18
\LCD|Add2~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~16_combout\ = (\LCD|t_wait\(8) & ((GND) # (!\LCD|Add2~15\))) # (!\LCD|t_wait\(8) & (\LCD|Add2~15\ $ (GND)))
-- \LCD|Add2~17\ = CARRY((\LCD|t_wait\(8)) # (!\LCD|Add2~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(8),
	datad => VCC,
	cin => \LCD|Add2~15\,
	combout => \LCD|Add2~16_combout\,
	cout => \LCD|Add2~17\);

-- Location: LCCOMB_X112_Y44_N26
\LCD|t_wait[8]~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[8]~50_combout\ = (\nRST~input_o\ & (\LCD|Add2~16_combout\ & ((!\LCD|Equal0~9_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Add2~16_combout\,
	datab => \nRST~input_o\,
	datac => \LCD|t_wait\(8),
	datad => \LCD|Equal0~9_combout\,
	combout => \LCD|t_wait[8]~50_combout\);

-- Location: FF_X112_Y44_N27
\LCD|t_wait[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[8]~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(8));

-- Location: LCCOMB_X113_Y45_N20
\LCD|Add2~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~18_combout\ = (\LCD|t_wait\(9) & (\LCD|Add2~17\ & VCC)) # (!\LCD|t_wait\(9) & (!\LCD|Add2~17\))
-- \LCD|Add2~19\ = CARRY((!\LCD|t_wait\(9) & !\LCD|Add2~17\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(9),
	datad => VCC,
	cin => \LCD|Add2~17\,
	combout => \LCD|Add2~18_combout\,
	cout => \LCD|Add2~19\);

-- Location: LCCOMB_X112_Y44_N24
\LCD|t_wait[9]~49\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[9]~49_combout\ = (\nRST~input_o\ & (\LCD|Add2~18_combout\ & ((!\LCD|Equal0~9_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Add2~18_combout\,
	datab => \nRST~input_o\,
	datac => \LCD|t_wait\(9),
	datad => \LCD|Equal0~9_combout\,
	combout => \LCD|t_wait[9]~49_combout\);

-- Location: FF_X112_Y44_N25
\LCD|t_wait[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[9]~49_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(9));

-- Location: LCCOMB_X113_Y45_N22
\LCD|Add2~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~20_combout\ = (\LCD|t_wait\(10) & ((GND) # (!\LCD|Add2~19\))) # (!\LCD|t_wait\(10) & (\LCD|Add2~19\ $ (GND)))
-- \LCD|Add2~21\ = CARRY((\LCD|t_wait\(10)) # (!\LCD|Add2~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(10),
	datad => VCC,
	cin => \LCD|Add2~19\,
	combout => \LCD|Add2~20_combout\,
	cout => \LCD|Add2~21\);

-- Location: LCCOMB_X112_Y44_N14
\LCD|t_wait[10]~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[10]~48_combout\ = (\nRST~input_o\ & (!\LCD|Equal0~9_combout\ & ((\LCD|Add2~20_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \LCD|Equal0~9_combout\,
	datac => \LCD|t_wait\(10),
	datad => \LCD|Add2~20_combout\,
	combout => \LCD|t_wait[10]~48_combout\);

-- Location: FF_X112_Y44_N15
\LCD|t_wait[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[10]~48_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(10));

-- Location: LCCOMB_X112_Y44_N12
\LCD|Equal0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~6_combout\ = (!\LCD|t_wait\(10) & (!\LCD|t_wait\(9) & (!\LCD|t_wait\(8) & !\LCD|t_wait\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(10),
	datab => \LCD|t_wait\(9),
	datac => \LCD|t_wait\(8),
	datad => \LCD|t_wait\(7),
	combout => \LCD|Equal0~6_combout\);

-- Location: LCCOMB_X113_Y45_N24
\LCD|Add2~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~22_combout\ = (\LCD|t_wait\(11) & (\LCD|Add2~21\ & VCC)) # (!\LCD|t_wait\(11) & (!\LCD|Add2~21\))
-- \LCD|Add2~23\ = CARRY((!\LCD|t_wait\(11) & !\LCD|Add2~21\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(11),
	datad => VCC,
	cin => \LCD|Add2~21\,
	combout => \LCD|Add2~22_combout\,
	cout => \LCD|Add2~23\);

-- Location: LCCOMB_X112_Y44_N10
\LCD|t_wait[11]~47\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[11]~47_combout\ = (\nRST~input_o\ & (\LCD|Add2~22_combout\ & ((!\LCD|Equal0~9_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(11)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Add2~22_combout\,
	datab => \nRST~input_o\,
	datac => \LCD|t_wait\(11),
	datad => \LCD|Equal0~9_combout\,
	combout => \LCD|t_wait[11]~47_combout\);

-- Location: FF_X112_Y44_N11
\LCD|t_wait[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[11]~47_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(11));

-- Location: LCCOMB_X113_Y45_N26
\LCD|Add2~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~24_combout\ = (\LCD|t_wait\(12) & ((GND) # (!\LCD|Add2~23\))) # (!\LCD|t_wait\(12) & (\LCD|Add2~23\ $ (GND)))
-- \LCD|Add2~25\ = CARRY((\LCD|t_wait\(12)) # (!\LCD|Add2~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(12),
	datad => VCC,
	cin => \LCD|Add2~23\,
	combout => \LCD|Add2~24_combout\,
	cout => \LCD|Add2~25\);

-- Location: FF_X113_Y45_N27
\LCD|t_wait[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add2~24_combout\,
	ena => \LCD|t_wait[30]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(12));

-- Location: LCCOMB_X113_Y45_N28
\LCD|Add2~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~26_combout\ = (\LCD|t_wait\(13) & (\LCD|Add2~25\ & VCC)) # (!\LCD|t_wait\(13) & (!\LCD|Add2~25\))
-- \LCD|Add2~27\ = CARRY((!\LCD|t_wait\(13) & !\LCD|Add2~25\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(13),
	datad => VCC,
	cin => \LCD|Add2~25\,
	combout => \LCD|Add2~26_combout\,
	cout => \LCD|Add2~27\);

-- Location: FF_X113_Y45_N29
\LCD|t_wait[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add2~26_combout\,
	ena => \LCD|t_wait[30]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(13));

-- Location: LCCOMB_X113_Y45_N30
\LCD|Add2~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~28_combout\ = (\LCD|t_wait\(14) & ((GND) # (!\LCD|Add2~27\))) # (!\LCD|t_wait\(14) & (\LCD|Add2~27\ $ (GND)))
-- \LCD|Add2~29\ = CARRY((\LCD|t_wait\(14)) # (!\LCD|Add2~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(14),
	datad => VCC,
	cin => \LCD|Add2~27\,
	combout => \LCD|Add2~28_combout\,
	cout => \LCD|Add2~29\);

-- Location: LCCOMB_X112_Y44_N0
\LCD|t_wait[14]~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[14]~46_combout\ = (\nRST~input_o\ & (\LCD|Add2~28_combout\ & ((!\LCD|Equal0~9_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(14)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Add2~28_combout\,
	datab => \nRST~input_o\,
	datac => \LCD|t_wait\(14),
	datad => \LCD|Equal0~9_combout\,
	combout => \LCD|t_wait[14]~46_combout\);

-- Location: FF_X112_Y44_N1
\LCD|t_wait[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[14]~46_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(14));

-- Location: LCCOMB_X112_Y44_N20
\LCD|Equal0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~5_combout\ = (!\LCD|t_wait\(11) & (!\LCD|t_wait\(14) & (!\LCD|t_wait\(13) & !\LCD|t_wait\(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(11),
	datab => \LCD|t_wait\(14),
	datac => \LCD|t_wait\(13),
	datad => \LCD|t_wait\(12),
	combout => \LCD|Equal0~5_combout\);

-- Location: LCCOMB_X112_Y44_N28
\LCD|Equal0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~7_combout\ = (!\LCD|t_wait\(4) & (!\LCD|t_wait\(5) & (!\LCD|t_wait\(6) & !\LCD|t_wait\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(4),
	datab => \LCD|t_wait\(5),
	datac => \LCD|t_wait\(6),
	datad => \LCD|t_wait\(3),
	combout => \LCD|Equal0~7_combout\);

-- Location: LCCOMB_X112_Y44_N30
\LCD|Equal0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~8_combout\ = (!\LCD|t_wait\(0) & (!\LCD|t_wait\(1) & (!\LCD|t_wait\(2) & \LCD|Equal0~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(0),
	datab => \LCD|t_wait\(1),
	datac => \LCD|t_wait\(2),
	datad => \LCD|Equal0~7_combout\,
	combout => \LCD|Equal0~8_combout\);

-- Location: LCCOMB_X113_Y44_N0
\LCD|Add2~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~30_combout\ = (\LCD|t_wait\(15) & (\LCD|Add2~29\ & VCC)) # (!\LCD|t_wait\(15) & (!\LCD|Add2~29\))
-- \LCD|Add2~31\ = CARRY((!\LCD|t_wait\(15) & !\LCD|Add2~29\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(15),
	datad => VCC,
	cin => \LCD|Add2~29\,
	combout => \LCD|Add2~30_combout\,
	cout => \LCD|Add2~31\);

-- Location: FF_X113_Y44_N1
\LCD|t_wait[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add2~30_combout\,
	ena => \LCD|t_wait[30]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(15));

-- Location: LCCOMB_X113_Y44_N2
\LCD|Add2~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~32_combout\ = (\LCD|t_wait\(16) & ((GND) # (!\LCD|Add2~31\))) # (!\LCD|t_wait\(16) & (\LCD|Add2~31\ $ (GND)))
-- \LCD|Add2~33\ = CARRY((\LCD|t_wait\(16)) # (!\LCD|Add2~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(16),
	datad => VCC,
	cin => \LCD|Add2~31\,
	combout => \LCD|Add2~32_combout\,
	cout => \LCD|Add2~33\);

-- Location: LCCOMB_X114_Y44_N6
\LCD|t_wait[16]~45\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[16]~45_combout\ = (\nRST~input_o\ & (!\LCD|Equal0~9_combout\ & ((\LCD|Add2~32_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(16)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \LCD|Equal0~9_combout\,
	datac => \LCD|t_wait\(16),
	datad => \LCD|Add2~32_combout\,
	combout => \LCD|t_wait[16]~45_combout\);

-- Location: FF_X114_Y44_N7
\LCD|t_wait[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[16]~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(16));

-- Location: LCCOMB_X113_Y44_N4
\LCD|Add2~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~34_combout\ = (\LCD|t_wait\(17) & (\LCD|Add2~33\ & VCC)) # (!\LCD|t_wait\(17) & (!\LCD|Add2~33\))
-- \LCD|Add2~35\ = CARRY((!\LCD|t_wait\(17) & !\LCD|Add2~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(17),
	datad => VCC,
	cin => \LCD|Add2~33\,
	combout => \LCD|Add2~34_combout\,
	cout => \LCD|Add2~35\);

-- Location: LCCOMB_X114_Y44_N16
\LCD|t_wait[17]~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[17]~44_combout\ = (\nRST~input_o\ & (\LCD|Add2~34_combout\ & ((!\LCD|Equal0~9_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(17)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \LCD|Add2~34_combout\,
	datac => \LCD|t_wait\(17),
	datad => \LCD|Equal0~9_combout\,
	combout => \LCD|t_wait[17]~44_combout\);

-- Location: FF_X114_Y44_N17
\LCD|t_wait[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[17]~44_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(17));

-- Location: LCCOMB_X113_Y44_N6
\LCD|Add2~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~36_combout\ = (\LCD|t_wait\(18) & ((GND) # (!\LCD|Add2~35\))) # (!\LCD|t_wait\(18) & (\LCD|Add2~35\ $ (GND)))
-- \LCD|Add2~37\ = CARRY((\LCD|t_wait\(18)) # (!\LCD|Add2~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(18),
	datad => VCC,
	cin => \LCD|Add2~35\,
	combout => \LCD|Add2~36_combout\,
	cout => \LCD|Add2~37\);

-- Location: FF_X114_Y44_N11
\LCD|t_wait[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \LCD|Add2~36_combout\,
	sload => VCC,
	ena => \LCD|t_wait[30]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(18));

-- Location: LCCOMB_X113_Y44_N8
\LCD|Add2~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~38_combout\ = (\LCD|t_wait\(19) & (\LCD|Add2~37\ & VCC)) # (!\LCD|t_wait\(19) & (!\LCD|Add2~37\))
-- \LCD|Add2~39\ = CARRY((!\LCD|t_wait\(19) & !\LCD|Add2~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(19),
	datad => VCC,
	cin => \LCD|Add2~37\,
	combout => \LCD|Add2~38_combout\,
	cout => \LCD|Add2~39\);

-- Location: FF_X113_Y44_N9
\LCD|t_wait[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add2~38_combout\,
	ena => \LCD|t_wait[30]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(19));

-- Location: LCCOMB_X113_Y44_N10
\LCD|Add2~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~40_combout\ = (\LCD|t_wait\(20) & ((GND) # (!\LCD|Add2~39\))) # (!\LCD|t_wait\(20) & (\LCD|Add2~39\ $ (GND)))
-- \LCD|Add2~41\ = CARRY((\LCD|t_wait\(20)) # (!\LCD|Add2~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(20),
	datad => VCC,
	cin => \LCD|Add2~39\,
	combout => \LCD|Add2~40_combout\,
	cout => \LCD|Add2~41\);

-- Location: FF_X113_Y44_N11
\LCD|t_wait[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add2~40_combout\,
	ena => \LCD|t_wait[30]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(20));

-- Location: LCCOMB_X113_Y44_N12
\LCD|Add2~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~42_combout\ = (\LCD|t_wait\(21) & (\LCD|Add2~41\ & VCC)) # (!\LCD|t_wait\(21) & (!\LCD|Add2~41\))
-- \LCD|Add2~43\ = CARRY((!\LCD|t_wait\(21) & !\LCD|Add2~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(21),
	datad => VCC,
	cin => \LCD|Add2~41\,
	combout => \LCD|Add2~42_combout\,
	cout => \LCD|Add2~43\);

-- Location: LCCOMB_X114_Y44_N14
\LCD|t_wait[21]~43\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[21]~43_combout\ = (\nRST~input_o\ & (!\LCD|Equal0~9_combout\ & ((\LCD|Add2~42_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(21)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \LCD|Equal0~9_combout\,
	datac => \LCD|t_wait\(21),
	datad => \LCD|Add2~42_combout\,
	combout => \LCD|t_wait[21]~43_combout\);

-- Location: FF_X114_Y44_N15
\LCD|t_wait[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[21]~43_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(21));

-- Location: LCCOMB_X113_Y44_N14
\LCD|Add2~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~44_combout\ = (\LCD|t_wait\(22) & ((GND) # (!\LCD|Add2~43\))) # (!\LCD|t_wait\(22) & (\LCD|Add2~43\ $ (GND)))
-- \LCD|Add2~45\ = CARRY((\LCD|t_wait\(22)) # (!\LCD|Add2~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(22),
	datad => VCC,
	cin => \LCD|Add2~43\,
	combout => \LCD|Add2~44_combout\,
	cout => \LCD|Add2~45\);

-- Location: FF_X114_Y44_N1
\LCD|t_wait[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \LCD|Add2~44_combout\,
	sload => VCC,
	ena => \LCD|t_wait[30]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(22));

-- Location: LCCOMB_X113_Y44_N16
\LCD|Add2~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~46_combout\ = (\LCD|t_wait\(23) & (\LCD|Add2~45\ & VCC)) # (!\LCD|t_wait\(23) & (!\LCD|Add2~45\))
-- \LCD|Add2~47\ = CARRY((!\LCD|t_wait\(23) & !\LCD|Add2~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(23),
	datad => VCC,
	cin => \LCD|Add2~45\,
	combout => \LCD|Add2~46_combout\,
	cout => \LCD|Add2~47\);

-- Location: FF_X113_Y44_N17
\LCD|t_wait[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add2~46_combout\,
	ena => \LCD|t_wait[30]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(23));

-- Location: LCCOMB_X113_Y44_N18
\LCD|Add2~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~48_combout\ = (\LCD|t_wait\(24) & ((GND) # (!\LCD|Add2~47\))) # (!\LCD|t_wait\(24) & (\LCD|Add2~47\ $ (GND)))
-- \LCD|Add2~49\ = CARRY((\LCD|t_wait\(24)) # (!\LCD|Add2~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(24),
	datad => VCC,
	cin => \LCD|Add2~47\,
	combout => \LCD|Add2~48_combout\,
	cout => \LCD|Add2~49\);

-- Location: LCCOMB_X114_Y44_N28
\LCD|t_wait[24]~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[24]~42_combout\ = (\nRST~input_o\ & (!\LCD|Equal0~9_combout\ & ((\LCD|Add2~48_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(24)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \LCD|Equal0~9_combout\,
	datac => \LCD|t_wait\(24),
	datad => \LCD|Add2~48_combout\,
	combout => \LCD|t_wait[24]~42_combout\);

-- Location: FF_X114_Y44_N29
\LCD|t_wait[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[24]~42_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(24));

-- Location: LCCOMB_X113_Y44_N20
\LCD|Add2~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~50_combout\ = (\LCD|t_wait\(25) & (\LCD|Add2~49\ & VCC)) # (!\LCD|t_wait\(25) & (!\LCD|Add2~49\))
-- \LCD|Add2~51\ = CARRY((!\LCD|t_wait\(25) & !\LCD|Add2~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(25),
	datad => VCC,
	cin => \LCD|Add2~49\,
	combout => \LCD|Add2~50_combout\,
	cout => \LCD|Add2~51\);

-- Location: FF_X113_Y44_N21
\LCD|t_wait[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Add2~50_combout\,
	ena => \LCD|t_wait[30]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(25));

-- Location: LCCOMB_X113_Y44_N22
\LCD|Add2~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~52_combout\ = (\LCD|t_wait\(26) & ((GND) # (!\LCD|Add2~51\))) # (!\LCD|t_wait\(26) & (\LCD|Add2~51\ $ (GND)))
-- \LCD|Add2~53\ = CARRY((\LCD|t_wait\(26)) # (!\LCD|Add2~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(26),
	datad => VCC,
	cin => \LCD|Add2~51\,
	combout => \LCD|Add2~52_combout\,
	cout => \LCD|Add2~53\);

-- Location: LCCOMB_X114_Y44_N26
\LCD|t_wait[26]~41\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[26]~41_combout\ = (\nRST~input_o\ & (!\LCD|Equal0~9_combout\ & ((\LCD|Add2~52_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(26)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \LCD|Equal0~9_combout\,
	datac => \LCD|t_wait\(26),
	datad => \LCD|Add2~52_combout\,
	combout => \LCD|t_wait[26]~41_combout\);

-- Location: FF_X114_Y44_N27
\LCD|t_wait[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[26]~41_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(26));

-- Location: LCCOMB_X114_Y44_N30
\LCD|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~1_combout\ = (!\LCD|t_wait\(26) & (!\LCD|t_wait\(24) & (!\LCD|t_wait\(23) & !\LCD|t_wait\(25))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(26),
	datab => \LCD|t_wait\(24),
	datac => \LCD|t_wait\(23),
	datad => \LCD|t_wait\(25),
	combout => \LCD|Equal0~1_combout\);

-- Location: LCCOMB_X114_Y44_N20
\LCD|Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~2_combout\ = (!\LCD|t_wait\(20) & (!\LCD|t_wait\(22) & (!\LCD|t_wait\(21) & !\LCD|t_wait\(19))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(20),
	datab => \LCD|t_wait\(22),
	datac => \LCD|t_wait\(21),
	datad => \LCD|t_wait\(19),
	combout => \LCD|Equal0~2_combout\);

-- Location: LCCOMB_X113_Y44_N24
\LCD|Add2~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~54_combout\ = (\LCD|t_wait\(27) & (\LCD|Add2~53\ & VCC)) # (!\LCD|t_wait\(27) & (!\LCD|Add2~53\))
-- \LCD|Add2~55\ = CARRY((!\LCD|t_wait\(27) & !\LCD|Add2~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(27),
	datad => VCC,
	cin => \LCD|Add2~53\,
	combout => \LCD|Add2~54_combout\,
	cout => \LCD|Add2~55\);

-- Location: LCCOMB_X114_Y44_N18
\LCD|t_wait[27]~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[27]~40_combout\ = (\nRST~input_o\ & (!\LCD|Equal0~9_combout\ & ((\LCD|Add2~54_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(27)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \LCD|Equal0~9_combout\,
	datac => \LCD|t_wait\(27),
	datad => \LCD|Add2~54_combout\,
	combout => \LCD|t_wait[27]~40_combout\);

-- Location: FF_X114_Y44_N19
\LCD|t_wait[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[27]~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(27));

-- Location: LCCOMB_X113_Y44_N26
\LCD|Add2~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~56_combout\ = (\LCD|t_wait\(28) & ((GND) # (!\LCD|Add2~55\))) # (!\LCD|t_wait\(28) & (\LCD|Add2~55\ $ (GND)))
-- \LCD|Add2~57\ = CARRY((\LCD|t_wait\(28)) # (!\LCD|Add2~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(28),
	datad => VCC,
	cin => \LCD|Add2~55\,
	combout => \LCD|Add2~56_combout\,
	cout => \LCD|Add2~57\);

-- Location: LCCOMB_X114_Y44_N12
\LCD|t_wait[28]~39\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[28]~39_combout\ = (\nRST~input_o\ & (!\LCD|Equal0~9_combout\ & ((\LCD|Add2~56_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(28)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \LCD|Equal0~9_combout\,
	datac => \LCD|t_wait\(28),
	datad => \LCD|Add2~56_combout\,
	combout => \LCD|t_wait[28]~39_combout\);

-- Location: FF_X114_Y44_N13
\LCD|t_wait[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[28]~39_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(28));

-- Location: LCCOMB_X113_Y44_N28
\LCD|Add2~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~58_combout\ = (\LCD|t_wait\(29) & (\LCD|Add2~57\ & VCC)) # (!\LCD|t_wait\(29) & (!\LCD|Add2~57\))
-- \LCD|Add2~59\ = CARRY((!\LCD|t_wait\(29) & !\LCD|Add2~57\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(29),
	datad => VCC,
	cin => \LCD|Add2~57\,
	combout => \LCD|Add2~58_combout\,
	cout => \LCD|Add2~59\);

-- Location: LCCOMB_X114_Y44_N22
\LCD|t_wait[29]~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[29]~38_combout\ = (\nRST~input_o\ & (!\LCD|Equal0~9_combout\ & ((\LCD|Add2~58_combout\)))) # (!\nRST~input_o\ & (((\LCD|t_wait\(29)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \LCD|Equal0~9_combout\,
	datac => \LCD|t_wait\(29),
	datad => \LCD|Add2~58_combout\,
	combout => \LCD|t_wait[29]~38_combout\);

-- Location: FF_X114_Y44_N23
\LCD|t_wait[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[29]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(29));

-- Location: LCCOMB_X114_Y44_N0
\LCD|t_wait[30]~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[30]~34_combout\ = (\LCD|t_wait\(30) & !\nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(30),
	datad => \nRST~input_o\,
	combout => \LCD|t_wait[30]~34_combout\);

-- Location: LCCOMB_X113_Y44_N30
\LCD|Add2~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Add2~60_combout\ = \LCD|t_wait\(30) $ (\LCD|Add2~59\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|t_wait\(30),
	cin => \LCD|Add2~59\,
	combout => \LCD|Add2~60_combout\);

-- Location: LCCOMB_X114_Y44_N8
\LCD|t_wait[30]~37\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|t_wait[30]~37_combout\ = (\LCD|t_wait[30]~34_combout\) # ((\LCD|t_wait[30]~36_combout\ & (!\LCD|Equal0~9_combout\ & \LCD|Add2~60_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait[30]~36_combout\,
	datab => \LCD|Equal0~9_combout\,
	datac => \LCD|t_wait[30]~34_combout\,
	datad => \LCD|Add2~60_combout\,
	combout => \LCD|t_wait[30]~37_combout\);

-- Location: FF_X114_Y44_N9
\LCD|t_wait[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|t_wait[30]~37_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|t_wait\(30));

-- Location: LCCOMB_X114_Y44_N4
\LCD|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~0_combout\ = (!\LCD|t_wait\(29) & (!\LCD|t_wait\(27) & (!\LCD|t_wait\(30) & !\LCD|t_wait\(28))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(29),
	datab => \LCD|t_wait\(27),
	datac => \LCD|t_wait\(30),
	datad => \LCD|t_wait\(28),
	combout => \LCD|Equal0~0_combout\);

-- Location: LCCOMB_X114_Y44_N10
\LCD|Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~3_combout\ = (!\LCD|t_wait\(16) & (!\LCD|t_wait\(17) & (!\LCD|t_wait\(18) & !\LCD|t_wait\(15))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|t_wait\(16),
	datab => \LCD|t_wait\(17),
	datac => \LCD|t_wait\(18),
	datad => \LCD|t_wait\(15),
	combout => \LCD|Equal0~3_combout\);

-- Location: LCCOMB_X114_Y44_N24
\LCD|Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~4_combout\ = (\LCD|Equal0~1_combout\ & (\LCD|Equal0~2_combout\ & (\LCD|Equal0~0_combout\ & \LCD|Equal0~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Equal0~1_combout\,
	datab => \LCD|Equal0~2_combout\,
	datac => \LCD|Equal0~0_combout\,
	datad => \LCD|Equal0~3_combout\,
	combout => \LCD|Equal0~4_combout\);

-- Location: LCCOMB_X112_Y44_N16
\LCD|Equal0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Equal0~9_combout\ = (\LCD|Equal0~6_combout\ & (\LCD|Equal0~5_combout\ & (\LCD|Equal0~8_combout\ & \LCD|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Equal0~6_combout\,
	datab => \LCD|Equal0~5_combout\,
	datac => \LCD|Equal0~8_combout\,
	datad => \LCD|Equal0~4_combout\,
	combout => \LCD|Equal0~9_combout\);

-- Location: LCCOMB_X114_Y43_N20
\LCD|s_en~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_en~0_combout\ = (\LCD|Equal0~9_combout\ & ((\nRST~input_o\ & (\LCD|DISPLAY|s_busy~q\)) # (!\nRST~input_o\ & ((\LCD|s_en~q\))))) # (!\LCD|Equal0~9_combout\ & (((\LCD|s_en~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|Equal0~9_combout\,
	datab => \LCD|DISPLAY|s_busy~q\,
	datac => \LCD|s_en~q\,
	datad => \nRST~input_o\,
	combout => \LCD|s_en~0_combout\);

-- Location: FF_X114_Y43_N21
\LCD|s_en\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|s_en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_en~q\);

-- Location: LCCOMB_X114_Y43_N16
\LCD|DISPLAY|s_state_next.Ready~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_next.Ready~0_combout\ = (\LCD|DISPLAY|s_state_current.Ready~q\ & (!\LCD|s_en~q\)) # (!\LCD|DISPLAY|s_state_current.Ready~q\ & (((\LCD|DISPLAY|s_state_current.Init~q\) # (\LCD|DISPLAY|s_state_current.Send~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_current.Ready~q\,
	datab => \LCD|s_en~q\,
	datac => \LCD|DISPLAY|s_state_current.Init~q\,
	datad => \LCD|DISPLAY|s_state_current.Send~q\,
	combout => \LCD|DISPLAY|s_state_next.Ready~0_combout\);

-- Location: LCCOMB_X114_Y43_N6
\LCD|DISPLAY|s_state_send_next~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_send_next~0_combout\ = (\LCD|DISPLAY|s_state_init_next.Finish~0_combout\ & ((\LCD|DISPLAY|s_state_current.Send~q\ & ((!\LCD|DISPLAY|s_state_send_current~q\))) # (!\LCD|DISPLAY|s_state_current.Send~q\ & 
-- (\LCD|DISPLAY|s_state_send_next~q\)))) # (!\LCD|DISPLAY|s_state_init_next.Finish~0_combout\ & (((\LCD|DISPLAY|s_state_send_next~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_init_next.Finish~0_combout\,
	datab => \LCD|DISPLAY|s_state_current.Send~q\,
	datac => \LCD|DISPLAY|s_state_send_next~q\,
	datad => \LCD|DISPLAY|s_state_send_current~q\,
	combout => \LCD|DISPLAY|s_state_send_next~0_combout\);

-- Location: FF_X114_Y43_N7
\LCD|DISPLAY|s_state_send_next\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_state_send_next~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_send_next~q\);

-- Location: LCCOMB_X114_Y41_N28
\LCD|DISPLAY|s_state_send_current~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_send_current~feeder_combout\ = \LCD|DISPLAY|s_state_send_next~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|s_state_send_next~q\,
	combout => \LCD|DISPLAY|s_state_send_current~feeder_combout\);

-- Location: FF_X114_Y41_N29
\LCD|DISPLAY|s_state_send_current\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_state_send_current~feeder_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_send_current~q\);

-- Location: LCCOMB_X110_Y40_N2
\LCD|DISPLAY|Equal0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Equal0~10_combout\ = (\LCD|DISPLAY|Equal0~4_combout\ & \LCD|DISPLAY|Equal0~9_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \LCD|DISPLAY|Equal0~4_combout\,
	datad => \LCD|DISPLAY|Equal0~9_combout\,
	combout => \LCD|DISPLAY|Equal0~10_combout\);

-- Location: LCCOMB_X113_Y41_N28
\LCD|DISPLAY|rs~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|rs~4_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & ((\LCD|DISPLAY|s_state_current.Send~q\ & ((\LCD|DISPLAY|s_state_send_current~q\))) # (!\LCD|DISPLAY|s_state_current.Send~q\ & (\LCD|DISPLAY|s_state_current.Init~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_current.Init~q\,
	datab => \LCD|DISPLAY|s_state_send_current~q\,
	datac => \LCD|DISPLAY|s_state_current.Send~q\,
	datad => \LCD|DISPLAY|Equal0~10_combout\,
	combout => \LCD|DISPLAY|rs~4_combout\);

-- Location: LCCOMB_X113_Y41_N6
\LCD|DISPLAY|rs~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|rs~2_combout\ = (\LCD|DISPLAY|rs~4_combout\ & ((\LCD|DISPLAY|s_state_init_current.Finish~q\) # ((\LCD|DISPLAY|s_state_init_current.Reset~q\ & \LCD|DISPLAY|P_UEBERGANG:rs~q\)))) # (!\LCD|DISPLAY|rs~4_combout\ & 
-- (((\LCD|DISPLAY|P_UEBERGANG:rs~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_init_current.Reset~q\,
	datab => \LCD|DISPLAY|s_state_init_current.Finish~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:rs~q\,
	datad => \LCD|DISPLAY|rs~4_combout\,
	combout => \LCD|DISPLAY|rs~2_combout\);

-- Location: LCCOMB_X113_Y41_N24
\LCD|DISPLAY|rs~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|rs~3_combout\ = (\LCD|DISPLAY|s_state_current.Send~q\ & ((\LCD|DISPLAY|rs~4_combout\ & (!\LCD|DISPLAY|P_UEBERGANG:c_pos_y~0_combout\)) # (!\LCD|DISPLAY|rs~4_combout\ & ((\LCD|DISPLAY|rs~2_combout\))))) # (!\LCD|DISPLAY|s_state_current.Send~q\ 
-- & (((\LCD|DISPLAY|rs~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~0_combout\,
	datab => \LCD|DISPLAY|s_state_current.Send~q\,
	datac => \LCD|DISPLAY|rs~4_combout\,
	datad => \LCD|DISPLAY|rs~2_combout\,
	combout => \LCD|DISPLAY|rs~3_combout\);

-- Location: LCCOMB_X113_Y41_N30
\LCD|DISPLAY|P_UEBERGANG:rs~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:rs~feeder_combout\ = \LCD|DISPLAY|rs~3_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|rs~3_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:rs~feeder_combout\);

-- Location: FF_X113_Y41_N31
\LCD|DISPLAY|P_UEBERGANG:rs\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:rs~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:rs~q\);

-- Location: LCCOMB_X114_Y41_N10
\LCD|DISPLAY|P_UEBERGANG:c_pos_y~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:c_pos_y~1_combout\ = (\LCD|DISPLAY|s_state_send_current~q\ & \LCD|DISPLAY|s_state_current.Send~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|s_state_send_current~q\,
	datad => \LCD|DISPLAY|s_state_current.Send~q\,
	combout => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~1_combout\);

-- Location: LCCOMB_X113_Y41_N20
\LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~0_combout\ = \LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~q\ $ (((\LCD|DISPLAY|P_UEBERGANG:rs~q\ & (\LCD|DISPLAY|P_UEBERGANG:c_pos_y~1_combout\ & \LCD|DISPLAY|Equal0~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:rs~q\,
	datab => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~1_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~q\,
	datad => \LCD|DISPLAY|Equal0~10_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~0_combout\);

-- Location: FF_X113_Y41_N21
\LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~q\);

-- Location: LCCOMB_X114_Y41_N16
\LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\ = (\LCD|DISPLAY|P_UEBERGANG:c_pos_y~1_combout\ & (\LCD|DISPLAY|Equal0~9_combout\ & (\LCD|DISPLAY|P_UEBERGANG:rs~q\ & \LCD|DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~1_combout\,
	datab => \LCD|DISPLAY|Equal0~9_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:rs~q\,
	datad => \LCD|DISPLAY|Equal0~4_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\);

-- Location: LCCOMB_X113_Y41_N10
\LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]~0_combout\ = \LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]~q\ $ (((\LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~q\ & \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]~0_combout\);

-- Location: FF_X113_Y41_N11
\LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]~q\);

-- Location: LCCOMB_X113_Y41_N8
\LCD|DISPLAY|P_UEBERGANG:c_pos_x[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:c_pos_x[2]~0_combout\ = \LCD|DISPLAY|P_UEBERGANG:c_pos_x[2]~q\ $ (((\LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]~q\ & (\LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~q\ & \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]~q\,
	datab => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[2]~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[2]~0_combout\);

-- Location: FF_X113_Y41_N9
\LCD|DISPLAY|P_UEBERGANG:c_pos_x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[2]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[2]~q\);

-- Location: LCCOMB_X113_Y41_N18
\LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\ = (\LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]~q\ & (\LCD|DISPLAY|P_UEBERGANG:c_pos_x[2]~q\ & \LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[1]~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[2]~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[0]~q\,
	combout => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\);

-- Location: LCCOMB_X114_Y41_N30
\LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~2_combout\ = \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~q\ $ (((\LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\ & \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~2_combout\);

-- Location: FF_X114_Y41_N31
\LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~2_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~q\);

-- Location: LCCOMB_X113_Y41_N0
\LCD|DISPLAY|P_UEBERGANG:c_pos_y~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:c_pos_y~0_combout\ = (\LCD|DISPLAY|P_UEBERGANG:rs~q\ & (\LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~q\ & \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:rs~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~0_combout\);

-- Location: LCCOMB_X114_Y43_N14
\LCD|DISPLAY|s_state_next.Send~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_next.Send~0_combout\ = (\LCD|DISPLAY|s_state_current.Send~q\ & ((\LCD|DISPLAY|P_UEBERGANG:c_pos_y~0_combout\) # (!\LCD|DISPLAY|s_state_send_current~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|s_state_send_current~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~0_combout\,
	datad => \LCD|DISPLAY|s_state_current.Send~q\,
	combout => \LCD|DISPLAY|s_state_next.Send~0_combout\);

-- Location: LCCOMB_X114_Y43_N12
\LCD|DISPLAY|s_state_next.Init~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_next.Init~0_combout\ = ((!\LCD|DISPLAY|s_state_init_current.Finish~q\ & \LCD|DISPLAY|s_state_current.Init~q\)) # (!\LCD|DISPLAY|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111101001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_init_current.Finish~q\,
	datab => \LCD|DISPLAY|s_state_current.Init~q\,
	datac => \LCD|DISPLAY|Equal0~4_combout\,
	combout => \LCD|DISPLAY|s_state_next.Init~0_combout\);

-- Location: LCCOMB_X114_Y43_N8
\LCD|DISPLAY|s_state_next.Init~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_next.Init~1_combout\ = (\LCD|DISPLAY|Equal0~9_combout\ & (\nRST~input_o\ & (!\LCD|DISPLAY|s_state_next.Send~0_combout\ & !\LCD|DISPLAY|s_state_next.Init~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => \LCD|DISPLAY|s_state_next.Send~0_combout\,
	datad => \LCD|DISPLAY|s_state_next.Init~0_combout\,
	combout => \LCD|DISPLAY|s_state_next.Init~1_combout\);

-- Location: FF_X114_Y43_N17
\LCD|DISPLAY|s_state_next.Ready\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_state_next.Ready~0_combout\,
	ena => \LCD|DISPLAY|s_state_next.Init~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_next.Ready~q\);

-- Location: LCCOMB_X114_Y43_N22
\LCD|DISPLAY|s_state_current.Ready~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_current.Ready~feeder_combout\ = \LCD|DISPLAY|s_state_next.Ready~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|s_state_next.Ready~q\,
	combout => \LCD|DISPLAY|s_state_current.Ready~feeder_combout\);

-- Location: FF_X114_Y43_N23
\LCD|DISPLAY|s_state_current.Ready\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_state_current.Ready~feeder_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_current.Ready~q\);

-- Location: LCCOMB_X114_Y43_N18
\LCD|DISPLAY|s_busy~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_busy~2_combout\ = (\LCD|DISPLAY|s_state_current.Ready~q\ & \LCD|s_en~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_current.Ready~q\,
	datad => \LCD|s_en~q\,
	combout => \LCD|DISPLAY|s_busy~2_combout\);

-- Location: FF_X114_Y43_N31
\LCD|DISPLAY|s_state_next.Send\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \LCD|DISPLAY|s_busy~2_combout\,
	sload => VCC,
	ena => \LCD|DISPLAY|s_state_next.Init~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_next.Send~q\);

-- Location: LCCOMB_X114_Y41_N0
\LCD|DISPLAY|s_state_current.Send~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_current.Send~feeder_combout\ = \LCD|DISPLAY|s_state_next.Send~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|s_state_next.Send~q\,
	combout => \LCD|DISPLAY|s_state_current.Send~feeder_combout\);

-- Location: FF_X114_Y41_N1
\LCD|DISPLAY|s_state_current.Send\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_state_current.Send~feeder_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_current.Send~q\);

-- Location: LCCOMB_X113_Y42_N24
\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~0_combout\ = (!\LCD|DISPLAY|s_state_current.Ready~q\ & ((!\LCD|DISPLAY|s_state_current.Init~q\) # (!\LCD|DISPLAY|s_state_init_current.Finish~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|s_state_init_current.Finish~q\,
	datac => \LCD|DISPLAY|s_state_current.Init~q\,
	datad => \LCD|DISPLAY|s_state_current.Ready~q\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~0_combout\);

-- Location: LCCOMB_X113_Y42_N14
\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~0_combout\ & (((\LCD|DISPLAY|P_UEBERGANG:c_pos_y~0_combout\) # (!\LCD|DISPLAY|s_state_send_current~q\)) # (!\LCD|DISPLAY|s_state_current.Send~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_current.Send~q\,
	datab => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~0_combout\,
	datac => \LCD|DISPLAY|s_state_send_current~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~0_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\);

-- Location: LCCOMB_X110_Y42_N0
\LCD|DISPLAY|Add1~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~24_combout\ = \LCD|DISPLAY|P_UEBERGANG:t_wait[0]~q\ $ (VCC)
-- \LCD|DISPLAY|Add1~25\ = CARRY(\LCD|DISPLAY|P_UEBERGANG:t_wait[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[0]~q\,
	datad => VCC,
	combout => \LCD|DISPLAY|Add1~24_combout\,
	cout => \LCD|DISPLAY|Add1~25\);

-- Location: LCCOMB_X112_Y42_N24
\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\ = (!\LCD|DISPLAY|s_state_current.Send~q\ & !\LCD|DISPLAY|s_state_current.Init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|s_state_current.Send~q\,
	datac => \LCD|DISPLAY|s_state_current.Init~q\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\);

-- Location: LCCOMB_X111_Y42_N4
\LCD|DISPLAY|P_UEBERGANG:t_wait[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[0]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (((\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\ & \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\)))) # (!\LCD|DISPLAY|Equal0~10_combout\ & 
-- (\LCD|DISPLAY|Add1~24_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Add1~24_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datad => \LCD|DISPLAY|Equal0~10_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[0]~0_combout\);

-- Location: FF_X111_Y42_N5
\LCD|DISPLAY|P_UEBERGANG:t_wait[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[0]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[0]~q\);

-- Location: LCCOMB_X110_Y42_N2
\LCD|DISPLAY|Add1~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~26_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[1]~q\ & (\LCD|DISPLAY|Add1~25\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[1]~q\ & (!\LCD|DISPLAY|Add1~25\))
-- \LCD|DISPLAY|Add1~27\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[1]~q\ & !\LCD|DISPLAY|Add1~25\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[1]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~25\,
	combout => \LCD|DISPLAY|Add1~26_combout\,
	cout => \LCD|DISPLAY|Add1~27\);

-- Location: LCCOMB_X111_Y42_N6
\LCD|DISPLAY|P_UEBERGANG:t_wait[1]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[1]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (((\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\ & \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\)))) # (!\LCD|DISPLAY|Equal0~10_combout\ & 
-- (\LCD|DISPLAY|Add1~26_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Add1~26_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datad => \LCD|DISPLAY|Equal0~10_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[1]~0_combout\);

-- Location: FF_X111_Y42_N7
\LCD|DISPLAY|P_UEBERGANG:t_wait[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[1]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[1]~q\);

-- Location: LCCOMB_X110_Y42_N4
\LCD|DISPLAY|Add1~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~28_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[2]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~27\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[2]~q\ & (\LCD|DISPLAY|Add1~27\ $ (GND)))
-- \LCD|DISPLAY|Add1~29\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[2]~q\) # (!\LCD|DISPLAY|Add1~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[2]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~27\,
	combout => \LCD|DISPLAY|Add1~28_combout\,
	cout => \LCD|DISPLAY|Add1~29\);

-- Location: LCCOMB_X111_Y41_N0
\LCD|DISPLAY|P_UEBERGANG:t_wait[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[2]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (\LCD|DISPLAY|t_wait~0_combout\ & ((\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\)))) # (!\LCD|DISPLAY|Equal0~10_combout\ & (((\LCD|DISPLAY|Add1~28_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|t_wait~0_combout\,
	datab => \LCD|DISPLAY|Add1~28_combout\,
	datac => \LCD|DISPLAY|Equal0~10_combout\,
	datad => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[2]~0_combout\);

-- Location: FF_X111_Y41_N1
\LCD|DISPLAY|P_UEBERGANG:t_wait[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[2]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[2]~q\);

-- Location: LCCOMB_X110_Y42_N6
\LCD|DISPLAY|Add1~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~30_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[3]~q\ & (\LCD|DISPLAY|Add1~29\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[3]~q\ & (!\LCD|DISPLAY|Add1~29\))
-- \LCD|DISPLAY|Add1~31\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[3]~q\ & !\LCD|DISPLAY|Add1~29\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[3]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~29\,
	combout => \LCD|DISPLAY|Add1~30_combout\,
	cout => \LCD|DISPLAY|Add1~31\);

-- Location: LCCOMB_X111_Y41_N14
\LCD|DISPLAY|Add1~91\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~91_combout\ = (\LCD|DISPLAY|Add1~30_combout\ & ((!\LCD|DISPLAY|Equal0~4_combout\) # (!\LCD|DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|Equal0~9_combout\,
	datac => \LCD|DISPLAY|Add1~30_combout\,
	datad => \LCD|DISPLAY|Equal0~4_combout\,
	combout => \LCD|DISPLAY|Add1~91_combout\);

-- Location: FF_X111_Y41_N15
\LCD|DISPLAY|P_UEBERGANG:t_wait[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~91_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[3]~q\);

-- Location: LCCOMB_X110_Y42_N8
\LCD|DISPLAY|Add1~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~32_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[4]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~31\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[4]~q\ & (\LCD|DISPLAY|Add1~31\ $ (GND)))
-- \LCD|DISPLAY|Add1~33\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[4]~q\) # (!\LCD|DISPLAY|Add1~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[4]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~31\,
	combout => \LCD|DISPLAY|Add1~32_combout\,
	cout => \LCD|DISPLAY|Add1~33\);

-- Location: LCCOMB_X111_Y42_N26
\LCD|DISPLAY|P_UEBERGANG:t_wait[4]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[4]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (!\LCD|DISPLAY|t_wait~0_combout\)) # (!\LCD|DISPLAY|Equal0~10_combout\ & ((\LCD|DISPLAY|Add1~32_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|t_wait~0_combout\,
	datab => \LCD|DISPLAY|Equal0~10_combout\,
	datad => \LCD|DISPLAY|Add1~32_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[4]~0_combout\);

-- Location: LCCOMB_X112_Y42_N8
\LCD|DISPLAY|P_UEBERGANG:t_wait[17]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~4_combout\ = (\LCD|DISPLAY|Equal0~9_combout\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\ & \LCD|DISPLAY|Equal0~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~9_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datad => \LCD|DISPLAY|Equal0~4_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~4_combout\);

-- Location: FF_X111_Y42_N27
\LCD|DISPLAY|P_UEBERGANG:t_wait[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[4]~0_combout\,
	asdata => \LCD|DISPLAY|P_UEBERGANG:t_wait[4]~q\,
	clrn => \nRST~input_o\,
	sload => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[4]~q\);

-- Location: LCCOMB_X110_Y42_N10
\LCD|DISPLAY|Add1~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~34_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[5]~q\ & (\LCD|DISPLAY|Add1~33\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[5]~q\ & (!\LCD|DISPLAY|Add1~33\))
-- \LCD|DISPLAY|Add1~35\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[5]~q\ & !\LCD|DISPLAY|Add1~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[5]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~33\,
	combout => \LCD|DISPLAY|Add1~34_combout\,
	cout => \LCD|DISPLAY|Add1~35\);

-- Location: LCCOMB_X111_Y41_N12
\LCD|DISPLAY|Add1~90\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~90_combout\ = (\LCD|DISPLAY|Add1~34_combout\ & ((!\LCD|DISPLAY|Equal0~4_combout\) # (!\LCD|DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|Equal0~9_combout\,
	datac => \LCD|DISPLAY|Add1~34_combout\,
	datad => \LCD|DISPLAY|Equal0~4_combout\,
	combout => \LCD|DISPLAY|Add1~90_combout\);

-- Location: FF_X111_Y41_N13
\LCD|DISPLAY|P_UEBERGANG:t_wait[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~90_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[5]~q\);

-- Location: LCCOMB_X110_Y42_N12
\LCD|DISPLAY|Add1~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~36_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[6]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~35\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[6]~q\ & (\LCD|DISPLAY|Add1~35\ $ (GND)))
-- \LCD|DISPLAY|Add1~37\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[6]~q\) # (!\LCD|DISPLAY|Add1~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[6]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~35\,
	combout => \LCD|DISPLAY|Add1~36_combout\,
	cout => \LCD|DISPLAY|Add1~37\);

-- Location: LCCOMB_X111_Y41_N18
\LCD|DISPLAY|P_UEBERGANG:t_wait[6]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[6]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (\LCD|DISPLAY|t_wait~0_combout\ & (\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\))) # (!\LCD|DISPLAY|Equal0~10_combout\ & (((\LCD|DISPLAY|Add1~36_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|t_wait~0_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datac => \LCD|DISPLAY|Equal0~10_combout\,
	datad => \LCD|DISPLAY|Add1~36_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[6]~0_combout\);

-- Location: FF_X111_Y41_N19
\LCD|DISPLAY|P_UEBERGANG:t_wait[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[6]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[6]~q\);

-- Location: LCCOMB_X110_Y42_N14
\LCD|DISPLAY|Add1~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~38_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[7]~q\ & (\LCD|DISPLAY|Add1~37\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[7]~q\ & (!\LCD|DISPLAY|Add1~37\))
-- \LCD|DISPLAY|Add1~39\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[7]~q\ & !\LCD|DISPLAY|Add1~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[7]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~37\,
	combout => \LCD|DISPLAY|Add1~38_combout\,
	cout => \LCD|DISPLAY|Add1~39\);

-- Location: LCCOMB_X111_Y42_N14
\LCD|DISPLAY|P_UEBERGANG:t_wait[7]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[7]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (((!\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\ & \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\)))) # (!\LCD|DISPLAY|Equal0~10_combout\ & 
-- (\LCD|DISPLAY|Add1~38_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Add1~38_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datad => \LCD|DISPLAY|Equal0~10_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[7]~0_combout\);

-- Location: FF_X111_Y42_N15
\LCD|DISPLAY|P_UEBERGANG:t_wait[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[7]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[7]~q\);

-- Location: LCCOMB_X110_Y42_N16
\LCD|DISPLAY|Add1~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~40_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[8]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~39\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[8]~q\ & (\LCD|DISPLAY|Add1~39\ $ (GND)))
-- \LCD|DISPLAY|Add1~41\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[8]~q\) # (!\LCD|DISPLAY|Add1~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[8]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~39\,
	combout => \LCD|DISPLAY|Add1~40_combout\,
	cout => \LCD|DISPLAY|Add1~41\);

-- Location: LCCOMB_X111_Y42_N28
\LCD|DISPLAY|P_UEBERGANG:t_wait[8]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[8]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (\LCD|DISPLAY|t_wait~0_combout\ & (\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\))) # (!\LCD|DISPLAY|Equal0~10_combout\ & (((\LCD|DISPLAY|Add1~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|t_wait~0_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datac => \LCD|DISPLAY|Add1~40_combout\,
	datad => \LCD|DISPLAY|Equal0~10_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[8]~0_combout\);

-- Location: FF_X111_Y42_N29
\LCD|DISPLAY|P_UEBERGANG:t_wait[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[8]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[8]~q\);

-- Location: LCCOMB_X110_Y42_N18
\LCD|DISPLAY|Add1~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~42_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[9]~q\ & (\LCD|DISPLAY|Add1~41\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[9]~q\ & (!\LCD|DISPLAY|Add1~41\))
-- \LCD|DISPLAY|Add1~43\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[9]~q\ & !\LCD|DISPLAY|Add1~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[9]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~41\,
	combout => \LCD|DISPLAY|Add1~42_combout\,
	cout => \LCD|DISPLAY|Add1~43\);

-- Location: LCCOMB_X111_Y42_N18
\LCD|DISPLAY|P_UEBERGANG:t_wait[9]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[9]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (((\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\ & \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\)))) # (!\LCD|DISPLAY|Equal0~10_combout\ & 
-- (\LCD|DISPLAY|Add1~42_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Add1~42_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datad => \LCD|DISPLAY|Equal0~10_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[9]~0_combout\);

-- Location: FF_X111_Y42_N19
\LCD|DISPLAY|P_UEBERGANG:t_wait[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[9]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[9]~q\);

-- Location: LCCOMB_X110_Y42_N20
\LCD|DISPLAY|Add1~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~44_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[10]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~43\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[10]~q\ & (\LCD|DISPLAY|Add1~43\ $ (GND)))
-- \LCD|DISPLAY|Add1~45\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[10]~q\) # (!\LCD|DISPLAY|Add1~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[10]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~43\,
	combout => \LCD|DISPLAY|Add1~44_combout\,
	cout => \LCD|DISPLAY|Add1~45\);

-- Location: LCCOMB_X111_Y42_N0
\LCD|DISPLAY|P_UEBERGANG:t_wait[10]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[10]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (((\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\ & \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\)))) # (!\LCD|DISPLAY|Equal0~10_combout\ & 
-- (\LCD|DISPLAY|Add1~44_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Add1~44_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datad => \LCD|DISPLAY|Equal0~10_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[10]~0_combout\);

-- Location: FF_X111_Y42_N1
\LCD|DISPLAY|P_UEBERGANG:t_wait[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[10]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[10]~q\);

-- Location: LCCOMB_X111_Y42_N12
\LCD|DISPLAY|Equal0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Equal0~5_combout\ = (!\LCD|DISPLAY|P_UEBERGANG:t_wait[10]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[8]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[7]~q\ & !\LCD|DISPLAY|P_UEBERGANG:t_wait[9]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[10]~q\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[8]~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[7]~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:t_wait[9]~q\,
	combout => \LCD|DISPLAY|Equal0~5_combout\);

-- Location: LCCOMB_X110_Y42_N26
\LCD|DISPLAY|Add1~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~50_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[13]~q\ & (\LCD|DISPLAY|Add1~49\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[13]~q\ & (!\LCD|DISPLAY|Add1~49\))
-- \LCD|DISPLAY|Add1~51\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[13]~q\ & !\LCD|DISPLAY|Add1~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[13]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~49\,
	combout => \LCD|DISPLAY|Add1~50_combout\,
	cout => \LCD|DISPLAY|Add1~51\);

-- Location: LCCOMB_X110_Y42_N28
\LCD|DISPLAY|Add1~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~52_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[14]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~51\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[14]~q\ & (\LCD|DISPLAY|Add1~51\ $ (GND)))
-- \LCD|DISPLAY|Add1~53\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[14]~q\) # (!\LCD|DISPLAY|Add1~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[14]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~51\,
	combout => \LCD|DISPLAY|Add1~52_combout\,
	cout => \LCD|DISPLAY|Add1~53\);

-- Location: LCCOMB_X111_Y42_N2
\LCD|DISPLAY|P_UEBERGANG:t_wait[14]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[14]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (!\LCD|DISPLAY|t_wait~0_combout\)) # (!\LCD|DISPLAY|Equal0~10_combout\ & ((\LCD|DISPLAY|Add1~52_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|t_wait~0_combout\,
	datab => \LCD|DISPLAY|Equal0~10_combout\,
	datad => \LCD|DISPLAY|Add1~52_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[14]~0_combout\);

-- Location: FF_X111_Y42_N3
\LCD|DISPLAY|P_UEBERGANG:t_wait[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[14]~0_combout\,
	asdata => \LCD|DISPLAY|P_UEBERGANG:t_wait[14]~q\,
	clrn => \nRST~input_o\,
	sload => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[14]~q\);

-- Location: LCCOMB_X112_Y42_N28
\LCD|DISPLAY|P_UEBERGANG:t_wait[17]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~3_combout\ = (!\LCD|DISPLAY|s_state_current.Send~q\ & ((!\LCD|DISPLAY|s_state_current.Init~q\) # (!\LCD|DISPLAY|s_state_init_current.Reset~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001100010011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_init_current.Reset~q\,
	datab => \LCD|DISPLAY|s_state_current.Send~q\,
	datac => \LCD|DISPLAY|s_state_current.Init~q\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~3_combout\);

-- Location: LCCOMB_X110_Y42_N30
\LCD|DISPLAY|Add1~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~54_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[15]~q\ & (\LCD|DISPLAY|Add1~53\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[15]~q\ & (!\LCD|DISPLAY|Add1~53\))
-- \LCD|DISPLAY|Add1~55\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[15]~q\ & !\LCD|DISPLAY|Add1~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[15]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~53\,
	combout => \LCD|DISPLAY|Add1~54_combout\,
	cout => \LCD|DISPLAY|Add1~55\);

-- Location: LCCOMB_X111_Y42_N8
\LCD|DISPLAY|P_UEBERGANG:t_wait[15]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[15]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (\LCD|DISPLAY|P_UEBERGANG:t_wait[17]~3_combout\)) # (!\LCD|DISPLAY|Equal0~10_combout\ & ((\LCD|DISPLAY|Add1~54_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~10_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~3_combout\,
	datad => \LCD|DISPLAY|Add1~54_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[15]~0_combout\);

-- Location: FF_X111_Y42_N9
\LCD|DISPLAY|P_UEBERGANG:t_wait[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[15]~0_combout\,
	asdata => \LCD|DISPLAY|P_UEBERGANG:t_wait[15]~q\,
	clrn => \nRST~input_o\,
	sload => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[15]~q\);

-- Location: LCCOMB_X111_Y42_N16
\LCD|DISPLAY|Equal0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Equal0~8_combout\ = (!\LCD|DISPLAY|P_UEBERGANG:t_wait[4]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[14]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[15]~q\ & !\LCD|DISPLAY|P_UEBERGANG:t_wait[12]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[4]~q\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[14]~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[15]~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:t_wait[12]~q\,
	combout => \LCD|DISPLAY|Equal0~8_combout\);

-- Location: LCCOMB_X110_Y41_N0
\LCD|DISPLAY|Add1~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~56_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[16]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~55\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[16]~q\ & (\LCD|DISPLAY|Add1~55\ $ (GND)))
-- \LCD|DISPLAY|Add1~57\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[16]~q\) # (!\LCD|DISPLAY|Add1~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[16]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~55\,
	combout => \LCD|DISPLAY|Add1~56_combout\,
	cout => \LCD|DISPLAY|Add1~57\);

-- Location: LCCOMB_X111_Y42_N30
\LCD|DISPLAY|P_UEBERGANG:t_wait[16]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[16]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & ((\LCD|DISPLAY|P_UEBERGANG:t_wait[17]~3_combout\))) # (!\LCD|DISPLAY|Equal0~10_combout\ & (\LCD|DISPLAY|Add1~56_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Add1~56_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~3_combout\,
	datad => \LCD|DISPLAY|Equal0~10_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[16]~0_combout\);

-- Location: FF_X111_Y42_N31
\LCD|DISPLAY|P_UEBERGANG:t_wait[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[16]~0_combout\,
	asdata => \LCD|DISPLAY|P_UEBERGANG:t_wait[16]~q\,
	clrn => \nRST~input_o\,
	sload => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[16]~q\);

-- Location: LCCOMB_X110_Y41_N2
\LCD|DISPLAY|Add1~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~58_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[17]~q\ & (\LCD|DISPLAY|Add1~57\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[17]~q\ & (!\LCD|DISPLAY|Add1~57\))
-- \LCD|DISPLAY|Add1~59\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[17]~q\ & !\LCD|DISPLAY|Add1~57\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~57\,
	combout => \LCD|DISPLAY|Add1~58_combout\,
	cout => \LCD|DISPLAY|Add1~59\);

-- Location: LCCOMB_X111_Y42_N24
\LCD|DISPLAY|P_UEBERGANG:t_wait[17]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & ((\LCD|DISPLAY|P_UEBERGANG:t_wait[17]~3_combout\))) # (!\LCD|DISPLAY|Equal0~10_combout\ & (\LCD|DISPLAY|Add1~58_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Add1~58_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~3_combout\,
	datad => \LCD|DISPLAY|Equal0~10_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~0_combout\);

-- Location: FF_X111_Y42_N25
\LCD|DISPLAY|P_UEBERGANG:t_wait[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~0_combout\,
	asdata => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~q\,
	clrn => \nRST~input_o\,
	sload => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~q\);

-- Location: LCCOMB_X111_Y42_N22
\LCD|DISPLAY|Equal0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Equal0~7_combout\ = (!\LCD|DISPLAY|P_UEBERGANG:t_wait[1]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[0]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[16]~q\ & !\LCD|DISPLAY|P_UEBERGANG:t_wait[17]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[1]~q\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[0]~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[16]~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~q\,
	combout => \LCD|DISPLAY|Equal0~7_combout\);

-- Location: LCCOMB_X111_Y41_N26
\LCD|DISPLAY|Equal0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Equal0~6_combout\ = (!\LCD|DISPLAY|P_UEBERGANG:t_wait[5]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[2]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[3]~q\ & !\LCD|DISPLAY|P_UEBERGANG:t_wait[6]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[5]~q\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[2]~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[3]~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:t_wait[6]~q\,
	combout => \LCD|DISPLAY|Equal0~6_combout\);

-- Location: LCCOMB_X111_Y42_N10
\LCD|DISPLAY|Equal0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Equal0~9_combout\ = (\LCD|DISPLAY|Equal0~5_combout\ & (\LCD|DISPLAY|Equal0~8_combout\ & (\LCD|DISPLAY|Equal0~7_combout\ & \LCD|DISPLAY|Equal0~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~5_combout\,
	datab => \LCD|DISPLAY|Equal0~8_combout\,
	datac => \LCD|DISPLAY|Equal0~7_combout\,
	datad => \LCD|DISPLAY|Equal0~6_combout\,
	combout => \LCD|DISPLAY|Equal0~9_combout\);

-- Location: LCCOMB_X112_Y42_N6
\LCD|DISPLAY|s_state_init_next.Finish~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_init_next.Finish~1_combout\ = (\LCD|DISPLAY|Equal0~9_combout\ & (\LCD|DISPLAY|Equal0~4_combout\ & (\LCD|DISPLAY|s_state_current.Init~q\ & \nRST~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~9_combout\,
	datab => \LCD|DISPLAY|Equal0~4_combout\,
	datac => \LCD|DISPLAY|s_state_current.Init~q\,
	datad => \nRST~input_o\,
	combout => \LCD|DISPLAY|s_state_init_next.Finish~1_combout\);

-- Location: FF_X112_Y42_N11
\LCD|DISPLAY|s_state_init_next.Func\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Selector1~0_combout\,
	ena => \LCD|DISPLAY|s_state_init_next.Finish~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_init_next.Func~q\);

-- Location: FF_X114_Y42_N1
\LCD|DISPLAY|s_state_init_current.Func\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \LCD|DISPLAY|s_state_init_next.Func~q\,
	clrn => \nRST~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_init_current.Func~q\);

-- Location: LCCOMB_X112_Y42_N0
\LCD|DISPLAY|Selector2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Selector2~0_combout\ = (\LCD|DISPLAY|s_state_init_current.Func~q\) # ((\LCD|DISPLAY|s_state_init_next.Disp~q\ & \LCD|DISPLAY|s_state_init_current.Finish~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|s_state_init_current.Func~q\,
	datac => \LCD|DISPLAY|s_state_init_next.Disp~q\,
	datad => \LCD|DISPLAY|s_state_init_current.Finish~q\,
	combout => \LCD|DISPLAY|Selector2~0_combout\);

-- Location: FF_X112_Y42_N1
\LCD|DISPLAY|s_state_init_next.Disp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Selector2~0_combout\,
	ena => \LCD|DISPLAY|s_state_init_next.Finish~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_init_next.Disp~q\);

-- Location: LCCOMB_X114_Y42_N14
\LCD|DISPLAY|s_state_init_current.Disp~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_init_current.Disp~feeder_combout\ = \LCD|DISPLAY|s_state_init_next.Disp~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|s_state_init_next.Disp~q\,
	combout => \LCD|DISPLAY|s_state_init_current.Disp~feeder_combout\);

-- Location: FF_X114_Y42_N15
\LCD|DISPLAY|s_state_init_current.Disp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_state_init_current.Disp~feeder_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_init_current.Disp~q\);

-- Location: LCCOMB_X113_Y42_N2
\LCD|DISPLAY|Selector3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Selector3~0_combout\ = (\LCD|DISPLAY|s_state_init_current.Disp~q\) # ((\LCD|DISPLAY|s_state_init_next.Entry~q\ & \LCD|DISPLAY|s_state_init_current.Finish~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_init_next.Entry~q\,
	datab => \LCD|DISPLAY|s_state_init_current.Finish~q\,
	datac => \LCD|DISPLAY|s_state_init_current.Disp~q\,
	combout => \LCD|DISPLAY|Selector3~0_combout\);

-- Location: LCCOMB_X112_Y42_N4
\LCD|DISPLAY|s_state_init_next.Entry~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_init_next.Entry~feeder_combout\ = \LCD|DISPLAY|Selector3~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|Selector3~0_combout\,
	combout => \LCD|DISPLAY|s_state_init_next.Entry~feeder_combout\);

-- Location: FF_X112_Y42_N5
\LCD|DISPLAY|s_state_init_next.Entry\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_state_init_next.Entry~feeder_combout\,
	ena => \LCD|DISPLAY|s_state_init_next.Finish~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_init_next.Entry~q\);

-- Location: FF_X114_Y42_N11
\LCD|DISPLAY|s_state_init_current.Entry\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \LCD|DISPLAY|s_state_init_next.Entry~q\,
	clrn => \nRST~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_init_current.Entry~q\);

-- Location: LCCOMB_X113_Y42_N20
\LCD|DISPLAY|Selector4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Selector4~0_combout\ = (\LCD|DISPLAY|s_state_init_current.Entry~q\) # ((\LCD|DISPLAY|s_state_init_next.Finish~q\ & \LCD|DISPLAY|s_state_init_current.Finish~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_init_next.Finish~q\,
	datab => \LCD|DISPLAY|s_state_init_current.Finish~q\,
	datac => \LCD|DISPLAY|s_state_init_current.Entry~q\,
	combout => \LCD|DISPLAY|Selector4~0_combout\);

-- Location: LCCOMB_X112_Y42_N26
\LCD|DISPLAY|s_state_init_next.Finish~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_init_next.Finish~feeder_combout\ = \LCD|DISPLAY|Selector4~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \LCD|DISPLAY|Selector4~0_combout\,
	combout => \LCD|DISPLAY|s_state_init_next.Finish~feeder_combout\);

-- Location: FF_X112_Y42_N27
\LCD|DISPLAY|s_state_init_next.Finish\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_state_init_next.Finish~feeder_combout\,
	ena => \LCD|DISPLAY|s_state_init_next.Finish~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_init_next.Finish~q\);

-- Location: LCCOMB_X114_Y42_N8
\LCD|DISPLAY|s_state_init_current.Finish~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_init_current.Finish~feeder_combout\ = \LCD|DISPLAY|s_state_init_next.Finish~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \LCD|DISPLAY|s_state_init_next.Finish~q\,
	combout => \LCD|DISPLAY|s_state_init_current.Finish~feeder_combout\);

-- Location: FF_X114_Y42_N9
\LCD|DISPLAY|s_state_init_current.Finish\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_state_init_current.Finish~feeder_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_init_current.Finish~q\);

-- Location: LCCOMB_X114_Y43_N26
\LCD|DISPLAY|s_state_init_next.Reset~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_init_next.Reset~0_combout\ = (\LCD|DISPLAY|s_state_init_next.Reset~q\) # ((\LCD|DISPLAY|s_state_init_next.Finish~0_combout\ & (\LCD|DISPLAY|s_state_current.Init~q\ & !\LCD|DISPLAY|s_state_init_current.Finish~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_init_next.Finish~0_combout\,
	datab => \LCD|DISPLAY|s_state_current.Init~q\,
	datac => \LCD|DISPLAY|s_state_init_next.Reset~q\,
	datad => \LCD|DISPLAY|s_state_init_current.Finish~q\,
	combout => \LCD|DISPLAY|s_state_init_next.Reset~0_combout\);

-- Location: FF_X114_Y43_N27
\LCD|DISPLAY|s_state_init_next.Reset\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_state_init_next.Reset~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_init_next.Reset~q\);

-- Location: FF_X114_Y43_N19
\LCD|DISPLAY|s_state_init_current.Reset\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \LCD|DISPLAY|s_state_init_next.Reset~q\,
	clrn => \nRST~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_init_current.Reset~q\);

-- Location: LCCOMB_X112_Y42_N2
\LCD|DISPLAY|t_wait~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|t_wait~0_combout\ = (\LCD|DISPLAY|s_state_init_current.Reset~q\) # (!\LCD|DISPLAY|s_state_current.Init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|s_state_current.Init~q\,
	datac => \LCD|DISPLAY|s_state_init_current.Reset~q\,
	combout => \LCD|DISPLAY|t_wait~0_combout\);

-- Location: LCCOMB_X110_Y42_N22
\LCD|DISPLAY|Add1~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~46_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[11]~q\ & (\LCD|DISPLAY|Add1~45\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[11]~q\ & (!\LCD|DISPLAY|Add1~45\))
-- \LCD|DISPLAY|Add1~47\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[11]~q\ & !\LCD|DISPLAY|Add1~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[11]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~45\,
	combout => \LCD|DISPLAY|Add1~46_combout\,
	cout => \LCD|DISPLAY|Add1~47\);

-- Location: LCCOMB_X109_Y40_N6
\LCD|DISPLAY|P_UEBERGANG:t_wait[11]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[11]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (\LCD|DISPLAY|t_wait~0_combout\ & ((\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\)))) # (!\LCD|DISPLAY|Equal0~10_combout\ & (((\LCD|DISPLAY|Add1~46_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~10_combout\,
	datab => \LCD|DISPLAY|t_wait~0_combout\,
	datac => \LCD|DISPLAY|Add1~46_combout\,
	datad => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[11]~0_combout\);

-- Location: FF_X109_Y40_N7
\LCD|DISPLAY|P_UEBERGANG:t_wait[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[11]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[11]~q\);

-- Location: LCCOMB_X110_Y42_N24
\LCD|DISPLAY|Add1~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~48_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[12]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~47\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[12]~q\ & (\LCD|DISPLAY|Add1~47\ $ (GND)))
-- \LCD|DISPLAY|Add1~49\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[12]~q\) # (!\LCD|DISPLAY|Add1~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[12]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~47\,
	combout => \LCD|DISPLAY|Add1~48_combout\,
	cout => \LCD|DISPLAY|Add1~49\);

-- Location: LCCOMB_X111_Y42_N20
\LCD|DISPLAY|P_UEBERGANG:t_wait[12]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[12]~0_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (!\LCD|DISPLAY|t_wait~0_combout\)) # (!\LCD|DISPLAY|Equal0~10_combout\ & ((\LCD|DISPLAY|Add1~48_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|t_wait~0_combout\,
	datab => \LCD|DISPLAY|Add1~48_combout\,
	datad => \LCD|DISPLAY|Equal0~10_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[12]~0_combout\);

-- Location: FF_X111_Y42_N21
\LCD|DISPLAY|P_UEBERGANG:t_wait[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[12]~0_combout\,
	asdata => \LCD|DISPLAY|P_UEBERGANG:t_wait[12]~q\,
	clrn => \nRST~input_o\,
	sload => \LCD|DISPLAY|P_UEBERGANG:t_wait[17]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[12]~q\);

-- Location: LCCOMB_X110_Y40_N12
\LCD|DISPLAY|Add1~89\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~89_combout\ = (\LCD|DISPLAY|Add1~50_combout\ & ((!\LCD|DISPLAY|Equal0~9_combout\) # (!\LCD|DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|Equal0~4_combout\,
	datac => \LCD|DISPLAY|Add1~50_combout\,
	datad => \LCD|DISPLAY|Equal0~9_combout\,
	combout => \LCD|DISPLAY|Add1~89_combout\);

-- Location: FF_X110_Y40_N13
\LCD|DISPLAY|P_UEBERGANG:t_wait[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~89_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[13]~q\);

-- Location: LCCOMB_X110_Y41_N4
\LCD|DISPLAY|Add1~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~60_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[18]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~59\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[18]~q\ & (\LCD|DISPLAY|Add1~59\ $ (GND)))
-- \LCD|DISPLAY|Add1~61\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[18]~q\) # (!\LCD|DISPLAY|Add1~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[18]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~59\,
	combout => \LCD|DISPLAY|Add1~60_combout\,
	cout => \LCD|DISPLAY|Add1~61\);

-- Location: LCCOMB_X110_Y40_N14
\LCD|DISPLAY|Add1~88\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~88_combout\ = (\LCD|DISPLAY|Add1~60_combout\ & ((!\LCD|DISPLAY|Equal0~9_combout\) # (!\LCD|DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|Equal0~4_combout\,
	datac => \LCD|DISPLAY|Add1~60_combout\,
	datad => \LCD|DISPLAY|Equal0~9_combout\,
	combout => \LCD|DISPLAY|Add1~88_combout\);

-- Location: FF_X110_Y40_N15
\LCD|DISPLAY|P_UEBERGANG:t_wait[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~88_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[18]~q\);

-- Location: LCCOMB_X110_Y41_N6
\LCD|DISPLAY|Add1~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~62_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~q\ & (\LCD|DISPLAY|Add1~61\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~q\ & (!\LCD|DISPLAY|Add1~61\))
-- \LCD|DISPLAY|Add1~63\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~q\ & !\LCD|DISPLAY|Add1~61\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~61\,
	combout => \LCD|DISPLAY|Add1~62_combout\,
	cout => \LCD|DISPLAY|Add1~63\);

-- Location: LCCOMB_X109_Y40_N24
\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~3_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & (\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\ & (\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\))) # (!\LCD|DISPLAY|Equal0~10_combout\ & 
-- (((\LCD|DISPLAY|Add1~62_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~10_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\,
	datad => \LCD|DISPLAY|Add1~62_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~3_combout\);

-- Location: FF_X109_Y40_N25
\LCD|DISPLAY|P_UEBERGANG:t_wait[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~3_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~q\);

-- Location: LCCOMB_X110_Y40_N22
\LCD|DISPLAY|Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Equal0~3_combout\ = (!\LCD|DISPLAY|P_UEBERGANG:t_wait[13]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[18]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[11]~q\ & !\LCD|DISPLAY|P_UEBERGANG:t_wait[19]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[13]~q\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[18]~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[11]~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:t_wait[19]~q\,
	combout => \LCD|DISPLAY|Equal0~3_combout\);

-- Location: LCCOMB_X110_Y41_N8
\LCD|DISPLAY|Add1~64\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~64_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[20]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~63\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[20]~q\ & (\LCD|DISPLAY|Add1~63\ $ (GND)))
-- \LCD|DISPLAY|Add1~65\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[20]~q\) # (!\LCD|DISPLAY|Add1~63\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[20]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~63\,
	combout => \LCD|DISPLAY|Add1~64_combout\,
	cout => \LCD|DISPLAY|Add1~65\);

-- Location: LCCOMB_X110_Y40_N18
\LCD|DISPLAY|Add1~103\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~103_combout\ = (\LCD|DISPLAY|Add1~64_combout\ & ((!\LCD|DISPLAY|Equal0~9_combout\) # (!\LCD|DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000001110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~4_combout\,
	datab => \LCD|DISPLAY|Equal0~9_combout\,
	datac => \LCD|DISPLAY|Add1~64_combout\,
	combout => \LCD|DISPLAY|Add1~103_combout\);

-- Location: FF_X110_Y40_N19
\LCD|DISPLAY|P_UEBERGANG:t_wait[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~103_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[20]~q\);

-- Location: LCCOMB_X110_Y41_N10
\LCD|DISPLAY|Add1~66\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~66_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[21]~q\ & (\LCD|DISPLAY|Add1~65\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[21]~q\ & (!\LCD|DISPLAY|Add1~65\))
-- \LCD|DISPLAY|Add1~67\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[21]~q\ & !\LCD|DISPLAY|Add1~65\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[21]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~65\,
	combout => \LCD|DISPLAY|Add1~66_combout\,
	cout => \LCD|DISPLAY|Add1~67\);

-- Location: LCCOMB_X110_Y40_N4
\LCD|DISPLAY|Add1~102\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~102_combout\ = (\LCD|DISPLAY|Add1~66_combout\ & ((!\LCD|DISPLAY|Equal0~4_combout\) # (!\LCD|DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|Equal0~9_combout\,
	datac => \LCD|DISPLAY|Equal0~4_combout\,
	datad => \LCD|DISPLAY|Add1~66_combout\,
	combout => \LCD|DISPLAY|Add1~102_combout\);

-- Location: FF_X110_Y40_N5
\LCD|DISPLAY|P_UEBERGANG:t_wait[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~102_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[21]~q\);

-- Location: LCCOMB_X110_Y41_N12
\LCD|DISPLAY|Add1~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~68_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[22]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~67\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[22]~q\ & (\LCD|DISPLAY|Add1~67\ $ (GND)))
-- \LCD|DISPLAY|Add1~69\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[22]~q\) # (!\LCD|DISPLAY|Add1~67\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[22]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~67\,
	combout => \LCD|DISPLAY|Add1~68_combout\,
	cout => \LCD|DISPLAY|Add1~69\);

-- Location: LCCOMB_X110_Y40_N26
\LCD|DISPLAY|Add1~101\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~101_combout\ = (\LCD|DISPLAY|Add1~68_combout\ & ((!\LCD|DISPLAY|Equal0~9_combout\) # (!\LCD|DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|Equal0~4_combout\,
	datac => \LCD|DISPLAY|Add1~68_combout\,
	datad => \LCD|DISPLAY|Equal0~9_combout\,
	combout => \LCD|DISPLAY|Add1~101_combout\);

-- Location: FF_X110_Y40_N27
\LCD|DISPLAY|P_UEBERGANG:t_wait[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~101_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[22]~q\);

-- Location: LCCOMB_X110_Y41_N14
\LCD|DISPLAY|Add1~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~70_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[23]~q\ & (\LCD|DISPLAY|Add1~69\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[23]~q\ & (!\LCD|DISPLAY|Add1~69\))
-- \LCD|DISPLAY|Add1~71\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[23]~q\ & !\LCD|DISPLAY|Add1~69\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[23]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~69\,
	combout => \LCD|DISPLAY|Add1~70_combout\,
	cout => \LCD|DISPLAY|Add1~71\);

-- Location: LCCOMB_X111_Y41_N8
\LCD|DISPLAY|Add1~100\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~100_combout\ = (\LCD|DISPLAY|Add1~70_combout\ & ((!\LCD|DISPLAY|Equal0~9_combout\) # (!\LCD|DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~4_combout\,
	datac => \LCD|DISPLAY|Equal0~9_combout\,
	datad => \LCD|DISPLAY|Add1~70_combout\,
	combout => \LCD|DISPLAY|Add1~100_combout\);

-- Location: FF_X111_Y41_N9
\LCD|DISPLAY|P_UEBERGANG:t_wait[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~100_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[23]~q\);

-- Location: LCCOMB_X110_Y40_N20
\LCD|DISPLAY|Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Equal0~2_combout\ = (!\LCD|DISPLAY|P_UEBERGANG:t_wait[22]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[20]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[21]~q\ & !\LCD|DISPLAY|P_UEBERGANG:t_wait[23]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[22]~q\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[20]~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[21]~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:t_wait[23]~q\,
	combout => \LCD|DISPLAY|Equal0~2_combout\);

-- Location: LCCOMB_X110_Y41_N16
\LCD|DISPLAY|Add1~72\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~72_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[24]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~71\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[24]~q\ & (\LCD|DISPLAY|Add1~71\ $ (GND)))
-- \LCD|DISPLAY|Add1~73\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[24]~q\) # (!\LCD|DISPLAY|Add1~71\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[24]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~71\,
	combout => \LCD|DISPLAY|Add1~72_combout\,
	cout => \LCD|DISPLAY|Add1~73\);

-- Location: LCCOMB_X111_Y41_N30
\LCD|DISPLAY|Add1~99\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~99_combout\ = (\LCD|DISPLAY|Add1~72_combout\ & ((!\LCD|DISPLAY|Equal0~4_combout\) # (!\LCD|DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|Equal0~9_combout\,
	datac => \LCD|DISPLAY|Add1~72_combout\,
	datad => \LCD|DISPLAY|Equal0~4_combout\,
	combout => \LCD|DISPLAY|Add1~99_combout\);

-- Location: FF_X111_Y41_N31
\LCD|DISPLAY|P_UEBERGANG:t_wait[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~99_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[24]~q\);

-- Location: LCCOMB_X110_Y41_N18
\LCD|DISPLAY|Add1~74\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~74_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[25]~q\ & (\LCD|DISPLAY|Add1~73\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[25]~q\ & (!\LCD|DISPLAY|Add1~73\))
-- \LCD|DISPLAY|Add1~75\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[25]~q\ & !\LCD|DISPLAY|Add1~73\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[25]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~73\,
	combout => \LCD|DISPLAY|Add1~74_combout\,
	cout => \LCD|DISPLAY|Add1~75\);

-- Location: LCCOMB_X111_Y41_N16
\LCD|DISPLAY|Add1~98\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~98_combout\ = (\LCD|DISPLAY|Add1~74_combout\ & ((!\LCD|DISPLAY|Equal0~9_combout\) # (!\LCD|DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~4_combout\,
	datac => \LCD|DISPLAY|Equal0~9_combout\,
	datad => \LCD|DISPLAY|Add1~74_combout\,
	combout => \LCD|DISPLAY|Add1~98_combout\);

-- Location: FF_X111_Y41_N17
\LCD|DISPLAY|P_UEBERGANG:t_wait[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~98_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[25]~q\);

-- Location: LCCOMB_X110_Y41_N20
\LCD|DISPLAY|Add1~76\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~76_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[26]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~75\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[26]~q\ & (\LCD|DISPLAY|Add1~75\ $ (GND)))
-- \LCD|DISPLAY|Add1~77\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[26]~q\) # (!\LCD|DISPLAY|Add1~75\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[26]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~75\,
	combout => \LCD|DISPLAY|Add1~76_combout\,
	cout => \LCD|DISPLAY|Add1~77\);

-- Location: LCCOMB_X110_Y40_N6
\LCD|DISPLAY|Add1~97\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~97_combout\ = (\LCD|DISPLAY|Add1~76_combout\ & ((!\LCD|DISPLAY|Equal0~4_combout\) # (!\LCD|DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|Equal0~9_combout\,
	datac => \LCD|DISPLAY|Equal0~4_combout\,
	datad => \LCD|DISPLAY|Add1~76_combout\,
	combout => \LCD|DISPLAY|Add1~97_combout\);

-- Location: FF_X110_Y40_N7
\LCD|DISPLAY|P_UEBERGANG:t_wait[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~97_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[26]~q\);

-- Location: LCCOMB_X110_Y41_N22
\LCD|DISPLAY|Add1~78\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~78_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[27]~q\ & (\LCD|DISPLAY|Add1~77\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[27]~q\ & (!\LCD|DISPLAY|Add1~77\))
-- \LCD|DISPLAY|Add1~79\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[27]~q\ & !\LCD|DISPLAY|Add1~77\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[27]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~77\,
	combout => \LCD|DISPLAY|Add1~78_combout\,
	cout => \LCD|DISPLAY|Add1~79\);

-- Location: LCCOMB_X110_Y40_N0
\LCD|DISPLAY|Add1~96\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~96_combout\ = (\LCD|DISPLAY|Add1~78_combout\ & ((!\LCD|DISPLAY|Equal0~4_combout\) # (!\LCD|DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|Equal0~9_combout\,
	datac => \LCD|DISPLAY|Equal0~4_combout\,
	datad => \LCD|DISPLAY|Add1~78_combout\,
	combout => \LCD|DISPLAY|Add1~96_combout\);

-- Location: FF_X110_Y40_N1
\LCD|DISPLAY|P_UEBERGANG:t_wait[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~96_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[27]~q\);

-- Location: LCCOMB_X110_Y41_N24
\LCD|DISPLAY|Add1~80\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~80_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[28]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~79\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[28]~q\ & (\LCD|DISPLAY|Add1~79\ $ (GND)))
-- \LCD|DISPLAY|Add1~81\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[28]~q\) # (!\LCD|DISPLAY|Add1~79\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[28]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~79\,
	combout => \LCD|DISPLAY|Add1~80_combout\,
	cout => \LCD|DISPLAY|Add1~81\);

-- Location: LCCOMB_X110_Y40_N28
\LCD|DISPLAY|Add1~95\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~95_combout\ = (\LCD|DISPLAY|Add1~80_combout\ & ((!\LCD|DISPLAY|Equal0~9_combout\) # (!\LCD|DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|Equal0~4_combout\,
	datac => \LCD|DISPLAY|Add1~80_combout\,
	datad => \LCD|DISPLAY|Equal0~9_combout\,
	combout => \LCD|DISPLAY|Add1~95_combout\);

-- Location: FF_X110_Y40_N29
\LCD|DISPLAY|P_UEBERGANG:t_wait[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~95_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[28]~q\);

-- Location: LCCOMB_X110_Y41_N26
\LCD|DISPLAY|Add1~82\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~82_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[29]~q\ & (\LCD|DISPLAY|Add1~81\ & VCC)) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[29]~q\ & (!\LCD|DISPLAY|Add1~81\))
-- \LCD|DISPLAY|Add1~83\ = CARRY((!\LCD|DISPLAY|P_UEBERGANG:t_wait[29]~q\ & !\LCD|DISPLAY|Add1~81\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[29]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~81\,
	combout => \LCD|DISPLAY|Add1~82_combout\,
	cout => \LCD|DISPLAY|Add1~83\);

-- Location: LCCOMB_X110_Y40_N10
\LCD|DISPLAY|Add1~94\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~94_combout\ = (\LCD|DISPLAY|Add1~82_combout\ & ((!\LCD|DISPLAY|Equal0~9_combout\) # (!\LCD|DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|Equal0~4_combout\,
	datac => \LCD|DISPLAY|Add1~82_combout\,
	datad => \LCD|DISPLAY|Equal0~9_combout\,
	combout => \LCD|DISPLAY|Add1~94_combout\);

-- Location: FF_X110_Y40_N11
\LCD|DISPLAY|P_UEBERGANG:t_wait[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~94_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[29]~q\);

-- Location: LCCOMB_X110_Y41_N28
\LCD|DISPLAY|Add1~84\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~84_combout\ = (\LCD|DISPLAY|P_UEBERGANG:t_wait[30]~q\ & ((GND) # (!\LCD|DISPLAY|Add1~83\))) # (!\LCD|DISPLAY|P_UEBERGANG:t_wait[30]~q\ & (\LCD|DISPLAY|Add1~83\ $ (GND)))
-- \LCD|DISPLAY|Add1~85\ = CARRY((\LCD|DISPLAY|P_UEBERGANG:t_wait[30]~q\) # (!\LCD|DISPLAY|Add1~83\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[30]~q\,
	datad => VCC,
	cin => \LCD|DISPLAY|Add1~83\,
	combout => \LCD|DISPLAY|Add1~84_combout\,
	cout => \LCD|DISPLAY|Add1~85\);

-- Location: LCCOMB_X110_Y40_N16
\LCD|DISPLAY|Add1~93\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~93_combout\ = (\LCD|DISPLAY|Add1~84_combout\ & ((!\LCD|DISPLAY|Equal0~4_combout\) # (!\LCD|DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|Equal0~9_combout\,
	datac => \LCD|DISPLAY|Equal0~4_combout\,
	datad => \LCD|DISPLAY|Add1~84_combout\,
	combout => \LCD|DISPLAY|Add1~93_combout\);

-- Location: FF_X110_Y40_N17
\LCD|DISPLAY|P_UEBERGANG:t_wait[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~93_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[30]~q\);

-- Location: LCCOMB_X110_Y41_N30
\LCD|DISPLAY|Add1~86\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~86_combout\ = \LCD|DISPLAY|Add1~85\ $ (!\LCD|DISPLAY|P_UEBERGANG:t_wait[31]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|P_UEBERGANG:t_wait[31]~q\,
	cin => \LCD|DISPLAY|Add1~85\,
	combout => \LCD|DISPLAY|Add1~86_combout\);

-- Location: LCCOMB_X111_Y41_N22
\LCD|DISPLAY|Add1~92\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Add1~92_combout\ = (\LCD|DISPLAY|Add1~86_combout\ & ((!\LCD|DISPLAY|Equal0~9_combout\) # (!\LCD|DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~4_combout\,
	datac => \LCD|DISPLAY|Equal0~9_combout\,
	datad => \LCD|DISPLAY|Add1~86_combout\,
	combout => \LCD|DISPLAY|Add1~92_combout\);

-- Location: FF_X111_Y41_N23
\LCD|DISPLAY|P_UEBERGANG:t_wait[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|Add1~92_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:t_wait[31]~q\);

-- Location: LCCOMB_X110_Y40_N30
\LCD|DISPLAY|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Equal0~0_combout\ = (!\LCD|DISPLAY|P_UEBERGANG:t_wait[29]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[30]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[28]~q\ & !\LCD|DISPLAY|P_UEBERGANG:t_wait[31]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[29]~q\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[30]~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[28]~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:t_wait[31]~q\,
	combout => \LCD|DISPLAY|Equal0~0_combout\);

-- Location: LCCOMB_X110_Y40_N24
\LCD|DISPLAY|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Equal0~1_combout\ = (!\LCD|DISPLAY|P_UEBERGANG:t_wait[26]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[27]~q\ & (!\LCD|DISPLAY|P_UEBERGANG:t_wait[24]~q\ & !\LCD|DISPLAY|P_UEBERGANG:t_wait[25]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:t_wait[26]~q\,
	datab => \LCD|DISPLAY|P_UEBERGANG:t_wait[27]~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:t_wait[24]~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:t_wait[25]~q\,
	combout => \LCD|DISPLAY|Equal0~1_combout\);

-- Location: LCCOMB_X110_Y40_N8
\LCD|DISPLAY|Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|Equal0~4_combout\ = (\LCD|DISPLAY|Equal0~3_combout\ & (\LCD|DISPLAY|Equal0~2_combout\ & (\LCD|DISPLAY|Equal0~0_combout\ & \LCD|DISPLAY|Equal0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~3_combout\,
	datab => \LCD|DISPLAY|Equal0~2_combout\,
	datac => \LCD|DISPLAY|Equal0~0_combout\,
	datad => \LCD|DISPLAY|Equal0~1_combout\,
	combout => \LCD|DISPLAY|Equal0~4_combout\);

-- Location: LCCOMB_X114_Y43_N2
\LCD|DISPLAY|s_state_init_next.Finish~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_init_next.Finish~0_combout\ = (\nRST~input_o\ & (\LCD|DISPLAY|Equal0~4_combout\ & \LCD|DISPLAY|Equal0~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datac => \LCD|DISPLAY|Equal0~4_combout\,
	datad => \LCD|DISPLAY|Equal0~9_combout\,
	combout => \LCD|DISPLAY|s_state_init_next.Finish~0_combout\);

-- Location: LCCOMB_X114_Y43_N4
\LCD|DISPLAY|s_state_next.Start~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_next.Start~0_combout\ = (\LCD|DISPLAY|s_state_next.Init~1_combout\) # (\LCD|DISPLAY|s_state_next.Start~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_next.Init~1_combout\,
	datac => \LCD|DISPLAY|s_state_next.Start~q\,
	combout => \LCD|DISPLAY|s_state_next.Start~0_combout\);

-- Location: FF_X114_Y43_N5
\LCD|DISPLAY|s_state_next.Start\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_state_next.Start~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_next.Start~q\);

-- Location: FF_X114_Y43_N29
\LCD|DISPLAY|s_state_current.Start\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \LCD|DISPLAY|s_state_next.Start~q\,
	clrn => \nRST~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_current.Start~q\);

-- Location: LCCOMB_X114_Y43_N24
\LCD|DISPLAY|s_state_next.Init~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_next.Init~2_combout\ = (\LCD|DISPLAY|s_state_init_next.Finish~0_combout\ & (((!\LCD|DISPLAY|s_state_next.Init~1_combout\ & \LCD|DISPLAY|s_state_next.Init~q\)) # (!\LCD|DISPLAY|s_state_current.Start~q\))) # 
-- (!\LCD|DISPLAY|s_state_init_next.Finish~0_combout\ & (!\LCD|DISPLAY|s_state_next.Init~1_combout\ & (\LCD|DISPLAY|s_state_next.Init~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_init_next.Finish~0_combout\,
	datab => \LCD|DISPLAY|s_state_next.Init~1_combout\,
	datac => \LCD|DISPLAY|s_state_next.Init~q\,
	datad => \LCD|DISPLAY|s_state_current.Start~q\,
	combout => \LCD|DISPLAY|s_state_next.Init~2_combout\);

-- Location: FF_X114_Y43_N25
\LCD|DISPLAY|s_state_next.Init\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_state_next.Init~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_next.Init~q\);

-- Location: LCCOMB_X114_Y43_N0
\LCD|DISPLAY|s_state_current.Init~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_state_current.Init~feeder_combout\ = \LCD|DISPLAY|s_state_next.Init~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|s_state_next.Init~q\,
	combout => \LCD|DISPLAY|s_state_current.Init~feeder_combout\);

-- Location: FF_X114_Y43_N1
\LCD|DISPLAY|s_state_current.Init\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_state_current.Init~feeder_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_state_current.Init~q\);

-- Location: LCCOMB_X113_Y42_N22
\LCD|DISPLAY|en~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|en~0_combout\ = (\LCD|DISPLAY|s_state_current.Init~q\ & (\LCD|DISPLAY|s_state_init_current.Reset~q\ & (!\LCD|DISPLAY|s_state_init_current.Finish~q\ & !\LCD|DISPLAY|s_state_current.Send~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_current.Init~q\,
	datab => \LCD|DISPLAY|s_state_init_current.Reset~q\,
	datac => \LCD|DISPLAY|s_state_init_current.Finish~q\,
	datad => \LCD|DISPLAY|s_state_current.Send~q\,
	combout => \LCD|DISPLAY|en~0_combout\);

-- Location: FF_X113_Y42_N17
\LCD|DISPLAY|P_UEBERGANG:en\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|en~1_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:en~q\);

-- Location: LCCOMB_X113_Y42_N16
\LCD|DISPLAY|en~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|en~1_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & ((\LCD|DISPLAY|en~0_combout\) # ((\LCD|DISPLAY|s_state_next.Send~0_combout\) # (\LCD|DISPLAY|P_UEBERGANG:en~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|en~0_combout\,
	datab => \LCD|DISPLAY|s_state_next.Send~0_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:en~q\,
	datad => \LCD|DISPLAY|Equal0~10_combout\,
	combout => \LCD|DISPLAY|en~1_combout\);

-- Location: LCCOMB_X113_Y42_N4
\LCD|DISPLAY|s_en~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_en~feeder_combout\ = \LCD|DISPLAY|en~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|en~1_combout\,
	combout => \LCD|DISPLAY|s_en~feeder_combout\);

-- Location: FF_X113_Y42_N5
\LCD|DISPLAY|s_en\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_en~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_en~q\);

-- Location: FF_X113_Y41_N25
\LCD|DISPLAY|s_rs\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|rs~3_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_rs~q\);

-- Location: LCCOMB_X114_Y41_N8
\LCD|DISPLAY|P_UEBERGANG:data[0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:data[0]~feeder_combout\ = \LCD|DISPLAY|data~7_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|data~7_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:data[0]~feeder_combout\);

-- Location: FF_X114_Y41_N9
\LCD|DISPLAY|P_UEBERGANG:data[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:data[0]~feeder_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:data[0]~q\);

-- Location: LCCOMB_X114_Y42_N30
\LCD|Mux6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Mux6~0_combout\ = (\LCD|ci\(1) & (\LCD|ci\(2) $ (((\LCD|ci\(3)) # (!\LCD|ci\(0)))))) # (!\LCD|ci\(1) & ((\LCD|ci\(0) & ((\LCD|ci\(2)))) # (!\LCD|ci\(0) & (\LCD|ci\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110110000111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|ci\(3),
	datab => \LCD|ci\(2),
	datac => \LCD|ci\(1),
	datad => \LCD|ci\(0),
	combout => \LCD|Mux6~0_combout\);

-- Location: FF_X114_Y42_N31
\LCD|s_data[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Mux6~0_combout\,
	ena => \LCD|s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_data\(0));

-- Location: LCCOMB_X114_Y41_N12
\LCD|DISPLAY|data~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~5_combout\ = (((!\LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\) # (!\LCD|DISPLAY|P_UEBERGANG:rs~q\)) # (!\LCD|DISPLAY|s_state_send_current~q\)) # (!\LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~q\,
	datab => \LCD|DISPLAY|s_state_send_current~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:rs~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\,
	combout => \LCD|DISPLAY|data~5_combout\);

-- Location: LCCOMB_X114_Y41_N22
\LCD|DISPLAY|data~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~4_combout\ = (!\LCD|DISPLAY|s_busy~2_combout\ & (((\LCD|DISPLAY|s_state_init_current.Finish~q\) # (!\LCD|DISPLAY|s_state_current.Init~q\)) # (!\LCD|DISPLAY|s_state_init_current.Reset~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_busy~2_combout\,
	datab => \LCD|DISPLAY|s_state_init_current.Reset~q\,
	datac => \LCD|DISPLAY|s_state_init_current.Finish~q\,
	datad => \LCD|DISPLAY|s_state_current.Init~q\,
	combout => \LCD|DISPLAY|data~4_combout\);

-- Location: LCCOMB_X114_Y41_N18
\LCD|DISPLAY|data~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~6_combout\ = ((\LCD|DISPLAY|data~4_combout\ & ((\LCD|DISPLAY|data~5_combout\) # (!\LCD|DISPLAY|s_state_current.Send~q\)))) # (!\LCD|DISPLAY|Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|data~5_combout\,
	datab => \LCD|DISPLAY|s_state_current.Send~q\,
	datac => \LCD|DISPLAY|data~4_combout\,
	datad => \LCD|DISPLAY|Equal0~10_combout\,
	combout => \LCD|DISPLAY|data~6_combout\);

-- Location: LCCOMB_X114_Y41_N20
\LCD|DISPLAY|data~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~7_combout\ = (\LCD|DISPLAY|data~6_combout\ & (((\LCD|DISPLAY|P_UEBERGANG:data[0]~q\)))) # (!\LCD|DISPLAY|data~6_combout\ & (\LCD|DISPLAY|s_state_current.Ready~q\ & ((\LCD|s_data\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_current.Ready~q\,
	datab => \LCD|DISPLAY|P_UEBERGANG:data[0]~q\,
	datac => \LCD|s_data\(0),
	datad => \LCD|DISPLAY|data~6_combout\,
	combout => \LCD|DISPLAY|data~7_combout\);

-- Location: FF_X114_Y41_N21
\LCD|DISPLAY|s_data[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|data~7_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_data\(0));

-- Location: LCCOMB_X112_Y40_N16
\LCD|DISPLAY|P_UEBERGANG:data[1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:data[1]~feeder_combout\ = \LCD|DISPLAY|data~14_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|data~14_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:data[1]~feeder_combout\);

-- Location: FF_X112_Y40_N17
\LCD|DISPLAY|P_UEBERGANG:data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:data[1]~feeder_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:data[1]~q\);

-- Location: LCCOMB_X114_Y42_N24
\LCD|Mux5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Mux5~0_combout\ = (\LCD|ci\(3) & (\LCD|ci\(2) & ((!\LCD|ci\(0))))) # (!\LCD|ci\(3) & ((\LCD|ci\(1) $ (\LCD|ci\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010111011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|ci\(3),
	datab => \LCD|ci\(2),
	datac => \LCD|ci\(1),
	datad => \LCD|ci\(0),
	combout => \LCD|Mux5~0_combout\);

-- Location: FF_X114_Y42_N25
\LCD|s_data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Mux5~0_combout\,
	ena => \LCD|s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_data\(1));

-- Location: LCCOMB_X113_Y42_N0
\LCD|DISPLAY|data~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~8_combout\ = (\LCD|DISPLAY|s_state_current.Ready~q\ & (\LCD|s_data\(1))) # (!\LCD|DISPLAY|s_state_current.Ready~q\ & ((\LCD|DISPLAY|s_state_init_current.Entry~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|s_data\(1),
	datac => \LCD|DISPLAY|s_state_init_current.Entry~q\,
	datad => \LCD|DISPLAY|s_state_current.Ready~q\,
	combout => \LCD|DISPLAY|data~8_combout\);

-- Location: LCCOMB_X114_Y43_N30
\LCD|DISPLAY|data~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~11_combout\ = (\LCD|DISPLAY|s_state_current.Start~q\ & ((\LCD|s_en~q\) # (!\LCD|DISPLAY|s_state_current.Ready~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_current.Ready~q\,
	datab => \LCD|DISPLAY|s_state_current.Start~q\,
	datad => \LCD|s_en~q\,
	combout => \LCD|DISPLAY|data~11_combout\);

-- Location: LCCOMB_X112_Y42_N30
\LCD|DISPLAY|data~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~30_combout\ = (\LCD|DISPLAY|Equal0~9_combout\ & (!\LCD|DISPLAY|s_state_current.Send~q\ & (\LCD|DISPLAY|data~11_combout\ & \LCD|DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~9_combout\,
	datab => \LCD|DISPLAY|s_state_current.Send~q\,
	datac => \LCD|DISPLAY|data~11_combout\,
	datad => \LCD|DISPLAY|Equal0~4_combout\,
	combout => \LCD|DISPLAY|data~30_combout\);

-- Location: LCCOMB_X112_Y42_N12
\LCD|DISPLAY|data~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~10_combout\ = (!\LCD|DISPLAY|s_state_current.Ready~q\ & (!\LCD|DISPLAY|s_state_current.Send~q\ & ((\LCD|DISPLAY|s_state_init_current.Finish~q\) # (!\LCD|DISPLAY|s_state_init_current.Reset~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_current.Ready~q\,
	datab => \LCD|DISPLAY|s_state_current.Send~q\,
	datac => \LCD|DISPLAY|s_state_init_current.Reset~q\,
	datad => \LCD|DISPLAY|s_state_init_current.Finish~q\,
	combout => \LCD|DISPLAY|data~10_combout\);

-- Location: LCCOMB_X113_Y42_N18
\LCD|DISPLAY|data~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~9_combout\ = (\LCD|DISPLAY|s_state_current.Send~q\ & ((!\LCD|DISPLAY|P_UEBERGANG:c_pos_y~0_combout\) # (!\LCD|DISPLAY|s_state_send_current~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_send_current~q\,
	datac => \LCD|DISPLAY|s_state_current.Send~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~0_combout\,
	combout => \LCD|DISPLAY|data~9_combout\);

-- Location: LCCOMB_X112_Y42_N18
\LCD|DISPLAY|data~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~12_combout\ = (\LCD|DISPLAY|Equal0~9_combout\ & (\LCD|DISPLAY|data~11_combout\ & \LCD|DISPLAY|Equal0~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~9_combout\,
	datac => \LCD|DISPLAY|data~11_combout\,
	datad => \LCD|DISPLAY|Equal0~4_combout\,
	combout => \LCD|DISPLAY|data~12_combout\);

-- Location: LCCOMB_X112_Y42_N20
\LCD|DISPLAY|data~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~13_combout\ = (\LCD|DISPLAY|data~10_combout\) # ((\LCD|DISPLAY|data~9_combout\) # (!\LCD|DISPLAY|data~12_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|data~10_combout\,
	datab => \LCD|DISPLAY|data~9_combout\,
	datad => \LCD|DISPLAY|data~12_combout\,
	combout => \LCD|DISPLAY|data~13_combout\);

-- Location: LCCOMB_X112_Y42_N16
\LCD|DISPLAY|data~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~14_combout\ = (\LCD|DISPLAY|P_UEBERGANG:data[1]~q\ & ((\LCD|DISPLAY|data~13_combout\) # ((\LCD|DISPLAY|data~8_combout\ & \LCD|DISPLAY|data~30_combout\)))) # (!\LCD|DISPLAY|P_UEBERGANG:data[1]~q\ & (\LCD|DISPLAY|data~8_combout\ & 
-- (\LCD|DISPLAY|data~30_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:data[1]~q\,
	datab => \LCD|DISPLAY|data~8_combout\,
	datac => \LCD|DISPLAY|data~30_combout\,
	datad => \LCD|DISPLAY|data~13_combout\,
	combout => \LCD|DISPLAY|data~14_combout\);

-- Location: FF_X112_Y42_N21
\LCD|DISPLAY|s_data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \LCD|DISPLAY|data~14_combout\,
	sload => VCC,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_data\(1));

-- Location: LCCOMB_X114_Y42_N16
\LCD|Mux4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Mux4~0_combout\ = (\LCD|ci\(3) & (\LCD|ci\(2) & (\LCD|ci\(1) $ (\LCD|ci\(0))))) # (!\LCD|ci\(3) & (\LCD|ci\(0) & ((\LCD|ci\(2)) # (\LCD|ci\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|ci\(3),
	datab => \LCD|ci\(2),
	datac => \LCD|ci\(1),
	datad => \LCD|ci\(0),
	combout => \LCD|Mux4~0_combout\);

-- Location: FF_X114_Y42_N17
\LCD|s_data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Mux4~0_combout\,
	ena => \LCD|s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_data\(2));

-- Location: LCCOMB_X114_Y42_N10
\LCD|DISPLAY|data~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~15_combout\ = (\LCD|DISPLAY|s_state_current.Ready~q\ & (\LCD|s_data\(2))) # (!\LCD|DISPLAY|s_state_current.Ready~q\ & ((!\LCD|DISPLAY|s_state_current.Send~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011011101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_current.Ready~q\,
	datab => \LCD|s_data\(2),
	datad => \LCD|DISPLAY|s_state_current.Send~q\,
	combout => \LCD|DISPLAY|data~15_combout\);

-- Location: FF_X114_Y41_N25
\LCD|DISPLAY|P_UEBERGANG:data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|data~16_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:data[2]~q\);

-- Location: LCCOMB_X114_Y41_N24
\LCD|DISPLAY|data~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~16_combout\ = (\LCD|DISPLAY|data~6_combout\ & ((\LCD|DISPLAY|P_UEBERGANG:data[2]~q\))) # (!\LCD|DISPLAY|data~6_combout\ & (\LCD|DISPLAY|data~15_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|data~15_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:data[2]~q\,
	datad => \LCD|DISPLAY|data~6_combout\,
	combout => \LCD|DISPLAY|data~16_combout\);

-- Location: LCCOMB_X114_Y41_N2
\LCD|DISPLAY|s_data[2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_data[2]~feeder_combout\ = \LCD|DISPLAY|data~16_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|data~16_combout\,
	combout => \LCD|DISPLAY|s_data[2]~feeder_combout\);

-- Location: FF_X114_Y41_N3
\LCD|DISPLAY|s_data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_data[2]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_data\(2));

-- Location: LCCOMB_X114_Y42_N26
\LCD|Mux3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Mux3~0_combout\ = (\LCD|ci\(3) & (\LCD|ci\(0) $ (((\LCD|ci\(2)) # (!\LCD|ci\(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|ci\(3),
	datab => \LCD|ci\(2),
	datac => \LCD|ci\(1),
	datad => \LCD|ci\(0),
	combout => \LCD|Mux3~0_combout\);

-- Location: FF_X114_Y42_N27
\LCD|s_data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Mux3~0_combout\,
	ena => \LCD|s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_data\(3));

-- Location: LCCOMB_X113_Y42_N28
\LCD|DISPLAY|data~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~17_combout\ = (\LCD|DISPLAY|s_state_current.Ready~q\ & (\LCD|s_data\(3))) # (!\LCD|DISPLAY|s_state_current.Ready~q\ & (((\LCD|DISPLAY|s_state_init_current.Func~q\) # (\LCD|DISPLAY|s_state_init_current.Disp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|s_data\(3),
	datab => \LCD|DISPLAY|s_state_init_current.Func~q\,
	datac => \LCD|DISPLAY|s_state_init_current.Disp~q\,
	datad => \LCD|DISPLAY|s_state_current.Ready~q\,
	combout => \LCD|DISPLAY|data~17_combout\);

-- Location: FF_X112_Y41_N29
\LCD|DISPLAY|P_UEBERGANG:data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|data~18_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:data[3]~q\);

-- Location: LCCOMB_X112_Y41_N28
\LCD|DISPLAY|data~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~18_combout\ = (\LCD|DISPLAY|data~30_combout\ & ((\LCD|DISPLAY|data~17_combout\) # ((\LCD|DISPLAY|P_UEBERGANG:data[3]~q\ & \LCD|DISPLAY|data~13_combout\)))) # (!\LCD|DISPLAY|data~30_combout\ & (((\LCD|DISPLAY|P_UEBERGANG:data[3]~q\ & 
-- \LCD|DISPLAY|data~13_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|data~30_combout\,
	datab => \LCD|DISPLAY|data~17_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:data[3]~q\,
	datad => \LCD|DISPLAY|data~13_combout\,
	combout => \LCD|DISPLAY|data~18_combout\);

-- Location: LCCOMB_X112_Y41_N20
\LCD|DISPLAY|s_data[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_data[3]~feeder_combout\ = \LCD|DISPLAY|data~18_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|data~18_combout\,
	combout => \LCD|DISPLAY|s_data[3]~feeder_combout\);

-- Location: FF_X112_Y41_N21
\LCD|DISPLAY|s_data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_data[3]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_data\(3));

-- Location: LCCOMB_X114_Y42_N20
\LCD|Mux2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Mux2~0_combout\ = (!\LCD|ci\(0) & ((\LCD|ci\(3) & (!\LCD|ci\(2) & !\LCD|ci\(1))) # (!\LCD|ci\(3) & (\LCD|ci\(2) & \LCD|ci\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|ci\(3),
	datab => \LCD|ci\(2),
	datac => \LCD|ci\(1),
	datad => \LCD|ci\(0),
	combout => \LCD|Mux2~0_combout\);

-- Location: FF_X114_Y42_N21
\LCD|s_data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Mux2~0_combout\,
	ena => \LCD|s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_data\(4));

-- Location: LCCOMB_X113_Y42_N30
\LCD|DISPLAY|data~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~19_combout\ = (\LCD|DISPLAY|s_state_current.Ready~q\ & (\LCD|s_data\(4))) # (!\LCD|DISPLAY|s_state_current.Ready~q\ & ((\LCD|DISPLAY|s_state_init_current.Func~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_current.Ready~q\,
	datac => \LCD|s_data\(4),
	datad => \LCD|DISPLAY|s_state_init_current.Func~q\,
	combout => \LCD|DISPLAY|data~19_combout\);

-- Location: LCCOMB_X112_Y40_N26
\LCD|DISPLAY|P_UEBERGANG:data[4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:data[4]~feeder_combout\ = \LCD|DISPLAY|data~20_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|data~20_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:data[4]~feeder_combout\);

-- Location: FF_X112_Y40_N27
\LCD|DISPLAY|P_UEBERGANG:data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:data[4]~feeder_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:data[4]~q\);

-- Location: LCCOMB_X112_Y42_N14
\LCD|DISPLAY|data~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~20_combout\ = (\LCD|DISPLAY|data~19_combout\ & ((\LCD|DISPLAY|data~30_combout\) # ((\LCD|DISPLAY|P_UEBERGANG:data[4]~q\ & \LCD|DISPLAY|data~13_combout\)))) # (!\LCD|DISPLAY|data~19_combout\ & (\LCD|DISPLAY|P_UEBERGANG:data[4]~q\ & 
-- ((\LCD|DISPLAY|data~13_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|data~19_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:data[4]~q\,
	datac => \LCD|DISPLAY|data~30_combout\,
	datad => \LCD|DISPLAY|data~13_combout\,
	combout => \LCD|DISPLAY|data~20_combout\);

-- Location: FF_X112_Y42_N15
\LCD|DISPLAY|s_data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|data~20_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_data\(4));

-- Location: LCCOMB_X114_Y42_N28
\LCD|Mux1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|Mux1~0_combout\ = (\LCD|ci\(3) & ((\LCD|ci\(2) $ (\LCD|ci\(1))) # (!\LCD|ci\(0)))) # (!\LCD|ci\(3) & (((\LCD|ci\(1)) # (\LCD|ci\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111110111111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|ci\(3),
	datab => \LCD|ci\(2),
	datac => \LCD|ci\(1),
	datad => \LCD|ci\(0),
	combout => \LCD|Mux1~0_combout\);

-- Location: LCCOMB_X114_Y42_N6
\LCD|s_data[5]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|s_data[5]~1_combout\ = !\LCD|Mux1~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|Mux1~0_combout\,
	combout => \LCD|s_data[5]~1_combout\);

-- Location: FF_X114_Y42_N7
\LCD|s_data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|s_data[5]~1_combout\,
	ena => \LCD|s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_data\(5));

-- Location: LCCOMB_X113_Y42_N8
\LCD|DISPLAY|data~21\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~21_combout\ = (\LCD|DISPLAY|s_state_current.Ready~q\ & ((\LCD|s_data\(5)))) # (!\LCD|DISPLAY|s_state_current.Ready~q\ & (\LCD|DISPLAY|s_state_init_current.Func~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_init_current.Func~q\,
	datac => \LCD|s_data\(5),
	datad => \LCD|DISPLAY|s_state_current.Ready~q\,
	combout => \LCD|DISPLAY|data~21_combout\);

-- Location: FF_X112_Y41_N19
\LCD|DISPLAY|P_UEBERGANG:data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|data~22_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:data[5]~q\);

-- Location: LCCOMB_X112_Y41_N18
\LCD|DISPLAY|data~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~22_combout\ = (\LCD|DISPLAY|data~30_combout\ & ((\LCD|DISPLAY|data~21_combout\) # ((\LCD|DISPLAY|P_UEBERGANG:data[5]~q\ & \LCD|DISPLAY|data~13_combout\)))) # (!\LCD|DISPLAY|data~30_combout\ & (((\LCD|DISPLAY|P_UEBERGANG:data[5]~q\ & 
-- \LCD|DISPLAY|data~13_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|data~30_combout\,
	datab => \LCD|DISPLAY|data~21_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:data[5]~q\,
	datad => \LCD|DISPLAY|data~13_combout\,
	combout => \LCD|DISPLAY|data~22_combout\);

-- Location: LCCOMB_X112_Y41_N22
\LCD|DISPLAY|s_data[5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_data[5]~feeder_combout\ = \LCD|DISPLAY|data~22_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|data~22_combout\,
	combout => \LCD|DISPLAY|s_data[5]~feeder_combout\);

-- Location: FF_X112_Y41_N23
\LCD|DISPLAY|s_data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_data[5]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_data\(5));

-- Location: LCCOMB_X114_Y41_N6
\LCD|DISPLAY|data~25\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~25_combout\ = (\LCD|DISPLAY|P_UEBERGANG:c_pos_y~1_combout\ & (\LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~q\ & (\LCD|DISPLAY|P_UEBERGANG:rs~q\ & \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~1_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~q\,
	datac => \LCD|DISPLAY|P_UEBERGANG:rs~q\,
	datad => \LCD|DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\,
	combout => \LCD|DISPLAY|data~25_combout\);

-- Location: LCCOMB_X114_Y41_N4
\LCD|DISPLAY|P_UEBERGANG:c_pos_y~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|P_UEBERGANG:c_pos_y~2_combout\ = \LCD|DISPLAY|P_UEBERGANG:c_pos_y~q\ $ (((\LCD|DISPLAY|data~25_combout\ & (\LCD|DISPLAY|Equal0~9_combout\ & \LCD|DISPLAY|Equal0~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|data~25_combout\,
	datab => \LCD|DISPLAY|Equal0~9_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~q\,
	datad => \LCD|DISPLAY|Equal0~4_combout\,
	combout => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~2_combout\);

-- Location: FF_X114_Y41_N5
\LCD|DISPLAY|P_UEBERGANG:c_pos_y\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~2_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~q\);

-- Location: FF_X114_Y42_N29
\LCD|s_data[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|Mux1~0_combout\,
	ena => \LCD|s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|s_data\(6));

-- Location: LCCOMB_X114_Y41_N14
\LCD|DISPLAY|data~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~26_combout\ = (\LCD|DISPLAY|data~25_combout\ & (((\LCD|DISPLAY|s_busy~2_combout\ & \LCD|s_data\(6))) # (!\LCD|DISPLAY|P_UEBERGANG:c_pos_y~q\))) # (!\LCD|DISPLAY|data~25_combout\ & (((\LCD|DISPLAY|s_busy~2_combout\ & \LCD|s_data\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|data~25_combout\,
	datab => \LCD|DISPLAY|P_UEBERGANG:c_pos_y~q\,
	datac => \LCD|DISPLAY|s_busy~2_combout\,
	datad => \LCD|s_data\(6),
	combout => \LCD|DISPLAY|data~26_combout\);

-- Location: FF_X113_Y42_N7
\LCD|DISPLAY|P_UEBERGANG:data[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|data~27_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:data[6]~q\);

-- Location: LCCOMB_X113_Y42_N12
\LCD|DISPLAY|data~23\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~23_combout\ = (\LCD|DISPLAY|s_state_init_current.Finish~q\) # (!\LCD|DISPLAY|s_state_init_current.Reset~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD|DISPLAY|s_state_init_current.Finish~q\,
	datac => \LCD|DISPLAY|s_state_init_current.Reset~q\,
	combout => \LCD|DISPLAY|data~23_combout\);

-- Location: LCCOMB_X113_Y42_N10
\LCD|DISPLAY|data~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~24_combout\ = (\LCD|DISPLAY|data~9_combout\) # (((\LCD|DISPLAY|data~23_combout\ & \LCD|DISPLAY|s_state_current.Init~q\)) # (!\LCD|DISPLAY|data~12_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|data~23_combout\,
	datab => \LCD|DISPLAY|data~9_combout\,
	datac => \LCD|DISPLAY|s_state_current.Init~q\,
	datad => \LCD|DISPLAY|data~12_combout\,
	combout => \LCD|DISPLAY|data~24_combout\);

-- Location: LCCOMB_X113_Y42_N6
\LCD|DISPLAY|data~27\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~27_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & ((\LCD|DISPLAY|data~26_combout\) # ((\LCD|DISPLAY|P_UEBERGANG:data[6]~q\ & \LCD|DISPLAY|data~24_combout\)))) # (!\LCD|DISPLAY|Equal0~10_combout\ & (((\LCD|DISPLAY|P_UEBERGANG:data[6]~q\ & 
-- \LCD|DISPLAY|data~24_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~10_combout\,
	datab => \LCD|DISPLAY|data~26_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:data[6]~q\,
	datad => \LCD|DISPLAY|data~24_combout\,
	combout => \LCD|DISPLAY|data~27_combout\);

-- Location: LCCOMB_X113_Y42_N26
\LCD|DISPLAY|s_data[6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_data[6]~feeder_combout\ = \LCD|DISPLAY|data~27_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|data~27_combout\,
	combout => \LCD|DISPLAY|s_data[6]~feeder_combout\);

-- Location: FF_X113_Y42_N27
\LCD|DISPLAY|s_data[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_data[6]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_data\(6));

-- Location: FF_X111_Y41_N29
\LCD|DISPLAY|P_UEBERGANG:data[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|data~29_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|P_UEBERGANG:data[7]~q\);

-- Location: LCCOMB_X112_Y42_N22
\LCD|DISPLAY|data~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~28_combout\ = (\LCD|DISPLAY|s_state_current.Send~q\) # (((\LCD|DISPLAY|s_state_current.Init~q\ & \LCD|DISPLAY|data~23_combout\)) # (!\LCD|DISPLAY|data~12_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|s_state_current.Send~q\,
	datab => \LCD|DISPLAY|s_state_current.Init~q\,
	datac => \LCD|DISPLAY|data~23_combout\,
	datad => \LCD|DISPLAY|data~12_combout\,
	combout => \LCD|DISPLAY|data~28_combout\);

-- Location: LCCOMB_X111_Y41_N28
\LCD|DISPLAY|data~29\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|data~29_combout\ = (\LCD|DISPLAY|Equal0~10_combout\ & ((\LCD|DISPLAY|data~25_combout\) # ((\LCD|DISPLAY|P_UEBERGANG:data[7]~q\ & \LCD|DISPLAY|data~28_combout\)))) # (!\LCD|DISPLAY|Equal0~10_combout\ & (((\LCD|DISPLAY|P_UEBERGANG:data[7]~q\ & 
-- \LCD|DISPLAY|data~28_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD|DISPLAY|Equal0~10_combout\,
	datab => \LCD|DISPLAY|data~25_combout\,
	datac => \LCD|DISPLAY|P_UEBERGANG:data[7]~q\,
	datad => \LCD|DISPLAY|data~28_combout\,
	combout => \LCD|DISPLAY|data~29_combout\);

-- Location: LCCOMB_X111_Y41_N24
\LCD|DISPLAY|s_data[7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \LCD|DISPLAY|s_data[7]~feeder_combout\ = \LCD|DISPLAY|data~29_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \LCD|DISPLAY|data~29_combout\,
	combout => \LCD|DISPLAY|s_data[7]~feeder_combout\);

-- Location: FF_X111_Y41_N25
\LCD|DISPLAY|s_data[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \LCD|DISPLAY|s_data[7]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LCD|DISPLAY|s_data\(7));

-- Location: IOIBUF_X115_Y14_N8
\SW[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: LCCOMB_X113_Y40_N2
\Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~0_combout\ = \TIMER:c[0]~q\ $ (VCC)
-- \Add0~1\ = CARRY(\TIMER:c[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[0]~q\,
	datad => VCC,
	combout => \Add0~0_combout\,
	cout => \Add0~1\);

-- Location: LCCOMB_X113_Y40_N0
\c~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~18_combout\ = (!\Equal0~9_combout\ & \Add0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datac => \Add0~0_combout\,
	combout => \c~18_combout\);

-- Location: FF_X113_Y40_N1
\TIMER:c[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~18_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[0]~q\);

-- Location: LCCOMB_X113_Y40_N4
\Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~2_combout\ = (\TIMER:c[1]~q\ & (\Add0~1\ & VCC)) # (!\TIMER:c[1]~q\ & (!\Add0~1\))
-- \Add0~3\ = CARRY((!\TIMER:c[1]~q\ & !\Add0~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[1]~q\,
	datad => VCC,
	cin => \Add0~1\,
	combout => \Add0~2_combout\,
	cout => \Add0~3\);

-- Location: LCCOMB_X114_Y40_N12
\c~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~17_combout\ = (\Add0~2_combout\ & !\Equal0~9_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~2_combout\,
	datad => \Equal0~9_combout\,
	combout => \c~17_combout\);

-- Location: FF_X113_Y40_N3
\TIMER:c[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \c~17_combout\,
	clrn => \nRST~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[1]~q\);

-- Location: LCCOMB_X113_Y40_N6
\Add0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~4_combout\ = (\TIMER:c[2]~q\ & ((GND) # (!\Add0~3\))) # (!\TIMER:c[2]~q\ & (\Add0~3\ $ (GND)))
-- \Add0~5\ = CARRY((\TIMER:c[2]~q\) # (!\Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[2]~q\,
	datad => VCC,
	cin => \Add0~3\,
	combout => \Add0~4_combout\,
	cout => \Add0~5\);

-- Location: LCCOMB_X114_Y40_N10
\c~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~16_combout\ = (\Add0~4_combout\ & !\Equal0~9_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Add0~4_combout\,
	datad => \Equal0~9_combout\,
	combout => \c~16_combout\);

-- Location: FF_X113_Y40_N7
\TIMER:c[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \c~16_combout\,
	clrn => \nRST~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[2]~q\);

-- Location: LCCOMB_X113_Y40_N8
\Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~6_combout\ = (\TIMER:c[3]~q\ & (\Add0~5\ & VCC)) # (!\TIMER:c[3]~q\ & (!\Add0~5\))
-- \Add0~7\ = CARRY((!\TIMER:c[3]~q\ & !\Add0~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[3]~q\,
	datad => VCC,
	cin => \Add0~5\,
	combout => \Add0~6_combout\,
	cout => \Add0~7\);

-- Location: LCCOMB_X114_Y40_N0
\c~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~15_combout\ = (!\Equal0~9_combout\ & \Add0~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datad => \Add0~6_combout\,
	combout => \c~15_combout\);

-- Location: FF_X113_Y40_N5
\TIMER:c[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \c~15_combout\,
	clrn => \nRST~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[3]~q\);

-- Location: LCCOMB_X113_Y40_N10
\Add0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~8_combout\ = (\TIMER:c[4]~q\ & ((GND) # (!\Add0~7\))) # (!\TIMER:c[4]~q\ & (\Add0~7\ $ (GND)))
-- \Add0~9\ = CARRY((\TIMER:c[4]~q\) # (!\Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[4]~q\,
	datad => VCC,
	cin => \Add0~7\,
	combout => \Add0~8_combout\,
	cout => \Add0~9\);

-- Location: FF_X113_Y40_N11
\TIMER:c[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~8_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[4]~q\);

-- Location: LCCOMB_X113_Y40_N12
\Add0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~10_combout\ = (\TIMER:c[5]~q\ & (\Add0~9\ & VCC)) # (!\TIMER:c[5]~q\ & (!\Add0~9\))
-- \Add0~11\ = CARRY((!\TIMER:c[5]~q\ & !\Add0~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[5]~q\,
	datad => VCC,
	cin => \Add0~9\,
	combout => \Add0~10_combout\,
	cout => \Add0~11\);

-- Location: LCCOMB_X114_Y40_N20
\c~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~14_combout\ = (!\Equal0~9_combout\ & \Add0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datac => \Add0~10_combout\,
	combout => \c~14_combout\);

-- Location: FF_X113_Y40_N13
\TIMER:c[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \c~14_combout\,
	clrn => \nRST~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[5]~q\);

-- Location: LCCOMB_X113_Y40_N14
\Add0~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~12_combout\ = (\TIMER:c[6]~q\ & ((GND) # (!\Add0~11\))) # (!\TIMER:c[6]~q\ & (\Add0~11\ $ (GND)))
-- \Add0~13\ = CARRY((\TIMER:c[6]~q\) # (!\Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[6]~q\,
	datad => VCC,
	cin => \Add0~11\,
	combout => \Add0~12_combout\,
	cout => \Add0~13\);

-- Location: LCCOMB_X114_Y40_N6
\c~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~13_combout\ = (!\Equal0~9_combout\ & \Add0~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datad => \Add0~12_combout\,
	combout => \c~13_combout\);

-- Location: FF_X114_Y40_N7
\TIMER:c[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~13_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[6]~q\);

-- Location: LCCOMB_X113_Y40_N16
\Add0~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~14_combout\ = (\TIMER:c[7]~q\ & (\Add0~13\ & VCC)) # (!\TIMER:c[7]~q\ & (!\Add0~13\))
-- \Add0~15\ = CARRY((!\TIMER:c[7]~q\ & !\Add0~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[7]~q\,
	datad => VCC,
	cin => \Add0~13\,
	combout => \Add0~14_combout\,
	cout => \Add0~15\);

-- Location: LCCOMB_X114_Y40_N26
\c~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~12_combout\ = (!\Equal0~9_combout\ & \Add0~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datad => \Add0~14_combout\,
	combout => \c~12_combout\);

-- Location: FF_X114_Y40_N27
\TIMER:c[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~12_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[7]~q\);

-- Location: LCCOMB_X113_Y40_N18
\Add0~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~16_combout\ = (\TIMER:c[8]~q\ & ((GND) # (!\Add0~15\))) # (!\TIMER:c[8]~q\ & (\Add0~15\ $ (GND)))
-- \Add0~17\ = CARRY((\TIMER:c[8]~q\) # (!\Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[8]~q\,
	datad => VCC,
	cin => \Add0~15\,
	combout => \Add0~16_combout\,
	cout => \Add0~17\);

-- Location: LCCOMB_X114_Y40_N28
\c~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~11_combout\ = (!\Equal0~9_combout\ & \Add0~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datad => \Add0~16_combout\,
	combout => \c~11_combout\);

-- Location: FF_X114_Y40_N29
\TIMER:c[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~11_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[8]~q\);

-- Location: LCCOMB_X113_Y40_N20
\Add0~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~18_combout\ = (\TIMER:c[9]~q\ & (\Add0~17\ & VCC)) # (!\TIMER:c[9]~q\ & (!\Add0~17\))
-- \Add0~19\ = CARRY((!\TIMER:c[9]~q\ & !\Add0~17\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[9]~q\,
	datad => VCC,
	cin => \Add0~17\,
	combout => \Add0~18_combout\,
	cout => \Add0~19\);

-- Location: FF_X113_Y40_N21
\TIMER:c[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~18_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[9]~q\);

-- Location: LCCOMB_X113_Y40_N22
\Add0~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~20_combout\ = (\TIMER:c[10]~q\ & ((GND) # (!\Add0~19\))) # (!\TIMER:c[10]~q\ & (\Add0~19\ $ (GND)))
-- \Add0~21\ = CARRY((\TIMER:c[10]~q\) # (!\Add0~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[10]~q\,
	datad => VCC,
	cin => \Add0~19\,
	combout => \Add0~20_combout\,
	cout => \Add0~21\);

-- Location: FF_X113_Y40_N23
\TIMER:c[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~20_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[10]~q\);

-- Location: LCCOMB_X114_Y40_N22
\Equal0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~6_combout\ = (!\TIMER:c[7]~q\ & (!\TIMER:c[8]~q\ & (!\TIMER:c[9]~q\ & !\TIMER:c[10]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[7]~q\,
	datab => \TIMER:c[8]~q\,
	datac => \TIMER:c[9]~q\,
	datad => \TIMER:c[10]~q\,
	combout => \Equal0~6_combout\);

-- Location: LCCOMB_X113_Y40_N24
\Add0~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~22_combout\ = (\TIMER:c[11]~q\ & (\Add0~21\ & VCC)) # (!\TIMER:c[11]~q\ & (!\Add0~21\))
-- \Add0~23\ = CARRY((!\TIMER:c[11]~q\ & !\Add0~21\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[11]~q\,
	datad => VCC,
	cin => \Add0~21\,
	combout => \Add0~22_combout\,
	cout => \Add0~23\);

-- Location: FF_X113_Y40_N25
\TIMER:c[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~22_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[11]~q\);

-- Location: LCCOMB_X113_Y40_N26
\Add0~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~24_combout\ = (\TIMER:c[12]~q\ & ((GND) # (!\Add0~23\))) # (!\TIMER:c[12]~q\ & (\Add0~23\ $ (GND)))
-- \Add0~25\ = CARRY((\TIMER:c[12]~q\) # (!\Add0~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[12]~q\,
	datad => VCC,
	cin => \Add0~23\,
	combout => \Add0~24_combout\,
	cout => \Add0~25\);

-- Location: FF_X113_Y40_N27
\TIMER:c[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~24_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[12]~q\);

-- Location: LCCOMB_X113_Y40_N28
\Add0~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~26_combout\ = (\TIMER:c[13]~q\ & (\Add0~25\ & VCC)) # (!\TIMER:c[13]~q\ & (!\Add0~25\))
-- \Add0~27\ = CARRY((!\TIMER:c[13]~q\ & !\Add0~25\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[13]~q\,
	datad => VCC,
	cin => \Add0~25\,
	combout => \Add0~26_combout\,
	cout => \Add0~27\);

-- Location: LCCOMB_X114_Y40_N4
\c~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~10_combout\ = (!\Equal0~9_combout\ & \Add0~26_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datad => \Add0~26_combout\,
	combout => \c~10_combout\);

-- Location: FF_X114_Y40_N5
\TIMER:c[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~10_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[13]~q\);

-- Location: LCCOMB_X113_Y40_N30
\Add0~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~28_combout\ = (\TIMER:c[14]~q\ & ((GND) # (!\Add0~27\))) # (!\TIMER:c[14]~q\ & (\Add0~27\ $ (GND)))
-- \Add0~29\ = CARRY((\TIMER:c[14]~q\) # (!\Add0~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[14]~q\,
	datad => VCC,
	cin => \Add0~27\,
	combout => \Add0~28_combout\,
	cout => \Add0~29\);

-- Location: FF_X113_Y40_N31
\TIMER:c[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~28_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[14]~q\);

-- Location: LCCOMB_X114_Y40_N18
\Equal0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~5_combout\ = (!\TIMER:c[12]~q\ & (!\TIMER:c[14]~q\ & (!\TIMER:c[13]~q\ & !\TIMER:c[11]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[12]~q\,
	datab => \TIMER:c[14]~q\,
	datac => \TIMER:c[13]~q\,
	datad => \TIMER:c[11]~q\,
	combout => \Equal0~5_combout\);

-- Location: LCCOMB_X114_Y40_N16
\Equal0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~7_combout\ = (!\TIMER:c[4]~q\ & (!\TIMER:c[6]~q\ & (!\TIMER:c[3]~q\ & !\TIMER:c[5]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[4]~q\,
	datab => \TIMER:c[6]~q\,
	datac => \TIMER:c[3]~q\,
	datad => \TIMER:c[5]~q\,
	combout => \Equal0~7_combout\);

-- Location: LCCOMB_X114_Y40_N30
\Equal0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~8_combout\ = (!\TIMER:c[2]~q\ & (!\TIMER:c[1]~q\ & (!\TIMER:c[0]~q\ & \Equal0~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[2]~q\,
	datab => \TIMER:c[1]~q\,
	datac => \TIMER:c[0]~q\,
	datad => \Equal0~7_combout\,
	combout => \Equal0~8_combout\);

-- Location: LCCOMB_X113_Y39_N0
\Add0~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~30_combout\ = (\TIMER:c[15]~q\ & (\Add0~29\ & VCC)) # (!\TIMER:c[15]~q\ & (!\Add0~29\))
-- \Add0~31\ = CARRY((!\TIMER:c[15]~q\ & !\Add0~29\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[15]~q\,
	datad => VCC,
	cin => \Add0~29\,
	combout => \Add0~30_combout\,
	cout => \Add0~31\);

-- Location: LCCOMB_X114_Y39_N20
\c~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~9_combout\ = (!\Equal0~9_combout\ & \Add0~30_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datad => \Add0~30_combout\,
	combout => \c~9_combout\);

-- Location: FF_X114_Y39_N21
\TIMER:c[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~9_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[15]~q\);

-- Location: LCCOMB_X113_Y39_N2
\Add0~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~32_combout\ = (\TIMER:c[16]~q\ & ((GND) # (!\Add0~31\))) # (!\TIMER:c[16]~q\ & (\Add0~31\ $ (GND)))
-- \Add0~33\ = CARRY((\TIMER:c[16]~q\) # (!\Add0~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[16]~q\,
	datad => VCC,
	cin => \Add0~31\,
	combout => \Add0~32_combout\,
	cout => \Add0~33\);

-- Location: FF_X113_Y39_N3
\TIMER:c[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~32_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[16]~q\);

-- Location: LCCOMB_X113_Y39_N4
\Add0~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~34_combout\ = (\TIMER:c[17]~q\ & (\Add0~33\ & VCC)) # (!\TIMER:c[17]~q\ & (!\Add0~33\))
-- \Add0~35\ = CARRY((!\TIMER:c[17]~q\ & !\Add0~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[17]~q\,
	datad => VCC,
	cin => \Add0~33\,
	combout => \Add0~34_combout\,
	cout => \Add0~35\);

-- Location: FF_X113_Y39_N5
\TIMER:c[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~34_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[17]~q\);

-- Location: LCCOMB_X113_Y39_N6
\Add0~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~36_combout\ = (\TIMER:c[18]~q\ & ((GND) # (!\Add0~35\))) # (!\TIMER:c[18]~q\ & (\Add0~35\ $ (GND)))
-- \Add0~37\ = CARRY((\TIMER:c[18]~q\) # (!\Add0~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[18]~q\,
	datad => VCC,
	cin => \Add0~35\,
	combout => \Add0~36_combout\,
	cout => \Add0~37\);

-- Location: FF_X113_Y39_N7
\TIMER:c[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~36_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[18]~q\);

-- Location: LCCOMB_X113_Y39_N8
\Add0~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~38_combout\ = (\TIMER:c[19]~q\ & (\Add0~37\ & VCC)) # (!\TIMER:c[19]~q\ & (!\Add0~37\))
-- \Add0~39\ = CARRY((!\TIMER:c[19]~q\ & !\Add0~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[19]~q\,
	datad => VCC,
	cin => \Add0~37\,
	combout => \Add0~38_combout\,
	cout => \Add0~39\);

-- Location: FF_X113_Y39_N9
\TIMER:c[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~38_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[19]~q\);

-- Location: LCCOMB_X113_Y39_N10
\Add0~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~40_combout\ = (\TIMER:c[20]~q\ & ((GND) # (!\Add0~39\))) # (!\TIMER:c[20]~q\ & (\Add0~39\ $ (GND)))
-- \Add0~41\ = CARRY((\TIMER:c[20]~q\) # (!\Add0~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[20]~q\,
	datad => VCC,
	cin => \Add0~39\,
	combout => \Add0~40_combout\,
	cout => \Add0~41\);

-- Location: FF_X113_Y39_N11
\TIMER:c[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~40_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[20]~q\);

-- Location: LCCOMB_X113_Y39_N12
\Add0~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~42_combout\ = (\TIMER:c[21]~q\ & (\Add0~41\ & VCC)) # (!\TIMER:c[21]~q\ & (!\Add0~41\))
-- \Add0~43\ = CARRY((!\TIMER:c[21]~q\ & !\Add0~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[21]~q\,
	datad => VCC,
	cin => \Add0~41\,
	combout => \Add0~42_combout\,
	cout => \Add0~43\);

-- Location: LCCOMB_X114_Y39_N4
\c~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~8_combout\ = (!\Equal0~9_combout\ & \Add0~42_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datad => \Add0~42_combout\,
	combout => \c~8_combout\);

-- Location: FF_X114_Y39_N5
\TIMER:c[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~8_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[21]~q\);

-- Location: LCCOMB_X113_Y39_N14
\Add0~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~44_combout\ = (\TIMER:c[22]~q\ & ((GND) # (!\Add0~43\))) # (!\TIMER:c[22]~q\ & (\Add0~43\ $ (GND)))
-- \Add0~45\ = CARRY((\TIMER:c[22]~q\) # (!\Add0~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[22]~q\,
	datad => VCC,
	cin => \Add0~43\,
	combout => \Add0~44_combout\,
	cout => \Add0~45\);

-- Location: FF_X113_Y39_N15
\TIMER:c[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~44_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[22]~q\);

-- Location: LCCOMB_X114_Y39_N10
\Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = (!\TIMER:c[19]~q\ & (!\TIMER:c[21]~q\ & (!\TIMER:c[20]~q\ & !\TIMER:c[22]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[19]~q\,
	datab => \TIMER:c[21]~q\,
	datac => \TIMER:c[20]~q\,
	datad => \TIMER:c[22]~q\,
	combout => \Equal0~2_combout\);

-- Location: LCCOMB_X113_Y39_N16
\Add0~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~46_combout\ = (\TIMER:c[23]~q\ & (\Add0~45\ & VCC)) # (!\TIMER:c[23]~q\ & (!\Add0~45\))
-- \Add0~47\ = CARRY((!\TIMER:c[23]~q\ & !\Add0~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[23]~q\,
	datad => VCC,
	cin => \Add0~45\,
	combout => \Add0~46_combout\,
	cout => \Add0~47\);

-- Location: LCCOMB_X114_Y39_N8
\c~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~7_combout\ = (\Add0~46_combout\ & !\Equal0~9_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~46_combout\,
	datad => \Equal0~9_combout\,
	combout => \c~7_combout\);

-- Location: FF_X114_Y39_N9
\TIMER:c[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~7_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[23]~q\);

-- Location: LCCOMB_X113_Y39_N18
\Add0~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~48_combout\ = (\TIMER:c[24]~q\ & ((GND) # (!\Add0~47\))) # (!\TIMER:c[24]~q\ & (\Add0~47\ $ (GND)))
-- \Add0~49\ = CARRY((\TIMER:c[24]~q\) # (!\Add0~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[24]~q\,
	datad => VCC,
	cin => \Add0~47\,
	combout => \Add0~48_combout\,
	cout => \Add0~49\);

-- Location: LCCOMB_X114_Y39_N6
\c~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~6_combout\ = (!\Equal0~9_combout\ & \Add0~48_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datad => \Add0~48_combout\,
	combout => \c~6_combout\);

-- Location: FF_X114_Y39_N7
\TIMER:c[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~6_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[24]~q\);

-- Location: LCCOMB_X113_Y39_N20
\Add0~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~50_combout\ = (\TIMER:c[25]~q\ & (\Add0~49\ & VCC)) # (!\TIMER:c[25]~q\ & (!\Add0~49\))
-- \Add0~51\ = CARRY((!\TIMER:c[25]~q\ & !\Add0~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[25]~q\,
	datad => VCC,
	cin => \Add0~49\,
	combout => \Add0~50_combout\,
	cout => \Add0~51\);

-- Location: LCCOMB_X114_Y39_N12
\c~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~5_combout\ = (!\Equal0~9_combout\ & \Add0~50_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datad => \Add0~50_combout\,
	combout => \c~5_combout\);

-- Location: FF_X114_Y39_N13
\TIMER:c[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~5_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[25]~q\);

-- Location: LCCOMB_X113_Y39_N22
\Add0~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~52_combout\ = (\TIMER:c[26]~q\ & ((GND) # (!\Add0~51\))) # (!\TIMER:c[26]~q\ & (\Add0~51\ $ (GND)))
-- \Add0~53\ = CARRY((\TIMER:c[26]~q\) # (!\Add0~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[26]~q\,
	datad => VCC,
	cin => \Add0~51\,
	combout => \Add0~52_combout\,
	cout => \Add0~53\);

-- Location: LCCOMB_X114_Y39_N14
\c~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~4_combout\ = (!\Equal0~9_combout\ & \Add0~52_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datad => \Add0~52_combout\,
	combout => \c~4_combout\);

-- Location: FF_X114_Y39_N15
\TIMER:c[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~4_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[26]~q\);

-- Location: LCCOMB_X114_Y39_N22
\Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = (!\TIMER:c[24]~q\ & (!\TIMER:c[26]~q\ & (!\TIMER:c[23]~q\ & !\TIMER:c[25]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[24]~q\,
	datab => \TIMER:c[26]~q\,
	datac => \TIMER:c[23]~q\,
	datad => \TIMER:c[25]~q\,
	combout => \Equal0~1_combout\);

-- Location: LCCOMB_X114_Y39_N26
\Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~3_combout\ = (!\TIMER:c[16]~q\ & (!\TIMER:c[18]~q\ & (!\TIMER:c[17]~q\ & !\TIMER:c[15]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[16]~q\,
	datab => \TIMER:c[18]~q\,
	datac => \TIMER:c[17]~q\,
	datad => \TIMER:c[15]~q\,
	combout => \Equal0~3_combout\);

-- Location: LCCOMB_X113_Y39_N24
\Add0~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~54_combout\ = (\TIMER:c[27]~q\ & (\Add0~53\ & VCC)) # (!\TIMER:c[27]~q\ & (!\Add0~53\))
-- \Add0~55\ = CARRY((!\TIMER:c[27]~q\ & !\Add0~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[27]~q\,
	datad => VCC,
	cin => \Add0~53\,
	combout => \Add0~54_combout\,
	cout => \Add0~55\);

-- Location: LCCOMB_X114_Y39_N30
\c~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~3_combout\ = (!\Equal0~9_combout\ & \Add0~54_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datad => \Add0~54_combout\,
	combout => \c~3_combout\);

-- Location: FF_X114_Y39_N31
\TIMER:c[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~3_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[27]~q\);

-- Location: LCCOMB_X113_Y39_N26
\Add0~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~56_combout\ = (\TIMER:c[28]~q\ & ((GND) # (!\Add0~55\))) # (!\TIMER:c[28]~q\ & (\Add0~55\ $ (GND)))
-- \Add0~57\ = CARRY((\TIMER:c[28]~q\) # (!\Add0~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[28]~q\,
	datad => VCC,
	cin => \Add0~55\,
	combout => \Add0~56_combout\,
	cout => \Add0~57\);

-- Location: LCCOMB_X114_Y39_N28
\c~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~2_combout\ = (\Add0~56_combout\ & !\Equal0~9_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~56_combout\,
	datad => \Equal0~9_combout\,
	combout => \c~2_combout\);

-- Location: FF_X114_Y39_N29
\TIMER:c[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~2_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[28]~q\);

-- Location: LCCOMB_X113_Y39_N28
\Add0~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~58_combout\ = (\TIMER:c[29]~q\ & (\Add0~57\ & VCC)) # (!\TIMER:c[29]~q\ & (!\Add0~57\))
-- \Add0~59\ = CARRY((!\TIMER:c[29]~q\ & !\Add0~57\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TIMER:c[29]~q\,
	datad => VCC,
	cin => \Add0~57\,
	combout => \Add0~58_combout\,
	cout => \Add0~59\);

-- Location: LCCOMB_X114_Y39_N18
\c~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~1_combout\ = (!\Equal0~9_combout\ & \Add0~58_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datad => \Add0~58_combout\,
	combout => \c~1_combout\);

-- Location: FF_X114_Y39_N19
\TIMER:c[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~1_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[29]~q\);

-- Location: LCCOMB_X113_Y39_N30
\Add0~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~60_combout\ = \Add0~59\ $ (\TIMER:c[30]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \TIMER:c[30]~q\,
	cin => \Add0~59\,
	combout => \Add0~60_combout\);

-- Location: LCCOMB_X114_Y39_N24
\c~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \c~0_combout\ = (!\Equal0~9_combout\ & \Add0~60_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~9_combout\,
	datad => \Add0~60_combout\,
	combout => \c~0_combout\);

-- Location: FF_X114_Y39_N25
\TIMER:c[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \c~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TIMER:c[30]~q\);

-- Location: LCCOMB_X114_Y39_N16
\Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = (!\TIMER:c[30]~q\ & (!\TIMER:c[29]~q\ & (!\TIMER:c[27]~q\ & !\TIMER:c[28]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER:c[30]~q\,
	datab => \TIMER:c[29]~q\,
	datac => \TIMER:c[27]~q\,
	datad => \TIMER:c[28]~q\,
	combout => \Equal0~0_combout\);

-- Location: LCCOMB_X114_Y39_N0
\Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~4_combout\ = (\Equal0~2_combout\ & (\Equal0~1_combout\ & (\Equal0~3_combout\ & \Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~2_combout\,
	datab => \Equal0~1_combout\,
	datac => \Equal0~3_combout\,
	datad => \Equal0~0_combout\,
	combout => \Equal0~4_combout\);

-- Location: LCCOMB_X114_Y40_N24
\Equal0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~9_combout\ = (\Equal0~6_combout\ & (\Equal0~5_combout\ & (\Equal0~8_combout\ & \Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~6_combout\,
	datab => \Equal0~5_combout\,
	datac => \Equal0~8_combout\,
	datad => \Equal0~4_combout\,
	combout => \Equal0~9_combout\);

-- Location: LCCOMB_X114_Y40_N8
\s_clk~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_clk~0_combout\ = \s_clk~q\ $ (((\nRST~input_o\ & \Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datac => \s_clk~q\,
	datad => \Equal0~9_combout\,
	combout => \s_clk~0_combout\);

-- Location: LCCOMB_X114_Y40_N2
\s_clk~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_clk~feeder_combout\ = \s_clk~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_clk~0_combout\,
	combout => \s_clk~feeder_combout\);

-- Location: FF_X114_Y40_N3
s_clk : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_clk~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_clk~q\);

-- Location: CLKCTRL_G7
\s_clk~clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \s_clk~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \s_clk~clkctrl_outclk\);

-- Location: IOIBUF_X115_Y13_N1
\SW[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: CLKCTRL_G8
\s_rst~clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \s_rst~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \s_rst~clkctrl_outclk\);

-- Location: LCCOMB_X111_Y40_N22
\COUNT|Count:c[2]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Count:c[2]~1_combout\ = (GLOBAL(\s_rst~clkctrl_outclk\) & (\SW[2]~input_o\)) # (!GLOBAL(\s_rst~clkctrl_outclk\) & ((\COUNT|Count:c[2]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[2]~input_o\,
	datac => \s_rst~clkctrl_outclk\,
	datad => \COUNT|Count:c[2]~1_combout\,
	combout => \COUNT|Count:c[2]~1_combout\);

-- Location: IOIBUF_X115_Y10_N8
\SW[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: LCCOMB_X111_Y40_N18
\COUNT|Count:c[0]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Count:c[0]~1_combout\ = (GLOBAL(\s_rst~clkctrl_outclk\) & (\SW[0]~input_o\)) # (!GLOBAL(\s_rst~clkctrl_outclk\) & ((\COUNT|Count:c[0]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[0]~input_o\,
	datac => \s_rst~clkctrl_outclk\,
	datad => \COUNT|Count:c[0]~1_combout\,
	combout => \COUNT|Count:c[0]~1_combout\);

-- Location: LCCOMB_X111_Y40_N4
\COUNT|Count:c[0]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Count:c[0]~3_combout\ = \COUNT|Count:c[0]~1_combout\ $ (((!\COUNT|Count:c[0]~2_combout\ & ((\COUNT|Count:c[3]~2_combout\) # (!\COUNT|Equal0~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001011000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[0]~2_combout\,
	datac => \COUNT|Count:c[0]~1_combout\,
	datad => \COUNT|Equal0~0_combout\,
	combout => \COUNT|Count:c[0]~3_combout\);

-- Location: FF_X111_Y40_N5
\COUNT|Count:c[0]~_emulated\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_clk~clkctrl_outclk\,
	d => \COUNT|Count:c[0]~3_combout\,
	clrn => \ALT_INV_s_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT|Count:c[0]~_emulated_q\);

-- Location: LCCOMB_X111_Y40_N16
\COUNT|Count:c[0]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Count:c[0]~2_combout\ = (\s_rst~combout\ & (\SW[0]~input_o\)) # (!\s_rst~combout\ & ((\COUNT|Count:c[0]~_emulated_q\ $ (\COUNT|Count:c[0]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[0]~input_o\,
	datab => \s_rst~combout\,
	datac => \COUNT|Count:c[0]~_emulated_q\,
	datad => \COUNT|Count:c[0]~1_combout\,
	combout => \COUNT|Count:c[0]~2_combout\);

-- Location: IOIBUF_X115_Y6_N15
\SW[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: LCCOMB_X111_Y40_N12
\COUNT|Count:c[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Count:c[1]~1_combout\ = (GLOBAL(\s_rst~clkctrl_outclk\) & (\SW[1]~input_o\)) # (!GLOBAL(\s_rst~clkctrl_outclk\) & ((\COUNT|Count:c[1]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[1]~input_o\,
	datac => \s_rst~clkctrl_outclk\,
	datad => \COUNT|Count:c[1]~1_combout\,
	combout => \COUNT|Count:c[1]~1_combout\);

-- Location: LCCOMB_X111_Y40_N24
\COUNT|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Add0~0_combout\ = (\COUNT|Count:c[0]~2_combout\ & (\COUNT|Count:c[1]~2_combout\ $ (VCC))) # (!\COUNT|Count:c[0]~2_combout\ & (\COUNT|Count:c[1]~2_combout\ & VCC))
-- \COUNT|Add0~1\ = CARRY((\COUNT|Count:c[0]~2_combout\ & \COUNT|Count:c[1]~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[0]~2_combout\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datad => VCC,
	combout => \COUNT|Add0~0_combout\,
	cout => \COUNT|Add0~1\);

-- Location: LCCOMB_X111_Y40_N8
\COUNT|Count:c[1]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Count:c[1]~3_combout\ = \COUNT|Count:c[1]~1_combout\ $ (((\COUNT|Add0~0_combout\ & ((\COUNT|Count:c[3]~2_combout\) # (!\COUNT|Equal0~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[1]~1_combout\,
	datac => \COUNT|Equal0~0_combout\,
	datad => \COUNT|Add0~0_combout\,
	combout => \COUNT|Count:c[1]~3_combout\);

-- Location: FF_X111_Y40_N9
\COUNT|Count:c[1]~_emulated\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_clk~clkctrl_outclk\,
	d => \COUNT|Count:c[1]~3_combout\,
	clrn => \ALT_INV_s_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT|Count:c[1]~_emulated_q\);

-- Location: LCCOMB_X111_Y40_N2
\COUNT|Count:c[1]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Count:c[1]~2_combout\ = (\s_rst~combout\ & (\SW[1]~input_o\)) # (!\s_rst~combout\ & ((\COUNT|Count:c[1]~_emulated_q\ $ (\COUNT|Count:c[1]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001110101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[1]~input_o\,
	datab => \COUNT|Count:c[1]~_emulated_q\,
	datac => \s_rst~combout\,
	datad => \COUNT|Count:c[1]~1_combout\,
	combout => \COUNT|Count:c[1]~2_combout\);

-- Location: LCCOMB_X111_Y40_N26
\COUNT|Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Add0~2_combout\ = (\COUNT|Count:c[2]~2_combout\ & (!\COUNT|Add0~1\)) # (!\COUNT|Count:c[2]~2_combout\ & ((\COUNT|Add0~1\) # (GND)))
-- \COUNT|Add0~3\ = CARRY((!\COUNT|Add0~1\) # (!\COUNT|Count:c[2]~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[2]~2_combout\,
	datad => VCC,
	cin => \COUNT|Add0~1\,
	combout => \COUNT|Add0~2_combout\,
	cout => \COUNT|Add0~3\);

-- Location: LCCOMB_X111_Y40_N20
\COUNT|Count:c[2]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Count:c[2]~3_combout\ = \COUNT|Count:c[2]~1_combout\ $ (((\COUNT|Add0~2_combout\ & ((\COUNT|Count:c[3]~2_combout\) # (!\COUNT|Equal0~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001110001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Add0~2_combout\,
	datac => \COUNT|Equal0~0_combout\,
	datad => \COUNT|Count:c[2]~1_combout\,
	combout => \COUNT|Count:c[2]~3_combout\);

-- Location: FF_X111_Y40_N21
\COUNT|Count:c[2]~_emulated\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_clk~clkctrl_outclk\,
	d => \COUNT|Count:c[2]~3_combout\,
	clrn => \ALT_INV_s_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT|Count:c[2]~_emulated_q\);

-- Location: LCCOMB_X111_Y40_N14
\COUNT|Count:c[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Count:c[2]~2_combout\ = (\s_rst~combout\ & (\SW[2]~input_o\)) # (!\s_rst~combout\ & ((\COUNT|Count:c[2]~1_combout\ $ (\COUNT|Count:c[2]~_emulated_q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[2]~input_o\,
	datab => \s_rst~combout\,
	datac => \COUNT|Count:c[2]~1_combout\,
	datad => \COUNT|Count:c[2]~_emulated_q\,
	combout => \COUNT|Count:c[2]~2_combout\);

-- Location: LCCOMB_X111_Y40_N28
\COUNT|Add0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Add0~4_combout\ = (\COUNT|Count:c[3]~2_combout\ & (\COUNT|Add0~3\ $ (GND))) # (!\COUNT|Count:c[3]~2_combout\ & (!\COUNT|Add0~3\ & VCC))
-- \COUNT|Add0~5\ = CARRY((\COUNT|Count:c[3]~2_combout\ & !\COUNT|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \COUNT|Count:c[3]~2_combout\,
	datad => VCC,
	cin => \COUNT|Add0~3\,
	combout => \COUNT|Add0~4_combout\,
	cout => \COUNT|Add0~5\);

-- Location: LCCOMB_X111_Y40_N30
\COUNT|Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Add0~6_combout\ = \COUNT|Add0~5\ $ (\COUNT|Count:c[4]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \COUNT|Count:c[4]~q\,
	cin => \COUNT|Add0~5\,
	combout => \COUNT|Add0~6_combout\);

-- Location: LCCOMB_X111_Y40_N10
\COUNT|Count:c[4]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Count:c[4]~0_combout\ = (\COUNT|Count:c[3]~2_combout\ & (((\COUNT|Add0~6_combout\)))) # (!\COUNT|Count:c[3]~2_combout\ & ((\COUNT|Equal0~0_combout\ & (\COUNT|Count:c[4]~q\)) # (!\COUNT|Equal0~0_combout\ & ((\COUNT|Add0~6_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Equal0~0_combout\,
	datac => \COUNT|Count:c[4]~q\,
	datad => \COUNT|Add0~6_combout\,
	combout => \COUNT|Count:c[4]~0_combout\);

-- Location: FF_X111_Y40_N11
\COUNT|Count:c[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_clk~clkctrl_outclk\,
	d => \COUNT|Count:c[4]~0_combout\,
	clrn => \ALT_INV_s_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT|Count:c[4]~q\);

-- Location: LCCOMB_X111_Y40_N0
\COUNT|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Equal0~0_combout\ = (\COUNT|Count:c[4]~q\ & (!\COUNT|Count:c[1]~2_combout\ & (!\COUNT|Count:c[2]~2_combout\ & !\COUNT|Count:c[0]~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[4]~q\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datac => \COUNT|Count:c[2]~2_combout\,
	datad => \COUNT|Count:c[0]~2_combout\,
	combout => \COUNT|Equal0~0_combout\);

-- Location: LCCOMB_X111_Y40_N6
\COUNT|s_ov~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|s_ov~0_combout\ = (\COUNT|s_ov~q\) # ((!\COUNT|Count:c[3]~2_combout\ & \COUNT|Equal0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \COUNT|Count:c[3]~2_combout\,
	datac => \COUNT|s_ov~q\,
	datad => \COUNT|Equal0~0_combout\,
	combout => \COUNT|s_ov~0_combout\);

-- Location: FF_X111_Y40_N7
\COUNT|s_ov\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_clk~q\,
	d => \COUNT|s_ov~0_combout\,
	clrn => \ALT_INV_s_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT|s_ov~q\);

-- Location: LCCOMB_X114_Y40_N14
s_rst : cycloneive_lcell_comb
-- Equation(s):
-- \s_rst~combout\ = \COUNT|s_ov~q\ $ (!\nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \COUNT|s_ov~q\,
	datad => \nRST~input_o\,
	combout => \s_rst~combout\);

-- Location: LCCOMB_X112_Y40_N24
\COUNT|Count:c[3]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Count:c[3]~1_combout\ = (GLOBAL(\s_rst~clkctrl_outclk\) & (\SW[3]~input_o\)) # (!GLOBAL(\s_rst~clkctrl_outclk\) & ((\COUNT|Count:c[3]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[3]~input_o\,
	datab => \COUNT|Count:c[3]~1_combout\,
	datad => \s_rst~clkctrl_outclk\,
	combout => \COUNT|Count:c[3]~1_combout\);

-- Location: LCCOMB_X112_Y40_N8
\COUNT|Count:c[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Count:c[3]~3_combout\ = \COUNT|Count:c[3]~1_combout\ $ (((\COUNT|Add0~4_combout\ & ((\COUNT|Count:c[3]~2_combout\) # (!\COUNT|Equal0~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011100111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Equal0~0_combout\,
	datab => \COUNT|Count:c[3]~1_combout\,
	datac => \COUNT|Count:c[3]~2_combout\,
	datad => \COUNT|Add0~4_combout\,
	combout => \COUNT|Count:c[3]~3_combout\);

-- Location: FF_X112_Y40_N9
\COUNT|Count:c[3]~_emulated\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_s_clk~clkctrl_outclk\,
	d => \COUNT|Count:c[3]~3_combout\,
	clrn => \ALT_INV_s_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT|Count:c[3]~_emulated_q\);

-- Location: LCCOMB_X112_Y40_N22
\COUNT|Count:c[3]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \COUNT|Count:c[3]~2_combout\ = (\s_rst~combout\ & (\SW[3]~input_o\)) # (!\s_rst~combout\ & ((\COUNT|Count:c[3]~_emulated_q\ $ (\COUNT|Count:c[3]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[3]~input_o\,
	datab => \s_rst~combout\,
	datac => \COUNT|Count:c[3]~_emulated_q\,
	datad => \COUNT|Count:c[3]~1_combout\,
	combout => \COUNT|Count:c[3]~2_combout\);

-- Location: LCCOMB_X107_Y72_N4
\GRAY|Mux3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \GRAY|Mux3~0_combout\ = (\COUNT|Count:c[3]~2_combout\ & (!\COUNT|Count:c[1]~2_combout\ & (\COUNT|Count:c[0]~2_combout\ & !\COUNT|Count:c[2]~2_combout\))) # (!\COUNT|Count:c[3]~2_combout\ & (\COUNT|Count:c[1]~2_combout\ $ ((\COUNT|Count:c[0]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010000110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datac => \COUNT|Count:c[0]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \GRAY|Mux3~0_combout\);

-- Location: LCCOMB_X107_Y72_N2
\GRAY|Mux2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \GRAY|Mux2~0_combout\ = (!\COUNT|Count:c[3]~2_combout\ & (\COUNT|Count:c[1]~2_combout\ $ (\COUNT|Count:c[2]~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datac => \COUNT|Count:c[1]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \GRAY|Mux2~0_combout\);

-- Location: LCCOMB_X107_Y72_N12
\GRAY|Mux1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \GRAY|Mux1~0_combout\ = (\COUNT|Count:c[3]~2_combout\ & (!\COUNT|Count:c[1]~2_combout\ & !\COUNT|Count:c[2]~2_combout\)) # (!\COUNT|Count:c[3]~2_combout\ & ((\COUNT|Count:c[2]~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datac => \COUNT|Count:c[1]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \GRAY|Mux1~0_combout\);

-- Location: LCCOMB_X107_Y72_N22
\GRAY|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \GRAY|Mux0~0_combout\ = (\COUNT|Count:c[3]~2_combout\ & (!\COUNT|Count:c[1]~2_combout\ & !\COUNT|Count:c[2]~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datac => \COUNT|Count:c[1]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \GRAY|Mux0~0_combout\);

-- Location: LCCOMB_X107_Y72_N20
\AIKEN|Mux3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \AIKEN|Mux3~0_combout\ = (\COUNT|Count:c[0]~2_combout\ & (((!\COUNT|Count:c[1]~2_combout\ & !\COUNT|Count:c[2]~2_combout\)) # (!\COUNT|Count:c[3]~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datac => \COUNT|Count:c[0]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \AIKEN|Mux3~0_combout\);

-- Location: LCCOMB_X107_Y72_N26
\AIKEN|Mux2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \AIKEN|Mux2~0_combout\ = (\COUNT|Count:c[2]~2_combout\ & (!\COUNT|Count:c[3]~2_combout\ & (!\COUNT|Count:c[1]~2_combout\ & \COUNT|Count:c[0]~2_combout\))) # (!\COUNT|Count:c[2]~2_combout\ & (\COUNT|Count:c[3]~2_combout\ $ 
-- ((\COUNT|Count:c[1]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000001100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datac => \COUNT|Count:c[0]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \AIKEN|Mux2~0_combout\);

-- Location: LCCOMB_X107_Y72_N24
\AIKEN|Mux1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \AIKEN|Mux1~0_combout\ = (\COUNT|Count:c[3]~2_combout\ & (!\COUNT|Count:c[1]~2_combout\ & ((!\COUNT|Count:c[2]~2_combout\)))) # (!\COUNT|Count:c[3]~2_combout\ & (\COUNT|Count:c[2]~2_combout\ & ((\COUNT|Count:c[1]~2_combout\) # 
-- (!\COUNT|Count:c[0]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datac => \COUNT|Count:c[0]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \AIKEN|Mux1~0_combout\);

-- Location: LCCOMB_X107_Y72_N18
\AIKEN|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \AIKEN|Mux0~0_combout\ = (\COUNT|Count:c[3]~2_combout\ & (!\COUNT|Count:c[1]~2_combout\ & ((!\COUNT|Count:c[2]~2_combout\)))) # (!\COUNT|Count:c[3]~2_combout\ & (\COUNT|Count:c[2]~2_combout\ & ((\COUNT|Count:c[1]~2_combout\) # 
-- (\COUNT|Count:c[0]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datac => \COUNT|Count:c[0]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \AIKEN|Mux0~0_combout\);

-- Location: LCCOMB_X107_Y72_N0
\SEGDISP|Mux6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \SEGDISP|Mux6~0_combout\ = (\COUNT|Count:c[3]~2_combout\ & (\COUNT|Count:c[0]~2_combout\ & (\COUNT|Count:c[1]~2_combout\ $ (\COUNT|Count:c[2]~2_combout\)))) # (!\COUNT|Count:c[3]~2_combout\ & (!\COUNT|Count:c[1]~2_combout\ & (\COUNT|Count:c[0]~2_combout\ 
-- $ (\COUNT|Count:c[2]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datac => \COUNT|Count:c[0]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \SEGDISP|Mux6~0_combout\);

-- Location: LCCOMB_X107_Y72_N10
\SEGDISP|Mux5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \SEGDISP|Mux5~0_combout\ = (\COUNT|Count:c[3]~2_combout\ & ((\COUNT|Count:c[0]~2_combout\ & (\COUNT|Count:c[1]~2_combout\)) # (!\COUNT|Count:c[0]~2_combout\ & ((\COUNT|Count:c[2]~2_combout\))))) # (!\COUNT|Count:c[3]~2_combout\ & 
-- (\COUNT|Count:c[2]~2_combout\ & (\COUNT|Count:c[1]~2_combout\ $ (\COUNT|Count:c[0]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001111010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datac => \COUNT|Count:c[0]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \SEGDISP|Mux5~0_combout\);

-- Location: LCCOMB_X107_Y72_N28
\SEGDISP|Mux4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \SEGDISP|Mux4~0_combout\ = (\COUNT|Count:c[3]~2_combout\ & (\COUNT|Count:c[2]~2_combout\ & ((\COUNT|Count:c[1]~2_combout\) # (!\COUNT|Count:c[0]~2_combout\)))) # (!\COUNT|Count:c[3]~2_combout\ & (\COUNT|Count:c[1]~2_combout\ & 
-- (!\COUNT|Count:c[0]~2_combout\ & !\COUNT|Count:c[2]~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datac => \COUNT|Count:c[0]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \SEGDISP|Mux4~0_combout\);

-- Location: LCCOMB_X107_Y72_N6
\SEGDISP|Mux3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \SEGDISP|Mux3~0_combout\ = (\COUNT|Count:c[1]~2_combout\ & ((\COUNT|Count:c[0]~2_combout\ & ((\COUNT|Count:c[2]~2_combout\))) # (!\COUNT|Count:c[0]~2_combout\ & (\COUNT|Count:c[3]~2_combout\ & !\COUNT|Count:c[2]~2_combout\)))) # 
-- (!\COUNT|Count:c[1]~2_combout\ & (!\COUNT|Count:c[3]~2_combout\ & (\COUNT|Count:c[0]~2_combout\ $ (\COUNT|Count:c[2]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000100011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datac => \COUNT|Count:c[0]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \SEGDISP|Mux3~0_combout\);

-- Location: LCCOMB_X107_Y72_N16
\SEGDISP|Mux2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \SEGDISP|Mux2~0_combout\ = (\COUNT|Count:c[1]~2_combout\ & (!\COUNT|Count:c[3]~2_combout\ & (\COUNT|Count:c[0]~2_combout\))) # (!\COUNT|Count:c[1]~2_combout\ & ((\COUNT|Count:c[2]~2_combout\ & (!\COUNT|Count:c[3]~2_combout\)) # 
-- (!\COUNT|Count:c[2]~2_combout\ & ((\COUNT|Count:c[0]~2_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datac => \COUNT|Count:c[0]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \SEGDISP|Mux2~0_combout\);

-- Location: LCCOMB_X107_Y72_N30
\SEGDISP|Mux1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \SEGDISP|Mux1~0_combout\ = (\COUNT|Count:c[1]~2_combout\ & (!\COUNT|Count:c[3]~2_combout\ & ((\COUNT|Count:c[0]~2_combout\) # (!\COUNT|Count:c[2]~2_combout\)))) # (!\COUNT|Count:c[1]~2_combout\ & (\COUNT|Count:c[0]~2_combout\ & 
-- (\COUNT|Count:c[3]~2_combout\ $ (!\COUNT|Count:c[2]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datac => \COUNT|Count:c[0]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \SEGDISP|Mux1~0_combout\);

-- Location: LCCOMB_X107_Y72_N8
\SEGDISP|Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \SEGDISP|Mux0~0_combout\ = (\COUNT|Count:c[0]~2_combout\ & ((\COUNT|Count:c[3]~2_combout\) # (\COUNT|Count:c[1]~2_combout\ $ (\COUNT|Count:c[2]~2_combout\)))) # (!\COUNT|Count:c[0]~2_combout\ & ((\COUNT|Count:c[1]~2_combout\) # 
-- (\COUNT|Count:c[3]~2_combout\ $ (\COUNT|Count:c[2]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT|Count:c[3]~2_combout\,
	datab => \COUNT|Count:c[1]~2_combout\,
	datac => \COUNT|Count:c[0]~2_combout\,
	datad => \COUNT|Count:c[2]~2_combout\,
	combout => \SEGDISP|Mux0~0_combout\);

ww_LCD_EN <= \LCD_EN~output_o\;

ww_LCD_RS <= \LCD_RS~output_o\;

ww_LCD_RW <= \LCD_RW~output_o\;

ww_LCD_Data(0) <= \LCD_Data[0]~output_o\;

ww_LCD_Data(1) <= \LCD_Data[1]~output_o\;

ww_LCD_Data(2) <= \LCD_Data[2]~output_o\;

ww_LCD_Data(3) <= \LCD_Data[3]~output_o\;

ww_LCD_Data(4) <= \LCD_Data[4]~output_o\;

ww_LCD_Data(5) <= \LCD_Data[5]~output_o\;

ww_LCD_Data(6) <= \LCD_Data[6]~output_o\;

ww_LCD_Data(7) <= \LCD_Data[7]~output_o\;

ww_LED_GRAY(0) <= \LED_GRAY[0]~output_o\;

ww_LED_GRAY(1) <= \LED_GRAY[1]~output_o\;

ww_LED_GRAY(2) <= \LED_GRAY[2]~output_o\;

ww_LED_GRAY(3) <= \LED_GRAY[3]~output_o\;

ww_LED_AIK(0) <= \LED_AIK[0]~output_o\;

ww_LED_AIK(1) <= \LED_AIK[1]~output_o\;

ww_LED_AIK(2) <= \LED_AIK[2]~output_o\;

ww_LED_AIK(3) <= \LED_AIK[3]~output_o\;

ww_SEG0(0) <= \SEG0[0]~output_o\;

ww_SEG0(1) <= \SEG0[1]~output_o\;

ww_SEG0(2) <= \SEG0[2]~output_o\;

ww_SEG0(3) <= \SEG0[3]~output_o\;

ww_SEG0(4) <= \SEG0[4]~output_o\;

ww_SEG0(5) <= \SEG0[5]~output_o\;

ww_SEG0(6) <= \SEG0[6]~output_o\;
END structure;


