LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY LCDDisplayTextGenerator IS
	GENERIC (
		WAIT_CYCLES	:	Integer	:= 50000;
		DISPLAY_SIZE:	Integer	:= 32
	);
	PORT (
		EN, nRST		:	IN		std_logic;
		Data			:	IN		String (1 to DISPLAY_SIZE);
		
		Busy			:	OUT	std_logic;
		
		LCD_EN, LCD_RW, LCD_RS	:	OUT	std_logic;
		LCD_Data						:	OUT	std_logic_vector(7 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF LCDDisplayTextGenerator IS
	FUNCTION Conv_Char2LCD(C : Character) return std_logic_vector IS
		VARIABLE cu : std_logic_vector(7 downto 0) := (Others => '0');
	BEGIN
		case C IS
			when 'A' => cu := "01000001";
			when 'B' => cu := "01000010";
			when 'C' => cu := "01000011";
			when 'D' => cu := "01000100";
			when 'E' => cu := "01000101";
			when 'F' => cu := "01000110";
			when 'G' => cu := "01000111";
			when 'H' => cu := "01001000";
			when 'I' => cu := "01001001";
			when 'J' => cu := "01001010";
			when 'K' => cu := "01001011";
			when 'L' => cu := "01001100";
			when 'M' => cu := "01001101";
			when 'N' => cu := "01001110";
			when 'O' => cu := "01001111";
			when 'P' => cu := "01010000";
			when 'Q' => cu := "01010001";
			when 'R' => cu := "01010010";
			when 'S' => cu := "01010011";
			when 'T' => cu := "01010100";
			when 'U' => cu := "01010101";
			when 'V' => cu := "01010110";
			when 'W' => cu := "01010111";
			when 'X' => cu := "01011000";
			when 'Y' => cu := "01011001";
			when 'Z' => cu := "01011010";
			when 'a' => cu := "01100001";
			when 'b' => cu := "01100010";
			when 'c' => cu := "01100011";
			when 'd' => cu := "01100100";
			when 'e' => cu := "01100101";
			when 'f' => cu := "01100110";
			when 'g' => cu := "01100111";
			when 'h' => cu := "01101000";
			when 'i' => cu := "01101001";
			when 'j' => cu := "01101010";
			when 'k' => cu := "01101011";
			when 'l' => cu := "01101100";
			when 'm' => cu := "01101101";
			when 'n' => cu := "01101110";
			when 'o' => cu := "01101111";
			when 'p' => cu := "01110000";
			when 'q' => cu := "01110001";
			when 'r' => cu := "01110010";
			when 's' => cu := "01110011";
			when 't' => cu := "01110100";
			when 'u' => cu := "01110101";
			when 'v' => cu := "01110110";
			when 'w' => cu := "01110111";
			when 'x' => cu := "01111000";
			when 'y' => cu := "01111001";
			when 'z' => cu := "01111010";
			when '0' => cu := "00110000";
			when '1' => cu := "00110001";
			when Others => cu := "00111111";
		end case;
		return cu;
	END Conv_Char2LCD;

	SIGNAL s_en, s_busy			:	std_logic	:= '0';
	SIGNAL s_rw, s_rs				:	std_logic	:= '0';
	SIGNAL s_data					:	std_logic_vector(7 downto 0) := (Others => '0');
BEGIN

WAIT_P : PROCESS(nRST, s_en)
	VARIABLE cycles				:	Integer range 0 to WAIT_CYCLES-1	:= 0;
	VARIABLE busy					:	std_logic	:= '0';
BEGIN
	if ( nRST = '0' ) then
		if ( s_en = '1' ) then
			busy := '1';
		end if;
		if ( busy = '1' ) then
			for c in 0 to WAIT_CYCLES-1 loop
				cycles := cycles + 1;
			end loop;
			busy := '0';
		end if;
	else
		busy := '0';
		cycles := 0;
	end if;
	s_busy <= busy;
END PROCESS WAIT_P;

WRITE_P : PROCESS (s_busy, nRST, EN)
	VARIABLE data_index	:	Integer	range 1 to DISPLAY_SIZE+1	:= 1;
BEGIN
	if ( nRST = '0' ) then
		if ( EN = '1' ) then
			if ( s_busy = '0' ) then
				s_rw <= '0';
				s_rs <= '1';
				s_data <= Conv_Char2LCD(Data(data_index));
				s_en <= '1';
				data_index := data_index + 1;
				if ( data_index = DISPLAY_SIZE+1 ) then
					data_index := data_index - DISPLAY_SIZE;
				end if;
			else
				
			end if;
		else
			s_en <= '0';
		end if;
	else
		data_index := 1;
	end if;	
END PROCESS WRITE_P;

	Busy <= s_busy;

	LCD_EN <= s_en;
	LCD_RW <= s_rw;
	LCD_RS <= s_rs;
	
	lCD_Data <= s_data;
	
END Behaviour;