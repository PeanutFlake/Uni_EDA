LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY LCDDeviceController IS
	GENERIC (
		F_CLK				:	Integer range 0 to 2147483647 := 50*(10**6);
		LINEWIDTH		:	Integer range 0 to 48			:=	16
	);
	PORT (
		CLK, nRST, WEN	:	IN		std_logic;
		DI					:	IN		std_logic_vector(7 downto 0);
		lcd_data			:	OUT	std_logic_vector(7 downto 0);
		lcd_en, lcd_rw, lcd_rs	:	OUT	std_logic;
		busy				:	OUT	std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF LCDDeviceController IS

	TYPE	State			IS	(Start, Init, Ready, Send);
	TYPE 	State_Init	IS	(Reset, Func, Disp, Entry, Finish);
	TYPE 	State_Send	IS	(Push, Check);
	
	CONSTANT	WT_Start	:	Integer	:=	F_CLK/66;		-- >15ms
	CONSTANT WT_Init	:	Integer	:= F_CLK/200;		--	>5 ms
	CONSTANT WT_Send	:	Integer	:=	F_CLK/20000;	-- >50us

	SIGNAL s_state_current, s_state_next				:	State			:= Start;
	SIGNAL s_state_init_current, s_state_init_next	:	State_Init	:= Reset;
	SIGNAL s_state_send_current, s_state_send_next	:	State_Send	:=	Push;

	SIGNAL s_en, s_rw, s_rs		:	std_logic	:= '0';
	SIGNAL s_data					:	std_logic_vector(7 downto 0)	:= (Others => '0');
	
	SIGNAL s_busy					:	std_logic	:= '1';
	
BEGIN

	P_UEBERGANG : PROCESS(CLK, nRST, WEN, DI, s_state_current, s_state_init_current, s_state_send_current)
		VARIABLE t_wait		:	Integer		:= 0;
		VARIABLE rs, rw, en	:	std_logic	:=	'0';
		
		VARIABLE data			:	std_logic_vector(7 downto 0)	:= (Others => '0');
		
		VARIABLE c_pos_x		:	Integer	range 0 to LINEWIDTH-1 	:= 0;
		VARIABLE c_pos_y		:	Integer	range 0 to 1	:= 0;
	BEGIN
	
		if ( nRST = '1' ) then
			if ( rising_edge(CLK) ) then
				if ( t_wait = 0 ) then
			
					case s_state_current is
						when Start =>
							t_wait := WT_Start;
							s_state_next <= Init;
						when Init =>
							case s_state_init_current is
								when Reset =>
									rs := '0';
									rw := '0';
									s_state_init_next <= Func;
									t_wait := WT_Init;
								when Func =>
									en := '1';
									data := "00111100";
									s_state_init_next <= Disp;
									t_wait := WT_Init;
								when Disp =>
									en := '1';
									data := "00001100";
									s_state_init_next <= Entry;
									t_wait := WT_Init;
								when Entry =>
									en := '1';
									data := "00000110";
									s_state_init_next <= Finish;
									t_wait := WT_Init;
								when Finish =>
									rs := '1';
									rw := '0';
									s_state_next <= Ready;
							end case;
						when Ready =>
							if ( WEN = '1' ) then
								data := DI;
								s_state_next <= Send;
								s_busy <= '1';
							else
								s_state_next <= Ready;
								s_busy <= '0';
							end if;
						when Send =>
							case s_state_send_current is
								when Push =>
									en := '1';
									t_wait := WT_Send;
									s_state_send_next <= Check;
								when Check =>
									-- check
									if ( rs = '1' ) then
										if ( c_pos_x = LINEWIDTH-1 ) then
											c_pos_x := 0;
											if ( c_pos_y = 0 ) then
												c_pos_y := 1;
												data := "11000000"; -- set Line 1
												rs := '0';
												en := '1';
											else
												c_pos_y := 0;
												data := "10000000"; -- set Line 0
												rs := '0';
												en := '1';
											end if;
											t_wait := WT_Send;
										else
											c_pos_x := c_pos_x + 1;
											s_state_next <= Ready;
										end if;
									else
										rs := '1';
										s_state_next <= Ready;
									end if;
									s_state_send_next <= Push;
							end case;
					end case;
					
				else
					t_wait := t_wait - 1;
					en := '0';
				end if;
				
				s_en <= en;
				s_rs <= rs;
				s_rw <= rw;
				s_data <= data;
				
			else
				-- out of clock
			end if;
		else
			-- reset
			t_wait := 0;
			c_pos_x := 0;
			c_pos_y := 0;
			data := (Others => '0');
		end if;
	
	END PROCESS P_UEBERGANG;
	
	P_ZUSTAND : PROCESS(CLK, nRST, s_state_next, s_state_init_next, s_state_send_next)
	BEGIN
		if ( nRST = '1' ) then
			if ( rising_edge(CLK) ) then
				s_state_current <= s_state_next;
				s_state_init_current <= s_state_init_next;
				s_state_send_current <= s_state_send_next;
			else
				-- out of clock
			end if;
		else
			s_state_current <= Start;
			s_state_init_current <= Reset;
			s_state_send_current <= Push;
		end if;
	END PROCESS P_ZUSTAND;
	
	lcd_en <= s_en;
	lcd_rs <= s_rs;
	lcd_rw <= s_rw;
	lcd_data <= s_data;
	
	busy <= s_busy;
	
END Behaviour;