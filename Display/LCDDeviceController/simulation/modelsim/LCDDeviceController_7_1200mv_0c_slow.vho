-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "06/18/2018 17:18:44"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	LCDDeviceController IS
    PORT (
	CLK : IN std_logic;
	nRST : IN std_logic;
	WEN : IN std_logic;
	DI : IN std_logic_vector(7 DOWNTO 0);
	lcd_data : OUT std_logic_vector(7 DOWNTO 0);
	lcd_en : OUT std_logic;
	lcd_rw : OUT std_logic;
	lcd_rs : OUT std_logic;
	busy : OUT std_logic
	);
END LCDDeviceController;

-- Design Ports Information
-- lcd_data[0]	=>  Location: PIN_V24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lcd_data[1]	=>  Location: PIN_W26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lcd_data[2]	=>  Location: PIN_U23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lcd_data[3]	=>  Location: PIN_W28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lcd_data[4]	=>  Location: PIN_AB19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lcd_data[5]	=>  Location: PIN_W25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lcd_data[6]	=>  Location: PIN_V26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lcd_data[7]	=>  Location: PIN_V28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lcd_en	=>  Location: PIN_V27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lcd_rw	=>  Location: PIN_AB14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- lcd_rs	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- busy	=>  Location: PIN_W27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DI[0]	=>  Location: PIN_V21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- WEN	=>  Location: PIN_V23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- nRST	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DI[1]	=>  Location: PIN_U26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DI[2]	=>  Location: PIN_U25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DI[3]	=>  Location: PIN_U28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DI[4]	=>  Location: PIN_U24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DI[5]	=>  Location: PIN_V25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DI[6]	=>  Location: PIN_U22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DI[7]	=>  Location: PIN_V22,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF LCDDeviceController IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_nRST : std_logic;
SIGNAL ww_WEN : std_logic;
SIGNAL ww_DI : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_lcd_data : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_lcd_en : std_logic;
SIGNAL ww_lcd_rw : std_logic;
SIGNAL ww_lcd_rs : std_logic;
SIGNAL ww_busy : std_logic;
SIGNAL \nRST~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \CLK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \lcd_data[0]~output_o\ : std_logic;
SIGNAL \lcd_data[1]~output_o\ : std_logic;
SIGNAL \lcd_data[2]~output_o\ : std_logic;
SIGNAL \lcd_data[3]~output_o\ : std_logic;
SIGNAL \lcd_data[4]~output_o\ : std_logic;
SIGNAL \lcd_data[5]~output_o\ : std_logic;
SIGNAL \lcd_data[6]~output_o\ : std_logic;
SIGNAL \lcd_data[7]~output_o\ : std_logic;
SIGNAL \lcd_en~output_o\ : std_logic;
SIGNAL \lcd_rw~output_o\ : std_logic;
SIGNAL \lcd_rs~output_o\ : std_logic;
SIGNAL \busy~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \CLK~inputclkctrl_outclk\ : std_logic;
SIGNAL \DI[0]~input_o\ : std_logic;
SIGNAL \WEN~input_o\ : std_logic;
SIGNAL \s_busy~1_combout\ : std_logic;
SIGNAL \s_state_next.Start~0_combout\ : std_logic;
SIGNAL \s_state_next.Start~q\ : std_logic;
SIGNAL \nRST~input_o\ : std_logic;
SIGNAL \nRST~inputclkctrl_outclk\ : std_logic;
SIGNAL \s_state_current.Start~q\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[19]~1_combout\ : std_logic;
SIGNAL \s_state_init_next.Reset~0_combout\ : std_logic;
SIGNAL \s_state_init_next.Reset~q\ : std_logic;
SIGNAL \s_state_init_current.Reset~feeder_combout\ : std_logic;
SIGNAL \s_state_init_current.Reset~q\ : std_logic;
SIGNAL \Selector1~0_combout\ : std_logic;
SIGNAL \s_state_init_next.Func~1_combout\ : std_logic;
SIGNAL \s_state_init_next.Func~q\ : std_logic;
SIGNAL \s_state_init_current.Func~q\ : std_logic;
SIGNAL \Selector2~0_combout\ : std_logic;
SIGNAL \s_state_init_next.Disp~q\ : std_logic;
SIGNAL \s_state_init_current.Disp~q\ : std_logic;
SIGNAL \Selector3~0_combout\ : std_logic;
SIGNAL \s_state_init_next.Entry~q\ : std_logic;
SIGNAL \s_state_init_current.Entry~q\ : std_logic;
SIGNAL \Selector4~0_combout\ : std_logic;
SIGNAL \s_state_init_next.Finish~q\ : std_logic;
SIGNAL \s_state_init_current.Finish~q\ : std_logic;
SIGNAL \s_state_send_next~2_combout\ : std_logic;
SIGNAL \s_state_send_next~q\ : std_logic;
SIGNAL \s_state_send_current~feeder_combout\ : std_logic;
SIGNAL \s_state_send_current~q\ : std_logic;
SIGNAL \rs~3_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[19]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[17]~1_combout\ : std_logic;
SIGNAL \t_wait~0_combout\ : std_logic;
SIGNAL \Add0~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[0]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[0]~q\ : std_logic;
SIGNAL \Add0~1\ : std_logic;
SIGNAL \Add0~2_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[1]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[1]~q\ : std_logic;
SIGNAL \Add0~3\ : std_logic;
SIGNAL \Add0~4_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[2]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[2]~q\ : std_logic;
SIGNAL \Add0~5\ : std_logic;
SIGNAL \Add0~6_combout\ : std_logic;
SIGNAL \Add0~79_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[3]~q\ : std_logic;
SIGNAL \Add0~7\ : std_logic;
SIGNAL \Add0~8_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[4]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[17]~2_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[4]~q\ : std_logic;
SIGNAL \Add0~9\ : std_logic;
SIGNAL \Add0~10_combout\ : std_logic;
SIGNAL \Add0~78_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[5]~q\ : std_logic;
SIGNAL \Add0~11\ : std_logic;
SIGNAL \Add0~12_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[6]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[6]~q\ : std_logic;
SIGNAL \Add0~13\ : std_logic;
SIGNAL \Add0~14_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[7]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[7]~q\ : std_logic;
SIGNAL \Add0~15\ : std_logic;
SIGNAL \Add0~16_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[8]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[8]~q\ : std_logic;
SIGNAL \Add0~17\ : std_logic;
SIGNAL \Add0~18_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[9]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[9]~q\ : std_logic;
SIGNAL \Add0~19\ : std_logic;
SIGNAL \Add0~20_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[10]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[10]~q\ : std_logic;
SIGNAL \Add0~21\ : std_logic;
SIGNAL \Add0~22_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[11]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[11]~q\ : std_logic;
SIGNAL \Add0~23\ : std_logic;
SIGNAL \Add0~24_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[12]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[12]~q\ : std_logic;
SIGNAL \Add0~25\ : std_logic;
SIGNAL \Add0~26_combout\ : std_logic;
SIGNAL \Add0~77_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[13]~q\ : std_logic;
SIGNAL \Add0~27\ : std_logic;
SIGNAL \Add0~28_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[14]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[14]~q\ : std_logic;
SIGNAL \Add0~29\ : std_logic;
SIGNAL \Add0~30_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[15]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[15]~q\ : std_logic;
SIGNAL \Add0~31\ : std_logic;
SIGNAL \Add0~32_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[16]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[16]~q\ : std_logic;
SIGNAL \Add0~33\ : std_logic;
SIGNAL \Add0~34_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[17]~0_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[17]~q\ : std_logic;
SIGNAL \Add0~35\ : std_logic;
SIGNAL \Add0~36_combout\ : std_logic;
SIGNAL \Add0~76_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[18]~q\ : std_logic;
SIGNAL \Add0~37\ : std_logic;
SIGNAL \Add0~38_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[19]~2_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[19]~q\ : std_logic;
SIGNAL \Add0~39\ : std_logic;
SIGNAL \Add0~40_combout\ : std_logic;
SIGNAL \Add0~75_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[20]~q\ : std_logic;
SIGNAL \Add0~41\ : std_logic;
SIGNAL \Add0~42_combout\ : std_logic;
SIGNAL \Add0~74_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[21]~q\ : std_logic;
SIGNAL \Add0~43\ : std_logic;
SIGNAL \Add0~44_combout\ : std_logic;
SIGNAL \Add0~73_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[22]~q\ : std_logic;
SIGNAL \Add0~45\ : std_logic;
SIGNAL \Add0~46_combout\ : std_logic;
SIGNAL \Add0~72_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[23]~q\ : std_logic;
SIGNAL \Add0~47\ : std_logic;
SIGNAL \Add0~48_combout\ : std_logic;
SIGNAL \Add0~71_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[24]~q\ : std_logic;
SIGNAL \Add0~49\ : std_logic;
SIGNAL \Add0~50_combout\ : std_logic;
SIGNAL \Add0~70_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[25]~q\ : std_logic;
SIGNAL \Add0~51\ : std_logic;
SIGNAL \Add0~52_combout\ : std_logic;
SIGNAL \Add0~69_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[26]~q\ : std_logic;
SIGNAL \Add0~53\ : std_logic;
SIGNAL \Add0~54_combout\ : std_logic;
SIGNAL \Add0~68_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[27]~q\ : std_logic;
SIGNAL \Add0~55\ : std_logic;
SIGNAL \Add0~56_combout\ : std_logic;
SIGNAL \Add0~67_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[28]~q\ : std_logic;
SIGNAL \Add0~57\ : std_logic;
SIGNAL \Add0~58_combout\ : std_logic;
SIGNAL \Add0~66_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[29]~q\ : std_logic;
SIGNAL \Add0~59\ : std_logic;
SIGNAL \Add0~60_combout\ : std_logic;
SIGNAL \Add0~65_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[30]~q\ : std_logic;
SIGNAL \Add0~61\ : std_logic;
SIGNAL \Add0~62_combout\ : std_logic;
SIGNAL \Add0~64_combout\ : std_logic;
SIGNAL \P_UEBERGANG:t_wait[31]~q\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \Equal0~3_combout\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \Equal0~4_combout\ : std_logic;
SIGNAL \Equal0~8_combout\ : std_logic;
SIGNAL \Equal0~5_combout\ : std_logic;
SIGNAL \Equal0~6_combout\ : std_logic;
SIGNAL \Equal0~7_combout\ : std_logic;
SIGNAL \Equal0~9_combout\ : std_logic;
SIGNAL \Equal0~10_combout\ : std_logic;
SIGNAL \s_state_init_next.Func~0_combout\ : std_logic;
SIGNAL \s_state_next.Init~0_combout\ : std_logic;
SIGNAL \s_state_next.Init~q\ : std_logic;
SIGNAL \s_state_current.Init~feeder_combout\ : std_logic;
SIGNAL \s_state_current.Init~q\ : std_logic;
SIGNAL \s_state_next.Ready~0_combout\ : std_logic;
SIGNAL \s_state_next.Ready~2_combout\ : std_logic;
SIGNAL \s_state_next.Send~q\ : std_logic;
SIGNAL \s_state_current.Send~feeder_combout\ : std_logic;
SIGNAL \s_state_current.Send~q\ : std_logic;
SIGNAL \s_state_next.Ready~1_combout\ : std_logic;
SIGNAL \s_state_next.Ready~q\ : std_logic;
SIGNAL \s_state_current.Ready~q\ : std_logic;
SIGNAL \P_UEBERGANG:data[0]~q\ : std_logic;
SIGNAL \en~0_combout\ : std_logic;
SIGNAL \data~0_combout\ : std_logic;
SIGNAL \data~1_combout\ : std_logic;
SIGNAL \s_data[0]~feeder_combout\ : std_logic;
SIGNAL \data~2_combout\ : std_logic;
SIGNAL \DI[1]~input_o\ : std_logic;
SIGNAL \data~4_combout\ : std_logic;
SIGNAL \P_UEBERGANG:data[1]~q\ : std_logic;
SIGNAL \data~3_combout\ : std_logic;
SIGNAL \data~5_combout\ : std_logic;
SIGNAL \DI[2]~input_o\ : std_logic;
SIGNAL \P_UEBERGANG:data[2]~q\ : std_logic;
SIGNAL \data~6_combout\ : std_logic;
SIGNAL \s_data[2]~feeder_combout\ : std_logic;
SIGNAL \DI[3]~input_o\ : std_logic;
SIGNAL \data~7_combout\ : std_logic;
SIGNAL \P_UEBERGANG:data[3]~q\ : std_logic;
SIGNAL \data~8_combout\ : std_logic;
SIGNAL \DI[4]~input_o\ : std_logic;
SIGNAL \data~9_combout\ : std_logic;
SIGNAL \P_UEBERGANG:data[4]~q\ : std_logic;
SIGNAL \data~10_combout\ : std_logic;
SIGNAL \s_data[4]~feeder_combout\ : std_logic;
SIGNAL \DI[5]~input_o\ : std_logic;
SIGNAL \data~11_combout\ : std_logic;
SIGNAL \P_UEBERGANG:data[5]~q\ : std_logic;
SIGNAL \data~12_combout\ : std_logic;
SIGNAL \DI[6]~input_o\ : std_logic;
SIGNAL \P_UEBERGANG:data[6]~q\ : std_logic;
SIGNAL \data~13_combout\ : std_logic;
SIGNAL \s_data[6]~feeder_combout\ : std_logic;
SIGNAL \DI[7]~input_o\ : std_logic;
SIGNAL \P_UEBERGANG:data[7]~q\ : std_logic;
SIGNAL \data~14_combout\ : std_logic;
SIGNAL \s_data[7]~feeder_combout\ : std_logic;
SIGNAL \en~1_combout\ : std_logic;
SIGNAL \P_UEBERGANG:en~q\ : std_logic;
SIGNAL \en~2_combout\ : std_logic;
SIGNAL \s_en~feeder_combout\ : std_logic;
SIGNAL \s_en~q\ : std_logic;
SIGNAL \rs~2_combout\ : std_logic;
SIGNAL \P_UEBERGANG:rs~q\ : std_logic;
SIGNAL \rs~5_combout\ : std_logic;
SIGNAL \rs~4_combout\ : std_logic;
SIGNAL \s_rs~feeder_combout\ : std_logic;
SIGNAL \s_rs~q\ : std_logic;
SIGNAL \s_busy~0_combout\ : std_logic;
SIGNAL \s_busy~q\ : std_logic;
SIGNAL s_data : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_s_busy~q\ : std_logic;

BEGIN

ww_CLK <= CLK;
ww_nRST <= nRST;
ww_WEN <= WEN;
ww_DI <= DI;
lcd_data <= ww_lcd_data;
lcd_en <= ww_lcd_en;
lcd_rw <= ww_lcd_rw;
lcd_rs <= ww_lcd_rs;
busy <= ww_busy;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\nRST~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \nRST~input_o\);

\CLK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLK~input_o\);
\ALT_INV_s_busy~q\ <= NOT \s_busy~q\;

-- Location: IOOBUF_X115_Y24_N9
\lcd_data[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_data(0),
	devoe => ww_devoe,
	o => \lcd_data[0]~output_o\);

-- Location: IOOBUF_X115_Y19_N2
\lcd_data[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_data(1),
	devoe => ww_devoe,
	o => \lcd_data[1]~output_o\);

-- Location: IOOBUF_X115_Y22_N2
\lcd_data[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_data(2),
	devoe => ww_devoe,
	o => \lcd_data[2]~output_o\);

-- Location: IOOBUF_X115_Y21_N16
\lcd_data[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_data(3),
	devoe => ww_devoe,
	o => \lcd_data[3]~output_o\);

-- Location: IOOBUF_X98_Y0_N23
\lcd_data[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_data(4),
	devoe => ww_devoe,
	o => \lcd_data[4]~output_o\);

-- Location: IOOBUF_X115_Y20_N9
\lcd_data[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_data(5),
	devoe => ww_devoe,
	o => \lcd_data[5]~output_o\);

-- Location: IOOBUF_X115_Y23_N9
\lcd_data[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_data(6),
	devoe => ww_devoe,
	o => \lcd_data[6]~output_o\);

-- Location: IOOBUF_X115_Y22_N23
\lcd_data[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_data(7),
	devoe => ww_devoe,
	o => \lcd_data[7]~output_o\);

-- Location: IOOBUF_X115_Y22_N16
\lcd_en~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_en~q\,
	devoe => ww_devoe,
	o => \lcd_en~output_o\);

-- Location: IOOBUF_X54_Y0_N16
\lcd_rw~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \lcd_rw~output_o\);

-- Location: IOOBUF_X115_Y25_N23
\lcd_rs~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_rs~q\,
	devoe => ww_devoe,
	o => \lcd_rs~output_o\);

-- Location: IOOBUF_X115_Y20_N2
\busy~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_s_busy~q\,
	devoe => ww_devoe,
	o => \busy~output_o\);

-- Location: IOIBUF_X0_Y36_N8
\CLK~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: CLKCTRL_G2
\CLK~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLK~inputclkctrl_outclk\);

-- Location: IOIBUF_X115_Y25_N15
\DI[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_DI(0),
	o => \DI[0]~input_o\);

-- Location: IOIBUF_X115_Y24_N1
\WEN~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_WEN,
	o => \WEN~input_o\);

-- Location: LCCOMB_X100_Y24_N10
\s_busy~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_busy~1_combout\ = (\s_state_current.Ready~q\ & \WEN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Ready~q\,
	datad => \WEN~input_o\,
	combout => \s_busy~1_combout\);

-- Location: LCCOMB_X99_Y24_N22
\s_state_next.Start~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_next.Start~0_combout\ = (\s_state_next.Start~q\) # (\s_state_next.Ready~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \s_state_next.Start~q\,
	datad => \s_state_next.Ready~2_combout\,
	combout => \s_state_next.Start~0_combout\);

-- Location: FF_X99_Y24_N23
\s_state_next.Start\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_state_next.Start~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_next.Start~q\);

-- Location: IOIBUF_X0_Y36_N15
\nRST~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_nRST,
	o => \nRST~input_o\);

-- Location: CLKCTRL_G4
\nRST~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \nRST~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \nRST~inputclkctrl_outclk\);

-- Location: FF_X99_Y24_N17
\s_state_current.Start\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \s_state_next.Start~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Start~q\);

-- Location: LCCOMB_X99_Y24_N16
\P_UEBERGANG:t_wait[19]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[19]~1_combout\ = (!\s_state_current.Init~q\ & !\s_state_current.Send~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Init~q\,
	datad => \s_state_current.Send~q\,
	combout => \P_UEBERGANG:t_wait[19]~1_combout\);

-- Location: LCCOMB_X100_Y24_N12
\s_state_init_next.Reset~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_init_next.Reset~0_combout\ = (\s_state_init_next.Reset~q\) # ((\s_state_current.Init~q\ & (!\s_state_init_current.Finish~q\ & \s_state_init_next.Func~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Init~q\,
	datab => \s_state_init_current.Finish~q\,
	datac => \s_state_init_next.Reset~q\,
	datad => \s_state_init_next.Func~0_combout\,
	combout => \s_state_init_next.Reset~0_combout\);

-- Location: FF_X100_Y24_N13
\s_state_init_next.Reset\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_state_init_next.Reset~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_init_next.Reset~q\);

-- Location: LCCOMB_X100_Y24_N18
\s_state_init_current.Reset~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_init_current.Reset~feeder_combout\ = \s_state_init_next.Reset~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \s_state_init_next.Reset~q\,
	combout => \s_state_init_current.Reset~feeder_combout\);

-- Location: FF_X100_Y24_N19
\s_state_init_current.Reset\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_state_init_current.Reset~feeder_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_init_current.Reset~q\);

-- Location: LCCOMB_X101_Y24_N8
\Selector1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector1~0_combout\ = ((\s_state_init_current.Finish~q\ & \s_state_init_next.Func~q\)) # (!\s_state_init_current.Reset~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_init_current.Finish~q\,
	datac => \s_state_init_next.Func~q\,
	datad => \s_state_init_current.Reset~q\,
	combout => \Selector1~0_combout\);

-- Location: LCCOMB_X101_Y24_N16
\s_state_init_next.Func~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_init_next.Func~1_combout\ = (\nRST~input_o\ & (\s_state_current.Init~q\ & \Equal0~10_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \nRST~input_o\,
	datac => \s_state_current.Init~q\,
	datad => \Equal0~10_combout\,
	combout => \s_state_init_next.Func~1_combout\);

-- Location: FF_X101_Y24_N9
\s_state_init_next.Func\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Selector1~0_combout\,
	ena => \s_state_init_next.Func~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_init_next.Func~q\);

-- Location: FF_X100_Y24_N27
\s_state_init_current.Func\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \s_state_init_next.Func~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_init_current.Func~q\);

-- Location: LCCOMB_X101_Y24_N10
\Selector2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector2~0_combout\ = (\s_state_init_current.Func~q\) # ((\s_state_init_next.Disp~q\ & \s_state_init_current.Finish~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_init_current.Func~q\,
	datac => \s_state_init_next.Disp~q\,
	datad => \s_state_init_current.Finish~q\,
	combout => \Selector2~0_combout\);

-- Location: FF_X101_Y24_N11
\s_state_init_next.Disp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Selector2~0_combout\,
	ena => \s_state_init_next.Func~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_init_next.Disp~q\);

-- Location: FF_X100_Y24_N15
\s_state_init_current.Disp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \s_state_init_next.Disp~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_init_current.Disp~q\);

-- Location: LCCOMB_X101_Y24_N30
\Selector3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector3~0_combout\ = (\s_state_init_current.Disp~q\) # ((\s_state_init_current.Finish~q\ & \s_state_init_next.Entry~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_init_current.Finish~q\,
	datac => \s_state_init_next.Entry~q\,
	datad => \s_state_init_current.Disp~q\,
	combout => \Selector3~0_combout\);

-- Location: FF_X101_Y24_N31
\s_state_init_next.Entry\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Selector3~0_combout\,
	ena => \s_state_init_next.Func~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_init_next.Entry~q\);

-- Location: FF_X100_Y24_N29
\s_state_init_current.Entry\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \s_state_init_next.Entry~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_init_current.Entry~q\);

-- Location: LCCOMB_X101_Y24_N0
\Selector4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector4~0_combout\ = (\s_state_init_current.Entry~q\) # ((\s_state_init_current.Finish~q\ & \s_state_init_next.Finish~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_init_current.Finish~q\,
	datac => \s_state_init_next.Finish~q\,
	datad => \s_state_init_current.Entry~q\,
	combout => \Selector4~0_combout\);

-- Location: FF_X101_Y24_N1
\s_state_init_next.Finish\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Selector4~0_combout\,
	ena => \s_state_init_next.Func~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_init_next.Finish~q\);

-- Location: FF_X100_Y24_N25
\s_state_init_current.Finish\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \s_state_init_next.Finish~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_init_current.Finish~q\);

-- Location: LCCOMB_X100_Y24_N0
\s_state_send_next~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_send_next~2_combout\ = (\s_state_current.Send~q\ & ((\s_state_init_next.Func~0_combout\ & (!\s_state_send_current~q\)) # (!\s_state_init_next.Func~0_combout\ & ((\s_state_send_next~q\))))) # (!\s_state_current.Send~q\ & 
-- (((\s_state_send_next~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_send_current~q\,
	datab => \s_state_current.Send~q\,
	datac => \s_state_send_next~q\,
	datad => \s_state_init_next.Func~0_combout\,
	combout => \s_state_send_next~2_combout\);

-- Location: FF_X100_Y24_N1
s_state_send_next : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_state_send_next~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_send_next~q\);

-- Location: LCCOMB_X100_Y24_N6
\s_state_send_current~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_send_current~feeder_combout\ = \s_state_send_next~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \s_state_send_next~q\,
	combout => \s_state_send_current~feeder_combout\);

-- Location: FF_X100_Y24_N7
s_state_send_current : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_state_send_current~feeder_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_send_current~q\);

-- Location: LCCOMB_X100_Y24_N28
\rs~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \rs~3_combout\ = (\s_state_current.Send~q\ & \s_state_send_current~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Send~q\,
	datad => \s_state_send_current~q\,
	combout => \rs~3_combout\);

-- Location: LCCOMB_X99_Y24_N14
\P_UEBERGANG:t_wait[19]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[19]~0_combout\ = (!\s_state_current.Ready~q\ & (!\rs~3_combout\ & ((!\s_state_init_current.Finish~q\) # (!\s_state_current.Init~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Init~q\,
	datab => \s_state_init_current.Finish~q\,
	datac => \s_state_current.Ready~q\,
	datad => \rs~3_combout\,
	combout => \P_UEBERGANG:t_wait[19]~0_combout\);

-- Location: LCCOMB_X99_Y24_N8
\P_UEBERGANG:t_wait[17]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[17]~1_combout\ = (!\s_state_current.Send~q\ & ((!\s_state_init_current.Reset~q\) # (!\s_state_current.Init~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Init~q\,
	datab => \s_state_init_current.Reset~q\,
	datad => \s_state_current.Send~q\,
	combout => \P_UEBERGANG:t_wait[17]~1_combout\);

-- Location: LCCOMB_X99_Y24_N0
\t_wait~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait~0_combout\ = (\s_state_init_current.Reset~q\) # (!\s_state_current.Init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110111011101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Init~q\,
	datab => \s_state_init_current.Reset~q\,
	combout => \t_wait~0_combout\);

-- Location: LCCOMB_X97_Y25_N0
\Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~0_combout\ = \P_UEBERGANG:t_wait[0]~q\ $ (VCC)
-- \Add0~1\ = CARRY(\P_UEBERGANG:t_wait[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[0]~q\,
	datad => VCC,
	combout => \Add0~0_combout\,
	cout => \Add0~1\);

-- Location: LCCOMB_X98_Y25_N8
\P_UEBERGANG:t_wait[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[0]~0_combout\ = (\Equal0~10_combout\ & (\P_UEBERGANG:t_wait[19]~0_combout\ & (\P_UEBERGANG:t_wait[19]~1_combout\))) # (!\Equal0~10_combout\ & (((\Add0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[19]~0_combout\,
	datab => \P_UEBERGANG:t_wait[19]~1_combout\,
	datac => \Add0~0_combout\,
	datad => \Equal0~10_combout\,
	combout => \P_UEBERGANG:t_wait[0]~0_combout\);

-- Location: FF_X97_Y25_N19
\P_UEBERGANG:t_wait[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \P_UEBERGANG:t_wait[0]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[0]~q\);

-- Location: LCCOMB_X97_Y25_N2
\Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~2_combout\ = (\P_UEBERGANG:t_wait[1]~q\ & (\Add0~1\ & VCC)) # (!\P_UEBERGANG:t_wait[1]~q\ & (!\Add0~1\))
-- \Add0~3\ = CARRY((!\P_UEBERGANG:t_wait[1]~q\ & !\Add0~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[1]~q\,
	datad => VCC,
	cin => \Add0~1\,
	combout => \Add0~2_combout\,
	cout => \Add0~3\);

-- Location: LCCOMB_X98_Y25_N10
\P_UEBERGANG:t_wait[1]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[1]~0_combout\ = (\Equal0~10_combout\ & (\P_UEBERGANG:t_wait[19]~0_combout\ & ((\P_UEBERGANG:t_wait[19]~1_combout\)))) # (!\Equal0~10_combout\ & (((\Add0~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[19]~0_combout\,
	datab => \Add0~2_combout\,
	datac => \P_UEBERGANG:t_wait[19]~1_combout\,
	datad => \Equal0~10_combout\,
	combout => \P_UEBERGANG:t_wait[1]~0_combout\);

-- Location: FF_X97_Y25_N11
\P_UEBERGANG:t_wait[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \P_UEBERGANG:t_wait[1]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[1]~q\);

-- Location: LCCOMB_X97_Y25_N4
\Add0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~4_combout\ = (\P_UEBERGANG:t_wait[2]~q\ & ((GND) # (!\Add0~3\))) # (!\P_UEBERGANG:t_wait[2]~q\ & (\Add0~3\ $ (GND)))
-- \Add0~5\ = CARRY((\P_UEBERGANG:t_wait[2]~q\) # (!\Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[2]~q\,
	datad => VCC,
	cin => \Add0~3\,
	combout => \Add0~4_combout\,
	cout => \Add0~5\);

-- Location: LCCOMB_X98_Y25_N6
\P_UEBERGANG:t_wait[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[2]~0_combout\ = (\Equal0~10_combout\ & (\P_UEBERGANG:t_wait[19]~0_combout\ & (\t_wait~0_combout\))) # (!\Equal0~10_combout\ & (((\Add0~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[19]~0_combout\,
	datab => \t_wait~0_combout\,
	datac => \Add0~4_combout\,
	datad => \Equal0~10_combout\,
	combout => \P_UEBERGANG:t_wait[2]~0_combout\);

-- Location: FF_X98_Y25_N7
\P_UEBERGANG:t_wait[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[2]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[2]~q\);

-- Location: LCCOMB_X97_Y25_N6
\Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~6_combout\ = (\P_UEBERGANG:t_wait[3]~q\ & (\Add0~5\ & VCC)) # (!\P_UEBERGANG:t_wait[3]~q\ & (!\Add0~5\))
-- \Add0~7\ = CARRY((!\P_UEBERGANG:t_wait[3]~q\ & !\Add0~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[3]~q\,
	datad => VCC,
	cin => \Add0~5\,
	combout => \Add0~6_combout\,
	cout => \Add0~7\);

-- Location: LCCOMB_X98_Y25_N20
\Add0~79\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~79_combout\ = (\Add0~6_combout\ & !\Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~6_combout\,
	datad => \Equal0~10_combout\,
	combout => \Add0~79_combout\);

-- Location: FF_X98_Y25_N21
\P_UEBERGANG:t_wait[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~79_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[3]~q\);

-- Location: LCCOMB_X97_Y25_N8
\Add0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~8_combout\ = (\P_UEBERGANG:t_wait[4]~q\ & ((GND) # (!\Add0~7\))) # (!\P_UEBERGANG:t_wait[4]~q\ & (\Add0~7\ $ (GND)))
-- \Add0~9\ = CARRY((\P_UEBERGANG:t_wait[4]~q\) # (!\Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[4]~q\,
	datad => VCC,
	cin => \Add0~7\,
	combout => \Add0~8_combout\,
	cout => \Add0~9\);

-- Location: LCCOMB_X98_Y25_N2
\P_UEBERGANG:t_wait[4]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[4]~0_combout\ = (\Equal0~10_combout\ & ((!\t_wait~0_combout\))) # (!\Equal0~10_combout\ & (\Add0~8_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Add0~8_combout\,
	datab => \t_wait~0_combout\,
	datad => \Equal0~10_combout\,
	combout => \P_UEBERGANG:t_wait[4]~0_combout\);

-- Location: LCCOMB_X98_Y25_N18
\P_UEBERGANG:t_wait[17]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[17]~2_combout\ = (!\P_UEBERGANG:t_wait[19]~0_combout\ & \Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[19]~0_combout\,
	datad => \Equal0~10_combout\,
	combout => \P_UEBERGANG:t_wait[17]~2_combout\);

-- Location: FF_X98_Y25_N3
\P_UEBERGANG:t_wait[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[4]~0_combout\,
	asdata => \P_UEBERGANG:t_wait[4]~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => \P_UEBERGANG:t_wait[17]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[4]~q\);

-- Location: LCCOMB_X97_Y25_N10
\Add0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~10_combout\ = (\P_UEBERGANG:t_wait[5]~q\ & (\Add0~9\ & VCC)) # (!\P_UEBERGANG:t_wait[5]~q\ & (!\Add0~9\))
-- \Add0~11\ = CARRY((!\P_UEBERGANG:t_wait[5]~q\ & !\Add0~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[5]~q\,
	datad => VCC,
	cin => \Add0~9\,
	combout => \Add0~10_combout\,
	cout => \Add0~11\);

-- Location: LCCOMB_X98_Y25_N22
\Add0~78\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~78_combout\ = (!\Equal0~10_combout\ & \Add0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~10_combout\,
	datad => \Add0~10_combout\,
	combout => \Add0~78_combout\);

-- Location: FF_X98_Y25_N23
\P_UEBERGANG:t_wait[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~78_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[5]~q\);

-- Location: LCCOMB_X97_Y25_N12
\Add0~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~12_combout\ = (\P_UEBERGANG:t_wait[6]~q\ & ((GND) # (!\Add0~11\))) # (!\P_UEBERGANG:t_wait[6]~q\ & (\Add0~11\ $ (GND)))
-- \Add0~13\ = CARRY((\P_UEBERGANG:t_wait[6]~q\) # (!\Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[6]~q\,
	datad => VCC,
	cin => \Add0~11\,
	combout => \Add0~12_combout\,
	cout => \Add0~13\);

-- Location: LCCOMB_X98_Y25_N12
\P_UEBERGANG:t_wait[6]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[6]~0_combout\ = (\Equal0~10_combout\ & (\P_UEBERGANG:t_wait[19]~0_combout\ & (\t_wait~0_combout\))) # (!\Equal0~10_combout\ & (((\Add0~12_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[19]~0_combout\,
	datab => \t_wait~0_combout\,
	datac => \Add0~12_combout\,
	datad => \Equal0~10_combout\,
	combout => \P_UEBERGANG:t_wait[6]~0_combout\);

-- Location: FF_X98_Y25_N13
\P_UEBERGANG:t_wait[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[6]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[6]~q\);

-- Location: LCCOMB_X97_Y25_N14
\Add0~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~14_combout\ = (\P_UEBERGANG:t_wait[7]~q\ & (\Add0~13\ & VCC)) # (!\P_UEBERGANG:t_wait[7]~q\ & (!\Add0~13\))
-- \Add0~15\ = CARRY((!\P_UEBERGANG:t_wait[7]~q\ & !\Add0~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[7]~q\,
	datad => VCC,
	cin => \Add0~13\,
	combout => \Add0~14_combout\,
	cout => \Add0~15\);

-- Location: LCCOMB_X98_Y24_N14
\P_UEBERGANG:t_wait[7]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[7]~0_combout\ = (\Equal0~10_combout\ & (\P_UEBERGANG:t_wait[19]~0_combout\ & (!\P_UEBERGANG:t_wait[19]~1_combout\))) # (!\Equal0~10_combout\ & (((\Add0~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101110100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~10_combout\,
	datab => \P_UEBERGANG:t_wait[19]~0_combout\,
	datac => \P_UEBERGANG:t_wait[19]~1_combout\,
	datad => \Add0~14_combout\,
	combout => \P_UEBERGANG:t_wait[7]~0_combout\);

-- Location: FF_X98_Y24_N15
\P_UEBERGANG:t_wait[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[7]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[7]~q\);

-- Location: LCCOMB_X97_Y25_N16
\Add0~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~16_combout\ = (\P_UEBERGANG:t_wait[8]~q\ & ((GND) # (!\Add0~15\))) # (!\P_UEBERGANG:t_wait[8]~q\ & (\Add0~15\ $ (GND)))
-- \Add0~17\ = CARRY((\P_UEBERGANG:t_wait[8]~q\) # (!\Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[8]~q\,
	datad => VCC,
	cin => \Add0~15\,
	combout => \Add0~16_combout\,
	cout => \Add0~17\);

-- Location: LCCOMB_X98_Y24_N0
\P_UEBERGANG:t_wait[8]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[8]~0_combout\ = (\Equal0~10_combout\ & (\t_wait~0_combout\ & ((\P_UEBERGANG:t_wait[19]~0_combout\)))) # (!\Equal0~10_combout\ & (((\Add0~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \t_wait~0_combout\,
	datab => \Add0~16_combout\,
	datac => \P_UEBERGANG:t_wait[19]~0_combout\,
	datad => \Equal0~10_combout\,
	combout => \P_UEBERGANG:t_wait[8]~0_combout\);

-- Location: FF_X98_Y24_N1
\P_UEBERGANG:t_wait[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[8]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[8]~q\);

-- Location: LCCOMB_X97_Y25_N18
\Add0~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~18_combout\ = (\P_UEBERGANG:t_wait[9]~q\ & (\Add0~17\ & VCC)) # (!\P_UEBERGANG:t_wait[9]~q\ & (!\Add0~17\))
-- \Add0~19\ = CARRY((!\P_UEBERGANG:t_wait[9]~q\ & !\Add0~17\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[9]~q\,
	datad => VCC,
	cin => \Add0~17\,
	combout => \Add0~18_combout\,
	cout => \Add0~19\);

-- Location: LCCOMB_X98_Y24_N2
\P_UEBERGANG:t_wait[9]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[9]~0_combout\ = (\Equal0~10_combout\ & (\P_UEBERGANG:t_wait[19]~1_combout\ & (\P_UEBERGANG:t_wait[19]~0_combout\))) # (!\Equal0~10_combout\ & (((\Add0~18_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[19]~1_combout\,
	datab => \Equal0~10_combout\,
	datac => \P_UEBERGANG:t_wait[19]~0_combout\,
	datad => \Add0~18_combout\,
	combout => \P_UEBERGANG:t_wait[9]~0_combout\);

-- Location: FF_X98_Y24_N3
\P_UEBERGANG:t_wait[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[9]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[9]~q\);

-- Location: LCCOMB_X97_Y25_N20
\Add0~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~20_combout\ = (\P_UEBERGANG:t_wait[10]~q\ & ((GND) # (!\Add0~19\))) # (!\P_UEBERGANG:t_wait[10]~q\ & (\Add0~19\ $ (GND)))
-- \Add0~21\ = CARRY((\P_UEBERGANG:t_wait[10]~q\) # (!\Add0~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[10]~q\,
	datad => VCC,
	cin => \Add0~19\,
	combout => \Add0~20_combout\,
	cout => \Add0~21\);

-- Location: LCCOMB_X98_Y24_N20
\P_UEBERGANG:t_wait[10]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[10]~0_combout\ = (\Equal0~10_combout\ & (\P_UEBERGANG:t_wait[19]~1_combout\ & (\P_UEBERGANG:t_wait[19]~0_combout\))) # (!\Equal0~10_combout\ & (((\Add0~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[19]~1_combout\,
	datab => \Equal0~10_combout\,
	datac => \P_UEBERGANG:t_wait[19]~0_combout\,
	datad => \Add0~20_combout\,
	combout => \P_UEBERGANG:t_wait[10]~0_combout\);

-- Location: FF_X98_Y24_N21
\P_UEBERGANG:t_wait[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[10]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[10]~q\);

-- Location: LCCOMB_X97_Y25_N22
\Add0~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~22_combout\ = (\P_UEBERGANG:t_wait[11]~q\ & (\Add0~21\ & VCC)) # (!\P_UEBERGANG:t_wait[11]~q\ & (!\Add0~21\))
-- \Add0~23\ = CARRY((!\P_UEBERGANG:t_wait[11]~q\ & !\Add0~21\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[11]~q\,
	datad => VCC,
	cin => \Add0~21\,
	combout => \Add0~22_combout\,
	cout => \Add0~23\);

-- Location: LCCOMB_X98_Y24_N22
\P_UEBERGANG:t_wait[11]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[11]~0_combout\ = (\Equal0~10_combout\ & (\t_wait~0_combout\ & (\P_UEBERGANG:t_wait[19]~0_combout\))) # (!\Equal0~10_combout\ & (((\Add0~22_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \t_wait~0_combout\,
	datab => \P_UEBERGANG:t_wait[19]~0_combout\,
	datac => \Equal0~10_combout\,
	datad => \Add0~22_combout\,
	combout => \P_UEBERGANG:t_wait[11]~0_combout\);

-- Location: FF_X98_Y24_N23
\P_UEBERGANG:t_wait[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[11]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[11]~q\);

-- Location: LCCOMB_X97_Y25_N24
\Add0~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~24_combout\ = (\P_UEBERGANG:t_wait[12]~q\ & ((GND) # (!\Add0~23\))) # (!\P_UEBERGANG:t_wait[12]~q\ & (\Add0~23\ $ (GND)))
-- \Add0~25\ = CARRY((\P_UEBERGANG:t_wait[12]~q\) # (!\Add0~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[12]~q\,
	datad => VCC,
	cin => \Add0~23\,
	combout => \Add0~24_combout\,
	cout => \Add0~25\);

-- Location: LCCOMB_X98_Y25_N4
\P_UEBERGANG:t_wait[12]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[12]~0_combout\ = (\Equal0~10_combout\ & (!\t_wait~0_combout\)) # (!\Equal0~10_combout\ & ((\Add0~24_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~10_combout\,
	datab => \t_wait~0_combout\,
	datad => \Add0~24_combout\,
	combout => \P_UEBERGANG:t_wait[12]~0_combout\);

-- Location: FF_X98_Y25_N5
\P_UEBERGANG:t_wait[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[12]~0_combout\,
	asdata => \P_UEBERGANG:t_wait[12]~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => \P_UEBERGANG:t_wait[17]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[12]~q\);

-- Location: LCCOMB_X97_Y25_N26
\Add0~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~26_combout\ = (\P_UEBERGANG:t_wait[13]~q\ & (\Add0~25\ & VCC)) # (!\P_UEBERGANG:t_wait[13]~q\ & (!\Add0~25\))
-- \Add0~27\ = CARRY((!\P_UEBERGANG:t_wait[13]~q\ & !\Add0~25\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[13]~q\,
	datad => VCC,
	cin => \Add0~25\,
	combout => \Add0~26_combout\,
	cout => \Add0~27\);

-- Location: LCCOMB_X98_Y24_N24
\Add0~77\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~77_combout\ = (\Add0~26_combout\ & !\Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~26_combout\,
	datad => \Equal0~10_combout\,
	combout => \Add0~77_combout\);

-- Location: FF_X98_Y24_N25
\P_UEBERGANG:t_wait[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~77_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[13]~q\);

-- Location: LCCOMB_X97_Y25_N28
\Add0~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~28_combout\ = (\P_UEBERGANG:t_wait[14]~q\ & ((GND) # (!\Add0~27\))) # (!\P_UEBERGANG:t_wait[14]~q\ & (\Add0~27\ $ (GND)))
-- \Add0~29\ = CARRY((\P_UEBERGANG:t_wait[14]~q\) # (!\Add0~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[14]~q\,
	datad => VCC,
	cin => \Add0~27\,
	combout => \Add0~28_combout\,
	cout => \Add0~29\);

-- Location: LCCOMB_X98_Y25_N14
\P_UEBERGANG:t_wait[14]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[14]~0_combout\ = (\Equal0~10_combout\ & (!\t_wait~0_combout\)) # (!\Equal0~10_combout\ & ((\Add0~28_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~10_combout\,
	datab => \t_wait~0_combout\,
	datad => \Add0~28_combout\,
	combout => \P_UEBERGANG:t_wait[14]~0_combout\);

-- Location: FF_X98_Y25_N15
\P_UEBERGANG:t_wait[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[14]~0_combout\,
	asdata => \P_UEBERGANG:t_wait[14]~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => \P_UEBERGANG:t_wait[17]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[14]~q\);

-- Location: LCCOMB_X97_Y25_N30
\Add0~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~30_combout\ = (\P_UEBERGANG:t_wait[15]~q\ & (\Add0~29\ & VCC)) # (!\P_UEBERGANG:t_wait[15]~q\ & (!\Add0~29\))
-- \Add0~31\ = CARRY((!\P_UEBERGANG:t_wait[15]~q\ & !\Add0~29\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[15]~q\,
	datad => VCC,
	cin => \Add0~29\,
	combout => \Add0~30_combout\,
	cout => \Add0~31\);

-- Location: LCCOMB_X98_Y25_N16
\P_UEBERGANG:t_wait[15]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[15]~0_combout\ = (\Equal0~10_combout\ & (\P_UEBERGANG:t_wait[17]~1_combout\)) # (!\Equal0~10_combout\ & ((\Add0~30_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~10_combout\,
	datab => \P_UEBERGANG:t_wait[17]~1_combout\,
	datad => \Add0~30_combout\,
	combout => \P_UEBERGANG:t_wait[15]~0_combout\);

-- Location: FF_X98_Y25_N17
\P_UEBERGANG:t_wait[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[15]~0_combout\,
	asdata => \P_UEBERGANG:t_wait[15]~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => \P_UEBERGANG:t_wait[17]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[15]~q\);

-- Location: LCCOMB_X97_Y24_N0
\Add0~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~32_combout\ = (\P_UEBERGANG:t_wait[16]~q\ & ((GND) # (!\Add0~31\))) # (!\P_UEBERGANG:t_wait[16]~q\ & (\Add0~31\ $ (GND)))
-- \Add0~33\ = CARRY((\P_UEBERGANG:t_wait[16]~q\) # (!\Add0~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[16]~q\,
	datad => VCC,
	cin => \Add0~31\,
	combout => \Add0~32_combout\,
	cout => \Add0~33\);

-- Location: LCCOMB_X98_Y25_N26
\P_UEBERGANG:t_wait[16]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[16]~0_combout\ = (\Equal0~10_combout\ & (\P_UEBERGANG:t_wait[17]~1_combout\)) # (!\Equal0~10_combout\ & ((\Add0~32_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~10_combout\,
	datab => \P_UEBERGANG:t_wait[17]~1_combout\,
	datad => \Add0~32_combout\,
	combout => \P_UEBERGANG:t_wait[16]~0_combout\);

-- Location: FF_X98_Y25_N27
\P_UEBERGANG:t_wait[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[16]~0_combout\,
	asdata => \P_UEBERGANG:t_wait[16]~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => \P_UEBERGANG:t_wait[17]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[16]~q\);

-- Location: LCCOMB_X97_Y24_N2
\Add0~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~34_combout\ = (\P_UEBERGANG:t_wait[17]~q\ & (\Add0~33\ & VCC)) # (!\P_UEBERGANG:t_wait[17]~q\ & (!\Add0~33\))
-- \Add0~35\ = CARRY((!\P_UEBERGANG:t_wait[17]~q\ & !\Add0~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[17]~q\,
	datad => VCC,
	cin => \Add0~33\,
	combout => \Add0~34_combout\,
	cout => \Add0~35\);

-- Location: LCCOMB_X98_Y25_N24
\P_UEBERGANG:t_wait[17]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[17]~0_combout\ = (\Equal0~10_combout\ & (\P_UEBERGANG:t_wait[17]~1_combout\)) # (!\Equal0~10_combout\ & ((\Add0~34_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~10_combout\,
	datab => \P_UEBERGANG:t_wait[17]~1_combout\,
	datad => \Add0~34_combout\,
	combout => \P_UEBERGANG:t_wait[17]~0_combout\);

-- Location: FF_X98_Y25_N25
\P_UEBERGANG:t_wait[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[17]~0_combout\,
	asdata => \P_UEBERGANG:t_wait[17]~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => \P_UEBERGANG:t_wait[17]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[17]~q\);

-- Location: LCCOMB_X97_Y24_N4
\Add0~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~36_combout\ = (\P_UEBERGANG:t_wait[18]~q\ & ((GND) # (!\Add0~35\))) # (!\P_UEBERGANG:t_wait[18]~q\ & (\Add0~35\ $ (GND)))
-- \Add0~37\ = CARRY((\P_UEBERGANG:t_wait[18]~q\) # (!\Add0~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[18]~q\,
	datad => VCC,
	cin => \Add0~35\,
	combout => \Add0~36_combout\,
	cout => \Add0~37\);

-- Location: LCCOMB_X98_Y24_N18
\Add0~76\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~76_combout\ = (\Add0~36_combout\ & !\Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~36_combout\,
	datad => \Equal0~10_combout\,
	combout => \Add0~76_combout\);

-- Location: FF_X98_Y24_N19
\P_UEBERGANG:t_wait[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~76_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[18]~q\);

-- Location: LCCOMB_X97_Y24_N6
\Add0~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~38_combout\ = (\P_UEBERGANG:t_wait[19]~q\ & (\Add0~37\ & VCC)) # (!\P_UEBERGANG:t_wait[19]~q\ & (!\Add0~37\))
-- \Add0~39\ = CARRY((!\P_UEBERGANG:t_wait[19]~q\ & !\Add0~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[19]~q\,
	datad => VCC,
	cin => \Add0~37\,
	combout => \Add0~38_combout\,
	cout => \Add0~39\);

-- Location: LCCOMB_X98_Y24_N8
\P_UEBERGANG:t_wait[19]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \P_UEBERGANG:t_wait[19]~2_combout\ = (\Equal0~10_combout\ & (\P_UEBERGANG:t_wait[19]~1_combout\ & (\P_UEBERGANG:t_wait[19]~0_combout\))) # (!\Equal0~10_combout\ & (((\Add0~38_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[19]~1_combout\,
	datab => \P_UEBERGANG:t_wait[19]~0_combout\,
	datac => \Add0~38_combout\,
	datad => \Equal0~10_combout\,
	combout => \P_UEBERGANG:t_wait[19]~2_combout\);

-- Location: FF_X98_Y24_N9
\P_UEBERGANG:t_wait[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \P_UEBERGANG:t_wait[19]~2_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[19]~q\);

-- Location: LCCOMB_X97_Y24_N8
\Add0~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~40_combout\ = (\P_UEBERGANG:t_wait[20]~q\ & ((GND) # (!\Add0~39\))) # (!\P_UEBERGANG:t_wait[20]~q\ & (\Add0~39\ $ (GND)))
-- \Add0~41\ = CARRY((\P_UEBERGANG:t_wait[20]~q\) # (!\Add0~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[20]~q\,
	datad => VCC,
	cin => \Add0~39\,
	combout => \Add0~40_combout\,
	cout => \Add0~41\);

-- Location: LCCOMB_X96_Y24_N14
\Add0~75\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~75_combout\ = (\Add0~40_combout\ & !\Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~40_combout\,
	datad => \Equal0~10_combout\,
	combout => \Add0~75_combout\);

-- Location: FF_X96_Y24_N15
\P_UEBERGANG:t_wait[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~75_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[20]~q\);

-- Location: LCCOMB_X97_Y24_N10
\Add0~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~42_combout\ = (\P_UEBERGANG:t_wait[21]~q\ & (\Add0~41\ & VCC)) # (!\P_UEBERGANG:t_wait[21]~q\ & (!\Add0~41\))
-- \Add0~43\ = CARRY((!\P_UEBERGANG:t_wait[21]~q\ & !\Add0~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[21]~q\,
	datad => VCC,
	cin => \Add0~41\,
	combout => \Add0~42_combout\,
	cout => \Add0~43\);

-- Location: LCCOMB_X96_Y24_N20
\Add0~74\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~74_combout\ = (\Add0~42_combout\ & !\Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~42_combout\,
	datad => \Equal0~10_combout\,
	combout => \Add0~74_combout\);

-- Location: FF_X96_Y24_N21
\P_UEBERGANG:t_wait[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~74_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[21]~q\);

-- Location: LCCOMB_X97_Y24_N12
\Add0~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~44_combout\ = (\P_UEBERGANG:t_wait[22]~q\ & ((GND) # (!\Add0~43\))) # (!\P_UEBERGANG:t_wait[22]~q\ & (\Add0~43\ $ (GND)))
-- \Add0~45\ = CARRY((\P_UEBERGANG:t_wait[22]~q\) # (!\Add0~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[22]~q\,
	datad => VCC,
	cin => \Add0~43\,
	combout => \Add0~44_combout\,
	cout => \Add0~45\);

-- Location: LCCOMB_X96_Y24_N30
\Add0~73\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~73_combout\ = (\Add0~44_combout\ & !\Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~44_combout\,
	datad => \Equal0~10_combout\,
	combout => \Add0~73_combout\);

-- Location: FF_X96_Y24_N31
\P_UEBERGANG:t_wait[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~73_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[22]~q\);

-- Location: LCCOMB_X97_Y24_N14
\Add0~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~46_combout\ = (\P_UEBERGANG:t_wait[23]~q\ & (\Add0~45\ & VCC)) # (!\P_UEBERGANG:t_wait[23]~q\ & (!\Add0~45\))
-- \Add0~47\ = CARRY((!\P_UEBERGANG:t_wait[23]~q\ & !\Add0~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[23]~q\,
	datad => VCC,
	cin => \Add0~45\,
	combout => \Add0~46_combout\,
	cout => \Add0~47\);

-- Location: LCCOMB_X96_Y24_N8
\Add0~72\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~72_combout\ = (\Add0~46_combout\ & !\Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~46_combout\,
	datad => \Equal0~10_combout\,
	combout => \Add0~72_combout\);

-- Location: FF_X96_Y24_N9
\P_UEBERGANG:t_wait[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~72_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[23]~q\);

-- Location: LCCOMB_X97_Y24_N16
\Add0~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~48_combout\ = (\P_UEBERGANG:t_wait[24]~q\ & ((GND) # (!\Add0~47\))) # (!\P_UEBERGANG:t_wait[24]~q\ & (\Add0~47\ $ (GND)))
-- \Add0~49\ = CARRY((\P_UEBERGANG:t_wait[24]~q\) # (!\Add0~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[24]~q\,
	datad => VCC,
	cin => \Add0~47\,
	combout => \Add0~48_combout\,
	cout => \Add0~49\);

-- Location: LCCOMB_X96_Y24_N28
\Add0~71\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~71_combout\ = (!\Equal0~10_combout\ & \Add0~48_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Equal0~10_combout\,
	datad => \Add0~48_combout\,
	combout => \Add0~71_combout\);

-- Location: FF_X96_Y24_N29
\P_UEBERGANG:t_wait[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~71_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[24]~q\);

-- Location: LCCOMB_X97_Y24_N18
\Add0~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~50_combout\ = (\P_UEBERGANG:t_wait[25]~q\ & (\Add0~49\ & VCC)) # (!\P_UEBERGANG:t_wait[25]~q\ & (!\Add0~49\))
-- \Add0~51\ = CARRY((!\P_UEBERGANG:t_wait[25]~q\ & !\Add0~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[25]~q\,
	datad => VCC,
	cin => \Add0~49\,
	combout => \Add0~50_combout\,
	cout => \Add0~51\);

-- Location: LCCOMB_X96_Y24_N18
\Add0~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~70_combout\ = (!\Equal0~10_combout\ & \Add0~50_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Equal0~10_combout\,
	datad => \Add0~50_combout\,
	combout => \Add0~70_combout\);

-- Location: FF_X96_Y24_N19
\P_UEBERGANG:t_wait[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~70_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[25]~q\);

-- Location: LCCOMB_X97_Y24_N20
\Add0~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~52_combout\ = (\P_UEBERGANG:t_wait[26]~q\ & ((GND) # (!\Add0~51\))) # (!\P_UEBERGANG:t_wait[26]~q\ & (\Add0~51\ $ (GND)))
-- \Add0~53\ = CARRY((\P_UEBERGANG:t_wait[26]~q\) # (!\Add0~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[26]~q\,
	datad => VCC,
	cin => \Add0~51\,
	combout => \Add0~52_combout\,
	cout => \Add0~53\);

-- Location: LCCOMB_X96_Y24_N4
\Add0~69\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~69_combout\ = (\Add0~52_combout\ & !\Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~52_combout\,
	datad => \Equal0~10_combout\,
	combout => \Add0~69_combout\);

-- Location: FF_X96_Y24_N5
\P_UEBERGANG:t_wait[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~69_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[26]~q\);

-- Location: LCCOMB_X97_Y24_N22
\Add0~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~54_combout\ = (\P_UEBERGANG:t_wait[27]~q\ & (\Add0~53\ & VCC)) # (!\P_UEBERGANG:t_wait[27]~q\ & (!\Add0~53\))
-- \Add0~55\ = CARRY((!\P_UEBERGANG:t_wait[27]~q\ & !\Add0~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[27]~q\,
	datad => VCC,
	cin => \Add0~53\,
	combout => \Add0~54_combout\,
	cout => \Add0~55\);

-- Location: LCCOMB_X96_Y24_N10
\Add0~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~68_combout\ = (!\Equal0~10_combout\ & \Add0~54_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Equal0~10_combout\,
	datad => \Add0~54_combout\,
	combout => \Add0~68_combout\);

-- Location: FF_X96_Y24_N11
\P_UEBERGANG:t_wait[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~68_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[27]~q\);

-- Location: LCCOMB_X97_Y24_N24
\Add0~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~56_combout\ = (\P_UEBERGANG:t_wait[28]~q\ & ((GND) # (!\Add0~55\))) # (!\P_UEBERGANG:t_wait[28]~q\ & (\Add0~55\ $ (GND)))
-- \Add0~57\ = CARRY((\P_UEBERGANG:t_wait[28]~q\) # (!\Add0~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[28]~q\,
	datad => VCC,
	cin => \Add0~55\,
	combout => \Add0~56_combout\,
	cout => \Add0~57\);

-- Location: LCCOMB_X96_Y24_N6
\Add0~67\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~67_combout\ = (!\Equal0~10_combout\ & \Add0~56_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Equal0~10_combout\,
	datad => \Add0~56_combout\,
	combout => \Add0~67_combout\);

-- Location: FF_X96_Y24_N7
\P_UEBERGANG:t_wait[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~67_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[28]~q\);

-- Location: LCCOMB_X97_Y24_N26
\Add0~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~58_combout\ = (\P_UEBERGANG:t_wait[29]~q\ & (\Add0~57\ & VCC)) # (!\P_UEBERGANG:t_wait[29]~q\ & (!\Add0~57\))
-- \Add0~59\ = CARRY((!\P_UEBERGANG:t_wait[29]~q\ & !\Add0~57\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[29]~q\,
	datad => VCC,
	cin => \Add0~57\,
	combout => \Add0~58_combout\,
	cout => \Add0~59\);

-- Location: LCCOMB_X96_Y24_N0
\Add0~66\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~66_combout\ = (!\Equal0~10_combout\ & \Add0~58_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Equal0~10_combout\,
	datad => \Add0~58_combout\,
	combout => \Add0~66_combout\);

-- Location: FF_X96_Y24_N1
\P_UEBERGANG:t_wait[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~66_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[29]~q\);

-- Location: LCCOMB_X97_Y24_N28
\Add0~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~60_combout\ = (\P_UEBERGANG:t_wait[30]~q\ & ((GND) # (!\Add0~59\))) # (!\P_UEBERGANG:t_wait[30]~q\ & (\Add0~59\ $ (GND)))
-- \Add0~61\ = CARRY((\P_UEBERGANG:t_wait[30]~q\) # (!\Add0~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \P_UEBERGANG:t_wait[30]~q\,
	datad => VCC,
	cin => \Add0~59\,
	combout => \Add0~60_combout\,
	cout => \Add0~61\);

-- Location: LCCOMB_X96_Y24_N22
\Add0~65\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~65_combout\ = (!\Equal0~10_combout\ & \Add0~60_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Equal0~10_combout\,
	datad => \Add0~60_combout\,
	combout => \Add0~65_combout\);

-- Location: FF_X96_Y24_N23
\P_UEBERGANG:t_wait[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~65_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[30]~q\);

-- Location: LCCOMB_X97_Y24_N30
\Add0~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~62_combout\ = \P_UEBERGANG:t_wait[31]~q\ $ (!\Add0~61\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[31]~q\,
	cin => \Add0~61\,
	combout => \Add0~62_combout\);

-- Location: LCCOMB_X96_Y24_N16
\Add0~64\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~64_combout\ = (\Add0~62_combout\ & !\Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~62_combout\,
	datad => \Equal0~10_combout\,
	combout => \Add0~64_combout\);

-- Location: FF_X96_Y24_N17
\P_UEBERGANG:t_wait[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~64_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:t_wait[31]~q\);

-- Location: LCCOMB_X96_Y24_N24
\Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = (!\P_UEBERGANG:t_wait[28]~q\ & (!\P_UEBERGANG:t_wait[29]~q\ & (!\P_UEBERGANG:t_wait[30]~q\ & !\P_UEBERGANG:t_wait[31]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[28]~q\,
	datab => \P_UEBERGANG:t_wait[29]~q\,
	datac => \P_UEBERGANG:t_wait[30]~q\,
	datad => \P_UEBERGANG:t_wait[31]~q\,
	combout => \Equal0~0_combout\);

-- Location: LCCOMB_X98_Y24_N28
\Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~3_combout\ = (!\P_UEBERGANG:t_wait[11]~q\ & (!\P_UEBERGANG:t_wait[13]~q\ & (!\P_UEBERGANG:t_wait[19]~q\ & !\P_UEBERGANG:t_wait[18]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[11]~q\,
	datab => \P_UEBERGANG:t_wait[13]~q\,
	datac => \P_UEBERGANG:t_wait[19]~q\,
	datad => \P_UEBERGANG:t_wait[18]~q\,
	combout => \Equal0~3_combout\);

-- Location: LCCOMB_X96_Y24_N26
\Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = (!\P_UEBERGANG:t_wait[27]~q\ & (!\P_UEBERGANG:t_wait[25]~q\ & (!\P_UEBERGANG:t_wait[26]~q\ & !\P_UEBERGANG:t_wait[24]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[27]~q\,
	datab => \P_UEBERGANG:t_wait[25]~q\,
	datac => \P_UEBERGANG:t_wait[26]~q\,
	datad => \P_UEBERGANG:t_wait[24]~q\,
	combout => \Equal0~1_combout\);

-- Location: LCCOMB_X96_Y24_N12
\Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = (!\P_UEBERGANG:t_wait[22]~q\ & (!\P_UEBERGANG:t_wait[23]~q\ & (!\P_UEBERGANG:t_wait[20]~q\ & !\P_UEBERGANG:t_wait[21]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[22]~q\,
	datab => \P_UEBERGANG:t_wait[23]~q\,
	datac => \P_UEBERGANG:t_wait[20]~q\,
	datad => \P_UEBERGANG:t_wait[21]~q\,
	combout => \Equal0~2_combout\);

-- Location: LCCOMB_X98_Y24_N26
\Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~4_combout\ = (\Equal0~0_combout\ & (\Equal0~3_combout\ & (\Equal0~1_combout\ & \Equal0~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~0_combout\,
	datab => \Equal0~3_combout\,
	datac => \Equal0~1_combout\,
	datad => \Equal0~2_combout\,
	combout => \Equal0~4_combout\);

-- Location: LCCOMB_X98_Y25_N30
\Equal0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~8_combout\ = (!\P_UEBERGANG:t_wait[16]~q\ & (!\P_UEBERGANG:t_wait[0]~q\ & (!\P_UEBERGANG:t_wait[17]~q\ & !\P_UEBERGANG:t_wait[1]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[16]~q\,
	datab => \P_UEBERGANG:t_wait[0]~q\,
	datac => \P_UEBERGANG:t_wait[17]~q\,
	datad => \P_UEBERGANG:t_wait[1]~q\,
	combout => \Equal0~8_combout\);

-- Location: LCCOMB_X98_Y24_N4
\Equal0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~5_combout\ = (!\P_UEBERGANG:t_wait[8]~q\ & (!\P_UEBERGANG:t_wait[9]~q\ & (!\P_UEBERGANG:t_wait[7]~q\ & !\P_UEBERGANG:t_wait[10]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[8]~q\,
	datab => \P_UEBERGANG:t_wait[9]~q\,
	datac => \P_UEBERGANG:t_wait[7]~q\,
	datad => \P_UEBERGANG:t_wait[10]~q\,
	combout => \Equal0~5_combout\);

-- Location: LCCOMB_X98_Y25_N0
\Equal0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~6_combout\ = (!\P_UEBERGANG:t_wait[6]~q\ & (!\P_UEBERGANG:t_wait[3]~q\ & (!\P_UEBERGANG:t_wait[5]~q\ & !\P_UEBERGANG:t_wait[2]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[6]~q\,
	datab => \P_UEBERGANG:t_wait[3]~q\,
	datac => \P_UEBERGANG:t_wait[5]~q\,
	datad => \P_UEBERGANG:t_wait[2]~q\,
	combout => \Equal0~6_combout\);

-- Location: LCCOMB_X98_Y24_N30
\Equal0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~7_combout\ = (\Equal0~5_combout\ & \Equal0~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Equal0~5_combout\,
	datad => \Equal0~6_combout\,
	combout => \Equal0~7_combout\);

-- Location: LCCOMB_X98_Y25_N28
\Equal0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~9_combout\ = (!\P_UEBERGANG:t_wait[4]~q\ & (!\P_UEBERGANG:t_wait[14]~q\ & (!\P_UEBERGANG:t_wait[12]~q\ & !\P_UEBERGANG:t_wait[15]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \P_UEBERGANG:t_wait[4]~q\,
	datab => \P_UEBERGANG:t_wait[14]~q\,
	datac => \P_UEBERGANG:t_wait[12]~q\,
	datad => \P_UEBERGANG:t_wait[15]~q\,
	combout => \Equal0~9_combout\);

-- Location: LCCOMB_X98_Y24_N12
\Equal0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~10_combout\ = (\Equal0~4_combout\ & (\Equal0~8_combout\ & (\Equal0~7_combout\ & \Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~4_combout\,
	datab => \Equal0~8_combout\,
	datac => \Equal0~7_combout\,
	datad => \Equal0~9_combout\,
	combout => \Equal0~10_combout\);

-- Location: LCCOMB_X99_Y24_N24
\s_state_init_next.Func~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_init_next.Func~0_combout\ = (\nRST~input_o\ & \Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \nRST~input_o\,
	datad => \Equal0~10_combout\,
	combout => \s_state_init_next.Func~0_combout\);

-- Location: LCCOMB_X99_Y24_N2
\s_state_next.Init~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_next.Init~0_combout\ = (\s_state_next.Ready~2_combout\ & (!\s_state_current.Start~q\ & ((\s_state_init_next.Func~0_combout\)))) # (!\s_state_next.Ready~2_combout\ & ((\s_state_next.Init~q\) # ((!\s_state_current.Start~q\ & 
-- \s_state_init_next.Func~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_next.Ready~2_combout\,
	datab => \s_state_current.Start~q\,
	datac => \s_state_next.Init~q\,
	datad => \s_state_init_next.Func~0_combout\,
	combout => \s_state_next.Init~0_combout\);

-- Location: FF_X99_Y24_N3
\s_state_next.Init\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_state_next.Init~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_next.Init~q\);

-- Location: LCCOMB_X99_Y24_N12
\s_state_current.Init~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_current.Init~feeder_combout\ = \s_state_next.Init~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \s_state_next.Init~q\,
	combout => \s_state_current.Init~feeder_combout\);

-- Location: FF_X99_Y24_N13
\s_state_current.Init\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_state_current.Init~feeder_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Init~q\);

-- Location: LCCOMB_X100_Y24_N8
\s_state_next.Ready~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_next.Ready~0_combout\ = (\s_state_current.Send~q\ & !\s_state_send_current~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Send~q\,
	datad => \s_state_send_current~q\,
	combout => \s_state_next.Ready~0_combout\);

-- Location: LCCOMB_X99_Y24_N6
\s_state_next.Ready~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_next.Ready~2_combout\ = (!\s_state_next.Ready~0_combout\ & (\s_state_init_next.Func~0_combout\ & ((\s_state_init_current.Finish~q\) # (!\s_state_current.Init~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Init~q\,
	datab => \s_state_init_current.Finish~q\,
	datac => \s_state_next.Ready~0_combout\,
	datad => \s_state_init_next.Func~0_combout\,
	combout => \s_state_next.Ready~2_combout\);

-- Location: FF_X100_Y24_N11
\s_state_next.Send\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_busy~1_combout\,
	ena => \s_state_next.Ready~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_next.Send~q\);

-- Location: LCCOMB_X100_Y24_N20
\s_state_current.Send~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_current.Send~feeder_combout\ = \s_state_next.Send~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \s_state_next.Send~q\,
	combout => \s_state_current.Send~feeder_combout\);

-- Location: FF_X100_Y24_N21
\s_state_current.Send\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_state_current.Send~feeder_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Send~q\);

-- Location: LCCOMB_X100_Y24_N30
\s_state_next.Ready~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_next.Ready~1_combout\ = (\s_state_current.Ready~q\ & (!\WEN~input_o\)) # (!\s_state_current.Ready~q\ & (((\s_state_current.Send~q\) # (\s_state_current.Init~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Ready~q\,
	datab => \WEN~input_o\,
	datac => \s_state_current.Send~q\,
	datad => \s_state_current.Init~q\,
	combout => \s_state_next.Ready~1_combout\);

-- Location: FF_X100_Y24_N31
\s_state_next.Ready\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_state_next.Ready~1_combout\,
	ena => \s_state_next.Ready~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_next.Ready~q\);

-- Location: FF_X99_Y24_N15
\s_state_current.Ready\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \s_state_next.Ready~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Ready~q\);

-- Location: FF_X103_Y24_N29
\P_UEBERGANG:data[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \data~1_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:data[0]~q\);

-- Location: LCCOMB_X100_Y24_N26
\en~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \en~0_combout\ = (\s_state_current.Init~q\ & (!\s_state_init_current.Finish~q\ & \s_state_init_current.Reset~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Init~q\,
	datab => \s_state_init_current.Finish~q\,
	datad => \s_state_init_current.Reset~q\,
	combout => \en~0_combout\);

-- Location: LCCOMB_X103_Y24_N10
\data~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~0_combout\ = ((!\en~0_combout\ & ((!\s_state_current.Ready~q\) # (!\WEN~input_o\)))) # (!\Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \WEN~input_o\,
	datab => \en~0_combout\,
	datac => \s_state_current.Ready~q\,
	datad => \Equal0~10_combout\,
	combout => \data~0_combout\);

-- Location: LCCOMB_X103_Y24_N28
\data~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~1_combout\ = (\data~0_combout\ & (((\P_UEBERGANG:data[0]~q\)))) # (!\data~0_combout\ & (\DI[0]~input_o\ & (\s_state_current.Ready~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DI[0]~input_o\,
	datab => \s_state_current.Ready~q\,
	datac => \P_UEBERGANG:data[0]~q\,
	datad => \data~0_combout\,
	combout => \data~1_combout\);

-- Location: LCCOMB_X103_Y24_N12
\s_data[0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_data[0]~feeder_combout\ = \data~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \data~1_combout\,
	combout => \s_data[0]~feeder_combout\);

-- Location: FF_X103_Y24_N13
\s_data[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_data[0]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(0));

-- Location: LCCOMB_X99_Y24_N10
\data~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~2_combout\ = ((!\s_state_current.Init~q\ & ((!\WEN~input_o\) # (!\s_state_current.Ready~q\)))) # (!\Equal0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Init~q\,
	datab => \s_state_current.Ready~q\,
	datac => \WEN~input_o\,
	datad => \Equal0~10_combout\,
	combout => \data~2_combout\);

-- Location: IOIBUF_X115_Y27_N8
\DI[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_DI(1),
	o => \DI[1]~input_o\);

-- Location: LCCOMB_X100_Y24_N24
\data~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~4_combout\ = (\s_state_current.Ready~q\ & ((\DI[1]~input_o\))) # (!\s_state_current.Ready~q\ & (\s_state_init_current.Entry~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Ready~q\,
	datab => \s_state_init_current.Entry~q\,
	datad => \DI[1]~input_o\,
	combout => \data~4_combout\);

-- Location: FF_X99_Y24_N27
\P_UEBERGANG:data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \data~5_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:data[1]~q\);

-- Location: LCCOMB_X99_Y24_N20
\data~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~3_combout\ = (\data~2_combout\) # ((!\s_state_current.Ready~q\ & ((\s_state_init_current.Finish~q\) # (!\s_state_init_current.Reset~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_init_current.Reset~q\,
	datab => \s_state_init_current.Finish~q\,
	datac => \s_state_current.Ready~q\,
	datad => \data~2_combout\,
	combout => \data~3_combout\);

-- Location: LCCOMB_X99_Y24_N26
\data~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~5_combout\ = (\data~2_combout\ & (((\P_UEBERGANG:data[1]~q\ & \data~3_combout\)))) # (!\data~2_combout\ & ((\data~4_combout\) # ((\P_UEBERGANG:data[1]~q\ & \data~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \data~2_combout\,
	datab => \data~4_combout\,
	datac => \P_UEBERGANG:data[1]~q\,
	datad => \data~3_combout\,
	combout => \data~5_combout\);

-- Location: FF_X99_Y24_N25
\s_data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \data~5_combout\,
	sload => VCC,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(1));

-- Location: IOIBUF_X115_Y27_N1
\DI[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_DI(2),
	o => \DI[2]~input_o\);

-- Location: FF_X103_Y24_N21
\P_UEBERGANG:data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \data~6_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:data[2]~q\);

-- Location: LCCOMB_X103_Y24_N20
\data~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~6_combout\ = (\data~0_combout\ & (((\P_UEBERGANG:data[2]~q\)))) # (!\data~0_combout\ & ((\DI[2]~input_o\) # ((!\s_state_current.Ready~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DI[2]~input_o\,
	datab => \s_state_current.Ready~q\,
	datac => \P_UEBERGANG:data[2]~q\,
	datad => \data~0_combout\,
	combout => \data~6_combout\);

-- Location: LCCOMB_X103_Y24_N14
\s_data[2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_data[2]~feeder_combout\ = \data~6_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \data~6_combout\,
	combout => \s_data[2]~feeder_combout\);

-- Location: FF_X103_Y24_N15
\s_data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_data[2]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(2));

-- Location: IOIBUF_X115_Y28_N1
\DI[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_DI(3),
	o => \DI[3]~input_o\);

-- Location: LCCOMB_X100_Y24_N14
\data~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~7_combout\ = (\s_state_current.Ready~q\ & (((\DI[3]~input_o\)))) # (!\s_state_current.Ready~q\ & ((\s_state_init_current.Func~q\) # ((\s_state_init_current.Disp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_init_current.Func~q\,
	datab => \DI[3]~input_o\,
	datac => \s_state_init_current.Disp~q\,
	datad => \s_state_current.Ready~q\,
	combout => \data~7_combout\);

-- Location: FF_X99_Y24_N31
\P_UEBERGANG:data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \data~8_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:data[3]~q\);

-- Location: LCCOMB_X99_Y24_N30
\data~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~8_combout\ = (\data~2_combout\ & (((\P_UEBERGANG:data[3]~q\ & \data~3_combout\)))) # (!\data~2_combout\ & ((\data~7_combout\) # ((\P_UEBERGANG:data[3]~q\ & \data~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \data~2_combout\,
	datab => \data~7_combout\,
	datac => \P_UEBERGANG:data[3]~q\,
	datad => \data~3_combout\,
	combout => \data~8_combout\);

-- Location: FF_X99_Y24_N1
\s_data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \data~8_combout\,
	sload => VCC,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(3));

-- Location: IOIBUF_X115_Y28_N8
\DI[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_DI(4),
	o => \DI[4]~input_o\);

-- Location: LCCOMB_X100_Y24_N16
\data~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~9_combout\ = (\s_state_current.Ready~q\ & (\DI[4]~input_o\)) # (!\s_state_current.Ready~q\ & ((\s_state_init_current.Func~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DI[4]~input_o\,
	datac => \s_state_init_current.Func~q\,
	datad => \s_state_current.Ready~q\,
	combout => \data~9_combout\);

-- Location: FF_X99_Y24_N19
\P_UEBERGANG:data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \data~10_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:data[4]~q\);

-- Location: LCCOMB_X99_Y24_N18
\data~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~10_combout\ = (\data~2_combout\ & (((\P_UEBERGANG:data[4]~q\ & \data~3_combout\)))) # (!\data~2_combout\ & ((\data~9_combout\) # ((\P_UEBERGANG:data[4]~q\ & \data~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \data~2_combout\,
	datab => \data~9_combout\,
	datac => \P_UEBERGANG:data[4]~q\,
	datad => \data~3_combout\,
	combout => \data~10_combout\);

-- Location: LCCOMB_X99_Y24_N28
\s_data[4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_data[4]~feeder_combout\ = \data~10_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \data~10_combout\,
	combout => \s_data[4]~feeder_combout\);

-- Location: FF_X99_Y24_N29
\s_data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_data[4]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(4));

-- Location: IOIBUF_X115_Y23_N1
\DI[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_DI(5),
	o => \DI[5]~input_o\);

-- Location: LCCOMB_X100_Y24_N22
\data~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~11_combout\ = (\s_state_current.Ready~q\ & (\DI[5]~input_o\)) # (!\s_state_current.Ready~q\ & ((\s_state_init_current.Func~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DI[5]~input_o\,
	datac => \s_state_init_current.Func~q\,
	datad => \s_state_current.Ready~q\,
	combout => \data~11_combout\);

-- Location: FF_X99_Y24_N5
\P_UEBERGANG:data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \data~12_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:data[5]~q\);

-- Location: LCCOMB_X99_Y24_N4
\data~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~12_combout\ = (\data~2_combout\ & (((\P_UEBERGANG:data[5]~q\ & \data~3_combout\)))) # (!\data~2_combout\ & ((\data~11_combout\) # ((\P_UEBERGANG:data[5]~q\ & \data~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \data~2_combout\,
	datab => \data~11_combout\,
	datac => \P_UEBERGANG:data[5]~q\,
	datad => \data~3_combout\,
	combout => \data~12_combout\);

-- Location: FF_X99_Y24_N9
\s_data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \data~12_combout\,
	sload => VCC,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(5));

-- Location: IOIBUF_X115_Y26_N15
\DI[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_DI(6),
	o => \DI[6]~input_o\);

-- Location: FF_X103_Y24_N3
\P_UEBERGANG:data[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \data~13_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:data[6]~q\);

-- Location: LCCOMB_X103_Y24_N2
\data~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~13_combout\ = (\data~0_combout\ & (((\P_UEBERGANG:data[6]~q\)))) # (!\data~0_combout\ & (\DI[6]~input_o\ & (\s_state_current.Ready~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DI[6]~input_o\,
	datab => \s_state_current.Ready~q\,
	datac => \P_UEBERGANG:data[6]~q\,
	datad => \data~0_combout\,
	combout => \data~13_combout\);

-- Location: LCCOMB_X103_Y24_N0
\s_data[6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_data[6]~feeder_combout\ = \data~13_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \data~13_combout\,
	combout => \s_data[6]~feeder_combout\);

-- Location: FF_X103_Y24_N1
\s_data[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_data[6]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(6));

-- Location: IOIBUF_X115_Y26_N22
\DI[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_DI(7),
	o => \DI[7]~input_o\);

-- Location: FF_X103_Y24_N25
\P_UEBERGANG:data[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \data~14_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:data[7]~q\);

-- Location: LCCOMB_X103_Y24_N24
\data~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \data~14_combout\ = (\data~0_combout\ & (((\P_UEBERGANG:data[7]~q\)))) # (!\data~0_combout\ & (\DI[7]~input_o\ & (\s_state_current.Ready~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DI[7]~input_o\,
	datab => \s_state_current.Ready~q\,
	datac => \P_UEBERGANG:data[7]~q\,
	datad => \data~0_combout\,
	combout => \data~14_combout\);

-- Location: LCCOMB_X103_Y24_N18
\s_data[7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_data[7]~feeder_combout\ = \data~14_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \data~14_combout\,
	combout => \s_data[7]~feeder_combout\);

-- Location: FF_X103_Y24_N19
\s_data[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_data[7]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(7));

-- Location: LCCOMB_X101_Y24_N28
\en~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \en~1_combout\ = (!\s_state_current.Send~q\ & (!\s_state_init_current.Finish~q\ & (\s_state_current.Init~q\ & \s_state_init_current.Reset~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Send~q\,
	datab => \s_state_init_current.Finish~q\,
	datac => \s_state_current.Init~q\,
	datad => \s_state_init_current.Reset~q\,
	combout => \en~1_combout\);

-- Location: FF_X101_Y24_N7
\P_UEBERGANG:en\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \en~2_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:en~q\);

-- Location: LCCOMB_X101_Y24_N6
\en~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \en~2_combout\ = (\Equal0~10_combout\ & ((\s_state_next.Ready~0_combout\) # ((\en~1_combout\) # (\P_UEBERGANG:en~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_next.Ready~0_combout\,
	datab => \en~1_combout\,
	datac => \P_UEBERGANG:en~q\,
	datad => \Equal0~10_combout\,
	combout => \en~2_combout\);

-- Location: LCCOMB_X101_Y24_N12
\s_en~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_en~feeder_combout\ = \en~2_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \en~2_combout\,
	combout => \s_en~feeder_combout\);

-- Location: FF_X101_Y24_N13
s_en : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_en~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_en~q\);

-- Location: LCCOMB_X101_Y24_N26
\rs~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \rs~2_combout\ = (\s_state_current.Send~q\) # (\s_state_init_current.Finish~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Send~q\,
	datad => \s_state_init_current.Finish~q\,
	combout => \rs~2_combout\);

-- Location: FF_X101_Y24_N25
\P_UEBERGANG:rs\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \rs~4_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \P_UEBERGANG:rs~q\);

-- Location: LCCOMB_X101_Y24_N2
\rs~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \rs~5_combout\ = (\Equal0~10_combout\ & ((\s_state_current.Send~q\ & ((\s_state_send_current~q\))) # (!\s_state_current.Send~q\ & (\s_state_current.Init~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Init~q\,
	datab => \s_state_current.Send~q\,
	datac => \s_state_send_current~q\,
	datad => \Equal0~10_combout\,
	combout => \rs~5_combout\);

-- Location: LCCOMB_X101_Y24_N24
\rs~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \rs~4_combout\ = (\rs~5_combout\ & ((\rs~2_combout\) # ((\s_state_init_current.Reset~q\ & \P_UEBERGANG:rs~q\)))) # (!\rs~5_combout\ & (((\P_UEBERGANG:rs~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rs~2_combout\,
	datab => \s_state_init_current.Reset~q\,
	datac => \P_UEBERGANG:rs~q\,
	datad => \rs~5_combout\,
	combout => \rs~4_combout\);

-- Location: LCCOMB_X101_Y24_N22
\s_rs~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_rs~feeder_combout\ = \rs~4_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \rs~4_combout\,
	combout => \s_rs~feeder_combout\);

-- Location: FF_X101_Y24_N23
s_rs : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_rs~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_rs~q\);

-- Location: LCCOMB_X100_Y24_N4
\s_busy~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_busy~0_combout\ = (\s_state_current.Ready~q\ & ((\s_state_init_next.Func~0_combout\ & (!\WEN~input_o\)) # (!\s_state_init_next.Func~0_combout\ & ((\s_busy~q\))))) # (!\s_state_current.Ready~q\ & (((\s_busy~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Ready~q\,
	datab => \WEN~input_o\,
	datac => \s_busy~q\,
	datad => \s_state_init_next.Func~0_combout\,
	combout => \s_busy~0_combout\);

-- Location: FF_X100_Y24_N5
s_busy : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_busy~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_busy~q\);

ww_lcd_data(0) <= \lcd_data[0]~output_o\;

ww_lcd_data(1) <= \lcd_data[1]~output_o\;

ww_lcd_data(2) <= \lcd_data[2]~output_o\;

ww_lcd_data(3) <= \lcd_data[3]~output_o\;

ww_lcd_data(4) <= \lcd_data[4]~output_o\;

ww_lcd_data(5) <= \lcd_data[5]~output_o\;

ww_lcd_data(6) <= \lcd_data[6]~output_o\;

ww_lcd_data(7) <= \lcd_data[7]~output_o\;

ww_lcd_en <= \lcd_en~output_o\;

ww_lcd_rw <= \lcd_rw~output_o\;

ww_lcd_rs <= \lcd_rs~output_o\;

ww_busy <= \busy~output_o\;
END structure;


