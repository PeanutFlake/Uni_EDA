LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_LCDDeviceController IS
END ENTITY;

ARCHITECTURE Testbench OF tb_LCDDeviceController IS
	COMPONENT LCDDeviceController IS
		GENERIC (
			F_CLK				:	Integer range 0 to 2147483647 := 50*(10**6);
			LINEWIDTH		:	Integer range 0 to 48			:=	16
		);
		PORT (
			CLK, nRST, WEN	:	IN		std_logic;
			DI					:	IN		std_logic_vector(7 downto 0);
			lcd_data			:	OUT	std_logic_vector(7 downto 0);
			lcd_en, lcd_rw, lcd_rs	:	OUT	std_logic
		);
	END COMPONENT;
	
	SIGNAL s_clk, s_rst	:	std_logic	:= '1';
	SIGNAL s_en				:	std_logic	:= '0';
	
	SIGNAL s_dat 			:	std_logic_vector(7 downto 0)	:= (Others => '0');
	
BEGIN

	DUT	:	LCDDeviceController
	GENERIC MAP(50*(10**6), 2)
	PORT MAP (
		CLK => s_clk, 
		nRST => s_rst, 
		WEN => s_en, 
		DI => s_dat
	);
	
	s_clk <= not s_clk after 20ns;
	s_rst <= '0' after 50ns, '1' after 100ns;

	s_dat <= "00110000" after 42ms, "00100000" after 45ms, "00110000" after 50ms;
	s_en <= '1' after 42005us, '0' after 42100us, '1' after 45005us, '0' after 45100us, '1' after 50005us, '0' after 50100us;
	
END Testbench;