LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY lcd_controller IS
  PORT(
    clk        : IN    STD_LOGIC;  --Systemclock
    reset_n    : IN    STD_LOGIC;  --Reset
    lcd_enable : IN    STD_LOGIC;  --LCD Enable
    lcd_bus    : IN    STD_LOGIC_VECTOR(9 DOWNTO 0);  --Datenregister
    busy       : OUT   STD_LOGIC := '1';  --LCD Busy
    rw, rs, e  : OUT   STD_LOGIC;  --RW, Daten oder setup, Enable
    lcd_data   : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0)); --Datenbus des LCDs
END lcd_controller;

ARCHITECTURE controller OF lcd_controller IS
  TYPE CONTROL IS(power_up, initialize, ready, send);
  SIGNAL    state      : CONTROL;
  CONSTANT  freq       : INTEGER := 50; --Systemclock in MHz
BEGIN
  PROCESS(clk)
    VARIABLE clk_count : INTEGER := 0; --Counter für's Timing
  BEGIN
  IF(clk'EVENT and clk = '1') THEN
    
      CASE state IS
        
        --Warte 50ms für die Stabilisierung der Versorgungsspannung
        WHEN power_up =>
          busy <= '1';
          IF(clk_count < (50000 * freq)) THEN    --warte 50ms
            clk_count := clk_count + 1;
            state <= power_up;
          ELSE                                   --Stabilisierung ist durch
            clk_count := 0;
            rs <= '0';
            rw <= '0';
            lcd_data <= "00110000";
            state <= initialize;
          END IF;
          
        --Initialisierung  
        WHEN initialize =>
          busy <= '1';
          clk_count := clk_count + 1;
          IF(clk_count < (10 * freq)) THEN       
            lcd_data <= "00111100";      -- 2 Zeilen, Cursor ein
            e <= '1';
            state <= initialize;
          ELSIF(clk_count < (60 * freq)) THEN    --warte 50ms
            lcd_data <= "00000000";
            e <= '0';
            state <= initialize;
          ELSIF(clk_count < (70 * freq)) THEN   
            lcd_data <= "00001100";      --Display ein, Cursor aus, Blinken aus           
            e <= '1';
            state <= initialize;
          ELSIF(clk_count < (120 * freq)) THEN   --warte 50ms
            lcd_data <= "00000000";
            e <= '0';
            state <= initialize;
          ELSIF(clk_count < (130 * freq)) THEN   --Display löschen
            lcd_data <= "00000001";
            e <= '1';
            state <= initialize;
          ELSIF(clk_count < (2130 * freq)) THEN  --warte 2ms
            lcd_data <= "00000000";
            e <= '0';
            state <= initialize;
          ELSIF(clk_count < (2140 * freq)) THEN 
            lcd_data <= "00000110";      --Incrementmodus, Shift aus
            e <= '1';
            state <= initialize;
          ELSIF(clk_count < (2200 * freq)) THEN  --warte 60us
            lcd_data <= "00000000";
            e <= '0';
            state <= initialize;
          ELSE                                   --DONE
            clk_count := 0;
            busy <= '0';
            state <= ready;
          END IF;    
       
        --Warte aufs Enable
        WHEN ready =>
          IF(lcd_enable = '1') THEN
            busy <= '1';
            rs <= lcd_bus(9);
            rw <= lcd_bus(8);
            lcd_data <= lcd_bus(7 DOWNTO 0);
            clk_count := 0;            
            state <= send;
          ELSE
            busy <= '0';
            rs <= '0';
            rw <= '0';
            lcd_data <= "00000000";
            clk_count := 0;
            state <= ready;
          END IF;
               
        WHEN send =>
        busy <= '1';
        IF(clk_count < (50 * freq)) THEN
           busy <= '1';
           IF(clk_count < freq) THEN
            e <= '0';
           ELSIF(clk_count < (14 * freq)) THEN
            e <= '1';
           ELSIF(clk_count < (27 * freq)) THEN
            e <= '0';
           END IF;
           clk_count := clk_count + 1;
           state <= send;
        ELSE
          clk_count := 0;
          state <= ready;
        END IF;

      END CASE;    
    
      --Reset
      IF(reset_n = '0') THEN
          state <= power_up;
      END IF;
    
    END IF;
  END PROCESS;
END controller;
