ENTITY LCDDisplay IS
	PORT (
		CLK, nRST, EN	:	IN		std_logic;
		W_Pos, W_Byte	:	IN		std_logic_vector(7 downto 0);
		LCD_RW, LCD_EN	:	OUT	std_logic;
		LCD_D				:	OUT	std_logic_vector(7 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF LCDDisplay IS

BEGIN

	LCDWrite : PROCESS(CLK, nRST)
	
	BEGIN
	
		if ( nRST = '0' ) then
			-- Reset
		else
			if ( rising_edge(CLK) ) then
				
			else
			
			end if;
		end if;
	
	END PROCESS LCDWrite;

END Behaviour;