LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_LCDCodes IS
END ENTITY;

ARCHITECTURE Testbench OF tb_LCDCodes IS

	COMPONENT LCDCodes IS
		PORT (
			nRST, CLK					:	IN		std_logic;
			BCD, Gray, Aiken			:	IN		std_logic_vector(3 downto 0);
			
			F_RDY							:	OUT	std_logic;
			-- LCD Output Connections
			LCD_EN, LCD_RS, LCD_RW	:	OUT	std_logic;
			LCD_DATA						:	OUT	std_logic_vector(7 downto 0)
		);
	END COMPONENT;

	SIGNAL s_rst									:	std_logic	:= '1';
	SIGNAL s_clk, s_rdy, s_en, s_rs, s_rw	:	std_logic	:= '0';
	
	SIGNAL s_bcd, s_gray, s_aiken				:	std_logic_vector(3 downto 0) := (Others => '0');
	SIGNAL s_dat									:	std_logic_vector(7 downto 0) := (Others => '0');
	
BEGIN

	DUT	:	LCDCodes
	PORT MAP (s_rst, s_clk, s_bcd, s_gray, s_aiken, s_rdy, s_en, s_rs, s_rw, s_dat);

	s_clk <= not s_clk after 20ns;
	
	
	
END Testbench;