LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY LCDCodes IS
	GENERIC (
		Hold_Time					:	Integer	:=	2500
	);
	PORT (
		nRST, CLK					:	IN		std_logic;
		BCD, Gray, Aiken			:	IN		std_logic_vector(3 downto 0);
		
		F_RDY							:	OUT	std_logic;
		-- LCD Output Connections
		LCD_EN, LCD_RS, LCD_RW	:	OUT	std_logic;
		LCD_DATA						:	OUT	std_logic_vector(7 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF LCDCodes IS

	TYPE State IS (Startup, Reset, Init_F, Init_D, Init_E, Ready, Hold);
	
	SIGNAL s_state_current, s_state_next	:	State	:= Startup;
	
	SIGNAL s_rs, s_en, s_rw, s_rdy			:	std_logic	:= '0';
	SIGNAL s_data									:	std_logic_vector(7 downto 0)	:= (Others => '0');

BEGIN

	UEBERGANG : PROCESS (s_state_current)
		VARIABLE ht		:	Integer	range 0 to Hold_Time	:= 0;
	BEGIN
		case s_state_current is		
		when Startup =>
			s_state_next <= Reset;
		when Reset =>
			s_state_next <= Init_F;
		when Init_F =>
			s_state_next <= Init_D;
		when Init_D =>
			s_state_next <= Init_E;
		when Init_E =>
			s_state_next <= Ready;
		when Ready =>
			s_rs <= '1';
			s_rw <= '0';
			s_rdy <= '1';
		when Others =>
			
		end case;
	END PROCESS UEBERGANG;

	ZUSTANDSSPEICHER	:	PROCESS(CLK, nRST, s_state_next)
		VARIABLE zustand		:	State		:=	Startup;
	BEGIN
		if ( nRST = '1' ) then
			if ( rising_edge(CLK) ) then
				zustand := s_state_next;
			else
				zustand := zustand;
			end if;
			s_state_current <= zustand;
		else
			zustand := Reset;
		end if;
	END PROCESS ZUSTANDSSPEICHER;

	AUSGANG	:	PROCESS(BCD, Gray, Aiken, s_state_current)
	
	BEGIN
		case s_state_current is
			when Ready =>
			
			when Others =>
				
		end case;
	END PROCESS AUSGANG;
	
	LCD_DATA <= s_data;
	LCD_RS <= s_rs;
	LCD_RW <= s_rw;
	LCD_EN <= s_en;
	
	F_RDY <= s_rdy;

END Behaviour;