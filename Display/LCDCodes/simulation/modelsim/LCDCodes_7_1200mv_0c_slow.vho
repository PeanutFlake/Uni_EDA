-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "06/12/2018 19:56:06"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	LCDCodes IS
    PORT (
	nRST : IN std_logic;
	CLK : IN std_logic;
	BCD : IN std_logic_vector(3 DOWNTO 0);
	Gray : IN std_logic_vector(3 DOWNTO 0);
	Aiken : IN std_logic_vector(3 DOWNTO 0);
	F_RDY : OUT std_logic;
	LCD_EN : OUT std_logic;
	LCD_RS : OUT std_logic;
	LCD_RW : OUT std_logic;
	LCD_DATA : OUT std_logic_vector(7 DOWNTO 0)
	);
END LCDCodes;

-- Design Ports Information
-- BCD[0]	=>  Location: PIN_AD5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BCD[1]	=>  Location: PIN_AC5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BCD[2]	=>  Location: PIN_J6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BCD[3]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Gray[0]	=>  Location: PIN_R22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Gray[1]	=>  Location: PIN_AH12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Gray[2]	=>  Location: PIN_AF8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Gray[3]	=>  Location: PIN_D4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Aiken[0]	=>  Location: PIN_H10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Aiken[1]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Aiken[2]	=>  Location: PIN_AH11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Aiken[3]	=>  Location: PIN_P27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- F_RDY	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_EN	=>  Location: PIN_L4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_RS	=>  Location: PIN_M2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_RW	=>  Location: PIN_M1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_DATA[0]	=>  Location: PIN_L3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_DATA[1]	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_DATA[2]	=>  Location: PIN_L2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_DATA[3]	=>  Location: PIN_K7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_DATA[4]	=>  Location: PIN_K1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_DATA[5]	=>  Location: PIN_K2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_DATA[6]	=>  Location: PIN_M3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_DATA[7]	=>  Location: PIN_M5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_M21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- nRST	=>  Location: PIN_M23,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF LCDCodes IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_nRST : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_BCD : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_Gray : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_Aiken : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_F_RDY : std_logic;
SIGNAL ww_LCD_EN : std_logic;
SIGNAL ww_LCD_RS : std_logic;
SIGNAL ww_LCD_RW : std_logic;
SIGNAL ww_LCD_DATA : std_logic_vector(7 DOWNTO 0);
SIGNAL \WideOr6~0clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \s_state_next~1clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \BCD[0]~input_o\ : std_logic;
SIGNAL \BCD[1]~input_o\ : std_logic;
SIGNAL \BCD[2]~input_o\ : std_logic;
SIGNAL \BCD[3]~input_o\ : std_logic;
SIGNAL \Gray[0]~input_o\ : std_logic;
SIGNAL \Gray[1]~input_o\ : std_logic;
SIGNAL \Gray[2]~input_o\ : std_logic;
SIGNAL \Gray[3]~input_o\ : std_logic;
SIGNAL \Aiken[0]~input_o\ : std_logic;
SIGNAL \Aiken[1]~input_o\ : std_logic;
SIGNAL \Aiken[2]~input_o\ : std_logic;
SIGNAL \Aiken[3]~input_o\ : std_logic;
SIGNAL \F_RDY~output_o\ : std_logic;
SIGNAL \LCD_EN~output_o\ : std_logic;
SIGNAL \LCD_RS~output_o\ : std_logic;
SIGNAL \LCD_RW~output_o\ : std_logic;
SIGNAL \LCD_DATA[0]~output_o\ : std_logic;
SIGNAL \LCD_DATA[1]~output_o\ : std_logic;
SIGNAL \LCD_DATA[2]~output_o\ : std_logic;
SIGNAL \LCD_DATA[3]~output_o\ : std_logic;
SIGNAL \LCD_DATA[4]~output_o\ : std_logic;
SIGNAL \LCD_DATA[5]~output_o\ : std_logic;
SIGNAL \LCD_DATA[6]~output_o\ : std_logic;
SIGNAL \LCD_DATA[7]~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \s_state_current.Reset~feeder_combout\ : std_logic;
SIGNAL \nRST~input_o\ : std_logic;
SIGNAL \WideNor0~1_combout\ : std_logic;
SIGNAL \WideNor0~0_combout\ : std_logic;
SIGNAL \ZUSTAND:zustand.Reset~feeder_combout\ : std_logic;
SIGNAL \ZUSTAND:zustand.Reset~q\ : std_logic;
SIGNAL \WideNor0~2_combout\ : std_logic;
SIGNAL \s_state_current.Init~0_combout\ : std_logic;
SIGNAL \s_state_current.Reset~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[0]~1_combout\ : std_logic;
SIGNAL \~GND~combout\ : std_logic;
SIGNAL \WideNor0~3_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[11]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[12]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[12]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[12]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[13]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[13]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[13]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[14]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[14]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[14]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[15]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[15]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[15]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[16]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[16]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[16]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[17]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[17]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[17]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[18]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[18]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[18]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[19]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[19]~q\ : std_logic;
SIGNAL \Equal0~5_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[19]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[20]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[20]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[20]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[21]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[21]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[21]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[22]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[22]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[22]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[23]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[23]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[23]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[24]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[24]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[24]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[25]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[25]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[25]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[26]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[26]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[26]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[27]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[27]~q\ : std_logic;
SIGNAL \Equal0~7_combout\ : std_logic;
SIGNAL \Equal0~6_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[27]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[28]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[28]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[28]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[29]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[29]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[29]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[30]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[30]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[30]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[31]~3_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[31]~q\ : std_logic;
SIGNAL \Equal0~8_combout\ : std_logic;
SIGNAL \Equal0~9_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[31]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[31]~2_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[0]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[0]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[1]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[1]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[1]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[2]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[2]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[2]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[3]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[3]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[3]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[4]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[4]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[4]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[5]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[5]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[5]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[6]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[6]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[6]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[7]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[7]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[7]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[8]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[8]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[8]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[9]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[9]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[9]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[10]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[10]~q\ : std_logic;
SIGNAL \ZUSTAND:holdTime[10]~2\ : std_logic;
SIGNAL \ZUSTAND:holdTime[11]~1_combout\ : std_logic;
SIGNAL \ZUSTAND:holdTime[11]~q\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \Equal0~3_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Equal0~4_combout\ : std_logic;
SIGNAL \Equal0~10_combout\ : std_logic;
SIGNAL \s_state_current.Pause~q\ : std_logic;
SIGNAL \s_state_next~1_combout\ : std_logic;
SIGNAL \s_state_next~1clkctrl_outclk\ : std_logic;
SIGNAL \s_state_next.Init_1004~combout\ : std_logic;
SIGNAL \ZUSTAND:zustand.Init~feeder_combout\ : std_logic;
SIGNAL \ZUSTAND:zustand.Init~q\ : std_logic;
SIGNAL \s_state_current.Init~1_combout\ : std_logic;
SIGNAL \s_state_current.Init~q\ : std_logic;
SIGNAL \s_state_next.Func_996~combout\ : std_logic;
SIGNAL \ZUSTAND:zustand.Func~feeder_combout\ : std_logic;
SIGNAL \ZUSTAND:zustand.Func~q\ : std_logic;
SIGNAL \s_state_current.Func~0_combout\ : std_logic;
SIGNAL \s_state_current.Func~q\ : std_logic;
SIGNAL \s_state_next.Disp_988~combout\ : std_logic;
SIGNAL \ZUSTAND:zustand.Disp~q\ : std_logic;
SIGNAL \s_state_current.Disp~0_combout\ : std_logic;
SIGNAL \s_state_current.Disp~q\ : std_logic;
SIGNAL \s_state_next.Entry_980~combout\ : std_logic;
SIGNAL \ZUSTAND:zustand.Entry~feeder_combout\ : std_logic;
SIGNAL \ZUSTAND:zustand.Entry~q\ : std_logic;
SIGNAL \s_state_current.Entry~0_combout\ : std_logic;
SIGNAL \s_state_current.Entry~q\ : std_logic;
SIGNAL \s_state_next.Ready_972~combout\ : std_logic;
SIGNAL \ZUSTAND:zustand.Ready~feeder_combout\ : std_logic;
SIGNAL \ZUSTAND:zustand.Ready~q\ : std_logic;
SIGNAL \s_state_current.Ready~0_combout\ : std_logic;
SIGNAL \s_state_current.Ready~q\ : std_logic;
SIGNAL \WideOr7~0_combout\ : std_logic;
SIGNAL \s_rdy~combout\ : std_logic;
SIGNAL \WideOr7~combout\ : std_logic;
SIGNAL \s_en~combout\ : std_logic;
SIGNAL \WideOr6~0_combout\ : std_logic;
SIGNAL \WideOr6~0clkctrl_outclk\ : std_logic;
SIGNAL \s_data~0_combout\ : std_logic;
SIGNAL s_data : std_logic_vector(7 DOWNTO 0);

BEGIN

ww_nRST <= nRST;
ww_CLK <= CLK;
ww_BCD <= BCD;
ww_Gray <= Gray;
ww_Aiken <= Aiken;
F_RDY <= ww_F_RDY;
LCD_EN <= ww_LCD_EN;
LCD_RS <= ww_LCD_RS;
LCD_RW <= ww_LCD_RW;
LCD_DATA <= ww_LCD_DATA;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\WideOr6~0clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \WideOr6~0_combout\);

\s_state_next~1clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \s_state_next~1_combout\);

-- Location: IOOBUF_X69_Y73_N16
\F_RDY~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_rdy~combout\,
	devoe => ww_devoe,
	o => \F_RDY~output_o\);

-- Location: IOOBUF_X0_Y52_N2
\LCD_EN~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_en~combout\,
	devoe => ww_devoe,
	o => \LCD_EN~output_o\);

-- Location: IOOBUF_X0_Y44_N16
\LCD_RS~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_rdy~combout\,
	devoe => ww_devoe,
	o => \LCD_RS~output_o\);

-- Location: IOOBUF_X0_Y44_N23
\LCD_RW~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_RW~output_o\);

-- Location: IOOBUF_X0_Y52_N16
\LCD_DATA[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_DATA[0]~output_o\);

-- Location: IOOBUF_X0_Y44_N9
\LCD_DATA[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_data(1),
	devoe => ww_devoe,
	o => \LCD_DATA[1]~output_o\);

-- Location: IOOBUF_X0_Y44_N2
\LCD_DATA[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \LCD_DATA[2]~output_o\);

-- Location: IOOBUF_X0_Y49_N9
\LCD_DATA[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_data(3),
	devoe => ww_devoe,
	o => \LCD_DATA[3]~output_o\);

-- Location: IOOBUF_X0_Y54_N9
\LCD_DATA[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_data(4),
	devoe => ww_devoe,
	o => \LCD_DATA[4]~output_o\);

-- Location: IOOBUF_X0_Y55_N23
\LCD_DATA[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_data(4),
	devoe => ww_devoe,
	o => \LCD_DATA[5]~output_o\);

-- Location: IOOBUF_X0_Y51_N16
\LCD_DATA[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_DATA[6]~output_o\);

-- Location: IOOBUF_X0_Y47_N2
\LCD_DATA[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_DATA[7]~output_o\);

-- Location: IOIBUF_X115_Y53_N15
\CLK~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: LCCOMB_X91_Y52_N20
\s_state_current.Reset~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_current.Reset~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \s_state_current.Reset~feeder_combout\);

-- Location: IOIBUF_X115_Y40_N8
\nRST~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_nRST,
	o => \nRST~input_o\);

-- Location: LCCOMB_X87_Y52_N2
\WideNor0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \WideNor0~1_combout\ = (\ZUSTAND:zustand.Disp~q\ & ((\ZUSTAND:zustand.Func~q\ $ (\s_state_next.Func_996~combout\)) # (!\s_state_next.Disp_988~combout\))) # (!\ZUSTAND:zustand.Disp~q\ & ((\s_state_next.Disp_988~combout\) # (\ZUSTAND:zustand.Func~q\ $ 
-- (\s_state_next.Func_996~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111110110111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:zustand.Disp~q\,
	datab => \ZUSTAND:zustand.Func~q\,
	datac => \s_state_next.Func_996~combout\,
	datad => \s_state_next.Disp_988~combout\,
	combout => \WideNor0~1_combout\);

-- Location: LCCOMB_X87_Y52_N4
\WideNor0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \WideNor0~0_combout\ = (\ZUSTAND:zustand.Entry~q\ & ((\ZUSTAND:zustand.Ready~q\ $ (\s_state_next.Ready_972~combout\)) # (!\s_state_next.Entry_980~combout\))) # (!\ZUSTAND:zustand.Entry~q\ & ((\s_state_next.Entry_980~combout\) # (\ZUSTAND:zustand.Ready~q\ 
-- $ (\s_state_next.Ready_972~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111110110111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:zustand.Entry~q\,
	datab => \ZUSTAND:zustand.Ready~q\,
	datac => \s_state_next.Ready_972~combout\,
	datad => \s_state_next.Entry_980~combout\,
	combout => \WideNor0~0_combout\);

-- Location: LCCOMB_X87_Y52_N10
\ZUSTAND:zustand.Reset~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:zustand.Reset~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \ZUSTAND:zustand.Reset~feeder_combout\);

-- Location: FF_X87_Y52_N11
\ZUSTAND:zustand.Reset\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:zustand.Reset~feeder_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:zustand.Reset~q\);

-- Location: LCCOMB_X87_Y52_N12
\WideNor0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \WideNor0~2_combout\ = (\ZUSTAND:zustand.Init~q\ $ (\s_state_next.Init_1004~combout\)) # (!\ZUSTAND:zustand.Reset~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111111001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:zustand.Init~q\,
	datac => \ZUSTAND:zustand.Reset~q\,
	datad => \s_state_next.Init_1004~combout\,
	combout => \WideNor0~2_combout\);

-- Location: LCCOMB_X87_Y52_N18
\s_state_current.Init~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_current.Init~0_combout\ = (\nRST~input_o\ & (!\WideNor0~1_combout\ & (!\WideNor0~0_combout\ & !\WideNor0~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \WideNor0~1_combout\,
	datac => \WideNor0~0_combout\,
	datad => \WideNor0~2_combout\,
	combout => \s_state_current.Init~0_combout\);

-- Location: FF_X91_Y52_N21
\s_state_current.Reset\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \s_state_current.Reset~feeder_combout\,
	ena => \s_state_current.Init~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Reset~q\);

-- Location: LCCOMB_X89_Y53_N0
\ZUSTAND:holdTime[0]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[0]~1_combout\ = \ZUSTAND:holdTime[0]~q\ $ (VCC)
-- \ZUSTAND:holdTime[0]~2\ = CARRY(\ZUSTAND:holdTime[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[0]~q\,
	datad => VCC,
	combout => \ZUSTAND:holdTime[0]~1_combout\,
	cout => \ZUSTAND:holdTime[0]~2\);

-- Location: LCCOMB_X90_Y52_N26
\~GND\ : cycloneive_lcell_comb
-- Equation(s):
-- \~GND~combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~GND~combout\);

-- Location: LCCOMB_X87_Y52_N0
\WideNor0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \WideNor0~3_combout\ = (\WideNor0~2_combout\) # ((\WideNor0~0_combout\) # (\WideNor0~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \WideNor0~2_combout\,
	datac => \WideNor0~0_combout\,
	datad => \WideNor0~1_combout\,
	combout => \WideNor0~3_combout\);

-- Location: LCCOMB_X89_Y53_N22
\ZUSTAND:holdTime[11]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[11]~1_combout\ = (\ZUSTAND:holdTime[11]~q\ & (\ZUSTAND:holdTime[10]~2\ & VCC)) # (!\ZUSTAND:holdTime[11]~q\ & (!\ZUSTAND:holdTime[10]~2\))
-- \ZUSTAND:holdTime[11]~2\ = CARRY((!\ZUSTAND:holdTime[11]~q\ & !\ZUSTAND:holdTime[10]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[11]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[10]~2\,
	combout => \ZUSTAND:holdTime[11]~1_combout\,
	cout => \ZUSTAND:holdTime[11]~2\);

-- Location: LCCOMB_X89_Y53_N24
\ZUSTAND:holdTime[12]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[12]~1_combout\ = (\ZUSTAND:holdTime[12]~q\ & ((GND) # (!\ZUSTAND:holdTime[11]~2\))) # (!\ZUSTAND:holdTime[12]~q\ & (\ZUSTAND:holdTime[11]~2\ $ (GND)))
-- \ZUSTAND:holdTime[12]~2\ = CARRY((\ZUSTAND:holdTime[12]~q\) # (!\ZUSTAND:holdTime[11]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[12]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[11]~2\,
	combout => \ZUSTAND:holdTime[12]~1_combout\,
	cout => \ZUSTAND:holdTime[12]~2\);

-- Location: FF_X89_Y53_N25
\ZUSTAND:holdTime[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[12]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[12]~q\);

-- Location: LCCOMB_X89_Y53_N26
\ZUSTAND:holdTime[13]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[13]~1_combout\ = (\ZUSTAND:holdTime[13]~q\ & (\ZUSTAND:holdTime[12]~2\ & VCC)) # (!\ZUSTAND:holdTime[13]~q\ & (!\ZUSTAND:holdTime[12]~2\))
-- \ZUSTAND:holdTime[13]~2\ = CARRY((!\ZUSTAND:holdTime[13]~q\ & !\ZUSTAND:holdTime[12]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[13]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[12]~2\,
	combout => \ZUSTAND:holdTime[13]~1_combout\,
	cout => \ZUSTAND:holdTime[13]~2\);

-- Location: FF_X89_Y53_N27
\ZUSTAND:holdTime[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[13]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[13]~q\);

-- Location: LCCOMB_X89_Y53_N28
\ZUSTAND:holdTime[14]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[14]~1_combout\ = (\ZUSTAND:holdTime[14]~q\ & ((GND) # (!\ZUSTAND:holdTime[13]~2\))) # (!\ZUSTAND:holdTime[14]~q\ & (\ZUSTAND:holdTime[13]~2\ $ (GND)))
-- \ZUSTAND:holdTime[14]~2\ = CARRY((\ZUSTAND:holdTime[14]~q\) # (!\ZUSTAND:holdTime[13]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[14]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[13]~2\,
	combout => \ZUSTAND:holdTime[14]~1_combout\,
	cout => \ZUSTAND:holdTime[14]~2\);

-- Location: FF_X89_Y53_N29
\ZUSTAND:holdTime[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[14]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[14]~q\);

-- Location: LCCOMB_X89_Y53_N30
\ZUSTAND:holdTime[15]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[15]~1_combout\ = (\ZUSTAND:holdTime[15]~q\ & (\ZUSTAND:holdTime[14]~2\ & VCC)) # (!\ZUSTAND:holdTime[15]~q\ & (!\ZUSTAND:holdTime[14]~2\))
-- \ZUSTAND:holdTime[15]~2\ = CARRY((!\ZUSTAND:holdTime[15]~q\ & !\ZUSTAND:holdTime[14]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[15]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[14]~2\,
	combout => \ZUSTAND:holdTime[15]~1_combout\,
	cout => \ZUSTAND:holdTime[15]~2\);

-- Location: FF_X89_Y53_N31
\ZUSTAND:holdTime[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[15]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[15]~q\);

-- Location: LCCOMB_X89_Y52_N0
\ZUSTAND:holdTime[16]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[16]~1_combout\ = (\ZUSTAND:holdTime[16]~q\ & ((GND) # (!\ZUSTAND:holdTime[15]~2\))) # (!\ZUSTAND:holdTime[16]~q\ & (\ZUSTAND:holdTime[15]~2\ $ (GND)))
-- \ZUSTAND:holdTime[16]~2\ = CARRY((\ZUSTAND:holdTime[16]~q\) # (!\ZUSTAND:holdTime[15]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[16]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[15]~2\,
	combout => \ZUSTAND:holdTime[16]~1_combout\,
	cout => \ZUSTAND:holdTime[16]~2\);

-- Location: FF_X89_Y52_N1
\ZUSTAND:holdTime[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[16]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[16]~q\);

-- Location: LCCOMB_X89_Y52_N2
\ZUSTAND:holdTime[17]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[17]~1_combout\ = (\ZUSTAND:holdTime[17]~q\ & (\ZUSTAND:holdTime[16]~2\ & VCC)) # (!\ZUSTAND:holdTime[17]~q\ & (!\ZUSTAND:holdTime[16]~2\))
-- \ZUSTAND:holdTime[17]~2\ = CARRY((!\ZUSTAND:holdTime[17]~q\ & !\ZUSTAND:holdTime[16]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[17]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[16]~2\,
	combout => \ZUSTAND:holdTime[17]~1_combout\,
	cout => \ZUSTAND:holdTime[17]~2\);

-- Location: FF_X89_Y52_N3
\ZUSTAND:holdTime[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[17]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[17]~q\);

-- Location: LCCOMB_X89_Y52_N4
\ZUSTAND:holdTime[18]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[18]~1_combout\ = (\ZUSTAND:holdTime[18]~q\ & ((GND) # (!\ZUSTAND:holdTime[17]~2\))) # (!\ZUSTAND:holdTime[18]~q\ & (\ZUSTAND:holdTime[17]~2\ $ (GND)))
-- \ZUSTAND:holdTime[18]~2\ = CARRY((\ZUSTAND:holdTime[18]~q\) # (!\ZUSTAND:holdTime[17]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[18]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[17]~2\,
	combout => \ZUSTAND:holdTime[18]~1_combout\,
	cout => \ZUSTAND:holdTime[18]~2\);

-- Location: FF_X89_Y52_N5
\ZUSTAND:holdTime[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[18]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[18]~q\);

-- Location: LCCOMB_X89_Y52_N6
\ZUSTAND:holdTime[19]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[19]~1_combout\ = (\ZUSTAND:holdTime[19]~q\ & (\ZUSTAND:holdTime[18]~2\ & VCC)) # (!\ZUSTAND:holdTime[19]~q\ & (!\ZUSTAND:holdTime[18]~2\))
-- \ZUSTAND:holdTime[19]~2\ = CARRY((!\ZUSTAND:holdTime[19]~q\ & !\ZUSTAND:holdTime[18]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[19]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[18]~2\,
	combout => \ZUSTAND:holdTime[19]~1_combout\,
	cout => \ZUSTAND:holdTime[19]~2\);

-- Location: FF_X89_Y52_N7
\ZUSTAND:holdTime[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[19]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[19]~q\);

-- Location: LCCOMB_X90_Y52_N12
\Equal0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~5_combout\ = (!\ZUSTAND:holdTime[16]~q\ & (!\ZUSTAND:holdTime[19]~q\ & (!\ZUSTAND:holdTime[18]~q\ & !\ZUSTAND:holdTime[17]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[16]~q\,
	datab => \ZUSTAND:holdTime[19]~q\,
	datac => \ZUSTAND:holdTime[18]~q\,
	datad => \ZUSTAND:holdTime[17]~q\,
	combout => \Equal0~5_combout\);

-- Location: LCCOMB_X89_Y52_N8
\ZUSTAND:holdTime[20]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[20]~1_combout\ = (\ZUSTAND:holdTime[20]~q\ & ((GND) # (!\ZUSTAND:holdTime[19]~2\))) # (!\ZUSTAND:holdTime[20]~q\ & (\ZUSTAND:holdTime[19]~2\ $ (GND)))
-- \ZUSTAND:holdTime[20]~2\ = CARRY((\ZUSTAND:holdTime[20]~q\) # (!\ZUSTAND:holdTime[19]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[20]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[19]~2\,
	combout => \ZUSTAND:holdTime[20]~1_combout\,
	cout => \ZUSTAND:holdTime[20]~2\);

-- Location: FF_X89_Y52_N9
\ZUSTAND:holdTime[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[20]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[20]~q\);

-- Location: LCCOMB_X89_Y52_N10
\ZUSTAND:holdTime[21]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[21]~1_combout\ = (\ZUSTAND:holdTime[21]~q\ & (\ZUSTAND:holdTime[20]~2\ & VCC)) # (!\ZUSTAND:holdTime[21]~q\ & (!\ZUSTAND:holdTime[20]~2\))
-- \ZUSTAND:holdTime[21]~2\ = CARRY((!\ZUSTAND:holdTime[21]~q\ & !\ZUSTAND:holdTime[20]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[21]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[20]~2\,
	combout => \ZUSTAND:holdTime[21]~1_combout\,
	cout => \ZUSTAND:holdTime[21]~2\);

-- Location: FF_X89_Y52_N11
\ZUSTAND:holdTime[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[21]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[21]~q\);

-- Location: LCCOMB_X89_Y52_N12
\ZUSTAND:holdTime[22]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[22]~1_combout\ = (\ZUSTAND:holdTime[22]~q\ & ((GND) # (!\ZUSTAND:holdTime[21]~2\))) # (!\ZUSTAND:holdTime[22]~q\ & (\ZUSTAND:holdTime[21]~2\ $ (GND)))
-- \ZUSTAND:holdTime[22]~2\ = CARRY((\ZUSTAND:holdTime[22]~q\) # (!\ZUSTAND:holdTime[21]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[22]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[21]~2\,
	combout => \ZUSTAND:holdTime[22]~1_combout\,
	cout => \ZUSTAND:holdTime[22]~2\);

-- Location: FF_X89_Y52_N13
\ZUSTAND:holdTime[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[22]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[22]~q\);

-- Location: LCCOMB_X89_Y52_N14
\ZUSTAND:holdTime[23]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[23]~1_combout\ = (\ZUSTAND:holdTime[23]~q\ & (\ZUSTAND:holdTime[22]~2\ & VCC)) # (!\ZUSTAND:holdTime[23]~q\ & (!\ZUSTAND:holdTime[22]~2\))
-- \ZUSTAND:holdTime[23]~2\ = CARRY((!\ZUSTAND:holdTime[23]~q\ & !\ZUSTAND:holdTime[22]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[23]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[22]~2\,
	combout => \ZUSTAND:holdTime[23]~1_combout\,
	cout => \ZUSTAND:holdTime[23]~2\);

-- Location: FF_X89_Y52_N15
\ZUSTAND:holdTime[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[23]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[23]~q\);

-- Location: LCCOMB_X89_Y52_N16
\ZUSTAND:holdTime[24]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[24]~1_combout\ = (\ZUSTAND:holdTime[24]~q\ & ((GND) # (!\ZUSTAND:holdTime[23]~2\))) # (!\ZUSTAND:holdTime[24]~q\ & (\ZUSTAND:holdTime[23]~2\ $ (GND)))
-- \ZUSTAND:holdTime[24]~2\ = CARRY((\ZUSTAND:holdTime[24]~q\) # (!\ZUSTAND:holdTime[23]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[24]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[23]~2\,
	combout => \ZUSTAND:holdTime[24]~1_combout\,
	cout => \ZUSTAND:holdTime[24]~2\);

-- Location: FF_X89_Y52_N17
\ZUSTAND:holdTime[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[24]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[24]~q\);

-- Location: LCCOMB_X89_Y52_N18
\ZUSTAND:holdTime[25]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[25]~1_combout\ = (\ZUSTAND:holdTime[25]~q\ & (\ZUSTAND:holdTime[24]~2\ & VCC)) # (!\ZUSTAND:holdTime[25]~q\ & (!\ZUSTAND:holdTime[24]~2\))
-- \ZUSTAND:holdTime[25]~2\ = CARRY((!\ZUSTAND:holdTime[25]~q\ & !\ZUSTAND:holdTime[24]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[25]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[24]~2\,
	combout => \ZUSTAND:holdTime[25]~1_combout\,
	cout => \ZUSTAND:holdTime[25]~2\);

-- Location: FF_X89_Y52_N19
\ZUSTAND:holdTime[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[25]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[25]~q\);

-- Location: LCCOMB_X89_Y52_N20
\ZUSTAND:holdTime[26]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[26]~1_combout\ = (\ZUSTAND:holdTime[26]~q\ & ((GND) # (!\ZUSTAND:holdTime[25]~2\))) # (!\ZUSTAND:holdTime[26]~q\ & (\ZUSTAND:holdTime[25]~2\ $ (GND)))
-- \ZUSTAND:holdTime[26]~2\ = CARRY((\ZUSTAND:holdTime[26]~q\) # (!\ZUSTAND:holdTime[25]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[26]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[25]~2\,
	combout => \ZUSTAND:holdTime[26]~1_combout\,
	cout => \ZUSTAND:holdTime[26]~2\);

-- Location: FF_X89_Y52_N21
\ZUSTAND:holdTime[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[26]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[26]~q\);

-- Location: LCCOMB_X89_Y52_N22
\ZUSTAND:holdTime[27]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[27]~1_combout\ = (\ZUSTAND:holdTime[27]~q\ & (\ZUSTAND:holdTime[26]~2\ & VCC)) # (!\ZUSTAND:holdTime[27]~q\ & (!\ZUSTAND:holdTime[26]~2\))
-- \ZUSTAND:holdTime[27]~2\ = CARRY((!\ZUSTAND:holdTime[27]~q\ & !\ZUSTAND:holdTime[26]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[27]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[26]~2\,
	combout => \ZUSTAND:holdTime[27]~1_combout\,
	cout => \ZUSTAND:holdTime[27]~2\);

-- Location: FF_X89_Y52_N23
\ZUSTAND:holdTime[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[27]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[27]~q\);

-- Location: LCCOMB_X90_Y52_N4
\Equal0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~7_combout\ = (!\ZUSTAND:holdTime[24]~q\ & (!\ZUSTAND:holdTime[27]~q\ & (!\ZUSTAND:holdTime[26]~q\ & !\ZUSTAND:holdTime[25]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[24]~q\,
	datab => \ZUSTAND:holdTime[27]~q\,
	datac => \ZUSTAND:holdTime[26]~q\,
	datad => \ZUSTAND:holdTime[25]~q\,
	combout => \Equal0~7_combout\);

-- Location: LCCOMB_X90_Y52_N30
\Equal0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~6_combout\ = (!\ZUSTAND:holdTime[23]~q\ & (!\ZUSTAND:holdTime[20]~q\ & (!\ZUSTAND:holdTime[21]~q\ & !\ZUSTAND:holdTime[22]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[23]~q\,
	datab => \ZUSTAND:holdTime[20]~q\,
	datac => \ZUSTAND:holdTime[21]~q\,
	datad => \ZUSTAND:holdTime[22]~q\,
	combout => \Equal0~6_combout\);

-- Location: LCCOMB_X89_Y52_N24
\ZUSTAND:holdTime[28]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[28]~1_combout\ = (\ZUSTAND:holdTime[28]~q\ & ((GND) # (!\ZUSTAND:holdTime[27]~2\))) # (!\ZUSTAND:holdTime[28]~q\ & (\ZUSTAND:holdTime[27]~2\ $ (GND)))
-- \ZUSTAND:holdTime[28]~2\ = CARRY((\ZUSTAND:holdTime[28]~q\) # (!\ZUSTAND:holdTime[27]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[28]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[27]~2\,
	combout => \ZUSTAND:holdTime[28]~1_combout\,
	cout => \ZUSTAND:holdTime[28]~2\);

-- Location: FF_X89_Y52_N25
\ZUSTAND:holdTime[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[28]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[28]~q\);

-- Location: LCCOMB_X89_Y52_N26
\ZUSTAND:holdTime[29]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[29]~1_combout\ = (\ZUSTAND:holdTime[29]~q\ & (\ZUSTAND:holdTime[28]~2\ & VCC)) # (!\ZUSTAND:holdTime[29]~q\ & (!\ZUSTAND:holdTime[28]~2\))
-- \ZUSTAND:holdTime[29]~2\ = CARRY((!\ZUSTAND:holdTime[29]~q\ & !\ZUSTAND:holdTime[28]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[29]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[28]~2\,
	combout => \ZUSTAND:holdTime[29]~1_combout\,
	cout => \ZUSTAND:holdTime[29]~2\);

-- Location: FF_X89_Y52_N27
\ZUSTAND:holdTime[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[29]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[29]~q\);

-- Location: LCCOMB_X89_Y52_N28
\ZUSTAND:holdTime[30]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[30]~1_combout\ = (\ZUSTAND:holdTime[30]~q\ & ((GND) # (!\ZUSTAND:holdTime[29]~2\))) # (!\ZUSTAND:holdTime[30]~q\ & (\ZUSTAND:holdTime[29]~2\ $ (GND)))
-- \ZUSTAND:holdTime[30]~2\ = CARRY((\ZUSTAND:holdTime[30]~q\) # (!\ZUSTAND:holdTime[29]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[30]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[29]~2\,
	combout => \ZUSTAND:holdTime[30]~1_combout\,
	cout => \ZUSTAND:holdTime[30]~2\);

-- Location: FF_X89_Y52_N29
\ZUSTAND:holdTime[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[30]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[30]~q\);

-- Location: LCCOMB_X89_Y52_N30
\ZUSTAND:holdTime[31]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[31]~3_combout\ = \ZUSTAND:holdTime[31]~q\ $ (!\ZUSTAND:holdTime[30]~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[31]~q\,
	cin => \ZUSTAND:holdTime[30]~2\,
	combout => \ZUSTAND:holdTime[31]~3_combout\);

-- Location: FF_X89_Y52_N31
\ZUSTAND:holdTime[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[31]~3_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[31]~q\);

-- Location: LCCOMB_X90_Y52_N6
\Equal0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~8_combout\ = (!\ZUSTAND:holdTime[29]~q\ & (!\ZUSTAND:holdTime[30]~q\ & (!\ZUSTAND:holdTime[31]~q\ & !\ZUSTAND:holdTime[28]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[29]~q\,
	datab => \ZUSTAND:holdTime[30]~q\,
	datac => \ZUSTAND:holdTime[31]~q\,
	datad => \ZUSTAND:holdTime[28]~q\,
	combout => \Equal0~8_combout\);

-- Location: LCCOMB_X90_Y52_N0
\Equal0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~9_combout\ = (\Equal0~5_combout\ & (\Equal0~7_combout\ & (\Equal0~6_combout\ & \Equal0~8_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~5_combout\,
	datab => \Equal0~7_combout\,
	datac => \Equal0~6_combout\,
	datad => \Equal0~8_combout\,
	combout => \Equal0~9_combout\);

-- Location: LCCOMB_X88_Y52_N12
\ZUSTAND:holdTime[31]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[31]~1_combout\ = ((\WideNor0~1_combout\) # (!\Equal0~9_combout\)) # (!\Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~4_combout\,
	datac => \Equal0~9_combout\,
	datad => \WideNor0~1_combout\,
	combout => \ZUSTAND:holdTime[31]~1_combout\);

-- Location: LCCOMB_X88_Y52_N0
\ZUSTAND:holdTime[31]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[31]~2_combout\ = (\nRST~input_o\ & ((\WideNor0~2_combout\) # ((\WideNor0~0_combout\) # (\ZUSTAND:holdTime[31]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \WideNor0~2_combout\,
	datab => \nRST~input_o\,
	datac => \WideNor0~0_combout\,
	datad => \ZUSTAND:holdTime[31]~1_combout\,
	combout => \ZUSTAND:holdTime[31]~2_combout\);

-- Location: FF_X89_Y53_N1
\ZUSTAND:holdTime[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[0]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[0]~q\);

-- Location: LCCOMB_X89_Y53_N2
\ZUSTAND:holdTime[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[1]~1_combout\ = (\ZUSTAND:holdTime[1]~q\ & (\ZUSTAND:holdTime[0]~2\ & VCC)) # (!\ZUSTAND:holdTime[1]~q\ & (!\ZUSTAND:holdTime[0]~2\))
-- \ZUSTAND:holdTime[1]~2\ = CARRY((!\ZUSTAND:holdTime[1]~q\ & !\ZUSTAND:holdTime[0]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[1]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[0]~2\,
	combout => \ZUSTAND:holdTime[1]~1_combout\,
	cout => \ZUSTAND:holdTime[1]~2\);

-- Location: FF_X89_Y53_N3
\ZUSTAND:holdTime[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[1]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[1]~q\);

-- Location: LCCOMB_X89_Y53_N4
\ZUSTAND:holdTime[2]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[2]~1_combout\ = (\ZUSTAND:holdTime[2]~q\ & ((GND) # (!\ZUSTAND:holdTime[1]~2\))) # (!\ZUSTAND:holdTime[2]~q\ & (\ZUSTAND:holdTime[1]~2\ $ (GND)))
-- \ZUSTAND:holdTime[2]~2\ = CARRY((\ZUSTAND:holdTime[2]~q\) # (!\ZUSTAND:holdTime[1]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[2]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[1]~2\,
	combout => \ZUSTAND:holdTime[2]~1_combout\,
	cout => \ZUSTAND:holdTime[2]~2\);

-- Location: FF_X89_Y53_N5
\ZUSTAND:holdTime[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[2]~1_combout\,
	asdata => VCC,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[2]~q\);

-- Location: LCCOMB_X89_Y53_N6
\ZUSTAND:holdTime[3]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[3]~1_combout\ = (\ZUSTAND:holdTime[3]~q\ & (\ZUSTAND:holdTime[2]~2\ & VCC)) # (!\ZUSTAND:holdTime[3]~q\ & (!\ZUSTAND:holdTime[2]~2\))
-- \ZUSTAND:holdTime[3]~2\ = CARRY((!\ZUSTAND:holdTime[3]~q\ & !\ZUSTAND:holdTime[2]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[3]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[2]~2\,
	combout => \ZUSTAND:holdTime[3]~1_combout\,
	cout => \ZUSTAND:holdTime[3]~2\);

-- Location: FF_X89_Y53_N7
\ZUSTAND:holdTime[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[3]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[3]~q\);

-- Location: LCCOMB_X89_Y53_N8
\ZUSTAND:holdTime[4]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[4]~1_combout\ = (\ZUSTAND:holdTime[4]~q\ & ((GND) # (!\ZUSTAND:holdTime[3]~2\))) # (!\ZUSTAND:holdTime[4]~q\ & (\ZUSTAND:holdTime[3]~2\ $ (GND)))
-- \ZUSTAND:holdTime[4]~2\ = CARRY((\ZUSTAND:holdTime[4]~q\) # (!\ZUSTAND:holdTime[3]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[4]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[3]~2\,
	combout => \ZUSTAND:holdTime[4]~1_combout\,
	cout => \ZUSTAND:holdTime[4]~2\);

-- Location: FF_X89_Y53_N9
\ZUSTAND:holdTime[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[4]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[4]~q\);

-- Location: LCCOMB_X89_Y53_N10
\ZUSTAND:holdTime[5]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[5]~1_combout\ = (\ZUSTAND:holdTime[5]~q\ & (\ZUSTAND:holdTime[4]~2\ & VCC)) # (!\ZUSTAND:holdTime[5]~q\ & (!\ZUSTAND:holdTime[4]~2\))
-- \ZUSTAND:holdTime[5]~2\ = CARRY((!\ZUSTAND:holdTime[5]~q\ & !\ZUSTAND:holdTime[4]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[5]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[4]~2\,
	combout => \ZUSTAND:holdTime[5]~1_combout\,
	cout => \ZUSTAND:holdTime[5]~2\);

-- Location: FF_X89_Y53_N11
\ZUSTAND:holdTime[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[5]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[5]~q\);

-- Location: LCCOMB_X89_Y53_N12
\ZUSTAND:holdTime[6]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[6]~1_combout\ = (\ZUSTAND:holdTime[6]~q\ & ((GND) # (!\ZUSTAND:holdTime[5]~2\))) # (!\ZUSTAND:holdTime[6]~q\ & (\ZUSTAND:holdTime[5]~2\ $ (GND)))
-- \ZUSTAND:holdTime[6]~2\ = CARRY((\ZUSTAND:holdTime[6]~q\) # (!\ZUSTAND:holdTime[5]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[6]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[5]~2\,
	combout => \ZUSTAND:holdTime[6]~1_combout\,
	cout => \ZUSTAND:holdTime[6]~2\);

-- Location: FF_X89_Y53_N13
\ZUSTAND:holdTime[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[6]~1_combout\,
	asdata => VCC,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[6]~q\);

-- Location: LCCOMB_X89_Y53_N14
\ZUSTAND:holdTime[7]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[7]~1_combout\ = (\ZUSTAND:holdTime[7]~q\ & (\ZUSTAND:holdTime[6]~2\ & VCC)) # (!\ZUSTAND:holdTime[7]~q\ & (!\ZUSTAND:holdTime[6]~2\))
-- \ZUSTAND:holdTime[7]~2\ = CARRY((!\ZUSTAND:holdTime[7]~q\ & !\ZUSTAND:holdTime[6]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[7]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[6]~2\,
	combout => \ZUSTAND:holdTime[7]~1_combout\,
	cout => \ZUSTAND:holdTime[7]~2\);

-- Location: FF_X89_Y53_N15
\ZUSTAND:holdTime[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[7]~1_combout\,
	asdata => VCC,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[7]~q\);

-- Location: LCCOMB_X89_Y53_N16
\ZUSTAND:holdTime[8]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[8]~1_combout\ = (\ZUSTAND:holdTime[8]~q\ & ((GND) # (!\ZUSTAND:holdTime[7]~2\))) # (!\ZUSTAND:holdTime[8]~q\ & (\ZUSTAND:holdTime[7]~2\ $ (GND)))
-- \ZUSTAND:holdTime[8]~2\ = CARRY((\ZUSTAND:holdTime[8]~q\) # (!\ZUSTAND:holdTime[7]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[8]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[7]~2\,
	combout => \ZUSTAND:holdTime[8]~1_combout\,
	cout => \ZUSTAND:holdTime[8]~2\);

-- Location: FF_X89_Y53_N17
\ZUSTAND:holdTime[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[8]~1_combout\,
	asdata => VCC,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[8]~q\);

-- Location: LCCOMB_X89_Y53_N18
\ZUSTAND:holdTime[9]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[9]~1_combout\ = (\ZUSTAND:holdTime[9]~q\ & (\ZUSTAND:holdTime[8]~2\ & VCC)) # (!\ZUSTAND:holdTime[9]~q\ & (!\ZUSTAND:holdTime[8]~2\))
-- \ZUSTAND:holdTime[9]~2\ = CARRY((!\ZUSTAND:holdTime[9]~q\ & !\ZUSTAND:holdTime[8]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[9]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[8]~2\,
	combout => \ZUSTAND:holdTime[9]~1_combout\,
	cout => \ZUSTAND:holdTime[9]~2\);

-- Location: FF_X89_Y53_N19
\ZUSTAND:holdTime[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[9]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[9]~q\);

-- Location: LCCOMB_X89_Y53_N20
\ZUSTAND:holdTime[10]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:holdTime[10]~1_combout\ = (\ZUSTAND:holdTime[10]~q\ & ((GND) # (!\ZUSTAND:holdTime[9]~2\))) # (!\ZUSTAND:holdTime[10]~q\ & (\ZUSTAND:holdTime[9]~2\ $ (GND)))
-- \ZUSTAND:holdTime[10]~2\ = CARRY((\ZUSTAND:holdTime[10]~q\) # (!\ZUSTAND:holdTime[9]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:holdTime[10]~q\,
	datad => VCC,
	cin => \ZUSTAND:holdTime[9]~2\,
	combout => \ZUSTAND:holdTime[10]~1_combout\,
	cout => \ZUSTAND:holdTime[10]~2\);

-- Location: FF_X89_Y53_N21
\ZUSTAND:holdTime[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[10]~1_combout\,
	asdata => \~GND~combout\,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[10]~q\);

-- Location: FF_X89_Y53_N23
\ZUSTAND:holdTime[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:holdTime[11]~1_combout\,
	asdata => VCC,
	sload => \WideNor0~3_combout\,
	ena => \ZUSTAND:holdTime[31]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:holdTime[11]~q\);

-- Location: LCCOMB_X88_Y52_N26
\Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = (!\ZUSTAND:holdTime[11]~q\ & (!\ZUSTAND:holdTime[10]~q\ & (!\ZUSTAND:holdTime[9]~q\ & !\ZUSTAND:holdTime[8]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[11]~q\,
	datab => \ZUSTAND:holdTime[10]~q\,
	datac => \ZUSTAND:holdTime[9]~q\,
	datad => \ZUSTAND:holdTime[8]~q\,
	combout => \Equal0~2_combout\);

-- Location: LCCOMB_X88_Y52_N4
\Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~3_combout\ = (!\ZUSTAND:holdTime[13]~q\ & (!\ZUSTAND:holdTime[12]~q\ & (!\ZUSTAND:holdTime[15]~q\ & !\ZUSTAND:holdTime[14]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[13]~q\,
	datab => \ZUSTAND:holdTime[12]~q\,
	datac => \ZUSTAND:holdTime[15]~q\,
	datad => \ZUSTAND:holdTime[14]~q\,
	combout => \Equal0~3_combout\);

-- Location: LCCOMB_X88_Y52_N8
\Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = (!\ZUSTAND:holdTime[1]~q\ & (!\ZUSTAND:holdTime[3]~q\ & (!\ZUSTAND:holdTime[2]~q\ & !\ZUSTAND:holdTime[0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[1]~q\,
	datab => \ZUSTAND:holdTime[3]~q\,
	datac => \ZUSTAND:holdTime[2]~q\,
	datad => \ZUSTAND:holdTime[0]~q\,
	combout => \Equal0~0_combout\);

-- Location: LCCOMB_X88_Y52_N16
\Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = (!\ZUSTAND:holdTime[6]~q\ & (!\ZUSTAND:holdTime[4]~q\ & (!\ZUSTAND:holdTime[7]~q\ & !\ZUSTAND:holdTime[5]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:holdTime[6]~q\,
	datab => \ZUSTAND:holdTime[4]~q\,
	datac => \ZUSTAND:holdTime[7]~q\,
	datad => \ZUSTAND:holdTime[5]~q\,
	combout => \Equal0~1_combout\);

-- Location: LCCOMB_X88_Y52_N14
\Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~4_combout\ = (\Equal0~2_combout\ & (\Equal0~3_combout\ & (\Equal0~0_combout\ & \Equal0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~2_combout\,
	datab => \Equal0~3_combout\,
	datac => \Equal0~0_combout\,
	datad => \Equal0~1_combout\,
	combout => \Equal0~4_combout\);

-- Location: LCCOMB_X91_Y52_N18
\Equal0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~10_combout\ = (!\Equal0~9_combout\) # (!\Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Equal0~4_combout\,
	datad => \Equal0~9_combout\,
	combout => \Equal0~10_combout\);

-- Location: FF_X91_Y52_N19
\s_state_current.Pause\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \Equal0~10_combout\,
	ena => \s_state_current.Init~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Pause~q\);

-- Location: LCCOMB_X91_Y52_N0
\s_state_next~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_next~1_combout\ = (\s_state_current.Ready~q\) # (\s_state_current.Pause~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \s_state_current.Ready~q\,
	datad => \s_state_current.Pause~q\,
	combout => \s_state_next~1_combout\);

-- Location: CLKCTRL_G7
\s_state_next~1clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \s_state_next~1clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \s_state_next~1clkctrl_outclk\);

-- Location: LCCOMB_X87_Y52_N14
\s_state_next.Init_1004\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_next.Init_1004~combout\ = (GLOBAL(\s_state_next~1clkctrl_outclk\) & ((\s_state_next.Init_1004~combout\))) # (!GLOBAL(\s_state_next~1clkctrl_outclk\) & (!\s_state_current.Reset~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Reset~q\,
	datac => \s_state_next~1clkctrl_outclk\,
	datad => \s_state_next.Init_1004~combout\,
	combout => \s_state_next.Init_1004~combout\);

-- Location: LCCOMB_X87_Y52_N16
\ZUSTAND:zustand.Init~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:zustand.Init~feeder_combout\ = \s_state_next.Init_1004~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \s_state_next.Init_1004~combout\,
	combout => \ZUSTAND:zustand.Init~feeder_combout\);

-- Location: FF_X87_Y52_N17
\ZUSTAND:zustand.Init\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:zustand.Init~feeder_combout\,
	clrn => \nRST~input_o\,
	ena => \WideNor0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:zustand.Init~q\);

-- Location: LCCOMB_X91_Y52_N30
\s_state_current.Init~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_current.Init~1_combout\ = (\ZUSTAND:zustand.Init~q\ & (\Equal0~4_combout\ & \Equal0~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:zustand.Init~q\,
	datac => \Equal0~4_combout\,
	datad => \Equal0~9_combout\,
	combout => \s_state_current.Init~1_combout\);

-- Location: FF_X91_Y52_N31
\s_state_current.Init\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \s_state_current.Init~1_combout\,
	ena => \s_state_current.Init~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Init~q\);

-- Location: LCCOMB_X87_Y52_N8
\s_state_next.Func_996\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_next.Func_996~combout\ = (GLOBAL(\s_state_next~1clkctrl_outclk\) & ((\s_state_next.Func_996~combout\))) # (!GLOBAL(\s_state_next~1clkctrl_outclk\) & (\s_state_current.Init~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Init~q\,
	datac => \s_state_next.Func_996~combout\,
	datad => \s_state_next~1clkctrl_outclk\,
	combout => \s_state_next.Func_996~combout\);

-- Location: LCCOMB_X87_Y52_N20
\ZUSTAND:zustand.Func~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:zustand.Func~feeder_combout\ = \s_state_next.Func_996~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \s_state_next.Func_996~combout\,
	combout => \ZUSTAND:zustand.Func~feeder_combout\);

-- Location: FF_X87_Y52_N21
\ZUSTAND:zustand.Func\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:zustand.Func~feeder_combout\,
	clrn => \nRST~input_o\,
	ena => \WideNor0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:zustand.Func~q\);

-- Location: LCCOMB_X88_Y52_N10
\s_state_current.Func~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_current.Func~0_combout\ = (\ZUSTAND:zustand.Func~q\ & (\Equal0~9_combout\ & \Equal0~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:zustand.Func~q\,
	datac => \Equal0~9_combout\,
	datad => \Equal0~4_combout\,
	combout => \s_state_current.Func~0_combout\);

-- Location: FF_X88_Y52_N11
\s_state_current.Func\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \s_state_current.Func~0_combout\,
	ena => \s_state_current.Init~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Func~q\);

-- Location: LCCOMB_X87_Y52_N6
\s_state_next.Disp_988\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_next.Disp_988~combout\ = (GLOBAL(\s_state_next~1clkctrl_outclk\) & ((\s_state_next.Disp_988~combout\))) # (!GLOBAL(\s_state_next~1clkctrl_outclk\) & (\s_state_current.Func~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Func~q\,
	datac => \s_state_next.Disp_988~combout\,
	datad => \s_state_next~1clkctrl_outclk\,
	combout => \s_state_next.Disp_988~combout\);

-- Location: FF_X87_Y52_N31
\ZUSTAND:zustand.Disp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	asdata => \s_state_next.Disp_988~combout\,
	clrn => \nRST~input_o\,
	sload => VCC,
	ena => \WideNor0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:zustand.Disp~q\);

-- Location: LCCOMB_X88_Y52_N22
\s_state_current.Disp~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_current.Disp~0_combout\ = (\ZUSTAND:zustand.Disp~q\ & (\Equal0~4_combout\ & \Equal0~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \ZUSTAND:zustand.Disp~q\,
	datac => \Equal0~4_combout\,
	datad => \Equal0~9_combout\,
	combout => \s_state_current.Disp~0_combout\);

-- Location: FF_X88_Y52_N23
\s_state_current.Disp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \s_state_current.Disp~0_combout\,
	ena => \s_state_current.Init~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Disp~q\);

-- Location: LCCOMB_X87_Y52_N28
\s_state_next.Entry_980\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_next.Entry_980~combout\ = (GLOBAL(\s_state_next~1clkctrl_outclk\) & (\s_state_next.Entry_980~combout\)) # (!GLOBAL(\s_state_next~1clkctrl_outclk\) & ((\s_state_current.Disp~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_next.Entry_980~combout\,
	datac => \s_state_current.Disp~q\,
	datad => \s_state_next~1clkctrl_outclk\,
	combout => \s_state_next.Entry_980~combout\);

-- Location: LCCOMB_X87_Y52_N26
\ZUSTAND:zustand.Entry~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:zustand.Entry~feeder_combout\ = \s_state_next.Entry_980~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \s_state_next.Entry_980~combout\,
	combout => \ZUSTAND:zustand.Entry~feeder_combout\);

-- Location: FF_X87_Y52_N27
\ZUSTAND:zustand.Entry\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:zustand.Entry~feeder_combout\,
	clrn => \nRST~input_o\,
	ena => \WideNor0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:zustand.Entry~q\);

-- Location: LCCOMB_X88_Y52_N6
\s_state_current.Entry~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_current.Entry~0_combout\ = (\ZUSTAND:zustand.Entry~q\ & (\Equal0~9_combout\ & \Equal0~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:zustand.Entry~q\,
	datab => \Equal0~9_combout\,
	datac => \Equal0~4_combout\,
	combout => \s_state_current.Entry~0_combout\);

-- Location: FF_X88_Y52_N7
\s_state_current.Entry\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \s_state_current.Entry~0_combout\,
	ena => \s_state_current.Init~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Entry~q\);

-- Location: LCCOMB_X87_Y52_N22
\s_state_next.Ready_972\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_next.Ready_972~combout\ = (GLOBAL(\s_state_next~1clkctrl_outclk\) & ((\s_state_next.Ready_972~combout\))) # (!GLOBAL(\s_state_next~1clkctrl_outclk\) & (\s_state_current.Entry~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Entry~q\,
	datac => \s_state_next.Ready_972~combout\,
	datad => \s_state_next~1clkctrl_outclk\,
	combout => \s_state_next.Ready_972~combout\);

-- Location: LCCOMB_X87_Y52_N24
\ZUSTAND:zustand.Ready~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \ZUSTAND:zustand.Ready~feeder_combout\ = \s_state_next.Ready_972~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \s_state_next.Ready_972~combout\,
	combout => \ZUSTAND:zustand.Ready~feeder_combout\);

-- Location: FF_X87_Y52_N25
\ZUSTAND:zustand.Ready\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \ZUSTAND:zustand.Ready~feeder_combout\,
	clrn => \nRST~input_o\,
	ena => \WideNor0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ZUSTAND:zustand.Ready~q\);

-- Location: LCCOMB_X91_Y52_N4
\s_state_current.Ready~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_current.Ready~0_combout\ = (\ZUSTAND:zustand.Ready~q\ & (\Equal0~4_combout\ & \Equal0~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ZUSTAND:zustand.Ready~q\,
	datac => \Equal0~4_combout\,
	datad => \Equal0~9_combout\,
	combout => \s_state_current.Ready~0_combout\);

-- Location: FF_X91_Y52_N5
\s_state_current.Ready\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \s_state_current.Ready~0_combout\,
	ena => \s_state_current.Init~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Ready~q\);

-- Location: LCCOMB_X91_Y52_N28
\WideOr7~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \WideOr7~0_combout\ = (!\s_state_current.Ready~q\ & \s_state_current.Reset~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \s_state_current.Ready~q\,
	datad => \s_state_current.Reset~q\,
	combout => \WideOr7~0_combout\);

-- Location: LCCOMB_X91_Y52_N26
s_rdy : cycloneive_lcell_comb
-- Equation(s):
-- \s_rdy~combout\ = (\WideOr7~0_combout\ & ((\s_rdy~combout\))) # (!\WideOr7~0_combout\ & (\s_state_current.Ready~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Ready~q\,
	datac => \s_rdy~combout\,
	datad => \WideOr7~0_combout\,
	combout => \s_rdy~combout\);

-- Location: LCCOMB_X91_Y52_N14
WideOr7 : cycloneive_lcell_comb
-- Equation(s):
-- \WideOr7~combout\ = (\s_state_current.Ready~q\) # ((\s_state_current.Init~q\) # (!\s_state_current.Reset~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Ready~q\,
	datac => \s_state_current.Init~q\,
	datad => \s_state_current.Reset~q\,
	combout => \WideOr7~combout\);

-- Location: LCCOMB_X91_Y52_N24
s_en : cycloneive_lcell_comb
-- Equation(s):
-- \s_en~combout\ = (\WideOr7~combout\ & ((\s_en~combout\))) # (!\WideOr7~combout\ & (!\s_state_current.Pause~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Pause~q\,
	datac => \WideOr7~combout\,
	datad => \s_en~combout\,
	combout => \s_en~combout\);

-- Location: LCCOMB_X88_Y52_N28
\WideOr6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \WideOr6~0_combout\ = (\s_state_current.Entry~q\) # ((\s_state_current.Disp~q\) # (\s_state_current.Func~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Entry~q\,
	datac => \s_state_current.Disp~q\,
	datad => \s_state_current.Func~q\,
	combout => \WideOr6~0_combout\);

-- Location: CLKCTRL_G9
\WideOr6~0clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \WideOr6~0clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \WideOr6~0clkctrl_outclk\);

-- Location: LCCOMB_X88_Y52_N24
\s_data[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- s_data(1) = (GLOBAL(\WideOr6~0clkctrl_outclk\) & (\s_state_current.Entry~q\)) # (!GLOBAL(\WideOr6~0clkctrl_outclk\) & ((s_data(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \WideOr6~0clkctrl_outclk\,
	datac => \s_state_current.Entry~q\,
	datad => s_data(1),
	combout => s_data(1));

-- Location: LCCOMB_X88_Y52_N2
\s_data[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- s_data(3) = (GLOBAL(\WideOr6~0clkctrl_outclk\) & (!\s_state_current.Entry~q\)) # (!GLOBAL(\WideOr6~0clkctrl_outclk\) & ((s_data(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \WideOr6~0clkctrl_outclk\,
	datac => \s_state_current.Entry~q\,
	datad => s_data(3),
	combout => s_data(3));

-- Location: LCCOMB_X88_Y52_N18
\s_data~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_data~0_combout\ = (!\s_state_current.Entry~q\ & !\s_state_current.Disp~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Entry~q\,
	datad => \s_state_current.Disp~q\,
	combout => \s_data~0_combout\);

-- Location: LCCOMB_X88_Y52_N30
\s_data[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- s_data(4) = (GLOBAL(\WideOr6~0clkctrl_outclk\) & ((\s_data~0_combout\))) # (!GLOBAL(\WideOr6~0clkctrl_outclk\) & (s_data(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => s_data(4),
	datab => \WideOr6~0clkctrl_outclk\,
	datac => \s_data~0_combout\,
	combout => s_data(4));

-- Location: IOIBUF_X1_Y0_N22
\BCD[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BCD(0),
	o => \BCD[0]~input_o\);

-- Location: IOIBUF_X0_Y5_N15
\BCD[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BCD(1),
	o => \BCD[1]~input_o\);

-- Location: IOIBUF_X0_Y50_N15
\BCD[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BCD(2),
	o => \BCD[2]~input_o\);

-- Location: IOIBUF_X38_Y73_N22
\BCD[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BCD(3),
	o => \BCD[3]~input_o\);

-- Location: IOIBUF_X115_Y36_N15
\Gray[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Gray(0),
	o => \Gray[0]~input_o\);

-- Location: IOIBUF_X54_Y0_N1
\Gray[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Gray(1),
	o => \Gray[1]~input_o\);

-- Location: IOIBUF_X23_Y0_N15
\Gray[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Gray(2),
	o => \Gray[2]~input_o\);

-- Location: IOIBUF_X1_Y73_N1
\Gray[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Gray(3),
	o => \Gray[3]~input_o\);

-- Location: IOIBUF_X20_Y73_N15
\Aiken[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Aiken(0),
	o => \Aiken[0]~input_o\);

-- Location: IOIBUF_X65_Y73_N8
\Aiken[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Aiken(1),
	o => \Aiken[1]~input_o\);

-- Location: IOIBUF_X40_Y0_N15
\Aiken[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Aiken(2),
	o => \Aiken[2]~input_o\);

-- Location: IOIBUF_X115_Y44_N8
\Aiken[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Aiken(3),
	o => \Aiken[3]~input_o\);

ww_F_RDY <= \F_RDY~output_o\;

ww_LCD_EN <= \LCD_EN~output_o\;

ww_LCD_RS <= \LCD_RS~output_o\;

ww_LCD_RW <= \LCD_RW~output_o\;

ww_LCD_DATA(0) <= \LCD_DATA[0]~output_o\;

ww_LCD_DATA(1) <= \LCD_DATA[1]~output_o\;

ww_LCD_DATA(2) <= \LCD_DATA[2]~output_o\;

ww_LCD_DATA(3) <= \LCD_DATA[3]~output_o\;

ww_LCD_DATA(4) <= \LCD_DATA[4]~output_o\;

ww_LCD_DATA(5) <= \LCD_DATA[5]~output_o\;

ww_LCD_DATA(6) <= \LCD_DATA[6]~output_o\;

ww_LCD_DATA(7) <= \LCD_DATA[7]~output_o\;
END structure;


