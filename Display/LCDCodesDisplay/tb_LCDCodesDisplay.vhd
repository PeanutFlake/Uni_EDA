LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_LCDCodesDisplay IS
END ENTITY;

ARCHITECTURE Testbench OF tb_LCDCodesDisplay IS
	COMPONENT LCDCodesDisplay IS
		GENERIC (
			F_CLK					:	Integer range 0 to 2147483647 := 50*(10**6);
			FRAMES_PER_SECOND	:	Integer range 0 to 60			:=	24
		);
		PORT (
			CLK, nRST			:	IN		std_logic;
			BCD, GRAY, AIKEN	:	IN		std_logic_vector(3 downto 0);
			-- LCD Connections
			LCD_Data						:	OUT	std_logic_vector(7 downto 0);
			LCD_EN, LCD_RW, LCD_RS	:	OUT	std_logic
		);
	END COMPONENT;
	
	SIGNAL s_clk, s_rst				:	std_logic	:= '1';
	SIGNAL s_bcd, s_gray, s_aiken : 	std_logic_vector(3 downto 0) := (Others => '0');
	
BEGIN

	DUT	:	LCDCodesDisplay
	GENERIC MAP (50*(10**6), 24)
	PORT MAP (
		CLK => s_clk,
		nRST => s_rst,
		BCD => s_bcd,
		GRAY => s_gray,
		AIKEN => s_aiken
	);
	
	s_clk <= not s_clk after 20ns;
	
	s_bcd <= "0100" after 0ns;
	s_aiken <= "1100" after 0ns;

END Testbench;