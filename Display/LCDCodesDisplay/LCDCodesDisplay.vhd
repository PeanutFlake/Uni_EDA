LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY LCDCodesDisplay IS
	GENERIC (
		F_CLK					:	Integer range 0 to 2147483647 := 50*(10**6);
		FRAMES_PER_SECOND	:	Integer range 0 to 60			:=	24
	);
	PORT (
		CLK, nRST			:	IN		std_logic;
		BCD, GRAY, AIKEN	:	IN		std_logic_vector(3 downto 0);
		-- LCD Connections
		LCD_Data						:	OUT	std_logic_vector(7 downto 0);
		LCD_EN, LCD_RW, LCD_RS	:	OUT	std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF LCDCodesDisplay IS

	TYPE Input_Row 	IS ARRAY (integer range <>) of std_logic_vector(7 downto 0);
	TYPE Input_Data	IS ARRAY (integer range <>) of Input_Row(0 to 15);

	FUNCTION encode_charset(d : std_logic_vector(3 downto 0)) 
		return Input_Row is
		VARIABLE data : Input_Row(0 to 3) := (Others => (Others => '0'));
		VARIABLE char : std_logic_vector(7 downto 0) := (Others => '0');
	BEGIN
		for n in 0 to 3 loop
			if ( d(n) = '0' ) then
				char := "00110000";
			elsif ( d(n) = '1' ) then
				char := "00110001";
			else
				char := "00111111";
			end if;
			data(n) := char;
		end loop;
		return data;
	END encode_charset;
	
	COMPONENT LCDDeviceController IS
		GENERIC (
			F_CLK				:	Integer range 0 to 2147483647 := 50*(10**6);
			LINEWIDTH		:	Integer range 0 to 48			:=	16
		);
		PORT (
			CLK, nRST, WEN	:	IN		std_logic;
			DI					:	IN		std_logic_vector(7 downto 0);
			lcd_data			:	OUT	std_logic_vector(7 downto 0);
			lcd_en, lcd_rw, lcd_rs	:	OUT	std_logic;
			busy				:	OUT	std_logic
		);
	END COMPONENT;
	
	CONSTANT HoldTime		:	Integer					:= F_CLK - 33*(F_CLK/20000+3)*FRAMES_PER_SECOND;	-- ((number of chars) + 1) * (WT_Send + 3)
	CONSTANT TopRow 		:	Input_Row(0 to 15)	:=
	(
		"00100000", "01000010", "01000011", "01000100", "00100000", "01000111", "01010010", "01000101", "01011001", "00100000", "01000001", "01001001", "01001011", "01000101", "01001110", "00100000"
	);
	
	SIGNAL s_en, s_busy	:	std_logic	:= '0';
	SIGNAL s_data			:	std_logic_vector(7 downto 0) := (Others => '0');
	
	SIGNAL s_input			:	Input_Data(0 to 1) := (Others => (Others => (Others => '0')));
	
BEGIN

	DISPLAY : LCDDeviceController
	GENERIC MAP (F_CLK, 16)
	PORT MAP (
		CLK => CLK,
		nRST => nRST,
		WEN => s_en,
		DI => s_data,
		lcd_data => LCD_Data,
		lcd_en => LCD_EN,
		lcd_rs => LCD_RS,
		lcd_rw => LCD_RW,
		busy => s_busy
	);
	
	PROCESS (CLK, nRST, s_busy)
		VARIABLE ci		:	Integer range 0 to 15			:= 0;
		VARIABLE ri		:	Integer range 0 to 1				:= 0;
		VARIABLE t_wait:	Integer range 0 to 2147483647	:= 0;
	BEGIN
		if ( nRST = '1' ) then
			if ( rising_edge(CLK) ) then
				if ( t_wait = 0 ) then
					if ( s_busy = '0' ) then
						s_data <= s_input(ri)(ci);
						ci := ci + 1;
						if ( ci = 15 ) then
							ri := ri + 1;
							if ( ri = 1 ) then
								ri := 0;
								t_wait := HoldTime;
							end if;
						end if;
						s_en <= '1';
					else
						-- controller is busy
						s_en <= '0';
					end if;
				else
					t_wait := t_wait - 1;
				end if;
			else
				-- out of clock
			end if;
		else
			-- reset
		end if;
	END PROCESS;

	s_input(0) <= TopRow;
	s_input(1)(0 to 3) <= encode_charset(BCD(3 downto 0));
	s_input(1)(5 to 8) <= encode_charset(GRAY(3 downto 0));
	s_input(1)(10 to 13) <= encode_charset(AIKEN(3 downto 0));

END Behaviour;