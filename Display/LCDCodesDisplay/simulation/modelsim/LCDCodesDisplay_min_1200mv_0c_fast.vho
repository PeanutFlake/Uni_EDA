-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "06/18/2018 18:32:41"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	LCDCodesDisplay IS
    PORT (
	CLK : IN std_logic;
	nRST : IN std_logic;
	BCD : IN std_logic_vector(3 DOWNTO 0);
	GRAY : IN std_logic_vector(3 DOWNTO 0);
	AIKEN : IN std_logic_vector(3 DOWNTO 0);
	LCD_Data : OUT std_logic_vector(7 DOWNTO 0);
	LCD_EN : OUT std_logic;
	LCD_RW : OUT std_logic;
	LCD_RS : OUT std_logic
	);
END LCDCodesDisplay;

-- Design Ports Information
-- BCD[0]	=>  Location: PIN_A21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BCD[1]	=>  Location: PIN_F7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BCD[2]	=>  Location: PIN_Y6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BCD[3]	=>  Location: PIN_AE14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- GRAY[0]	=>  Location: PIN_D4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- GRAY[1]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- GRAY[2]	=>  Location: PIN_J3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- GRAY[3]	=>  Location: PIN_V6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- AIKEN[0]	=>  Location: PIN_A10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- AIKEN[1]	=>  Location: PIN_H10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- AIKEN[2]	=>  Location: PIN_R22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- AIKEN[3]	=>  Location: PIN_Y24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[0]	=>  Location: PIN_L8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[1]	=>  Location: PIN_M5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[2]	=>  Location: PIN_L7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[3]	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[4]	=>  Location: PIN_N4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[5]	=>  Location: PIN_L6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[6]	=>  Location: PIN_M3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_Data[7]	=>  Location: PIN_J6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_EN	=>  Location: PIN_M7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_RW	=>  Location: PIN_R23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_RS	=>  Location: PIN_M8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- nRST	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF LCDCodesDisplay IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_nRST : std_logic;
SIGNAL ww_BCD : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_GRAY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_AIKEN : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_LCD_Data : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_LCD_EN : std_logic;
SIGNAL ww_LCD_RW : std_logic;
SIGNAL ww_LCD_RS : std_logic;
SIGNAL \nRST~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \CLK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \BCD[0]~input_o\ : std_logic;
SIGNAL \BCD[1]~input_o\ : std_logic;
SIGNAL \BCD[2]~input_o\ : std_logic;
SIGNAL \BCD[3]~input_o\ : std_logic;
SIGNAL \GRAY[0]~input_o\ : std_logic;
SIGNAL \GRAY[1]~input_o\ : std_logic;
SIGNAL \GRAY[2]~input_o\ : std_logic;
SIGNAL \GRAY[3]~input_o\ : std_logic;
SIGNAL \AIKEN[0]~input_o\ : std_logic;
SIGNAL \AIKEN[1]~input_o\ : std_logic;
SIGNAL \AIKEN[2]~input_o\ : std_logic;
SIGNAL \AIKEN[3]~input_o\ : std_logic;
SIGNAL \LCD_Data[0]~output_o\ : std_logic;
SIGNAL \LCD_Data[1]~output_o\ : std_logic;
SIGNAL \LCD_Data[2]~output_o\ : std_logic;
SIGNAL \LCD_Data[3]~output_o\ : std_logic;
SIGNAL \LCD_Data[4]~output_o\ : std_logic;
SIGNAL \LCD_Data[5]~output_o\ : std_logic;
SIGNAL \LCD_Data[6]~output_o\ : std_logic;
SIGNAL \LCD_Data[7]~output_o\ : std_logic;
SIGNAL \LCD_EN~output_o\ : std_logic;
SIGNAL \LCD_RW~output_o\ : std_logic;
SIGNAL \LCD_RS~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \CLK~inputclkctrl_outclk\ : std_logic;
SIGNAL \nRST~input_o\ : std_logic;
SIGNAL \Add2~0_combout\ : std_logic;
SIGNAL \t_wait[0]~56_combout\ : std_logic;
SIGNAL \Add2~1\ : std_logic;
SIGNAL \Add2~2_combout\ : std_logic;
SIGNAL \t_wait[1]~55_combout\ : std_logic;
SIGNAL \Add2~3\ : std_logic;
SIGNAL \Add2~4_combout\ : std_logic;
SIGNAL \t_wait[2]~54_combout\ : std_logic;
SIGNAL \Add2~5\ : std_logic;
SIGNAL \Add2~6_combout\ : std_logic;
SIGNAL \ci[0]~1_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~24_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_send_next~0_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_send_next~q\ : std_logic;
SIGNAL \DISPLAY|s_state_send_current~feeder_combout\ : std_logic;
SIGNAL \nRST~inputclkctrl_outclk\ : std_logic;
SIGNAL \DISPLAY|s_state_send_current~q\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_y~1_combout\ : std_logic;
SIGNAL \DISPLAY|Selector1~0_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_next.Send~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_y~0_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_next~10_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_next.Init~0_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_next.Start~0_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_next.Start~1_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_next.Start~2_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_next.Start~q\ : std_logic;
SIGNAL \DISPLAY|s_state_current.Start~feeder_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_current.Start~q\ : std_logic;
SIGNAL \DISPLAY|s_state_next.Init~1_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_next.Init~q\ : std_logic;
SIGNAL \DISPLAY|s_state_current.Init~q\ : std_logic;
SIGNAL \DISPLAY|s_state_init_next.Finish~1_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_init_next.Func~q\ : std_logic;
SIGNAL \DISPLAY|s_state_init_current.Func~q\ : std_logic;
SIGNAL \DISPLAY|Selector2~0_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_init_next.Disp~q\ : std_logic;
SIGNAL \DISPLAY|s_state_init_current.Disp~q\ : std_logic;
SIGNAL \DISPLAY|Selector3~0_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_init_next.Entry~q\ : std_logic;
SIGNAL \DISPLAY|s_state_init_current.Entry~q\ : std_logic;
SIGNAL \DISPLAY|Selector4~0_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_init_next.Finish~q\ : std_logic;
SIGNAL \DISPLAY|s_state_init_current.Finish~q\ : std_logic;
SIGNAL \DISPLAY|s_state_init_next.Reset~2_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_init_next.Reset~q\ : std_logic;
SIGNAL \DISPLAY|s_state_init_current.Reset~q\ : std_logic;
SIGNAL \DISPLAY|rs~3_combout\ : std_logic;
SIGNAL \DISPLAY|rs~2_combout\ : std_logic;
SIGNAL \DISPLAY|Equal0~10_combout\ : std_logic;
SIGNAL \DISPLAY|rs~5_combout\ : std_logic;
SIGNAL \DISPLAY|rs~4_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:rs~q\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_x[0]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_x[0]~q\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_x[1]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_x[1]~q\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_x[2]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_x[2]~q\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_x[3]~2_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_x[3]~q\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[19]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[0]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[0]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~25\ : std_logic;
SIGNAL \DISPLAY|Add1~26_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[1]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[1]~q\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[17]~1_combout\ : std_logic;
SIGNAL \DISPLAY|t_wait~0_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~27\ : std_logic;
SIGNAL \DISPLAY|Add1~28_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[2]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[2]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~29\ : std_logic;
SIGNAL \DISPLAY|Add1~30_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~91_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[3]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~31\ : std_logic;
SIGNAL \DISPLAY|Add1~32_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[4]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[17]~2_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[4]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~33\ : std_logic;
SIGNAL \DISPLAY|Add1~34_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~90_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[5]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~35\ : std_logic;
SIGNAL \DISPLAY|Add1~36_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[6]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[6]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~37\ : std_logic;
SIGNAL \DISPLAY|Add1~38_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[7]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[7]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~39\ : std_logic;
SIGNAL \DISPLAY|Add1~40_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[8]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[8]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~41\ : std_logic;
SIGNAL \DISPLAY|Add1~42_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[9]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[9]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~43\ : std_logic;
SIGNAL \DISPLAY|Add1~44_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[10]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[10]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~45\ : std_logic;
SIGNAL \DISPLAY|Add1~46_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[11]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[11]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~47\ : std_logic;
SIGNAL \DISPLAY|Add1~48_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[12]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[12]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~49\ : std_logic;
SIGNAL \DISPLAY|Add1~50_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~89_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[13]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~51\ : std_logic;
SIGNAL \DISPLAY|Add1~52_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[14]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[14]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~53\ : std_logic;
SIGNAL \DISPLAY|Add1~54_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[15]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[15]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~55\ : std_logic;
SIGNAL \DISPLAY|Add1~56_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[16]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[16]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~57\ : std_logic;
SIGNAL \DISPLAY|Add1~58_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[17]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[17]~q\ : std_logic;
SIGNAL \DISPLAY|Equal0~7_combout\ : std_logic;
SIGNAL \DISPLAY|Equal0~8_combout\ : std_logic;
SIGNAL \DISPLAY|Equal0~6_combout\ : std_logic;
SIGNAL \DISPLAY|Equal0~5_combout\ : std_logic;
SIGNAL \DISPLAY|Equal0~9_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~59\ : std_logic;
SIGNAL \DISPLAY|Add1~60_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~88_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[18]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~61\ : std_logic;
SIGNAL \DISPLAY|Add1~62_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[19]~3_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[19]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~63\ : std_logic;
SIGNAL \DISPLAY|Add1~64_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~103_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[20]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~65\ : std_logic;
SIGNAL \DISPLAY|Add1~66_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~102_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[21]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~67\ : std_logic;
SIGNAL \DISPLAY|Add1~68_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~101_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[22]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~69\ : std_logic;
SIGNAL \DISPLAY|Add1~70_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~100_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[23]~q\ : std_logic;
SIGNAL \DISPLAY|Equal0~2_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~71\ : std_logic;
SIGNAL \DISPLAY|Add1~72_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~99_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[24]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~73\ : std_logic;
SIGNAL \DISPLAY|Add1~74_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~98_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[25]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~75\ : std_logic;
SIGNAL \DISPLAY|Add1~76_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~97_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[26]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~77\ : std_logic;
SIGNAL \DISPLAY|Add1~78_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~96_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[27]~q\ : std_logic;
SIGNAL \DISPLAY|Equal0~1_combout\ : std_logic;
SIGNAL \DISPLAY|Equal0~3_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~79\ : std_logic;
SIGNAL \DISPLAY|Add1~80_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~95_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[28]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~81\ : std_logic;
SIGNAL \DISPLAY|Add1~82_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~94_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[29]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~83\ : std_logic;
SIGNAL \DISPLAY|Add1~84_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~93_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[30]~q\ : std_logic;
SIGNAL \DISPLAY|Add1~85\ : std_logic;
SIGNAL \DISPLAY|Add1~86_combout\ : std_logic;
SIGNAL \DISPLAY|Add1~92_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:t_wait[31]~q\ : std_logic;
SIGNAL \DISPLAY|Equal0~0_combout\ : std_logic;
SIGNAL \DISPLAY|Equal0~4_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_init_next.Finish~0_combout\ : std_logic;
SIGNAL \DISPLAY|s_busy~3_combout\ : std_logic;
SIGNAL \DISPLAY|s_busy~q\ : std_logic;
SIGNAL \ci[3]~0_combout\ : std_logic;
SIGNAL \Add0~0_combout\ : std_logic;
SIGNAL \Add0~1_combout\ : std_logic;
SIGNAL \Add0~2_combout\ : std_logic;
SIGNAL \t_wait[30]~36_combout\ : std_logic;
SIGNAL \t_wait[30]~37_combout\ : std_logic;
SIGNAL \t_wait[30]~38_combout\ : std_logic;
SIGNAL \Add2~7\ : std_logic;
SIGNAL \Add2~8_combout\ : std_logic;
SIGNAL \Add2~9\ : std_logic;
SIGNAL \Add2~10_combout\ : std_logic;
SIGNAL \t_wait[5]~53_combout\ : std_logic;
SIGNAL \Add2~11\ : std_logic;
SIGNAL \Add2~12_combout\ : std_logic;
SIGNAL \Add2~13\ : std_logic;
SIGNAL \Add2~14_combout\ : std_logic;
SIGNAL \Add2~15\ : std_logic;
SIGNAL \Add2~16_combout\ : std_logic;
SIGNAL \t_wait[8]~52_combout\ : std_logic;
SIGNAL \Add2~17\ : std_logic;
SIGNAL \Add2~18_combout\ : std_logic;
SIGNAL \t_wait[9]~51_combout\ : std_logic;
SIGNAL \Add2~19\ : std_logic;
SIGNAL \Add2~20_combout\ : std_logic;
SIGNAL \t_wait[10]~50_combout\ : std_logic;
SIGNAL \Equal0~6_combout\ : std_logic;
SIGNAL \Equal0~7_combout\ : std_logic;
SIGNAL \Equal0~8_combout\ : std_logic;
SIGNAL \Add2~21\ : std_logic;
SIGNAL \Add2~22_combout\ : std_logic;
SIGNAL \t_wait[11]~49_combout\ : std_logic;
SIGNAL \Add2~23\ : std_logic;
SIGNAL \Add2~24_combout\ : std_logic;
SIGNAL \Add2~25\ : std_logic;
SIGNAL \Add2~26_combout\ : std_logic;
SIGNAL \Add2~27\ : std_logic;
SIGNAL \Add2~28_combout\ : std_logic;
SIGNAL \t_wait[14]~48_combout\ : std_logic;
SIGNAL \Equal0~5_combout\ : std_logic;
SIGNAL \Add2~29\ : std_logic;
SIGNAL \Add2~30_combout\ : std_logic;
SIGNAL \Add2~31\ : std_logic;
SIGNAL \Add2~32_combout\ : std_logic;
SIGNAL \t_wait[16]~47_combout\ : std_logic;
SIGNAL \Add2~33\ : std_logic;
SIGNAL \Add2~34_combout\ : std_logic;
SIGNAL \t_wait[17]~46_combout\ : std_logic;
SIGNAL \Add2~35\ : std_logic;
SIGNAL \Add2~36_combout\ : std_logic;
SIGNAL \Equal0~3_combout\ : std_logic;
SIGNAL \Add2~37\ : std_logic;
SIGNAL \Add2~38_combout\ : std_logic;
SIGNAL \Add2~39\ : std_logic;
SIGNAL \Add2~40_combout\ : std_logic;
SIGNAL \Add2~41\ : std_logic;
SIGNAL \Add2~42_combout\ : std_logic;
SIGNAL \t_wait[21]~45_combout\ : std_logic;
SIGNAL \Add2~43\ : std_logic;
SIGNAL \Add2~44_combout\ : std_logic;
SIGNAL \Add2~45\ : std_logic;
SIGNAL \Add2~46_combout\ : std_logic;
SIGNAL \Add2~47\ : std_logic;
SIGNAL \Add2~48_combout\ : std_logic;
SIGNAL \t_wait[24]~44_combout\ : std_logic;
SIGNAL \Add2~49\ : std_logic;
SIGNAL \Add2~50_combout\ : std_logic;
SIGNAL \Add2~51\ : std_logic;
SIGNAL \Add2~52_combout\ : std_logic;
SIGNAL \t_wait[26]~43_combout\ : std_logic;
SIGNAL \Add2~53\ : std_logic;
SIGNAL \Add2~54_combout\ : std_logic;
SIGNAL \t_wait[27]~42_combout\ : std_logic;
SIGNAL \Add2~55\ : std_logic;
SIGNAL \Add2~56_combout\ : std_logic;
SIGNAL \t_wait[28]~41_combout\ : std_logic;
SIGNAL \Add2~57\ : std_logic;
SIGNAL \Add2~58_combout\ : std_logic;
SIGNAL \t_wait[29]~40_combout\ : std_logic;
SIGNAL \Add2~59\ : std_logic;
SIGNAL \Add2~60_combout\ : std_logic;
SIGNAL \t_wait[30]~39_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \Equal0~4_combout\ : std_logic;
SIGNAL \Equal0~9_combout\ : std_logic;
SIGNAL \s_en~0_combout\ : std_logic;
SIGNAL \s_en~q\ : std_logic;
SIGNAL \DISPLAY|s_busy~2_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_next.Send~q\ : std_logic;
SIGNAL \DISPLAY|s_state_current.Send~q\ : std_logic;
SIGNAL \DISPLAY|s_state_next.Ready~0_combout\ : std_logic;
SIGNAL \DISPLAY|s_state_next.Ready~q\ : std_logic;
SIGNAL \DISPLAY|s_state_current.Ready~q\ : std_logic;
SIGNAL \Mux6~0_combout\ : std_logic;
SIGNAL \s_data[0]~0_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:data[0]~q\ : std_logic;
SIGNAL \DISPLAY|data~1_combout\ : std_logic;
SIGNAL \DISPLAY|data~0_combout\ : std_logic;
SIGNAL \DISPLAY|data~2_combout\ : std_logic;
SIGNAL \DISPLAY|data~3_combout\ : std_logic;
SIGNAL \Mux5~0_combout\ : std_logic;
SIGNAL \DISPLAY|data~4_combout\ : std_logic;
SIGNAL \DISPLAY|data~5_combout\ : std_logic;
SIGNAL \DISPLAY|data~7_combout\ : std_logic;
SIGNAL \DISPLAY|data~9_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:data[1]~q\ : std_logic;
SIGNAL \DISPLAY|data~6_combout\ : std_logic;
SIGNAL \DISPLAY|data~8_combout\ : std_logic;
SIGNAL \DISPLAY|data~10_combout\ : std_logic;
SIGNAL \Mux4~0_combout\ : std_logic;
SIGNAL \DISPLAY|data~11_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:data[2]~q\ : std_logic;
SIGNAL \DISPLAY|data~12_combout\ : std_logic;
SIGNAL \DISPLAY|s_data[2]~feeder_combout\ : std_logic;
SIGNAL \Mux3~0_combout\ : std_logic;
SIGNAL \DISPLAY|data~13_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:data[3]~q\ : std_logic;
SIGNAL \DISPLAY|data~14_combout\ : std_logic;
SIGNAL \DISPLAY|s_data[3]~feeder_combout\ : std_logic;
SIGNAL \Mux2~0_combout\ : std_logic;
SIGNAL \DISPLAY|data~15_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:data[4]~q\ : std_logic;
SIGNAL \DISPLAY|data~16_combout\ : std_logic;
SIGNAL \DISPLAY|s_data[4]~feeder_combout\ : std_logic;
SIGNAL \Mux1~0_combout\ : std_logic;
SIGNAL \s_data[5]~1_combout\ : std_logic;
SIGNAL \DISPLAY|data~17_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:data[5]~q\ : std_logic;
SIGNAL \DISPLAY|data~18_combout\ : std_logic;
SIGNAL \DISPLAY|data~21_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_y~2_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:c_pos_y~q\ : std_logic;
SIGNAL \DISPLAY|data~22_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:data[6]~q\ : std_logic;
SIGNAL \DISPLAY|data~19_combout\ : std_logic;
SIGNAL \DISPLAY|data~20_combout\ : std_logic;
SIGNAL \DISPLAY|data~23_combout\ : std_logic;
SIGNAL \DISPLAY|s_data[6]~feeder_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:data[7]~q\ : std_logic;
SIGNAL \DISPLAY|data~24_combout\ : std_logic;
SIGNAL \DISPLAY|data~25_combout\ : std_logic;
SIGNAL \DISPLAY|s_data[7]~feeder_combout\ : std_logic;
SIGNAL \DISPLAY|P_UEBERGANG:en~q\ : std_logic;
SIGNAL \DISPLAY|en~0_combout\ : std_logic;
SIGNAL \DISPLAY|en~1_combout\ : std_logic;
SIGNAL \DISPLAY|s_en~q\ : std_logic;
SIGNAL \DISPLAY|s_rs~q\ : std_logic;
SIGNAL \DISPLAY|s_data\ : std_logic_vector(7 DOWNTO 0);
SIGNAL s_data : std_logic_vector(7 DOWNTO 0);
SIGNAL ci : std_logic_vector(3 DOWNTO 0);
SIGNAL t_wait : std_logic_vector(30 DOWNTO 0);

BEGIN

ww_CLK <= CLK;
ww_nRST <= nRST;
ww_BCD <= BCD;
ww_GRAY <= GRAY;
ww_AIKEN <= AIKEN;
LCD_Data <= ww_LCD_Data;
LCD_EN <= ww_LCD_EN;
LCD_RW <= ww_LCD_RW;
LCD_RS <= ww_LCD_RS;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\nRST~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \nRST~input_o\);

\CLK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLK~input_o\);

-- Location: IOOBUF_X0_Y48_N9
\LCD_Data[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISPLAY|s_data\(0),
	devoe => ww_devoe,
	o => \LCD_Data[0]~output_o\);

-- Location: IOOBUF_X0_Y47_N2
\LCD_Data[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISPLAY|s_data\(1),
	devoe => ww_devoe,
	o => \LCD_Data[1]~output_o\);

-- Location: IOOBUF_X0_Y47_N16
\LCD_Data[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISPLAY|s_data\(2),
	devoe => ww_devoe,
	o => \LCD_Data[2]~output_o\);

-- Location: IOOBUF_X0_Y48_N2
\LCD_Data[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISPLAY|s_data\(3),
	devoe => ww_devoe,
	o => \LCD_Data[3]~output_o\);

-- Location: IOOBUF_X0_Y46_N16
\LCD_Data[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISPLAY|s_data\(4),
	devoe => ww_devoe,
	o => \LCD_Data[4]~output_o\);

-- Location: IOOBUF_X0_Y47_N23
\LCD_Data[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISPLAY|s_data\(5),
	devoe => ww_devoe,
	o => \LCD_Data[5]~output_o\);

-- Location: IOOBUF_X0_Y51_N16
\LCD_Data[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISPLAY|s_data\(6),
	devoe => ww_devoe,
	o => \LCD_Data[6]~output_o\);

-- Location: IOOBUF_X0_Y50_N16
\LCD_Data[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISPLAY|s_data\(7),
	devoe => ww_devoe,
	o => \LCD_Data[7]~output_o\);

-- Location: IOOBUF_X0_Y45_N23
\LCD_EN~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISPLAY|s_en~q\,
	devoe => ww_devoe,
	o => \LCD_EN~output_o\);

-- Location: IOOBUF_X115_Y35_N16
\LCD_RW~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_RW~output_o\);

-- Location: IOOBUF_X0_Y45_N16
\LCD_RS~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISPLAY|s_rs~q\,
	devoe => ww_devoe,
	o => \LCD_RS~output_o\);

-- Location: IOIBUF_X0_Y36_N8
\CLK~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: CLKCTRL_G2
\CLK~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLK~inputclkctrl_outclk\);

-- Location: IOIBUF_X0_Y36_N15
\nRST~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_nRST,
	o => \nRST~input_o\);

-- Location: LCCOMB_X8_Y50_N2
\Add2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~0_combout\ = t_wait(0) $ (VCC)
-- \Add2~1\ = CARRY(t_wait(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => t_wait(0),
	datad => VCC,
	combout => \Add2~0_combout\,
	cout => \Add2~1\);

-- Location: LCCOMB_X7_Y50_N6
\t_wait[0]~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[0]~56_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~0_combout\)))) # (!\nRST~input_o\ & (((t_wait(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(0),
	datad => \Add2~0_combout\,
	combout => \t_wait[0]~56_combout\);

-- Location: FF_X7_Y50_N7
\t_wait[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[0]~56_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(0));

-- Location: LCCOMB_X8_Y50_N4
\Add2~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~2_combout\ = (t_wait(1) & (\Add2~1\ & VCC)) # (!t_wait(1) & (!\Add2~1\))
-- \Add2~3\ = CARRY((!t_wait(1) & !\Add2~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(1),
	datad => VCC,
	cin => \Add2~1\,
	combout => \Add2~2_combout\,
	cout => \Add2~3\);

-- Location: LCCOMB_X7_Y50_N26
\t_wait[1]~55\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[1]~55_combout\ = (\nRST~input_o\ & (((!\Equal0~9_combout\ & \Add2~2_combout\)))) # (!\nRST~input_o\ & (t_wait(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => t_wait(1),
	datac => \Equal0~9_combout\,
	datad => \Add2~2_combout\,
	combout => \t_wait[1]~55_combout\);

-- Location: FF_X8_Y50_N23
\t_wait[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \t_wait[1]~55_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(1));

-- Location: LCCOMB_X8_Y50_N6
\Add2~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~4_combout\ = (t_wait(2) & ((GND) # (!\Add2~3\))) # (!t_wait(2) & (\Add2~3\ $ (GND)))
-- \Add2~5\ = CARRY((t_wait(2)) # (!\Add2~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(2),
	datad => VCC,
	cin => \Add2~3\,
	combout => \Add2~4_combout\,
	cout => \Add2~5\);

-- Location: LCCOMB_X8_Y50_N0
\t_wait[2]~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[2]~54_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~4_combout\)))) # (!\nRST~input_o\ & (((t_wait(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \Equal0~9_combout\,
	datac => t_wait(2),
	datad => \Add2~4_combout\,
	combout => \t_wait[2]~54_combout\);

-- Location: FF_X8_Y50_N1
\t_wait[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[2]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(2));

-- Location: LCCOMB_X8_Y50_N8
\Add2~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~6_combout\ = (t_wait(3) & (\Add2~5\ & VCC)) # (!t_wait(3) & (!\Add2~5\))
-- \Add2~7\ = CARRY((!t_wait(3) & !\Add2~5\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(3),
	datad => VCC,
	cin => \Add2~5\,
	combout => \Add2~6_combout\,
	cout => \Add2~7\);

-- Location: LCCOMB_X6_Y50_N14
\ci[0]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ci[0]~1_combout\ = !ci(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => ci(0),
	combout => \ci[0]~1_combout\);

-- Location: LCCOMB_X4_Y46_N0
\DISPLAY|Add1~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~24_combout\ = \DISPLAY|P_UEBERGANG:t_wait[0]~q\ $ (VCC)
-- \DISPLAY|Add1~25\ = CARRY(\DISPLAY|P_UEBERGANG:t_wait[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[0]~q\,
	datad => VCC,
	combout => \DISPLAY|Add1~24_combout\,
	cout => \DISPLAY|Add1~25\);

-- Location: LCCOMB_X6_Y45_N6
\DISPLAY|s_state_send_next~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_send_next~0_combout\ = (\DISPLAY|s_state_current.Send~q\ & ((\DISPLAY|s_state_init_next.Finish~0_combout\ & (!\DISPLAY|s_state_send_current~q\)) # (!\DISPLAY|s_state_init_next.Finish~0_combout\ & ((\DISPLAY|s_state_send_next~q\))))) # 
-- (!\DISPLAY|s_state_current.Send~q\ & (((\DISPLAY|s_state_send_next~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Send~q\,
	datab => \DISPLAY|s_state_send_current~q\,
	datac => \DISPLAY|s_state_send_next~q\,
	datad => \DISPLAY|s_state_init_next.Finish~0_combout\,
	combout => \DISPLAY|s_state_send_next~0_combout\);

-- Location: FF_X6_Y45_N7
\DISPLAY|s_state_send_next\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|s_state_send_next~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_send_next~q\);

-- Location: LCCOMB_X6_Y45_N28
\DISPLAY|s_state_send_current~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_send_current~feeder_combout\ = \DISPLAY|s_state_send_next~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \DISPLAY|s_state_send_next~q\,
	combout => \DISPLAY|s_state_send_current~feeder_combout\);

-- Location: CLKCTRL_G4
\nRST~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \nRST~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \nRST~inputclkctrl_outclk\);

-- Location: FF_X6_Y45_N29
\DISPLAY|s_state_send_current\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|s_state_send_current~feeder_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_send_current~q\);

-- Location: LCCOMB_X6_Y45_N26
\DISPLAY|P_UEBERGANG:c_pos_y~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:c_pos_y~1_combout\ = (\DISPLAY|s_state_send_current~q\ & \DISPLAY|s_state_current.Send~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|s_state_send_current~q\,
	datad => \DISPLAY|s_state_current.Send~q\,
	combout => \DISPLAY|P_UEBERGANG:c_pos_y~1_combout\);

-- Location: LCCOMB_X6_Y47_N22
\DISPLAY|Selector1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Selector1~0_combout\ = ((\DISPLAY|s_state_init_current.Finish~q\ & \DISPLAY|s_state_init_next.Func~q\)) # (!\DISPLAY|s_state_init_current.Reset~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_init_current.Finish~q\,
	datac => \DISPLAY|s_state_init_next.Func~q\,
	datad => \DISPLAY|s_state_init_current.Reset~q\,
	combout => \DISPLAY|Selector1~0_combout\);

-- Location: LCCOMB_X6_Y45_N10
\DISPLAY|s_state_next.Send~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_next.Send~0_combout\ = (!\DISPLAY|s_state_init_current.Finish~q\ & \DISPLAY|s_state_current.Init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DISPLAY|s_state_init_current.Finish~q\,
	datad => \DISPLAY|s_state_current.Init~q\,
	combout => \DISPLAY|s_state_next.Send~0_combout\);

-- Location: LCCOMB_X3_Y45_N14
\DISPLAY|P_UEBERGANG:c_pos_y~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:c_pos_y~0_combout\ = (\DISPLAY|P_UEBERGANG:c_pos_x[1]~q\ & (\DISPLAY|P_UEBERGANG:c_pos_x[2]~q\ & (\DISPLAY|P_UEBERGANG:rs~q\ & \DISPLAY|P_UEBERGANG:c_pos_x[0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:c_pos_x[1]~q\,
	datab => \DISPLAY|P_UEBERGANG:c_pos_x[2]~q\,
	datac => \DISPLAY|P_UEBERGANG:rs~q\,
	datad => \DISPLAY|P_UEBERGANG:c_pos_x[0]~q\,
	combout => \DISPLAY|P_UEBERGANG:c_pos_y~0_combout\);

-- Location: LCCOMB_X6_Y45_N12
\DISPLAY|s_state_next~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_next~10_combout\ = (\DISPLAY|s_state_send_current~q\ & ((!\DISPLAY|P_UEBERGANG:c_pos_y~0_combout\) # (!\DISPLAY|P_UEBERGANG:c_pos_x[3]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|s_state_send_current~q\,
	datac => \DISPLAY|P_UEBERGANG:c_pos_x[3]~q\,
	datad => \DISPLAY|P_UEBERGANG:c_pos_y~0_combout\,
	combout => \DISPLAY|s_state_next~10_combout\);

-- Location: LCCOMB_X5_Y45_N26
\DISPLAY|s_state_next.Init~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_next.Init~0_combout\ = (!\DISPLAY|s_state_next.Send~0_combout\ & (\DISPLAY|s_state_init_next.Finish~0_combout\ & ((\DISPLAY|s_state_next~10_combout\) # (!\DISPLAY|s_state_current.Send~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_next.Send~0_combout\,
	datab => \DISPLAY|s_state_current.Send~q\,
	datac => \DISPLAY|s_state_next~10_combout\,
	datad => \DISPLAY|s_state_init_next.Finish~0_combout\,
	combout => \DISPLAY|s_state_next.Init~0_combout\);

-- Location: LCCOMB_X6_Y47_N4
\DISPLAY|s_state_next.Start~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_next.Start~0_combout\ = (!\DISPLAY|s_state_current.Init~q\ & (\nRST~input_o\ & (\DISPLAY|Equal0~4_combout\ & \DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Init~q\,
	datab => \nRST~input_o\,
	datac => \DISPLAY|Equal0~4_combout\,
	datad => \DISPLAY|Equal0~9_combout\,
	combout => \DISPLAY|s_state_next.Start~0_combout\);

-- Location: LCCOMB_X6_Y47_N16
\DISPLAY|s_state_next.Start~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_next.Start~1_combout\ = (\DISPLAY|s_state_init_current.Finish~q\ & ((\DISPLAY|s_state_init_next.Finish~0_combout\))) # (!\DISPLAY|s_state_init_current.Finish~q\ & (\DISPLAY|s_state_next.Start~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_init_current.Finish~q\,
	datac => \DISPLAY|s_state_next.Start~0_combout\,
	datad => \DISPLAY|s_state_init_next.Finish~0_combout\,
	combout => \DISPLAY|s_state_next.Start~1_combout\);

-- Location: LCCOMB_X6_Y47_N6
\DISPLAY|s_state_next.Start~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_next.Start~2_combout\ = (\DISPLAY|s_state_next.Start~q\) # ((\DISPLAY|s_state_next.Start~1_combout\ & ((\DISPLAY|s_state_next~10_combout\) # (!\DISPLAY|s_state_current.Send~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_next~10_combout\,
	datab => \DISPLAY|s_state_current.Send~q\,
	datac => \DISPLAY|s_state_next.Start~q\,
	datad => \DISPLAY|s_state_next.Start~1_combout\,
	combout => \DISPLAY|s_state_next.Start~2_combout\);

-- Location: FF_X6_Y47_N7
\DISPLAY|s_state_next.Start\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|s_state_next.Start~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_next.Start~q\);

-- Location: LCCOMB_X5_Y47_N0
\DISPLAY|s_state_current.Start~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_current.Start~feeder_combout\ = \DISPLAY|s_state_next.Start~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \DISPLAY|s_state_next.Start~q\,
	combout => \DISPLAY|s_state_current.Start~feeder_combout\);

-- Location: FF_X5_Y47_N1
\DISPLAY|s_state_current.Start\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|s_state_current.Start~feeder_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_current.Start~q\);

-- Location: LCCOMB_X6_Y45_N18
\DISPLAY|s_state_next.Init~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_next.Init~1_combout\ = (\DISPLAY|s_state_next.Init~0_combout\ & (!\DISPLAY|s_state_current.Start~q\ & ((\DISPLAY|s_state_init_next.Finish~0_combout\)))) # (!\DISPLAY|s_state_next.Init~0_combout\ & ((\DISPLAY|s_state_next.Init~q\) # 
-- ((!\DISPLAY|s_state_current.Start~q\ & \DISPLAY|s_state_init_next.Finish~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_next.Init~0_combout\,
	datab => \DISPLAY|s_state_current.Start~q\,
	datac => \DISPLAY|s_state_next.Init~q\,
	datad => \DISPLAY|s_state_init_next.Finish~0_combout\,
	combout => \DISPLAY|s_state_next.Init~1_combout\);

-- Location: FF_X6_Y45_N19
\DISPLAY|s_state_next.Init\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|s_state_next.Init~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_next.Init~q\);

-- Location: FF_X6_Y47_N11
\DISPLAY|s_state_current.Init\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|s_state_next.Init~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_current.Init~q\);

-- Location: LCCOMB_X6_Y47_N10
\DISPLAY|s_state_init_next.Finish~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_init_next.Finish~1_combout\ = (\DISPLAY|Equal0~4_combout\ & (\nRST~input_o\ & (\DISPLAY|s_state_current.Init~q\ & \DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Equal0~4_combout\,
	datab => \nRST~input_o\,
	datac => \DISPLAY|s_state_current.Init~q\,
	datad => \DISPLAY|Equal0~9_combout\,
	combout => \DISPLAY|s_state_init_next.Finish~1_combout\);

-- Location: FF_X6_Y47_N23
\DISPLAY|s_state_init_next.Func\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Selector1~0_combout\,
	ena => \DISPLAY|s_state_init_next.Finish~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_init_next.Func~q\);

-- Location: FF_X6_Y47_N25
\DISPLAY|s_state_init_current.Func\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|s_state_init_next.Func~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_init_current.Func~q\);

-- Location: LCCOMB_X6_Y47_N30
\DISPLAY|Selector2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Selector2~0_combout\ = (\DISPLAY|s_state_init_current.Func~q\) # ((\DISPLAY|s_state_init_current.Finish~q\ & \DISPLAY|s_state_init_next.Disp~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_init_current.Finish~q\,
	datac => \DISPLAY|s_state_init_next.Disp~q\,
	datad => \DISPLAY|s_state_init_current.Func~q\,
	combout => \DISPLAY|Selector2~0_combout\);

-- Location: FF_X6_Y47_N31
\DISPLAY|s_state_init_next.Disp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Selector2~0_combout\,
	ena => \DISPLAY|s_state_init_next.Finish~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_init_next.Disp~q\);

-- Location: FF_X6_Y47_N29
\DISPLAY|s_state_init_current.Disp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|s_state_init_next.Disp~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_init_current.Disp~q\);

-- Location: LCCOMB_X6_Y47_N14
\DISPLAY|Selector3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Selector3~0_combout\ = (\DISPLAY|s_state_init_current.Disp~q\) # ((\DISPLAY|s_state_init_current.Finish~q\ & \DISPLAY|s_state_init_next.Entry~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_init_current.Finish~q\,
	datac => \DISPLAY|s_state_init_next.Entry~q\,
	datad => \DISPLAY|s_state_init_current.Disp~q\,
	combout => \DISPLAY|Selector3~0_combout\);

-- Location: FF_X6_Y47_N15
\DISPLAY|s_state_init_next.Entry\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Selector3~0_combout\,
	ena => \DISPLAY|s_state_init_next.Finish~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_init_next.Entry~q\);

-- Location: FF_X6_Y47_N13
\DISPLAY|s_state_init_current.Entry\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|s_state_init_next.Entry~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_init_current.Entry~q\);

-- Location: LCCOMB_X6_Y47_N8
\DISPLAY|Selector4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Selector4~0_combout\ = (\DISPLAY|s_state_init_current.Entry~q\) # ((\DISPLAY|s_state_init_current.Finish~q\ & \DISPLAY|s_state_init_next.Finish~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_init_current.Finish~q\,
	datac => \DISPLAY|s_state_init_next.Finish~q\,
	datad => \DISPLAY|s_state_init_current.Entry~q\,
	combout => \DISPLAY|Selector4~0_combout\);

-- Location: FF_X6_Y47_N9
\DISPLAY|s_state_init_next.Finish\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Selector4~0_combout\,
	ena => \DISPLAY|s_state_init_next.Finish~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_init_next.Finish~q\);

-- Location: FF_X6_Y47_N27
\DISPLAY|s_state_init_current.Finish\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|s_state_init_next.Finish~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_init_current.Finish~q\);

-- Location: LCCOMB_X6_Y45_N8
\DISPLAY|s_state_init_next.Reset~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_init_next.Reset~2_combout\ = (\DISPLAY|s_state_init_next.Reset~q\) # ((!\DISPLAY|s_state_init_current.Finish~q\ & (\DISPLAY|s_state_current.Init~q\ & \DISPLAY|s_state_init_next.Finish~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_init_current.Finish~q\,
	datab => \DISPLAY|s_state_current.Init~q\,
	datac => \DISPLAY|s_state_init_next.Reset~q\,
	datad => \DISPLAY|s_state_init_next.Finish~0_combout\,
	combout => \DISPLAY|s_state_init_next.Reset~2_combout\);

-- Location: FF_X6_Y45_N9
\DISPLAY|s_state_init_next.Reset\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|s_state_init_next.Reset~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_init_next.Reset~q\);

-- Location: FF_X6_Y45_N17
\DISPLAY|s_state_init_current.Reset\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|s_state_init_next.Reset~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_init_current.Reset~q\);

-- Location: LCCOMB_X6_Y45_N16
\DISPLAY|rs~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|rs~3_combout\ = (\DISPLAY|s_state_init_current.Reset~q\ & !\DISPLAY|s_state_current.Send~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DISPLAY|s_state_init_current.Reset~q\,
	datad => \DISPLAY|s_state_current.Send~q\,
	combout => \DISPLAY|rs~3_combout\);

-- Location: LCCOMB_X6_Y45_N22
\DISPLAY|rs~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|rs~2_combout\ = (\DISPLAY|s_state_current.Send~q\ & (((!\DISPLAY|P_UEBERGANG:c_pos_y~0_combout\) # (!\DISPLAY|P_UEBERGANG:c_pos_x[3]~q\)))) # (!\DISPLAY|s_state_current.Send~q\ & (\DISPLAY|s_state_init_current.Finish~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010111011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_init_current.Finish~q\,
	datab => \DISPLAY|s_state_current.Send~q\,
	datac => \DISPLAY|P_UEBERGANG:c_pos_x[3]~q\,
	datad => \DISPLAY|P_UEBERGANG:c_pos_y~0_combout\,
	combout => \DISPLAY|rs~2_combout\);

-- Location: LCCOMB_X5_Y47_N2
\DISPLAY|Equal0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Equal0~10_combout\ = (\DISPLAY|Equal0~9_combout\ & \DISPLAY|Equal0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~9_combout\,
	datad => \DISPLAY|Equal0~4_combout\,
	combout => \DISPLAY|Equal0~10_combout\);

-- Location: LCCOMB_X6_Y45_N2
\DISPLAY|rs~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|rs~5_combout\ = (\DISPLAY|Equal0~10_combout\ & ((\DISPLAY|s_state_current.Send~q\ & ((\DISPLAY|s_state_send_current~q\))) # (!\DISPLAY|s_state_current.Send~q\ & (\DISPLAY|s_state_current.Init~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Init~q\,
	datab => \DISPLAY|s_state_send_current~q\,
	datac => \DISPLAY|s_state_current.Send~q\,
	datad => \DISPLAY|Equal0~10_combout\,
	combout => \DISPLAY|rs~5_combout\);

-- Location: LCCOMB_X6_Y45_N14
\DISPLAY|rs~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|rs~4_combout\ = (\DISPLAY|rs~5_combout\ & ((\DISPLAY|rs~2_combout\) # ((\DISPLAY|P_UEBERGANG:rs~q\ & \DISPLAY|rs~3_combout\)))) # (!\DISPLAY|rs~5_combout\ & (\DISPLAY|P_UEBERGANG:rs~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:rs~q\,
	datab => \DISPLAY|rs~3_combout\,
	datac => \DISPLAY|rs~2_combout\,
	datad => \DISPLAY|rs~5_combout\,
	combout => \DISPLAY|rs~4_combout\);

-- Location: FF_X6_Y45_N5
\DISPLAY|P_UEBERGANG:rs\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|rs~4_combout\,
	sload => VCC,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:rs~q\);

-- Location: LCCOMB_X3_Y45_N12
\DISPLAY|P_UEBERGANG:c_pos_x[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:c_pos_x[0]~0_combout\ = \DISPLAY|P_UEBERGANG:c_pos_x[0]~q\ $ (((\DISPLAY|P_UEBERGANG:c_pos_y~1_combout\ & (\DISPLAY|P_UEBERGANG:rs~q\ & \DISPLAY|Equal0~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:c_pos_y~1_combout\,
	datab => \DISPLAY|P_UEBERGANG:rs~q\,
	datac => \DISPLAY|P_UEBERGANG:c_pos_x[0]~q\,
	datad => \DISPLAY|Equal0~10_combout\,
	combout => \DISPLAY|P_UEBERGANG:c_pos_x[0]~0_combout\);

-- Location: FF_X3_Y45_N13
\DISPLAY|P_UEBERGANG:c_pos_x[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:c_pos_x[0]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:c_pos_x[0]~q\);

-- Location: LCCOMB_X3_Y45_N24
\DISPLAY|P_UEBERGANG:c_pos_x[3]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\ = (\DISPLAY|P_UEBERGANG:c_pos_y~1_combout\ & (\DISPLAY|P_UEBERGANG:rs~q\ & (\DISPLAY|Equal0~4_combout\ & \DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:c_pos_y~1_combout\,
	datab => \DISPLAY|P_UEBERGANG:rs~q\,
	datac => \DISPLAY|Equal0~4_combout\,
	datad => \DISPLAY|Equal0~9_combout\,
	combout => \DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\);

-- Location: LCCOMB_X3_Y45_N30
\DISPLAY|P_UEBERGANG:c_pos_x[1]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:c_pos_x[1]~0_combout\ = \DISPLAY|P_UEBERGANG:c_pos_x[1]~q\ $ (((\DISPLAY|P_UEBERGANG:c_pos_x[0]~q\ & \DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:c_pos_x[0]~q\,
	datac => \DISPLAY|P_UEBERGANG:c_pos_x[1]~q\,
	datad => \DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\,
	combout => \DISPLAY|P_UEBERGANG:c_pos_x[1]~0_combout\);

-- Location: FF_X3_Y45_N31
\DISPLAY|P_UEBERGANG:c_pos_x[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:c_pos_x[1]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:c_pos_x[1]~q\);

-- Location: LCCOMB_X3_Y45_N8
\DISPLAY|P_UEBERGANG:c_pos_x[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:c_pos_x[2]~0_combout\ = \DISPLAY|P_UEBERGANG:c_pos_x[2]~q\ $ (((\DISPLAY|P_UEBERGANG:c_pos_x[1]~q\ & (\DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\ & \DISPLAY|P_UEBERGANG:c_pos_x[0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:c_pos_x[1]~q\,
	datab => \DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\,
	datac => \DISPLAY|P_UEBERGANG:c_pos_x[2]~q\,
	datad => \DISPLAY|P_UEBERGANG:c_pos_x[0]~q\,
	combout => \DISPLAY|P_UEBERGANG:c_pos_x[2]~0_combout\);

-- Location: FF_X3_Y45_N9
\DISPLAY|P_UEBERGANG:c_pos_x[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:c_pos_x[2]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:c_pos_x[2]~q\);

-- Location: LCCOMB_X3_Y45_N22
\DISPLAY|P_UEBERGANG:c_pos_x[3]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\ = (\DISPLAY|P_UEBERGANG:c_pos_x[2]~q\ & (\DISPLAY|P_UEBERGANG:c_pos_x[1]~q\ & \DISPLAY|P_UEBERGANG:c_pos_x[0]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:c_pos_x[2]~q\,
	datac => \DISPLAY|P_UEBERGANG:c_pos_x[1]~q\,
	datad => \DISPLAY|P_UEBERGANG:c_pos_x[0]~q\,
	combout => \DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\);

-- Location: LCCOMB_X2_Y45_N0
\DISPLAY|P_UEBERGANG:c_pos_x[3]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:c_pos_x[3]~2_combout\ = \DISPLAY|P_UEBERGANG:c_pos_x[3]~q\ $ (((\DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\ & \DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:c_pos_x[3]~0_combout\,
	datac => \DISPLAY|P_UEBERGANG:c_pos_x[3]~q\,
	datad => \DISPLAY|P_UEBERGANG:c_pos_x[3]~1_combout\,
	combout => \DISPLAY|P_UEBERGANG:c_pos_x[3]~2_combout\);

-- Location: FF_X2_Y45_N1
\DISPLAY|P_UEBERGANG:c_pos_x[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:c_pos_x[3]~2_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:c_pos_x[3]~q\);

-- Location: LCCOMB_X6_Y47_N12
\DISPLAY|P_UEBERGANG:t_wait[19]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[19]~0_combout\ = (!\DISPLAY|s_state_current.Ready~q\ & ((!\DISPLAY|s_state_current.Init~q\) # (!\DISPLAY|s_state_init_current.Finish~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_init_current.Finish~q\,
	datab => \DISPLAY|s_state_current.Ready~q\,
	datad => \DISPLAY|s_state_current.Init~q\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[19]~0_combout\);

-- Location: LCCOMB_X2_Y45_N2
\DISPLAY|P_UEBERGANG:t_wait[19]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[19]~0_combout\ & (((\DISPLAY|P_UEBERGANG:c_pos_x[3]~q\ & \DISPLAY|P_UEBERGANG:c_pos_y~0_combout\)) # (!\DISPLAY|P_UEBERGANG:c_pos_y~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:c_pos_y~1_combout\,
	datab => \DISPLAY|P_UEBERGANG:c_pos_x[3]~q\,
	datac => \DISPLAY|P_UEBERGANG:t_wait[19]~0_combout\,
	datad => \DISPLAY|P_UEBERGANG:c_pos_y~0_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\);

-- Location: LCCOMB_X6_Y45_N4
\DISPLAY|P_UEBERGANG:t_wait[19]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\ = (!\DISPLAY|s_state_current.Init~q\ & !\DISPLAY|s_state_current.Send~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|s_state_current.Init~q\,
	datad => \DISPLAY|s_state_current.Send~q\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\);

-- Location: LCCOMB_X5_Y46_N6
\DISPLAY|P_UEBERGANG:t_wait[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[0]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (((\DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\ & \DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\)))) # (!\DISPLAY|Equal0~10_combout\ & (\DISPLAY|Add1~24_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Add1~24_combout\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datac => \DISPLAY|Equal0~10_combout\,
	datad => \DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[0]~0_combout\);

-- Location: FF_X5_Y46_N7
\DISPLAY|P_UEBERGANG:t_wait[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[0]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[0]~q\);

-- Location: LCCOMB_X4_Y46_N2
\DISPLAY|Add1~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~26_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[1]~q\ & (\DISPLAY|Add1~25\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[1]~q\ & (!\DISPLAY|Add1~25\))
-- \DISPLAY|Add1~27\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[1]~q\ & !\DISPLAY|Add1~25\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[1]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~25\,
	combout => \DISPLAY|Add1~26_combout\,
	cout => \DISPLAY|Add1~27\);

-- Location: LCCOMB_X5_Y46_N20
\DISPLAY|P_UEBERGANG:t_wait[1]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[1]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (\DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\ & (\DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\))) # (!\DISPLAY|Equal0~10_combout\ & (((\DISPLAY|Add1~26_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Equal0~10_combout\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\,
	datac => \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datad => \DISPLAY|Add1~26_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[1]~0_combout\);

-- Location: FF_X5_Y46_N21
\DISPLAY|P_UEBERGANG:t_wait[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[1]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[1]~q\);

-- Location: LCCOMB_X6_Y47_N18
\DISPLAY|P_UEBERGANG:t_wait[17]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[17]~1_combout\ = (!\DISPLAY|s_state_current.Send~q\ & ((!\DISPLAY|s_state_init_current.Reset~q\) # (!\DISPLAY|s_state_current.Init~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Init~q\,
	datab => \DISPLAY|s_state_current.Send~q\,
	datad => \DISPLAY|s_state_init_current.Reset~q\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[17]~1_combout\);

-- Location: LCCOMB_X6_Y45_N0
\DISPLAY|t_wait~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|t_wait~0_combout\ = (\DISPLAY|s_state_init_current.Reset~q\) # (!\DISPLAY|s_state_current.Init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DISPLAY|s_state_init_current.Reset~q\,
	datad => \DISPLAY|s_state_current.Init~q\,
	combout => \DISPLAY|t_wait~0_combout\);

-- Location: LCCOMB_X4_Y46_N4
\DISPLAY|Add1~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~28_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[2]~q\ & ((GND) # (!\DISPLAY|Add1~27\))) # (!\DISPLAY|P_UEBERGANG:t_wait[2]~q\ & (\DISPLAY|Add1~27\ $ (GND)))
-- \DISPLAY|Add1~29\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[2]~q\) # (!\DISPLAY|Add1~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[2]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~27\,
	combout => \DISPLAY|Add1~28_combout\,
	cout => \DISPLAY|Add1~29\);

-- Location: LCCOMB_X3_Y46_N12
\DISPLAY|P_UEBERGANG:t_wait[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[2]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (((\DISPLAY|t_wait~0_combout\ & \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\)))) # (!\DISPLAY|Equal0~10_combout\ & (\DISPLAY|Add1~28_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Add1~28_combout\,
	datab => \DISPLAY|t_wait~0_combout\,
	datac => \DISPLAY|Equal0~10_combout\,
	datad => \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[2]~0_combout\);

-- Location: FF_X3_Y46_N13
\DISPLAY|P_UEBERGANG:t_wait[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[2]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[2]~q\);

-- Location: LCCOMB_X4_Y46_N6
\DISPLAY|Add1~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~30_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[3]~q\ & (\DISPLAY|Add1~29\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[3]~q\ & (!\DISPLAY|Add1~29\))
-- \DISPLAY|Add1~31\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[3]~q\ & !\DISPLAY|Add1~29\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[3]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~29\,
	combout => \DISPLAY|Add1~30_combout\,
	cout => \DISPLAY|Add1~31\);

-- Location: LCCOMB_X3_Y46_N14
\DISPLAY|Add1~91\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~91_combout\ = (\DISPLAY|Add1~30_combout\ & ((!\DISPLAY|Equal0~9_combout\) # (!\DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~4_combout\,
	datac => \DISPLAY|Equal0~9_combout\,
	datad => \DISPLAY|Add1~30_combout\,
	combout => \DISPLAY|Add1~91_combout\);

-- Location: FF_X3_Y46_N15
\DISPLAY|P_UEBERGANG:t_wait[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~91_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[3]~q\);

-- Location: LCCOMB_X4_Y46_N8
\DISPLAY|Add1~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~32_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[4]~q\ & ((GND) # (!\DISPLAY|Add1~31\))) # (!\DISPLAY|P_UEBERGANG:t_wait[4]~q\ & (\DISPLAY|Add1~31\ $ (GND)))
-- \DISPLAY|Add1~33\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[4]~q\) # (!\DISPLAY|Add1~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[4]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~31\,
	combout => \DISPLAY|Add1~32_combout\,
	cout => \DISPLAY|Add1~33\);

-- Location: LCCOMB_X5_Y46_N30
\DISPLAY|P_UEBERGANG:t_wait[4]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[4]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (!\DISPLAY|t_wait~0_combout\)) # (!\DISPLAY|Equal0~10_combout\ & ((\DISPLAY|Add1~32_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|t_wait~0_combout\,
	datab => \DISPLAY|Add1~32_combout\,
	datad => \DISPLAY|Equal0~10_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[4]~0_combout\);

-- Location: LCCOMB_X5_Y46_N14
\DISPLAY|P_UEBERGANG:t_wait[17]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[17]~2_combout\ = (\DISPLAY|Equal0~9_combout\ & (!\DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\ & \DISPLAY|Equal0~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~9_combout\,
	datac => \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datad => \DISPLAY|Equal0~4_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[17]~2_combout\);

-- Location: FF_X5_Y46_N31
\DISPLAY|P_UEBERGANG:t_wait[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[4]~0_combout\,
	asdata => \DISPLAY|P_UEBERGANG:t_wait[4]~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => \DISPLAY|P_UEBERGANG:t_wait[17]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[4]~q\);

-- Location: LCCOMB_X4_Y46_N10
\DISPLAY|Add1~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~34_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[5]~q\ & (\DISPLAY|Add1~33\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[5]~q\ & (!\DISPLAY|Add1~33\))
-- \DISPLAY|Add1~35\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[5]~q\ & !\DISPLAY|Add1~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[5]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~33\,
	combout => \DISPLAY|Add1~34_combout\,
	cout => \DISPLAY|Add1~35\);

-- Location: LCCOMB_X3_Y46_N4
\DISPLAY|Add1~90\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~90_combout\ = (\DISPLAY|Add1~34_combout\ & ((!\DISPLAY|Equal0~4_combout\) # (!\DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~9_combout\,
	datac => \DISPLAY|Add1~34_combout\,
	datad => \DISPLAY|Equal0~4_combout\,
	combout => \DISPLAY|Add1~90_combout\);

-- Location: FF_X3_Y46_N5
\DISPLAY|P_UEBERGANG:t_wait[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~90_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[5]~q\);

-- Location: LCCOMB_X4_Y46_N12
\DISPLAY|Add1~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~36_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[6]~q\ & ((GND) # (!\DISPLAY|Add1~35\))) # (!\DISPLAY|P_UEBERGANG:t_wait[6]~q\ & (\DISPLAY|Add1~35\ $ (GND)))
-- \DISPLAY|Add1~37\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[6]~q\) # (!\DISPLAY|Add1~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[6]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~35\,
	combout => \DISPLAY|Add1~36_combout\,
	cout => \DISPLAY|Add1~37\);

-- Location: LCCOMB_X3_Y46_N6
\DISPLAY|P_UEBERGANG:t_wait[6]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[6]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (\DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\ & (\DISPLAY|t_wait~0_combout\))) # (!\DISPLAY|Equal0~10_combout\ & (((\DISPLAY|Add1~36_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datab => \DISPLAY|t_wait~0_combout\,
	datac => \DISPLAY|Add1~36_combout\,
	datad => \DISPLAY|Equal0~10_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[6]~0_combout\);

-- Location: FF_X3_Y46_N7
\DISPLAY|P_UEBERGANG:t_wait[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[6]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[6]~q\);

-- Location: LCCOMB_X4_Y46_N14
\DISPLAY|Add1~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~38_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[7]~q\ & (\DISPLAY|Add1~37\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[7]~q\ & (!\DISPLAY|Add1~37\))
-- \DISPLAY|Add1~39\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[7]~q\ & !\DISPLAY|Add1~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[7]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~37\,
	combout => \DISPLAY|Add1~38_combout\,
	cout => \DISPLAY|Add1~39\);

-- Location: LCCOMB_X5_Y46_N24
\DISPLAY|P_UEBERGANG:t_wait[7]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[7]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (!\DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\ & (\DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\))) # (!\DISPLAY|Equal0~10_combout\ & (((\DISPLAY|Add1~38_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Equal0~10_combout\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\,
	datac => \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datad => \DISPLAY|Add1~38_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[7]~0_combout\);

-- Location: FF_X5_Y46_N25
\DISPLAY|P_UEBERGANG:t_wait[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[7]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[7]~q\);

-- Location: LCCOMB_X4_Y46_N16
\DISPLAY|Add1~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~40_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[8]~q\ & ((GND) # (!\DISPLAY|Add1~39\))) # (!\DISPLAY|P_UEBERGANG:t_wait[8]~q\ & (\DISPLAY|Add1~39\ $ (GND)))
-- \DISPLAY|Add1~41\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[8]~q\) # (!\DISPLAY|Add1~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[8]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~39\,
	combout => \DISPLAY|Add1~40_combout\,
	cout => \DISPLAY|Add1~41\);

-- Location: LCCOMB_X5_Y46_N22
\DISPLAY|P_UEBERGANG:t_wait[8]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[8]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (\DISPLAY|t_wait~0_combout\ & (\DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\))) # (!\DISPLAY|Equal0~10_combout\ & (((\DISPLAY|Add1~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|t_wait~0_combout\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datac => \DISPLAY|Equal0~10_combout\,
	datad => \DISPLAY|Add1~40_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[8]~0_combout\);

-- Location: FF_X5_Y46_N23
\DISPLAY|P_UEBERGANG:t_wait[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[8]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[8]~q\);

-- Location: LCCOMB_X4_Y46_N18
\DISPLAY|Add1~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~42_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[9]~q\ & (\DISPLAY|Add1~41\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[9]~q\ & (!\DISPLAY|Add1~41\))
-- \DISPLAY|Add1~43\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[9]~q\ & !\DISPLAY|Add1~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[9]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~41\,
	combout => \DISPLAY|Add1~42_combout\,
	cout => \DISPLAY|Add1~43\);

-- Location: LCCOMB_X5_Y46_N8
\DISPLAY|P_UEBERGANG:t_wait[9]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[9]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (\DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\ & (\DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\))) # (!\DISPLAY|Equal0~10_combout\ & (((\DISPLAY|Add1~42_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Equal0~10_combout\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\,
	datac => \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datad => \DISPLAY|Add1~42_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[9]~0_combout\);

-- Location: FF_X5_Y46_N9
\DISPLAY|P_UEBERGANG:t_wait[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[9]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[9]~q\);

-- Location: LCCOMB_X4_Y46_N20
\DISPLAY|Add1~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~44_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[10]~q\ & ((GND) # (!\DISPLAY|Add1~43\))) # (!\DISPLAY|P_UEBERGANG:t_wait[10]~q\ & (\DISPLAY|Add1~43\ $ (GND)))
-- \DISPLAY|Add1~45\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[10]~q\) # (!\DISPLAY|Add1~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[10]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~43\,
	combout => \DISPLAY|Add1~44_combout\,
	cout => \DISPLAY|Add1~45\);

-- Location: LCCOMB_X3_Y46_N16
\DISPLAY|P_UEBERGANG:t_wait[10]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[10]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (((\DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\ & \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\)))) # (!\DISPLAY|Equal0~10_combout\ & (\DISPLAY|Add1~44_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Add1~44_combout\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\,
	datac => \DISPLAY|Equal0~10_combout\,
	datad => \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[10]~0_combout\);

-- Location: FF_X3_Y46_N17
\DISPLAY|P_UEBERGANG:t_wait[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[10]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[10]~q\);

-- Location: LCCOMB_X4_Y46_N22
\DISPLAY|Add1~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~46_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[11]~q\ & (\DISPLAY|Add1~45\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[11]~q\ & (!\DISPLAY|Add1~45\))
-- \DISPLAY|Add1~47\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[11]~q\ & !\DISPLAY|Add1~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[11]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~45\,
	combout => \DISPLAY|Add1~46_combout\,
	cout => \DISPLAY|Add1~47\);

-- Location: LCCOMB_X5_Y45_N14
\DISPLAY|P_UEBERGANG:t_wait[11]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[11]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (\DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\ & (\DISPLAY|t_wait~0_combout\))) # (!\DISPLAY|Equal0~10_combout\ & (((\DISPLAY|Add1~46_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datab => \DISPLAY|t_wait~0_combout\,
	datac => \DISPLAY|Equal0~10_combout\,
	datad => \DISPLAY|Add1~46_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[11]~0_combout\);

-- Location: FF_X5_Y45_N15
\DISPLAY|P_UEBERGANG:t_wait[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[11]~0_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[11]~q\);

-- Location: LCCOMB_X4_Y46_N24
\DISPLAY|Add1~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~48_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[12]~q\ & ((GND) # (!\DISPLAY|Add1~47\))) # (!\DISPLAY|P_UEBERGANG:t_wait[12]~q\ & (\DISPLAY|Add1~47\ $ (GND)))
-- \DISPLAY|Add1~49\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[12]~q\) # (!\DISPLAY|Add1~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[12]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~47\,
	combout => \DISPLAY|Add1~48_combout\,
	cout => \DISPLAY|Add1~49\);

-- Location: LCCOMB_X5_Y46_N28
\DISPLAY|P_UEBERGANG:t_wait[12]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[12]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (!\DISPLAY|t_wait~0_combout\)) # (!\DISPLAY|Equal0~10_combout\ & ((\DISPLAY|Add1~48_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Equal0~10_combout\,
	datab => \DISPLAY|t_wait~0_combout\,
	datad => \DISPLAY|Add1~48_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[12]~0_combout\);

-- Location: FF_X5_Y46_N29
\DISPLAY|P_UEBERGANG:t_wait[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[12]~0_combout\,
	asdata => \DISPLAY|P_UEBERGANG:t_wait[12]~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => \DISPLAY|P_UEBERGANG:t_wait[17]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[12]~q\);

-- Location: LCCOMB_X4_Y46_N26
\DISPLAY|Add1~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~50_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[13]~q\ & (\DISPLAY|Add1~49\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[13]~q\ & (!\DISPLAY|Add1~49\))
-- \DISPLAY|Add1~51\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[13]~q\ & !\DISPLAY|Add1~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[13]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~49\,
	combout => \DISPLAY|Add1~50_combout\,
	cout => \DISPLAY|Add1~51\);

-- Location: LCCOMB_X5_Y45_N0
\DISPLAY|Add1~89\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~89_combout\ = (\DISPLAY|Add1~50_combout\ & ((!\DISPLAY|Equal0~9_combout\) # (!\DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~4_combout\,
	datac => \DISPLAY|Add1~50_combout\,
	datad => \DISPLAY|Equal0~9_combout\,
	combout => \DISPLAY|Add1~89_combout\);

-- Location: FF_X5_Y45_N1
\DISPLAY|P_UEBERGANG:t_wait[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~89_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[13]~q\);

-- Location: LCCOMB_X4_Y46_N28
\DISPLAY|Add1~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~52_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[14]~q\ & ((GND) # (!\DISPLAY|Add1~51\))) # (!\DISPLAY|P_UEBERGANG:t_wait[14]~q\ & (\DISPLAY|Add1~51\ $ (GND)))
-- \DISPLAY|Add1~53\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[14]~q\) # (!\DISPLAY|Add1~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[14]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~51\,
	combout => \DISPLAY|Add1~52_combout\,
	cout => \DISPLAY|Add1~53\);

-- Location: LCCOMB_X5_Y46_N2
\DISPLAY|P_UEBERGANG:t_wait[14]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[14]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (!\DISPLAY|t_wait~0_combout\)) # (!\DISPLAY|Equal0~10_combout\ & ((\DISPLAY|Add1~52_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Equal0~10_combout\,
	datab => \DISPLAY|t_wait~0_combout\,
	datad => \DISPLAY|Add1~52_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[14]~0_combout\);

-- Location: FF_X5_Y46_N3
\DISPLAY|P_UEBERGANG:t_wait[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[14]~0_combout\,
	asdata => \DISPLAY|P_UEBERGANG:t_wait[14]~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => \DISPLAY|P_UEBERGANG:t_wait[17]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[14]~q\);

-- Location: LCCOMB_X4_Y46_N30
\DISPLAY|Add1~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~54_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[15]~q\ & (\DISPLAY|Add1~53\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[15]~q\ & (!\DISPLAY|Add1~53\))
-- \DISPLAY|Add1~55\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[15]~q\ & !\DISPLAY|Add1~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[15]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~53\,
	combout => \DISPLAY|Add1~54_combout\,
	cout => \DISPLAY|Add1~55\);

-- Location: LCCOMB_X5_Y46_N4
\DISPLAY|P_UEBERGANG:t_wait[15]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[15]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (\DISPLAY|P_UEBERGANG:t_wait[17]~1_combout\)) # (!\DISPLAY|Equal0~10_combout\ & ((\DISPLAY|Add1~54_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Equal0~10_combout\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[17]~1_combout\,
	datad => \DISPLAY|Add1~54_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[15]~0_combout\);

-- Location: FF_X5_Y46_N5
\DISPLAY|P_UEBERGANG:t_wait[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[15]~0_combout\,
	asdata => \DISPLAY|P_UEBERGANG:t_wait[15]~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => \DISPLAY|P_UEBERGANG:t_wait[17]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[15]~q\);

-- Location: LCCOMB_X4_Y45_N0
\DISPLAY|Add1~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~56_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[16]~q\ & ((GND) # (!\DISPLAY|Add1~55\))) # (!\DISPLAY|P_UEBERGANG:t_wait[16]~q\ & (\DISPLAY|Add1~55\ $ (GND)))
-- \DISPLAY|Add1~57\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[16]~q\) # (!\DISPLAY|Add1~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[16]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~55\,
	combout => \DISPLAY|Add1~56_combout\,
	cout => \DISPLAY|Add1~57\);

-- Location: LCCOMB_X5_Y46_N26
\DISPLAY|P_UEBERGANG:t_wait[16]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[16]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (\DISPLAY|P_UEBERGANG:t_wait[17]~1_combout\)) # (!\DISPLAY|Equal0~10_combout\ & ((\DISPLAY|Add1~56_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Equal0~10_combout\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[17]~1_combout\,
	datad => \DISPLAY|Add1~56_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[16]~0_combout\);

-- Location: FF_X5_Y46_N27
\DISPLAY|P_UEBERGANG:t_wait[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[16]~0_combout\,
	asdata => \DISPLAY|P_UEBERGANG:t_wait[16]~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => \DISPLAY|P_UEBERGANG:t_wait[17]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[16]~q\);

-- Location: LCCOMB_X4_Y45_N2
\DISPLAY|Add1~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~58_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[17]~q\ & (\DISPLAY|Add1~57\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[17]~q\ & (!\DISPLAY|Add1~57\))
-- \DISPLAY|Add1~59\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[17]~q\ & !\DISPLAY|Add1~57\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[17]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~57\,
	combout => \DISPLAY|Add1~58_combout\,
	cout => \DISPLAY|Add1~59\);

-- Location: LCCOMB_X5_Y46_N16
\DISPLAY|P_UEBERGANG:t_wait[17]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[17]~0_combout\ = (\DISPLAY|Equal0~10_combout\ & (\DISPLAY|P_UEBERGANG:t_wait[17]~1_combout\)) # (!\DISPLAY|Equal0~10_combout\ & ((\DISPLAY|Add1~58_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Equal0~10_combout\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[17]~1_combout\,
	datad => \DISPLAY|Add1~58_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[17]~0_combout\);

-- Location: FF_X5_Y46_N17
\DISPLAY|P_UEBERGANG:t_wait[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[17]~0_combout\,
	asdata => \DISPLAY|P_UEBERGANG:t_wait[17]~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => \DISPLAY|P_UEBERGANG:t_wait[17]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[17]~q\);

-- Location: LCCOMB_X5_Y46_N12
\DISPLAY|Equal0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Equal0~7_combout\ = (!\DISPLAY|P_UEBERGANG:t_wait[0]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[1]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[16]~q\ & !\DISPLAY|P_UEBERGANG:t_wait[17]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[0]~q\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[1]~q\,
	datac => \DISPLAY|P_UEBERGANG:t_wait[16]~q\,
	datad => \DISPLAY|P_UEBERGANG:t_wait[17]~q\,
	combout => \DISPLAY|Equal0~7_combout\);

-- Location: LCCOMB_X5_Y46_N18
\DISPLAY|Equal0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Equal0~8_combout\ = (!\DISPLAY|P_UEBERGANG:t_wait[4]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[12]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[15]~q\ & !\DISPLAY|P_UEBERGANG:t_wait[14]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[4]~q\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[12]~q\,
	datac => \DISPLAY|P_UEBERGANG:t_wait[15]~q\,
	datad => \DISPLAY|P_UEBERGANG:t_wait[14]~q\,
	combout => \DISPLAY|Equal0~8_combout\);

-- Location: LCCOMB_X3_Y46_N18
\DISPLAY|Equal0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Equal0~6_combout\ = (!\DISPLAY|P_UEBERGANG:t_wait[6]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[5]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[3]~q\ & !\DISPLAY|P_UEBERGANG:t_wait[2]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[6]~q\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[5]~q\,
	datac => \DISPLAY|P_UEBERGANG:t_wait[3]~q\,
	datad => \DISPLAY|P_UEBERGANG:t_wait[2]~q\,
	combout => \DISPLAY|Equal0~6_combout\);

-- Location: LCCOMB_X5_Y46_N10
\DISPLAY|Equal0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Equal0~5_combout\ = (!\DISPLAY|P_UEBERGANG:t_wait[8]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[7]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[9]~q\ & !\DISPLAY|P_UEBERGANG:t_wait[10]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[8]~q\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[7]~q\,
	datac => \DISPLAY|P_UEBERGANG:t_wait[9]~q\,
	datad => \DISPLAY|P_UEBERGANG:t_wait[10]~q\,
	combout => \DISPLAY|Equal0~5_combout\);

-- Location: LCCOMB_X5_Y46_N0
\DISPLAY|Equal0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Equal0~9_combout\ = (\DISPLAY|Equal0~7_combout\ & (\DISPLAY|Equal0~8_combout\ & (\DISPLAY|Equal0~6_combout\ & \DISPLAY|Equal0~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Equal0~7_combout\,
	datab => \DISPLAY|Equal0~8_combout\,
	datac => \DISPLAY|Equal0~6_combout\,
	datad => \DISPLAY|Equal0~5_combout\,
	combout => \DISPLAY|Equal0~9_combout\);

-- Location: LCCOMB_X4_Y45_N4
\DISPLAY|Add1~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~60_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[18]~q\ & ((GND) # (!\DISPLAY|Add1~59\))) # (!\DISPLAY|P_UEBERGANG:t_wait[18]~q\ & (\DISPLAY|Add1~59\ $ (GND)))
-- \DISPLAY|Add1~61\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[18]~q\) # (!\DISPLAY|Add1~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[18]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~59\,
	combout => \DISPLAY|Add1~60_combout\,
	cout => \DISPLAY|Add1~61\);

-- Location: LCCOMB_X5_Y45_N6
\DISPLAY|Add1~88\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~88_combout\ = (\DISPLAY|Add1~60_combout\ & ((!\DISPLAY|Equal0~9_combout\) # (!\DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~4_combout\,
	datac => \DISPLAY|Add1~60_combout\,
	datad => \DISPLAY|Equal0~9_combout\,
	combout => \DISPLAY|Add1~88_combout\);

-- Location: FF_X5_Y45_N7
\DISPLAY|P_UEBERGANG:t_wait[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~88_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[18]~q\);

-- Location: LCCOMB_X4_Y45_N6
\DISPLAY|Add1~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~62_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[19]~q\ & (\DISPLAY|Add1~61\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[19]~q\ & (!\DISPLAY|Add1~61\))
-- \DISPLAY|Add1~63\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[19]~q\ & !\DISPLAY|Add1~61\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[19]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~61\,
	combout => \DISPLAY|Add1~62_combout\,
	cout => \DISPLAY|Add1~63\);

-- Location: LCCOMB_X5_Y45_N12
\DISPLAY|P_UEBERGANG:t_wait[19]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:t_wait[19]~3_combout\ = (\DISPLAY|Equal0~10_combout\ & (\DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\ & ((\DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\)))) # (!\DISPLAY|Equal0~10_combout\ & (((\DISPLAY|Add1~62_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[19]~1_combout\,
	datab => \DISPLAY|Add1~62_combout\,
	datac => \DISPLAY|Equal0~10_combout\,
	datad => \DISPLAY|P_UEBERGANG:t_wait[19]~2_combout\,
	combout => \DISPLAY|P_UEBERGANG:t_wait[19]~3_combout\);

-- Location: FF_X5_Y45_N13
\DISPLAY|P_UEBERGANG:t_wait[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:t_wait[19]~3_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[19]~q\);

-- Location: LCCOMB_X4_Y45_N8
\DISPLAY|Add1~64\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~64_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[20]~q\ & ((GND) # (!\DISPLAY|Add1~63\))) # (!\DISPLAY|P_UEBERGANG:t_wait[20]~q\ & (\DISPLAY|Add1~63\ $ (GND)))
-- \DISPLAY|Add1~65\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[20]~q\) # (!\DISPLAY|Add1~63\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[20]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~63\,
	combout => \DISPLAY|Add1~64_combout\,
	cout => \DISPLAY|Add1~65\);

-- Location: LCCOMB_X5_Y45_N8
\DISPLAY|Add1~103\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~103_combout\ = (\DISPLAY|Add1~64_combout\ & ((!\DISPLAY|Equal0~4_combout\) # (!\DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~9_combout\,
	datac => \DISPLAY|Equal0~4_combout\,
	datad => \DISPLAY|Add1~64_combout\,
	combout => \DISPLAY|Add1~103_combout\);

-- Location: FF_X5_Y45_N9
\DISPLAY|P_UEBERGANG:t_wait[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~103_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[20]~q\);

-- Location: LCCOMB_X4_Y45_N10
\DISPLAY|Add1~66\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~66_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[21]~q\ & (\DISPLAY|Add1~65\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[21]~q\ & (!\DISPLAY|Add1~65\))
-- \DISPLAY|Add1~67\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[21]~q\ & !\DISPLAY|Add1~65\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[21]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~65\,
	combout => \DISPLAY|Add1~66_combout\,
	cout => \DISPLAY|Add1~67\);

-- Location: LCCOMB_X5_Y45_N10
\DISPLAY|Add1~102\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~102_combout\ = (\DISPLAY|Add1~66_combout\ & ((!\DISPLAY|Equal0~4_combout\) # (!\DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~9_combout\,
	datac => \DISPLAY|Equal0~4_combout\,
	datad => \DISPLAY|Add1~66_combout\,
	combout => \DISPLAY|Add1~102_combout\);

-- Location: FF_X5_Y45_N11
\DISPLAY|P_UEBERGANG:t_wait[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~102_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[21]~q\);

-- Location: LCCOMB_X4_Y45_N12
\DISPLAY|Add1~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~68_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[22]~q\ & ((GND) # (!\DISPLAY|Add1~67\))) # (!\DISPLAY|P_UEBERGANG:t_wait[22]~q\ & (\DISPLAY|Add1~67\ $ (GND)))
-- \DISPLAY|Add1~69\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[22]~q\) # (!\DISPLAY|Add1~67\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[22]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~67\,
	combout => \DISPLAY|Add1~68_combout\,
	cout => \DISPLAY|Add1~69\);

-- Location: LCCOMB_X5_Y45_N16
\DISPLAY|Add1~101\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~101_combout\ = (\DISPLAY|Add1~68_combout\ & ((!\DISPLAY|Equal0~4_combout\) # (!\DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~9_combout\,
	datac => \DISPLAY|Equal0~4_combout\,
	datad => \DISPLAY|Add1~68_combout\,
	combout => \DISPLAY|Add1~101_combout\);

-- Location: FF_X5_Y45_N17
\DISPLAY|P_UEBERGANG:t_wait[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~101_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[22]~q\);

-- Location: LCCOMB_X4_Y45_N14
\DISPLAY|Add1~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~70_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[23]~q\ & (\DISPLAY|Add1~69\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[23]~q\ & (!\DISPLAY|Add1~69\))
-- \DISPLAY|Add1~71\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[23]~q\ & !\DISPLAY|Add1~69\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[23]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~69\,
	combout => \DISPLAY|Add1~70_combout\,
	cout => \DISPLAY|Add1~71\);

-- Location: LCCOMB_X5_Y45_N18
\DISPLAY|Add1~100\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~100_combout\ = (\DISPLAY|Add1~70_combout\ & ((!\DISPLAY|Equal0~4_combout\) # (!\DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~9_combout\,
	datac => \DISPLAY|Equal0~4_combout\,
	datad => \DISPLAY|Add1~70_combout\,
	combout => \DISPLAY|Add1~100_combout\);

-- Location: FF_X5_Y45_N19
\DISPLAY|P_UEBERGANG:t_wait[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~100_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[23]~q\);

-- Location: LCCOMB_X5_Y45_N22
\DISPLAY|Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Equal0~2_combout\ = (!\DISPLAY|P_UEBERGANG:t_wait[21]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[23]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[20]~q\ & !\DISPLAY|P_UEBERGANG:t_wait[22]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[21]~q\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[23]~q\,
	datac => \DISPLAY|P_UEBERGANG:t_wait[20]~q\,
	datad => \DISPLAY|P_UEBERGANG:t_wait[22]~q\,
	combout => \DISPLAY|Equal0~2_combout\);

-- Location: LCCOMB_X4_Y45_N16
\DISPLAY|Add1~72\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~72_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[24]~q\ & ((GND) # (!\DISPLAY|Add1~71\))) # (!\DISPLAY|P_UEBERGANG:t_wait[24]~q\ & (\DISPLAY|Add1~71\ $ (GND)))
-- \DISPLAY|Add1~73\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[24]~q\) # (!\DISPLAY|Add1~71\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[24]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~71\,
	combout => \DISPLAY|Add1~72_combout\,
	cout => \DISPLAY|Add1~73\);

-- Location: LCCOMB_X3_Y45_N20
\DISPLAY|Add1~99\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~99_combout\ = (\DISPLAY|Add1~72_combout\ & ((!\DISPLAY|Equal0~4_combout\) # (!\DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~9_combout\,
	datac => \DISPLAY|Equal0~4_combout\,
	datad => \DISPLAY|Add1~72_combout\,
	combout => \DISPLAY|Add1~99_combout\);

-- Location: FF_X3_Y45_N21
\DISPLAY|P_UEBERGANG:t_wait[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~99_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[24]~q\);

-- Location: LCCOMB_X4_Y45_N18
\DISPLAY|Add1~74\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~74_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[25]~q\ & (\DISPLAY|Add1~73\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[25]~q\ & (!\DISPLAY|Add1~73\))
-- \DISPLAY|Add1~75\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[25]~q\ & !\DISPLAY|Add1~73\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[25]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~73\,
	combout => \DISPLAY|Add1~74_combout\,
	cout => \DISPLAY|Add1~75\);

-- Location: LCCOMB_X5_Y45_N30
\DISPLAY|Add1~98\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~98_combout\ = (\DISPLAY|Add1~74_combout\ & ((!\DISPLAY|Equal0~4_combout\) # (!\DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~9_combout\,
	datac => \DISPLAY|Equal0~4_combout\,
	datad => \DISPLAY|Add1~74_combout\,
	combout => \DISPLAY|Add1~98_combout\);

-- Location: FF_X5_Y45_N31
\DISPLAY|P_UEBERGANG:t_wait[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~98_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[25]~q\);

-- Location: LCCOMB_X4_Y45_N20
\DISPLAY|Add1~76\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~76_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[26]~q\ & ((GND) # (!\DISPLAY|Add1~75\))) # (!\DISPLAY|P_UEBERGANG:t_wait[26]~q\ & (\DISPLAY|Add1~75\ $ (GND)))
-- \DISPLAY|Add1~77\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[26]~q\) # (!\DISPLAY|Add1~75\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[26]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~75\,
	combout => \DISPLAY|Add1~76_combout\,
	cout => \DISPLAY|Add1~77\);

-- Location: LCCOMB_X3_Y45_N18
\DISPLAY|Add1~97\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~97_combout\ = (\DISPLAY|Add1~76_combout\ & ((!\DISPLAY|Equal0~9_combout\) # (!\DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~4_combout\,
	datac => \DISPLAY|Add1~76_combout\,
	datad => \DISPLAY|Equal0~9_combout\,
	combout => \DISPLAY|Add1~97_combout\);

-- Location: FF_X3_Y45_N19
\DISPLAY|P_UEBERGANG:t_wait[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~97_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[26]~q\);

-- Location: LCCOMB_X4_Y45_N22
\DISPLAY|Add1~78\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~78_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[27]~q\ & (\DISPLAY|Add1~77\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[27]~q\ & (!\DISPLAY|Add1~77\))
-- \DISPLAY|Add1~79\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[27]~q\ & !\DISPLAY|Add1~77\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|P_UEBERGANG:t_wait[27]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~77\,
	combout => \DISPLAY|Add1~78_combout\,
	cout => \DISPLAY|Add1~79\);

-- Location: LCCOMB_X5_Y45_N28
\DISPLAY|Add1~96\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~96_combout\ = (\DISPLAY|Add1~78_combout\ & ((!\DISPLAY|Equal0~4_combout\) # (!\DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~9_combout\,
	datac => \DISPLAY|Equal0~4_combout\,
	datad => \DISPLAY|Add1~78_combout\,
	combout => \DISPLAY|Add1~96_combout\);

-- Location: FF_X5_Y45_N29
\DISPLAY|P_UEBERGANG:t_wait[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~96_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[27]~q\);

-- Location: LCCOMB_X5_Y45_N24
\DISPLAY|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Equal0~1_combout\ = (!\DISPLAY|P_UEBERGANG:t_wait[25]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[27]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[26]~q\ & !\DISPLAY|P_UEBERGANG:t_wait[24]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[25]~q\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[27]~q\,
	datac => \DISPLAY|P_UEBERGANG:t_wait[26]~q\,
	datad => \DISPLAY|P_UEBERGANG:t_wait[24]~q\,
	combout => \DISPLAY|Equal0~1_combout\);

-- Location: LCCOMB_X5_Y45_N4
\DISPLAY|Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Equal0~3_combout\ = (!\DISPLAY|P_UEBERGANG:t_wait[19]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[13]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[11]~q\ & !\DISPLAY|P_UEBERGANG:t_wait[18]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[19]~q\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[13]~q\,
	datac => \DISPLAY|P_UEBERGANG:t_wait[11]~q\,
	datad => \DISPLAY|P_UEBERGANG:t_wait[18]~q\,
	combout => \DISPLAY|Equal0~3_combout\);

-- Location: LCCOMB_X4_Y45_N24
\DISPLAY|Add1~80\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~80_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[28]~q\ & ((GND) # (!\DISPLAY|Add1~79\))) # (!\DISPLAY|P_UEBERGANG:t_wait[28]~q\ & (\DISPLAY|Add1~79\ $ (GND)))
-- \DISPLAY|Add1~81\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[28]~q\) # (!\DISPLAY|Add1~79\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[28]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~79\,
	combout => \DISPLAY|Add1~80_combout\,
	cout => \DISPLAY|Add1~81\);

-- Location: LCCOMB_X3_Y45_N10
\DISPLAY|Add1~95\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~95_combout\ = (\DISPLAY|Add1~80_combout\ & ((!\DISPLAY|Equal0~4_combout\) # (!\DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~9_combout\,
	datac => \DISPLAY|Equal0~4_combout\,
	datad => \DISPLAY|Add1~80_combout\,
	combout => \DISPLAY|Add1~95_combout\);

-- Location: FF_X3_Y45_N11
\DISPLAY|P_UEBERGANG:t_wait[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~95_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[28]~q\);

-- Location: LCCOMB_X4_Y45_N26
\DISPLAY|Add1~82\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~82_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[29]~q\ & (\DISPLAY|Add1~81\ & VCC)) # (!\DISPLAY|P_UEBERGANG:t_wait[29]~q\ & (!\DISPLAY|Add1~81\))
-- \DISPLAY|Add1~83\ = CARRY((!\DISPLAY|P_UEBERGANG:t_wait[29]~q\ & !\DISPLAY|Add1~81\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[29]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~81\,
	combout => \DISPLAY|Add1~82_combout\,
	cout => \DISPLAY|Add1~83\);

-- Location: LCCOMB_X3_Y45_N16
\DISPLAY|Add1~94\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~94_combout\ = (\DISPLAY|Add1~82_combout\ & ((!\DISPLAY|Equal0~4_combout\) # (!\DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~9_combout\,
	datac => \DISPLAY|Equal0~4_combout\,
	datad => \DISPLAY|Add1~82_combout\,
	combout => \DISPLAY|Add1~94_combout\);

-- Location: FF_X3_Y45_N17
\DISPLAY|P_UEBERGANG:t_wait[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~94_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[29]~q\);

-- Location: LCCOMB_X4_Y45_N28
\DISPLAY|Add1~84\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~84_combout\ = (\DISPLAY|P_UEBERGANG:t_wait[30]~q\ & ((GND) # (!\DISPLAY|Add1~83\))) # (!\DISPLAY|P_UEBERGANG:t_wait[30]~q\ & (\DISPLAY|Add1~83\ $ (GND)))
-- \DISPLAY|Add1~85\ = CARRY((\DISPLAY|P_UEBERGANG:t_wait[30]~q\) # (!\DISPLAY|Add1~83\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[30]~q\,
	datad => VCC,
	cin => \DISPLAY|Add1~83\,
	combout => \DISPLAY|Add1~84_combout\,
	cout => \DISPLAY|Add1~85\);

-- Location: LCCOMB_X3_Y45_N26
\DISPLAY|Add1~93\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~93_combout\ = (\DISPLAY|Add1~84_combout\ & ((!\DISPLAY|Equal0~4_combout\) # (!\DISPLAY|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~9_combout\,
	datac => \DISPLAY|Equal0~4_combout\,
	datad => \DISPLAY|Add1~84_combout\,
	combout => \DISPLAY|Add1~93_combout\);

-- Location: FF_X3_Y45_N27
\DISPLAY|P_UEBERGANG:t_wait[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~93_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[30]~q\);

-- Location: LCCOMB_X4_Y45_N30
\DISPLAY|Add1~86\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~86_combout\ = \DISPLAY|Add1~85\ $ (!\DISPLAY|P_UEBERGANG:t_wait[31]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \DISPLAY|P_UEBERGANG:t_wait[31]~q\,
	cin => \DISPLAY|Add1~85\,
	combout => \DISPLAY|Add1~86_combout\);

-- Location: LCCOMB_X3_Y45_N28
\DISPLAY|Add1~92\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Add1~92_combout\ = (\DISPLAY|Add1~86_combout\ & ((!\DISPLAY|Equal0~9_combout\) # (!\DISPLAY|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|Equal0~4_combout\,
	datac => \DISPLAY|Add1~86_combout\,
	datad => \DISPLAY|Equal0~9_combout\,
	combout => \DISPLAY|Add1~92_combout\);

-- Location: FF_X3_Y45_N29
\DISPLAY|P_UEBERGANG:t_wait[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|Add1~92_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:t_wait[31]~q\);

-- Location: LCCOMB_X3_Y45_N0
\DISPLAY|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Equal0~0_combout\ = (!\DISPLAY|P_UEBERGANG:t_wait[28]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[29]~q\ & (!\DISPLAY|P_UEBERGANG:t_wait[30]~q\ & !\DISPLAY|P_UEBERGANG:t_wait[31]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:t_wait[28]~q\,
	datab => \DISPLAY|P_UEBERGANG:t_wait[29]~q\,
	datac => \DISPLAY|P_UEBERGANG:t_wait[30]~q\,
	datad => \DISPLAY|P_UEBERGANG:t_wait[31]~q\,
	combout => \DISPLAY|Equal0~0_combout\);

-- Location: LCCOMB_X5_Y45_N2
\DISPLAY|Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|Equal0~4_combout\ = (\DISPLAY|Equal0~2_combout\ & (\DISPLAY|Equal0~1_combout\ & (\DISPLAY|Equal0~3_combout\ & \DISPLAY|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Equal0~2_combout\,
	datab => \DISPLAY|Equal0~1_combout\,
	datac => \DISPLAY|Equal0~3_combout\,
	datad => \DISPLAY|Equal0~0_combout\,
	combout => \DISPLAY|Equal0~4_combout\);

-- Location: LCCOMB_X5_Y45_N20
\DISPLAY|s_state_init_next.Finish~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_init_next.Finish~0_combout\ = (\nRST~input_o\ & (\DISPLAY|Equal0~4_combout\ & \DISPLAY|Equal0~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nRST~input_o\,
	datab => \DISPLAY|Equal0~4_combout\,
	datad => \DISPLAY|Equal0~9_combout\,
	combout => \DISPLAY|s_state_init_next.Finish~0_combout\);

-- Location: LCCOMB_X6_Y47_N2
\DISPLAY|s_busy~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_busy~3_combout\ = (\DISPLAY|s_state_current.Ready~q\ & ((\DISPLAY|s_state_init_next.Finish~0_combout\ & (!\s_en~q\)) # (!\DISPLAY|s_state_init_next.Finish~0_combout\ & ((\DISPLAY|s_busy~q\))))) # (!\DISPLAY|s_state_current.Ready~q\ & 
-- (((\DISPLAY|s_busy~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Ready~q\,
	datab => \s_en~q\,
	datac => \DISPLAY|s_busy~q\,
	datad => \DISPLAY|s_state_init_next.Finish~0_combout\,
	combout => \DISPLAY|s_busy~3_combout\);

-- Location: FF_X6_Y47_N3
\DISPLAY|s_busy\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|s_busy~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_busy~q\);

-- Location: LCCOMB_X6_Y50_N2
\ci[3]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \ci[3]~0_combout\ = (\DISPLAY|s_busy~q\ & (\nRST~input_o\ & (\Equal0~9_combout\ & !\s_en~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_busy~q\,
	datab => \nRST~input_o\,
	datac => \Equal0~9_combout\,
	datad => \s_en~q\,
	combout => \ci[3]~0_combout\);

-- Location: FF_X6_Y50_N15
\ci[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \ci[0]~1_combout\,
	ena => \ci[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => ci(0));

-- Location: LCCOMB_X6_Y50_N24
\Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~0_combout\ = ci(0) $ (ci(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => ci(0),
	datac => ci(1),
	combout => \Add0~0_combout\);

-- Location: FF_X6_Y50_N25
\ci[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~0_combout\,
	ena => \ci[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => ci(1));

-- Location: LCCOMB_X6_Y50_N10
\Add0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~1_combout\ = ci(2) $ (((ci(0) & ci(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => ci(0),
	datac => ci(2),
	datad => ci(1),
	combout => \Add0~1_combout\);

-- Location: FF_X6_Y50_N11
\ci[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~1_combout\,
	ena => \ci[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => ci(2));

-- Location: LCCOMB_X6_Y50_N16
\Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~2_combout\ = ci(3) $ (((ci(2) & (ci(0) & ci(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(2),
	datab => ci(0),
	datac => ci(3),
	datad => ci(1),
	combout => \Add0~2_combout\);

-- Location: FF_X6_Y50_N17
\ci[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add0~2_combout\,
	ena => \ci[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => ci(3));

-- Location: LCCOMB_X6_Y50_N12
\t_wait[30]~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[30]~36_combout\ = (ci(2) & (ci(3) & (ci(1) $ (ci(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(2),
	datab => ci(1),
	datac => ci(0),
	datad => ci(3),
	combout => \t_wait[30]~36_combout\);

-- Location: LCCOMB_X6_Y50_N22
\t_wait[30]~37\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[30]~37_combout\ = (!\s_en~q\ & (!ci(0) & \DISPLAY|s_busy~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_en~q\,
	datac => ci(0),
	datad => \DISPLAY|s_busy~q\,
	combout => \t_wait[30]~37_combout\);

-- Location: LCCOMB_X7_Y50_N20
\t_wait[30]~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[30]~38_combout\ = (\nRST~input_o\ & (((\t_wait[30]~36_combout\ & \t_wait[30]~37_combout\)) # (!\Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \t_wait[30]~36_combout\,
	datab => \nRST~input_o\,
	datac => \Equal0~9_combout\,
	datad => \t_wait[30]~37_combout\,
	combout => \t_wait[30]~38_combout\);

-- Location: FF_X8_Y50_N9
\t_wait[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add2~6_combout\,
	ena => \t_wait[30]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(3));

-- Location: LCCOMB_X8_Y50_N10
\Add2~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~8_combout\ = (t_wait(4) & ((GND) # (!\Add2~7\))) # (!t_wait(4) & (\Add2~7\ $ (GND)))
-- \Add2~9\ = CARRY((t_wait(4)) # (!\Add2~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(4),
	datad => VCC,
	cin => \Add2~7\,
	combout => \Add2~8_combout\,
	cout => \Add2~9\);

-- Location: FF_X8_Y50_N11
\t_wait[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add2~8_combout\,
	ena => \t_wait[30]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(4));

-- Location: LCCOMB_X8_Y50_N12
\Add2~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~10_combout\ = (t_wait(5) & (\Add2~9\ & VCC)) # (!t_wait(5) & (!\Add2~9\))
-- \Add2~11\ = CARRY((!t_wait(5) & !\Add2~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(5),
	datad => VCC,
	cin => \Add2~9\,
	combout => \Add2~10_combout\,
	cout => \Add2~11\);

-- Location: LCCOMB_X7_Y50_N14
\t_wait[5]~53\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[5]~53_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~10_combout\)))) # (!\nRST~input_o\ & (((t_wait(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(5),
	datad => \Add2~10_combout\,
	combout => \t_wait[5]~53_combout\);

-- Location: FF_X7_Y50_N15
\t_wait[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[5]~53_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(5));

-- Location: LCCOMB_X8_Y50_N14
\Add2~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~12_combout\ = (t_wait(6) & ((GND) # (!\Add2~11\))) # (!t_wait(6) & (\Add2~11\ $ (GND)))
-- \Add2~13\ = CARRY((t_wait(6)) # (!\Add2~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(6),
	datad => VCC,
	cin => \Add2~11\,
	combout => \Add2~12_combout\,
	cout => \Add2~13\);

-- Location: FF_X8_Y50_N15
\t_wait[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add2~12_combout\,
	ena => \t_wait[30]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(6));

-- Location: LCCOMB_X8_Y50_N16
\Add2~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~14_combout\ = (t_wait(7) & (\Add2~13\ & VCC)) # (!t_wait(7) & (!\Add2~13\))
-- \Add2~15\ = CARRY((!t_wait(7) & !\Add2~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(7),
	datad => VCC,
	cin => \Add2~13\,
	combout => \Add2~14_combout\,
	cout => \Add2~15\);

-- Location: FF_X8_Y50_N17
\t_wait[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add2~14_combout\,
	ena => \t_wait[30]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(7));

-- Location: LCCOMB_X8_Y50_N18
\Add2~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~16_combout\ = (t_wait(8) & ((GND) # (!\Add2~15\))) # (!t_wait(8) & (\Add2~15\ $ (GND)))
-- \Add2~17\ = CARRY((t_wait(8)) # (!\Add2~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(8),
	datad => VCC,
	cin => \Add2~15\,
	combout => \Add2~16_combout\,
	cout => \Add2~17\);

-- Location: LCCOMB_X7_Y50_N18
\t_wait[8]~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[8]~52_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~16_combout\)))) # (!\nRST~input_o\ & (((t_wait(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(8),
	datad => \Add2~16_combout\,
	combout => \t_wait[8]~52_combout\);

-- Location: FF_X7_Y50_N19
\t_wait[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[8]~52_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(8));

-- Location: LCCOMB_X8_Y50_N20
\Add2~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~18_combout\ = (t_wait(9) & (\Add2~17\ & VCC)) # (!t_wait(9) & (!\Add2~17\))
-- \Add2~19\ = CARRY((!t_wait(9) & !\Add2~17\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(9),
	datad => VCC,
	cin => \Add2~17\,
	combout => \Add2~18_combout\,
	cout => \Add2~19\);

-- Location: LCCOMB_X7_Y50_N12
\t_wait[9]~51\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[9]~51_combout\ = (\nRST~input_o\ & (\Add2~18_combout\ & ((!\Equal0~9_combout\)))) # (!\nRST~input_o\ & (((t_wait(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Add2~18_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(9),
	datad => \Equal0~9_combout\,
	combout => \t_wait[9]~51_combout\);

-- Location: FF_X7_Y50_N13
\t_wait[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[9]~51_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(9));

-- Location: LCCOMB_X8_Y50_N22
\Add2~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~20_combout\ = (t_wait(10) & ((GND) # (!\Add2~19\))) # (!t_wait(10) & (\Add2~19\ $ (GND)))
-- \Add2~21\ = CARRY((t_wait(10)) # (!\Add2~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(10),
	datad => VCC,
	cin => \Add2~19\,
	combout => \Add2~20_combout\,
	cout => \Add2~21\);

-- Location: LCCOMB_X7_Y50_N22
\t_wait[10]~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[10]~50_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~20_combout\)))) # (!\nRST~input_o\ & (((t_wait(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(10),
	datad => \Add2~20_combout\,
	combout => \t_wait[10]~50_combout\);

-- Location: FF_X7_Y50_N23
\t_wait[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[10]~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(10));

-- Location: LCCOMB_X7_Y50_N16
\Equal0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~6_combout\ = (!t_wait(10) & (!t_wait(8) & (!t_wait(7) & !t_wait(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(10),
	datab => t_wait(8),
	datac => t_wait(7),
	datad => t_wait(9),
	combout => \Equal0~6_combout\);

-- Location: LCCOMB_X7_Y50_N28
\Equal0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~7_combout\ = (!t_wait(4) & (!t_wait(5) & (!t_wait(3) & !t_wait(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(4),
	datab => t_wait(5),
	datac => t_wait(3),
	datad => t_wait(6),
	combout => \Equal0~7_combout\);

-- Location: LCCOMB_X7_Y50_N24
\Equal0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~8_combout\ = (!t_wait(0) & (!t_wait(2) & (!t_wait(1) & \Equal0~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(0),
	datab => t_wait(2),
	datac => t_wait(1),
	datad => \Equal0~7_combout\,
	combout => \Equal0~8_combout\);

-- Location: LCCOMB_X8_Y50_N24
\Add2~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~22_combout\ = (t_wait(11) & (\Add2~21\ & VCC)) # (!t_wait(11) & (!\Add2~21\))
-- \Add2~23\ = CARRY((!t_wait(11) & !\Add2~21\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(11),
	datad => VCC,
	cin => \Add2~21\,
	combout => \Add2~22_combout\,
	cout => \Add2~23\);

-- Location: LCCOMB_X7_Y50_N10
\t_wait[11]~49\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[11]~49_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~22_combout\)))) # (!\nRST~input_o\ & (((t_wait(11)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(11),
	datad => \Add2~22_combout\,
	combout => \t_wait[11]~49_combout\);

-- Location: FF_X7_Y50_N11
\t_wait[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[11]~49_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(11));

-- Location: LCCOMB_X8_Y50_N26
\Add2~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~24_combout\ = (t_wait(12) & ((GND) # (!\Add2~23\))) # (!t_wait(12) & (\Add2~23\ $ (GND)))
-- \Add2~25\ = CARRY((t_wait(12)) # (!\Add2~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(12),
	datad => VCC,
	cin => \Add2~23\,
	combout => \Add2~24_combout\,
	cout => \Add2~25\);

-- Location: FF_X8_Y50_N27
\t_wait[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add2~24_combout\,
	ena => \t_wait[30]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(12));

-- Location: LCCOMB_X8_Y50_N28
\Add2~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~26_combout\ = (t_wait(13) & (\Add2~25\ & VCC)) # (!t_wait(13) & (!\Add2~25\))
-- \Add2~27\ = CARRY((!t_wait(13) & !\Add2~25\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(13),
	datad => VCC,
	cin => \Add2~25\,
	combout => \Add2~26_combout\,
	cout => \Add2~27\);

-- Location: FF_X8_Y50_N29
\t_wait[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add2~26_combout\,
	ena => \t_wait[30]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(13));

-- Location: LCCOMB_X8_Y50_N30
\Add2~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~28_combout\ = (t_wait(14) & ((GND) # (!\Add2~27\))) # (!t_wait(14) & (\Add2~27\ $ (GND)))
-- \Add2~29\ = CARRY((t_wait(14)) # (!\Add2~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(14),
	datad => VCC,
	cin => \Add2~27\,
	combout => \Add2~28_combout\,
	cout => \Add2~29\);

-- Location: LCCOMB_X7_Y50_N8
\t_wait[14]~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[14]~48_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~28_combout\)))) # (!\nRST~input_o\ & (((t_wait(14)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(14),
	datad => \Add2~28_combout\,
	combout => \t_wait[14]~48_combout\);

-- Location: FF_X7_Y50_N9
\t_wait[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[14]~48_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(14));

-- Location: LCCOMB_X7_Y50_N4
\Equal0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~5_combout\ = (!t_wait(11) & (!t_wait(14) & (!t_wait(12) & !t_wait(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(11),
	datab => t_wait(14),
	datac => t_wait(12),
	datad => t_wait(13),
	combout => \Equal0~5_combout\);

-- Location: LCCOMB_X8_Y49_N0
\Add2~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~30_combout\ = (t_wait(15) & (\Add2~29\ & VCC)) # (!t_wait(15) & (!\Add2~29\))
-- \Add2~31\ = CARRY((!t_wait(15) & !\Add2~29\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(15),
	datad => VCC,
	cin => \Add2~29\,
	combout => \Add2~30_combout\,
	cout => \Add2~31\);

-- Location: FF_X8_Y49_N1
\t_wait[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add2~30_combout\,
	ena => \t_wait[30]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(15));

-- Location: LCCOMB_X8_Y49_N2
\Add2~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~32_combout\ = (t_wait(16) & ((GND) # (!\Add2~31\))) # (!t_wait(16) & (\Add2~31\ $ (GND)))
-- \Add2~33\ = CARRY((t_wait(16)) # (!\Add2~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(16),
	datad => VCC,
	cin => \Add2~31\,
	combout => \Add2~32_combout\,
	cout => \Add2~33\);

-- Location: LCCOMB_X7_Y49_N14
\t_wait[16]~47\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[16]~47_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~32_combout\)))) # (!\nRST~input_o\ & (((t_wait(16)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(16),
	datad => \Add2~32_combout\,
	combout => \t_wait[16]~47_combout\);

-- Location: FF_X7_Y49_N15
\t_wait[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[16]~47_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(16));

-- Location: LCCOMB_X8_Y49_N4
\Add2~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~34_combout\ = (t_wait(17) & (\Add2~33\ & VCC)) # (!t_wait(17) & (!\Add2~33\))
-- \Add2~35\ = CARRY((!t_wait(17) & !\Add2~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(17),
	datad => VCC,
	cin => \Add2~33\,
	combout => \Add2~34_combout\,
	cout => \Add2~35\);

-- Location: LCCOMB_X7_Y49_N24
\t_wait[17]~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[17]~46_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~34_combout\)))) # (!\nRST~input_o\ & (((t_wait(17)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(17),
	datad => \Add2~34_combout\,
	combout => \t_wait[17]~46_combout\);

-- Location: FF_X7_Y49_N25
\t_wait[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[17]~46_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(17));

-- Location: LCCOMB_X8_Y49_N6
\Add2~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~36_combout\ = (t_wait(18) & ((GND) # (!\Add2~35\))) # (!t_wait(18) & (\Add2~35\ $ (GND)))
-- \Add2~37\ = CARRY((t_wait(18)) # (!\Add2~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(18),
	datad => VCC,
	cin => \Add2~35\,
	combout => \Add2~36_combout\,
	cout => \Add2~37\);

-- Location: FF_X8_Y49_N7
\t_wait[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add2~36_combout\,
	ena => \t_wait[30]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(18));

-- Location: LCCOMB_X7_Y49_N12
\Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~3_combout\ = (!t_wait(17) & (!t_wait(16) & (!t_wait(15) & !t_wait(18))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(17),
	datab => t_wait(16),
	datac => t_wait(15),
	datad => t_wait(18),
	combout => \Equal0~3_combout\);

-- Location: LCCOMB_X8_Y49_N8
\Add2~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~38_combout\ = (t_wait(19) & (\Add2~37\ & VCC)) # (!t_wait(19) & (!\Add2~37\))
-- \Add2~39\ = CARRY((!t_wait(19) & !\Add2~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(19),
	datad => VCC,
	cin => \Add2~37\,
	combout => \Add2~38_combout\,
	cout => \Add2~39\);

-- Location: FF_X8_Y49_N9
\t_wait[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add2~38_combout\,
	ena => \t_wait[30]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(19));

-- Location: LCCOMB_X8_Y49_N10
\Add2~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~40_combout\ = (t_wait(20) & ((GND) # (!\Add2~39\))) # (!t_wait(20) & (\Add2~39\ $ (GND)))
-- \Add2~41\ = CARRY((t_wait(20)) # (!\Add2~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(20),
	datad => VCC,
	cin => \Add2~39\,
	combout => \Add2~40_combout\,
	cout => \Add2~41\);

-- Location: FF_X8_Y49_N11
\t_wait[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add2~40_combout\,
	ena => \t_wait[30]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(20));

-- Location: LCCOMB_X8_Y49_N12
\Add2~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~42_combout\ = (t_wait(21) & (\Add2~41\ & VCC)) # (!t_wait(21) & (!\Add2~41\))
-- \Add2~43\ = CARRY((!t_wait(21) & !\Add2~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(21),
	datad => VCC,
	cin => \Add2~41\,
	combout => \Add2~42_combout\,
	cout => \Add2~43\);

-- Location: LCCOMB_X7_Y49_N28
\t_wait[21]~45\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[21]~45_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~42_combout\)))) # (!\nRST~input_o\ & (((t_wait(21)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(21),
	datad => \Add2~42_combout\,
	combout => \t_wait[21]~45_combout\);

-- Location: FF_X7_Y49_N29
\t_wait[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[21]~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(21));

-- Location: LCCOMB_X8_Y49_N14
\Add2~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~44_combout\ = (t_wait(22) & ((GND) # (!\Add2~43\))) # (!t_wait(22) & (\Add2~43\ $ (GND)))
-- \Add2~45\ = CARRY((t_wait(22)) # (!\Add2~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(22),
	datad => VCC,
	cin => \Add2~43\,
	combout => \Add2~44_combout\,
	cout => \Add2~45\);

-- Location: FF_X8_Y49_N15
\t_wait[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add2~44_combout\,
	ena => \t_wait[30]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(22));

-- Location: LCCOMB_X8_Y49_N16
\Add2~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~46_combout\ = (t_wait(23) & (\Add2~45\ & VCC)) # (!t_wait(23) & (!\Add2~45\))
-- \Add2~47\ = CARRY((!t_wait(23) & !\Add2~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(23),
	datad => VCC,
	cin => \Add2~45\,
	combout => \Add2~46_combout\,
	cout => \Add2~47\);

-- Location: FF_X8_Y49_N17
\t_wait[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add2~46_combout\,
	ena => \t_wait[30]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(23));

-- Location: LCCOMB_X8_Y49_N18
\Add2~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~48_combout\ = (t_wait(24) & ((GND) # (!\Add2~47\))) # (!t_wait(24) & (\Add2~47\ $ (GND)))
-- \Add2~49\ = CARRY((t_wait(24)) # (!\Add2~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(24),
	datad => VCC,
	cin => \Add2~47\,
	combout => \Add2~48_combout\,
	cout => \Add2~49\);

-- Location: LCCOMB_X7_Y49_N20
\t_wait[24]~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[24]~44_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~48_combout\)))) # (!\nRST~input_o\ & (((t_wait(24)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(24),
	datad => \Add2~48_combout\,
	combout => \t_wait[24]~44_combout\);

-- Location: FF_X7_Y49_N21
\t_wait[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[24]~44_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(24));

-- Location: LCCOMB_X8_Y49_N20
\Add2~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~50_combout\ = (t_wait(25) & (\Add2~49\ & VCC)) # (!t_wait(25) & (!\Add2~49\))
-- \Add2~51\ = CARRY((!t_wait(25) & !\Add2~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(25),
	datad => VCC,
	cin => \Add2~49\,
	combout => \Add2~50_combout\,
	cout => \Add2~51\);

-- Location: FF_X8_Y49_N21
\t_wait[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Add2~50_combout\,
	ena => \t_wait[30]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(25));

-- Location: LCCOMB_X8_Y49_N22
\Add2~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~52_combout\ = (t_wait(26) & ((GND) # (!\Add2~51\))) # (!t_wait(26) & (\Add2~51\ $ (GND)))
-- \Add2~53\ = CARRY((t_wait(26)) # (!\Add2~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => t_wait(26),
	datad => VCC,
	cin => \Add2~51\,
	combout => \Add2~52_combout\,
	cout => \Add2~53\);

-- Location: LCCOMB_X7_Y49_N30
\t_wait[26]~43\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[26]~43_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~52_combout\)))) # (!\nRST~input_o\ & (((t_wait(26)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(26),
	datad => \Add2~52_combout\,
	combout => \t_wait[26]~43_combout\);

-- Location: FF_X7_Y49_N31
\t_wait[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[26]~43_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(26));

-- Location: LCCOMB_X8_Y49_N24
\Add2~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~54_combout\ = (t_wait(27) & (\Add2~53\ & VCC)) # (!t_wait(27) & (!\Add2~53\))
-- \Add2~55\ = CARRY((!t_wait(27) & !\Add2~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(27),
	datad => VCC,
	cin => \Add2~53\,
	combout => \Add2~54_combout\,
	cout => \Add2~55\);

-- Location: LCCOMB_X7_Y49_N2
\t_wait[27]~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[27]~42_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~54_combout\)))) # (!\nRST~input_o\ & (((t_wait(27)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(27),
	datad => \Add2~54_combout\,
	combout => \t_wait[27]~42_combout\);

-- Location: FF_X7_Y49_N3
\t_wait[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[27]~42_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(27));

-- Location: LCCOMB_X8_Y49_N26
\Add2~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~56_combout\ = (t_wait(28) & ((GND) # (!\Add2~55\))) # (!t_wait(28) & (\Add2~55\ $ (GND)))
-- \Add2~57\ = CARRY((t_wait(28)) # (!\Add2~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(28),
	datad => VCC,
	cin => \Add2~55\,
	combout => \Add2~56_combout\,
	cout => \Add2~57\);

-- Location: LCCOMB_X7_Y49_N8
\t_wait[28]~41\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[28]~41_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~56_combout\)))) # (!\nRST~input_o\ & (((t_wait(28)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(28),
	datad => \Add2~56_combout\,
	combout => \t_wait[28]~41_combout\);

-- Location: FF_X7_Y49_N9
\t_wait[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[28]~41_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(28));

-- Location: LCCOMB_X8_Y49_N28
\Add2~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~58_combout\ = (t_wait(29) & (\Add2~57\ & VCC)) # (!t_wait(29) & (!\Add2~57\))
-- \Add2~59\ = CARRY((!t_wait(29) & !\Add2~57\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(29),
	datad => VCC,
	cin => \Add2~57\,
	combout => \Add2~58_combout\,
	cout => \Add2~59\);

-- Location: LCCOMB_X7_Y49_N26
\t_wait[29]~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[29]~40_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~58_combout\)))) # (!\nRST~input_o\ & (((t_wait(29)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(29),
	datad => \Add2~58_combout\,
	combout => \t_wait[29]~40_combout\);

-- Location: FF_X7_Y49_N27
\t_wait[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[29]~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(29));

-- Location: LCCOMB_X8_Y49_N30
\Add2~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add2~60_combout\ = \Add2~59\ $ (t_wait(30))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => t_wait(30),
	cin => \Add2~59\,
	combout => \Add2~60_combout\);

-- Location: LCCOMB_X7_Y49_N16
\t_wait[30]~39\ : cycloneive_lcell_comb
-- Equation(s):
-- \t_wait[30]~39_combout\ = (\nRST~input_o\ & (!\Equal0~9_combout\ & ((\Add2~60_combout\)))) # (!\nRST~input_o\ & (((t_wait(30)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => t_wait(30),
	datad => \Add2~60_combout\,
	combout => \t_wait[30]~39_combout\);

-- Location: FF_X7_Y49_N17
\t_wait[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \t_wait[30]~39_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => t_wait(30));

-- Location: LCCOMB_X7_Y49_N4
\Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = (!t_wait(29) & (!t_wait(27) & (!t_wait(28) & !t_wait(30))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(29),
	datab => t_wait(27),
	datac => t_wait(28),
	datad => t_wait(30),
	combout => \Equal0~0_combout\);

-- Location: LCCOMB_X7_Y49_N22
\Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = (!t_wait(26) & (!t_wait(24) & (!t_wait(25) & !t_wait(23))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(26),
	datab => t_wait(24),
	datac => t_wait(25),
	datad => t_wait(23),
	combout => \Equal0~1_combout\);

-- Location: LCCOMB_X7_Y49_N10
\Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = (!t_wait(22) & (!t_wait(19) & (!t_wait(20) & !t_wait(21))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => t_wait(22),
	datab => t_wait(19),
	datac => t_wait(20),
	datad => t_wait(21),
	combout => \Equal0~2_combout\);

-- Location: LCCOMB_X7_Y49_N18
\Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~4_combout\ = (\Equal0~3_combout\ & (\Equal0~0_combout\ & (\Equal0~1_combout\ & \Equal0~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~3_combout\,
	datab => \Equal0~0_combout\,
	datac => \Equal0~1_combout\,
	datad => \Equal0~2_combout\,
	combout => \Equal0~4_combout\);

-- Location: LCCOMB_X7_Y50_N30
\Equal0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~9_combout\ = (\Equal0~6_combout\ & (\Equal0~8_combout\ & (\Equal0~5_combout\ & \Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~6_combout\,
	datab => \Equal0~8_combout\,
	datac => \Equal0~5_combout\,
	datad => \Equal0~4_combout\,
	combout => \Equal0~9_combout\);

-- Location: LCCOMB_X6_Y50_N28
\s_en~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_en~0_combout\ = (\Equal0~9_combout\ & ((\nRST~input_o\ & ((\DISPLAY|s_busy~q\))) # (!\nRST~input_o\ & (\s_en~q\)))) # (!\Equal0~9_combout\ & (((\s_en~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~9_combout\,
	datab => \nRST~input_o\,
	datac => \s_en~q\,
	datad => \DISPLAY|s_busy~q\,
	combout => \s_en~0_combout\);

-- Location: FF_X6_Y50_N29
s_en : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_en~q\);

-- Location: LCCOMB_X6_Y47_N20
\DISPLAY|s_busy~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_busy~2_combout\ = (\DISPLAY|s_state_current.Ready~q\ & \s_en~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|s_state_current.Ready~q\,
	datad => \s_en~q\,
	combout => \DISPLAY|s_busy~2_combout\);

-- Location: FF_X4_Y45_N19
\DISPLAY|s_state_next.Send\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|s_busy~2_combout\,
	sload => VCC,
	ena => \DISPLAY|s_state_next.Init~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_next.Send~q\);

-- Location: FF_X6_Y45_N25
\DISPLAY|s_state_current.Send\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|s_state_next.Send~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_current.Send~q\);

-- Location: LCCOMB_X5_Y47_N22
\DISPLAY|s_state_next.Ready~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_state_next.Ready~0_combout\ = (\DISPLAY|s_state_current.Ready~q\ & (((!\s_en~q\)))) # (!\DISPLAY|s_state_current.Ready~q\ & ((\DISPLAY|s_state_current.Send~q\) # ((\DISPLAY|s_state_current.Init~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Send~q\,
	datab => \DISPLAY|s_state_current.Ready~q\,
	datac => \s_en~q\,
	datad => \DISPLAY|s_state_current.Init~q\,
	combout => \DISPLAY|s_state_next.Ready~0_combout\);

-- Location: FF_X5_Y45_N21
\DISPLAY|s_state_next.Ready\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|s_state_next.Ready~0_combout\,
	sload => VCC,
	ena => \DISPLAY|s_state_next.Init~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_next.Ready~q\);

-- Location: FF_X6_Y47_N19
\DISPLAY|s_state_current.Ready\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|s_state_next.Ready~q\,
	clrn => \nRST~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_state_current.Ready~q\);

-- Location: LCCOMB_X5_Y50_N24
\Mux6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux6~0_combout\ = (ci(0) & (ci(2) $ (((ci(3) & ci(1)))))) # (!ci(0) & ((ci(1) & (!ci(2))) # (!ci(1) & ((ci(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110010110101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(2),
	datab => ci(3),
	datac => ci(0),
	datad => ci(1),
	combout => \Mux6~0_combout\);

-- Location: LCCOMB_X5_Y50_N2
\s_data[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_data[0]~0_combout\ = (\DISPLAY|s_busy~q\ & (\nRST~input_o\ & \Equal0~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_busy~q\,
	datab => \nRST~input_o\,
	datac => \Equal0~9_combout\,
	combout => \s_data[0]~0_combout\);

-- Location: FF_X5_Y50_N25
\s_data[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Mux6~0_combout\,
	ena => \s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(0));

-- Location: FF_X4_Y47_N9
\DISPLAY|P_UEBERGANG:data[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|data~3_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:data[0]~q\);

-- Location: LCCOMB_X6_Y47_N0
\DISPLAY|data~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~1_combout\ = (!\DISPLAY|s_busy~2_combout\ & (((\DISPLAY|s_state_init_current.Finish~q\) # (!\DISPLAY|s_state_current.Init~q\)) # (!\DISPLAY|s_state_init_current.Reset~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_init_current.Reset~q\,
	datab => \DISPLAY|s_state_current.Init~q\,
	datac => \DISPLAY|s_state_init_current.Finish~q\,
	datad => \DISPLAY|s_busy~2_combout\,
	combout => \DISPLAY|data~1_combout\);

-- Location: LCCOMB_X4_Y47_N26
\DISPLAY|data~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~0_combout\ = (((!\DISPLAY|P_UEBERGANG:c_pos_y~0_combout\) # (!\DISPLAY|s_state_send_current~q\)) # (!\DISPLAY|s_state_current.Send~q\)) # (!\DISPLAY|P_UEBERGANG:c_pos_x[3]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:c_pos_x[3]~q\,
	datab => \DISPLAY|s_state_current.Send~q\,
	datac => \DISPLAY|s_state_send_current~q\,
	datad => \DISPLAY|P_UEBERGANG:c_pos_y~0_combout\,
	combout => \DISPLAY|data~0_combout\);

-- Location: LCCOMB_X4_Y47_N16
\DISPLAY|data~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~2_combout\ = (((\DISPLAY|data~1_combout\ & \DISPLAY|data~0_combout\)) # (!\DISPLAY|Equal0~4_combout\)) # (!\DISPLAY|Equal0~9_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Equal0~9_combout\,
	datab => \DISPLAY|data~1_combout\,
	datac => \DISPLAY|data~0_combout\,
	datad => \DISPLAY|Equal0~4_combout\,
	combout => \DISPLAY|data~2_combout\);

-- Location: LCCOMB_X4_Y47_N8
\DISPLAY|data~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~3_combout\ = (\DISPLAY|data~2_combout\ & (((\DISPLAY|P_UEBERGANG:data[0]~q\)))) # (!\DISPLAY|data~2_combout\ & (\DISPLAY|s_state_current.Ready~q\ & (s_data(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Ready~q\,
	datab => s_data(0),
	datac => \DISPLAY|P_UEBERGANG:data[0]~q\,
	datad => \DISPLAY|data~2_combout\,
	combout => \DISPLAY|data~3_combout\);

-- Location: FF_X4_Y47_N1
\DISPLAY|s_data[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|data~3_combout\,
	sload => VCC,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_data\(0));

-- Location: LCCOMB_X5_Y50_N6
\Mux5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux5~0_combout\ = (ci(3) & (ci(2) & (!ci(0)))) # (!ci(3) & ((ci(0) $ (ci(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101100111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(2),
	datab => ci(3),
	datac => ci(0),
	datad => ci(1),
	combout => \Mux5~0_combout\);

-- Location: FF_X5_Y50_N7
\s_data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Mux5~0_combout\,
	ena => \s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(1));

-- Location: LCCOMB_X5_Y47_N24
\DISPLAY|data~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~4_combout\ = (\DISPLAY|s_state_current.Ready~q\ & ((s_data(1)))) # (!\DISPLAY|s_state_current.Ready~q\ & (\DISPLAY|s_state_init_current.Entry~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Ready~q\,
	datac => \DISPLAY|s_state_init_current.Entry~q\,
	datad => s_data(1),
	combout => \DISPLAY|data~4_combout\);

-- Location: LCCOMB_X4_Y47_N24
\DISPLAY|data~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~5_combout\ = (\DISPLAY|s_state_current.Send~q\ & (((!\DISPLAY|P_UEBERGANG:c_pos_y~0_combout\) # (!\DISPLAY|s_state_send_current~q\)) # (!\DISPLAY|P_UEBERGANG:c_pos_x[3]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:c_pos_x[3]~q\,
	datab => \DISPLAY|s_state_current.Send~q\,
	datac => \DISPLAY|s_state_send_current~q\,
	datad => \DISPLAY|P_UEBERGANG:c_pos_y~0_combout\,
	combout => \DISPLAY|data~5_combout\);

-- Location: LCCOMB_X5_Y47_N6
\DISPLAY|data~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~7_combout\ = (\DISPLAY|s_state_current.Start~q\ & (\DISPLAY|Equal0~10_combout\ & ((\s_en~q\) # (!\DISPLAY|s_state_current.Ready~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Ready~q\,
	datab => \DISPLAY|s_state_current.Start~q\,
	datac => \s_en~q\,
	datad => \DISPLAY|Equal0~10_combout\,
	combout => \DISPLAY|data~7_combout\);

-- Location: LCCOMB_X4_Y47_N2
\DISPLAY|data~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~9_combout\ = (!\DISPLAY|s_state_current.Send~q\ & (!\DISPLAY|data~5_combout\ & \DISPLAY|data~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Send~q\,
	datab => \DISPLAY|data~5_combout\,
	datad => \DISPLAY|data~7_combout\,
	combout => \DISPLAY|data~9_combout\);

-- Location: FF_X4_Y47_N23
\DISPLAY|P_UEBERGANG:data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|data~10_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:data[1]~q\);

-- Location: LCCOMB_X6_Y47_N26
\DISPLAY|data~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~6_combout\ = (!\DISPLAY|s_state_current.Ready~q\ & (!\DISPLAY|s_state_current.Send~q\ & ((\DISPLAY|s_state_init_current.Finish~q\) # (!\DISPLAY|s_state_init_current.Reset~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Ready~q\,
	datab => \DISPLAY|s_state_current.Send~q\,
	datac => \DISPLAY|s_state_init_current.Finish~q\,
	datad => \DISPLAY|s_state_init_current.Reset~q\,
	combout => \DISPLAY|data~6_combout\);

-- Location: LCCOMB_X4_Y47_N6
\DISPLAY|data~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~8_combout\ = (\DISPLAY|data~6_combout\) # ((\DISPLAY|data~5_combout\) # (!\DISPLAY|data~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|data~6_combout\,
	datab => \DISPLAY|data~5_combout\,
	datad => \DISPLAY|data~7_combout\,
	combout => \DISPLAY|data~8_combout\);

-- Location: LCCOMB_X4_Y47_N22
\DISPLAY|data~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~10_combout\ = (\DISPLAY|data~4_combout\ & ((\DISPLAY|data~9_combout\) # ((\DISPLAY|P_UEBERGANG:data[1]~q\ & \DISPLAY|data~8_combout\)))) # (!\DISPLAY|data~4_combout\ & (((\DISPLAY|P_UEBERGANG:data[1]~q\ & \DISPLAY|data~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|data~4_combout\,
	datab => \DISPLAY|data~9_combout\,
	datac => \DISPLAY|P_UEBERGANG:data[1]~q\,
	datad => \DISPLAY|data~8_combout\,
	combout => \DISPLAY|data~10_combout\);

-- Location: FF_X4_Y47_N3
\DISPLAY|s_data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|data~10_combout\,
	sload => VCC,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_data\(1));

-- Location: LCCOMB_X5_Y50_N16
\Mux4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux4~0_combout\ = (ci(2) & (ci(0) $ (((ci(3) & ci(1)))))) # (!ci(2) & (!ci(3) & (ci(0) & ci(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(2),
	datab => ci(3),
	datac => ci(0),
	datad => ci(1),
	combout => \Mux4~0_combout\);

-- Location: FF_X5_Y50_N17
\s_data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Mux4~0_combout\,
	ena => \s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(2));

-- Location: LCCOMB_X4_Y47_N0
\DISPLAY|data~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~11_combout\ = (\DISPLAY|s_state_current.Ready~q\ & (s_data(2))) # (!\DISPLAY|s_state_current.Ready~q\ & ((!\DISPLAY|s_state_current.Send~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011011101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Ready~q\,
	datab => s_data(2),
	datad => \DISPLAY|s_state_current.Send~q\,
	combout => \DISPLAY|data~11_combout\);

-- Location: FF_X4_Y47_N19
\DISPLAY|P_UEBERGANG:data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|data~12_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:data[2]~q\);

-- Location: LCCOMB_X4_Y47_N18
\DISPLAY|data~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~12_combout\ = (\DISPLAY|data~2_combout\ & ((\DISPLAY|P_UEBERGANG:data[2]~q\))) # (!\DISPLAY|data~2_combout\ & (\DISPLAY|data~11_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DISPLAY|data~11_combout\,
	datac => \DISPLAY|P_UEBERGANG:data[2]~q\,
	datad => \DISPLAY|data~2_combout\,
	combout => \DISPLAY|data~12_combout\);

-- Location: LCCOMB_X4_Y47_N28
\DISPLAY|s_data[2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_data[2]~feeder_combout\ = \DISPLAY|data~12_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \DISPLAY|data~12_combout\,
	combout => \DISPLAY|s_data[2]~feeder_combout\);

-- Location: FF_X4_Y47_N29
\DISPLAY|s_data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|s_data[2]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_data\(2));

-- Location: LCCOMB_X5_Y50_N10
\Mux3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux3~0_combout\ = (ci(3) & (ci(0) $ (((ci(2)) # (!ci(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(2),
	datab => ci(3),
	datac => ci(0),
	datad => ci(1),
	combout => \Mux3~0_combout\);

-- Location: FF_X5_Y50_N11
\s_data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Mux3~0_combout\,
	ena => \s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(3));

-- Location: LCCOMB_X5_Y47_N28
\DISPLAY|data~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~13_combout\ = (\DISPLAY|s_state_current.Ready~q\ & (((s_data(3))))) # (!\DISPLAY|s_state_current.Ready~q\ & ((\DISPLAY|s_state_init_current.Disp~q\) # ((\DISPLAY|s_state_init_current.Func~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010111100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Ready~q\,
	datab => \DISPLAY|s_state_init_current.Disp~q\,
	datac => s_data(3),
	datad => \DISPLAY|s_state_init_current.Func~q\,
	combout => \DISPLAY|data~13_combout\);

-- Location: FF_X4_Y47_N13
\DISPLAY|P_UEBERGANG:data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|data~14_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:data[3]~q\);

-- Location: LCCOMB_X4_Y47_N12
\DISPLAY|data~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~14_combout\ = (\DISPLAY|data~8_combout\ & ((\DISPLAY|P_UEBERGANG:data[3]~q\) # ((\DISPLAY|data~13_combout\ & \DISPLAY|data~9_combout\)))) # (!\DISPLAY|data~8_combout\ & (\DISPLAY|data~13_combout\ & ((\DISPLAY|data~9_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|data~8_combout\,
	datab => \DISPLAY|data~13_combout\,
	datac => \DISPLAY|P_UEBERGANG:data[3]~q\,
	datad => \DISPLAY|data~9_combout\,
	combout => \DISPLAY|data~14_combout\);

-- Location: LCCOMB_X4_Y47_N30
\DISPLAY|s_data[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_data[3]~feeder_combout\ = \DISPLAY|data~14_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \DISPLAY|data~14_combout\,
	combout => \DISPLAY|s_data[3]~feeder_combout\);

-- Location: FF_X4_Y47_N31
\DISPLAY|s_data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|s_data[3]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_data\(3));

-- Location: LCCOMB_X5_Y50_N4
\Mux2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux2~0_combout\ = (!ci(0) & ((ci(2) & (!ci(3) & ci(1))) # (!ci(2) & (ci(3) & !ci(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(2),
	datab => ci(3),
	datac => ci(0),
	datad => ci(1),
	combout => \Mux2~0_combout\);

-- Location: FF_X5_Y50_N5
\s_data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Mux2~0_combout\,
	ena => \s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(4));

-- Location: LCCOMB_X5_Y47_N30
\DISPLAY|data~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~15_combout\ = (\DISPLAY|s_state_current.Ready~q\ & (s_data(4))) # (!\DISPLAY|s_state_current.Ready~q\ & ((\DISPLAY|s_state_init_current.Func~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Ready~q\,
	datac => s_data(4),
	datad => \DISPLAY|s_state_init_current.Func~q\,
	combout => \DISPLAY|data~15_combout\);

-- Location: FF_X4_Y47_N11
\DISPLAY|P_UEBERGANG:data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|data~16_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:data[4]~q\);

-- Location: LCCOMB_X4_Y47_N10
\DISPLAY|data~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~16_combout\ = (\DISPLAY|data~8_combout\ & ((\DISPLAY|P_UEBERGANG:data[4]~q\) # ((\DISPLAY|data~15_combout\ & \DISPLAY|data~9_combout\)))) # (!\DISPLAY|data~8_combout\ & (\DISPLAY|data~15_combout\ & ((\DISPLAY|data~9_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|data~8_combout\,
	datab => \DISPLAY|data~15_combout\,
	datac => \DISPLAY|P_UEBERGANG:data[4]~q\,
	datad => \DISPLAY|data~9_combout\,
	combout => \DISPLAY|data~16_combout\);

-- Location: LCCOMB_X4_Y47_N20
\DISPLAY|s_data[4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_data[4]~feeder_combout\ = \DISPLAY|data~16_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \DISPLAY|data~16_combout\,
	combout => \DISPLAY|s_data[4]~feeder_combout\);

-- Location: FF_X4_Y47_N21
\DISPLAY|s_data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|s_data[4]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_data\(4));

-- Location: LCCOMB_X5_Y50_N12
\Mux1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux1~0_combout\ = (ci(3) & ((ci(2) $ (ci(1))) # (!ci(0)))) # (!ci(3) & (((ci(0)) # (ci(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111110111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => ci(2),
	datab => ci(3),
	datac => ci(0),
	datad => ci(1),
	combout => \Mux1~0_combout\);

-- Location: LCCOMB_X5_Y50_N22
\s_data[5]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_data[5]~1_combout\ = !\Mux1~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \Mux1~0_combout\,
	combout => \s_data[5]~1_combout\);

-- Location: FF_X5_Y50_N23
\s_data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_data[5]~1_combout\,
	ena => \s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(5));

-- Location: LCCOMB_X5_Y47_N20
\DISPLAY|data~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~17_combout\ = (\DISPLAY|s_state_current.Ready~q\ & (s_data(5))) # (!\DISPLAY|s_state_current.Ready~q\ & ((\DISPLAY|s_state_init_current.Func~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Ready~q\,
	datac => s_data(5),
	datad => \DISPLAY|s_state_init_current.Func~q\,
	combout => \DISPLAY|data~17_combout\);

-- Location: FF_X4_Y47_N5
\DISPLAY|P_UEBERGANG:data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|data~18_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:data[5]~q\);

-- Location: LCCOMB_X4_Y47_N4
\DISPLAY|data~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~18_combout\ = (\DISPLAY|data~8_combout\ & ((\DISPLAY|P_UEBERGANG:data[5]~q\) # ((\DISPLAY|data~17_combout\ & \DISPLAY|data~9_combout\)))) # (!\DISPLAY|data~8_combout\ & (\DISPLAY|data~17_combout\ & ((\DISPLAY|data~9_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|data~8_combout\,
	datab => \DISPLAY|data~17_combout\,
	datac => \DISPLAY|P_UEBERGANG:data[5]~q\,
	datad => \DISPLAY|data~9_combout\,
	combout => \DISPLAY|data~18_combout\);

-- Location: FF_X4_Y47_N7
\DISPLAY|s_data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|data~18_combout\,
	sload => VCC,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_data\(5));

-- Location: FF_X5_Y50_N13
\s_data[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Mux1~0_combout\,
	ena => \s_data[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_data(6));

-- Location: LCCOMB_X4_Y47_N14
\DISPLAY|data~21\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~21_combout\ = (\DISPLAY|P_UEBERGANG:c_pos_x[3]~q\ & (\DISPLAY|s_state_current.Send~q\ & (\DISPLAY|s_state_send_current~q\ & \DISPLAY|P_UEBERGANG:c_pos_y~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|P_UEBERGANG:c_pos_x[3]~q\,
	datab => \DISPLAY|s_state_current.Send~q\,
	datac => \DISPLAY|s_state_send_current~q\,
	datad => \DISPLAY|P_UEBERGANG:c_pos_y~0_combout\,
	combout => \DISPLAY|data~21_combout\);

-- Location: LCCOMB_X5_Y47_N14
\DISPLAY|P_UEBERGANG:c_pos_y~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|P_UEBERGANG:c_pos_y~2_combout\ = \DISPLAY|P_UEBERGANG:c_pos_y~q\ $ (((\DISPLAY|data~21_combout\ & (\DISPLAY|Equal0~9_combout\ & \DISPLAY|Equal0~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|data~21_combout\,
	datab => \DISPLAY|Equal0~9_combout\,
	datac => \DISPLAY|P_UEBERGANG:c_pos_y~q\,
	datad => \DISPLAY|Equal0~4_combout\,
	combout => \DISPLAY|P_UEBERGANG:c_pos_y~2_combout\);

-- Location: FF_X5_Y47_N15
\DISPLAY|P_UEBERGANG:c_pos_y\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|P_UEBERGANG:c_pos_y~2_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:c_pos_y~q\);

-- Location: LCCOMB_X5_Y47_N4
\DISPLAY|data~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~22_combout\ = (s_data(6) & ((\DISPLAY|s_busy~2_combout\) # ((!\DISPLAY|P_UEBERGANG:c_pos_y~q\ & \DISPLAY|data~21_combout\)))) # (!s_data(6) & (!\DISPLAY|P_UEBERGANG:c_pos_y~q\ & ((\DISPLAY|data~21_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => s_data(6),
	datab => \DISPLAY|P_UEBERGANG:c_pos_y~q\,
	datac => \DISPLAY|s_busy~2_combout\,
	datad => \DISPLAY|data~21_combout\,
	combout => \DISPLAY|data~22_combout\);

-- Location: FF_X5_Y47_N11
\DISPLAY|P_UEBERGANG:data[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|data~23_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:data[6]~q\);

-- Location: LCCOMB_X6_Y45_N20
\DISPLAY|data~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~19_combout\ = (\DISPLAY|s_state_init_current.Finish~q\) # (!\DISPLAY|s_state_init_current.Reset~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010111110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_init_current.Reset~q\,
	datac => \DISPLAY|s_state_init_current.Finish~q\,
	combout => \DISPLAY|data~19_combout\);

-- Location: LCCOMB_X5_Y47_N12
\DISPLAY|data~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~20_combout\ = (\DISPLAY|data~5_combout\) # (((\DISPLAY|s_state_current.Init~q\ & \DISPLAY|data~19_combout\)) # (!\DISPLAY|data~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Init~q\,
	datab => \DISPLAY|data~5_combout\,
	datac => \DISPLAY|data~19_combout\,
	datad => \DISPLAY|data~7_combout\,
	combout => \DISPLAY|data~20_combout\);

-- Location: LCCOMB_X5_Y47_N10
\DISPLAY|data~23\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~23_combout\ = (\DISPLAY|Equal0~10_combout\ & ((\DISPLAY|data~22_combout\) # ((\DISPLAY|P_UEBERGANG:data[6]~q\ & \DISPLAY|data~20_combout\)))) # (!\DISPLAY|Equal0~10_combout\ & (((\DISPLAY|P_UEBERGANG:data[6]~q\ & 
-- \DISPLAY|data~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|Equal0~10_combout\,
	datab => \DISPLAY|data~22_combout\,
	datac => \DISPLAY|P_UEBERGANG:data[6]~q\,
	datad => \DISPLAY|data~20_combout\,
	combout => \DISPLAY|data~23_combout\);

-- Location: LCCOMB_X5_Y47_N8
\DISPLAY|s_data[6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_data[6]~feeder_combout\ = \DISPLAY|data~23_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \DISPLAY|data~23_combout\,
	combout => \DISPLAY|s_data[6]~feeder_combout\);

-- Location: FF_X5_Y47_N9
\DISPLAY|s_data[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|s_data[6]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_data\(6));

-- Location: FF_X5_Y47_N19
\DISPLAY|P_UEBERGANG:data[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|data~25_combout\,
	clrn => \nRST~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:data[7]~q\);

-- Location: LCCOMB_X5_Y47_N16
\DISPLAY|data~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~24_combout\ = (\DISPLAY|s_state_current.Send~q\) # (((\DISPLAY|s_state_current.Init~q\ & \DISPLAY|data~19_combout\)) # (!\DISPLAY|data~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Init~q\,
	datab => \DISPLAY|s_state_current.Send~q\,
	datac => \DISPLAY|data~19_combout\,
	datad => \DISPLAY|data~7_combout\,
	combout => \DISPLAY|data~24_combout\);

-- Location: LCCOMB_X5_Y47_N18
\DISPLAY|data~25\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|data~25_combout\ = (\DISPLAY|data~21_combout\ & ((\DISPLAY|Equal0~10_combout\) # ((\DISPLAY|P_UEBERGANG:data[7]~q\ & \DISPLAY|data~24_combout\)))) # (!\DISPLAY|data~21_combout\ & (((\DISPLAY|P_UEBERGANG:data[7]~q\ & \DISPLAY|data~24_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|data~21_combout\,
	datab => \DISPLAY|Equal0~10_combout\,
	datac => \DISPLAY|P_UEBERGANG:data[7]~q\,
	datad => \DISPLAY|data~24_combout\,
	combout => \DISPLAY|data~25_combout\);

-- Location: LCCOMB_X5_Y47_N26
\DISPLAY|s_data[7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|s_data[7]~feeder_combout\ = \DISPLAY|data~25_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \DISPLAY|data~25_combout\,
	combout => \DISPLAY|s_data[7]~feeder_combout\);

-- Location: FF_X5_Y47_N27
\DISPLAY|s_data[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|s_data[7]~feeder_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_data\(7));

-- Location: FF_X6_Y45_N31
\DISPLAY|P_UEBERGANG:en\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|en~1_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|P_UEBERGANG:en~q\);

-- Location: LCCOMB_X6_Y45_N24
\DISPLAY|en~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|en~0_combout\ = (\DISPLAY|P_UEBERGANG:en~q\) # ((\DISPLAY|s_state_current.Init~q\ & (!\DISPLAY|s_state_current.Send~q\ & !\DISPLAY|data~19_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Init~q\,
	datab => \DISPLAY|P_UEBERGANG:en~q\,
	datac => \DISPLAY|s_state_current.Send~q\,
	datad => \DISPLAY|data~19_combout\,
	combout => \DISPLAY|en~0_combout\);

-- Location: LCCOMB_X6_Y45_N30
\DISPLAY|en~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \DISPLAY|en~1_combout\ = (\DISPLAY|Equal0~10_combout\ & ((\DISPLAY|en~0_combout\) # ((\DISPLAY|s_state_current.Send~q\ & !\DISPLAY|s_state_next~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DISPLAY|s_state_current.Send~q\,
	datab => \DISPLAY|s_state_next~10_combout\,
	datac => \DISPLAY|en~0_combout\,
	datad => \DISPLAY|Equal0~10_combout\,
	combout => \DISPLAY|en~1_combout\);

-- Location: FF_X6_Y45_N27
\DISPLAY|s_en\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \DISPLAY|en~1_combout\,
	sload => VCC,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_en~q\);

-- Location: FF_X6_Y45_N15
\DISPLAY|s_rs\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \DISPLAY|rs~4_combout\,
	ena => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \DISPLAY|s_rs~q\);

-- Location: IOIBUF_X89_Y73_N22
\BCD[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BCD(0),
	o => \BCD[0]~input_o\);

-- Location: IOIBUF_X9_Y73_N8
\BCD[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BCD(1),
	o => \BCD[1]~input_o\);

-- Location: IOIBUF_X0_Y13_N8
\BCD[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BCD(2),
	o => \BCD[2]~input_o\);

-- Location: IOIBUF_X49_Y0_N22
\BCD[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BCD(3),
	o => \BCD[3]~input_o\);

-- Location: IOIBUF_X1_Y73_N1
\GRAY[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GRAY(0),
	o => \GRAY[0]~input_o\);

-- Location: IOIBUF_X58_Y73_N1
\GRAY[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GRAY(1),
	o => \GRAY[1]~input_o\);

-- Location: IOIBUF_X0_Y57_N22
\GRAY[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GRAY(2),
	o => \GRAY[2]~input_o\);

-- Location: IOIBUF_X0_Y16_N22
\GRAY[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_GRAY(3),
	o => \GRAY[3]~input_o\);

-- Location: IOIBUF_X38_Y73_N1
\AIKEN[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_AIKEN(0),
	o => \AIKEN[0]~input_o\);

-- Location: IOIBUF_X20_Y73_N15
\AIKEN[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_AIKEN(1),
	o => \AIKEN[1]~input_o\);

-- Location: IOIBUF_X115_Y36_N15
\AIKEN[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_AIKEN(2),
	o => \AIKEN[2]~input_o\);

-- Location: IOIBUF_X115_Y13_N1
\AIKEN[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_AIKEN(3),
	o => \AIKEN[3]~input_o\);

ww_LCD_Data(0) <= \LCD_Data[0]~output_o\;

ww_LCD_Data(1) <= \LCD_Data[1]~output_o\;

ww_LCD_Data(2) <= \LCD_Data[2]~output_o\;

ww_LCD_Data(3) <= \LCD_Data[3]~output_o\;

ww_LCD_Data(4) <= \LCD_Data[4]~output_o\;

ww_LCD_Data(5) <= \LCD_Data[5]~output_o\;

ww_LCD_Data(6) <= \LCD_Data[6]~output_o\;

ww_LCD_Data(7) <= \LCD_Data[7]~output_o\;

ww_LCD_EN <= \LCD_EN~output_o\;

ww_LCD_RW <= \LCD_RW~output_o\;

ww_LCD_RS <= \LCD_RS~output_o\;
END structure;


