LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Segmentdisplay IS
	PORT (
		LTN, BLN, HB	:	IN		std_logic;
		D					:	IN		std_logic_vector(3 downto 0);
		S					:	OUT	std_logic_vector(6 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF Segmentdisplay IS

BEGIN
	
	PROG : PROCESS(LTN, BLN, D, HB)
		VARIABLE SD	:	std_logic_vector(6 downto 0) := (Others => '1');
	BEGIN
		if (BLN = '1') then
			SD := "0000000";
		else
			if (LTN = '0') then
				SD := "1111111";
			else
				case D is
					when "0000"	=> SD	:= "1000000";
					when "0001"	=>	SD	:= "1111001";
					when "0010" => SD	:= "0100100";
					when "0011" => SD	:= "0110000";
					when "0100" =>	SD	:= "0011001";
					when "0101" => SD	:=	"0010010";
					when "0110" => SD	:=	"0000010";
					when "0111" => SD	:= "1111000";
					when "1000" =>	SD	:=	"0000000";
					when "1001" => SD	:= "0010000";
					when "1010" => 
						if (HB = '0') then
							SD := "0001000";
						else
							SD := "1111111";
						end if;
					when "1011" =>
						if (HB = '0') then
							SD := "0000011";
						else
							SD := "1111111";
						end if;
					when "1100" => 
						if (HB = '0') then
							SD := "1000110";
						else
							SD := "1111111";
						end if;
					when "1101" => 
						if (HB = '0') then
							SD := "0100001";
						else
							SD := "1111111";
						end if;
					when "1110" => 
						if (HB = '0') then
							SD := "0000110";
						else
							SD := "1111111";
						end if;
					when "1111" => 
						if (HB = '0') then
							SD := "0001110";
						else
							SD := "1111111";
						end if;
					when OTHERS => SD := "1111111";
				end case;
			end if;
		end if;
		S <= SD;
	END PROCESS PROG;
END Behaviour;