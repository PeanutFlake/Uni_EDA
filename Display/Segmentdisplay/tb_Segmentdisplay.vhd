LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_Segmentdisplay IS
END ENTITY;

ARCHITECTURE Testbench OF tb_Segmentdisplay IS

	COMPONENT Segmentdisplay IS
		PORT (
			LTN, BLN	:	IN		std_logic;
			D			:	IN		std_logic_vector(3 downto 0);
			S			:	OUT	std_logic_vector(6 downto 0)
		);
	END COMPONENT;

	SIGNAL s_ltn, s_bln 	:	std_logic := '0';
	SIGNAL s_d				:	std_logic_vector(3 downto 0) := (Others => '0');
	SIGNAL s_q				:	std_logic_vector(6 downto 0) := (Others => '0');
	
BEGIN

	DUT : Segmentdisplay
	PORT MAP (
		s_ltn, s_bln, s_d, s_q
	);
	
	s_ltn <= '1' AFTER 0ns, '0' AFTER 200ns, '1' AFTER 300ns;
	s_bln <= '1' AFTER 160ns, '0' AFTER 260ns;
	
	s_d(0) <= NOT s_d(0) AFTER 10ns;
	s_d(1) <= NOT s_d(1) AFTER 20ns;
	s_d(2) <= NOT s_d(2) AFTER 40ns;
	s_d(3) <= NOT s_d(3) AFTER 80ns;

END Testbench;