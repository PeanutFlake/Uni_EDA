-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/13/2018 18:18:37"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	Segmentdisplay IS
    PORT (
	LTN : IN std_logic;
	BLN : IN std_logic;
	HB : IN std_logic;
	D : IN std_logic_vector(3 DOWNTO 0);
	S : OUT std_logic_vector(6 DOWNTO 0)
	);
END Segmentdisplay;

-- Design Ports Information
-- S[0]	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[1]	=>  Location: PIN_F22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[2]	=>  Location: PIN_E17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[3]	=>  Location: PIN_L26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[4]	=>  Location: PIN_L25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[5]	=>  Location: PIN_J22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[6]	=>  Location: PIN_H22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LTN	=>  Location: PIN_AB28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BLN	=>  Location: PIN_AC28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[1]	=>  Location: PIN_Y24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[2]	=>  Location: PIN_AA22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[3]	=>  Location: PIN_AA23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[0]	=>  Location: PIN_Y23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HB	=>  Location: PIN_AC27,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF Segmentdisplay IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_LTN : std_logic;
SIGNAL ww_BLN : std_logic;
SIGNAL ww_HB : std_logic;
SIGNAL ww_D : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_S : std_logic_vector(6 DOWNTO 0);
SIGNAL \S[0]~output_o\ : std_logic;
SIGNAL \S[1]~output_o\ : std_logic;
SIGNAL \S[2]~output_o\ : std_logic;
SIGNAL \S[3]~output_o\ : std_logic;
SIGNAL \S[4]~output_o\ : std_logic;
SIGNAL \S[5]~output_o\ : std_logic;
SIGNAL \S[6]~output_o\ : std_logic;
SIGNAL \LTN~input_o\ : std_logic;
SIGNAL \D[1]~input_o\ : std_logic;
SIGNAL \BLN~input_o\ : std_logic;
SIGNAL \D[0]~input_o\ : std_logic;
SIGNAL \D[3]~input_o\ : std_logic;
SIGNAL \HB~input_o\ : std_logic;
SIGNAL \SD~40_combout\ : std_logic;
SIGNAL \SD~32_combout\ : std_logic;
SIGNAL \D[2]~input_o\ : std_logic;
SIGNAL \SD~33_combout\ : std_logic;
SIGNAL \SD~34_combout\ : std_logic;
SIGNAL \SD~50_combout\ : std_logic;
SIGNAL \SD~51_combout\ : std_logic;
SIGNAL \SD~42_combout\ : std_logic;
SIGNAL \SD~48_combout\ : std_logic;
SIGNAL \SD~49_combout\ : std_logic;
SIGNAL \SD~41_combout\ : std_logic;
SIGNAL \Mux3~4_combout\ : std_logic;
SIGNAL \Mux3~5_combout\ : std_logic;
SIGNAL \SD~35_combout\ : std_logic;
SIGNAL \SD~36_combout\ : std_logic;
SIGNAL \SD~37_combout\ : std_logic;
SIGNAL \SD~38_combout\ : std_logic;
SIGNAL \SD~46_combout\ : std_logic;
SIGNAL \SD~47_combout\ : std_logic;
SIGNAL \SD~39_combout\ : std_logic;
SIGNAL \SD~44_combout\ : std_logic;
SIGNAL \SD~45_combout\ : std_logic;
SIGNAL \SD~43_combout\ : std_logic;

BEGIN

ww_LTN <= LTN;
ww_BLN <= BLN;
ww_HB <= HB;
ww_D <= D;
S <= ww_S;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: IOOBUF_X69_Y73_N23
\S[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SD~34_combout\,
	devoe => ww_devoe,
	o => \S[0]~output_o\);

-- Location: IOOBUF_X107_Y73_N23
\S[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SD~42_combout\,
	devoe => ww_devoe,
	o => \S[1]~output_o\);

-- Location: IOOBUF_X67_Y73_N23
\S[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SD~41_combout\,
	devoe => ww_devoe,
	o => \S[2]~output_o\);

-- Location: IOOBUF_X115_Y50_N2
\S[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SD~35_combout\,
	devoe => ww_devoe,
	o => \S[3]~output_o\);

-- Location: IOOBUF_X115_Y54_N16
\S[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SD~38_combout\,
	devoe => ww_devoe,
	o => \S[4]~output_o\);

-- Location: IOOBUF_X115_Y67_N16
\S[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SD~39_combout\,
	devoe => ww_devoe,
	o => \S[5]~output_o\);

-- Location: IOOBUF_X115_Y69_N2
\S[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SD~43_combout\,
	devoe => ww_devoe,
	o => \S[6]~output_o\);

-- Location: IOIBUF_X115_Y17_N1
\LTN~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_LTN,
	o => \LTN~input_o\);

-- Location: IOIBUF_X115_Y13_N1
\D[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(1),
	o => \D[1]~input_o\);

-- Location: IOIBUF_X115_Y14_N1
\BLN~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BLN,
	o => \BLN~input_o\);

-- Location: IOIBUF_X115_Y14_N8
\D[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(0),
	o => \D[0]~input_o\);

-- Location: IOIBUF_X115_Y10_N8
\D[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(3),
	o => \D[3]~input_o\);

-- Location: IOIBUF_X115_Y15_N8
\HB~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_HB,
	o => \HB~input_o\);

-- Location: LCCOMB_X114_Y18_N10
\SD~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~40_combout\ = (\D[0]~input_o\ & (((\D[3]~input_o\)))) # (!\D[0]~input_o\ & (((\D[3]~input_o\ & \HB~input_o\)) # (!\D[1]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000110110001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[0]~input_o\,
	datab => \D[1]~input_o\,
	datac => \D[3]~input_o\,
	datad => \HB~input_o\,
	combout => \SD~40_combout\);

-- Location: LCCOMB_X114_Y18_N8
\SD~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~32_combout\ = (\D[1]~input_o\ & (((\D[3]~input_o\ & \HB~input_o\)))) # (!\D[1]~input_o\ & ((\D[0]~input_o\ & (!\D[3]~input_o\)) # (!\D[0]~input_o\ & (\D[3]~input_o\ & !\HB~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001000010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[0]~input_o\,
	datab => \D[1]~input_o\,
	datac => \D[3]~input_o\,
	datad => \HB~input_o\,
	combout => \SD~32_combout\);

-- Location: IOIBUF_X115_Y6_N15
\D[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(2),
	o => \D[2]~input_o\);

-- Location: LCCOMB_X114_Y18_N18
\SD~33\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~33_combout\ = (\D[2]~input_o\ & ((\SD~32_combout\))) # (!\D[2]~input_o\ & (\SD~40_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SD~40_combout\,
	datab => \SD~32_combout\,
	datac => \D[2]~input_o\,
	combout => \SD~33_combout\);

-- Location: LCCOMB_X114_Y18_N4
\SD~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~34_combout\ = ((!\LTN~input_o\ & (\D[1]~input_o\ $ (!\SD~33_combout\)))) # (!\BLN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111100011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LTN~input_o\,
	datab => \D[1]~input_o\,
	datac => \BLN~input_o\,
	datad => \SD~33_combout\,
	combout => \SD~34_combout\);

-- Location: LCCOMB_X114_Y18_N12
\SD~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~50_combout\ = (\D[3]~input_o\ & ((\HB~input_o\) # (\D[0]~input_o\ $ (!\D[1]~input_o\)))) # (!\D[3]~input_o\ & (\D[0]~input_o\ & (!\D[1]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[0]~input_o\,
	datab => \D[1]~input_o\,
	datac => \D[3]~input_o\,
	datad => \HB~input_o\,
	combout => \SD~50_combout\);

-- Location: LCCOMB_X114_Y18_N14
\SD~51\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~51_combout\ = (\SD~50_combout\ & ((\D[1]~input_o\) # ((\D[2]~input_o\)))) # (!\SD~50_combout\ & (\D[1]~input_o\ & (\D[2]~input_o\ & !\D[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100011101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SD~50_combout\,
	datab => \D[1]~input_o\,
	datac => \D[2]~input_o\,
	datad => \D[0]~input_o\,
	combout => \SD~51_combout\);

-- Location: LCCOMB_X114_Y22_N8
\SD~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~42_combout\ = ((\SD~51_combout\ & !\LTN~input_o\)) # (!\BLN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \BLN~input_o\,
	datac => \SD~51_combout\,
	datad => \LTN~input_o\,
	combout => \SD~42_combout\);

-- Location: LCCOMB_X114_Y18_N16
\SD~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~48_combout\ = (\D[0]~input_o\ & (\D[1]~input_o\ & (\D[3]~input_o\))) # (!\D[0]~input_o\ & ((\D[1]~input_o\) # ((\D[3]~input_o\ & !\HB~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[0]~input_o\,
	datab => \D[1]~input_o\,
	datac => \D[3]~input_o\,
	datad => \HB~input_o\,
	combout => \SD~48_combout\);

-- Location: LCCOMB_X114_Y18_N26
\SD~49\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~49_combout\ = (\D[2]~input_o\ & (\D[3]~input_o\ & ((\HB~input_o\) # (\SD~48_combout\)))) # (!\D[2]~input_o\ & (\SD~48_combout\ & ((\HB~input_o\) # (!\D[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[2]~input_o\,
	datab => \HB~input_o\,
	datac => \D[3]~input_o\,
	datad => \SD~48_combout\,
	combout => \SD~49_combout\);

-- Location: LCCOMB_X114_Y22_N14
\SD~41\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~41_combout\ = ((\SD~49_combout\ & !\LTN~input_o\)) # (!\BLN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SD~49_combout\,
	datac => \BLN~input_o\,
	datad => \LTN~input_o\,
	combout => \SD~41_combout\);

-- Location: LCCOMB_X114_Y18_N28
\Mux3~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux3~4_combout\ = (\D[1]~input_o\ & ((\D[2]~input_o\ & ((\D[0]~input_o\))) # (!\D[2]~input_o\ & (\D[3]~input_o\ & !\D[0]~input_o\)))) # (!\D[1]~input_o\ & (\D[2]~input_o\ $ (((!\D[3]~input_o\ & \D[0]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100101001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[2]~input_o\,
	datab => \D[3]~input_o\,
	datac => \D[1]~input_o\,
	datad => \D[0]~input_o\,
	combout => \Mux3~4_combout\);

-- Location: LCCOMB_X114_Y18_N30
\Mux3~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux3~5_combout\ = (\HB~input_o\ & ((\Mux3~4_combout\) # ((\D[3]~input_o\ & \D[1]~input_o\)))) # (!\HB~input_o\ & (\Mux3~4_combout\ & ((\D[1]~input_o\) # (!\D[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \HB~input_o\,
	datab => \D[3]~input_o\,
	datac => \D[1]~input_o\,
	datad => \Mux3~4_combout\,
	combout => \Mux3~5_combout\);

-- Location: LCCOMB_X114_Y22_N16
\SD~35\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~35_combout\ = ((\Mux3~5_combout\ & !\LTN~input_o\)) # (!\BLN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \BLN~input_o\,
	datac => \Mux3~5_combout\,
	datad => \LTN~input_o\,
	combout => \SD~35_combout\);

-- Location: LCCOMB_X114_Y18_N6
\SD~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~36_combout\ = (\D[1]~input_o\ & ((\D[3]~input_o\))) # (!\D[1]~input_o\ & (\D[2]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[2]~input_o\,
	datab => \D[3]~input_o\,
	datac => \D[1]~input_o\,
	combout => \SD~36_combout\);

-- Location: LCCOMB_X114_Y18_N24
\SD~37\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~37_combout\ = (\SD~36_combout\ & ((\HB~input_o\) # ((!\D[3]~input_o\)))) # (!\SD~36_combout\ & (((\D[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111110001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SD~36_combout\,
	datab => \HB~input_o\,
	datac => \D[3]~input_o\,
	datad => \D[0]~input_o\,
	combout => \SD~37_combout\);

-- Location: LCCOMB_X114_Y22_N26
\SD~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~38_combout\ = ((\SD~37_combout\ & !\LTN~input_o\)) # (!\BLN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SD~37_combout\,
	datac => \BLN~input_o\,
	datad => \LTN~input_o\,
	combout => \SD~38_combout\);

-- Location: LCCOMB_X114_Y18_N0
\SD~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~46_combout\ = (\D[2]~input_o\ & (\D[0]~input_o\ & (\D[3]~input_o\ $ (\D[1]~input_o\)))) # (!\D[2]~input_o\ & ((\D[1]~input_o\) # ((!\D[3]~input_o\ & \D[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[2]~input_o\,
	datab => \D[3]~input_o\,
	datac => \D[1]~input_o\,
	datad => \D[0]~input_o\,
	combout => \SD~46_combout\);

-- Location: LCCOMB_X114_Y18_N2
\SD~47\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~47_combout\ = (\D[2]~input_o\ & ((\SD~46_combout\) # ((\HB~input_o\ & \D[3]~input_o\)))) # (!\D[2]~input_o\ & (\SD~46_combout\ & ((\HB~input_o\) # (!\D[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[2]~input_o\,
	datab => \HB~input_o\,
	datac => \D[3]~input_o\,
	datad => \SD~46_combout\,
	combout => \SD~47_combout\);

-- Location: LCCOMB_X114_Y22_N12
\SD~39\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~39_combout\ = ((\SD~47_combout\ & !\LTN~input_o\)) # (!\BLN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \BLN~input_o\,
	datac => \SD~47_combout\,
	datad => \LTN~input_o\,
	combout => \SD~39_combout\);

-- Location: LCCOMB_X114_Y18_N20
\SD~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~44_combout\ = (\D[2]~input_o\ & ((\D[3]~input_o\ & (!\D[1]~input_o\ & !\D[0]~input_o\)) # (!\D[3]~input_o\ & (\D[1]~input_o\ & \D[0]~input_o\)))) # (!\D[2]~input_o\ & (\D[3]~input_o\ $ ((!\D[1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000101001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[2]~input_o\,
	datab => \D[3]~input_o\,
	datac => \D[1]~input_o\,
	datad => \D[0]~input_o\,
	combout => \SD~44_combout\);

-- Location: LCCOMB_X114_Y18_N22
\SD~45\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~45_combout\ = (\D[2]~input_o\ & ((\SD~44_combout\) # ((\HB~input_o\ & \D[3]~input_o\)))) # (!\D[2]~input_o\ & (\SD~44_combout\ & ((\HB~input_o\) # (!\D[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[2]~input_o\,
	datab => \HB~input_o\,
	datac => \D[3]~input_o\,
	datad => \SD~44_combout\,
	combout => \SD~45_combout\);

-- Location: LCCOMB_X114_Y22_N2
\SD~43\ : cycloneive_lcell_comb
-- Equation(s):
-- \SD~43_combout\ = ((\SD~45_combout\ & !\LTN~input_o\)) # (!\BLN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SD~45_combout\,
	datac => \BLN~input_o\,
	datad => \LTN~input_o\,
	combout => \SD~43_combout\);

ww_S(0) <= \S[0]~output_o\;

ww_S(1) <= \S[1]~output_o\;

ww_S(2) <= \S[2]~output_o\;

ww_S(3) <= \S[3]~output_o\;

ww_S(4) <= \S[4]~output_o\;

ww_S(5) <= \S[5]~output_o\;

ww_S(6) <= \S[6]~output_o\;
END structure;


