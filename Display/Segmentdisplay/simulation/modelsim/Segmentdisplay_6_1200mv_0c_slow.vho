-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/13/2018 14:55:12"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	Segmentdisplay IS
    PORT (
	LTN : IN std_logic;
	BLN : IN std_logic;
	D : IN std_logic_vector(3 DOWNTO 0);
	S : OUT std_logic_vector(6 DOWNTO 0)
	);
END Segmentdisplay;

-- Design Ports Information
-- S[0]	=>  Location: PIN_M13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[1]	=>  Location: PIN_N6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[2]	=>  Location: PIN_M9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[3]	=>  Location: PIN_N8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[4]	=>  Location: PIN_N9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[5]	=>  Location: PIN_L7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[6]	=>  Location: PIN_K12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[0]	=>  Location: PIN_M4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[1]	=>  Location: PIN_N4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[2]	=>  Location: PIN_N10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[3]	=>  Location: PIN_L4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BLN	=>  Location: PIN_M6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LTN	=>  Location: PIN_L5,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF Segmentdisplay IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_LTN : std_logic;
SIGNAL ww_BLN : std_logic;
SIGNAL ww_D : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_S : std_logic_vector(6 DOWNTO 0);
SIGNAL \S[0]~output_o\ : std_logic;
SIGNAL \S[1]~output_o\ : std_logic;
SIGNAL \S[2]~output_o\ : std_logic;
SIGNAL \S[3]~output_o\ : std_logic;
SIGNAL \S[4]~output_o\ : std_logic;
SIGNAL \S[5]~output_o\ : std_logic;
SIGNAL \S[6]~output_o\ : std_logic;
SIGNAL \LTN~input_o\ : std_logic;
SIGNAL \BLN~input_o\ : std_logic;
SIGNAL \D[3]~input_o\ : std_logic;
SIGNAL \D[1]~input_o\ : std_logic;
SIGNAL \D[0]~input_o\ : std_logic;
SIGNAL \D[2]~input_o\ : std_logic;
SIGNAL \Mux6~0_combout\ : std_logic;
SIGNAL \SD~0_combout\ : std_logic;
SIGNAL \Mux5~0_combout\ : std_logic;
SIGNAL \SD~1_combout\ : std_logic;
SIGNAL \Mux4~0_combout\ : std_logic;
SIGNAL \SD~2_combout\ : std_logic;
SIGNAL \Mux3~0_combout\ : std_logic;
SIGNAL \SD~3_combout\ : std_logic;
SIGNAL \Mux2~0_combout\ : std_logic;
SIGNAL \SD~4_combout\ : std_logic;
SIGNAL \Mux1~0_combout\ : std_logic;
SIGNAL \SD~5_combout\ : std_logic;
SIGNAL \Mux0~0_combout\ : std_logic;
SIGNAL \SD~6_combout\ : std_logic;
SIGNAL \ALT_INV_SD~6_combout\ : std_logic;

BEGIN

ww_LTN <= LTN;
ww_BLN <= BLN;
ww_D <= D;
S <= ww_S;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_SD~6_combout\ <= NOT \SD~6_combout\;

-- Location: IOOBUF_X33_Y10_N2
\S[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SD~0_combout\,
	devoe => ww_devoe,
	o => \S[0]~output_o\);

-- Location: IOOBUF_X12_Y0_N2
\S[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SD~1_combout\,
	devoe => ww_devoe,
	o => \S[1]~output_o\);

-- Location: IOOBUF_X24_Y0_N2
\S[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SD~2_combout\,
	devoe => ww_devoe,
	o => \S[2]~output_o\);

-- Location: IOOBUF_X20_Y0_N9
\S[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SD~3_combout\,
	devoe => ww_devoe,
	o => \S[3]~output_o\);

-- Location: IOOBUF_X20_Y0_N2
\S[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SD~4_combout\,
	devoe => ww_devoe,
	o => \S[4]~output_o\);

-- Location: IOOBUF_X14_Y0_N2
\S[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SD~5_combout\,
	devoe => ww_devoe,
	o => \S[5]~output_o\);

-- Location: IOOBUF_X33_Y11_N9
\S[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_SD~6_combout\,
	devoe => ww_devoe,
	o => \S[6]~output_o\);

-- Location: IOIBUF_X14_Y0_N8
\LTN~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_LTN,
	o => \LTN~input_o\);

-- Location: IOIBUF_X12_Y0_N8
\BLN~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BLN,
	o => \BLN~input_o\);

-- Location: IOIBUF_X8_Y0_N8
\D[3]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(3),
	o => \D[3]~input_o\);

-- Location: IOIBUF_X10_Y0_N8
\D[1]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(1),
	o => \D[1]~input_o\);

-- Location: IOIBUF_X8_Y0_N1
\D[0]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(0),
	o => \D[0]~input_o\);

-- Location: IOIBUF_X26_Y0_N8
\D[2]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(2),
	o => \D[2]~input_o\);

-- Location: LCCOMB_X14_Y1_N24
\Mux6~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Mux6~0_combout\ = (\D[3]~input_o\ & (\D[0]~input_o\ & (\D[1]~input_o\ $ (\D[2]~input_o\)))) # (!\D[3]~input_o\ & (!\D[1]~input_o\ & (\D[0]~input_o\ $ (\D[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[3]~input_o\,
	datab => \D[1]~input_o\,
	datac => \D[0]~input_o\,
	datad => \D[2]~input_o\,
	combout => \Mux6~0_combout\);

-- Location: LCCOMB_X14_Y1_N26
\SD~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \SD~0_combout\ = ((!\BLN~input_o\ & \Mux6~0_combout\)) # (!\LTN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LTN~input_o\,
	datab => \BLN~input_o\,
	datad => \Mux6~0_combout\,
	combout => \SD~0_combout\);

-- Location: LCCOMB_X14_Y1_N20
\Mux5~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Mux5~0_combout\ = (\D[3]~input_o\ & ((\D[0]~input_o\ & (\D[1]~input_o\)) # (!\D[0]~input_o\ & ((\D[2]~input_o\))))) # (!\D[3]~input_o\ & (\D[2]~input_o\ & (\D[1]~input_o\ $ (\D[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001111010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[3]~input_o\,
	datab => \D[1]~input_o\,
	datac => \D[0]~input_o\,
	datad => \D[2]~input_o\,
	combout => \Mux5~0_combout\);

-- Location: LCCOMB_X14_Y1_N14
\SD~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \SD~1_combout\ = ((!\BLN~input_o\ & \Mux5~0_combout\)) # (!\LTN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LTN~input_o\,
	datab => \BLN~input_o\,
	datad => \Mux5~0_combout\,
	combout => \SD~1_combout\);

-- Location: LCCOMB_X14_Y1_N0
\Mux4~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Mux4~0_combout\ = (\D[3]~input_o\ & (\D[2]~input_o\ & ((\D[1]~input_o\) # (!\D[0]~input_o\)))) # (!\D[3]~input_o\ & (\D[1]~input_o\ & (!\D[0]~input_o\ & !\D[2]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[3]~input_o\,
	datab => \D[1]~input_o\,
	datac => \D[0]~input_o\,
	datad => \D[2]~input_o\,
	combout => \Mux4~0_combout\);

-- Location: LCCOMB_X14_Y1_N18
\SD~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \SD~2_combout\ = ((!\BLN~input_o\ & \Mux4~0_combout\)) # (!\LTN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LTN~input_o\,
	datab => \BLN~input_o\,
	datad => \Mux4~0_combout\,
	combout => \SD~2_combout\);

-- Location: LCCOMB_X14_Y1_N12
\Mux3~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Mux3~0_combout\ = (\D[1]~input_o\ & ((\D[0]~input_o\ & ((\D[2]~input_o\))) # (!\D[0]~input_o\ & (\D[3]~input_o\ & !\D[2]~input_o\)))) # (!\D[1]~input_o\ & (!\D[3]~input_o\ & (\D[0]~input_o\ $ (\D[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000100011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[3]~input_o\,
	datab => \D[1]~input_o\,
	datac => \D[0]~input_o\,
	datad => \D[2]~input_o\,
	combout => \Mux3~0_combout\);

-- Location: LCCOMB_X14_Y1_N6
\SD~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \SD~3_combout\ = ((!\BLN~input_o\ & \Mux3~0_combout\)) # (!\LTN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LTN~input_o\,
	datab => \BLN~input_o\,
	datad => \Mux3~0_combout\,
	combout => \SD~3_combout\);

-- Location: LCCOMB_X14_Y1_N8
\Mux2~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Mux2~0_combout\ = (\D[1]~input_o\ & (!\D[3]~input_o\ & (\D[0]~input_o\))) # (!\D[1]~input_o\ & ((\D[2]~input_o\ & (!\D[3]~input_o\)) # (!\D[2]~input_o\ & ((\D[0]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[3]~input_o\,
	datab => \D[1]~input_o\,
	datac => \D[0]~input_o\,
	datad => \D[2]~input_o\,
	combout => \Mux2~0_combout\);

-- Location: LCCOMB_X14_Y1_N10
\SD~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \SD~4_combout\ = ((!\BLN~input_o\ & \Mux2~0_combout\)) # (!\LTN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010101110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LTN~input_o\,
	datab => \BLN~input_o\,
	datac => \Mux2~0_combout\,
	combout => \SD~4_combout\);

-- Location: LCCOMB_X14_Y1_N28
\Mux1~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Mux1~0_combout\ = (\D[1]~input_o\ & (!\D[3]~input_o\ & ((\D[0]~input_o\) # (!\D[2]~input_o\)))) # (!\D[1]~input_o\ & (\D[0]~input_o\ & (\D[3]~input_o\ $ (!\D[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[3]~input_o\,
	datab => \D[1]~input_o\,
	datac => \D[0]~input_o\,
	datad => \D[2]~input_o\,
	combout => \Mux1~0_combout\);

-- Location: LCCOMB_X14_Y1_N30
\SD~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \SD~5_combout\ = ((!\BLN~input_o\ & \Mux1~0_combout\)) # (!\LTN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LTN~input_o\,
	datab => \BLN~input_o\,
	datad => \Mux1~0_combout\,
	combout => \SD~5_combout\);

-- Location: LCCOMB_X14_Y1_N16
\Mux0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Mux0~0_combout\ = (\D[3]~input_o\ & ((\D[1]~input_o\) # ((\D[0]~input_o\) # (!\D[2]~input_o\)))) # (!\D[3]~input_o\ & (\D[2]~input_o\ $ (((\D[1]~input_o\ & \D[0]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[3]~input_o\,
	datab => \D[1]~input_o\,
	datac => \D[0]~input_o\,
	datad => \D[2]~input_o\,
	combout => \Mux0~0_combout\);

-- Location: LCCOMB_X14_Y1_N2
\SD~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \SD~6_combout\ = (\LTN~input_o\ & ((\BLN~input_o\) # (\Mux0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LTN~input_o\,
	datab => \BLN~input_o\,
	datad => \Mux0~0_combout\,
	combout => \SD~6_combout\);

ww_S(0) <= \S[0]~output_o\;

ww_S(1) <= \S[1]~output_o\;

ww_S(2) <= \S[2]~output_o\;

ww_S(3) <= \S[3]~output_o\;

ww_S(4) <= \S[4]~output_o\;

ww_S(5) <= \S[5]~output_o\;

ww_S(6) <= \S[6]~output_o\;
END structure;


