-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/15/2018 11:37:43"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	LCDDisplayControl IS
    PORT (
	EN : IN std_logic;
	RW : IN std_logic;
	RS : IN std_logic;
	BL : IN std_logic;
	D : IN std_logic_vector(7 DOWNTO 0);
	LCD_EN : BUFFER std_logic;
	LCD_RW : BUFFER std_logic;
	LCD_RS : BUFFER std_logic;
	LCD_BL : BUFFER std_logic;
	LED_BL : BUFFER std_logic;
	LED_EN : BUFFER std_logic;
	LED_RW : BUFFER std_logic;
	LED_RS : BUFFER std_logic;
	LED_D : BUFFER std_logic_vector(7 DOWNTO 0);
	LL_D : BUFFER std_logic_vector(7 DOWNTO 0);
	LCD_D : BUFFER std_logic_vector(7 DOWNTO 0)
	);
END LCDDisplayControl;

-- Design Ports Information
-- LCD_EN	=>  Location: PIN_L4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_RW	=>  Location: PIN_M1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_RS	=>  Location: PIN_M2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_BL	=>  Location: PIN_L6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_BL	=>  Location: PIN_E19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_EN	=>  Location: PIN_F21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_RW	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_RS	=>  Location: PIN_F19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[0]	=>  Location: PIN_E21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[1]	=>  Location: PIN_E22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[2]	=>  Location: PIN_E25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[3]	=>  Location: PIN_E24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[4]	=>  Location: PIN_H21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[5]	=>  Location: PIN_G20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[6]	=>  Location: PIN_G22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_D[7]	=>  Location: PIN_G21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[0]	=>  Location: PIN_J15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[1]	=>  Location: PIN_H16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[2]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[3]	=>  Location: PIN_H17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[4]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[5]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[6]	=>  Location: PIN_G16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LL_D[7]	=>  Location: PIN_H15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[0]	=>  Location: PIN_L3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[1]	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[2]	=>  Location: PIN_L2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[3]	=>  Location: PIN_K7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[4]	=>  Location: PIN_K1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[5]	=>  Location: PIN_K2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[6]	=>  Location: PIN_M3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[7]	=>  Location: PIN_M5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- EN	=>  Location: PIN_M23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RW	=>  Location: PIN_AB28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RS	=>  Location: PIN_AC28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BL	=>  Location: PIN_AC27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[0]	=>  Location: PIN_AC24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[1]	=>  Location: PIN_AB24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[2]	=>  Location: PIN_AB23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[3]	=>  Location: PIN_AA24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[4]	=>  Location: PIN_AA23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[5]	=>  Location: PIN_AA22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[6]	=>  Location: PIN_Y24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[7]	=>  Location: PIN_Y23,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF LCDDisplayControl IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_EN : std_logic;
SIGNAL ww_RW : std_logic;
SIGNAL ww_RS : std_logic;
SIGNAL ww_BL : std_logic;
SIGNAL ww_D : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_LCD_EN : std_logic;
SIGNAL ww_LCD_RW : std_logic;
SIGNAL ww_LCD_RS : std_logic;
SIGNAL ww_LCD_BL : std_logic;
SIGNAL ww_LED_BL : std_logic;
SIGNAL ww_LED_EN : std_logic;
SIGNAL ww_LED_RW : std_logic;
SIGNAL ww_LED_RS : std_logic;
SIGNAL ww_LED_D : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_LL_D : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_LCD_D : std_logic_vector(7 DOWNTO 0);
SIGNAL \LCD_D[0]~output_o\ : std_logic;
SIGNAL \LCD_D[1]~output_o\ : std_logic;
SIGNAL \LCD_D[2]~output_o\ : std_logic;
SIGNAL \LCD_D[3]~output_o\ : std_logic;
SIGNAL \LCD_D[4]~output_o\ : std_logic;
SIGNAL \LCD_D[5]~output_o\ : std_logic;
SIGNAL \LCD_D[6]~output_o\ : std_logic;
SIGNAL \LCD_D[7]~output_o\ : std_logic;
SIGNAL \LCD_EN~output_o\ : std_logic;
SIGNAL \LCD_RW~output_o\ : std_logic;
SIGNAL \LCD_RS~output_o\ : std_logic;
SIGNAL \LCD_BL~output_o\ : std_logic;
SIGNAL \LED_BL~output_o\ : std_logic;
SIGNAL \LED_EN~output_o\ : std_logic;
SIGNAL \LED_RW~output_o\ : std_logic;
SIGNAL \LED_RS~output_o\ : std_logic;
SIGNAL \LED_D[0]~output_o\ : std_logic;
SIGNAL \LED_D[1]~output_o\ : std_logic;
SIGNAL \LED_D[2]~output_o\ : std_logic;
SIGNAL \LED_D[3]~output_o\ : std_logic;
SIGNAL \LED_D[4]~output_o\ : std_logic;
SIGNAL \LED_D[5]~output_o\ : std_logic;
SIGNAL \LED_D[6]~output_o\ : std_logic;
SIGNAL \LED_D[7]~output_o\ : std_logic;
SIGNAL \LL_D[0]~output_o\ : std_logic;
SIGNAL \LL_D[1]~output_o\ : std_logic;
SIGNAL \LL_D[2]~output_o\ : std_logic;
SIGNAL \LL_D[3]~output_o\ : std_logic;
SIGNAL \LL_D[4]~output_o\ : std_logic;
SIGNAL \LL_D[5]~output_o\ : std_logic;
SIGNAL \LL_D[6]~output_o\ : std_logic;
SIGNAL \LL_D[7]~output_o\ : std_logic;
SIGNAL \D[0]~input_o\ : std_logic;
SIGNAL \RW~input_o\ : std_logic;
SIGNAL \D[1]~input_o\ : std_logic;
SIGNAL \D[2]~input_o\ : std_logic;
SIGNAL \D[3]~input_o\ : std_logic;
SIGNAL \D[4]~input_o\ : std_logic;
SIGNAL \D[5]~input_o\ : std_logic;
SIGNAL \D[6]~input_o\ : std_logic;
SIGNAL \D[7]~input_o\ : std_logic;
SIGNAL \EN~input_o\ : std_logic;
SIGNAL \RS~input_o\ : std_logic;
SIGNAL \BL~input_o\ : std_logic;
SIGNAL \LCD_D[0]~input_o\ : std_logic;
SIGNAL \s_d_out~0_combout\ : std_logic;
SIGNAL \LCD_D[1]~input_o\ : std_logic;
SIGNAL \s_d_out~1_combout\ : std_logic;
SIGNAL \LCD_D[2]~input_o\ : std_logic;
SIGNAL \s_d_out~2_combout\ : std_logic;
SIGNAL \LCD_D[3]~input_o\ : std_logic;
SIGNAL \s_d_out~3_combout\ : std_logic;
SIGNAL \LCD_D[4]~input_o\ : std_logic;
SIGNAL \s_d_out~4_combout\ : std_logic;
SIGNAL \LCD_D[5]~input_o\ : std_logic;
SIGNAL \s_d_out~5_combout\ : std_logic;
SIGNAL \LCD_D[6]~input_o\ : std_logic;
SIGNAL \s_d_out~6_combout\ : std_logic;
SIGNAL \LCD_D[7]~input_o\ : std_logic;
SIGNAL \s_d_out~7_combout\ : std_logic;
SIGNAL \ALT_INV_RW~input_o\ : std_logic;
SIGNAL \ALT_INV_EN~input_o\ : std_logic;

BEGIN

ww_EN <= EN;
ww_RW <= RW;
ww_RS <= RS;
ww_BL <= BL;
ww_D <= D;
LCD_EN <= ww_LCD_EN;
LCD_RW <= ww_LCD_RW;
LCD_RS <= ww_LCD_RS;
LCD_BL <= ww_LCD_BL;
LED_BL <= ww_LED_BL;
LED_EN <= ww_LED_EN;
LED_RW <= ww_LED_RW;
LED_RS <= ww_LED_RS;
LED_D <= ww_LED_D;
LL_D <= ww_LL_D;
LCD_D <= ww_LCD_D;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_RW~input_o\ <= NOT \RW~input_o\;
\ALT_INV_EN~input_o\ <= NOT \EN~input_o\;

-- Location: IOOBUF_X0_Y52_N16
\LCD_D[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[0]~input_o\,
	oe => \ALT_INV_RW~input_o\,
	devoe => ww_devoe,
	o => \LCD_D[0]~output_o\);

-- Location: IOOBUF_X0_Y44_N9
\LCD_D[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[1]~input_o\,
	oe => \ALT_INV_RW~input_o\,
	devoe => ww_devoe,
	o => \LCD_D[1]~output_o\);

-- Location: IOOBUF_X0_Y44_N2
\LCD_D[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[2]~input_o\,
	oe => \ALT_INV_RW~input_o\,
	devoe => ww_devoe,
	o => \LCD_D[2]~output_o\);

-- Location: IOOBUF_X0_Y49_N9
\LCD_D[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[3]~input_o\,
	oe => \ALT_INV_RW~input_o\,
	devoe => ww_devoe,
	o => \LCD_D[3]~output_o\);

-- Location: IOOBUF_X0_Y54_N9
\LCD_D[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[4]~input_o\,
	oe => \ALT_INV_RW~input_o\,
	devoe => ww_devoe,
	o => \LCD_D[4]~output_o\);

-- Location: IOOBUF_X0_Y55_N23
\LCD_D[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[5]~input_o\,
	oe => \ALT_INV_RW~input_o\,
	devoe => ww_devoe,
	o => \LCD_D[5]~output_o\);

-- Location: IOOBUF_X0_Y51_N16
\LCD_D[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[6]~input_o\,
	oe => \ALT_INV_RW~input_o\,
	devoe => ww_devoe,
	o => \LCD_D[6]~output_o\);

-- Location: IOOBUF_X0_Y47_N2
\LCD_D[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[7]~input_o\,
	oe => \ALT_INV_RW~input_o\,
	devoe => ww_devoe,
	o => \LCD_D[7]~output_o\);

-- Location: IOOBUF_X0_Y52_N2
\LCD_EN~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_EN~input_o\,
	devoe => ww_devoe,
	o => \LCD_EN~output_o\);

-- Location: IOOBUF_X0_Y44_N23
\LCD_RW~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \RW~input_o\,
	devoe => ww_devoe,
	o => \LCD_RW~output_o\);

-- Location: IOOBUF_X0_Y44_N16
\LCD_RS~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \RS~input_o\,
	devoe => ww_devoe,
	o => \LCD_RS~output_o\);

-- Location: IOOBUF_X0_Y47_N23
\LCD_BL~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \BL~input_o\,
	devoe => ww_devoe,
	o => \LCD_BL~output_o\);

-- Location: IOOBUF_X94_Y73_N9
\LED_BL~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \BL~input_o\,
	devoe => ww_devoe,
	o => \LED_BL~output_o\);

-- Location: IOOBUF_X107_Y73_N16
\LED_EN~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_EN~input_o\,
	devoe => ww_devoe,
	o => \LED_EN~output_o\);

-- Location: IOOBUF_X69_Y73_N16
\LED_RW~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \RW~input_o\,
	devoe => ww_devoe,
	o => \LED_RW~output_o\);

-- Location: IOOBUF_X94_Y73_N2
\LED_RS~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \RS~input_o\,
	devoe => ww_devoe,
	o => \LED_RS~output_o\);

-- Location: IOOBUF_X107_Y73_N9
\LED_D[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[0]~input_o\,
	devoe => ww_devoe,
	o => \LED_D[0]~output_o\);

-- Location: IOOBUF_X111_Y73_N9
\LED_D[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[1]~input_o\,
	devoe => ww_devoe,
	o => \LED_D[1]~output_o\);

-- Location: IOOBUF_X83_Y73_N2
\LED_D[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[2]~input_o\,
	devoe => ww_devoe,
	o => \LED_D[2]~output_o\);

-- Location: IOOBUF_X85_Y73_N23
\LED_D[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[3]~input_o\,
	devoe => ww_devoe,
	o => \LED_D[3]~output_o\);

-- Location: IOOBUF_X72_Y73_N16
\LED_D[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[4]~input_o\,
	devoe => ww_devoe,
	o => \LED_D[4]~output_o\);

-- Location: IOOBUF_X74_Y73_N16
\LED_D[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[5]~input_o\,
	devoe => ww_devoe,
	o => \LED_D[5]~output_o\);

-- Location: IOOBUF_X72_Y73_N23
\LED_D[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[6]~input_o\,
	devoe => ww_devoe,
	o => \LED_D[6]~output_o\);

-- Location: IOOBUF_X74_Y73_N23
\LED_D[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \D[7]~input_o\,
	devoe => ww_devoe,
	o => \LED_D[7]~output_o\);

-- Location: IOOBUF_X60_Y73_N23
\LL_D[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_d_out~0_combout\,
	devoe => ww_devoe,
	o => \LL_D[0]~output_o\);

-- Location: IOOBUF_X65_Y73_N23
\LL_D[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_d_out~1_combout\,
	devoe => ww_devoe,
	o => \LL_D[1]~output_o\);

-- Location: IOOBUF_X65_Y73_N16
\LL_D[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_d_out~2_combout\,
	devoe => ww_devoe,
	o => \LL_D[2]~output_o\);

-- Location: IOOBUF_X67_Y73_N9
\LL_D[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_d_out~3_combout\,
	devoe => ww_devoe,
	o => \LL_D[3]~output_o\);

-- Location: IOOBUF_X58_Y73_N2
\LL_D[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_d_out~4_combout\,
	devoe => ww_devoe,
	o => \LL_D[4]~output_o\);

-- Location: IOOBUF_X65_Y73_N9
\LL_D[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_d_out~5_combout\,
	devoe => ww_devoe,
	o => \LL_D[5]~output_o\);

-- Location: IOOBUF_X67_Y73_N2
\LL_D[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_d_out~6_combout\,
	devoe => ww_devoe,
	o => \LL_D[6]~output_o\);

-- Location: IOOBUF_X60_Y73_N16
\LL_D[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_d_out~7_combout\,
	devoe => ww_devoe,
	o => \LL_D[7]~output_o\);

-- Location: IOIBUF_X115_Y4_N15
\D[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(0),
	o => \D[0]~input_o\);

-- Location: IOIBUF_X115_Y17_N1
\RW~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_RW,
	o => \RW~input_o\);

-- Location: IOIBUF_X115_Y5_N15
\D[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(1),
	o => \D[1]~input_o\);

-- Location: IOIBUF_X115_Y7_N15
\D[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(2),
	o => \D[2]~input_o\);

-- Location: IOIBUF_X115_Y9_N22
\D[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(3),
	o => \D[3]~input_o\);

-- Location: IOIBUF_X115_Y10_N8
\D[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(4),
	o => \D[4]~input_o\);

-- Location: IOIBUF_X115_Y6_N15
\D[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(5),
	o => \D[5]~input_o\);

-- Location: IOIBUF_X115_Y13_N1
\D[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(6),
	o => \D[6]~input_o\);

-- Location: IOIBUF_X115_Y14_N8
\D[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(7),
	o => \D[7]~input_o\);

-- Location: IOIBUF_X115_Y40_N8
\EN~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_EN,
	o => \EN~input_o\);

-- Location: IOIBUF_X115_Y14_N1
\RS~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_RS,
	o => \RS~input_o\);

-- Location: IOIBUF_X115_Y15_N8
\BL~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BL,
	o => \BL~input_o\);

-- Location: IOIBUF_X0_Y52_N15
\LCD_D[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(0),
	o => \LCD_D[0]~input_o\);

-- Location: LCCOMB_X31_Y65_N16
\s_d_out~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d_out~0_combout\ = (\RW~input_o\ & (\LCD_D[0]~input_o\)) # (!\RW~input_o\ & ((\D[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LCD_D[0]~input_o\,
	datac => \RW~input_o\,
	datad => \D[0]~input_o\,
	combout => \s_d_out~0_combout\);

-- Location: IOIBUF_X0_Y44_N8
\LCD_D[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(1),
	o => \LCD_D[1]~input_o\);

-- Location: LCCOMB_X31_Y65_N10
\s_d_out~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d_out~1_combout\ = (\RW~input_o\ & (\LCD_D[1]~input_o\)) # (!\RW~input_o\ & ((\D[1]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD_D[1]~input_o\,
	datab => \D[1]~input_o\,
	datac => \RW~input_o\,
	combout => \s_d_out~1_combout\);

-- Location: IOIBUF_X0_Y44_N1
\LCD_D[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(2),
	o => \LCD_D[2]~input_o\);

-- Location: LCCOMB_X31_Y65_N12
\s_d_out~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d_out~2_combout\ = (\RW~input_o\ & (\LCD_D[2]~input_o\)) # (!\RW~input_o\ & ((\D[2]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD_D[2]~input_o\,
	datab => \RW~input_o\,
	datac => \D[2]~input_o\,
	combout => \s_d_out~2_combout\);

-- Location: IOIBUF_X0_Y49_N8
\LCD_D[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(3),
	o => \LCD_D[3]~input_o\);

-- Location: LCCOMB_X31_Y65_N14
\s_d_out~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d_out~3_combout\ = (\RW~input_o\ & (\LCD_D[3]~input_o\)) # (!\RW~input_o\ & ((\D[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD_D[3]~input_o\,
	datab => \D[3]~input_o\,
	datac => \RW~input_o\,
	combout => \s_d_out~3_combout\);

-- Location: IOIBUF_X0_Y54_N8
\LCD_D[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(4),
	o => \LCD_D[4]~input_o\);

-- Location: LCCOMB_X31_Y65_N24
\s_d_out~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d_out~4_combout\ = (\RW~input_o\ & ((\LCD_D[4]~input_o\))) # (!\RW~input_o\ & (\D[4]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \D[4]~input_o\,
	datac => \RW~input_o\,
	datad => \LCD_D[4]~input_o\,
	combout => \s_d_out~4_combout\);

-- Location: IOIBUF_X0_Y55_N22
\LCD_D[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(5),
	o => \LCD_D[5]~input_o\);

-- Location: LCCOMB_X31_Y65_N26
\s_d_out~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d_out~5_combout\ = (\RW~input_o\ & ((\LCD_D[5]~input_o\))) # (!\RW~input_o\ & (\D[5]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[5]~input_o\,
	datab => \LCD_D[5]~input_o\,
	datac => \RW~input_o\,
	combout => \s_d_out~5_combout\);

-- Location: IOIBUF_X0_Y51_N15
\LCD_D[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(6),
	o => \LCD_D[6]~input_o\);

-- Location: LCCOMB_X31_Y65_N4
\s_d_out~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d_out~6_combout\ = (\RW~input_o\ & ((\LCD_D[6]~input_o\))) # (!\RW~input_o\ & (\D[6]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \D[6]~input_o\,
	datac => \RW~input_o\,
	datad => \LCD_D[6]~input_o\,
	combout => \s_d_out~6_combout\);

-- Location: IOIBUF_X0_Y47_N1
\LCD_D[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => LCD_D(7),
	o => \LCD_D[7]~input_o\);

-- Location: LCCOMB_X31_Y65_N30
\s_d_out~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_d_out~7_combout\ = (\RW~input_o\ & (\LCD_D[7]~input_o\)) # (!\RW~input_o\ & ((\D[7]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LCD_D[7]~input_o\,
	datab => \RW~input_o\,
	datac => \D[7]~input_o\,
	combout => \s_d_out~7_combout\);

ww_LCD_EN <= \LCD_EN~output_o\;

ww_LCD_RW <= \LCD_RW~output_o\;

ww_LCD_RS <= \LCD_RS~output_o\;

ww_LCD_BL <= \LCD_BL~output_o\;

ww_LED_BL <= \LED_BL~output_o\;

ww_LED_EN <= \LED_EN~output_o\;

ww_LED_RW <= \LED_RW~output_o\;

ww_LED_RS <= \LED_RS~output_o\;

ww_LED_D(0) <= \LED_D[0]~output_o\;

ww_LED_D(1) <= \LED_D[1]~output_o\;

ww_LED_D(2) <= \LED_D[2]~output_o\;

ww_LED_D(3) <= \LED_D[3]~output_o\;

ww_LED_D(4) <= \LED_D[4]~output_o\;

ww_LED_D(5) <= \LED_D[5]~output_o\;

ww_LED_D(6) <= \LED_D[6]~output_o\;

ww_LED_D(7) <= \LED_D[7]~output_o\;

ww_LL_D(0) <= \LL_D[0]~output_o\;

ww_LL_D(1) <= \LL_D[1]~output_o\;

ww_LL_D(2) <= \LL_D[2]~output_o\;

ww_LL_D(3) <= \LL_D[3]~output_o\;

ww_LL_D(4) <= \LL_D[4]~output_o\;

ww_LL_D(5) <= \LL_D[5]~output_o\;

ww_LL_D(6) <= \LL_D[6]~output_o\;

ww_LL_D(7) <= \LL_D[7]~output_o\;

LCD_D(0) <= \LCD_D[0]~output_o\;

LCD_D(1) <= \LCD_D[1]~output_o\;

LCD_D(2) <= \LCD_D[2]~output_o\;

LCD_D(3) <= \LCD_D[3]~output_o\;

LCD_D(4) <= \LCD_D[4]~output_o\;

LCD_D(5) <= \LCD_D[5]~output_o\;

LCD_D(6) <= \LCD_D[6]~output_o\;

LCD_D(7) <= \LCD_D[7]~output_o\;
END structure;


