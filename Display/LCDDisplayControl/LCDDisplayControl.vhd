LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY LCDDisplayControl IS
	PORT (
		EN, RW, RS, BL				:	IN		std_logic;
		D								:	IN		std_logic_vector(7 downto 0);
		LCD_EN, LCD_RW, LCD_RS	:	OUT	std_logic;
		LCD_BL, LED_BL				:	OUT	std_logic;
		LED_EN, LED_RW, LED_RS	:	OUT	std_logic;
		LED_D, LL_D					:	OUT	std_logic_vector(7 downto 0);
		LCD_D							:	INOUT std_logic_vector(7 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF LCDDisplayControl IS

	SIGNAL s_en, s_rw, s_rs, s_bl		:	std_logic	:= '0';

	SIGNAL s_d_in, s_d_out				:	std_logic_vector(7 downto 0) := (Others => '0');
	
BEGIN

	s_en <= NOT EN;
	s_rw <= RW;
	s_rs <= RS;
	s_bl <= BL;
	
	s_d_in <= D;
	
	LED_EN <= s_en;
	LED_RW <= s_rw;
	LED_RS <= s_rs;
	LED_BL <= s_bl;
	
	LED_D <= s_d_in;
	
	LCD_EN <= s_en;
	LCD_RW <= s_rw;
	LCD_RS <= s_rs;
	LCD_BL <= s_bl;
	LCD_D  <= s_d_in when (s_rw = '0') else (Others => 'Z');
	s_d_out <= LCD_D when (s_rw = '1') else s_d_in;
	
	LL_D <= s_d_out;
	
END Behaviour;