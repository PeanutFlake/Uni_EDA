LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Counter IS
	GENERIC (
		Limit		:	Integer	:= 255
	);
	PORT (
		CLK, nRST	:	IN		std_logic;
		Start			:	IN		integer range 0 to Limit-1;
		Ov				:	OUT	std_logic;
		Q				:	OUT	integer range 0 to Limit
	);
END ENTITY;

ARCHITECTURE Behaviour OF Counter IS

	SIGNAL s_q				:	integer range 0 to Limit 	:= 0;
	SIGNAL s_ov 			:	std_logic						:= '0';

	
BEGIN

	Count : PROCESS(CLK, nRST, Start)
		VARIABLE c	:	integer	range 0 to Limit	:= Start;
	BEGIN
		if ( nRST = '1' ) then
			if ( rising_edge(CLK) ) then
				if ( c = Limit ) then
					c := c;
					s_ov <= '1';
				else
					c := c+1;
				end if;
			else
				c := c;
			end if;
		elsif ( nRST = '0' ) then
			c := Start;
			s_ov <= '0';
		else
			c := c;
		end if;
		s_q <= c;
	END PROCESS Count;

	Q <= s_q;
	Ov <= s_ov;
	
END Behaviour;