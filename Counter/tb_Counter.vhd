LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_Counter IS
END ENTITY;

ARCHITECTURE Testbench OF tb_Counter IS

	COMPONENT Counter IS
		GENERIC (
			Limit		:	Integer	:= 255
		);
		PORT (
			CLK, RST	:	IN		std_logic;
			Ov			:	OUT	std_logic;
			Q			:	OUT	integer range 0 to 255
		);
	END COMPONENT;

	SIGNAL s_clk, s_rst, s_ov	:	std_logic	:= '0';
	SIGNAL s_q						:	integer		:= 0;
	
BEGIN

	DUT	:	Counter
	GENERIC MAP (255)
	PORT MAP (s_clk, s_rst, s_ov, s_q);

	s_clk <= NOT s_clk AFTER 10ns;
	
	s_rst <= '1' AFTER 5500ns, '0' AFTER 5550ns;
	
END Testbench;