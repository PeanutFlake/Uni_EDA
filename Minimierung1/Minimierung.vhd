ENTITY Minimierung IS
PORT (
	x, y, z, v		:	IN bit;
	f					:	OUT bit
);
END ENTITY;

ARCHITECTURE Minimierung_DMF OF Minimierung IS

BEGIN

	f <= (z AND v) OR (z AND NOT v AND NOT y) OR (y AND NOT z AND NOT x) OR (x AND y AND NOT v AND NOT z);

END Minimierung_DMF;

ARCHITECTURE Minimierung_KMF OF Minimierung IS

BEGIN

	f <= (z OR y) AND (NOT z OR v OR NOT y) AND (z OR NOT v OR NOT y OR NOT x);

END Minimierung_KMF;