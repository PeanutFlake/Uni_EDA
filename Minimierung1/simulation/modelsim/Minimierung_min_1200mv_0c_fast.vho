-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/10/2018 08:36:29"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
LIBRARY STD;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;
USE STD.STANDARD.ALL;

ENTITY 	Minimierung IS
    PORT (
	x : IN std_logic;
	y : IN std_logic;
	z : IN std_logic;
	v : IN std_logic;
	f : OUT STD.STANDARD.bit
	);
END Minimierung;

-- Design Ports Information
-- f	=>  Location: PIN_L7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- z	=>  Location: PIN_L6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- v	=>  Location: PIN_N3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- y	=>  Location: PIN_M5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- x	=>  Location: PIN_N4,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF Minimierung IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_x : std_logic;
SIGNAL ww_y : std_logic;
SIGNAL ww_z : std_logic;
SIGNAL ww_v : std_logic;
SIGNAL ww_f : std_logic;
SIGNAL \f~output_o\ : std_logic;
SIGNAL \v~input_o\ : std_logic;
SIGNAL \y~input_o\ : std_logic;
SIGNAL \x~input_o\ : std_logic;
SIGNAL \z~input_o\ : std_logic;
SIGNAL \f~0_combout\ : std_logic;

BEGIN

ww_x <= x;
ww_y <= y;
ww_z <= z;
ww_v <= v;
f <= IEEE.STD_LOGIC_1164.TO_BIT(ww_f);
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: IOOBUF_X0_Y47_N16
\f~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \f~0_combout\,
	devoe => ww_devoe,
	o => \f~output_o\);

-- Location: IOIBUF_X0_Y46_N22
\v~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_v,
	o => \v~input_o\);

-- Location: IOIBUF_X0_Y47_N1
\y~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_y,
	o => \y~input_o\);

-- Location: IOIBUF_X0_Y46_N15
\x~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_x,
	o => \x~input_o\);

-- Location: IOIBUF_X0_Y47_N22
\z~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_z,
	o => \z~input_o\);

-- Location: LCCOMB_X1_Y47_N16
\f~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \f~0_combout\ = (\v~input_o\ & ((\z~input_o\) # ((\y~input_o\ & !\x~input_o\)))) # (!\v~input_o\ & (\y~input_o\ $ (((\z~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101101001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \v~input_o\,
	datab => \y~input_o\,
	datac => \x~input_o\,
	datad => \z~input_o\,
	combout => \f~0_combout\);

ww_f <= \f~output_o\;
END structure;


