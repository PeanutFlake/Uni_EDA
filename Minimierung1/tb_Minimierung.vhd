ENTITY tb_Minimierung IS
END ENTITY;

ARCHITECTURE Testbench OF tb_Minimierung IS

	COMPONENT Minimierung IS
		PORT (
			x, y, z, v		:	IN bit;
			f					:	OUT bit
		);
	END COMPONENT;

	SIGNAL s_x, s_y, s_z, s_v		:	bit := '0';
	SIGNAL s_df, s_kf					:	bit := '0';
	SIGNAL s_test						:	bit := '0';
	
BEGIN

	DUTD : entity work.Minimierung(Minimierung_DMF)
	PORT MAP(
		s_x, s_y, s_z, s_v, s_df
	);
	
	DUTK : entity work.Minimierung(Minimierung_KMF)
	PORT MAP(
		s_x, s_y, s_z, s_v, s_kf
	);

	s_x <= NOT s_x AFTER 160ns;
	s_y <= NOT s_y AFTER 80ns;
	s_z <= NOT s_z AFTER 40ns;
	s_v <= NOT s_v AFTER 20ns;
	
	s_test <= s_kf XNOR s_df;
	
END Testbench;