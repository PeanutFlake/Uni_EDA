-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/14/2018 12:03:38"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	ImpulsdetektorX IS
    PORT (
	X : IN std_logic;
	CLK : IN std_logic;
	RST : IN std_logic;
	Y : BUFFER std_logic;
	Err : BUFFER std_logic
	);
END ImpulsdetektorX;

-- Design Ports Information
-- Y	=>  Location: PIN_E21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Err	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- X	=>  Location: PIN_AB28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_M23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RST	=>  Location: PIN_R24,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF ImpulsdetektorX IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_X : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_RST : std_logic;
SIGNAL ww_Y : std_logic;
SIGNAL ww_Err : std_logic;
SIGNAL \X~input_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \RST~input_o\ : std_logic;
SIGNAL \Y~output_o\ : std_logic;
SIGNAL \Err~output_o\ : std_logic;
SIGNAL \Ausgang:cnt[0]~0_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \Add0~0_combout\ : std_logic;
SIGNAL \Ausgang:cnt[1]~combout\ : std_logic;
SIGNAL \Equal1~0_combout\ : std_logic;

BEGIN

ww_X <= X;
ww_CLK <= CLK;
ww_RST <= RST;
Y <= ww_Y;
Err <= ww_Err;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: IOOBUF_X107_Y73_N9
\Y~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Equal1~0_combout\,
	devoe => ww_devoe,
	o => \Y~output_o\);

-- Location: IOOBUF_X69_Y73_N16
\Err~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \Err~output_o\);

-- Location: LCCOMB_X108_Y72_N10
\Ausgang:cnt[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Ausgang:cnt[0]~0_combout\ = (\Ausgang:cnt[1]~combout\) # (!\Ausgang:cnt[0]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Ausgang:cnt[1]~combout\,
	datad => \Ausgang:cnt[0]~0_combout\,
	combout => \Ausgang:cnt[0]~0_combout\);

-- Location: LCCOMB_X108_Y72_N6
\Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = (\Ausgang:cnt[1]~combout\ & \Ausgang:cnt[0]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Ausgang:cnt[1]~combout\,
	datad => \Ausgang:cnt[0]~0_combout\,
	combout => \Equal0~0_combout\);

-- Location: LCCOMB_X108_Y72_N12
\Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~0_combout\ = \Ausgang:cnt[1]~combout\ $ (\Ausgang:cnt[0]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Ausgang:cnt[1]~combout\,
	datad => \Ausgang:cnt[0]~0_combout\,
	combout => \Add0~0_combout\);

-- Location: LCCOMB_X108_Y72_N24
\Ausgang:cnt[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \Ausgang:cnt[1]~combout\ = (\Equal0~0_combout\ & ((\Ausgang:cnt[1]~combout\))) # (!\Equal0~0_combout\ & (\Add0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~0_combout\,
	datac => \Add0~0_combout\,
	datad => \Ausgang:cnt[1]~combout\,
	combout => \Ausgang:cnt[1]~combout\);

-- Location: LCCOMB_X108_Y72_N0
\Equal1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal1~0_combout\ = (!\Ausgang:cnt[1]~combout\ & \Ausgang:cnt[0]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Ausgang:cnt[1]~combout\,
	datad => \Ausgang:cnt[0]~0_combout\,
	combout => \Equal1~0_combout\);

-- Location: IOIBUF_X115_Y17_N1
\X~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_X,
	o => \X~input_o\);

-- Location: IOIBUF_X115_Y40_N8
\CLK~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: IOIBUF_X115_Y35_N22
\RST~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_RST,
	o => \RST~input_o\);

ww_Y <= \Y~output_o\;

ww_Err <= \Err~output_o\;
END structure;


