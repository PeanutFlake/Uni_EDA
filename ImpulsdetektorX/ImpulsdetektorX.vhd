LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY ImpulsdetektorX IS
	GENERIC (
		Pulsewidth	:	integer := 3
	);
	PORT (
		X, CLK, RST	:	IN		std_logic;
		Y, Err		:	OUT	std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF ImpulsdetektorX IS

	TYPE State IS (A, E, N);
	
	SIGNAL s_stat 	: 	State 		:= A;
	SIGNAL s_e		:	std_logic	:= '0';
	
BEGIN

	Zustand: PROCESS(CLK, RST, X)
	BEGIN
		if ( RST = '1' ) then
			s_e <= '0';
			s_stat <= A;
		else
			if ( rising_edge(CLK) ) then
				if ( X = '0' ) then
					s_stat <= N;
				elsif ( X = '1' ) then
					s_stat <= E;
				else
					s_stat <= A;
					s_e <= '1';
				end if;
			end if;
		end if;
		Err <= s_e;
	END PROCESS Zustand;
	
	
	Ausgang: PROCESS(s_stat, s_e)
		VARIABLE cnt	:	integer range 0 to Pulsewidth := 0;
		VARIABLE last	:	State := A;
		VARIABLE dy		:	std_logic	:= '0';
	BEGIN
		if ( s_e = '0' ) then
			if ( s_stat = last ) then
				if ( cnt = Pulsewidth ) then
					cnt := cnt;
				else
					cnt := cnt + 1;
				end if;
			else
				cnt := 0;
			end if;
			if ( cnt = Pulsewidth-1 ) then
				dy := '1';
			else
				dy := '0';
			end if;
			last := s_stat;
		else
			last := A;
			dy := '0';
			cnt := 0;
		end if;
		Y <= dy;
	END PROCESS Ausgang;


END Behaviour;