LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_ImpulsdetektorX IS
END ENTITY;

ARCHITECTURE Testbench OF tb_ImpulsdetektorX IS

	COMPONENT ImpulsdetektorX IS
		GENERIC (
			Pulsewidth	:	integer := 3
		);
		PORT (
			X, CLK, RST	:	IN		std_logic;
			Y, Err		:	OUT	std_logic
		);
	END COMPONENT;

	SIGNAL s_x, s_clk, s_rst, s_y, s_err	:	std_logic	:= '0';
	
BEGIN

	DUT	:	ImpulsdetektorX
	GENERIC MAP (
		Pulsewidth => 3
	)
	PORT MAP (
		s_x, s_clk, s_rst, s_y, s_err
	);
	
	s_clk <= NOT s_clk AFTER 20ns;
	
	s_x <= '1' AFTER 60ns, '0' AFTER 130ns, '1' AFTER 280ns;

	s_rst <= '1' AFTER 400ns, '0' AFTER 430ns;

END Testbench;