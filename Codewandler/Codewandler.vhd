LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Codewandler IS
	PORT (
		D				:			std_logic_vector(3 downto 0);
		Q				:	OUT	std_logic_vector(3 downto 0)
	);
END ENTITY;

ARCHITECTURE BCD2Aiken OF Codewandler IS

BEGIN

Q(0) <= (NOT D(0) AND D(3)) OR (D(0) AND NOT D(1) AND NOT D(2) AND D(3));
Q(1) <= (NOT D(0) AND ((NOT D(1) AND D(2) AND NOT D(3)) OR (NOT D(1) AND D(2) AND D(3)) OR (D(1) AND NOT D(2) AND D(3)))) OR (D(0) AND NOT D(1) AND NOT D(2));
Q(2) <= ((NOT D(0) AND D(1)) AND ((D(2) XNOR D(3)) OR (D(2) AND NOT D(3)))) OR (D(0) AND NOT D(1) AND NOT D(2));
Q(3) <= ((NOT D(0) AND D(1)) AND ((D(2) XOR D(3)) OR (D(2) AND D(3)))) OR (D(0) AND NOT D(1) AND NOT D(2));

END BCD2Aiken;

ARCHITECTURE BCD2Grey OF Codewandler IS

BEGIN

END BCD2Grey;