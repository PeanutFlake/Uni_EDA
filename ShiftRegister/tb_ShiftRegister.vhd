LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_ShiftRegister IS
END ENTITY;

ARCHITECTURE Testbench OF tb_ShiftRegister IS

	COMPONENT ShiftRegister IS
		PORT (
			in_clk					:	IN std_logic;
			in_up, in_down			:	IN std_logic;
			out_value				:	OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;

	SIGNAL	s_clk, s_up, s_down	:	std_logic := '0';
	SIGNAL	s_val						:	std_logic_vector(3 downto 0) := '0000';
	
BEGIN

	DUT : ShiftRegister
	PORT MAP(
		in_clk	<=	s_clk,
		in_up		<=	s_up,
		in_down	<=	s_down,
		out_value<=	s_val
	);
	
	s_clk <= NOT s_clk AFTER 10ns;

	in_up <= '1' AFTER 5ns '0' AFTER 15ns;
	

END Testbench;