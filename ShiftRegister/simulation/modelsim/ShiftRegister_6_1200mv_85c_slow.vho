-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/09/2018 15:35:59"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	ShiftRegister IS
    PORT (
	in_clk : IN std_logic;
	in_up : IN std_logic;
	in_down : IN std_logic;
	out_value : OUT std_logic_vector(3 DOWNTO 0)
	);
END ShiftRegister;

-- Design Ports Information
-- out_value[0]	=>  Location: PIN_N8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out_value[1]	=>  Location: PIN_K9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out_value[2]	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- out_value[3]	=>  Location: PIN_N9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in_up	=>  Location: PIN_M9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in_down	=>  Location: PIN_L9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- in_clk	=>  Location: PIN_J7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF ShiftRegister IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_in_clk : std_logic;
SIGNAL ww_in_up : std_logic;
SIGNAL ww_in_down : std_logic;
SIGNAL ww_out_value : std_logic_vector(3 DOWNTO 0);
SIGNAL \in_clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \out_value[0]~output_o\ : std_logic;
SIGNAL \out_value[1]~output_o\ : std_logic;
SIGNAL \out_value[2]~output_o\ : std_logic;
SIGNAL \out_value[3]~output_o\ : std_logic;
SIGNAL \in_clk~input_o\ : std_logic;
SIGNAL \in_clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \in_up~input_o\ : std_logic;
SIGNAL \in_down~input_o\ : std_logic;
SIGNAL \vb[0]~3_combout\ : std_logic;
SIGNAL \vb[1]~5_cout\ : std_logic;
SIGNAL \vb[1]~6_combout\ : std_logic;
SIGNAL \vb[0]~8_combout\ : std_logic;
SIGNAL \vb[1]~7\ : std_logic;
SIGNAL \vb[2]~9_combout\ : std_logic;
SIGNAL \vb[2]~10\ : std_logic;
SIGNAL \vb[3]~11_combout\ : std_logic;
SIGNAL vb : std_logic_vector(3 DOWNTO 0);

BEGIN

ww_in_clk <= in_clk;
ww_in_up <= in_up;
ww_in_down <= in_down;
out_value <= ww_out_value;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\in_clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \in_clk~input_o\);

-- Location: IOOBUF_X20_Y0_N9
\out_value[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => vb(0),
	devoe => ww_devoe,
	o => \out_value[0]~output_o\);

-- Location: IOOBUF_X22_Y0_N2
\out_value[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => vb(1),
	devoe => ww_devoe,
	o => \out_value[1]~output_o\);

-- Location: IOOBUF_X22_Y0_N9
\out_value[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => vb(2),
	devoe => ww_devoe,
	o => \out_value[2]~output_o\);

-- Location: IOOBUF_X20_Y0_N2
\out_value[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => vb(3),
	devoe => ww_devoe,
	o => \out_value[3]~output_o\);

-- Location: IOIBUF_X16_Y0_N15
\in_clk~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_in_clk,
	o => \in_clk~input_o\);

-- Location: CLKCTRL_G17
\in_clk~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \in_clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \in_clk~inputclkctrl_outclk\);

-- Location: IOIBUF_X24_Y0_N1
\in_up~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_in_up,
	o => \in_up~input_o\);

-- Location: IOIBUF_X24_Y0_N8
\in_down~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_in_down,
	o => \in_down~input_o\);

-- Location: LCCOMB_X23_Y1_N0
\vb[0]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \vb[0]~3_combout\ = vb(0) $ (((\in_up~input_o\) # (\in_down~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \in_up~input_o\,
	datac => vb(0),
	datad => \in_down~input_o\,
	combout => \vb[0]~3_combout\);

-- Location: FF_X23_Y1_N1
\vb[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \in_clk~inputclkctrl_outclk\,
	d => \vb[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => vb(0));

-- Location: LCCOMB_X23_Y1_N6
\vb[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \vb[1]~5_cout\ = CARRY(vb(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => vb(0),
	datad => VCC,
	cout => \vb[1]~5_cout\);

-- Location: LCCOMB_X23_Y1_N8
\vb[1]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \vb[1]~6_combout\ = (\in_up~input_o\ & ((vb(1) & (!\vb[1]~5_cout\)) # (!vb(1) & ((\vb[1]~5_cout\) # (GND))))) # (!\in_up~input_o\ & ((vb(1) & (\vb[1]~5_cout\ & VCC)) # (!vb(1) & (!\vb[1]~5_cout\))))
-- \vb[1]~7\ = CARRY((\in_up~input_o\ & ((!\vb[1]~5_cout\) # (!vb(1)))) # (!\in_up~input_o\ & (!vb(1) & !\vb[1]~5_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \in_up~input_o\,
	datab => vb(1),
	datad => VCC,
	cin => \vb[1]~5_cout\,
	combout => \vb[1]~6_combout\,
	cout => \vb[1]~7\);

-- Location: LCCOMB_X23_Y1_N30
\vb[0]~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \vb[0]~8_combout\ = (\in_up~input_o\) # (\in_down~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \in_up~input_o\,
	datad => \in_down~input_o\,
	combout => \vb[0]~8_combout\);

-- Location: FF_X23_Y1_N9
\vb[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \in_clk~inputclkctrl_outclk\,
	d => \vb[1]~6_combout\,
	ena => \vb[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => vb(1));

-- Location: LCCOMB_X23_Y1_N10
\vb[2]~9\ : cycloneiv_lcell_comb
-- Equation(s):
-- \vb[2]~9_combout\ = ((vb(2) $ (\in_up~input_o\ $ (\vb[1]~7\)))) # (GND)
-- \vb[2]~10\ = CARRY((vb(2) & ((!\vb[1]~7\) # (!\in_up~input_o\))) # (!vb(2) & (!\in_up~input_o\ & !\vb[1]~7\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => vb(2),
	datab => \in_up~input_o\,
	datad => VCC,
	cin => \vb[1]~7\,
	combout => \vb[2]~9_combout\,
	cout => \vb[2]~10\);

-- Location: FF_X23_Y1_N11
\vb[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \in_clk~inputclkctrl_outclk\,
	d => \vb[2]~9_combout\,
	ena => \vb[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => vb(2));

-- Location: LCCOMB_X23_Y1_N12
\vb[3]~11\ : cycloneiv_lcell_comb
-- Equation(s):
-- \vb[3]~11_combout\ = \in_up~input_o\ $ (\vb[2]~10\ $ (!vb(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \in_up~input_o\,
	datad => vb(3),
	cin => \vb[2]~10\,
	combout => \vb[3]~11_combout\);

-- Location: FF_X23_Y1_N13
\vb[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \in_clk~inputclkctrl_outclk\,
	d => \vb[3]~11_combout\,
	ena => \vb[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => vb(3));

ww_out_value(0) <= \out_value[0]~output_o\;

ww_out_value(1) <= \out_value[1]~output_o\;

ww_out_value(2) <= \out_value[2]~output_o\;

ww_out_value(3) <= \out_value[3]~output_o\;
END structure;


