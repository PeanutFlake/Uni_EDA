LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY ShiftRegister IS
	PORT (
		in_clk					:	IN std_logic;
		in_up, in_down			:	IN std_logic;
		out_value				:	OUT std_logic_vector(3 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF ShiftRegister IS


	SIGNAL v_buffer			: integer range 0 to 15 := 0;

BEGIN

	PROCESS (in_clk, in_up, in_down)
		VARIABLE vb				: integer range 0 to 15 := 0;
	BEGIN
		if (in_clk'event AND in_clk = '1') then
			if ( in_up = '1' ) then
				vb := vb + 1;
			elsif ( in_down = '1' ) then
				vb := vb - 1;
			end if;
		end if;
		v_buffer <= vb;
	END PROCESS;
	
	out_value <= std_logic_vector(to_unsigned(v_buffer, out_value'length));

END Behaviour;