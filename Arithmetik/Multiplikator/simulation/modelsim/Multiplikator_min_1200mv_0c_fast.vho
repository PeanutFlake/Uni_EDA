-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "07/16/2018 14:37:15"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	Multiplikator IS
    PORT (
	nRST : IN std_logic;
	CLK : IN std_logic;
	EN : IN std_logic;
	X : IN std_logic_vector(3 DOWNTO 0);
	Y : IN std_logic_vector(3 DOWNTO 0);
	O : BUFFER std_logic_vector(7 DOWNTO 0);
	State : BUFFER std_logic_vector(3 DOWNTO 0)
	);
END Multiplikator;

-- Design Ports Information
-- O[0]	=>  Location: PIN_E21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- O[1]	=>  Location: PIN_E22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- O[2]	=>  Location: PIN_E25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- O[3]	=>  Location: PIN_E24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- O[4]	=>  Location: PIN_H21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- O[5]	=>  Location: PIN_G20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- O[6]	=>  Location: PIN_G22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- O[7]	=>  Location: PIN_G21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- State[0]	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- State[1]	=>  Location: PIN_F19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- State[2]	=>  Location: PIN_E19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- State[3]	=>  Location: PIN_F21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- X[0]	=>  Location: PIN_AA23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- EN	=>  Location: PIN_AB28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_M21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- nRST	=>  Location: PIN_M23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- X[1]	=>  Location: PIN_AA22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- X[2]	=>  Location: PIN_Y24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- X[3]	=>  Location: PIN_Y23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Y[0]	=>  Location: PIN_AC24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Y[1]	=>  Location: PIN_AB24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Y[2]	=>  Location: PIN_AB23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Y[3]	=>  Location: PIN_AA24,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF Multiplikator IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_nRST : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_EN : std_logic;
SIGNAL ww_X : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_Y : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_O : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_State : std_logic_vector(3 DOWNTO 0);
SIGNAL \s_state_current.Init~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \State~0clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \s_count~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \O[0]~output_o\ : std_logic;
SIGNAL \O[1]~output_o\ : std_logic;
SIGNAL \O[2]~output_o\ : std_logic;
SIGNAL \O[3]~output_o\ : std_logic;
SIGNAL \O[4]~output_o\ : std_logic;
SIGNAL \O[5]~output_o\ : std_logic;
SIGNAL \O[6]~output_o\ : std_logic;
SIGNAL \O[7]~output_o\ : std_logic;
SIGNAL \State[0]~output_o\ : std_logic;
SIGNAL \State[1]~output_o\ : std_logic;
SIGNAL \State[2]~output_o\ : std_logic;
SIGNAL \State[3]~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \EN~input_o\ : std_logic;
SIGNAL \Selector0~0_combout\ : std_logic;
SIGNAL \s_state_current.Init~feeder_combout\ : std_logic;
SIGNAL \nRST~input_o\ : std_logic;
SIGNAL \s_state_current.Init~q\ : std_logic;
SIGNAL \s_state_current.Step~q\ : std_logic;
SIGNAL \Selector0~1_combout\ : std_logic;
SIGNAL \s_state_current.Calc~q\ : std_logic;
SIGNAL \s_count~combout\ : std_logic;
SIGNAL \s_count~clkctrl_outclk\ : std_logic;
SIGNAL \CNT|Count:c[1]~0_combout\ : std_logic;
SIGNAL \CNT|Count:c[1]~q\ : std_logic;
SIGNAL \CNT|Count:c[0]~0_combout\ : std_logic;
SIGNAL \CNT|Count:c[0]~q\ : std_logic;
SIGNAL \CNT|Count:c[2]~0_combout\ : std_logic;
SIGNAL \CNT|Count:c[2]~q\ : std_logic;
SIGNAL \s_state_current.Output~0_combout\ : std_logic;
SIGNAL \s_state_current.Output~q\ : std_logic;
SIGNAL \X[0]~input_o\ : std_logic;
SIGNAL \X[1]~input_o\ : std_logic;
SIGNAL \X[2]~input_o\ : std_logic;
SIGNAL \Selector3~0_combout\ : std_logic;
SIGNAL \State~0_combout\ : std_logic;
SIGNAL \AUSGANG:A[0]~combout\ : std_logic;
SIGNAL \Selector13~0_combout\ : std_logic;
SIGNAL \Y[1]~input_o\ : std_logic;
SIGNAL \s_state_current.Init~clkctrl_outclk\ : std_logic;
SIGNAL \AUSGANG:M[6]~combout\ : std_logic;
SIGNAL \Y[3]~input_o\ : std_logic;
SIGNAL \AUSGANG:M[8]~combout\ : std_logic;
SIGNAL \Y[2]~input_o\ : std_logic;
SIGNAL \AUSGANG:M[7]~combout\ : std_logic;
SIGNAL \Y[0]~input_o\ : std_logic;
SIGNAL \AUSGANG:M[5]~combout\ : std_logic;
SIGNAL \Add1~1\ : std_logic;
SIGNAL \Add1~3\ : std_logic;
SIGNAL \Add1~5\ : std_logic;
SIGNAL \Add1~6_combout\ : std_logic;
SIGNAL \Add0~1\ : std_logic;
SIGNAL \Add0~3\ : std_logic;
SIGNAL \Add0~5\ : std_logic;
SIGNAL \Add0~6_combout\ : std_logic;
SIGNAL \Selector12~0_combout\ : std_logic;
SIGNAL \Selector13~1_combout\ : std_logic;
SIGNAL \AUSGANG:A[8]~combout\ : std_logic;
SIGNAL \Selector8~3_combout\ : std_logic;
SIGNAL \Selector11~0_combout\ : std_logic;
SIGNAL \Selector11~1_combout\ : std_logic;
SIGNAL \State~0clkctrl_outclk\ : std_logic;
SIGNAL \AUSGANG:A[7]~combout\ : std_logic;
SIGNAL \Add0~4_combout\ : std_logic;
SIGNAL \Add1~4_combout\ : std_logic;
SIGNAL \Selector10~0_combout\ : std_logic;
SIGNAL \Selector10~1_combout\ : std_logic;
SIGNAL \AUSGANG:A[6]~combout\ : std_logic;
SIGNAL \Add0~2_combout\ : std_logic;
SIGNAL \Add1~2_combout\ : std_logic;
SIGNAL \Selector9~0_combout\ : std_logic;
SIGNAL \Selector9~1_combout\ : std_logic;
SIGNAL \AUSGANG:A[5]~combout\ : std_logic;
SIGNAL \Selector8~1_combout\ : std_logic;
SIGNAL \Add0~0_combout\ : std_logic;
SIGNAL \Add1~0_combout\ : std_logic;
SIGNAL \Selector8~0_combout\ : std_logic;
SIGNAL \X[3]~input_o\ : std_logic;
SIGNAL \Selector8~2_combout\ : std_logic;
SIGNAL \AUSGANG:A[4]~combout\ : std_logic;
SIGNAL \Selector7~0_combout\ : std_logic;
SIGNAL \AUSGANG:A[3]~combout\ : std_logic;
SIGNAL \Selector6~0_combout\ : std_logic;
SIGNAL \AUSGANG:A[2]~combout\ : std_logic;
SIGNAL \Selector5~0_combout\ : std_logic;
SIGNAL \AUSGANG:A[1]~combout\ : std_logic;
SIGNAL \s_o~0_combout\ : std_logic;
SIGNAL \s_o~1_combout\ : std_logic;
SIGNAL \s_o~2_combout\ : std_logic;
SIGNAL \s_o~3_combout\ : std_logic;
SIGNAL \s_o~4_combout\ : std_logic;
SIGNAL \s_o~5_combout\ : std_logic;
SIGNAL \s_o~6_combout\ : std_logic;
SIGNAL \s_o~7_combout\ : std_logic;
SIGNAL \State~1_combout\ : std_logic;

BEGIN

ww_nRST <= nRST;
ww_CLK <= CLK;
ww_EN <= EN;
ww_X <= X;
ww_Y <= Y;
O <= ww_O;
State <= ww_State;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\s_state_current.Init~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \s_state_current.Init~q\);

\State~0clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \State~0_combout\);

\s_count~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \s_count~combout\);

-- Location: IOOBUF_X107_Y73_N9
\O[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_o~0_combout\,
	devoe => ww_devoe,
	o => \O[0]~output_o\);

-- Location: IOOBUF_X111_Y73_N9
\O[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_o~1_combout\,
	devoe => ww_devoe,
	o => \O[1]~output_o\);

-- Location: IOOBUF_X83_Y73_N2
\O[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_o~2_combout\,
	devoe => ww_devoe,
	o => \O[2]~output_o\);

-- Location: IOOBUF_X85_Y73_N23
\O[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_o~3_combout\,
	devoe => ww_devoe,
	o => \O[3]~output_o\);

-- Location: IOOBUF_X72_Y73_N16
\O[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_o~4_combout\,
	devoe => ww_devoe,
	o => \O[4]~output_o\);

-- Location: IOOBUF_X74_Y73_N16
\O[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_o~5_combout\,
	devoe => ww_devoe,
	o => \O[5]~output_o\);

-- Location: IOOBUF_X72_Y73_N23
\O[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_o~6_combout\,
	devoe => ww_devoe,
	o => \O[6]~output_o\);

-- Location: IOOBUF_X74_Y73_N23
\O[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_o~7_combout\,
	devoe => ww_devoe,
	o => \O[7]~output_o\);

-- Location: IOOBUF_X69_Y73_N16
\State[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \State~0_combout\,
	devoe => ww_devoe,
	o => \State[0]~output_o\);

-- Location: IOOBUF_X94_Y73_N2
\State[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \State~1_combout\,
	devoe => ww_devoe,
	o => \State[1]~output_o\);

-- Location: IOOBUF_X94_Y73_N9
\State[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_state_current.Step~q\,
	devoe => ww_devoe,
	o => \State[2]~output_o\);

-- Location: IOOBUF_X107_Y73_N16
\State[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_state_current.Output~q\,
	devoe => ww_devoe,
	o => \State[3]~output_o\);

-- Location: IOIBUF_X115_Y53_N15
\CLK~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: IOIBUF_X115_Y17_N1
\EN~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_EN,
	o => \EN~input_o\);

-- Location: LCCOMB_X114_Y37_N8
\Selector0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector0~0_combout\ = (\s_state_current.Output~q\ & \s_state_current.Calc~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Output~q\,
	datac => \s_state_current.Calc~q\,
	combout => \Selector0~0_combout\);

-- Location: LCCOMB_X114_Y37_N18
\s_state_current.Init~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_current.Init~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \s_state_current.Init~feeder_combout\);

-- Location: IOIBUF_X115_Y40_N8
\nRST~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_nRST,
	o => \nRST~input_o\);

-- Location: FF_X114_Y37_N19
\s_state_current.Init\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \s_state_current.Init~feeder_combout\,
	clrn => \nRST~input_o\,
	ena => \EN~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Init~q\);

-- Location: FF_X114_Y37_N7
\s_state_current.Step\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	asdata => \s_state_current.Calc~q\,
	clrn => \nRST~input_o\,
	sload => VCC,
	ena => \EN~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Step~q\);

-- Location: LCCOMB_X114_Y37_N4
\Selector0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector0~1_combout\ = (\Selector0~0_combout\) # (((!\CNT|Count:c[2]~q\ & \s_state_current.Step~q\)) # (!\s_state_current.Init~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111110101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector0~0_combout\,
	datab => \CNT|Count:c[2]~q\,
	datac => \s_state_current.Init~q\,
	datad => \s_state_current.Step~q\,
	combout => \Selector0~1_combout\);

-- Location: FF_X114_Y37_N5
\s_state_current.Calc\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \Selector0~1_combout\,
	clrn => \nRST~input_o\,
	ena => \EN~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Calc~q\);

-- Location: LCCOMB_X114_Y37_N22
s_count : cycloneive_lcell_comb
-- Equation(s):
-- \s_count~combout\ = (\s_state_current.Output~q\ & ((\s_count~combout\))) # (!\s_state_current.Output~q\ & (\s_state_current.Calc~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Calc~q\,
	datac => \s_count~combout\,
	datad => \s_state_current.Output~q\,
	combout => \s_count~combout\);

-- Location: CLKCTRL_G9
\s_count~clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \s_count~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \s_count~clkctrl_outclk\);

-- Location: LCCOMB_X113_Y37_N10
\CNT|Count:c[1]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \CNT|Count:c[1]~0_combout\ = \CNT|Count:c[1]~q\ $ (\CNT|Count:c[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \CNT|Count:c[1]~q\,
	datad => \CNT|Count:c[0]~q\,
	combout => \CNT|Count:c[1]~0_combout\);

-- Location: FF_X113_Y37_N11
\CNT|Count:c[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \s_count~clkctrl_outclk\,
	d => \CNT|Count:c[1]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CNT|Count:c[1]~q\);

-- Location: LCCOMB_X113_Y37_N28
\CNT|Count:c[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \CNT|Count:c[0]~0_combout\ = (!\CNT|Count:c[0]~q\ & ((\CNT|Count:c[1]~q\) # (!\CNT|Count:c[2]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \CNT|Count:c[2]~q\,
	datac => \CNT|Count:c[0]~q\,
	datad => \CNT|Count:c[1]~q\,
	combout => \CNT|Count:c[0]~0_combout\);

-- Location: FF_X113_Y37_N29
\CNT|Count:c[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \s_count~clkctrl_outclk\,
	d => \CNT|Count:c[0]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CNT|Count:c[0]~q\);

-- Location: LCCOMB_X113_Y37_N16
\CNT|Count:c[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \CNT|Count:c[2]~0_combout\ = \CNT|Count:c[2]~q\ $ (((\CNT|Count:c[0]~q\ & \CNT|Count:c[1]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \CNT|Count:c[0]~q\,
	datac => \CNT|Count:c[2]~q\,
	datad => \CNT|Count:c[1]~q\,
	combout => \CNT|Count:c[2]~0_combout\);

-- Location: FF_X113_Y37_N17
\CNT|Count:c[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \s_count~clkctrl_outclk\,
	d => \CNT|Count:c[2]~0_combout\,
	clrn => \nRST~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CNT|Count:c[2]~q\);

-- Location: LCCOMB_X114_Y37_N0
\s_state_current.Output~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_state_current.Output~0_combout\ = (\s_state_current.Output~q\) # ((\EN~input_o\ & (\CNT|Count:c[2]~q\ & \s_state_current.Step~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \EN~input_o\,
	datab => \s_state_current.Output~q\,
	datac => \CNT|Count:c[2]~q\,
	datad => \s_state_current.Step~q\,
	combout => \s_state_current.Output~0_combout\);

-- Location: FF_X114_Y37_N13
\s_state_current.Output\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	asdata => \s_state_current.Output~0_combout\,
	clrn => \nRST~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_state_current.Output~q\);

-- Location: IOIBUF_X115_Y10_N8
\X[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_X(0),
	o => \X[0]~input_o\);

-- Location: IOIBUF_X115_Y6_N15
\X[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_X(1),
	o => \X[1]~input_o\);

-- Location: IOIBUF_X115_Y13_N1
\X[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_X(2),
	o => \X[2]~input_o\);

-- Location: LCCOMB_X114_Y37_N16
\Selector3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector3~0_combout\ = (\s_state_current.Calc~q\ & \AUSGANG:A[1]~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Calc~q\,
	datac => \AUSGANG:A[1]~combout\,
	combout => \Selector3~0_combout\);

-- Location: LCCOMB_X114_Y37_N20
\State~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \State~0_combout\ = (\s_state_current.Calc~q\) # (!\s_state_current.Init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \s_state_current.Calc~q\,
	datad => \s_state_current.Init~q\,
	combout => \State~0_combout\);

-- Location: LCCOMB_X114_Y37_N10
\AUSGANG:A[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \AUSGANG:A[0]~combout\ = (\State~0_combout\ & ((\Selector3~0_combout\))) # (!\State~0_combout\ & (\AUSGANG:A[0]~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[0]~combout\,
	datab => \Selector3~0_combout\,
	datad => \State~0_combout\,
	combout => \AUSGANG:A[0]~combout\);

-- Location: LCCOMB_X112_Y37_N20
\Selector13~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector13~0_combout\ = (\s_state_current.Calc~q\ & (\AUSGANG:A[0]~combout\ $ (\AUSGANG:A[1]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[0]~combout\,
	datac => \AUSGANG:A[1]~combout\,
	datad => \s_state_current.Calc~q\,
	combout => \Selector13~0_combout\);

-- Location: IOIBUF_X115_Y5_N15
\Y[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Y(1),
	o => \Y[1]~input_o\);

-- Location: CLKCTRL_G7
\s_state_current.Init~clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \s_state_current.Init~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \s_state_current.Init~clkctrl_outclk\);

-- Location: LCCOMB_X111_Y37_N20
\AUSGANG:M[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \AUSGANG:M[6]~combout\ = (GLOBAL(\s_state_current.Init~clkctrl_outclk\) & (\AUSGANG:M[6]~combout\)) # (!GLOBAL(\s_state_current.Init~clkctrl_outclk\) & ((\Y[1]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \AUSGANG:M[6]~combout\,
	datac => \Y[1]~input_o\,
	datad => \s_state_current.Init~clkctrl_outclk\,
	combout => \AUSGANG:M[6]~combout\);

-- Location: IOIBUF_X115_Y9_N22
\Y[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Y(3),
	o => \Y[3]~input_o\);

-- Location: LCCOMB_X112_Y37_N4
\AUSGANG:M[8]\ : cycloneive_lcell_comb
-- Equation(s):
-- \AUSGANG:M[8]~combout\ = (GLOBAL(\s_state_current.Init~clkctrl_outclk\) & ((\AUSGANG:M[8]~combout\))) # (!GLOBAL(\s_state_current.Init~clkctrl_outclk\) & (\Y[3]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Y[3]~input_o\,
	datac => \AUSGANG:M[8]~combout\,
	datad => \s_state_current.Init~clkctrl_outclk\,
	combout => \AUSGANG:M[8]~combout\);

-- Location: IOIBUF_X115_Y7_N15
\Y[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Y(2),
	o => \Y[2]~input_o\);

-- Location: LCCOMB_X111_Y37_N10
\AUSGANG:M[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \AUSGANG:M[7]~combout\ = (GLOBAL(\s_state_current.Init~clkctrl_outclk\) & ((\AUSGANG:M[7]~combout\))) # (!GLOBAL(\s_state_current.Init~clkctrl_outclk\) & (\Y[2]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Y[2]~input_o\,
	datac => \AUSGANG:M[7]~combout\,
	datad => \s_state_current.Init~clkctrl_outclk\,
	combout => \AUSGANG:M[7]~combout\);

-- Location: IOIBUF_X115_Y4_N15
\Y[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Y(0),
	o => \Y[0]~input_o\);

-- Location: LCCOMB_X111_Y37_N22
\AUSGANG:M[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \AUSGANG:M[5]~combout\ = (GLOBAL(\s_state_current.Init~clkctrl_outclk\) & ((\AUSGANG:M[5]~combout\))) # (!GLOBAL(\s_state_current.Init~clkctrl_outclk\) & (\Y[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Y[0]~input_o\,
	datac => \AUSGANG:M[5]~combout\,
	datad => \s_state_current.Init~clkctrl_outclk\,
	combout => \AUSGANG:M[5]~combout\);

-- Location: LCCOMB_X111_Y37_N2
\Add1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add1~0_combout\ = (\AUSGANG:A[5]~combout\ & (\AUSGANG:M[5]~combout\ $ (VCC))) # (!\AUSGANG:A[5]~combout\ & (\AUSGANG:M[5]~combout\ & VCC))
-- \Add1~1\ = CARRY((\AUSGANG:A[5]~combout\ & \AUSGANG:M[5]~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[5]~combout\,
	datab => \AUSGANG:M[5]~combout\,
	datad => VCC,
	combout => \Add1~0_combout\,
	cout => \Add1~1\);

-- Location: LCCOMB_X111_Y37_N4
\Add1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add1~2_combout\ = (\AUSGANG:M[6]~combout\ & ((\AUSGANG:A[6]~combout\ & (\Add1~1\ & VCC)) # (!\AUSGANG:A[6]~combout\ & (!\Add1~1\)))) # (!\AUSGANG:M[6]~combout\ & ((\AUSGANG:A[6]~combout\ & (!\Add1~1\)) # (!\AUSGANG:A[6]~combout\ & ((\Add1~1\) # (GND)))))
-- \Add1~3\ = CARRY((\AUSGANG:M[6]~combout\ & (!\AUSGANG:A[6]~combout\ & !\Add1~1\)) # (!\AUSGANG:M[6]~combout\ & ((!\Add1~1\) # (!\AUSGANG:A[6]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:M[6]~combout\,
	datab => \AUSGANG:A[6]~combout\,
	datad => VCC,
	cin => \Add1~1\,
	combout => \Add1~2_combout\,
	cout => \Add1~3\);

-- Location: LCCOMB_X111_Y37_N6
\Add1~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add1~4_combout\ = ((\AUSGANG:A[7]~combout\ $ (\AUSGANG:M[7]~combout\ $ (!\Add1~3\)))) # (GND)
-- \Add1~5\ = CARRY((\AUSGANG:A[7]~combout\ & ((\AUSGANG:M[7]~combout\) # (!\Add1~3\))) # (!\AUSGANG:A[7]~combout\ & (\AUSGANG:M[7]~combout\ & !\Add1~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[7]~combout\,
	datab => \AUSGANG:M[7]~combout\,
	datad => VCC,
	cin => \Add1~3\,
	combout => \Add1~4_combout\,
	cout => \Add1~5\);

-- Location: LCCOMB_X111_Y37_N8
\Add1~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add1~6_combout\ = \AUSGANG:A[8]~combout\ $ (\Add1~5\ $ (\AUSGANG:M[8]~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \AUSGANG:A[8]~combout\,
	datad => \AUSGANG:M[8]~combout\,
	cin => \Add1~5\,
	combout => \Add1~6_combout\);

-- Location: LCCOMB_X111_Y37_N12
\Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~0_combout\ = (\AUSGANG:A[5]~combout\ & ((GND) # (!\AUSGANG:M[5]~combout\))) # (!\AUSGANG:A[5]~combout\ & (\AUSGANG:M[5]~combout\ $ (GND)))
-- \Add0~1\ = CARRY((\AUSGANG:A[5]~combout\) # (!\AUSGANG:M[5]~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[5]~combout\,
	datab => \AUSGANG:M[5]~combout\,
	datad => VCC,
	combout => \Add0~0_combout\,
	cout => \Add0~1\);

-- Location: LCCOMB_X111_Y37_N14
\Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~2_combout\ = (\AUSGANG:M[6]~combout\ & ((\AUSGANG:A[6]~combout\ & (!\Add0~1\)) # (!\AUSGANG:A[6]~combout\ & ((\Add0~1\) # (GND))))) # (!\AUSGANG:M[6]~combout\ & ((\AUSGANG:A[6]~combout\ & (\Add0~1\ & VCC)) # (!\AUSGANG:A[6]~combout\ & (!\Add0~1\))))
-- \Add0~3\ = CARRY((\AUSGANG:M[6]~combout\ & ((!\Add0~1\) # (!\AUSGANG:A[6]~combout\))) # (!\AUSGANG:M[6]~combout\ & (!\AUSGANG:A[6]~combout\ & !\Add0~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:M[6]~combout\,
	datab => \AUSGANG:A[6]~combout\,
	datad => VCC,
	cin => \Add0~1\,
	combout => \Add0~2_combout\,
	cout => \Add0~3\);

-- Location: LCCOMB_X111_Y37_N16
\Add0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~4_combout\ = ((\AUSGANG:A[7]~combout\ $ (\AUSGANG:M[7]~combout\ $ (\Add0~3\)))) # (GND)
-- \Add0~5\ = CARRY((\AUSGANG:A[7]~combout\ & ((!\Add0~3\) # (!\AUSGANG:M[7]~combout\))) # (!\AUSGANG:A[7]~combout\ & (!\AUSGANG:M[7]~combout\ & !\Add0~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[7]~combout\,
	datab => \AUSGANG:M[7]~combout\,
	datad => VCC,
	cin => \Add0~3\,
	combout => \Add0~4_combout\,
	cout => \Add0~5\);

-- Location: LCCOMB_X111_Y37_N18
\Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \Add0~6_combout\ = \AUSGANG:M[8]~combout\ $ (\Add0~5\ $ (!\AUSGANG:A[8]~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:M[8]~combout\,
	datad => \AUSGANG:A[8]~combout\,
	cin => \Add0~5\,
	combout => \Add0~6_combout\);

-- Location: LCCOMB_X111_Y37_N24
\Selector12~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector12~0_combout\ = (\s_state_current.Calc~q\ & ((\AUSGANG:A[0]~combout\ & (\Add1~6_combout\)) # (!\AUSGANG:A[0]~combout\ & ((\Add0~6_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \s_state_current.Calc~q\,
	datab => \AUSGANG:A[0]~combout\,
	datac => \Add1~6_combout\,
	datad => \Add0~6_combout\,
	combout => \Selector12~0_combout\);

-- Location: LCCOMB_X114_Y37_N2
\Selector13~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector13~1_combout\ = ((\s_state_current.Calc~q\ & (\AUSGANG:A[0]~combout\ $ (\AUSGANG:A[1]~combout\)))) # (!\s_state_current.Init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111101100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[0]~combout\,
	datab => \s_state_current.Init~q\,
	datac => \AUSGANG:A[1]~combout\,
	datad => \s_state_current.Calc~q\,
	combout => \Selector13~1_combout\);

-- Location: LCCOMB_X111_Y37_N28
\AUSGANG:A[8]\ : cycloneive_lcell_comb
-- Equation(s):
-- \AUSGANG:A[8]~combout\ = (\Selector13~1_combout\ & (\Selector12~0_combout\)) # (!\Selector13~1_combout\ & ((\AUSGANG:A[8]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Selector12~0_combout\,
	datac => \Selector13~1_combout\,
	datad => \AUSGANG:A[8]~combout\,
	combout => \AUSGANG:A[8]~combout\);

-- Location: LCCOMB_X112_Y37_N30
\Selector8~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector8~3_combout\ = (\s_state_current.Calc~q\ & (\AUSGANG:A[0]~combout\ $ (!\AUSGANG:A[1]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000010010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[0]~combout\,
	datab => \AUSGANG:A[1]~combout\,
	datac => \s_state_current.Calc~q\,
	combout => \Selector8~3_combout\);

-- Location: LCCOMB_X112_Y37_N12
\Selector11~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector11~0_combout\ = (\Selector13~0_combout\ & ((\AUSGANG:A[1]~combout\ & (\Add0~6_combout\)) # (!\AUSGANG:A[1]~combout\ & ((\Add1~6_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[1]~combout\,
	datab => \Selector13~0_combout\,
	datac => \Add0~6_combout\,
	datad => \Add1~6_combout\,
	combout => \Selector11~0_combout\);

-- Location: LCCOMB_X112_Y37_N14
\Selector11~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector11~1_combout\ = (\Selector11~0_combout\) # ((\AUSGANG:A[8]~combout\ & \Selector8~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[8]~combout\,
	datab => \Selector8~3_combout\,
	datac => \Selector11~0_combout\,
	combout => \Selector11~1_combout\);

-- Location: CLKCTRL_G5
\State~0clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \State~0clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \State~0clkctrl_outclk\);

-- Location: LCCOMB_X111_Y37_N26
\AUSGANG:A[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \AUSGANG:A[7]~combout\ = (GLOBAL(\State~0clkctrl_outclk\) & ((\Selector11~1_combout\))) # (!GLOBAL(\State~0clkctrl_outclk\) & (\AUSGANG:A[7]~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[7]~combout\,
	datac => \Selector11~1_combout\,
	datad => \State~0clkctrl_outclk\,
	combout => \AUSGANG:A[7]~combout\);

-- Location: LCCOMB_X110_Y37_N28
\Selector10~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector10~0_combout\ = (\Selector13~0_combout\ & ((\AUSGANG:A[1]~combout\ & (\Add0~4_combout\)) # (!\AUSGANG:A[1]~combout\ & ((\Add1~4_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector13~0_combout\,
	datab => \Add0~4_combout\,
	datac => \AUSGANG:A[1]~combout\,
	datad => \Add1~4_combout\,
	combout => \Selector10~0_combout\);

-- Location: LCCOMB_X110_Y37_N18
\Selector10~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector10~1_combout\ = (\Selector10~0_combout\) # ((\AUSGANG:A[7]~combout\ & \Selector8~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector10~0_combout\,
	datab => \AUSGANG:A[7]~combout\,
	datad => \Selector8~3_combout\,
	combout => \Selector10~1_combout\);

-- Location: LCCOMB_X111_Y37_N0
\AUSGANG:A[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \AUSGANG:A[6]~combout\ = (GLOBAL(\State~0clkctrl_outclk\) & (\Selector10~1_combout\)) # (!GLOBAL(\State~0clkctrl_outclk\) & ((\AUSGANG:A[6]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector10~1_combout\,
	datac => \AUSGANG:A[6]~combout\,
	datad => \State~0clkctrl_outclk\,
	combout => \AUSGANG:A[6]~combout\);

-- Location: LCCOMB_X110_Y37_N24
\Selector9~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector9~0_combout\ = (\Selector13~0_combout\ & ((\AUSGANG:A[1]~combout\ & (\Add0~2_combout\)) # (!\AUSGANG:A[1]~combout\ & ((\Add1~2_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[1]~combout\,
	datab => \Selector13~0_combout\,
	datac => \Add0~2_combout\,
	datad => \Add1~2_combout\,
	combout => \Selector9~0_combout\);

-- Location: LCCOMB_X110_Y37_N30
\Selector9~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector9~1_combout\ = (\Selector9~0_combout\) # ((\AUSGANG:A[6]~combout\ & \Selector8~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector9~0_combout\,
	datac => \AUSGANG:A[6]~combout\,
	datad => \Selector8~3_combout\,
	combout => \Selector9~1_combout\);

-- Location: LCCOMB_X111_Y37_N30
\AUSGANG:A[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \AUSGANG:A[5]~combout\ = (GLOBAL(\State~0clkctrl_outclk\) & (\Selector9~1_combout\)) # (!GLOBAL(\State~0clkctrl_outclk\) & ((\AUSGANG:A[5]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector9~1_combout\,
	datac => \AUSGANG:A[5]~combout\,
	datad => \State~0clkctrl_outclk\,
	combout => \AUSGANG:A[5]~combout\);

-- Location: LCCOMB_X110_Y37_N0
\Selector8~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector8~1_combout\ = (\AUSGANG:A[5]~combout\ & (\AUSGANG:A[0]~combout\ $ (!\AUSGANG:A[1]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \AUSGANG:A[0]~combout\,
	datac => \AUSGANG:A[1]~combout\,
	datad => \AUSGANG:A[5]~combout\,
	combout => \Selector8~1_combout\);

-- Location: LCCOMB_X110_Y37_N6
\Selector8~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector8~0_combout\ = (\Selector13~0_combout\ & ((\AUSGANG:A[1]~combout\ & (\Add0~0_combout\)) # (!\AUSGANG:A[1]~combout\ & ((\Add1~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[1]~combout\,
	datab => \Add0~0_combout\,
	datac => \Selector13~0_combout\,
	datad => \Add1~0_combout\,
	combout => \Selector8~0_combout\);

-- Location: IOIBUF_X115_Y14_N8
\X[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_X(3),
	o => \X[3]~input_o\);

-- Location: LCCOMB_X110_Y37_N22
\Selector8~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector8~2_combout\ = (\Selector8~0_combout\) # ((\s_state_current.Calc~q\ & (\Selector8~1_combout\)) # (!\s_state_current.Calc~q\ & ((\X[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector8~1_combout\,
	datab => \Selector8~0_combout\,
	datac => \X[3]~input_o\,
	datad => \s_state_current.Calc~q\,
	combout => \Selector8~2_combout\);

-- Location: LCCOMB_X110_Y37_N14
\AUSGANG:A[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \AUSGANG:A[4]~combout\ = (GLOBAL(\State~0clkctrl_outclk\) & (\Selector8~2_combout\)) # (!GLOBAL(\State~0clkctrl_outclk\) & ((\AUSGANG:A[4]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector8~2_combout\,
	datac => \AUSGANG:A[4]~combout\,
	datad => \State~0clkctrl_outclk\,
	combout => \AUSGANG:A[4]~combout\);

-- Location: LCCOMB_X110_Y37_N12
\Selector7~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector7~0_combout\ = (\s_state_current.Calc~q\ & ((\AUSGANG:A[4]~combout\))) # (!\s_state_current.Calc~q\ & (\X[2]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \X[2]~input_o\,
	datac => \AUSGANG:A[4]~combout\,
	datad => \s_state_current.Calc~q\,
	combout => \Selector7~0_combout\);

-- Location: LCCOMB_X110_Y37_N4
\AUSGANG:A[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \AUSGANG:A[3]~combout\ = (GLOBAL(\State~0clkctrl_outclk\) & (\Selector7~0_combout\)) # (!GLOBAL(\State~0clkctrl_outclk\) & ((\AUSGANG:A[3]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector7~0_combout\,
	datac => \AUSGANG:A[3]~combout\,
	datad => \State~0clkctrl_outclk\,
	combout => \AUSGANG:A[3]~combout\);

-- Location: LCCOMB_X114_Y37_N28
\Selector6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector6~0_combout\ = (\s_state_current.Calc~q\ & ((\AUSGANG:A[3]~combout\))) # (!\s_state_current.Calc~q\ & (\X[1]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \X[1]~input_o\,
	datac => \AUSGANG:A[3]~combout\,
	datad => \s_state_current.Calc~q\,
	combout => \Selector6~0_combout\);

-- Location: LCCOMB_X114_Y37_N24
\AUSGANG:A[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \AUSGANG:A[2]~combout\ = (GLOBAL(\State~0clkctrl_outclk\) & (\Selector6~0_combout\)) # (!GLOBAL(\State~0clkctrl_outclk\) & ((\AUSGANG:A[2]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Selector6~0_combout\,
	datac => \AUSGANG:A[2]~combout\,
	datad => \State~0clkctrl_outclk\,
	combout => \AUSGANG:A[2]~combout\);

-- Location: LCCOMB_X114_Y37_N26
\Selector5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Selector5~0_combout\ = (\s_state_current.Calc~q\ & ((\AUSGANG:A[2]~combout\))) # (!\s_state_current.Calc~q\ & (\X[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \X[0]~input_o\,
	datab => \s_state_current.Calc~q\,
	datad => \AUSGANG:A[2]~combout\,
	combout => \Selector5~0_combout\);

-- Location: LCCOMB_X114_Y37_N14
\AUSGANG:A[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \AUSGANG:A[1]~combout\ = (\State~0_combout\ & (\Selector5~0_combout\)) # (!\State~0_combout\ & ((\AUSGANG:A[1]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector5~0_combout\,
	datac => \AUSGANG:A[1]~combout\,
	datad => \State~0_combout\,
	combout => \AUSGANG:A[1]~combout\);

-- Location: LCCOMB_X110_Y37_N20
\s_o~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_o~0_combout\ = (\s_state_current.Output~q\ & \AUSGANG:A[1]~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \s_state_current.Output~q\,
	datad => \AUSGANG:A[1]~combout\,
	combout => \s_o~0_combout\);

-- Location: LCCOMB_X114_Y37_N12
\s_o~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_o~1_combout\ = (\AUSGANG:A[2]~combout\ & \s_state_current.Output~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[2]~combout\,
	datac => \s_state_current.Output~q\,
	combout => \s_o~1_combout\);

-- Location: LCCOMB_X110_Y37_N10
\s_o~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_o~2_combout\ = (\s_state_current.Output~q\ & \AUSGANG:A[3]~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Output~q\,
	datac => \AUSGANG:A[3]~combout\,
	combout => \s_o~2_combout\);

-- Location: LCCOMB_X114_Y37_N6
\s_o~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_o~3_combout\ = (\AUSGANG:A[4]~combout\ & \s_state_current.Output~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AUSGANG:A[4]~combout\,
	datad => \s_state_current.Output~q\,
	combout => \s_o~3_combout\);

-- Location: LCCOMB_X110_Y37_N16
\s_o~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_o~4_combout\ = (\AUSGANG:A[5]~combout\ & \s_state_current.Output~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \AUSGANG:A[5]~combout\,
	datac => \s_state_current.Output~q\,
	combout => \s_o~4_combout\);

-- Location: LCCOMB_X110_Y37_N2
\s_o~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_o~5_combout\ = (\s_state_current.Output~q\ & \AUSGANG:A[6]~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_state_current.Output~q\,
	datac => \AUSGANG:A[6]~combout\,
	combout => \s_o~5_combout\);

-- Location: LCCOMB_X110_Y37_N8
\s_o~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_o~6_combout\ = (\s_state_current.Output~q\ & \AUSGANG:A[7]~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \s_state_current.Output~q\,
	datad => \AUSGANG:A[7]~combout\,
	combout => \s_o~6_combout\);

-- Location: LCCOMB_X110_Y37_N26
\s_o~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_o~7_combout\ = (\s_state_current.Output~q\ & \AUSGANG:A[8]~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \s_state_current.Output~q\,
	datad => \AUSGANG:A[8]~combout\,
	combout => \s_o~7_combout\);

-- Location: LCCOMB_X114_Y37_N30
\State~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \State~1_combout\ = (\s_state_current.Init~q\ & \s_state_current.Calc~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \s_state_current.Init~q\,
	datad => \s_state_current.Calc~q\,
	combout => \State~1_combout\);

ww_O(0) <= \O[0]~output_o\;

ww_O(1) <= \O[1]~output_o\;

ww_O(2) <= \O[2]~output_o\;

ww_O(3) <= \O[3]~output_o\;

ww_O(4) <= \O[4]~output_o\;

ww_O(5) <= \O[5]~output_o\;

ww_O(6) <= \O[6]~output_o\;

ww_O(7) <= \O[7]~output_o\;

ww_State(0) <= \State[0]~output_o\;

ww_State(1) <= \State[1]~output_o\;

ww_State(2) <= \State[2]~output_o\;

ww_State(3) <= \State[3]~output_o\;
END structure;


