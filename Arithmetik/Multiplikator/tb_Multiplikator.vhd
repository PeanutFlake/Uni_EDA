LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_Multiplikator IS
END ENTITY;

ARCHITECTURE Testbench OF tb_Multiplikator IS

	COMPONENT Multiplikator IS
		GENERIC (
			Bitsize			:	Integer	:=	4
		);
		PORT (
			nRST, CLK, EN	:	IN		std_logic;
			X, Y				:	IN		std_logic_vector(Bitsize-1 downto 0);
			O					:	OUT	std_logic_vector(2*Bitsize-1 downto 0);
			State				:	OUT	std_logic_vector(Bitsize-1 downto 0)
		);
	END COMPONENT;

	SIGNAL s_x, s_y	:	std_logic_vector(3 downto 0)	:= (Others => '0');
	SIGNAL s_o 			:	std_logic_vector(7 downto 0)	:= (Others => '0');
	SIGNAL s_rst,s_clk:	std_logic							:=	'1';
	SIGNAL s_en			:	std_logic							:=	'0';
	SIGNAL s_state		:	std_logic_vector(3 downto 0)	:= (Others => '0');
	
BEGIN

	DUT	:	Multiplikator
	GENERIC MAP (4)
	PORT MAP (
		nRST	=>	s_rst,
		CLK	=>	s_clk,
		EN		=>	s_en,
		X		=>	s_x,
		Y		=>	s_y,
		O		=>	s_o,
		State	=>	s_state
	);
	
	s_clk <= not s_clk after 20ns;
	
	s_en <= '1' after 100ns, '0' after 3000ns;
	
	s_x <= "0010" after 10ns, "0100" after 800ns, "0011" after 1600ns, "1100" after 2400ns;
	s_y <= "0010" after 10ns, "0011" after 800ns, "0100" after 1600ns, "0010" after 2400ns;
	s_rst <= '0' after 50ns, '1' after 85ns, '0' after 790ns, '1' after 820ns, '0' after 1580ns, '1' after 1620ns, '0' after 2390ns, '1' after 2420ns, '0' after 3100ns, '1' after 3200ns;

END Testbench;