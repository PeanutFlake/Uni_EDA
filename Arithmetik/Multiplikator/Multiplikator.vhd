LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY Multiplikator IS
	GENERIC (
		Bitsize			:	Integer	:=	4
	);
	PORT (
		nRST, CLK, EN	:	IN		std_logic;
		X, Y				:	IN		std_logic_vector(Bitsize-1 downto 0);
		O					:	OUT	std_logic_vector(2*Bitsize-1 downto 0);
		State				:	OUT	std_logic_vector(Bitsize-1 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF Multiplikator IS

	COMPONENT Counter IS
		GENERIC (
			Limit		:	Integer	:= 255
		);
		PORT (
			CLK, nRST	:	IN		std_logic;
			Start			:	IN		integer range 0 to Limit-1;
			Ov				:	OUT	std_logic;
			Q				:	OUT	integer range 0 to Limit
		);
	END COMPONENT;
	
	COMPONENT RegisterU4 IS
		GENERIC (
			Bitsize		:	Integer	:= 4
		);
		PORT (
			CLK, RST, SHL, SHR	:			std_logic;
			S				:			std_logic_vector (1 downto 0);
			D				:			std_logic_vector (Bitsize-1 downto 0);
			Q				:	OUT	std_logic_vector (Bitsize-1 downto 0)
		);
	END COMPONENT;
	
	TYPE	Zustand	IS	(Init, Load, Calc, Step, Output);
	
	CONSTANT	Zeros				:	std_logic_vector(2*Bitsize downto 0)	:= (Others => '0');

	SIGNAL s_o, s_out			:	std_logic_vector(2*Bitsize-1 downto 0)	:= (Others => '0');
	SIGNAL s_x, s_y			:	std_logic_vector(Bitsize-1 downto 0)	:= (Others => '0');
	
	SIGNAL s_state_next, s_state_current :	Zustand := Init;
	
	SIGNAL s_count, s_ov		:	std_logic										:= '0';
	SIGNAL s_start				:	Integer range 0 to Bitsize-1				:= 0;
	SIGNAL s_q					:	Integer range 0 to Bitsize					:= 0;
	
	SIGNAL s_rst, s_shl, s_shr	:	std_logic									:= '0';
	SIGNAL s_sel					:	std_logic_vector(1 downto 0)			:= (Others => '0');
	SIGNAL s_rd, s_rq				:	std_logic_vector(2*Bitsize downto 0):=	(Others => '0');
	
BEGIN
	
	CNT	:	Counter
	GENERIC MAP (Bitsize)
	PORT MAP (
		CLK => s_count,
		nRST => nRST,
		Start => s_start,
		Ov => s_ov,
		Q => s_q
	);
	
	REG_A	:	RegisterU4
	GENERIC MAP (2*Bitsize+1)
	PORT MAP (
		CLK	=>	CLK,
		RST 	=>	s_rst,
		SHL	=>	s_shl,
		SHR	=>	s_shr,
		S		=>	s_sel,
		D		=>	s_rd,
		Q		=>	s_rq
	);
	
	s_x <= X;
	s_y <= Y;
	
	UEBERGANG : PROCESS(s_state_current, s_q)
	BEGIN	
		case s_state_current is
			when Init =>
				s_count <= '0';
				s_state_next <= Calc;
			when Calc =>
				s_count <= '1';
				s_state_next <= Step;
			when Step =>
				s_count <= '0';
				if ( s_q < Bitsize ) then
					s_state_next <= Calc;
				else
					s_state_next <= Output;
				end if;
			when Others =>
				s_state_next <= s_state_current;
		end case;
	END PROCESS UEBERGANG;
	
	AUSGANG : PROCESS(s_x, s_y, s_state_current)
		VARIABLE A, M		:	std_logic_vector(2*Bitsize downto 0)	:= (Others => '0');
	BEGIN		
		s_o <= (Others => '0');
		
		case s_state_current is
			when Init =>
				s_rd <= (Others => '0');
				A := (Others => '0');
				A(Bitsize downto 1) := s_x;
				M := (Others => '0');
				M(2*Bitsize downto Bitsize+1) := s_y;
			when Calc =>
				s_sel <= "11";
				if ( (A(1) XOR A(0)) = '1' ) then
					
				else
				
				end if;
				s_sel <= "10";
				case A(1 downto 0) is
					when "10" =>
						A := std_logic_vector(shift_right(signed(A) - signed(M), 1));
					when "01" =>
						A := std_logic_vector(shift_right(signed(A) + signed(M), 1));
					when Others =>
						A := std_logic_vector(shift_right(signed(A), 1));
				end case;
				M := M;
			when Step =>
				s_sel <= "00";
			when Output =>
				A := A;
				M := M;
				s_o(2*Bitsize-1 downto 0) <= A(2*Bitsize downto 1);
			when Others =>
				A := A;
				M := M;
		end case;
	END PROCESS AUSGANG;
	
	ZUSTANDSPEICHER : PROCESS(nRST, CLK, EN, s_state_next)
	BEGIN
		if ( nRST = '1' ) then
			if ( EN = '1' ) then
				if ( rising_edge(CLK) ) then
					s_state_current <= s_state_next;
				else
					s_state_current <= s_state_current;
				end if;
			else
				s_state_current <= s_state_current;
			end if;
		else
			s_state_current <= Init;
		end if;
	END PROCESS ZUSTANDSPEICHER;

	O <= s_o;
	State <= "0001" when s_state_current = Init else
				"0010" when s_state_current = Load else
				"0011" when s_state_current = Calc else
				"0100" when s_state_current = Step else
				"1000" when s_state_current = Output else
				"0000";
	
END Behaviour;