LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Volladdierer IS
	PORT (
		X, Y, CL, AS		:	IN		std_logic;
		S, CO					:	OUT	std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF Volladdierer IS

	SIGNAL s_m	:	std_logic := '0';

BEGIN 

	S <= (X XOR Y) XOR (NOT CL AND '1');
	CO <= (((AS AND '1') XOR X) AND Y) OR (CL AND (((AS AND '1') XOR X) OR Y));
	
END Behaviour;

ARCHITECTURE Derrived OF Volladdierer IS

	COMPONENT Halbaddierer IS
		PORT (
			X, Y		:	IN		std_logic;
			S, CO		:	OUT	std_logic
		);
	END COMPONENT;
	
	SIGNAL s_s, s_c	:	std_logic := '0';

BEGIN

	HA	:	Halbaddierer
	PORT MAP (X, Y, s_s, s_c);

	S <= s_s when CL='0' else NOT s_s;
	CO <= s_c OR (CL AND (X OR Y));

END Derrived;