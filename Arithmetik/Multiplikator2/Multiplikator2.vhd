LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY Multiplikator2 IS
	GENERIC (
		Bitsize			:	Integer	:=	4
	);
	PORT (
		nRST, CLK, EN	:	IN		std_logic;
		X, Y				:	IN		std_logic_vector(Bitsize-1 downto 0);
		O					:	OUT	std_logic_vector(2*Bitsize-1 downto 0);
		State				:	OUT	std_logic_vector(Bitsize-1 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF Multiplikator2 IS

	COMPONENT Counter IS
		GENERIC (
			Limit		:	Integer	:= 255
		);
		PORT (
			CLK, nRST	:	IN		std_logic;
			Start			:	IN		integer range 0 to Limit-1;
			Ov				:	OUT	std_logic;
			Q				:	OUT	integer range 0 to Limit
		);
	END COMPONENT;

	COMPONENT RegisterU4 IS
		GENERIC (
			Bitsize		:	Integer	:= 4
		);
		PORT (
			CLK, RST, SHL, SHR	:			std_logic;
			S				:			std_logic_vector (1 downto 0);
			D				:			std_logic_vector (Bitsize-1 downto 0);
			Q				:	OUT	std_logic_vector (Bitsize-1 downto 0)
		);
	END COMPONENT;
	
	COMPONENT GenericVAS IS
		GENERIC (
			Bitsize		:	Integer	:= 4
		);
		PORT (
			A, B			:	std_logic_vector(Bitsize-1 downto 0);
			AS				:	std_logic;
			Ov				:	OUT std_logic;
			Q				:	OUT std_logic_vector(Bitsize-1 downto 0)
		);
	END COMPONENT;
	
	TYPE	Zustand	IS	(Init, Load, Calc, Step, Output);
	
	CONSTANT	Zeros				:	std_logic_vector(2*Bitsize downto 0)	:= (Others => '0');

	SIGNAL s_o, s_out			:	std_logic_vector(2*Bitsize-1 downto 0)	:= (Others => '0');
	SIGNAL s_x, s_y			:	std_logic_vector(Bitsize-1 downto 0)	:= (Others => '0');
	
	SIGNAL s_state_next, s_state_current :	Zustand := Init;
	
	SIGNAL s_count, s_count_ov				:	std_logic										:=	'0';
	SIGNAL s_count_start						:	Integer range 0 to Bitsize-1				:= 0;
	SIGNAL s_count_n							:	Integer range 0 to Bitsize					:= 0;
	
	SIGNAL s_adder_a, s_adder_b, s_adder_sum	:	std_logic_vector(2*Bitsize downto 0)	:=	(Others => '0');
	SIGNAL s_adder_mode, s_adder_ov				:	std_logic										:=	'0';
	
	SIGNAL s_rega_clk, s_rega_clr					:	std_logic										:= '0';
	SIGNAL s_rega_shl, s_rega_shr					:	std_logic										:= '0';
	SIGNAL s_rega_sel									:	std_logic_vector(1 downto 0)				:=	(Others => '0');
	SIGNAL s_rega_d, s_rega_q						:	std_logic_vector(Bitsize-1 downto 0)	:= (Others => '0');
	
	SIGNAL s_regm_clk, s_regm_clr					:	std_logic										:= '0';
	SIGNAL s_regm_shl, s_regm_shr					:	std_logic										:= '0';
	SIGNAL s_regm_sel									:	std_logic_vector(1 downto 0)				:=	(Others => '0');
	SIGNAL s_regm_d, s_regm_q						:	std_logic_vector(Bitsize-1 downto 0)	:= (Others => '0');
	
BEGIN

	COUNTER : Counter
	GENERIC MAP (Bitsize)
	PORT MAP (
		CLK => s_count,
		nRST => nRST,
		Start => s_count_start,
		Ov => s_count_ov,
		Q => s_count_n
	);
	
	VAS : GenericVAS
	GENERIC MAP (2*Bitsize+1)
	PORT MAP (
		A	=>	s_adder_a,
		B	=>	s_adder_b,
		AS	=>	s_adder_mode,
		Ov	=>	s_adder_ov,
		Q	=>	s_adder_sum
	);
	
	REG_A	:	RegisterU4
	GENERIC MAP (2*Bitsize+1)
	PORT MAP (
		CLK	=>	s_rega_clk,
		RST	=>	s_rega_clr,
		SHL 	=>	s_rega_shl,
		SHR	=>	s_rega_shr,
		S		=>	s_rega_sel,
		D		=>	s_rega_d,
		Q		=>	s_rega_q
	);
	
	REG_M	:	RegisterU4
	GENERIC MAP (2*Bitsize+1)
	PORT MAP (
		CLK	=>	s_regm_clk,
		RST	=>	s_regm_clr,
		SHL 	=>	s_regm_shl,
		SHR	=>	s_regm_shr,
		S		=>	s_regm_sel,
		D		=>	s_regm_d,
		Q		=>	s_regm_q
	);
	
	s_x <= X;
	s_y <= Y;
	
	UEBERGANG : PROCESS(s_state_current)
	BEGIN	
		case s_state_current is
			when Init =>
				s_count <= '0';
				s_count_start <= 0;
				
				s_state_next <= Calc;
			when Calc =>
				Counter := Counter + 1;
				s_state_next <= Step;
			when Step =>
				if ( Counter < Bitsize ) then
					s_state_next <= Calc;
				else
					s_state_next <= Output;
				end if;
			when Others =>
				s_state_next <= s_state_current;
		end case;
	END PROCESS UEBERGANG;
	
	AUSGANG : PROCESS(s_x, s_y, s_state_current)
		VARIABLE A, M		:	std_logic_vector(2*Bitsize downto 0)	:= (Others => '0');
	BEGIN		
		s_o <= (Others => '0');
		
		case s_state_current is
			when Init =>
				s_adder_a <= (Others => '0');
				s_adder_b <= (Others => '0');
				s_adder_mode <= '0';
				
				s_rega_d <= (Others => '0');
				s_regm_d	<=	(Others => '0');
			
				A := (Others => '0');
				A(Bitsize downto 1) := s_x;
				M := (Others => '0');
				M(2*Bitsize downto Bitsize+1) := s_y;
			when Calc =>
				case A(1 downto 0) is
					when "10" =>
						A := std_logic_vector(shift_right(signed(A) - signed(M), 1));
					when "01" =>
						A := std_logic_vector(shift_right(signed(A) + signed(M), 1));
					when Others =>
						A := std_logic_vector(shift_right(signed(A), 1));
				end case;
				M := M;
			when Output =>
				A := A;
				M := M;
				s_o(2*Bitsize-1 downto 0) <= A(2*Bitsize downto 1);
			when Others =>
				A := A;
				M := M;
		end case;
	END PROCESS AUSGANG;
	
	ZUSTANDSPEICHER : PROCESS(nRST, CLK, EN, s_state_next)
	BEGIN
		if ( nRST = '1' ) then
			if ( EN = '1' ) then
				if ( rising_edge(CLK) ) then
					s_state_current <= s_state_next;
				else
					s_state_current <= s_state_current;
				end if;
			else
				s_state_current <= s_state_current;
			end if;
		else
			s_state_current <= Init;
		end if;
	END PROCESS ZUSTANDSPEICHER;

	O <= s_o;
	State <= "0001" when s_state_current = Init else
				"0010" when s_state_current = Load else
				"0011" when s_state_current = Calc else
				"0100" when s_state_current = Step else
				"1000" when s_state_current = Output else
				"0000";
	
END Behaviour;