LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_Volladdierer IS
END ENTITY;

ARCHITECTURE Testbench OF tb_Volladdierer IS

	COMPONENT Volladdierer IS
		PORT (
			X, Y, CL, AS:	IN		std_logic;
			S, CO			:	OUT	std_logic
		);
	END COMPONENT;

	SIGNAL s_x, s_y, s_s, s_l, s_c, s_m	:	std_logic	:= '0';
	
BEGIN

	DUT	:	Volladdierer
	PORT MAP(
		X	=>	s_x,
		Y	=>	s_y,
		CL	=>	s_l,
		AS =>	s_m,
		S	=>	s_s,
		CO	=>	s_c
	);
	
	s_m <= NOT s_m AFTER 160ns;
	s_l <= NOT s_l AFTER 80ns;
	s_x <= NOT s_x AFTER 40ns;
	s_y <= NOT s_y AFTER 20ns;

	
END Testbench;