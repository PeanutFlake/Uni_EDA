-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/11/2018 17:22:13"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	Volladdierer IS
    PORT (
	X : IN std_logic;
	Y : IN std_logic;
	CL : IN std_logic;
	AS : IN std_logic;
	S : OUT std_logic;
	CO : OUT std_logic
	);
END Volladdierer;

-- Design Ports Information
-- AS	=>  Location: PIN_L4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S	=>  Location: PIN_A7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CO	=>  Location: PIN_M4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- X	=>  Location: PIN_N4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Y	=>  Location: PIN_A6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CL	=>  Location: PIN_M6,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF Volladdierer IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_X : std_logic;
SIGNAL ww_Y : std_logic;
SIGNAL ww_CL : std_logic;
SIGNAL ww_AS : std_logic;
SIGNAL ww_S : std_logic;
SIGNAL ww_CO : std_logic;
SIGNAL \AS~input_o\ : std_logic;
SIGNAL \S~output_o\ : std_logic;
SIGNAL \CO~output_o\ : std_logic;
SIGNAL \X~input_o\ : std_logic;
SIGNAL \CL~input_o\ : std_logic;
SIGNAL \Y~input_o\ : std_logic;
SIGNAL \S~0_combout\ : std_logic;
SIGNAL \CO~0_combout\ : std_logic;

BEGIN

ww_X <= X;
ww_Y <= Y;
ww_CL <= CL;
ww_AS <= AS;
S <= ww_S;
CO <= ww_CO;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: IOOBUF_X12_Y31_N2
\S~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \S~0_combout\,
	devoe => ww_devoe,
	o => \S~output_o\);

-- Location: IOOBUF_X8_Y0_N2
\CO~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \CO~0_combout\,
	devoe => ww_devoe,
	o => \CO~output_o\);

-- Location: IOIBUF_X10_Y0_N8
\X~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_X,
	o => \X~input_o\);

-- Location: IOIBUF_X12_Y0_N8
\CL~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CL,
	o => \CL~input_o\);

-- Location: IOIBUF_X10_Y31_N1
\Y~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Y,
	o => \Y~input_o\);

-- Location: LCCOMB_X10_Y1_N0
\S~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \S~0_combout\ = \X~input_o\ $ (\CL~input_o\ $ (\Y~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \X~input_o\,
	datac => \CL~input_o\,
	datad => \Y~input_o\,
	combout => \S~0_combout\);

-- Location: LCCOMB_X10_Y1_N2
\CO~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \CO~0_combout\ = (\X~input_o\ & ((\CL~input_o\) # (\Y~input_o\))) # (!\X~input_o\ & (\CL~input_o\ & \Y~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \X~input_o\,
	datac => \CL~input_o\,
	datad => \Y~input_o\,
	combout => \CO~0_combout\);

-- Location: IOIBUF_X8_Y0_N8
\AS~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_AS,
	o => \AS~input_o\);

ww_S <= \S~output_o\;

ww_CO <= \CO~output_o\;
END structure;


