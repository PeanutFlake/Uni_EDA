-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/07/2018 15:40:39"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	GenericVAS IS
    PORT (
	A : IN std_logic_vector(3 DOWNTO 0);
	B : IN std_logic_vector(3 DOWNTO 0);
	AS : IN std_logic;
	Ov : BUFFER std_logic;
	Q : BUFFER std_logic_vector(3 DOWNTO 0)
	);
END GenericVAS;

-- Design Ports Information
-- Ov	=>  Location: PIN_M13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[0]	=>  Location: PIN_K9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[1]	=>  Location: PIN_B8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[2]	=>  Location: PIN_N8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[3]	=>  Location: PIN_N10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[3]	=>  Location: PIN_N13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[3]	=>  Location: PIN_L5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- AS	=>  Location: PIN_M9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[2]	=>  Location: PIN_L9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[2]	=>  Location: PIN_K11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[0]	=>  Location: PIN_H10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[0]	=>  Location: PIN_N11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[1]	=>  Location: PIN_B10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[1]	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF GenericVAS IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_A : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_B : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_AS : std_logic;
SIGNAL ww_Ov : std_logic;
SIGNAL ww_Q : std_logic_vector(3 DOWNTO 0);
SIGNAL \Ov~output_o\ : std_logic;
SIGNAL \Q[0]~output_o\ : std_logic;
SIGNAL \Q[1]~output_o\ : std_logic;
SIGNAL \Q[2]~output_o\ : std_logic;
SIGNAL \Q[3]~output_o\ : std_logic;
SIGNAL \B[3]~input_o\ : std_logic;
SIGNAL \AS~input_o\ : std_logic;
SIGNAL \A[3]~input_o\ : std_logic;
SIGNAL \B[2]~input_o\ : std_logic;
SIGNAL \A[2]~input_o\ : std_logic;
SIGNAL \B[0]~input_o\ : std_logic;
SIGNAL \A[0]~input_o\ : std_logic;
SIGNAL \Init:0:VA|CO~0_combout\ : std_logic;
SIGNAL \A[1]~input_o\ : std_logic;
SIGNAL \B[1]~input_o\ : std_logic;
SIGNAL \Init:1:VA|S~1_cout\ : std_logic;
SIGNAL \Init:1:VA|S~3\ : std_logic;
SIGNAL \Init:1:VA|S~5\ : std_logic;
SIGNAL \Init:1:VA|S~7\ : std_logic;
SIGNAL \Init:1:VA|S~8_combout\ : std_logic;
SIGNAL \Init:0:VA|S~0_combout\ : std_logic;
SIGNAL \Init:1:VA|S~2_combout\ : std_logic;
SIGNAL \Init:1:VA|S~4_combout\ : std_logic;
SIGNAL \Init:1:VA|S~6_combout\ : std_logic;
SIGNAL s_B : std_logic_vector(3 DOWNTO 0);

BEGIN

ww_A <= A;
ww_B <= B;
ww_AS <= AS;
Ov <= ww_Ov;
Q <= ww_Q;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: IOOBUF_X33_Y10_N2
\Ov~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Init:1:VA|S~8_combout\,
	devoe => ww_devoe,
	o => \Ov~output_o\);

-- Location: IOOBUF_X22_Y0_N2
\Q[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Init:0:VA|S~0_combout\,
	devoe => ww_devoe,
	o => \Q[0]~output_o\);

-- Location: IOOBUF_X22_Y31_N2
\Q[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Init:1:VA|S~2_combout\,
	devoe => ww_devoe,
	o => \Q[1]~output_o\);

-- Location: IOOBUF_X20_Y0_N9
\Q[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Init:1:VA|S~4_combout\,
	devoe => ww_devoe,
	o => \Q[2]~output_o\);

-- Location: IOOBUF_X26_Y0_N9
\Q[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Init:1:VA|S~6_combout\,
	devoe => ww_devoe,
	o => \Q[3]~output_o\);

-- Location: IOIBUF_X14_Y0_N8
\B[3]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(3),
	o => \B[3]~input_o\);

-- Location: IOIBUF_X24_Y0_N1
\AS~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_AS,
	o => \AS~input_o\);

-- Location: LCCOMB_X24_Y7_N10
\s_B[3]\ : cycloneiv_lcell_comb
-- Equation(s):
-- s_B(3) = \B[3]~input_o\ $ (\AS~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \B[3]~input_o\,
	datad => \AS~input_o\,
	combout => s_B(3));

-- Location: IOIBUF_X33_Y10_N8
\A[3]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(3),
	o => \A[3]~input_o\);

-- Location: IOIBUF_X33_Y11_N1
\B[2]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(2),
	o => \B[2]~input_o\);

-- Location: LCCOMB_X24_Y7_N12
\s_B[2]\ : cycloneiv_lcell_comb
-- Equation(s):
-- s_B(2) = \AS~input_o\ $ (\B[2]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AS~input_o\,
	datad => \B[2]~input_o\,
	combout => s_B(2));

-- Location: IOIBUF_X24_Y0_N8
\A[2]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(2),
	o => \A[2]~input_o\);

-- Location: IOIBUF_X26_Y0_N1
\B[0]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(0),
	o => \B[0]~input_o\);

-- Location: IOIBUF_X33_Y14_N1
\A[0]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(0),
	o => \A[0]~input_o\);

-- Location: LCCOMB_X24_Y7_N30
\Init:0:VA|CO~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Init:0:VA|CO~0_combout\ = (\B[0]~input_o\ & ((\A[0]~input_o\))) # (!\B[0]~input_o\ & (\AS~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AS~input_o\,
	datab => \B[0]~input_o\,
	datad => \A[0]~input_o\,
	combout => \Init:0:VA|CO~0_combout\);

-- Location: IOIBUF_X24_Y31_N8
\A[1]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(1),
	o => \A[1]~input_o\);

-- Location: IOIBUF_X22_Y0_N8
\B[1]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(1),
	o => \B[1]~input_o\);

-- Location: LCCOMB_X24_Y7_N0
\Init:1:VA|S~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Init:1:VA|S~1_cout\ = CARRY(\AS~input_o\ $ (\B[1]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \AS~input_o\,
	datab => \B[1]~input_o\,
	datad => VCC,
	cout => \Init:1:VA|S~1_cout\);

-- Location: LCCOMB_X24_Y7_N2
\Init:1:VA|S~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Init:1:VA|S~2_combout\ = (\Init:0:VA|CO~0_combout\ & ((\A[1]~input_o\ & (\Init:1:VA|S~1_cout\ & VCC)) # (!\A[1]~input_o\ & (!\Init:1:VA|S~1_cout\)))) # (!\Init:0:VA|CO~0_combout\ & ((\A[1]~input_o\ & (!\Init:1:VA|S~1_cout\)) # (!\A[1]~input_o\ & 
-- ((\Init:1:VA|S~1_cout\) # (GND)))))
-- \Init:1:VA|S~3\ = CARRY((\Init:0:VA|CO~0_combout\ & (!\A[1]~input_o\ & !\Init:1:VA|S~1_cout\)) # (!\Init:0:VA|CO~0_combout\ & ((!\Init:1:VA|S~1_cout\) # (!\A[1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Init:0:VA|CO~0_combout\,
	datab => \A[1]~input_o\,
	datad => VCC,
	cin => \Init:1:VA|S~1_cout\,
	combout => \Init:1:VA|S~2_combout\,
	cout => \Init:1:VA|S~3\);

-- Location: LCCOMB_X24_Y7_N4
\Init:1:VA|S~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Init:1:VA|S~4_combout\ = ((s_B(2) $ (\A[2]~input_o\ $ (!\Init:1:VA|S~3\)))) # (GND)
-- \Init:1:VA|S~5\ = CARRY((s_B(2) & ((\A[2]~input_o\) # (!\Init:1:VA|S~3\))) # (!s_B(2) & (\A[2]~input_o\ & !\Init:1:VA|S~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => s_B(2),
	datab => \A[2]~input_o\,
	datad => VCC,
	cin => \Init:1:VA|S~3\,
	combout => \Init:1:VA|S~4_combout\,
	cout => \Init:1:VA|S~5\);

-- Location: LCCOMB_X24_Y7_N6
\Init:1:VA|S~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Init:1:VA|S~6_combout\ = (s_B(3) & ((\A[3]~input_o\ & (\Init:1:VA|S~5\ & VCC)) # (!\A[3]~input_o\ & (!\Init:1:VA|S~5\)))) # (!s_B(3) & ((\A[3]~input_o\ & (!\Init:1:VA|S~5\)) # (!\A[3]~input_o\ & ((\Init:1:VA|S~5\) # (GND)))))
-- \Init:1:VA|S~7\ = CARRY((s_B(3) & (!\A[3]~input_o\ & !\Init:1:VA|S~5\)) # (!s_B(3) & ((!\Init:1:VA|S~5\) # (!\A[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => s_B(3),
	datab => \A[3]~input_o\,
	datad => VCC,
	cin => \Init:1:VA|S~5\,
	combout => \Init:1:VA|S~6_combout\,
	cout => \Init:1:VA|S~7\);

-- Location: LCCOMB_X24_Y7_N8
\Init:1:VA|S~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Init:1:VA|S~8_combout\ = !\Init:1:VA|S~7\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \Init:1:VA|S~7\,
	combout => \Init:1:VA|S~8_combout\);

-- Location: LCCOMB_X24_Y7_N16
\Init:0:VA|S~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Init:0:VA|S~0_combout\ = \B[0]~input_o\ $ (\A[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \B[0]~input_o\,
	datad => \A[0]~input_o\,
	combout => \Init:0:VA|S~0_combout\);

ww_Ov <= \Ov~output_o\;

ww_Q(0) <= \Q[0]~output_o\;

ww_Q(1) <= \Q[1]~output_o\;

ww_Q(2) <= \Q[2]~output_o\;

ww_Q(3) <= \Q[3]~output_o\;
END structure;


