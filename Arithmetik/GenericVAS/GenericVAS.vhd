LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY GenericVAS IS
	GENERIC (
		Bitsize		:	Integer	:= 4
	);
	PORT (
		A, B			:	std_logic_vector(Bitsize-1 downto 0);
		AS				:	std_logic;
		Ov				:	OUT std_logic;
		Q				:	OUT std_logic_vector(Bitsize-1 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF GenericVAS IS

	COMPONENT Volladdierer IS
		PORT (
			A, B, CI		:	std_logic;
			S, CO			:	OUT std_logic
		);
	END COMPONENT;

	SIGNAL s_carry : 	std_logic_vector(Bitsize downto 0)	:= (Others => '0');
	SIGNAL s_B		:	std_logic_vector(Bitsize-1 downto 0) := (Others => '0');
	
BEGIN

		Init:
		For N in 0 to Bitsize-1 generate
		
		s_B(N) <= B(N) XOR AS;
		
		VA : Volladdierer
		PORT MAP (
			A => A(N),
			B => s_B(N),
			CI => s_carry(N),
			S => Q(N),
			CO => s_carry(N+1)
		);
		
		end generate Init;

		s_carry(0) <= AS;
		Ov <= s_carry(Bitsize);
		
END Behaviour;