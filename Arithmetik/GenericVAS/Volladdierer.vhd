LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Volladdierer IS
	PORT (
		A, B, CI		:	std_logic;
		S, CO			:	OUT std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF Volladdierer IS

BEGIN

	S <= (NOT A AND (B XOR CI)) OR (A AND (B XNOR CI));
	CO <= (A AND (B OR CI)) OR (NOT A AND B AND CI);

END Behaviour;