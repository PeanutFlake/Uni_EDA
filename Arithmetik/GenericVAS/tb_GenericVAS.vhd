LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_GenericVAS IS
END ENTITY;

ARCHITECTURE Testbench OF tb_GenericVAS IS

	COMPONENT GenericVAS IS
		GENERIC (
			Bitsize		:	Integer	:= 4
		);
		PORT (
			A, B			:	std_logic_vector(Bitsize-1 downto 0);
			AS				:	std_logic;
			Ov				:	OUT std_logic;
			Q				:	OUT std_logic_vector(Bitsize-1 downto 0)
		);
	END COMPONENT;

	SIGNAL s_a, s_b, s_q	:	std_logic_vector(8 downto 0)	:= (Others => '0');
	SIGNAl s_ov, s_as		:	std_logic							:=	'0';
	
BEGIN

	DUT	:	GenericVAS
	GENERIC MAP (9)
	PORT MAP (
		A => s_a,
		B => s_b,
		AS => s_as,
		OV => s_ov,
		Q => s_q
	);
	
	s_a <= "000000100" after 20ns;
	s_b <= "001000000" after 20ns;
	s_as <= '1' after 100ns; 

END Testbench;