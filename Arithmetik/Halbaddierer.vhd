LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Halbaddierer IS

	PORT (
		X, Y		:	IN		std_logic;
		S, CO		:	OUT	std_logic
	);

END ENTITY;

ARCHITECTURE Behaviour OF Halbaddierer IS

BEGIN

	CO <= X AND Y;
	S <= X XOR Y;

END Behaviour;