ENTITY DetectorN IS
	GENERIC (
		SIG_LENGTH	:	Integer	:= 3
	);
	PORT (
		CLK, A, REF	:	IN		bit;
		SIG			:	OUT	bit
	);
END ENTITY;

ARCHITECTURE Behaviour OF DetectorN IS

BEGIN

PROCESS (CLK, A, REF)
	VARIABLE C	:	Integer Range 0 to SIG_LENGTH := 0;
	VARIABLE S	:	bit	:= '0';
BEGIN
	if (CLK'event AND CLK='1') then
		if ( A = REF ) then
			C := C + 1;
			if ( C = SIG_LENGTH-1 ) then
				S := '1';
			else
				S := '0';
			end if;
		else
			C := 0;
		end if;
	else
		S := S;
	end if;
	SIG <= S;
END PROCESS;

END Behaviour;