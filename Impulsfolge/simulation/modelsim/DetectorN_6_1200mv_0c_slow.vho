-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/16/2018 11:26:25"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	DetectorN IS
    PORT (
	CLK : IN std_logic;
	A : IN std_logic;
	REF : IN std_logic;
	SIG : BUFFER std_logic
	);
END DetectorN;

-- Design Ports Information
-- SIG	=>  Location: PIN_A6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A	=>  Location: PIN_A7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- REF	=>  Location: PIN_A8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_J7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF DetectorN IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_A : std_logic;
SIGNAL ww_REF : std_logic;
SIGNAL ww_SIG : std_logic;
SIGNAL \CLK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \SIG~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \CLK~inputclkctrl_outclk\ : std_logic;
SIGNAL \REF~input_o\ : std_logic;
SIGNAL \A~input_o\ : std_logic;
SIGNAL \C~0_combout\ : std_logic;
SIGNAL \C~1_combout\ : std_logic;
SIGNAL \process_0~0_combout\ : std_logic;
SIGNAL \S~0_combout\ : std_logic;
SIGNAL \S~q\ : std_logic;
SIGNAL C : std_logic_vector(1 DOWNTO 0);

BEGIN

ww_CLK <= CLK;
ww_A <= A;
ww_REF <= REF;
SIG <= ww_SIG;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLK~input_o\);

-- Location: IOOBUF_X10_Y31_N2
\SIG~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \S~q\,
	devoe => ww_devoe,
	o => \SIG~output_o\);

-- Location: IOIBUF_X16_Y0_N15
\CLK~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: CLKCTRL_G17
\CLK~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLK~inputclkctrl_outclk\);

-- Location: IOIBUF_X12_Y31_N8
\REF~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_REF,
	o => \REF~input_o\);

-- Location: IOIBUF_X12_Y31_N1
\A~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A,
	o => \A~input_o\);

-- Location: LCCOMB_X12_Y30_N28
\C~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \C~0_combout\ = (!C(0) & (\A~input_o\ $ (!\REF~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \A~input_o\,
	datac => C(0),
	datad => \REF~input_o\,
	combout => \C~0_combout\);

-- Location: FF_X12_Y30_N29
\C[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \C~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => C(0));

-- Location: LCCOMB_X12_Y30_N10
\C~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \C~1_combout\ = (\REF~input_o\ & (\A~input_o\ & (C(1) $ (C(0))))) # (!\REF~input_o\ & (!\A~input_o\ & (C(1) $ (C(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \REF~input_o\,
	datab => \A~input_o\,
	datac => C(1),
	datad => C(0),
	combout => \C~1_combout\);

-- Location: FF_X12_Y30_N11
\C[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \C~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => C(1));

-- Location: LCCOMB_X12_Y30_N18
\process_0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \process_0~0_combout\ = \A~input_o\ $ (\REF~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \A~input_o\,
	datad => \REF~input_o\,
	combout => \process_0~0_combout\);

-- Location: LCCOMB_X12_Y30_N12
\S~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \S~0_combout\ = (\process_0~0_combout\ & (((\S~q\)))) # (!\process_0~0_combout\ & (!C(1) & ((C(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => C(1),
	datab => \process_0~0_combout\,
	datac => \S~q\,
	datad => C(0),
	combout => \S~0_combout\);

-- Location: FF_X12_Y30_N13
S : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \S~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \S~q\);

ww_SIG <= \SIG~output_o\;
END structure;


