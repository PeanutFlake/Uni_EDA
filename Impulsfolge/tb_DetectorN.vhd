ENTITY tb_DetectorN IS
END ENTITY;

ARCHITECTURE Testbench OF tb_DetectorN IS

	COMPONENT DetectorN IS
		GENERIC (
			SIG_LENGTH	:	Integer	:= 3
		);
		PORT (
			CLK, A, REF	:	IN		bit;
			SIG			:	OUT	bit
		);
	END COMPONENT;

	SIGNAL s_clk, s_a, s_ref, s_sig : bit := '0';

BEGIN

	DUT	:	DetectorN GENERIC MAP(4) PORT MAP (s_clk, s_a, s_ref, s_sig);

	s_clk <= NOT s_clk AFTER 10ns;
	s_ref <= '1';
	s_a <= '1' AFTER 20ns, '0' AFTER 35ns, '1' AFTER 70ns, '0' AFTER 120ns, '1' AFTER 145ns, '0' AFTER 170ns;
	
END Testbench;