LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Impulsdetektor IS
	PORT (
		X, CLK	:	IN		std_logic;
		Y			:	OUT	std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF Impulsdetektor IS

	TYPE State IS (A, B, C, D, E, F, G, H, I);
	
	SIGNAL s_stat : State := A;
	
BEGIN

	Zustand: PROCESS(CLK, X)
	BEGIN
		if ( rising_edge(CLK) ) then
			case s_stat is
				when A =>
					if (X = '0') then
						s_stat <= B;
					else
						s_stat <= F;
					end if;
				when B => 
					if (X = '0') then
						s_stat <= C;
					else
						s_stat <= F;
					end if;
				when C =>
					if (X = '0') then
						s_stat <= D;
					else
						s_stat <= F;
					end if;
				when D => 
					if (X = '0') then
						s_stat <= E;
					else
						s_stat <= F;
					end if;
				when E =>
					if (X = '0') then
						s_stat <= E;
					else
						s_stat <= F;
					end if;
				when F =>
					if (X = '0') then
						s_stat <= B;
					else
						s_stat <= G;
					end if;
				when G =>
					if (X = '0') then
						s_stat <= B;
					else
						s_stat <= H;
					end if;
				when H =>
					if (X = '0') then
						s_stat <= B;
					else
						s_stat <= I;
					end if;
				when I =>
					if (X = '0') then
						s_stat <= B;
					else
						s_stat <= I;
					end if;
			end case;
		end if;
	
	END PROCESS Zustand;
	
	
	Ausgang: PROCESS(s_stat)
	BEGIN
		if (s_stat = I OR s_stat = E) then
			Y <= '1';
		else
			Y <= '0';
		end if;
	END PROCESS Ausgang;


END Behaviour;