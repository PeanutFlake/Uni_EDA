-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/14/2018 10:56:46"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	Impulsdetektor IS
    PORT (
	X : IN std_logic;
	CLK : IN std_logic;
	Y : OUT std_logic
	);
END Impulsdetektor;

-- Design Ports Information
-- Y	=>  Location: PIN_A7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- X	=>  Location: PIN_C6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_J7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF Impulsdetektor IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_X : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_Y : std_logic;
SIGNAL \CLK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \Y~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \CLK~inputclkctrl_outclk\ : std_logic;
SIGNAL \X~input_o\ : std_logic;
SIGNAL \s_stat~16_combout\ : std_logic;
SIGNAL \s_stat~17_combout\ : std_logic;
SIGNAL \s_stat.F~q\ : std_logic;
SIGNAL \s_stat~15_combout\ : std_logic;
SIGNAL \s_stat.G~q\ : std_logic;
SIGNAL \s_stat~13_combout\ : std_logic;
SIGNAL \s_stat.H~q\ : std_logic;
SIGNAL \s_stat~11_combout\ : std_logic;
SIGNAL \s_stat.I~q\ : std_logic;
SIGNAL \Selector0~0_combout\ : std_logic;
SIGNAL \Selector0~1_combout\ : std_logic;
SIGNAL \s_stat.B~q\ : std_logic;
SIGNAL \s_stat~14_combout\ : std_logic;
SIGNAL \s_stat.C~q\ : std_logic;
SIGNAL \s_stat~12_combout\ : std_logic;
SIGNAL \s_stat.D~q\ : std_logic;
SIGNAL \Selector1~0_combout\ : std_logic;
SIGNAL \s_stat.E~q\ : std_logic;
SIGNAL \Ausgang~0_combout\ : std_logic;

BEGIN

ww_X <= X;
ww_CLK <= CLK;
Y <= ww_Y;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLK~input_o\);

-- Location: IOOBUF_X12_Y31_N2
\Y~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Ausgang~0_combout\,
	devoe => ww_devoe,
	o => \Y~output_o\);

-- Location: IOIBUF_X16_Y0_N15
\CLK~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: CLKCTRL_G17
\CLK~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLK~inputclkctrl_outclk\);

-- Location: IOIBUF_X14_Y31_N1
\X~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_X,
	o => \X~input_o\);

-- Location: LCCOMB_X14_Y30_N28
\s_stat~16\ : cycloneiv_lcell_comb
-- Equation(s):
-- \s_stat~16_combout\ = (\X~input_o\ & (!\s_stat.H~q\ & (!\s_stat.I~q\ & !\s_stat.F~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \X~input_o\,
	datab => \s_stat.H~q\,
	datac => \s_stat.I~q\,
	datad => \s_stat.F~q\,
	combout => \s_stat~16_combout\);

-- Location: LCCOMB_X14_Y30_N20
\s_stat~17\ : cycloneiv_lcell_comb
-- Equation(s):
-- \s_stat~17_combout\ = (!\s_stat.G~q\ & \s_stat~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_stat.G~q\,
	datad => \s_stat~16_combout\,
	combout => \s_stat~17_combout\);

-- Location: FF_X14_Y30_N21
\s_stat.F\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_stat~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_stat.F~q\);

-- Location: LCCOMB_X14_Y30_N24
\s_stat~15\ : cycloneiv_lcell_comb
-- Equation(s):
-- \s_stat~15_combout\ = (\X~input_o\ & \s_stat.F~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \X~input_o\,
	datad => \s_stat.F~q\,
	combout => \s_stat~15_combout\);

-- Location: FF_X14_Y30_N25
\s_stat.G\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_stat~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_stat.G~q\);

-- Location: LCCOMB_X14_Y30_N4
\s_stat~13\ : cycloneiv_lcell_comb
-- Equation(s):
-- \s_stat~13_combout\ = (\X~input_o\ & \s_stat.G~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \X~input_o\,
	datad => \s_stat.G~q\,
	combout => \s_stat~13_combout\);

-- Location: FF_X14_Y30_N5
\s_stat.H\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_stat~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_stat.H~q\);

-- Location: LCCOMB_X14_Y30_N26
\s_stat~11\ : cycloneiv_lcell_comb
-- Equation(s):
-- \s_stat~11_combout\ = (\X~input_o\ & ((\s_stat.I~q\) # (\s_stat.H~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \X~input_o\,
	datac => \s_stat.I~q\,
	datad => \s_stat.H~q\,
	combout => \s_stat~11_combout\);

-- Location: FF_X14_Y30_N27
\s_stat.I\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_stat~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_stat.I~q\);

-- Location: LCCOMB_X14_Y30_N6
\Selector0~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Selector0~0_combout\ = (\X~input_o\) # ((\s_stat.E~q\) # ((\s_stat.B~q\) # (\s_stat.D~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \X~input_o\,
	datab => \s_stat.E~q\,
	datac => \s_stat.B~q\,
	datad => \s_stat.D~q\,
	combout => \Selector0~0_combout\);

-- Location: LCCOMB_X14_Y30_N30
\Selector0~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Selector0~1_combout\ = (!\s_stat.C~q\ & !\Selector0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \s_stat.C~q\,
	datad => \Selector0~0_combout\,
	combout => \Selector0~1_combout\);

-- Location: FF_X14_Y30_N31
\s_stat.B\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Selector0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_stat.B~q\);

-- Location: LCCOMB_X14_Y30_N22
\s_stat~14\ : cycloneiv_lcell_comb
-- Equation(s):
-- \s_stat~14_combout\ = (!\X~input_o\ & \s_stat.B~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \X~input_o\,
	datad => \s_stat.B~q\,
	combout => \s_stat~14_combout\);

-- Location: FF_X14_Y30_N23
\s_stat.C\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_stat~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_stat.C~q\);

-- Location: LCCOMB_X14_Y30_N18
\s_stat~12\ : cycloneiv_lcell_comb
-- Equation(s):
-- \s_stat~12_combout\ = (!\X~input_o\ & \s_stat.C~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \X~input_o\,
	datac => \s_stat.C~q\,
	combout => \s_stat~12_combout\);

-- Location: FF_X14_Y30_N19
\s_stat.D\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \s_stat~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_stat.D~q\);

-- Location: LCCOMB_X14_Y30_N8
\Selector1~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Selector1~0_combout\ = (!\X~input_o\ & ((\s_stat.E~q\) # (\s_stat.D~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \X~input_o\,
	datac => \s_stat.E~q\,
	datad => \s_stat.D~q\,
	combout => \Selector1~0_combout\);

-- Location: FF_X14_Y30_N9
\s_stat.E\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \Selector1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_stat.E~q\);

-- Location: LCCOMB_X14_Y30_N16
\Ausgang~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Ausgang~0_combout\ = (\s_stat.I~q\) # (\s_stat.E~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \s_stat.I~q\,
	datad => \s_stat.E~q\,
	combout => \Ausgang~0_combout\);

ww_Y <= \Y~output_o\;
END structure;


