LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_Impulsdetektor IS
END ENTITY;

ARCHITECTURE Testbench OF tb_Impulsdetektor IS

	COMPONENT Impulsdetektor IS
		PORT (
			X, CLK	:	IN		std_logic;
			Y			:	OUT	std_logic
		);
	END COMPONENT;

	SIGNAL s_x, s_clk, s_y	:	std_logic := '0';
	
BEGIN

	DUT	:	Impulsdetektor
	PORT MAP (s_x, s_clk, s_y);

	s_clk <= NOT s_clk AFTER 20ns;
	s_x <= '1' AFTER 50ns, '0' AFTER 120ns, '1' AFTER 180ns, '0' AFTER 330ns;
	
END Testbench;