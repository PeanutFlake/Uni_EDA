LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_CodeDisplay IS
END ENTITY;

ARCHITECTURE Testbench OF tb_CodeDisplay IS
	COMPONENT CodeDisplay IS
		GENERIC (
			F_CLK		:	integer	:= 50000000	--	50MHz
		);
		PORT (
			INC		:	IN		std_logic;
			SS, nRST	:	IN		std_logic;
			
			Err		:	OUT	std_logic;
			
			-- LCD Connections
			LCD_D							:	OUT	std_logic_vector(7 downto 0);
			LCD_RW, LCD_RS, LCD_EN	:	OUT	std_logic;
			-- Debug Connections
			LED_Counter					:	OUT	std_logic_vector(3 downto 0)
		);
	END COMPONENT;
	
	SIGNAL s_inc, s_ss, s_err		:	std_logic	:= '0';
	SIGNAL s_rst						:	std_logic	:= '1';
BEGIN

	DUT	:	CodeDisplay
	GENERIC MAP (15)
	PORT MAP (
		INC	=>	s_inc,
		SS		=>	s_ss,
		nRST	=>	s_rst,
		Err	=> s_err
	);
	
	s_inc <= NOT s_inc AFTER 30ns;

END Testbench;