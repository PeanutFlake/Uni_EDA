LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY CodeDisplay IS
	GENERIC (
		F_CLK		:	integer	:= 50000000	--	50MHz
	);
	PORT (
		INC		:	IN		std_logic;
		SS, nRST	:	IN		std_logic;
		
		Err		:	OUT	std_logic;
		
		-- LCD Connections
		LCD_D							:	OUT	std_logic_vector(7 downto 0);
		LCD_RW, LCD_RS, LCD_EN	:	OUT	std_logic;
		-- Debug Connections
		LED_Counter					:	OUT	std_logic_vector(3 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF CodeDisplay IS

	COMPONENT Counter IS
		GENERIC (
			Limit		:	Integer	:= 255
		);
		PORT (
			CLK, RST	:	IN		std_logic;
			Ov			:	OUT	std_logic;
			Q			:	OUT	integer range 0 to Limit
		);
	END COMPONENT;

	SIGNAL s_rst					:	std_logic	:= '1';
	SIGNAL s_ov, s_inc, s_irr	:	std_logic	:= '0';
	SIGNAL s_count					:	integer range 0 to 15 := 0;
	
BEGIN

	s_inc <= INC;
	s_rst	<= NOT nRST OR s_irr;

	Count : Counter
	GENERIC MAP (15)
	PORT MAP (s_inc, s_rst, s_ov, s_count);

	
	s_irr <= '1' when (s_count = 10) else '0';
	Err <= s_ov OR NOT nRST;
	LED_Counter <= std_logic_vector(to_unsigned(s_count, 4));
	
END Behaviour;