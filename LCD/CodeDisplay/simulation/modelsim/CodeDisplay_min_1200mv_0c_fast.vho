-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/29/2018 15:19:46"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	CodeDisplay IS
    PORT (
	INC : IN std_logic;
	SS : IN std_logic;
	nRST : IN std_logic;
	Err : OUT std_logic;
	LCD_D : OUT std_logic_vector(7 DOWNTO 0);
	LCD_RW : OUT std_logic;
	LCD_RS : OUT std_logic;
	LCD_EN : OUT std_logic;
	LED_Counter : OUT std_logic_vector(3 DOWNTO 0)
	);
END CodeDisplay;

-- Design Ports Information
-- SS	=>  Location: PIN_AA24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Err	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[0]	=>  Location: PIN_L3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[1]	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[2]	=>  Location: PIN_L2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[3]	=>  Location: PIN_K7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[4]	=>  Location: PIN_K1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[5]	=>  Location: PIN_K2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[6]	=>  Location: PIN_M3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_D[7]	=>  Location: PIN_M5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_RW	=>  Location: PIN_M1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_RS	=>  Location: PIN_M2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LCD_EN	=>  Location: PIN_L4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_Counter[0]	=>  Location: PIN_E21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_Counter[1]	=>  Location: PIN_E22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_Counter[2]	=>  Location: PIN_E25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_Counter[3]	=>  Location: PIN_E24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- nRST	=>  Location: PIN_M23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- INC	=>  Location: PIN_AB28,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF CodeDisplay IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_INC : std_logic;
SIGNAL ww_SS : std_logic;
SIGNAL ww_nRST : std_logic;
SIGNAL ww_Err : std_logic;
SIGNAL ww_LCD_D : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_LCD_RW : std_logic;
SIGNAL ww_LCD_RS : std_logic;
SIGNAL ww_LCD_EN : std_logic;
SIGNAL ww_LED_Counter : std_logic_vector(3 DOWNTO 0);
SIGNAL \SS~input_o\ : std_logic;
SIGNAL \Err~output_o\ : std_logic;
SIGNAL \LCD_D[0]~output_o\ : std_logic;
SIGNAL \LCD_D[1]~output_o\ : std_logic;
SIGNAL \LCD_D[2]~output_o\ : std_logic;
SIGNAL \LCD_D[3]~output_o\ : std_logic;
SIGNAL \LCD_D[4]~output_o\ : std_logic;
SIGNAL \LCD_D[5]~output_o\ : std_logic;
SIGNAL \LCD_D[6]~output_o\ : std_logic;
SIGNAL \LCD_D[7]~output_o\ : std_logic;
SIGNAL \LCD_RW~output_o\ : std_logic;
SIGNAL \LCD_RS~output_o\ : std_logic;
SIGNAL \LCD_EN~output_o\ : std_logic;
SIGNAL \LED_Counter[0]~output_o\ : std_logic;
SIGNAL \LED_Counter[1]~output_o\ : std_logic;
SIGNAL \LED_Counter[2]~output_o\ : std_logic;
SIGNAL \LED_Counter[3]~output_o\ : std_logic;
SIGNAL \INC~input_o\ : std_logic;
SIGNAL \Count|Count:c[1]~0_combout\ : std_logic;
SIGNAL \nRST~input_o\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \s_rst~combout\ : std_logic;
SIGNAL \Count|Count:c[1]~q\ : std_logic;
SIGNAL \Count|Count:c[2]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[2]~q\ : std_logic;
SIGNAL \Count|Count:c[3]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[3]~q\ : std_logic;
SIGNAL \Count|Count:c[0]~0_combout\ : std_logic;
SIGNAL \Count|Count:c[0]~q\ : std_logic;
SIGNAL \Count|Equal0~0_combout\ : std_logic;
SIGNAL \Count|s_ov~0_combout\ : std_logic;
SIGNAL \Count|s_ov~q\ : std_logic;
SIGNAL \Err~0_combout\ : std_logic;
SIGNAL \ALT_INV_s_rst~combout\ : std_logic;

BEGIN

ww_INC <= INC;
ww_SS <= SS;
ww_nRST <= nRST;
Err <= ww_Err;
LCD_D <= ww_LCD_D;
LCD_RW <= ww_LCD_RW;
LCD_RS <= ww_LCD_RS;
LCD_EN <= ww_LCD_EN;
LED_Counter <= ww_LED_Counter;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_s_rst~combout\ <= NOT \s_rst~combout\;

-- Location: IOOBUF_X69_Y73_N16
\Err~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Err~0_combout\,
	devoe => ww_devoe,
	o => \Err~output_o\);

-- Location: IOOBUF_X0_Y52_N16
\LCD_D[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_D[0]~output_o\);

-- Location: IOOBUF_X0_Y44_N9
\LCD_D[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_D[1]~output_o\);

-- Location: IOOBUF_X0_Y44_N2
\LCD_D[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_D[2]~output_o\);

-- Location: IOOBUF_X0_Y49_N9
\LCD_D[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_D[3]~output_o\);

-- Location: IOOBUF_X0_Y54_N9
\LCD_D[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_D[4]~output_o\);

-- Location: IOOBUF_X0_Y55_N23
\LCD_D[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_D[5]~output_o\);

-- Location: IOOBUF_X0_Y51_N16
\LCD_D[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_D[6]~output_o\);

-- Location: IOOBUF_X0_Y47_N2
\LCD_D[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_D[7]~output_o\);

-- Location: IOOBUF_X0_Y44_N23
\LCD_RW~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_RW~output_o\);

-- Location: IOOBUF_X0_Y44_N16
\LCD_RS~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_RS~output_o\);

-- Location: IOOBUF_X0_Y52_N2
\LCD_EN~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LCD_EN~output_o\);

-- Location: IOOBUF_X107_Y73_N9
\LED_Counter[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Count|Count:c[0]~q\,
	devoe => ww_devoe,
	o => \LED_Counter[0]~output_o\);

-- Location: IOOBUF_X111_Y73_N9
\LED_Counter[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Count|Count:c[1]~q\,
	devoe => ww_devoe,
	o => \LED_Counter[1]~output_o\);

-- Location: IOOBUF_X83_Y73_N2
\LED_Counter[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Count|Count:c[2]~q\,
	devoe => ww_devoe,
	o => \LED_Counter[2]~output_o\);

-- Location: IOOBUF_X85_Y73_N23
\LED_Counter[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Count|Count:c[3]~q\,
	devoe => ww_devoe,
	o => \LED_Counter[3]~output_o\);

-- Location: IOIBUF_X115_Y17_N1
\INC~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_INC,
	o => \INC~input_o\);

-- Location: LCCOMB_X114_Y69_N2
\Count|Count:c[1]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Count:c[1]~0_combout\ = (\Count|Count:c[1]~q\ & (((\Count|Count:c[2]~q\ & \Count|Count:c[3]~q\)) # (!\Count|Count:c[0]~q\))) # (!\Count|Count:c[1]~q\ & (((\Count|Count:c[0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[2]~q\,
	datab => \Count|Count:c[3]~q\,
	datac => \Count|Count:c[1]~q\,
	datad => \Count|Count:c[0]~q\,
	combout => \Count|Count:c[1]~0_combout\);

-- Location: IOIBUF_X115_Y40_N8
\nRST~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_nRST,
	o => \nRST~input_o\);

-- Location: LCCOMB_X114_Y69_N6
\Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = (!\Count|Count:c[0]~q\ & (!\Count|Count:c[2]~q\ & (\Count|Count:c[3]~q\ & \Count|Count:c[1]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[0]~q\,
	datab => \Count|Count:c[2]~q\,
	datac => \Count|Count:c[3]~q\,
	datad => \Count|Count:c[1]~q\,
	combout => \Equal0~0_combout\);

-- Location: LCCOMB_X114_Y69_N8
s_rst : cycloneive_lcell_comb
-- Equation(s):
-- \s_rst~combout\ = (\Equal0~0_combout\) # (!\nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \nRST~input_o\,
	datad => \Equal0~0_combout\,
	combout => \s_rst~combout\);

-- Location: FF_X114_Y69_N3
\Count|Count:c[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \INC~input_o\,
	d => \Count|Count:c[1]~0_combout\,
	clrn => \ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[1]~q\);

-- Location: LCCOMB_X114_Y69_N4
\Count|Count:c[2]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Count:c[2]~0_combout\ = (\Count|Count:c[1]~q\ & ((\Count|Count:c[2]~q\ & ((\Count|Count:c[3]~q\) # (!\Count|Count:c[0]~q\))) # (!\Count|Count:c[2]~q\ & ((\Count|Count:c[0]~q\))))) # (!\Count|Count:c[1]~q\ & (((\Count|Count:c[2]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[1]~q\,
	datab => \Count|Count:c[3]~q\,
	datac => \Count|Count:c[2]~q\,
	datad => \Count|Count:c[0]~q\,
	combout => \Count|Count:c[2]~0_combout\);

-- Location: FF_X114_Y69_N5
\Count|Count:c[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \INC~input_o\,
	d => \Count|Count:c[2]~0_combout\,
	clrn => \ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[2]~q\);

-- Location: LCCOMB_X114_Y69_N30
\Count|Count:c[3]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Count:c[3]~0_combout\ = (\Count|Count:c[3]~q\) # ((\Count|Count:c[2]~q\ & (\Count|Count:c[1]~q\ & \Count|Count:c[0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[2]~q\,
	datab => \Count|Count:c[1]~q\,
	datac => \Count|Count:c[3]~q\,
	datad => \Count|Count:c[0]~q\,
	combout => \Count|Count:c[3]~0_combout\);

-- Location: FF_X114_Y69_N31
\Count|Count:c[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \INC~input_o\,
	d => \Count|Count:c[3]~0_combout\,
	clrn => \ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[3]~q\);

-- Location: LCCOMB_X114_Y69_N12
\Count|Count:c[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Count:c[0]~0_combout\ = ((\Count|Count:c[3]~q\ & (\Count|Count:c[1]~q\ & \Count|Count:c[2]~q\))) # (!\Count|Count:c[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[3]~q\,
	datab => \Count|Count:c[1]~q\,
	datac => \Count|Count:c[0]~q\,
	datad => \Count|Count:c[2]~q\,
	combout => \Count|Count:c[0]~0_combout\);

-- Location: FF_X114_Y69_N13
\Count|Count:c[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \INC~input_o\,
	d => \Count|Count:c[0]~0_combout\,
	clrn => \ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|Count:c[0]~q\);

-- Location: LCCOMB_X114_Y69_N20
\Count|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|Equal0~0_combout\ = (\Count|Count:c[0]~q\ & (\Count|Count:c[1]~q\ & (\Count|Count:c[3]~q\ & \Count|Count:c[2]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|Count:c[0]~q\,
	datab => \Count|Count:c[1]~q\,
	datac => \Count|Count:c[3]~q\,
	datad => \Count|Count:c[2]~q\,
	combout => \Count|Equal0~0_combout\);

-- Location: LCCOMB_X114_Y69_N28
\Count|s_ov~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Count|s_ov~0_combout\ = (\Count|s_ov~q\) # (\Count|Equal0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Count|s_ov~q\,
	datad => \Count|Equal0~0_combout\,
	combout => \Count|s_ov~0_combout\);

-- Location: FF_X114_Y69_N29
\Count|s_ov\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \INC~input_o\,
	d => \Count|s_ov~0_combout\,
	clrn => \ALT_INV_s_rst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Count|s_ov~q\);

-- Location: LCCOMB_X114_Y69_N26
\Err~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Err~0_combout\ = (\Count|s_ov~q\) # (!\nRST~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Count|s_ov~q\,
	datac => \nRST~input_o\,
	combout => \Err~0_combout\);

-- Location: IOIBUF_X115_Y9_N22
\SS~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SS,
	o => \SS~input_o\);

ww_Err <= \Err~output_o\;

ww_LCD_D(0) <= \LCD_D[0]~output_o\;

ww_LCD_D(1) <= \LCD_D[1]~output_o\;

ww_LCD_D(2) <= \LCD_D[2]~output_o\;

ww_LCD_D(3) <= \LCD_D[3]~output_o\;

ww_LCD_D(4) <= \LCD_D[4]~output_o\;

ww_LCD_D(5) <= \LCD_D[5]~output_o\;

ww_LCD_D(6) <= \LCD_D[6]~output_o\;

ww_LCD_D(7) <= \LCD_D[7]~output_o\;

ww_LCD_RW <= \LCD_RW~output_o\;

ww_LCD_RS <= \LCD_RS~output_o\;

ww_LCD_EN <= \LCD_EN~output_o\;

ww_LED_Counter(0) <= \LED_Counter[0]~output_o\;

ww_LED_Counter(1) <= \LED_Counter[1]~output_o\;

ww_LED_Counter(2) <= \LED_Counter[2]~output_o\;

ww_LED_Counter(3) <= \LED_Counter[3]~output_o\;
END structure;


