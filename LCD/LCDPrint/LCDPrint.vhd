LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY LCDPrint IS
	GENERIC (
		TWO_LINE_MODE				:	std_logic	:=	'1';
		ENABLE_INCREMENT			:	bit	:= '1';
		ENABLE_CURSOR				:	bit	:= '0';
		ENABLE_BLINK				:	bit	:= '0';
		ENABLE_SHIFT				:	bit	:=	'0'
	);
	PORT (
		nRST			:	IN		std_logic;
		Content		:	IN		std_logic_vector((((TWO_LINE_MODE + 1) * 16) - 1) downto 0);
		-- LCD Connections
		LCD_Data						:	OUT	std_logic_vector(7 downto 0);
		LCD_RS, LCD_RW, LCD_EN	:	OUT	std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF LCDPrint IS

BEGIN


END Behaviour;