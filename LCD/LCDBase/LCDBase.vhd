LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY LCDBase IS
	GENERIC (
		TWO_LINE_MODE				:	bit	:=	'1';
		ENABLE_INCREMENT			:	bit	:= '1';
		ENABLE_CURSOR				:	bit	:= '0';
		ENABLE_BLINK				:	bit	:= '0';
		ENABLE_SHIFT				:	bit	:=	'0'
	);
	PORT (
		nRST							:	IN		std_logic;
		BUSY							:	OUT	std_logic;
		-- LCD
		LCD_D							:	OUT	std_logic_vector(7 downto 0);
		LCD_RW, LCD_RS, LCD_EN	:	OUT	std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF LCDBase IS

	TYPE State 		IS (START, INIT, READY);
	TYPE InitState IS (Disp, Func, Entry, Ready);

	SIGNAL current_state			:	State			:= START;
	SIGNAL next_state				:	State			:= INIT;
	SIGNAL s_rw, s_rs, s_en		:	std_logic	:= '0';
	SIGNAL s_busy					:	std_logic	:= '0';
	
	SIGNAL s_dat					:	std_logic_vector(7 downto 0) := (Others => '0');
	
BEGIN

	P_START : PROCESS(nRST)
	
	BEGIN
		if ( falling_edge(nRST) ) then
		
		else
		
		end if;
	END PROCESS P_START;


	P_INIT : PROCESS(current_state)
		VARIABLE toWait		:	Integer		:= 0;
		VARIABLE state			:	InitState	:= Func;
	BEGIN
		if ( current_state = INIT ) then
			s_rw <= '0';
			s_rs <= '0';
			if ( s_busy = '0' ) then
				s_en <= '0';
				case state is
					when Func =>
						s_dat <= "00111100";
						s_dat(3) <= to_stdulogic(TWO_LINE_MODE);
						state := Disp;
						s_busy <= '1';
						s_en <= '1';
					when Disp => 
						s_dat <= "00001100";
						s_dat(1) <= to_stdulogic(ENABLE_CURSOR);
						s_dat(0) <= to_stdulogic(ENABLE_BLINK);
						state := Entry;
						s_busy <= '1';
						s_en <= '1';
					when Entry =>
						s_dat <= "00000100";
						s_dat(1) <= to_stdulogic(ENABLE_INCREMENT);
						s_dat(0) <= to_stdulogic(ENABLE_SHIFT);
						state := Ready;
						s_busy <= '1';
						s_en <= '1';
					when Ready =>
						s_rs <= '1';
						next_state <= READY;
				end case;
			else
				s_en <= '0';
			end if;
		else
		
		end if;
	END PROCESS P_INIT;

	P_WRITE : PROCESS(s_busy)
	
	BEGIN
		if ( s_busy = '1' ) then
			s_en <= '0';
			s_busy <= '0';
		else
		
		end if;
	END PROCESS P_WRITE;

	current_state <= current_state when ( s_en = '1' ) else next_state;
	

END Behaviour;