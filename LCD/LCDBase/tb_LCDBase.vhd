LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_LCDBase IS
END ENTITY;

ARCHITECTURE Testbench OF tb_LCDBase IS

	COMPONENT LCDBase IS
		GENERIC (
			TWO_LINE_MODE				:	bit	:=	'1';
			ENABLE_INCREMENT			:	bit	:= '1';
			ENABLE_CURSOR				:	bit	:= '0';
			ENABLE_BLINK				:	bit	:= '0';
			ENABLE_SHIFT				:	bit	:=	'0'
		);
		PORT (
			BUSY							:	OUT	std_logic;
			-- LCD
			LCD_D							:	OUT	std_logic_vector(7 downto 0);
			LCD_RW, LCD_RS, LCD_EN	:	OUT	std_logic
		);
	END COMPONENT;

	SIGNAL s_busy						:	std_logic	:= '0';
BEGIN

	DUT	:	LCDBase
	GENERIC MAP ('1', '1', '0', '0', '0')
	PORT MAP (
		s_busy
	);

END Testbench;