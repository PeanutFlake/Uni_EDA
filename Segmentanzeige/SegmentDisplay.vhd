LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY SegmentDisplay IS

	PORT (
		LTN, BLN		:	IN		std_logic;
		B				:	IN		std_logic_vector(3 downto 0);
		DISP			:	OUT	std_logic_vector(6 downto 0)
	);

END ENTITY;

ARCHITECTURE Behaviour OF SegmentDisplay IS

BEGIN

	DISP(0) <= ((NOT B(1) AND NOT B(3)) AND (B(0) XOR B(2))) OR ((B(0) AND B(3)) AND (B(1) XOR B(2)));
	DISP(1) <= (B(0) AND B(1) AND B(2)) OR (NOT B(0) AND B(1) AND B(2)) OR ((NOT B(1) AND B(2)) AND (B(0) XOR B(3)));
	DISP(2) <= (B(0) AND B(1) AND B(2)) OR ((NOT B(1) AND NOT B(3)) AND (B(0) XOR B(2))) OR (B(3) AND NOT B(2) AND B(1) AND NOT B(0));
	DISP(3) <= ((B(1) AND NOT B(0)) AND (B(3) XNOR B(2))) OR ((B(3) AND B(2)) AND (B(1) XNOR B(0)));
	DISP(4) <= (NOT B(3) AND NOT B(2) AND B(0)) OR (NOT B(3) AND B(2) AND (NOT (B(1) AND NOT B(0)))) OR (B(3) AND B(0) AND NOT B(2) AND NOT B(1));
	DISP(5) <= ((NOT B(3) AND NOT B(2)) AND (B(1) OR (NOT B(1) AND B(0)))) OR ((B(0) AND B(2)) AND (B(3) XOR B(1)));
	DISP(6) <= (NOT B(3) AND (B(2) XNOR B(1))) OR (B(3) AND B(2) AND NOT B(1) AND NOT B(0));

END Behaviour;