LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_SegmentDisplay IS
END ENTITY;

ARCHITECTURE Testbench OF tb_SegmentDisplay IS

	COMPONENT SegmentDisplay IS

		PORT (
			LTN, BLN		:	IN		std_logic;
			B				:	IN		std_logic_vector(3 downto 0);
			DISP			:	OUT	std_logic_vector(6 downto 0)
		);

	END COMPONENT;

	SIGNAL s_ltn, s_bln		:	std_logic	:= '0';
	SIGNAL s_b					:	std_logic_vector(3 downto 0) := "0000";
	SIGNAL s_disp				:	std_logic_vector(6 downto 0) := "0000000";
	
BEGIN

	DUT : SegmentDisplay
	PORT MAP (s_ltn, s_bln, s_b, s_disp);

	s_ltn <= '1';
	s_bln <= '0';
	
	s_b(0) <= NOT s_b(0) AFTER 20ns;
	s_b(1) <= NOT s_b(1) AFTER 40ns;
	s_b(2) <= NOT s_b(2) AFTER 80ns;
	s_b(3) <= NOT s_b(3) AFTER 160ns;
	
END Testbench;