-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/23/2018 08:39:16"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	SegmentDisplay IS
    PORT (
	LTN : IN std_logic;
	BLN : IN std_logic;
	B : IN std_logic_vector(3 DOWNTO 0);
	DISP : BUFFER std_logic_vector(6 DOWNTO 0)
	);
END SegmentDisplay;

-- Design Ports Information
-- LTN	=>  Location: PIN_L12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BLN	=>  Location: PIN_B6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DISP[0]	=>  Location: PIN_N6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DISP[1]	=>  Location: PIN_N4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DISP[2]	=>  Location: PIN_N8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DISP[3]	=>  Location: PIN_N13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DISP[4]	=>  Location: PIN_M6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DISP[5]	=>  Location: PIN_M13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DISP[6]	=>  Location: PIN_L5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[0]	=>  Location: PIN_M4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[3]	=>  Location: PIN_L7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[2]	=>  Location: PIN_L9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[1]	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF SegmentDisplay IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_LTN : std_logic;
SIGNAL ww_BLN : std_logic;
SIGNAL ww_B : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_DISP : std_logic_vector(6 DOWNTO 0);
SIGNAL \LTN~input_o\ : std_logic;
SIGNAL \BLN~input_o\ : std_logic;
SIGNAL \DISP[0]~output_o\ : std_logic;
SIGNAL \DISP[1]~output_o\ : std_logic;
SIGNAL \DISP[2]~output_o\ : std_logic;
SIGNAL \DISP[3]~output_o\ : std_logic;
SIGNAL \DISP[4]~output_o\ : std_logic;
SIGNAL \DISP[5]~output_o\ : std_logic;
SIGNAL \DISP[6]~output_o\ : std_logic;
SIGNAL \B[1]~input_o\ : std_logic;
SIGNAL \B[3]~input_o\ : std_logic;
SIGNAL \B[0]~input_o\ : std_logic;
SIGNAL \B[2]~input_o\ : std_logic;
SIGNAL \DISP~0_combout\ : std_logic;
SIGNAL \DISP~1_combout\ : std_logic;
SIGNAL \DISP~2_combout\ : std_logic;
SIGNAL \DISP~3_combout\ : std_logic;
SIGNAL \DISP~4_combout\ : std_logic;
SIGNAL \DISP~5_combout\ : std_logic;
SIGNAL \DISP~6_combout\ : std_logic;

BEGIN

ww_LTN <= LTN;
ww_BLN <= BLN;
ww_B <= B;
DISP <= ww_DISP;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: IOOBUF_X12_Y0_N2
\DISP[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISP~0_combout\,
	devoe => ww_devoe,
	o => \DISP[0]~output_o\);

-- Location: IOOBUF_X10_Y0_N9
\DISP[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISP~1_combout\,
	devoe => ww_devoe,
	o => \DISP[1]~output_o\);

-- Location: IOOBUF_X20_Y0_N9
\DISP[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISP~2_combout\,
	devoe => ww_devoe,
	o => \DISP[2]~output_o\);

-- Location: IOOBUF_X33_Y10_N9
\DISP[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISP~3_combout\,
	devoe => ww_devoe,
	o => \DISP[3]~output_o\);

-- Location: IOOBUF_X12_Y0_N9
\DISP[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISP~4_combout\,
	devoe => ww_devoe,
	o => \DISP[4]~output_o\);

-- Location: IOOBUF_X33_Y10_N2
\DISP[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISP~5_combout\,
	devoe => ww_devoe,
	o => \DISP[5]~output_o\);

-- Location: IOOBUF_X14_Y0_N9
\DISP[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DISP~6_combout\,
	devoe => ww_devoe,
	o => \DISP[6]~output_o\);

-- Location: IOIBUF_X22_Y0_N8
\B[1]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(1),
	o => \B[1]~input_o\);

-- Location: IOIBUF_X14_Y0_N1
\B[3]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(3),
	o => \B[3]~input_o\);

-- Location: IOIBUF_X8_Y0_N1
\B[0]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(0),
	o => \B[0]~input_o\);

-- Location: IOIBUF_X24_Y0_N8
\B[2]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(2),
	o => \B[2]~input_o\);

-- Location: LCCOMB_X14_Y1_N8
\DISP~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DISP~0_combout\ = (\B[3]~input_o\ & (\B[0]~input_o\ & (\B[1]~input_o\ $ (\B[2]~input_o\)))) # (!\B[3]~input_o\ & (!\B[1]~input_o\ & (\B[0]~input_o\ $ (\B[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \B[1]~input_o\,
	datab => \B[3]~input_o\,
	datac => \B[0]~input_o\,
	datad => \B[2]~input_o\,
	combout => \DISP~0_combout\);

-- Location: LCCOMB_X14_Y1_N26
\DISP~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DISP~1_combout\ = (\B[2]~input_o\ & ((\B[1]~input_o\) # (\B[3]~input_o\ $ (\B[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \B[1]~input_o\,
	datab => \B[3]~input_o\,
	datac => \B[0]~input_o\,
	datad => \B[2]~input_o\,
	combout => \DISP~1_combout\);

-- Location: LCCOMB_X14_Y1_N4
\DISP~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DISP~2_combout\ = (\B[1]~input_o\ & ((\B[0]~input_o\ & ((\B[2]~input_o\))) # (!\B[0]~input_o\ & (\B[3]~input_o\ & !\B[2]~input_o\)))) # (!\B[1]~input_o\ & (!\B[3]~input_o\ & (\B[0]~input_o\ $ (\B[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000100011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \B[1]~input_o\,
	datab => \B[3]~input_o\,
	datac => \B[0]~input_o\,
	datad => \B[2]~input_o\,
	combout => \DISP~2_combout\);

-- Location: LCCOMB_X14_Y1_N6
\DISP~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DISP~3_combout\ = (\B[3]~input_o\ & (\B[2]~input_o\ & ((\B[1]~input_o\) # (!\B[0]~input_o\)))) # (!\B[3]~input_o\ & (\B[1]~input_o\ & (!\B[0]~input_o\ & !\B[2]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \B[1]~input_o\,
	datab => \B[3]~input_o\,
	datac => \B[0]~input_o\,
	datad => \B[2]~input_o\,
	combout => \DISP~3_combout\);

-- Location: LCCOMB_X14_Y1_N0
\DISP~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DISP~4_combout\ = (\B[1]~input_o\ & (!\B[3]~input_o\ & (\B[0]~input_o\))) # (!\B[1]~input_o\ & ((\B[2]~input_o\ & (!\B[3]~input_o\)) # (!\B[2]~input_o\ & ((\B[0]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000101110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \B[1]~input_o\,
	datab => \B[3]~input_o\,
	datac => \B[0]~input_o\,
	datad => \B[2]~input_o\,
	combout => \DISP~4_combout\);

-- Location: LCCOMB_X14_Y1_N2
\DISP~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DISP~5_combout\ = (\B[1]~input_o\ & (!\B[3]~input_o\ & ((\B[0]~input_o\) # (!\B[2]~input_o\)))) # (!\B[1]~input_o\ & (\B[0]~input_o\ & (\B[3]~input_o\ $ (!\B[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000000110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \B[1]~input_o\,
	datab => \B[3]~input_o\,
	datac => \B[0]~input_o\,
	datad => \B[2]~input_o\,
	combout => \DISP~5_combout\);

-- Location: LCCOMB_X14_Y1_N12
\DISP~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \DISP~6_combout\ = (\B[3]~input_o\ & (!\B[1]~input_o\ & (!\B[0]~input_o\ & \B[2]~input_o\))) # (!\B[3]~input_o\ & (\B[1]~input_o\ $ (((!\B[2]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010011000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \B[1]~input_o\,
	datab => \B[3]~input_o\,
	datac => \B[0]~input_o\,
	datad => \B[2]~input_o\,
	combout => \DISP~6_combout\);

-- Location: IOIBUF_X33_Y12_N1
\LTN~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_LTN,
	o => \LTN~input_o\);

-- Location: IOIBUF_X14_Y31_N8
\BLN~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BLN,
	o => \BLN~input_o\);

ww_DISP(0) <= \DISP[0]~output_o\;

ww_DISP(1) <= \DISP[1]~output_o\;

ww_DISP(2) <= \DISP[2]~output_o\;

ww_DISP(3) <= \DISP[3]~output_o\;

ww_DISP(4) <= \DISP[4]~output_o\;

ww_DISP(5) <= \DISP[5]~output_o\;

ww_DISP(6) <= \DISP[6]~output_o\;
END structure;


