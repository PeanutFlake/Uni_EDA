LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_ClickNHold IS
END ENTITY;

ARCHITECTURE Testbench OF tb_ClickNHold IS
	COMPONENT ClickNHold IS
		GENERIC (
			HoldTime		:	Integer	:= 500
		);
		PORT (
			CLK, I, EN	:	IN		std_logic;
			DC, DH		:	OUT	std_logic
		);
	END COMPONENT;
	
	SIGNAL s_clk, s_i, s_en, s_dc, s_dh	:	std_logic	:= '0';
BEGIN

	DUT	:	ClickNHold
	GENERIC MAP (10)
	PORT MAP (s_clk, s_i, s_en, s_dc, s_dh);

	s_clk <= NOT s_clk AFTER 20ns;
	
	s_i <= '1' AFTER 50ns, '0' AFTER 120ns, '1' AFTER 160ns, '0' AFTER 1500ns;
	
	s_en <= '1' AFTER 0ns, '0' AFTER 1200ns, '1' AFTER 1300ns;
	
END Testbench;