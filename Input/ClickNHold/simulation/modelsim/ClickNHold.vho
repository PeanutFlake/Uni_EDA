-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/23/2018 19:34:13"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	ClickNHold IS
    PORT (
	CLK : IN std_logic;
	nI : IN std_logic;
	EN : IN std_logic;
	DC : OUT std_logic;
	DH : OUT std_logic
	);
END ClickNHold;

-- Design Ports Information
-- DC	=>  Location: PIN_F19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DH	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- nI	=>  Location: PIN_M23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- EN	=>  Location: PIN_AB28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF ClickNHold IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_nI : std_logic;
SIGNAL ww_EN : std_logic;
SIGNAL ww_DC : std_logic;
SIGNAL ww_DH : std_logic;
SIGNAL \CLK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \DC~output_o\ : std_logic;
SIGNAL \DH~output_o\ : std_logic;
SIGNAL \EN~input_o\ : std_logic;
SIGNAL \nI~input_o\ : std_logic;
SIGNAL \DC~0_combout\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \CLK~inputclkctrl_outclk\ : std_logic;
SIGNAL \HOLD:hTime[0]~4_combout\ : std_logic;
SIGNAL \HOLD:hTime[9]~2\ : std_logic;
SIGNAL \HOLD:hTime[10]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[10]~q\ : std_logic;
SIGNAL \HOLD:hTime[10]~2\ : std_logic;
SIGNAL \HOLD:hTime[11]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[11]~q\ : std_logic;
SIGNAL \HOLD:hTime[11]~2\ : std_logic;
SIGNAL \HOLD:hTime[12]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[12]~q\ : std_logic;
SIGNAL \HOLD:hTime[12]~2\ : std_logic;
SIGNAL \HOLD:hTime[13]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[13]~q\ : std_logic;
SIGNAL \HOLD:hTime[13]~2\ : std_logic;
SIGNAL \HOLD:hTime[14]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[14]~q\ : std_logic;
SIGNAL \HOLD:hTime[14]~2\ : std_logic;
SIGNAL \HOLD:hTime[15]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[15]~q\ : std_logic;
SIGNAL \HOLD:hTime[15]~2\ : std_logic;
SIGNAL \HOLD:hTime[16]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[16]~q\ : std_logic;
SIGNAL \HOLD:hTime[16]~2\ : std_logic;
SIGNAL \HOLD:hTime[17]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[17]~q\ : std_logic;
SIGNAL \HOLD:hTime[17]~2\ : std_logic;
SIGNAL \HOLD:hTime[18]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[18]~q\ : std_logic;
SIGNAL \HOLD:hTime[18]~2\ : std_logic;
SIGNAL \HOLD:hTime[19]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[19]~q\ : std_logic;
SIGNAL \HOLD:hTime[19]~2\ : std_logic;
SIGNAL \HOLD:hTime[20]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[20]~q\ : std_logic;
SIGNAL \HOLD:hTime[20]~2\ : std_logic;
SIGNAL \HOLD:hTime[21]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[21]~q\ : std_logic;
SIGNAL \HOLD:hTime[21]~2\ : std_logic;
SIGNAL \HOLD:hTime[22]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[22]~q\ : std_logic;
SIGNAL \HOLD:hTime[22]~2\ : std_logic;
SIGNAL \HOLD:hTime[23]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[23]~q\ : std_logic;
SIGNAL \HOLD:hTime[23]~2\ : std_logic;
SIGNAL \HOLD:hTime[24]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[24]~q\ : std_logic;
SIGNAL \HOLD:hTime[0]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[0]~2_combout\ : std_logic;
SIGNAL \HOLD:hTime[0]~3_combout\ : std_logic;
SIGNAL \HOLD:hTime[0]~6_combout\ : std_logic;
SIGNAL \HOLD:hTime[0]~7_combout\ : std_logic;
SIGNAL \HOLD:hTime[0]~8_combout\ : std_logic;
SIGNAL \HOLD:hTime[0]~q\ : std_logic;
SIGNAL \HOLD:hTime[0]~5\ : std_logic;
SIGNAL \HOLD:hTime[1]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[1]~q\ : std_logic;
SIGNAL \HOLD:hTime[1]~2\ : std_logic;
SIGNAL \HOLD:hTime[2]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[2]~q\ : std_logic;
SIGNAL \HOLD:hTime[2]~2\ : std_logic;
SIGNAL \HOLD:hTime[3]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[3]~q\ : std_logic;
SIGNAL \HOLD:hTime[3]~2\ : std_logic;
SIGNAL \HOLD:hTime[4]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[4]~q\ : std_logic;
SIGNAL \HOLD:hTime[4]~2\ : std_logic;
SIGNAL \HOLD:hTime[5]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[5]~q\ : std_logic;
SIGNAL \HOLD:hTime[5]~2\ : std_logic;
SIGNAL \HOLD:hTime[6]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[6]~q\ : std_logic;
SIGNAL \HOLD:hTime[6]~2\ : std_logic;
SIGNAL \HOLD:hTime[7]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[7]~q\ : std_logic;
SIGNAL \HOLD:hTime[7]~2\ : std_logic;
SIGNAL \HOLD:hTime[8]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[8]~q\ : std_logic;
SIGNAL \HOLD:hTime[8]~2\ : std_logic;
SIGNAL \HOLD:hTime[9]~1_combout\ : std_logic;
SIGNAL \HOLD:hTime[9]~q\ : std_logic;
SIGNAL \hold~0_combout\ : std_logic;
SIGNAL \hold~3_combout\ : std_logic;
SIGNAL \hold~1_combout\ : std_logic;
SIGNAL \hold~2_combout\ : std_logic;
SIGNAL \hold~4_combout\ : std_logic;
SIGNAL \hold~5_combout\ : std_logic;
SIGNAL \HOLD:hold~q\ : std_logic;
SIGNAL \DH~0_combout\ : std_logic;
SIGNAL \ALT_INV_DC~0_combout\ : std_logic;

BEGIN

ww_CLK <= CLK;
ww_nI <= nI;
ww_EN <= EN;
DC <= ww_DC;
DH <= ww_DH;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLK~input_o\);
\ALT_INV_DC~0_combout\ <= NOT \DC~0_combout\;

-- Location: IOOBUF_X94_Y73_N2
\DC~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_DC~0_combout\,
	devoe => ww_devoe,
	o => \DC~output_o\);

-- Location: IOOBUF_X69_Y73_N16
\DH~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \DH~0_combout\,
	devoe => ww_devoe,
	o => \DH~output_o\);

-- Location: IOIBUF_X115_Y17_N1
\EN~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_EN,
	o => \EN~input_o\);

-- Location: IOIBUF_X115_Y40_N8
\nI~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_nI,
	o => \nI~input_o\);

-- Location: LCCOMB_X89_Y34_N0
\DC~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DC~0_combout\ = (\nI~input_o\) # (!\EN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \EN~input_o\,
	datad => \nI~input_o\,
	combout => \DC~0_combout\);

-- Location: IOIBUF_X0_Y36_N15
\CLK~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: CLKCTRL_G4
\CLK~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLK~inputclkctrl_outclk\);

-- Location: LCCOMB_X89_Y34_N8
\HOLD:hTime[0]~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[0]~4_combout\ = \HOLD:hTime[0]~q\ $ (VCC)
-- \HOLD:hTime[0]~5\ = CARRY(\HOLD:hTime[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[0]~q\,
	datad => VCC,
	combout => \HOLD:hTime[0]~4_combout\,
	cout => \HOLD:hTime[0]~5\);

-- Location: LCCOMB_X89_Y34_N26
\HOLD:hTime[9]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[9]~1_combout\ = (\HOLD:hTime[9]~q\ & (!\HOLD:hTime[8]~2\)) # (!\HOLD:hTime[9]~q\ & ((\HOLD:hTime[8]~2\) # (GND)))
-- \HOLD:hTime[9]~2\ = CARRY((!\HOLD:hTime[8]~2\) # (!\HOLD:hTime[9]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[9]~q\,
	datad => VCC,
	cin => \HOLD:hTime[8]~2\,
	combout => \HOLD:hTime[9]~1_combout\,
	cout => \HOLD:hTime[9]~2\);

-- Location: LCCOMB_X89_Y34_N28
\HOLD:hTime[10]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[10]~1_combout\ = (\HOLD:hTime[10]~q\ & (\HOLD:hTime[9]~2\ $ (GND))) # (!\HOLD:hTime[10]~q\ & (!\HOLD:hTime[9]~2\ & VCC))
-- \HOLD:hTime[10]~2\ = CARRY((\HOLD:hTime[10]~q\ & !\HOLD:hTime[9]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[10]~q\,
	datad => VCC,
	cin => \HOLD:hTime[9]~2\,
	combout => \HOLD:hTime[10]~1_combout\,
	cout => \HOLD:hTime[10]~2\);

-- Location: FF_X89_Y34_N29
\HOLD:hTime[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[10]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[10]~q\);

-- Location: LCCOMB_X89_Y34_N30
\HOLD:hTime[11]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[11]~1_combout\ = (\HOLD:hTime[11]~q\ & (!\HOLD:hTime[10]~2\)) # (!\HOLD:hTime[11]~q\ & ((\HOLD:hTime[10]~2\) # (GND)))
-- \HOLD:hTime[11]~2\ = CARRY((!\HOLD:hTime[10]~2\) # (!\HOLD:hTime[11]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[11]~q\,
	datad => VCC,
	cin => \HOLD:hTime[10]~2\,
	combout => \HOLD:hTime[11]~1_combout\,
	cout => \HOLD:hTime[11]~2\);

-- Location: FF_X88_Y34_N21
\HOLD:hTime[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \HOLD:hTime[11]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	sload => VCC,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[11]~q\);

-- Location: LCCOMB_X89_Y33_N0
\HOLD:hTime[12]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[12]~1_combout\ = (\HOLD:hTime[12]~q\ & (\HOLD:hTime[11]~2\ $ (GND))) # (!\HOLD:hTime[12]~q\ & (!\HOLD:hTime[11]~2\ & VCC))
-- \HOLD:hTime[12]~2\ = CARRY((\HOLD:hTime[12]~q\ & !\HOLD:hTime[11]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[12]~q\,
	datad => VCC,
	cin => \HOLD:hTime[11]~2\,
	combout => \HOLD:hTime[12]~1_combout\,
	cout => \HOLD:hTime[12]~2\);

-- Location: FF_X88_Y34_N23
\HOLD:hTime[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \HOLD:hTime[12]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	sload => VCC,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[12]~q\);

-- Location: LCCOMB_X89_Y33_N2
\HOLD:hTime[13]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[13]~1_combout\ = (\HOLD:hTime[13]~q\ & (!\HOLD:hTime[12]~2\)) # (!\HOLD:hTime[13]~q\ & ((\HOLD:hTime[12]~2\) # (GND)))
-- \HOLD:hTime[13]~2\ = CARRY((!\HOLD:hTime[12]~2\) # (!\HOLD:hTime[13]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[13]~q\,
	datad => VCC,
	cin => \HOLD:hTime[12]~2\,
	combout => \HOLD:hTime[13]~1_combout\,
	cout => \HOLD:hTime[13]~2\);

-- Location: FF_X88_Y34_N9
\HOLD:hTime[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \HOLD:hTime[13]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	sload => VCC,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[13]~q\);

-- Location: LCCOMB_X89_Y33_N4
\HOLD:hTime[14]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[14]~1_combout\ = (\HOLD:hTime[14]~q\ & (\HOLD:hTime[13]~2\ $ (GND))) # (!\HOLD:hTime[14]~q\ & (!\HOLD:hTime[13]~2\ & VCC))
-- \HOLD:hTime[14]~2\ = CARRY((\HOLD:hTime[14]~q\ & !\HOLD:hTime[13]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[14]~q\,
	datad => VCC,
	cin => \HOLD:hTime[13]~2\,
	combout => \HOLD:hTime[14]~1_combout\,
	cout => \HOLD:hTime[14]~2\);

-- Location: FF_X88_Y34_N15
\HOLD:hTime[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \HOLD:hTime[14]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	sload => VCC,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[14]~q\);

-- Location: LCCOMB_X89_Y33_N6
\HOLD:hTime[15]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[15]~1_combout\ = (\HOLD:hTime[15]~q\ & (!\HOLD:hTime[14]~2\)) # (!\HOLD:hTime[15]~q\ & ((\HOLD:hTime[14]~2\) # (GND)))
-- \HOLD:hTime[15]~2\ = CARRY((!\HOLD:hTime[14]~2\) # (!\HOLD:hTime[15]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[15]~q\,
	datad => VCC,
	cin => \HOLD:hTime[14]~2\,
	combout => \HOLD:hTime[15]~1_combout\,
	cout => \HOLD:hTime[15]~2\);

-- Location: FF_X88_Y34_N29
\HOLD:hTime[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \HOLD:hTime[15]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	sload => VCC,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[15]~q\);

-- Location: LCCOMB_X89_Y33_N8
\HOLD:hTime[16]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[16]~1_combout\ = (\HOLD:hTime[16]~q\ & (\HOLD:hTime[15]~2\ $ (GND))) # (!\HOLD:hTime[16]~q\ & (!\HOLD:hTime[15]~2\ & VCC))
-- \HOLD:hTime[16]~2\ = CARRY((\HOLD:hTime[16]~q\ & !\HOLD:hTime[15]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[16]~q\,
	datad => VCC,
	cin => \HOLD:hTime[15]~2\,
	combout => \HOLD:hTime[16]~1_combout\,
	cout => \HOLD:hTime[16]~2\);

-- Location: FF_X88_Y34_N11
\HOLD:hTime[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \HOLD:hTime[16]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	sload => VCC,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[16]~q\);

-- Location: LCCOMB_X89_Y33_N10
\HOLD:hTime[17]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[17]~1_combout\ = (\HOLD:hTime[17]~q\ & (!\HOLD:hTime[16]~2\)) # (!\HOLD:hTime[17]~q\ & ((\HOLD:hTime[16]~2\) # (GND)))
-- \HOLD:hTime[17]~2\ = CARRY((!\HOLD:hTime[16]~2\) # (!\HOLD:hTime[17]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[17]~q\,
	datad => VCC,
	cin => \HOLD:hTime[16]~2\,
	combout => \HOLD:hTime[17]~1_combout\,
	cout => \HOLD:hTime[17]~2\);

-- Location: FF_X88_Y34_N19
\HOLD:hTime[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \HOLD:hTime[17]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	sload => VCC,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[17]~q\);

-- Location: LCCOMB_X89_Y33_N12
\HOLD:hTime[18]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[18]~1_combout\ = (\HOLD:hTime[18]~q\ & (\HOLD:hTime[17]~2\ $ (GND))) # (!\HOLD:hTime[18]~q\ & (!\HOLD:hTime[17]~2\ & VCC))
-- \HOLD:hTime[18]~2\ = CARRY((\HOLD:hTime[18]~q\ & !\HOLD:hTime[17]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[18]~q\,
	datad => VCC,
	cin => \HOLD:hTime[17]~2\,
	combout => \HOLD:hTime[18]~1_combout\,
	cout => \HOLD:hTime[18]~2\);

-- Location: FF_X89_Y33_N13
\HOLD:hTime[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[18]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[18]~q\);

-- Location: LCCOMB_X89_Y33_N14
\HOLD:hTime[19]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[19]~1_combout\ = (\HOLD:hTime[19]~q\ & (!\HOLD:hTime[18]~2\)) # (!\HOLD:hTime[19]~q\ & ((\HOLD:hTime[18]~2\) # (GND)))
-- \HOLD:hTime[19]~2\ = CARRY((!\HOLD:hTime[18]~2\) # (!\HOLD:hTime[19]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[19]~q\,
	datad => VCC,
	cin => \HOLD:hTime[18]~2\,
	combout => \HOLD:hTime[19]~1_combout\,
	cout => \HOLD:hTime[19]~2\);

-- Location: FF_X89_Y33_N15
\HOLD:hTime[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[19]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[19]~q\);

-- Location: LCCOMB_X89_Y33_N16
\HOLD:hTime[20]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[20]~1_combout\ = (\HOLD:hTime[20]~q\ & (\HOLD:hTime[19]~2\ $ (GND))) # (!\HOLD:hTime[20]~q\ & (!\HOLD:hTime[19]~2\ & VCC))
-- \HOLD:hTime[20]~2\ = CARRY((\HOLD:hTime[20]~q\ & !\HOLD:hTime[19]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[20]~q\,
	datad => VCC,
	cin => \HOLD:hTime[19]~2\,
	combout => \HOLD:hTime[20]~1_combout\,
	cout => \HOLD:hTime[20]~2\);

-- Location: FF_X89_Y33_N17
\HOLD:hTime[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[20]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[20]~q\);

-- Location: LCCOMB_X89_Y33_N18
\HOLD:hTime[21]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[21]~1_combout\ = (\HOLD:hTime[21]~q\ & (!\HOLD:hTime[20]~2\)) # (!\HOLD:hTime[21]~q\ & ((\HOLD:hTime[20]~2\) # (GND)))
-- \HOLD:hTime[21]~2\ = CARRY((!\HOLD:hTime[20]~2\) # (!\HOLD:hTime[21]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[21]~q\,
	datad => VCC,
	cin => \HOLD:hTime[20]~2\,
	combout => \HOLD:hTime[21]~1_combout\,
	cout => \HOLD:hTime[21]~2\);

-- Location: FF_X89_Y33_N19
\HOLD:hTime[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[21]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[21]~q\);

-- Location: LCCOMB_X89_Y33_N20
\HOLD:hTime[22]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[22]~1_combout\ = (\HOLD:hTime[22]~q\ & (\HOLD:hTime[21]~2\ $ (GND))) # (!\HOLD:hTime[22]~q\ & (!\HOLD:hTime[21]~2\ & VCC))
-- \HOLD:hTime[22]~2\ = CARRY((\HOLD:hTime[22]~q\ & !\HOLD:hTime[21]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[22]~q\,
	datad => VCC,
	cin => \HOLD:hTime[21]~2\,
	combout => \HOLD:hTime[22]~1_combout\,
	cout => \HOLD:hTime[22]~2\);

-- Location: FF_X89_Y33_N21
\HOLD:hTime[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[22]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[22]~q\);

-- Location: LCCOMB_X89_Y33_N22
\HOLD:hTime[23]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[23]~1_combout\ = (\HOLD:hTime[23]~q\ & (!\HOLD:hTime[22]~2\)) # (!\HOLD:hTime[23]~q\ & ((\HOLD:hTime[22]~2\) # (GND)))
-- \HOLD:hTime[23]~2\ = CARRY((!\HOLD:hTime[22]~2\) # (!\HOLD:hTime[23]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[23]~q\,
	datad => VCC,
	cin => \HOLD:hTime[22]~2\,
	combout => \HOLD:hTime[23]~1_combout\,
	cout => \HOLD:hTime[23]~2\);

-- Location: FF_X89_Y33_N23
\HOLD:hTime[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[23]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[23]~q\);

-- Location: LCCOMB_X89_Y33_N24
\HOLD:hTime[24]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[24]~1_combout\ = \HOLD:hTime[23]~2\ $ (!\HOLD:hTime[24]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \HOLD:hTime[24]~q\,
	cin => \HOLD:hTime[23]~2\,
	combout => \HOLD:hTime[24]~1_combout\);

-- Location: FF_X89_Y33_N25
\HOLD:hTime[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[24]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[24]~q\);

-- Location: LCCOMB_X89_Y33_N30
\HOLD:hTime[0]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[0]~1_combout\ = (\HOLD:hTime[18]~q\ & (\HOLD:hTime[21]~q\ & (\HOLD:hTime[19]~q\ & \HOLD:hTime[20]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[18]~q\,
	datab => \HOLD:hTime[21]~q\,
	datac => \HOLD:hTime[19]~q\,
	datad => \HOLD:hTime[20]~q\,
	combout => \HOLD:hTime[0]~1_combout\);

-- Location: LCCOMB_X89_Y33_N28
\HOLD:hTime[0]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[0]~2_combout\ = (\HOLD:hTime[0]~1_combout\ & \HOLD:hTime[22]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \HOLD:hTime[0]~1_combout\,
	datad => \HOLD:hTime[22]~q\,
	combout => \HOLD:hTime[0]~2_combout\);

-- Location: LCCOMB_X88_Y34_N26
\HOLD:hTime[0]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[0]~3_combout\ = (\HOLD:hTime[12]~q\ & (\HOLD:hTime[11]~q\ & (\HOLD:hTime[13]~q\ & \HOLD:hTime[14]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[12]~q\,
	datab => \HOLD:hTime[11]~q\,
	datac => \HOLD:hTime[13]~q\,
	datad => \HOLD:hTime[14]~q\,
	combout => \HOLD:hTime[0]~3_combout\);

-- Location: LCCOMB_X88_Y34_N2
\HOLD:hTime[0]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[0]~6_combout\ = (\HOLD:hTime[15]~q\) # ((\HOLD:hTime[0]~3_combout\ & ((\HOLD:hTime[6]~q\) # (!\hold~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[0]~3_combout\,
	datab => \HOLD:hTime[15]~q\,
	datac => \HOLD:hTime[6]~q\,
	datad => \hold~0_combout\,
	combout => \HOLD:hTime[0]~6_combout\);

-- Location: LCCOMB_X88_Y34_N16
\HOLD:hTime[0]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[0]~7_combout\ = (\HOLD:hTime[0]~2_combout\ & ((\HOLD:hTime[17]~q\) # ((\HOLD:hTime[16]~q\ & \HOLD:hTime[0]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[16]~q\,
	datab => \HOLD:hTime[17]~q\,
	datac => \HOLD:hTime[0]~2_combout\,
	datad => \HOLD:hTime[0]~6_combout\,
	combout => \HOLD:hTime[0]~7_combout\);

-- Location: LCCOMB_X88_Y34_N30
\HOLD:hTime[0]~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[0]~8_combout\ = (\nI~input_o\) # (((!\HOLD:hTime[23]~q\ & !\HOLD:hTime[0]~7_combout\)) # (!\HOLD:hTime[24]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nI~input_o\,
	datab => \HOLD:hTime[24]~q\,
	datac => \HOLD:hTime[23]~q\,
	datad => \HOLD:hTime[0]~7_combout\,
	combout => \HOLD:hTime[0]~8_combout\);

-- Location: FF_X89_Y34_N9
\HOLD:hTime[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[0]~4_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[0]~q\);

-- Location: LCCOMB_X89_Y34_N10
\HOLD:hTime[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[1]~1_combout\ = (\HOLD:hTime[1]~q\ & (!\HOLD:hTime[0]~5\)) # (!\HOLD:hTime[1]~q\ & ((\HOLD:hTime[0]~5\) # (GND)))
-- \HOLD:hTime[1]~2\ = CARRY((!\HOLD:hTime[0]~5\) # (!\HOLD:hTime[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[1]~q\,
	datad => VCC,
	cin => \HOLD:hTime[0]~5\,
	combout => \HOLD:hTime[1]~1_combout\,
	cout => \HOLD:hTime[1]~2\);

-- Location: FF_X89_Y34_N11
\HOLD:hTime[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[1]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[1]~q\);

-- Location: LCCOMB_X89_Y34_N12
\HOLD:hTime[2]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[2]~1_combout\ = (\HOLD:hTime[2]~q\ & (\HOLD:hTime[1]~2\ $ (GND))) # (!\HOLD:hTime[2]~q\ & (!\HOLD:hTime[1]~2\ & VCC))
-- \HOLD:hTime[2]~2\ = CARRY((\HOLD:hTime[2]~q\ & !\HOLD:hTime[1]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[2]~q\,
	datad => VCC,
	cin => \HOLD:hTime[1]~2\,
	combout => \HOLD:hTime[2]~1_combout\,
	cout => \HOLD:hTime[2]~2\);

-- Location: FF_X89_Y34_N13
\HOLD:hTime[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[2]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[2]~q\);

-- Location: LCCOMB_X89_Y34_N14
\HOLD:hTime[3]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[3]~1_combout\ = (\HOLD:hTime[3]~q\ & (!\HOLD:hTime[2]~2\)) # (!\HOLD:hTime[3]~q\ & ((\HOLD:hTime[2]~2\) # (GND)))
-- \HOLD:hTime[3]~2\ = CARRY((!\HOLD:hTime[2]~2\) # (!\HOLD:hTime[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[3]~q\,
	datad => VCC,
	cin => \HOLD:hTime[2]~2\,
	combout => \HOLD:hTime[3]~1_combout\,
	cout => \HOLD:hTime[3]~2\);

-- Location: FF_X89_Y34_N15
\HOLD:hTime[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[3]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[3]~q\);

-- Location: LCCOMB_X89_Y34_N16
\HOLD:hTime[4]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[4]~1_combout\ = (\HOLD:hTime[4]~q\ & (\HOLD:hTime[3]~2\ $ (GND))) # (!\HOLD:hTime[4]~q\ & (!\HOLD:hTime[3]~2\ & VCC))
-- \HOLD:hTime[4]~2\ = CARRY((\HOLD:hTime[4]~q\ & !\HOLD:hTime[3]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[4]~q\,
	datad => VCC,
	cin => \HOLD:hTime[3]~2\,
	combout => \HOLD:hTime[4]~1_combout\,
	cout => \HOLD:hTime[4]~2\);

-- Location: FF_X89_Y34_N17
\HOLD:hTime[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[4]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[4]~q\);

-- Location: LCCOMB_X89_Y34_N18
\HOLD:hTime[5]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[5]~1_combout\ = (\HOLD:hTime[5]~q\ & (!\HOLD:hTime[4]~2\)) # (!\HOLD:hTime[5]~q\ & ((\HOLD:hTime[4]~2\) # (GND)))
-- \HOLD:hTime[5]~2\ = CARRY((!\HOLD:hTime[4]~2\) # (!\HOLD:hTime[5]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[5]~q\,
	datad => VCC,
	cin => \HOLD:hTime[4]~2\,
	combout => \HOLD:hTime[5]~1_combout\,
	cout => \HOLD:hTime[5]~2\);

-- Location: FF_X89_Y34_N19
\HOLD:hTime[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[5]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[5]~q\);

-- Location: LCCOMB_X89_Y34_N20
\HOLD:hTime[6]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[6]~1_combout\ = (\HOLD:hTime[6]~q\ & (\HOLD:hTime[5]~2\ $ (GND))) # (!\HOLD:hTime[6]~q\ & (!\HOLD:hTime[5]~2\ & VCC))
-- \HOLD:hTime[6]~2\ = CARRY((\HOLD:hTime[6]~q\ & !\HOLD:hTime[5]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[6]~q\,
	datad => VCC,
	cin => \HOLD:hTime[5]~2\,
	combout => \HOLD:hTime[6]~1_combout\,
	cout => \HOLD:hTime[6]~2\);

-- Location: FF_X88_Y34_N5
\HOLD:hTime[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	asdata => \HOLD:hTime[6]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	sload => VCC,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[6]~q\);

-- Location: LCCOMB_X89_Y34_N22
\HOLD:hTime[7]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[7]~1_combout\ = (\HOLD:hTime[7]~q\ & (!\HOLD:hTime[6]~2\)) # (!\HOLD:hTime[7]~q\ & ((\HOLD:hTime[6]~2\) # (GND)))
-- \HOLD:hTime[7]~2\ = CARRY((!\HOLD:hTime[6]~2\) # (!\HOLD:hTime[7]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[7]~q\,
	datad => VCC,
	cin => \HOLD:hTime[6]~2\,
	combout => \HOLD:hTime[7]~1_combout\,
	cout => \HOLD:hTime[7]~2\);

-- Location: FF_X89_Y34_N23
\HOLD:hTime[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[7]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[7]~q\);

-- Location: LCCOMB_X89_Y34_N24
\HOLD:hTime[8]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \HOLD:hTime[8]~1_combout\ = (\HOLD:hTime[8]~q\ & (\HOLD:hTime[7]~2\ $ (GND))) # (!\HOLD:hTime[8]~q\ & (!\HOLD:hTime[7]~2\ & VCC))
-- \HOLD:hTime[8]~2\ = CARRY((\HOLD:hTime[8]~q\ & !\HOLD:hTime[7]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \HOLD:hTime[8]~q\,
	datad => VCC,
	cin => \HOLD:hTime[7]~2\,
	combout => \HOLD:hTime[8]~1_combout\,
	cout => \HOLD:hTime[8]~2\);

-- Location: FF_X89_Y34_N25
\HOLD:hTime[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[8]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[8]~q\);

-- Location: FF_X89_Y34_N27
\HOLD:hTime[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \HOLD:hTime[9]~1_combout\,
	clrn => \EN~input_o\,
	sclr => \nI~input_o\,
	ena => \HOLD:hTime[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hTime[9]~q\);

-- Location: LCCOMB_X89_Y34_N6
\hold~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \hold~0_combout\ = (!\HOLD:hTime[9]~q\ & (!\HOLD:hTime[8]~q\ & (!\HOLD:hTime[7]~q\ & !\HOLD:hTime[10]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[9]~q\,
	datab => \HOLD:hTime[8]~q\,
	datac => \HOLD:hTime[7]~q\,
	datad => \HOLD:hTime[10]~q\,
	combout => \hold~0_combout\);

-- Location: LCCOMB_X88_Y34_N6
\hold~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \hold~3_combout\ = (\HOLD:hTime[16]~q\ & (!\HOLD:hTime[15]~q\ & (\HOLD:hTime[6]~q\ & !\HOLD:hTime[17]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[16]~q\,
	datab => \HOLD:hTime[15]~q\,
	datac => \HOLD:hTime[6]~q\,
	datad => \HOLD:hTime[17]~q\,
	combout => \hold~3_combout\);

-- Location: LCCOMB_X88_Y34_N0
\hold~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \hold~1_combout\ = (!\nI~input_o\ & (!\HOLD:hTime[1]~q\ & (!\HOLD:hTime[0]~q\ & \HOLD:hTime[24]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \nI~input_o\,
	datab => \HOLD:hTime[1]~q\,
	datac => \HOLD:hTime[0]~q\,
	datad => \HOLD:hTime[24]~q\,
	combout => \hold~1_combout\);

-- Location: LCCOMB_X89_Y34_N4
\hold~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \hold~2_combout\ = (!\HOLD:hTime[3]~q\ & (!\HOLD:hTime[2]~q\ & (!\HOLD:hTime[5]~q\ & !\HOLD:hTime[4]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hTime[3]~q\,
	datab => \HOLD:hTime[2]~q\,
	datac => \HOLD:hTime[5]~q\,
	datad => \HOLD:hTime[4]~q\,
	combout => \hold~2_combout\);

-- Location: LCCOMB_X88_Y34_N24
\hold~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \hold~4_combout\ = (\hold~3_combout\ & (\hold~1_combout\ & (!\HOLD:hTime[23]~q\ & \hold~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \hold~3_combout\,
	datab => \hold~1_combout\,
	datac => \HOLD:hTime[23]~q\,
	datad => \hold~2_combout\,
	combout => \hold~4_combout\);

-- Location: LCCOMB_X88_Y34_N12
\hold~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \hold~5_combout\ = (\hold~0_combout\ & (\hold~4_combout\ & (\HOLD:hTime[0]~3_combout\ & \HOLD:hTime[0]~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \hold~0_combout\,
	datab => \hold~4_combout\,
	datac => \HOLD:hTime[0]~3_combout\,
	datad => \HOLD:hTime[0]~2_combout\,
	combout => \hold~5_combout\);

-- Location: FF_X88_Y34_N13
\HOLD:hold\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputclkctrl_outclk\,
	d => \hold~5_combout\,
	ena => \EN~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \HOLD:hold~q\);

-- Location: LCCOMB_X88_Y34_N4
\DH~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \DH~0_combout\ = (\HOLD:hold~q\ & \EN~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \HOLD:hold~q\,
	datad => \EN~input_o\,
	combout => \DH~0_combout\);

ww_DC <= \DC~output_o\;

ww_DH <= \DH~output_o\;
END structure;


