LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY ClickNHold IS
	GENERIC (
		HoldEdges	:	Integer	:= 50000000
	);
	PORT (
		CLK, nI, EN	:	IN		std_logic;
		DC, DH		:	OUT	std_logic
	);
END ENTITY;

ARCHITECTURE Behaviour OF ClickNHold IS
	SIGNAL I	:	std_logic := '0';
BEGIN

	I <= NOT nI;

CLICK:
	PROCESS (I, EN)
	
	BEGIN
		if ( EN = '1' ) then 
			if ( I = '1' ) then
				DC <= '1';
			else
				DC <= '0';
			end if;
		else
			DC <= '0';
		end if;
	END PROCESS CLICK;
	
HOLD:
	PROCESS (CLK, I, EN)
		VARIABLE hold	:	std_logic 							:= '0';
		VARIABLE hTime	:	integer range 0 to HoldEdges	:= 0;
	BEGIN
		if ( EN = '1' ) then
			if ( rising_edge(CLK) ) then
				if ( I = '1' ) then
					if ( hTime = HoldEdges ) then
						hold := '1';
					else
						hold := '0';
						if ( hTime < HoldEdges ) then
							hTime := hTime + 1;
						else
							hTime := hTime;
						end if;
					end if;
				else
					hold := '0';
					hTime := 0;
				end if;
			else
				hold := hold;
				hTime := hTime;
			end if;
			DH <= hold;
		else
			hTime := 0;
			DH <= '0';
		end if;
	END PROCESS HOLD;

END Behaviour;