LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY BCDCodec IS
	GENERIC (
		S				:	Integer range 1 to 10	:=	10
	);
	PORT (
		CLK, RST		:	IN		std_logic;
		Wert			:	IN		Integer;
		Data			:	OUT	std_logic_vector(S*4-1 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF BCDCodec IS

	

BEGIN

	Encode:
	PROCESS(CLK, RST, Wert)
		VARIABLE D		:	std_logic_vector(S*4-1 downto 0)	:= (Others => '0');
		VARIABLE N		:	Integer range 0 to 9	:= 0;
		VARIABLE P		:	Integer := 0;
		VARIABLE R		:	std_logic_vector(3 downto 0)	:= (Others => '0');
	BEGIN
	if ( RST = '1' ) then
		D := (Others => '0');
		N := 0;
		P := 0;
		R := (Others => '0');
	else
		if ( CLK'event AND CLK = '1' ) then
			for I in 0 to S-1 loop
				P := Wert;
				for J in 1 to S-I loop
					P := P/10;
				end loop;
				N := P;
				R := std_logic_vector(to_unsigned(N, R'length));
				D(I*R'length) := R(0);
				D(I*R'length+1) := R(1);
				D(I*R'length+2) := R(2);
				D(I*R'length+3) := R(3);
			end loop;
		else
			D := D;
		end if;
	end if;
	Data <= D;
	END PROCESS Encode;

END Behaviour;