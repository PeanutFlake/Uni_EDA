LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_BCDCodec IS
END ENTITY;

ARCHITECTURE Testbench OF tb_BCDCodec IS

	COMPONENT BCDCodec IS
		GENERIC (
			S				:	Integer range 1 to 10	:=	10
		);
		PORT (
			CLK, RST		:	IN		std_logic;
			Wert			:	IN		Integer;
			Data			:	OUT	std_logic_vector(S*4-1 downto 0)
		);
	END COMPONENT;

	SIGNAL s_clk, s_rst	:	std_logic	:= '0';
	SIGNAL s_val 			:	Integer;
	SIGNAL s_dat			:	std_logic_vector(15 downto 0) := (Others => '0');
	
BEGIN

	DUT	:	BCDCodec
	GENERIC MAP(4)
	PORT MAP (
		CLK => s_clk,
		RST => s_rst,
		Wert => s_val,
		Data => s_dat
	);

	s_clk <= NOT s_clk AFTER 20ns;
	s_rst <= '1' AFTER 120ns, '0' AFTER 150ns;
	s_val <= 15 AFTER 50ns, 53 AFTER 70ns, 81 AFTER 95ns;
	
	
	
END Testbench;