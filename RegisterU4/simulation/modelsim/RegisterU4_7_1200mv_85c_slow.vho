-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/07/2018 11:43:11"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	RegisterU4 IS
    PORT (
	CLK : IN std_logic;
	RST : IN std_logic;
	SHL : IN std_logic;
	SHR : IN std_logic;
	S : IN std_logic_vector(1 DOWNTO 0);
	D : IN std_logic_vector(3 DOWNTO 0);
	Q : OUT std_logic_vector(3 DOWNTO 0)
	);
END RegisterU4;

-- Design Ports Information
-- Q[0]	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[1]	=>  Location: PIN_F19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[2]	=>  Location: PIN_E19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[3]	=>  Location: PIN_F21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_M23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SHR	=>  Location: PIN_AC28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[0]	=>  Location: PIN_AA23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[0]	=>  Location: PIN_AD27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RST	=>  Location: PIN_AB28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[1]	=>  Location: PIN_AB27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[1]	=>  Location: PIN_AA22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[2]	=>  Location: PIN_Y24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[3]	=>  Location: PIN_Y23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SHL	=>  Location: PIN_AC27,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF RegisterU4 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_RST : std_logic;
SIGNAL ww_SHL : std_logic;
SIGNAL ww_SHR : std_logic;
SIGNAL ww_S : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_D : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_Q : std_logic_vector(3 DOWNTO 0);
SIGNAL \Q[0]~output_o\ : std_logic;
SIGNAL \Q[1]~output_o\ : std_logic;
SIGNAL \Q[2]~output_o\ : std_logic;
SIGNAL \Q[3]~output_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \S[0]~input_o\ : std_logic;
SIGNAL \D[0]~input_o\ : std_logic;
SIGNAL \SHR~input_o\ : std_logic;
SIGNAL \REG_GEN:1:FF|qn~0_combout\ : std_logic;
SIGNAL \REG_GEN:1:FF|qn~feeder_combout\ : std_logic;
SIGNAL \D[1]~input_o\ : std_logic;
SIGNAL \REG_GEN:2:FF|qn~0_combout\ : std_logic;
SIGNAL \D[2]~input_o\ : std_logic;
SIGNAL \REG_GEN:3:FF|qn~0_combout\ : std_logic;
SIGNAL \D[3]~input_o\ : std_logic;
SIGNAL \REG_GEN:4:FF|qn~0_combout\ : std_logic;
SIGNAL \SHL~input_o\ : std_logic;
SIGNAL \RST~input_o\ : std_logic;
SIGNAL \S[1]~input_o\ : std_logic;
SIGNAL \REG_GEN:1:FF|qn~1_combout\ : std_logic;
SIGNAL \REG_GEN:4:FF|qn~q\ : std_logic;
SIGNAL \REG_GEN:3:FF|qn~q\ : std_logic;
SIGNAL \REG_GEN:2:FF|qn~q\ : std_logic;
SIGNAL \REG_GEN:1:FF|qn~q\ : std_logic;
SIGNAL \ALT_INV_S[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_RST~input_o\ : std_logic;

BEGIN

ww_CLK <= CLK;
ww_RST <= RST;
ww_SHL <= SHL;
ww_SHR <= SHR;
ww_S <= S;
ww_D <= D;
Q <= ww_Q;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_S[1]~input_o\ <= NOT \S[1]~input_o\;
\ALT_INV_RST~input_o\ <= NOT \RST~input_o\;

-- Location: IOOBUF_X69_Y73_N16
\Q[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \REG_GEN:1:FF|qn~q\,
	devoe => ww_devoe,
	o => \Q[0]~output_o\);

-- Location: IOOBUF_X94_Y73_N2
\Q[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \REG_GEN:2:FF|qn~q\,
	devoe => ww_devoe,
	o => \Q[1]~output_o\);

-- Location: IOOBUF_X94_Y73_N9
\Q[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \REG_GEN:3:FF|qn~q\,
	devoe => ww_devoe,
	o => \Q[2]~output_o\);

-- Location: IOOBUF_X107_Y73_N16
\Q[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \REG_GEN:4:FF|qn~q\,
	devoe => ww_devoe,
	o => \Q[3]~output_o\);

-- Location: IOIBUF_X115_Y40_N8
\CLK~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: IOIBUF_X115_Y13_N8
\S[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S(0),
	o => \S[0]~input_o\);

-- Location: IOIBUF_X115_Y10_N8
\D[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(0),
	o => \D[0]~input_o\);

-- Location: IOIBUF_X115_Y14_N1
\SHR~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SHR,
	o => \SHR~input_o\);

-- Location: LCCOMB_X114_Y14_N28
\REG_GEN:1:FF|qn~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \REG_GEN:1:FF|qn~0_combout\ = (\S[0]~input_o\ & (\D[0]~input_o\)) # (!\S[0]~input_o\ & ((\SHR~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datac => \D[0]~input_o\,
	datad => \SHR~input_o\,
	combout => \REG_GEN:1:FF|qn~0_combout\);

-- Location: LCCOMB_X114_Y40_N28
\REG_GEN:1:FF|qn~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \REG_GEN:1:FF|qn~feeder_combout\ = \REG_GEN:1:FF|qn~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \REG_GEN:1:FF|qn~0_combout\,
	combout => \REG_GEN:1:FF|qn~feeder_combout\);

-- Location: IOIBUF_X115_Y6_N15
\D[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(1),
	o => \D[1]~input_o\);

-- Location: LCCOMB_X114_Y40_N22
\REG_GEN:2:FF|qn~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \REG_GEN:2:FF|qn~0_combout\ = (\S[0]~input_o\ & (\D[1]~input_o\)) # (!\S[0]~input_o\ & ((\REG_GEN:1:FF|qn~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[1]~input_o\,
	datab => \S[0]~input_o\,
	datad => \REG_GEN:1:FF|qn~q\,
	combout => \REG_GEN:2:FF|qn~0_combout\);

-- Location: IOIBUF_X115_Y13_N1
\D[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(2),
	o => \D[2]~input_o\);

-- Location: LCCOMB_X114_Y40_N4
\REG_GEN:3:FF|qn~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \REG_GEN:3:FF|qn~0_combout\ = (\S[0]~input_o\ & (\D[2]~input_o\)) # (!\S[0]~input_o\ & ((\REG_GEN:2:FF|qn~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[2]~input_o\,
	datab => \S[0]~input_o\,
	datad => \REG_GEN:2:FF|qn~q\,
	combout => \REG_GEN:3:FF|qn~0_combout\);

-- Location: IOIBUF_X115_Y14_N8
\D[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(3),
	o => \D[3]~input_o\);

-- Location: LCCOMB_X114_Y40_N30
\REG_GEN:4:FF|qn~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \REG_GEN:4:FF|qn~0_combout\ = (\S[0]~input_o\ & (\D[3]~input_o\)) # (!\S[0]~input_o\ & ((\REG_GEN:3:FF|qn~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \D[3]~input_o\,
	datab => \S[0]~input_o\,
	datad => \REG_GEN:3:FF|qn~q\,
	combout => \REG_GEN:4:FF|qn~0_combout\);

-- Location: IOIBUF_X115_Y15_N8
\SHL~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SHL,
	o => \SHL~input_o\);

-- Location: IOIBUF_X115_Y17_N1
\RST~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_RST,
	o => \RST~input_o\);

-- Location: IOIBUF_X115_Y18_N8
\S[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S(1),
	o => \S[1]~input_o\);

-- Location: LCCOMB_X114_Y40_N24
\REG_GEN:1:FF|qn~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \REG_GEN:1:FF|qn~1_combout\ = (\S[0]~input_o\) # (\S[1]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \S[0]~input_o\,
	datad => \S[1]~input_o\,
	combout => \REG_GEN:1:FF|qn~1_combout\);

-- Location: FF_X114_Y40_N31
\REG_GEN:4:FF|qn\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \REG_GEN:4:FF|qn~0_combout\,
	asdata => \SHL~input_o\,
	clrn => \ALT_INV_RST~input_o\,
	sload => \ALT_INV_S[1]~input_o\,
	ena => \REG_GEN:1:FF|qn~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REG_GEN:4:FF|qn~q\);

-- Location: FF_X114_Y40_N5
\REG_GEN:3:FF|qn\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \REG_GEN:3:FF|qn~0_combout\,
	asdata => \REG_GEN:4:FF|qn~q\,
	clrn => \ALT_INV_RST~input_o\,
	sload => \ALT_INV_S[1]~input_o\,
	ena => \REG_GEN:1:FF|qn~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REG_GEN:3:FF|qn~q\);

-- Location: FF_X114_Y40_N23
\REG_GEN:2:FF|qn\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \REG_GEN:2:FF|qn~0_combout\,
	asdata => \REG_GEN:3:FF|qn~q\,
	clrn => \ALT_INV_RST~input_o\,
	sload => \ALT_INV_S[1]~input_o\,
	ena => \REG_GEN:1:FF|qn~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REG_GEN:2:FF|qn~q\);

-- Location: FF_X114_Y40_N29
\REG_GEN:1:FF|qn\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~input_o\,
	d => \REG_GEN:1:FF|qn~feeder_combout\,
	asdata => \REG_GEN:2:FF|qn~q\,
	clrn => \ALT_INV_RST~input_o\,
	sload => \ALT_INV_S[1]~input_o\,
	ena => \REG_GEN:1:FF|qn~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REG_GEN:1:FF|qn~q\);

ww_Q(0) <= \Q[0]~output_o\;

ww_Q(1) <= \Q[1]~output_o\;

ww_Q(2) <= \Q[2]~output_o\;

ww_Q(3) <= \Q[3]~output_o\;
END structure;


