LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY RegisterU4 IS
	GENERIC (
		Bitsize		:	Integer	:= 4
	);
	PORT (
		CLK, RST, SHL, SHR	:			std_logic;
		S				:			std_logic_vector (1 downto 0);
		D				:			std_logic_vector (Bitsize-1 downto 0);
		Q				:	OUT	std_logic_vector (Bitsize-1 downto 0)
	);
END ENTITY;

ARCHITECTURE Behaviour OF RegisterU4 IS

	SIGNAL s_d0_q, s_d0_d, s_en	:	std_logic := '0';
	SIGNAL s_d, s_q					:	std_logic_vector(Bitsize+1 downto 0)	:=	(Others => '0');
	SIGNAL s_sl, s_sr					:  std_logic_vector(Bitsize+1 downto 0)	:= (Others => '0');
	
BEGIN
	
	s_en <= '1';
	
	REG_GEN:
	for I in 1 to Bitsize generate
		
		BEGIN
		
			s_sl(I-1) <= s_q(I);
			s_sr(I+1) <= s_q(I);
			
			MUX	:	entity work.Multiplexer4x1
			PORT MAP(
				A => s_q(I), 
				B => s_sl(I), 
				C => s_sr(I),
				D => D(I-1), 
				S => S, 
				Y => s_d(I));
			
			FF		:	entity work.DFlipFlop
			PORT MAP(D => s_d(I), CLK => CLK, CLR => RST, EN => s_en, Q => s_q(I));
			
			Q(I-1) <= s_q(I);
	end generate REG_GEN;
	
	s_sr(1) <= SHR;
	s_sl(Bitsize) <= SHL;
	
END Behaviour;