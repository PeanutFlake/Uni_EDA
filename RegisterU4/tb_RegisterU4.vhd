LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY tb_RegisterU4 IS
END ENTITY;

ARCHITECTURE Testbench OF tb_RegisterU4 IS

	SIGNAL s_clk, s_rst, s_shl, s_shr	:	std_logic	:= '0';
	SIGNAL s_d, s_q							:	std_logic_vector(3 downto 0) := (Others => '0');
	SIGNAL s_s									:	std_logic_vector(1 downto 0) := (Others => '0');
	
BEGIN

	DUT : entity work.RegisterU4
	GENERIC MAP (4)
	PORT MAP (s_clk, s_rst, s_shl, s_shr, s_s, s_d, s_q);

	s_clk <= NOT s_clk AFTER 20ns;
	
	s_d <= "1111" AFTER 0ns, "0000" AFTER 80ns;
	
	s_s <= "00" AFTER 0ns, "11" AFTER 50ns, "00" AFTER 70ns, "01" AFTER 120ns, "10" AFTER 160ns, "00" AFTER 210ns;
	
	s_rst <= '1' AFTER 200ns;
	
END Testbench;