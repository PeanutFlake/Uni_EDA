ENTITY Mehrheitsentscheider IS
	PORT (
		x, y, z	:	IN bit;
		f			:	OUT bit
	);
END ENTITY;

ARCHITECTURE Behaviour OF Mehrheitsentscheider IS

	SIGNAL s_yz	: bit := '0';

BEGIN

	s_yz <= y AND z;
	f <= (x AND (y OR z)) OR s_yz;

END Behaviour;