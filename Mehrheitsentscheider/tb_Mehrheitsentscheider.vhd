LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY tb_Mehrheitsentscheider IS
END ENTITY;

ARCHITECTURE Testbench OF tb_Mehrheitsentscheider IS

	COMPONENT Mehrheitsentscheider IS
		PORT (
			x, y, z	: IN bit;
			f			: OUT bit
		);
	END COMPONENT;

	SIGNAL s_x, s_y, s_z		: bit := '0';
	SIGNAL s_f					: bit := '0';
	SIGNAL s_test				: bit := '0';
	
	SIGNAL truthtable			: bit_vector (7 downto 0) := "11101000";
	SIGNAL b_t					: std_logic_vector (2 downto 0) := (Others => '0');
	SIGNAL b_n					: integer := 0;
	
BEGIN

	DUT	:	Mehrheitsentscheider
	PORT MAP (
		s_x, s_y, s_z, s_f
	);
	
	s_x <= NOT s_x AFTER 80ps;
	s_y <= NOT s_y AFTER 40ps;
	s_z <= NOT s_z AFTER 20ps;

	b_t(0) <= to_stdulogic(s_x);
	b_t(1) <= to_stdulogic(s_y);
	b_t(2) <= to_stdulogic(s_z);
	b_n <= to_integer(unsigned(b_t));
	s_test <= s_f XNOR truthtable(b_n);
	
END Testbench;